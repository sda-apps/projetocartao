package br.gov.ce.sda.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;

import br.gov.ce.sda.BaseIntegrationTest;

public class HomeControllerTest extends BaseIntegrationTest {
	
	@Test
	public void homeTest() throws Exception {
		mvc.perform(get("/")).andExpect(status().isOk());
	}
	
}
