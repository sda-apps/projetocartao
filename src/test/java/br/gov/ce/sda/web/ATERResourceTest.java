package br.gov.ce.sda.web;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import br.gov.ce.sda.BaseIntegrationTest;
import br.gov.ce.sda.web.rest.dto.AtendimentoDTO;

public class ATERResourceTest extends BaseIntegrationTest{

	private MockHttpServletRequestBuilder request;
	private AtendimentoDTO dto;

	private static final String NOME_NAO_EXISTENTE = "Não Existente";

	@Test
	public void buscarTecnicoAgricultorTest() throws Exception{
		// dado que eu tenho agricultor id
		Long agricultorId = 10001329151L;

		// quando eu fizer uma requisição de notas
		quandoEuBuscarTecnicosPorUmAgricultor(agricultorId);

		// então eu devo receber o status HttpStatus.OK (200)
		entaoEuDevoEsperar(status().isOk());
	}

	@Test
	public void buscarTecnicoAgricultorNaoEncontradoTest() throws Exception{
		// dado que eu tenho agricultor id inexistente
		Long agricultorId = 999999999L;

		// quando eu fizer uma requisição de notas
		quandoEuBuscarTecnicosPorUmAgricultor(agricultorId);

		// então eu devo receber o status HttpStatus.NOTFOUND (404)
		entaoEuDevoEsperar(status().isNotFound());
	}

	@Test
	public void buscarAtendimentoAgricultorTest() throws Exception {
		// dado que eu tenho um AtendimentoDTO completo
		dadoQueEuTenhoUmAtendimentoDTOCompleto();

		// quando eu fizer uma busca de atendimentos do agricultor
		quandoEuFizerUmaBuscaDeAtendimentosDoAgricultor();

		// entao eu devo receber uma lista de agricultores de tamanho 10
		entaoEuDevoReceberListaDeAtendimentosComTamanho(1);

		// ou dado que eu tenho um AtendimentoDTO sem argumentos
		dadoQueEuTenhoUmAtendimentoDTOComNome(null);

		// quando eu fizer uma busca de atendimentos do agricultor
		quandoEuFizerUmaBuscaDeAtendimentosDoAgricultor();

		// entao eu devo receber uma lista de atendimentos de tamanho 0
		entaoEuDevoReceberListaDeAtendimentosComTamanho(1);
	}

	@Test
	public void buscarAtendimentoAgricultorSemResultadoTest() throws Exception {
		// dado que eu tenho um AtendimentoDTO com nome Não Encontrado
		dadoQueEuTenhoUmAtendimentoDTOComNome(NOME_NAO_EXISTENTE);

		// quando eu fizer uma busca de atendimentos do agricultor
		quandoEuFizerUmaBuscaDeAtendimentosDoAgricultor();

		// entao eu devo receber uma lista de atendimentos de tamanho 0
		entaoEuDevoReceberListaDeAtendimentosComTamanho(0);
	}

	private void dadoQueEuTenhoUmAtendimentoDTOComNome(String tecnico) {
		dto = AtendimentoDTO.builder()
				.tecnico(tecnico)
				.build();
	}

	private void entaoEuDevoReceberListaDeAtendimentosComTamanho(int size) throws Exception {
		mvc.perform(request).andExpect(jsonPath("$", hasSize(size)));
	}

	private void quandoEuFizerUmaBuscaDeAtendimentosDoAgricultor() {
		request = get("/api/ater/atendimento")
				.params(convertToParameters(dto))
				.contentType(contentType)
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf());
	}

	private void dadoQueEuTenhoUmAtendimentoDTOCompleto() {
		dto = AtendimentoDTO.builder()
				.id(10001329151L)
				.ano(2016)
				.mes(8)
				.tecnico("Administrador")
				.atividade("Agricultor Familiar")
				.build();
	}

	public void buscarIndicadoreAtendimentoTest() throws Exception {
		// dado que eu tenho um AtendimentoDTO completo
		final Long ID_ATENDIMENTO_ATER = 10015693022L;

		// quando eu fizer uma busca por indicadores de um atendimentos do agricultor
		quandoEuFizerUmaBuscarPorIndicadoresAtendimento(ID_ATENDIMENTO_ATER);

		// entao eu devo receber uma lista de agricultores de tamanho 10
		entaoEuDevoReceberListaDeIndicadoresComTamanho(1);
	}

	@Test
	public void buscarIndicadoreAtendimentoInexistenteTest() throws Exception {
		// dado que eu tenho um AtendimentoDTO completo
		final Long ID_ATENDIMENTO_ATER = 222L;

		// quando eu fizer uma busca por indicadores de um atendimentos do agricultor
		quandoEuFizerUmaBuscarPorIndicadoresAtendimento(ID_ATENDIMENTO_ATER);

		// entao eu devo receber uma lista de agricultores de tamanho 10
		entaoEuDevoReceberListaDeIndicadoresComTamanho(0);
	}

	private void entaoEuDevoReceberListaDeIndicadoresComTamanho(int size) throws Exception {
		mvc.perform(request).andExpect(jsonPath("$", hasSize(size)));
	}

	private void quandoEuFizerUmaBuscarPorIndicadoresAtendimento(Long id) {
		request = get("/api/ater/atendimento/indicadores/"+ id )
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf())
				.contentType(contentType);
	}

	private void quandoEuBuscarTecnicosPorUmAgricultor(Long agricultorId) {
		request = get("/api/ater/tecnico/"+ agricultorId )
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf())
				.contentType(contentType);
	}

	private void entaoEuDevoEsperar(ResultMatcher matcher) throws Exception {
		mvc.perform(request).andExpect(matcher);
	}

}
