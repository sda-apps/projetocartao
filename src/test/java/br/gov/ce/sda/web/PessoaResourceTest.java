package br.gov.ce.sda.web;

import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.inject.Inject;

import br.gov.ce.sda.domain.Pessoa;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import br.gov.ce.sda.BaseIntegrationTest;
import br.gov.ce.sda.repository.CustomPessoaRepository;


public class PessoaResourceTest extends BaseIntegrationTest {

    private MockHttpServletRequestBuilder request;

    @Inject
    private CustomPessoaRepository customPessoaRepository;

    private static final Long PESSOA_ID = 10000056872l;

    @Test
    public void updateTest() throws Exception {
        // dado que eu tenho o id de uma pessoa e o obj pessoa
        Long idPessoa = PESSOA_ID;
        Pessoa pessoa = customPessoaRepository.buscarPessoaPorId(idPessoa);

        // salvo o apelido atual e altero por um novo
        String apelidoBackup = pessoa.getApelido();
        String apelidoNovo = "TESTE";
        pessoa.setApelido(apelidoNovo);

        // quando eu fizer uma requisição para alterar dados de pessoa
        quandoEuFizerUmaRequisicaoParaAlterarDadosDePessoa(idPessoa, pessoa);

        // devo esperar que orgao expedidor tenha sido alterado
        devoEsperarQueApelidoTenhaSidoAlterado(apelidoNovo);

        // então volto pessoa para o estado original
        pessoa.setApelido(apelidoBackup);
        quandoEuFizerUmaRequisicaoParaAlterarDadosDePessoa(idPessoa, pessoa);
        mvc.perform(request);
    }

    @Test
    public void updateIdErradoTest() throws Exception {
        // dado que eu tenho o id de uma pessoa e o obj pessoa
        Long idPessoa = PESSOA_ID;
        Pessoa pessoa = customPessoaRepository.buscarPessoaPorId(idPessoa);

        // salvo o orgaoExpedidorRg atual e altero por um novo
        String apelidoBackup = pessoa.getApelido();
        String apelidoNovo = "TESTE";
        pessoa.setApelido(apelidoNovo);

        // quando eu fizer uma requisição para alterar dados de pessoa
        quandoEuFizerUmaRequisicaoParaAlterarDadosDePessoa(0L, pessoa);

        // entao eu devo esperar que o agricultor nao foi encontrado na busca por ID
        entaoEuDevoEsperarStatus(status().isNotFound());
    }

    @Test
    public void getDadosBancariosTest() throws Exception {
        // dado que eu tenho o id de uma pessoa
        Long pessoaId = PESSOA_ID;

        // quando eu fizer uma requisação para obter os dados bancarios
        quandoEuFizerUmaRequisicaoParaObterOsDadosFinanceiros(pessoaId);

        // entao eu devo esperar o status ok
        entaoEuDevoEsperarStatus(status().isOk());
    }

    @Test
    public void getDadosBancariosPessoaInvalidaTest() throws Exception {
        // dado que eu tenho um id de pessoa invalido
        Long pessoaId = 0l;

        // quando eu fizer uma requisação para obter os dados bancarios
        quandoEuFizerUmaRequisicaoParaObterOsDadosFinanceiros(pessoaId);

        // entao eu devo esperar o status ok
        entaoEuDevoEsperarStatus(status().isNotFound());
    }

    private void quandoEuFizerUmaRequisicaoParaAlterarDadosDePessoa(long idPessoa, Pessoa pessoa) throws Exception {
        request = put("/api/pessoa/" + idPessoa)
            .content(toJson(pessoa))
            .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
            .with(csrf())
            .contentType(contentType);
    }

    private void devoEsperarQueApelidoTenhaSidoAlterado(String apelido) throws Exception {
        mvc.perform(request).andExpect(jsonPath("$.apelido", is(apelido)));
    }

    private void quandoEuFizerUmaRequisicaoParaObterOsDadosFinanceiros(Long pessoaId) {
        request = get("/api/pessoa/dados-bancarios/" + pessoaId)
            .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
            .with(csrf());
    }

    private void entaoEuDevoEsperarStatus(ResultMatcher matcher) throws Exception {
        mvc.perform(request).andExpect(matcher);
    }
}
