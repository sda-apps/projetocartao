/*
package br.gov.ce.sda.web;

import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import br.gov.ce.sda.BaseIntegrationTest;
import br.gov.ce.sda.domain.Cargo;
import br.gov.ce.sda.domain.Municipio;
import br.gov.ce.sda.domain.Pessoa;
import br.gov.ce.sda.domain.PessoaFisica;
import br.gov.ce.sda.domain.UsuarioAvancado;
import br.gov.ce.sda.domain.mobile.AreaAtuacao;
import br.gov.ce.sda.repository.AreaAtuacaoRepository;
import br.gov.ce.sda.repository.CustomUsuarioAvancadoRepository;
import br.gov.ce.sda.repository.MunicipioRepository;
import br.gov.ce.sda.repository.UserRepository;

public class UsuarioAvancadoResourceTest extends BaseIntegrationTest {

	private static final Long CPF_TECNICO = 60390438308L;

	private static final String URL = "/api/usuarioavancado/";

	private MockHttpServletRequestBuilder request;

	@Inject
	CustomUsuarioAvancadoRepository customUsuarioAvancadoRepository;

	@Inject
	UserRepository userRepository;

	@Inject
	private MunicipioRepository municipioRepository;

	@Inject
	private AreaAtuacaoRepository areaAtuacaoRepository;

	private Set<AreaAtuacao> areasAtuacao = new HashSet<>();

	private UsuarioAvancado usuarioAvancado;

	@Test
	public void buscarDadosUsuarioAvancadoTest() throws Exception {
		// dado que eu tenha um CPF de um usuario avancado

		// quando eu buscar os dados do usuario avancado
		quandoEuBuscarDadosDeUmUsuarioAvancado(CPF_TECNICO);

		//entao eu devo receber os dados do usuario avancado
		entaoEuDevoReceberUmIdDeUsuarioAvancado(10009554280L);
	}

	@Test
	public void buscarDadosUsuarioAvancadoNaoEncontradoTest() throws Exception {
		// dado que eu passe um CPF de um usuario avancado inexistente

		// quando eu buscar os dados do usuario avancado
		quandoEuBuscarDadosDeUmUsuarioAvancado(22222222223L);

		//entao devo receber o status 404
		entaoEuDevoEsperar(status().isNotFound());
	}

	@Test
	public void sendBroadcastEmailTest() throws Exception {
		//devo eu estar logado
		List<UsuarioAvancado> usuarioAvancadoSemLogin = customUsuarioAvancadoRepository.buscarTodosUsuarioAvancadoSemLogin();

		// quando eu disparar a funcao broadcast
		quandoEuDispararBroadcastDeEmail();

		//entao devo receber o status OK
		entaoEuDevoEsperarEDeletarUsuariosCriados(status().isOk(), usuarioAvancadoSemLogin);
	}


	@Ignore
	@Test
	@Transactional
	public void updateTest() throws Exception {
		// dado que eu tenho um usuário avançado com 3 áreas de atuação
		dadoQueEuTenhoUmUsuarioAvancadoCom3AreasDeAtuacao();

		Cargo cargo = new Cargo();
		cargo.setDescricao("Teste");
		cargo.setId(1L);

		Municipio acarau = municipioRepository.findOne(10000000919L);
		Municipio aquiraz = municipioRepository.findOne(10000000928L);
		Municipio aracati = municipioRepository.findOne(10000000929L);

		AreaAtuacao areaAtuacao1 = AreaAtuacao.builder().municipio(acarau).usuarioAvancado(usuarioAvancado).build();
		AreaAtuacao areaAtuacao2 = AreaAtuacao.builder().municipio(aquiraz).usuarioAvancado(usuarioAvancado).build();
		AreaAtuacao areaAtuacao3 = AreaAtuacao.builder().municipio(aracati).usuarioAvancado(usuarioAvancado).build();

		areasAtuacao.add(areaAtuacao1);
		areasAtuacao.add(areaAtuacao2);
		areasAtuacao.add(areaAtuacao3);

		Pessoa pessoa = new Pessoa();
		pessoa.setId(1L);
		pessoa.setNome("Nome");

		PessoaFisica pessoaFisica = new PessoaFisica();
		pessoaFisica.setCpf("56546654897");
		pessoaFisica.setPessoa(pessoa);

		usuarioAvancado = new UsuarioAvancado();
		usuarioAvancado.setId(0L);
		usuarioAvancado.setCargo(cargo);
		usuarioAvancado.setAreasAtuacao(areasAtuacao);
		usuarioAvancado.setPessoaFisica(pessoaFisica);
		usuarioAvancado.setEmailInstitucional("emailInstitucional");
		usuarioAvancado.setTelefoneTrabalho("998989898");


		// quando eu remover todas as áreas de atuação
		//usuarioAvancado.setAreasAtuacao(null);
		request = put(URL + usuarioAvancado.getId())
				.content(toJson(usuarioAvancado))
				//.with(usuarioTecnico())
				//.with(csrf())
				.contentType(contentType);

		// então eu devo receber uma lista de áreas de atuação de tamanho 0
		entaoEuDevoEsperar(status().isOk());
		//List<AreaAtuacao> areas = areaAtuacaoRepository.findAllByUsuarioAvancadoId(usuarioAvancado.getId());
		//Assert.assertEquals("error", 0, areas.size());
		areaAtuacaoRepository.delete(areasAtuacao);
		areasAtuacao.clear();
	}


	private void dadoQueEuTenhoUmUsuarioAvancadoCom3AreasDeAtuacao() {
		usuarioAvancado = customUsuarioAvancadoRepository.buscarPorId(10013477715L);

		Municipio acarau = municipioRepository.findOne(10000000919L);
		Municipio aquiraz = municipioRepository.findOne(10000000928L);
		Municipio aracati = municipioRepository.findOne(10000000929L);

		AreaAtuacao areaAtuacao1 = AreaAtuacao.builder().municipio(acarau).usuarioAvancado(usuarioAvancado).build();
		AreaAtuacao areaAtuacao2 = AreaAtuacao.builder().municipio(aquiraz).usuarioAvancado(usuarioAvancado).build();
		AreaAtuacao areaAtuacao3 = AreaAtuacao.builder().municipio(aracati).usuarioAvancado(usuarioAvancado).build();

		areasAtuacao.add(areaAtuacao1);
		areasAtuacao.add(areaAtuacao2);
		areasAtuacao.add(areaAtuacao3);
		areaAtuacaoRepository.save(areasAtuacao);

		//usuarioAvancado.setAreasAtuacao(areasAtuacao);
		//usuarioAvancadoRepository.save(usuarioAvancado);
	}

	private void quandoEuBuscarDadosDeUmUsuarioAvancado(Long cpfTecnico) {
		request = get(URL + "buscarcpf/" + cpfTecnico)
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf())
				.contentType(contentType);
	}

	private void quandoEuDispararBroadcastDeEmail() {
		request = get(URL + "sendbroadcastemail")
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf())
				.contentType(contentType);
	}

	private void entaoEuDevoReceberUmIdDeUsuarioAvancado(Long id) throws Exception {
		mvc.perform(request).andExpect(jsonPath("$.id", is(id)));
	}

	private void entaoEuDevoEsperarEDeletarUsuariosCriados(ResultMatcher matcher, List<UsuarioAvancado> usuarioAvancadoSemLogin) throws Exception {
		mvc.perform(request).andExpect(matcher);
		for (UsuarioAvancado usuarioAvancado : usuarioAvancadoSemLogin) {
			userRepository.findOneByUsuarioAvancado(usuarioAvancado).ifPresent(u -> {
	            userRepository.delete(u);
	        });
		}

	}

	private void entaoEuDevoEsperar(ResultMatcher matcher) throws Exception{
		mvc.perform(request).andExpect(matcher);
	}

}
*/
