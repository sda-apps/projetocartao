package br.gov.ce.sda.web;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import br.gov.ce.sda.BaseIntegrationTest;

public class ProjetoResourceTest extends BaseIntegrationTest{

	private MockHttpServletRequestBuilder request;

	@Test
	public void getNotasProjetosTest() throws Exception{
		// dado que eu tenho agricultor id e projeto id
		Long agricultorId = 10001264606L;
		Long projetoCodigoSDA = 14L;

		// quando eu fizer uma requisição de notas
		quandoEuFizerUmaRequisicaoDeNotas(agricultorId, projetoCodigoSDA);

		// então eu devo receber o status HttpStatus.OK (200)
		entaoEuDevoEsperar(status().isOk());
	}

	@Test
	public void getNotasProjetosDeProjetoSemNotaNoAgricultorTest() throws Exception{
		// dado que eu tenho agricultor id e projeto id
		Long agricultorId = 10001329151L;
		Long projetoCodigoSDA = 5L;

		// quando eu fizer uma requisição de notas
		quandoEuFizerUmaRequisicaoDeNotas(agricultorId, projetoCodigoSDA);

		// então eu devo receber o status HttpStatus.isNotFound (400)
		entaoEuDevoEsperar(status().isNotFound());
	}

	@Test
	public void getAgricultorProjetoPorAgricultorEProjetoTest() throws Exception{
		// dado que eu tenho agricultor id e projeto id
		Long agricultorId = 10001329151L;
		Long projetoCodigoSDA = 10L;

		// quando eu fizer uma requisição de notas
		quandoEuFizerUmaRequisicaoDeProjetos(agricultorId, projetoCodigoSDA);

		// então eu devo receber o status HttpStatus.OK (200)
		entaoEuDevoEsperar(status().isOk());
	}

	@Test
	public void getAgricultorProjetoPorAgricultorEProjetoInexistenteNoAgricultorTest() throws Exception{
		// dado que eu tenho agricultor id e projeto id
		Long agricultorId = 10001329151L;
		Long projetoCodigoSDA = 17L;

		// quando eu fizer uma requisição de notas
		quandoEuFizerUmaRequisicaoDeProjetos(agricultorId, projetoCodigoSDA);

		// então eu devo receber o status HttpStatus.isNotFound (400)
		entaoEuDevoEsperar(status().isNotFound());
	}

	private void quandoEuFizerUmaRequisicaoDeProjetos(Long agricultorId, Long projetoId) {
		request = get("/api/projeto/agricultor-projeto/"+ agricultorId +"/"+projetoId)
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf())
				.contentType(contentType);
	}

	private void quandoEuFizerUmaRequisicaoDeNotas(Long agricultorId, Long projetoId) {
		request = get("/api/agricultor/notas-projetos/"+ agricultorId +"/"+projetoId)
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf())
				.contentType(contentType);
	}

	private void entaoEuDevoEsperar(ResultMatcher matcher) throws Exception {
		mvc.perform(request).andExpect(matcher);
	}


}
