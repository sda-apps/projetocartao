package br.gov.ce.sda.web;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import br.gov.ce.sda.BaseIntegrationTest;

public class ContratoResourceTest extends BaseIntegrationTest{

	private MockHttpServletRequestBuilder request;

	@Test
	public void findByIdTest() throws Exception {
		Long contratoId = 100581749L;

		quandoEuFizerUmaRequisicaoParaTodosOsContratos(contratoId);

		entaoEuDevoEsperarStatus(status().isOk());
	}

	private void quandoEuFizerUmaRequisicaoParaTodosOsContratos(Long contratoId) {
		request = get("/api/contratos/" + contratoId)
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf());
	}

	@Test
	public void findByEmpresaIdTest() throws Exception {
		Long empresaId = 100581536L;

		quandoEuFizerUmaRequisicaoParaTodosOsContratosDeUmaEmpresa(empresaId);

		entaoEuDevoEsperarStatus(status().isOk());
	}

	private void quandoEuFizerUmaRequisicaoParaTodosOsContratosDeUmaEmpresa(Long empresaId) {
		request = get("/api/contratos/empresa/" + empresaId)
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf());
	}

	private void entaoEuDevoEsperarStatus(ResultMatcher matcher) throws Exception {
		mvc.perform(request).andExpect(matcher);
	}

}
