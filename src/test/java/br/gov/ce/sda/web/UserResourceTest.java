/*
package br.gov.ce.sda.web;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;
import java.util.Objects;

import javax.inject.Inject;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import br.gov.ce.sda.BaseIntegrationTest;
import br.gov.ce.sda.domain.Pessoa;
import br.gov.ce.sda.domain.PessoaFisica;
import br.gov.ce.sda.domain.UsuarioAvancado;
import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.repository.CustomUserRepository;
import br.gov.ce.sda.repository.PessoaRepository;
import br.gov.ce.sda.repository.UserRepository;
import br.gov.ce.sda.repository.UsuarioAvancadoRepository;
import br.gov.ce.sda.service.UserService;
import br.gov.ce.sda.service.util.RandomUtil;
import br.gov.ce.sda.web.rest.dto.FiltroUsuarioDTO;

public class UserResourceTest extends BaseIntegrationTest{

	@Inject
	private UserService userService;

	@Inject
	private CustomUserRepository customUserRepositor;

	@Inject
	private UserRepository userRepository;

	@Inject
	private UsuarioAvancadoRepository usuarioAvancadoRepository;

	@Inject
	private PessoaRepository pessoaRepository;

	private MockHttpServletRequestBuilder request;

	private Usuario usuario;
	private FiltroUsuarioDTO dto;

	@Test
	public void getUserLoginTest() throws Exception{

		//dado que eu tenho um login
		String login="admin";

		//quando eu fizer um requisicao para buscar um usuario pelo login
		quandoEuFizerUmaRequisicaoParaBuscarUmUsuarioPeloLogin(login);

		//entao devo receber o estatus ok
		entaoEuDevoEsperar(status().isOk());
	}

	@Test
	public void getUserLoginNaoEncontradoTest() throws Exception{

		//dado que eu tenho um login
		String login="usuarioinexistente";

		//quando eu fizer um requisicao para buscar um usuario pelo login
		quandoEuFizerUmaRequisicaoParaBuscarUmUsuarioPeloLogin(login);

		//entao devo receber o estatus ok
		entaoEuDevoEsperar(status().isNotFound());
	}

	@Test
	public void getAllUsersTest() throws Exception{

		//quando eu fizer um requisicao para buscar todos os usuario
		quandoEuFizerUmaRequisicaoParaBuscarTodosOsUsuarios();

		//entao devo receber o estatus ok
		entaoEuDevoEsperar(status().isOk());
	}

	@Test
	public void solicitarAcessoComCpfInexistenteTest() throws Exception {
		// dado que eu tenha um cpf inexistente na base de dados
		String cpfInexistente = "45645645654";

		// quando eu solicitar acesso ao sistema a partir desse cpf
		quandoEuSolicitarAcessoAoSistemaPeloCpf(cpfInexistente);

		// então eu devo rebecer o status not found
		entaoEuDevoEsperar(status().isNotFound());
	}

	@Test
	public void solicitarAcessoComCpfCadastradoEUsuarioAvancadoInexistenteTest() throws Exception {
		// dado que eu tenha um cpf existente na base de dados; porém, sem usuário avançado.
		String cpfComUsuarioAvancadoInexistente = "01308079311";

		// quando eu solicitar acesso ao sistema a partir desse cpf
		quandoEuSolicitarAcessoAoSistemaPeloCpf(cpfComUsuarioAvancadoInexistente);

		// então eu devo rebecer o status not found
		entaoEuDevoEsperar(status().isNotFound());
	}

	@Test
	public void solicitarAcessoComCpfCadastradoEUsuarioAvancadoExistenteTest() throws Exception {
		// dado que eu tenha um cpf existente na base de dados com usuário avançado cadsatrado.
		String cpfComUsuarioAvancadoInexistente = "00412258307";

		// quando eu solicitar acesso ao sistema a partir desse cpf
		quandoEuSolicitarAcessoAoSistemaPeloCpf(cpfComUsuarioAvancadoInexistente);

		// então eu devo rebecer o status ok
		entaoEuDevoEsperar(status().isOk());
		Usuario usuarioInativo = customUserRepositor.buscarUsuarioPorCpf(cpfComUsuarioAvancadoInexistente);
		userRepository.delete(usuarioInativo);
	}

	@Test
    @Transactional
	public void solicatAcessoComUsuarioCompletoCadastradoComStatusInativoTest() throws Exception {
		// dado que eu tenho um usuário cadastrado
		dadoQueEuTenhoUmUsuarioCompletoCadastradoComStatus(Usuario.Status.INATIVO);

		// quando eu solicitar acesso ao sistema a partir desse cpf
		quandoEuSolicitarAcessoAoSistemaPeloCpf(usuario.getUsuarioAvancado().getPessoaFisica().getCpf());

		// então eu devo rebecer o status isOk
		entaoEuDevoEsperar(status().isOk());
		deletarUsuarioCompletoCadastrado();
	}

	@Test
	public void buscarUsuariosEmpresaTest() throws Exception {
		// dado que eu tenho um filtro usuário dto
		dto = dadoQueEuTenhoUmFiltroUsuarioDTOCompleto();

		// quando eu buscar usuários por filtro
		quandoEuBuscarUsuariosPorFiltro();

		// então eu devo rebecer uma lista com tamanho 1
		entaoEuDevoReceberUmaListaComTamanho(1);
	}

    @Transactional
    @Test
    public void solicarAcessoComUsuarioCompletoCadastradoComStatusAtivoTest() throws Exception {
        // dado que eu tenho um usuário completo cadastrado
        dadoQueEuTenhoUmUsuarioCompletoCadastradoComStatus(Usuario.Status.ATIVADO);

        // quando eu solicitar acesso ao sistema a partir desse cpf
        quandoEuSolicitarAcessoAoSistemaPeloCpf(usuario.getUsuarioAvancado().getPessoaFisica().getCpf());

        // então eu devo rebecer o status isFound
        entaoEuDevoEsperar(status().isFound());
        deletarUsuarioCompletoCadastrado();
    }

	@Test
	public void buscarUsuariosEmpresaPorNomeTest() throws Exception {
		// dado que eu tenho um nome de usuário
		String nome = "JOSE ADRIANO FERREIRA PONTES";

		// quando eu buscar usuários por filtro
		quandoEuBuscarUsuariosDeUmaEmpresaPorNome(nome);

		// então eu devo rebecer uma lista com tamanho 1
		entaoEuDevoReceberUmaListaComTamanho(1);
	}

	private void quandoEuBuscarUsuariosDeUmaEmpresaPorNome(String nome) {
		request = get("/api/users/company/name/" + nome)
				//.param("nome", nome)
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf())
				.contentType(contentType);
	}

	private void entaoEuDevoReceberUmaListaComTamanho(int size) throws Exception {
		mvc.perform(request).andExpect(jsonPath("$", hasSize(size)));
	}

	private void quandoEuBuscarUsuariosPorFiltro() {
		request = get("/api/users/company")
				.params(convertToParameters(dto))
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf())
				.contentType(contentType);
	}

	private FiltroUsuarioDTO dadoQueEuTenhoUmFiltroUsuarioDTOCompleto() {
		return FiltroUsuarioDTO.builder()
				.cpf("01154950379")
				.usuario(9L)
				.empresa(10014472648L)
				.cargo(10014473738L)
                .status("A")
				.build();
	}

	private void deletarUsuarioCompletoCadastrado() {
		Long usuarioId = usuario.getId();
		Long usuarioAvancadoId = usuario.getUsuarioAvancado().getId();
		Long pessoaId = usuario.getUsuarioAvancado().getPessoaFisica().getPessoa().getId();

		userRepository.delete(usuarioId);
		usuarioAvancadoRepository.delete(usuarioAvancadoId);
		pessoaRepository.delete(pessoaId);
	}

	private void dadoQueEuTenhoUmUsuarioCompletoCadastradoComStatus(Usuario.Status status) {
		Pessoa pessoa = new Pessoa();
		pessoa.setNome("José Maria");
		pessoa.setApelido("José");

		PessoaFisica pessoaFisica = new PessoaFisica();
		pessoaFisica.setCpf("56546654897");
		pessoaFisica.setSexo("M");
		pessoaFisica.setDataNascimento(new Date());

		pessoaFisica.setPessoa(pessoa);

		UsuarioAvancado usuarioAvancado = new UsuarioAvancado();
		usuarioAvancado.setEmailInstitucional("portalagricultor@sda.ce.gov.br");
		usuarioAvancado.setPessoaFisica(pessoaFisica);

		usuario = new Usuario();
		usuario.setStatus(status);
		usuario.setLogin(RandomUtil.generateLogin());
		usuario.setSenha(RandomUtil.generatePassword());
		usuario.setUsuarioAvancado(usuarioAvancado );

		userRepository.save(usuario);
	}

	private void quandoEuSolicitarAcessoAoSistemaPeloCpf(String cpf) {
		request = get("/api/users/requestaccess/")
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.param("cpf", cpf)
				.with(csrf());
	}

	*/
/*@Test //TODO Não funciona, corrigir problema com autoridade
	public void getDeletUserLoginTest() throws Exception{

		//dado que eu tenho um login
		String login="DeleteTest";

		//dado que existe um usuario com esse login
		dadoQueEuTenhaUmUsuario(login, "123321");

		//quando eu fizer um requisicao para deletar um usuario pelo login
		quandoEuFizerUmaRequisicaoParaDeletarUmUsuarioPeloLogin(login);

		//entao devo receber o estatus ok
		entaoEuDevoEsperar(status().isOk());
	}

	@Test
	public void getDeletUserLoginNaoExistenteTest() throws Exception{

		//dado que eu tenho um login inexistente
		String login="UsuarioInexsitente";

		//quando eu fizer um requisicao para deletar um usuario pelo login
		quandoEuFizerUmaRequisicaoParaDeletarUmUsuarioPeloLogin(login);

		//entao devo receber o estatus ok
		entaoEuDevoEsperar(status().isNotFound());
	}*//*


	private void dadoQueEuTenhaUmUsuario(String login, String senha){
		userService.createUserInformation(login, senha);
	}

	private void quandoEuFizerUmaRequisicaoParaBuscarUmUsuarioPeloLogin(String login) {
		request = get("/api/users/" + login)
            .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
			.with(csrf());
	}

	private void quandoEuFizerUmaRequisicaoParaBuscarTodosOsUsuarios() {
		request = get("/api/users/")
            .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
			.with(csrf());
	}

	private void quandoEuFizerUmaRequisicaoParaDeletarUmUsuarioPeloLogin(String login) {
		request = delete("/api/users/" + login)
            .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
			.with(csrf());
	}

	private void entaoEuDevoEsperar(ResultMatcher matcher) throws Exception {
		mvc.perform(request).andExpect(matcher);
	}

	public MultiValueMap<String, String> convertToParameters(FiltroUsuarioDTO dto) {
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();

		if (Objects.nonNull(dto.getCpf())) {
			parameters.add("cpf", dto.getCpf());
		}

		if (Objects.nonNull(dto.getUsuario())) {
			parameters.add("usuario", dto.getUsuario().toString());
		}

		if (Objects.nonNull(dto.getEmpresa())) {
			parameters.add("empresa", dto.getEmpresa().toString());
		}

		if (Objects.nonNull(dto.getCargo())) {
			parameters.add("cargo", dto.getCargo().toString());
		}

		return parameters;
	}

}
*/
