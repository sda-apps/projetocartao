package br.gov.ce.sda.web;

import java.util.List;

import javax.inject.Inject;

import org.junit.Assert;
import org.junit.Test;

import br.gov.ce.sda.BaseIntegrationTest;
import br.gov.ce.sda.domain.programaseprojeto.Cisterna;
import br.gov.ce.sda.repository.CisternaRepository;

public class CisternaResourceTest extends BaseIntegrationTest {
	
	private static final Long AGRICULTOR_ID = 10001329151L;
	private static final Long AGRICULTOR_ID_SEM_CISTERNA = 10001329124L;
	
	@Inject
	private CisternaRepository cisternaRepository;
	
	@Test
	public void buscarCisternasPorAgricultorTest() throws Exception {
		//dado que eu tenho um agricultor com cisterna;
		
		List<Cisterna> cisternas = quandoEuBuscarCisternasPorAgricultor(AGRICULTOR_ID);
		
		entaoEuDevoReceberUmaListaDeCisternasDeTamanho(1, cisternas);
	}
	
	@Test
	public void buscarCisternasPorAgricultorSemCisternaTest() throws Exception {
		//dado que eu tenho um agricultor sem cisterna;
		
		List<Cisterna> cisternas = quandoEuBuscarCisternasPorAgricultor(AGRICULTOR_ID_SEM_CISTERNA);
		
		entaoEuDevoReceberUmaListaDeCisternasDeTamanho(0, cisternas);
	}

	private void entaoEuDevoReceberUmaListaDeCisternasDeTamanho(int size, List<Cisterna> cisternas) throws Exception {
		Assert.assertEquals(size, cisternas.size());
	}

	private List<Cisterna> quandoEuBuscarCisternasPorAgricultor(Long agricultorId) {
		List<Cisterna> cisternas = cisternaRepository.buscarCisternasPorAgricultor(agricultorId);
		
		return cisternas;
	}

}
