package br.gov.ce.sda.web.mobile;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import br.gov.ce.sda.BaseIntegrationTest;
import br.gov.ce.sda.domain.mobile.AtividadeCadastro;
import br.gov.ce.sda.repository.mobile.CustomAtividadeCadastroRepository;
import br.gov.ce.sda.web.rest.dto.mobile.FiltroCadastroDTO;

public class AtividadeCadastroResourceTest extends BaseIntegrationTest{

	private MockHttpServletRequestBuilder request;
	private FiltroCadastroDTO dto;

	private static final Long ATIVIDADE_CADASTRO_ID= 14473509L;

	@Inject
	private CustomAtividadeCadastroRepository customAtividadeCadastroRepository ;

	@Test
	public void listAtividadeCadastroTest() throws Exception {
		//dado que eu tenha um FiltroCadastroDTO completo
		dto = dadoQueEuTenhoUmFiltroCadastroDtoCompleto();

		//quando eu fizer uma requisição para buscar os cadastros
		quandoEuBuscarCadastrosPorFiltro();

		//entao devo receber uma lista com tamanho 2
		entaoEuDevoReceberUmTotalDeCadastrosComTamanho(2);
	}

	@Test
	public void listAtividadeCadastroTestSemFiltro() throws Exception {
		//dado que eu tenha um FiltroCadastroDTO completo
		dto = dadoQueEuNaoTenhaUmFiltroCadastroDtoCompleto();

		//quando eu fizer uma requisição para buscar os cadastros sem filtros
		quandoEuBuscarCadastrosPorFiltro();

		//entao devo receber uma lista com tamanho 2
		entaoEuDevoReceberUmTotalDeCadastrosComTamanho(2);
	}

    @Transactional(readOnly = true)
	@Test
	public void listAtividadeCadastroComUsuarioDeAutoridadeUserTest() throws Exception {
		//dado que eu tenha um FiltroCadastroDTO completo
		dto = dadoQueEuTenhoUmFiltroCadastroDtoCompleto();

		//quando eu fizer uma requisição para buscar os cadastros com um usuário de autoridade role_user
		quandoEuBuscarCadastrosPorFiltroComUsuarioDeAutoridadeUser();

		//entao devo receber uma lista com tamanho 1
		entaoEuDevoReceberUmTotalDeCadastrosComTamanho(1);
	}

	@Test
	public void listAtividadeCadastroComUsuarioSemUsuarioAvancadoTest() throws Exception {
		//dado que eu tenha um FiltroCadastroDTO completo
		dto = dadoQueEuTenhoUmFiltroCadastroDtoCompleto();

		//quando eu fizer uma requisição para buscar os cadastros com um usuário sem usuário avançado
		quandoEuBuscarCadastrosPorFiltroSemUsuarioAvancadoUser();

		//entao devo receber uma lista com tamanho 1
		entaoEuDevoEsperarStatus(status().isNotFound());
	}

	@Test
	public void getAtividadeCadastroPorIdTest() throws Exception {

		//dado que eu tenha o id de um agricultor

		//quando eu buscar os dados do agricultor
		quandoEuBuscarAtividadeCadastro(ATIVIDADE_CADASTRO_ID);

		//entao devo receber o status 200
		entaoEuDevoEsperarStatus(status().isOk());
	}

	@Test
	public void getAtividadeCadastroInexistenteTest() throws Exception {

		//dado que eu saiba um id de agricultor inexistente

		//quando eu buscar os dados do agricultor
		quandoEuBuscarAtividadeCadastro(0L);

		//entao devo receber o status 404
		entaoEuDevoEsperarStatus(status().isNotFound());
	}

	@Test
	public void updateTest() throws Exception {
		//dado que eu tenha o id de um atividade cadastro e um obj atividade cadastro
		AtividadeCadastro atividadeCadastro = customAtividadeCadastroRepository.buscarPorId(ATIVIDADE_CADASTRO_ID);

		// salvo o status e a observacao e altero por um novo
		String statusBackup = atividadeCadastro.getStatus();
		String statusNovo = "A";
		String observacaoBackup = atividadeCadastro.getObservacao();
		String observacaoNovo = "Observacao";
		atividadeCadastro.setStatus(statusNovo);
		atividadeCadastro.setObservacao(observacaoNovo);

		// quando eu fizer uma requisição para alterar dados de Atividade Cadastro
		quandoEuAtualizarAtividadeCadastro(ATIVIDADE_CADASTRO_ID, atividadeCadastro);

		//entao devo verificar campos Obs e Status alterados
		entaoEuDevoVerificarCamposPreenchidos(observacaoNovo,statusNovo);

		// então volto Atividade Cadastro para o estado original
		atividadeCadastro.setStatus(statusBackup);
		atividadeCadastro.setObservacao(observacaoBackup);
		quandoEuAtualizarAtividadeCadastro(ATIVIDADE_CADASTRO_ID, atividadeCadastro);
		mvc.perform(request);
	}

	@Test
	public void updateAtividadeCadastroInexistenteTest() throws Exception {
		//dado que eu tenha o id de um atividade cadastro inexistente com obs e status

		AtividadeCadastro atividadeCadastro = customAtividadeCadastroRepository.buscarPorId(ATIVIDADE_CADASTRO_ID);

		// salvo o status e a observacao e altero por um novo
		String statusBackup = atividadeCadastro.getStatus();
		String statusNovo = "A";
		String observacaoBackup = atividadeCadastro.getObservacao();
		String observacaoNovo = "Observacao";
		atividadeCadastro.setStatus(statusNovo);
		atividadeCadastro.setObservacao(observacaoNovo);

		// quando eu fizer uma requisição para alterar dados de Atividade Cadastro passando o id inexistente
		quandoEuAtualizarAtividadeCadastro(0L, atividadeCadastro);

		//entao devo receber o status 200
		entaoEuDevoEsperarStatus(status().isNotFound());

	}

	/*@Test
	public void updateAtividadeCadastroComAgricultorInexistenteTest() throws Exception {
		//dado que eu tenha o id de um atividade cadastro existente
		AtividadeCadastro atividadeCadastro = customAtividadeCadastroRepository.buscarPorId(ATIVIDADE_CADASTRO_ID_2);

		//dado que eu tenha um novo agricultor
		Agricultor novoAgricultor = new Agricultor();
		Pessoa novaPessoa = new Pessoa();
		novaPessoa.setCpf("42611643881");
		novoAgricultor.setPessoa(novaPessoa);

		Agricultor agricultorBackup = atividadeCadastro.getInformacaoInicial().getAgricultor();
		atividadeCadastro.getInformacaoInicial().setAgricultor(novoAgricultor);

		// quando eu fizer uma requisição para alterar dados de Atividade Cadastro passando o id inexistente
		quandoEuAtualizarAtividadeCadastro(ATIVIDADE_CADASTRO_ID_2, atividadeCadastro);

		// então volto Atividade Cadastro para o estado original
		atividadeCadastro.getInformacaoInicial().setAgricultor(agricultorBackup);
		mvc.perform(request);

	}*/


	private void quandoEuBuscarCadastrosPorFiltroSemUsuarioAvancadoUser() {
		request = get("/api/mobile/cadastro/")
				.params(convertToParameters(dto))
                .header(HttpHeaders.AUTHORIZATION, bearerTokenSemUsuarioAvancado)
				.with(csrf())
				.contentType(contentType);
	}

	private void quandoEuBuscarCadastrosPorFiltroComUsuarioDeAutoridadeUser() {
		request = get("/api/mobile/cadastro/")
				.params(convertToParameters(dto))
            .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnicoSemAutoridade)
				.with(csrf())
				.contentType(contentType);
	}

	private void quandoEuBuscarCadastrosPorFiltro() {
		request = get("/api/mobile/cadastro/")
				.params(convertToParameters(dto))
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf())
				.contentType(contentType);
	}

	private FiltroCadastroDTO dadoQueEuNaoTenhaUmFiltroCadastroDtoCompleto() {
		return FiltroCadastroDTO.builder()
				.build();
	}

	private FiltroCadastroDTO dadoQueEuTenhoUmFiltroCadastroDtoCompleto() {
		List<Long> municipio = new ArrayList<Long>();
		municipio.add(10000000027L);

		List<Long> comunidades = new ArrayList<Long>();
		comunidades.add(10000057681L);

		return FiltroCadastroDTO.builder()
				.status("A")
				.tecnico(10010073227L)
				.municipios(municipio)
				.comunidades(comunidades)
				.build();
	}

	private void entaoEuDevoReceberUmTotalDeCadastrosComTamanho(Integer size) throws Exception {
		mvc.perform(request).andExpect(jsonPath("$", hasSize(size)));
	}

	private void entaoEuDevoEsperarStatus(ResultMatcher matcher) throws Exception {
		mvc.perform(request).andExpect(matcher);
	}

	private void entaoEuDevoVerificarCamposPreenchidos(String obs, String status) throws Exception{
		mvc.perform(request).andExpect(jsonPath("$.observacao").value(obs));
		mvc.perform(request).andExpect(jsonPath("$.status").value(status));
	}

	public MultiValueMap<String, String> convertToParameters(FiltroCadastroDTO dto) {
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();

		parameters.put("municipios", convert(dto.getMunicipios()));
		parameters.put("comunidades", convert(dto.getComunidades()));

		if (Objects.nonNull(dto.getStatus())) {
			parameters.add("status", dto.getStatus());
		}

		if (Objects.nonNull(dto.getTecnico())) {
			parameters.add("tecnico", dto.getTecnico().toString());
		}

		if (Objects.nonNull(dto.getStart())) {
			parameters.add("start", dto.getStart().toString());
		}

		if (Objects.nonNull(dto.getLimit())) {
			parameters.add("limit", dto.getLimit().toString());
		}

		return parameters;
	}

	private void quandoEuBuscarAtividadeCadastro(Long atividadeCadastroId) {
		request = get("/api/mobile/cadastro/buscar/"+ atividadeCadastroId)
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf());
	}

	private void quandoEuAtualizarAtividadeCadastro(Long atividadeCadastroId, AtividadeCadastro atividadeCadastro) throws Exception {
		request = put("/api/mobile/cadastro/avaliar/"+ atividadeCadastroId)
				.content(toJson(atividadeCadastro))
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf())
				.contentType(contentType);
	}

}
