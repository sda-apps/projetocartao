package br.gov.ce.sda.web;

import br.gov.ce.sda.BaseIntegrationTest;
import br.gov.ce.sda.domain.Agricultor;
import br.gov.ce.sda.domain.Etnia;
import br.gov.ce.sda.repository.CustomAgricultorRepository;
import br.gov.ce.sda.repository.EtniaRepository;
import br.gov.ce.sda.web.rest.AgricultorResource;
import br.gov.ce.sda.web.rest.dto.FiltroAgricultorDTO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AgricultorResourceTest extends BaseIntegrationTest {

	@Inject
	private CustomAgricultorRepository customAgricultorRepository;

	@Mock
	private EtniaRepository etniaRepositoryMock;

	@InjectMocks
    private AgricultorResource agricultorResource;

	@Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

	private static final String NOME_ZUBANEUDA = "Zubaneuda";

	private static final String NOME_MARIA = "Maria";

	private static final Long AGRICULTOR_ID = 10001329151L;

	private static final Long AGRICULTOR_ID_SEM_POLIGONO = 10014416640L;

	private static final Long AGRICULTOR_ID_COM_CONJUGE = 10001329124L;

	private static final Long CONJUGE_ID = 10001809984L;

	private static final Long AGRICULTOR_SEM_CONJUGE = 10001329151L;

	private FiltroAgricultorDTO dto;

	private MockHttpServletRequestBuilder request;

	@Test
	public void buscarCisternaTest() throws Exception {
		//dado que eu tenha um agricultor com cisterna

		//quando eu fizer uma requisição para buscar uma cisterna do agricultor
		quandoEuFizerUmaRequisicaoParaBuscarUmaCisternaDoAgricultor(AGRICULTOR_ID);

		//entao devo receber o status 200
		entaoEuDevoEsperarStatus(status().isOk());
	}

	@Test
	public void buscarCisternaDeAgricultorSemCisternaTest() throws Exception {
		//dado que eu tenha um agricultor sem cisterna
		Long agricultorId = 10001827315L;

		//quando eu fizer uma requisição para buscar uma cisterna do agricultor
		quandoEuFizerUmaRequisicaoParaBuscarUmaCisternaDoAgricultor(agricultorId);

		//entao devo receber o status 200
		entaoEuDevoEsperarStatus(status().isNotFound());
	}

	@Test
	public void buscarConjugeAgricultorTest() throws Exception {
		//dado que eu tenha um agricultor com conjuge

		//quando eu buscar um conjuge pelo id do agricultor
		quandoEuBuscarUmConjugePeloIdDoAgricultor(AGRICULTOR_ID_COM_CONJUGE);

		//entao eu devo receber um conjuge com id
		entaoEuDevoReceberUmConjugeComId(CONJUGE_ID);
	}

	@Test
	public void buscarConjugeInexistenteDoAgricultorTest() throws Exception {
		//dado que eu tenha um agricultor sem conjuge

		//quando eu buscar um conjuge sem que o Agricultor possuir conjuge
		quandoEuBuscarUmConjugePeloIdDoAgricultor(AGRICULTOR_SEM_CONJUGE);

		//entao devo receber o status 404
		entaoEuDevoEsperarStatus(status().isNotFound());
	}

	@Test
	public void buscarConjugeAgricultorComAgricultorInexistenteTest() throws Exception {
		//dado que eu tenha um ID de agricultor inexistente
		Long id_inexistente = 0L;

		//quando eu buscar um conjuge por esse ID inexistente
		quandoEuBuscarUmConjugePeloIdDoAgricultor(id_inexistente);

		//entao devo receber o status 404
		entaoEuDevoEsperarStatus(status().isNotFound());
	}

	@Test
	public void getProjetosAgricultorPorAnoTest() throws Exception {
		//dado que eu tenha um agricultor e o ano
		Integer ano = 2016;

		//quando eu listar os projetos de um agricultor em um determinado ano
		quandoEuListarOsPrjetosDeUmAgricultorEmUmDeterminadoAno(AGRICULTOR_ID, ano);

		//entao devo receber uma lista com 3 resultados
		entaoEuDevoReceberUmaListaDeAgricultorProjetoComTamanho(1);
	}

	@Test
	public void getProjetosAgricultorPorAnoInexistenteTest() throws Exception {
		//dado que eu tenha um agricultor e um ano no qual o agricultor nao tenha projeto
		Integer ano = 2001;

		//quando eu listar os projetos de um agricultor em um determinado ano
		quandoEuListarOsPrjetosDeUmAgricultorEmUmDeterminadoAno(AGRICULTOR_ID, ano);

		//entao devo receber o status 404
		entaoEuDevoEsperarStatus(status().isNotFound());
	}

	@Test
	public void getProjetosAgricultorTest() throws Exception {

		//dado que eu tenha um agricultor com projeto

		//quando eu listar os projetos de um agricultor em um determinado ano
		quandoEuListarOsProjetosDeUmAgricultor(AGRICULTOR_ID);

		//entao devo receber uma lista com 3 resultados
		entaoEuDevoReceberUmaListaDeAgricultorProjetoComTamanho(1);
	}

	@Test
	public void getProjetosAgricultorSemProjetoTest() throws Exception {

		//dado que eu tenha um agricultor inexistente
		Long agricultorId=10001524520L;

		//quando eu listar os projetos de um agricultor em um determinado ano
		quandoEuListarOsProjetosDeUmAgricultor(agricultorId);

		//entao devo receber o status 404
		entaoEuDevoEsperarStatus(status().isNotFound());
	}


	@Test
	public void  getDapsAgricultorTest() throws Exception {
		//dado que eu tenha um agricultor com DAP

		//quando eu buscar um agricultor que não possui conjuge
		quandoEuBuscarDapsDoAgricultor(AGRICULTOR_ID);

		entaoEuDevoReceberUmaListaDeDapsComTamanho(7);
	}

	@Test
	public void  getDapsAgricultorSemDapTest() throws Exception {
		//dado que eu tenha um agricultor sem DAP
		Long agricultorId = 10001335271L;

		//quando eu buscar um agricultor que não possui conjuge
		quandoEuBuscarDapsDoAgricultor(agricultorId);

		//entao devo receber o status 404
		entaoEuDevoEsperarStatus(status().isNotFound());
	}

	@Test
	public void  buscarTotalAgricultoresPorFiltroTest() throws Exception {
		//dado que eu tenha um dtofiltro completamente preenchido
		dto = dadoQueEuTenhoUmFiltroAgricultorDtoCompleto();

		//quando eu buscar um agricultor que não possui conjuge
		quandoEuBuscarTotalAgricultoresPorFiltro();

		entaoEuDevoReceberUmTotalDeAgricultoresComTamanho(1);
	}

	@Test
	public void buscarAgricultoresPorFiltroTest() throws Exception {
		// dado que eu tenho um FiltroAgricultorDTO com nome Maria
		dadoQueEuTenhoUmFiltroAgricultorDTOComNome(NOME_MARIA);

		// quando eu fizer uma busca de agricultores por filtro
		quandoEuFizerUmaBuscaDeAgricultoresPorFiltro();

		// entao eu devo receber uma lista de agricultores de tamanho 10
		entaoEuDevoReceberListaAgricultoresComTamanho(10);
	}

	@Test
	public void buscarAgricultoresPorFiltroCompletoTest() throws Exception {
		// dado que eu tenho um FiltroAgricultorDTO Completo
		dto = dadoQueEuTenhoUmFiltroAgricultorDtoCompleto();

		// quando eu fizer uma busca de agricultores por filtro
		quandoEuFizerUmaBuscaDeAgricultoresPorFiltro();

		// entao eu devo receber uma lista de agricultores de tamanho 10
		entaoEuDevoReceberListaAgricultoresComTamanho(1);
	}

	@Test
	public void buscarAgricultoresPorFiltroSemResultadoTest() throws Exception {
		// dado que eu tenho um FiltroAgricultorDTO com nome Zubaneuda
		dadoQueEuTenhoUmFiltroAgricultorDTOComNome(NOME_ZUBANEUDA);

		// quando eu fizer uma busca de agricultores por filtro
		quandoEuFizerUmaBuscaDeAgricultoresPorFiltro();

		// entao eu devo receber uma lista de agricultores de tamanho 0
		entaoEuDevoReceberListaAgricultoresComTamanho(0);
	}

	@Test
	public void buscarAgricultoresPorFiltroComStartELimitNaoNuloTest() throws Exception {
		// dado que eu tenho um FiltroAgricultorDTO com nome Maria
		dadoQueEuTenhoUmFiltroAgricultorDTOComNomeEStartELimitNaoNulo(NOME_MARIA);

		// quando eu fizer uma busca de agricultores por filtro
		quandoEuFizerUmaBuscaDeAgricultoresPorFiltro();

		// entao eu devo receber uma lista de agricultores de tamanho 10
		entaoEuDevoReceberListaAgricultoresComTamanho(5);
	}

	@Test
	public void buscarAgricultoresPorFiltroPorMunicipio() throws Exception {
		// dado que eu tenho um FiltroAgricultorDTO com nome Maria
		dadoQueEuTenhoUmFiltroAgricultorPorMunicipio();

		// quando eu fizer uma busca de agricultores por filtro
		quandoEuFizerUmaBuscaDeAgricultoresPorFiltro();

		// entao eu devo receber uma lista de agricultores de tamanho 10
		entaoEuDevoReceberListaAgricultoresComTamanho(10);
	}

	@Test
	public void buscarAgricultoresPorFiltroOrdemAscTest() throws Exception {
		// dado que eu tenho um FiltroAgricultorDTO com nome Maria
		dadoQueEuTenhoUmFiltroAgricultorDTOComNomeOrdemAsc(NOME_MARIA);

		// quando eu fizer uma busca de agricultores por filtro
		quandoEuFizerUmaBuscaDeAgricultoresPorFiltro();

		// entao eu devo receber uma lista de agricultores de tamanho 10
		entaoEuDevoReceberListaAgricultoresComTamanho(10);
	}

	@Test
	public void buscarAgricultoresPorFiltroOrdemDescTest() throws Exception {
		// dado que eu tenho um FiltroAgricultorDTO com nome Maria
		dadoQueEuTenhoUmFiltroAgricultorDTOComNomeOrdemDesc(NOME_MARIA);

		// quando eu fizer uma busca de agricultores por filtro
		quandoEuFizerUmaBuscaDeAgricultoresPorFiltro();

		// entao eu devo receber uma lista de agricultores de tamanho 10
		entaoEuDevoReceberListaAgricultoresComTamanho(10);
	}

	@Test
	public void buscarAgricultoresPorFiltroComOrdenacaoInvalidaTest() throws Exception {
		// dado que eu tenho um FiltroAgricultorDTO com nome Maria
		dadoQueEuTenhoUmFiltroAgricultorDTOComNomeComOrdenacaoInvalida(NOME_MARIA);

		// quando eu fizer uma busca de agricultores por filtro
		quandoEuFizerUmaBuscaDeAgricultoresPorFiltro();

		// entao eu devo receber uma lista de agricultores de tamanho 10
		entaoEuDevoReceberListaAgricultoresComTamanho(10);
	}

	@Test
	public void  getLotesComPoligonoAgricultorTest() throws Exception {
		//dado que eu tenha um agricultor com poligono

		//quando eu buscar lotes com poligono de um agricultor
		quandoEuBuscarLotesComPoligonoAgricultor(AGRICULTOR_ID);

		entaoEuDevoEsperarStatus(status().isOk());
	}

	@Test
	public void  getLotesComPoligonoAgricultorNaoEncontradoTest() throws Exception {
		//dado que eu tenha um agricultor sem poligono

		//quando eu buscar lotes com poligono de um agricultor
		quandoEuBuscarLotesComPoligonoAgricultor(AGRICULTOR_ID_SEM_POLIGONO);

		entaoEuDevoEsperarStatus(status().isNotFound());
	}

	@Test
	public void buscarAgricultoresAreasGeorreferenciadasPorFiltroTest() throws Exception {
		//dados que eu tenha um FiltroAgricultorDto completo com area georreferenciada
		dto = dadoQueEuTenhoUmFiltroAgricultorDtoCompleto();

		//quando eu fizer uma busca de agricultor com areas georreferenciadas por filtro
		quandoEuFizerUmaBuscaDeAgricultoresComAreasGeorreferenciadasPorFiltro();

		//entao eu devo receber o status ok
		entaoEuDevoEsperarStatus(status().isOk());
	}

	@Test
	public void buscarAgricultoresAreasGeorreferenciadasPorFiltroNaoEncontradoTest() throws Exception {
		//dados que eu tenha um FiltroAgricultorDto compelto sem area georreferenciada
		dadoQueEuTenhoUmAgricultorDtoSemAreaGeorreferenciada();

		//quando eu fizer uma busca de agricultor com areas georreferenciadas por filtro
		quandoEuFizerUmaBuscaDeAgricultoresComAreasGeorreferenciadasPorFiltro();

		//entao eu devo receber o status ok
		entaoEuDevoEsperarStatus(status().isNotFound());
	}

	@Test
	public void updateTest() throws Exception {
		// dado que eu tenho o id de um agricultor e o obj agricultor
		Agricultor agricultor = customAgricultorRepository.buscarPorId(AGRICULTOR_ID);

		// salvo o orgaoExpedidorRg atual e altero por um novo
		String antigoNIS = agricultor.getPessoaFisica().getNis();
		String novoNIS = "TESTE";
		agricultor.getPessoaFisica().setNis(novoNIS);

		// quando eu fizer uma requisição para alterar dados do agricultor
		quandoEuFizerUmaRequisicaoParaAlterarDadosDoAgricultor(AGRICULTOR_ID, agricultor);

		// devo esperar que nis do agricultor tenha sido alterado
		devoEsperarQueNISTenhaSidoAlterado(novoNIS);

		// então volto o agricultor para o estado original
		agricultor.getPessoaFisica().setNis(antigoNIS);
		quandoEuFizerUmaRequisicaoParaAlterarDadosDoAgricultor(AGRICULTOR_ID, agricultor);
		mvc.perform(request);
	}

	@Test
	public void updateAgricultorInexistenteTest() throws Exception{
		// dado que eu tenho um agricultor com id invalido
		Agricultor agricultor = new Agricultor();
		agricultor.setId(0l);

		// quando eu fizer uma requisação para obter os dados bancarios
		quandoEuFizerUmaRequisicaoParaAlterarDadosDoAgricultor(agricultor.getId(), agricultor);

		// entao eu devo esperar o status not found
		entaoEuDevoEsperarStatus(status().isNotFound());
	}

	@Test
	public void buscarEtniaTest() throws Exception {
		// quando eu fizer uma busca para obter as etnias
		quandoEuFizerUmaBuscaParaObterEtnias();

		// entao eu devo esperar uma lista com tamanho 5
		entaoEuDevoReceberUmaListaDeEtniasDeTamanho(5);
	}

	@Test
	public void buscarEtniasComResultadoVazioTest() throws Exception {
		Mockito.when(etniaRepositoryMock.findAllByOrderByNomeAsc()).thenReturn(new ArrayList<Etnia>());

		// quando eu fizer uma busca para obter as etnias
		ResponseEntity<List<Etnia>> result = agricultorResource.buscarEtnia();

		// entao eu devo esperar o status not found
		Assert.assertEquals(result.getStatusCode(), HttpStatus.NOT_FOUND);
	}

	@Test
	public void getDadosAgricultorTest() throws Exception {

		//dado que eu tenha o id de um agricultor

		//quando eu buscar os dados do agricultor
		quandoEuBuscarOsDadosDoAgricultor(AGRICULTOR_ID);

		//entao devo receber o status 200
		entaoEuDevoEsperarStatus(status().isOk());
	}

	@Test
	public void getDadosAgricultorInexistenteTest() throws Exception {

		//dado que eu saiba um id de agricultor inexistente

		//quando eu buscar os dados do agricultor
		quandoEuBuscarOsDadosDoAgricultor(0L);

		//entao devo receber o status 404
		entaoEuDevoEsperarStatus(status().isNotFound());
	}

	private void dadoQueEuTenhoUmAgricultorDtoSemAreaGeorreferenciada() {
		List<Long> territorios = new ArrayList<Long>();
		territorios.add(10000046265L);

		List<Long> municipio = new ArrayList<Long>();
		municipio.add(10000001274L);

		List<Long> distrito = new ArrayList<Long>();
		distrito.add(10000047024L);

		dto = FiltroAgricultorDTO.builder()
				.nome("andre")
				.territorio(territorios)
				.municipio(municipio)
				.cpf("11111111111")
				.distrito(distrito)
				.build();
	}

	private void dadoQueEuTenhoUmFiltroAgricultorDTOComNome(String nome) {
		dto = new FiltroAgricultorDTO();
		dto.setNome(nome);
	}

	private void dadoQueEuTenhoUmFiltroAgricultorDTOComNomeOrdemAsc(String nome) {
		dto = new FiltroAgricultorDTO();
		dto.setNome(nome);
		dto.setOrdem("asc");
		dto.setOrdenar(true);
	}

	private void dadoQueEuTenhoUmFiltroAgricultorDTOComNomeOrdemDesc(String nome) {
		dto = new FiltroAgricultorDTO();
		dto.setNome(nome);
		dto.setOrdem("desc");
		dto.setOrdenar(true);
	}

	private void dadoQueEuTenhoUmFiltroAgricultorDTOComNomeComOrdenacaoInvalida(String nome) {
		dto = new FiltroAgricultorDTO();
		dto.setNome(nome);
		dto.setOrdem("teste");
		dto.setOrdenar(true);
	}

	private void dadoQueEuTenhoUmFiltroAgricultorDTOComNomeEStartELimitNaoNulo(String nome) {
		dto = new FiltroAgricultorDTO();
		dto.setNome(nome);
		dto.setStart(1);
		dto.setLimit(5);
		dto.setOrdenar(false);
	}

	private void dadoQueEuTenhoUmFiltroAgricultorPorMunicipio() {

		List<Long> municipio = new ArrayList<Long>();
		municipio.add(10000001090L);

		dto = FiltroAgricultorDTO.builder()
				.municipio(municipio)
				.build();
	}

	private void quandoEuFizerUmaBuscaParaObterEtnias() {
		request = get("/api/agricultor/etnia")
				.header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf())
				.contentType(contentType);
	}

	private void quandoEuFizerUmaRequisicaoParaAlterarDadosDoAgricultor(Long agricultorId, Agricultor agricultor) throws Exception {
		request = put("/api/agricultor/"+ agricultorId)
				.content(toJson(agricultor))
				.header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf())
				.contentType(contentType);
	}

	private void quandoEuBuscarTotalAgricultoresPorFiltro() {
		request = get("/api/agricultores/filtro/total")
				.params(convertToParameters(dto))
				.header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf())
				.contentType(contentType);
	}

	private void quandoEuBuscarDapsDoAgricultor(Long agricultorId) {
		request = get("/api/agricultor/daps/"+ agricultorId)
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf());
	}
	private void quandoEuBuscarOsDadosDoAgricultor(Long agricultorId) {
		request = get("/api/agricultor/"+ agricultorId)
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf());
	}


	private void quandoEuFizerUmaBuscaDeAgricultoresPorFiltro() {
		request = get("/api/agricultores/filtro/")
				.params(convertToParameters(dto))
				.contentType(contentType)
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf());
	}

	private void quandoEuFizerUmaBuscaDeAgricultoresComAreasGeorreferenciadasPorFiltro() {
		request = get("/api/agricultores/lotes/poligonos")
				.params(convertToParameters(dto))
				.contentType(contentType)
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf());
	}

	private void quandoEuListarOsPrjetosDeUmAgricultorEmUmDeterminadoAno(Long agricultorId, Integer ano) {
		request = get("/api/agricultor/projetos/"+ agricultorId +"/" + ano)
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf());
	}

	private void quandoEuBuscarUmConjugePeloIdDoAgricultor(Long agricultorId) {
		request = get("/api/agricultor/conjuge/" + agricultorId)
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf());
	}

	private void quandoEuFizerUmaRequisicaoParaBuscarUmaCisternaDoAgricultor(Long agricultorId) {
		request = get("/api/agricultor/cisterna/" + agricultorId)
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf());
	}

	private void quandoEuListarOsProjetosDeUmAgricultor(Long agricultorId) {
		request = get("/api/agricultor/projetos/" + agricultorId)
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf());
	}

	private void quandoEuBuscarLotesComPoligonoAgricultor(Long agricultorId) {
		request = get("/api/agricultor/lote/poligono/"+ agricultorId)
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf());
	}

	private void entaoEuDevoReceberUmaListaDeAgricultorProjetoComTamanho(Integer size) throws Exception {
		mvc.perform(request).andExpect(jsonPath("$", hasSize(size)));
	}

	private void entaoEuDevoEsperarStatus(ResultMatcher matcher) throws Exception {
		mvc.perform(request).andExpect(matcher);
	}

	private void entaoEuDevoReceberUmConjugeComId(Long id) throws Exception {
		mvc.perform(request).andExpect(jsonPath("$.id", is(id)));
	}

	private void entaoEuDevoReceberUmaListaDeDapsComTamanho(Integer size) throws Exception {
		mvc.perform(request).andExpect(jsonPath("$", hasSize(size)));
	}

	private void entaoEuDevoReceberUmTotalDeAgricultoresComTamanho(Integer size) throws Exception {
		mvc.perform(request).andExpect(jsonPath("$.total", is(size)));
	}

	private void entaoEuDevoReceberListaAgricultoresComTamanho(int size) throws Exception {
		mvc.perform(request).andExpect(jsonPath("$.agricultores", hasSize(size)));
	}

	private void devoEsperarQueNISTenhaSidoAlterado(String novoNIS) throws Exception {
		mvc.perform(request).andExpect(jsonPath("$.nis", is(novoNIS)));
	}

	private void entaoEuDevoReceberUmaListaDeEtniasDeTamanho(int size) throws Exception {
		mvc.perform(request).andExpect(jsonPath("$", hasSize(size)));
	}

}
