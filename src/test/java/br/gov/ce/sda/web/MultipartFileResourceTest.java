package br.gov.ce.sda.web;

import br.gov.ce.sda.BaseIntegrationTest;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class MultipartFileResourceTest extends BaseIntegrationTest {

    private static final String URL = "/api/files";

    private MockHttpServletRequestBuilder request;

    private MockMultipartFile file;

    private String fileName;

    private String bucketName;

    @Test
    public void uploadFileTest() throws Exception {
        dadoQueEuTenhoUmArquivo();

        quandoEuFizerUploadDoArquivo();

        entaoEuDevoEsperarStatus(status().isOk());
    }

    @Test
    public void downloadFileTest() throws Exception {
        dadoQueEuTenhoMetadadosDeUmArquivo();

        quandoEuFizerDownloadDoArquivo();

        entaoEuDevoEsperarStatus(status().isOk());
    }

    @Test
    public void downloadListFilesTest() throws Exception {
        dadoQueEuTenhoUmaURLDeUmaPasta();

        quandoEuFizerDownloadDaListaDeArquivos();

        entaoEuDevoEsperarStatus(status().isOk());
    }

    @Test
    public void downloadNonexistentFileTest() throws Exception {
        dadoQueEuTenhoMetadadosDeUmArquivoInexistente();

        quandoEuFizerDownloadDoArquivo();

        entaoEuDevoEsperarStatus(status().isNotFound());
    }

    private void dadoQueEuTenhoMetadadosDeUmArquivoInexistente() {
        fileName = "file.pdf";
        bucketName = "cartao/testing";
    }

    private void dadoQueEuTenhoMetadadosDeUmArquivo() {
        fileName = "kotlin-docs.pdf";
        bucketName = "cartao/testing";
    }

    private void dadoQueEuTenhoUmaURLDeUmaPasta() {
        bucketName = "cartao/ater_atividades/37789206/photos";
    }

    private void quandoEuFizerDownloadDoArquivo() {
        request = get(URL)
            .param("bucketName", bucketName)
            .param("fileName", fileName)
            .header(AUTHORIZATION, bearerTokenTecnico);
    }

    private void quandoEuFizerDownloadDaListaDeArquivos() {
        request = get(URL)
            .param("bucketName", bucketName)
            .header(AUTHORIZATION, bearerTokenTecnico);
    }

    private void dadoQueEuTenhoUmArquivo() {
        file = new MockMultipartFile("file",
            "filename.txt",
            "text/plain", "some xml json yml".getBytes());
    }

    private void quandoEuFizerUploadDoArquivo() {
        request = multipart(URL)
            .file(file)
            .param("bucketName", "cartao/testing")
            .header(AUTHORIZATION, bearerTokenTecnico);
    }

    private void entaoEuDevoEsperarStatus(ResultMatcher matcher) throws Exception {
        mvc.perform(request).andExpect(matcher);
    }

}
