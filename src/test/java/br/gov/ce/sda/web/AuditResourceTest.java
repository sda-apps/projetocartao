package br.gov.ce.sda.web;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import br.gov.ce.sda.BaseIntegrationTest;
import br.gov.ce.sda.web.rest.AuditResource;

public class AuditResourceTest extends BaseIntegrationTest{

	@InjectMocks
	private AuditResource auditResource;

	private MockHttpServletRequestBuilder request;

	@Test
	public void getAllTest() throws Exception{
		quandoEuBuscarTodosOsAuditsEvents();

		entaoEuDevoReceberUmaListaDeEventsComTamanho(112);
	}

	@Test
	public void getByDatesTest() throws Exception{
		quandoEuBuscarOsAuditsEventsPorData();

		entaoEuDevoReceberUmaListaDeEventsComTamanho(103);
	}

	@Test
	public void getByIDTest() throws Exception{

		quandoEuBuscarOsAuditsEventsPorID(1020l);

		entaoEuDevoEsperar(status().isOk());
	}

	@Test
	public void getByIDInexisteteTest() throws Exception{

		quandoEuBuscarOsAuditsEventsPorID(10l);

		entaoEuDevoEsperar(status().isNotFound());
	}

	private void quandoEuBuscarTodosOsAuditsEvents() {
		request = get("/api/audits")
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf());
	}

	private void quandoEuBuscarOsAuditsEventsPorData() {
		request = get("/api/audits/?fromDate=2016-08-03&toDate=2016-09-01")
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf());
	}

	private void quandoEuBuscarOsAuditsEventsPorID(Long ID) {
		request = get("/api/audits/"+ID)
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf());
	}

	private void entaoEuDevoReceberUmaListaDeEventsComTamanho(Integer size) throws Exception {
		mvc.perform(request).andExpect(jsonPath("$", hasSize(size)));
	}

	private void entaoEuDevoEsperar(ResultMatcher matcher) throws Exception {
		mvc.perform(request).andExpect(matcher);
	}


}
