package br.gov.ce.sda.web;

import br.gov.ce.sda.BaseIntegrationTest;
import br.gov.ce.sda.domain.acesso.Usuario;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AccountResourceTest extends BaseIntegrationTest {

	private static final String LOGIN_TECNICO = "admin";

	private MockHttpServletRequestBuilder request;

	private List<Usuario> usuairos;

	@Test
	public void changePasswordTest() throws Exception {
		// dado que eu tenho uma senha
		String senha = "admin2";

		// quando eu fizer uma requisição para alterar senha
		quandoEuFizerUmaRequisicaoParaAlterarSenha(senha);

		// então eu devo receber o status HttpStatus.OK (200)
		entaoEuDevoEsperar(status().isOk());
	}

	@Test
	public void isAuthenticatedTest() throws Exception {
		// dado que eu tenho um usuário técnico logado

		// quando eu verificar se existe um usuário logado
		quandoEuVerificarSeExisteUmUsuarioLogado();

		// então eu devo receber o login do usuário técnico logado
		entaoEuDevoReceberUmUsuarioComLogin(LOGIN_TECNICO);
	}

	@Test
	public void isAuthenticatedFailTest() throws Exception {
		// dado que eu tenho não tenho um usuário logado

		// quando eu verificar se existe um usuário logado
		quandoEuVerificarSeExisteUmUsuarioLogadoSemUsuairo();

		// então eu não devo receber o login do usuário logado
		entaoEuDevoReceberUmUsuarioComLogin("");
	}

	@Test
	public void getAccountTest() throws Exception {
		//dado que eu tenho um usuário logado

		// quando eu buscar a conta do usuario logado
		quandoEuBuscarContaUsuarioLogado();

		//então devo receber a conta do usuario com login TECNICO
		entaoEuDevoReceberUmLoginDoUsuario(LOGIN_TECNICO);
	}

	@Test
	public void getAccountDeslogadoTest() throws Exception {
		//dado que eu não tenho um usuário logado

		// quando eu buscar a conta do usuario logado
		quandoEuBuscarContaUsuarioDeslogado();

		//então devo receber a conta do usuario com login TECNICO
		entaoEuDevoEsperar(status().isUnauthorized());
	}

	@Test
	public void approveAccessRequestTest() throws Exception {
		// dado que eu tenho uma lista de usuários
		dadoQueEuTenhoUmaListaDeUsuarios();

		// quando eu solicitar a aprovação das requisições de acesso
		quandoEuSolicitarAprovacaoDasRequisicoesDeAcesso();

		// então eu devo rebecer o status ok
		entaoEuDevoEsperar(status().isOk());
	}

	private void dadoQueEuTenhoUmaListaDeUsuarios() {
		usuairos = new ArrayList<>();
		Usuario usuario1 = new Usuario();
		usuario1.setId(1L);
		usuario1.setCreatedDate(null);
		usuairos.add(usuario1);
	}

	private void quandoEuSolicitarAprovacaoDasRequisicoesDeAcesso() throws Exception {
		request = post("/api/account/approve/accessrequest")
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf())
				.content(toJson(usuairos))
				.contentType(contentType);
	}

	private void quandoEuBuscarContaUsuarioLogado() {
		request = get("/api/account")
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf())
				.contentType(contentType);
	}

	private void quandoEuBuscarContaUsuarioDeslogado() {
		request = get("/api/account")
				.with(csrf())
				.contentType(contentType);
	}

	private void entaoEuDevoReceberUmUsuarioComLogin(String login) throws Exception {
		String usuarioAutenticadoLogin = mvc.perform(request).andReturn().getResponse().getContentAsString();
		assertEquals(usuarioAutenticadoLogin, login);
	}

	private void entaoEuDevoReceberUmLoginDoUsuario(String login) throws Exception {
		mvc.perform(request).andExpect(jsonPath("$.login", is(login)));
	}

	private void quandoEuVerificarSeExisteUmUsuarioLogadoSemUsuairo() {
		request = get("/api/authenticate")
				.with(csrf())
				.contentType(contentType);
	}

	private void quandoEuVerificarSeExisteUmUsuarioLogado() {
		request = get("/api/authenticate")
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf())
				.contentType(contentType);
	}

	private void quandoEuFizerUmaRequisicaoParaAlterarSenha(String senha) {
		request = post("/api/account/change_password")
			.content(senha)
            .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
			.with(csrf())
			.contentType(contentType);
	}

	private void entaoEuDevoEsperar(ResultMatcher matcher) throws Exception {
		mvc.perform(request).andExpect(matcher);
	}
}
