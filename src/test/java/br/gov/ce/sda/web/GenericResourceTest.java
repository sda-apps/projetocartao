package br.gov.ce.sda.web;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import br.gov.ce.sda.BaseIntegrationTest;

public class GenericResourceTest extends BaseIntegrationTest {

	private MockHttpServletRequestBuilder request;

	@Test
	public void getMunicipiosTest() throws Exception{
		//dado que eu tenha o nome do municipio
		String municipio = "Abaiara";

		//quando eu fizer um requisicao para buscar o municipio
		quandoEuFizerUmaRequisicaoParaBuscarOMunicipio(municipio);

		//entao devo receber o status ok
		entaoEuDevoEsperar(status().isOk());

	}

	@Test
	public void getMunicipiosNaoEncontradoTest() throws Exception{
		//dado que eu tenha o nome do municipio
		String municipio = "Frankfurt";

		//quando eu fizer um requisicao para buscar o municipio
		quandoEuFizerUmaRequisicaoParaBuscarOMunicipio(municipio);

		//entao devo receber o status not found
		entaoEuDevoEsperar(status().isNotFound());

	}

	@Test
	public void getTerritorioTest() throws Exception{
		//dado que eu tenha o nome do territorio
		String territorio = "Cariri";

		//quando eu fizer um requisicao para buscar o territorio
		quandoEuFizerUmaRequisicaoParaBuscarOTerritorio(territorio);

		//entao devo receber o status ok
		entaoEuDevoEsperar(status().isOk());

	}

	@Test
	public void getTerritorioNaoEncontradoTest() throws Exception{
		//dado que eu tenha o nome do territorio
		String territorio = "Teste";

		//quando eu fizer um requisicao para buscar o territorio
		quandoEuFizerUmaRequisicaoParaBuscarOTerritorio(territorio);

		//entao devo receber o status not found
		entaoEuDevoEsperar(status().isNotFound());
	}

	@Test
	public void getDistritoTest() throws Exception{
		//dado que eu tenha o nome do distrito
		String distrito = "Catarina";

		//quando eu fizer um requisicao para buscar o distrito
		quandoEuFizerUmaRequisicaoParaBuscarODistrito(distrito);

		//entao devo receber o status ok
		entaoEuDevoEsperar(status().isOk());

	}

	@Test
	public void getDistritoNaoEncontradoTest() throws Exception{
		//dado que eu tenha o nome do distrito
		String distrito = "Teste";

		//quando eu fizer um requisicao para buscar o distrito
		quandoEuFizerUmaRequisicaoParaBuscarODistrito(distrito);

		//entao devo receber o status not found
		entaoEuDevoEsperar(status().isNotFound());

	}

	@Test
	public void getProjetoTest() throws Exception{
		//dado que eu tenha o nome do projeto
		String projeto = "PAA LEITE";

		//quando eu fizer um requisicao para buscar o projeto
		quandoEuFizerUmaRequisicaoParaBuscarOProjeto(projeto);

		//entao devo receber o status ok
		entaoEuDevoEsperar(status().isOk());
	}

	@Test
	public void getProjetoNaoEncontradoTest() throws Exception{
		//dado que eu tenha o nome do projeto
		String projeto = "Teste";

		//quando eu fizer um requisicao para buscar o projeto
		quandoEuFizerUmaRequisicaoParaBuscarOProjeto(projeto);

		//entao devo receber o status not found
		entaoEuDevoEsperar(status().isNotFound());

	}

	@Test
	public void getMunicipioComTerritorioTest() throws Exception{
		//dado que eu tenha o id do territorio
		String territorio = "10000046224";

		//dado que eu tenha o nome do municipio
		String municipio =  "mauriti";

		//quando eu fizer uma requisicao para buscar o municipio tendo um territorio
		quandoEuFizerUmaRequisicaoParaBuscarOMunicipioTendoUmTerritorio(territorio, municipio);

		//entao devo receber o status ok
		entaoEuDevoEsperar(status().isOk());
	}

	@Test
	public void getMunicipioComTerritorioForaDoTerritorioTest() throws Exception{
		//dado que eu tenha o id do territorio
		String territorio = "10000046224";

		//dado que eu tenha o nome do municipio no territorio
		String municipio =  "fortaleza";

		//quando eu fizer uma requisicao para buscar o municipio tendo um territorio
		quandoEuFizerUmaRequisicaoParaBuscarOMunicipioTendoUmTerritorio(territorio, municipio);

		//entao devo receber o status ok
		entaoEuDevoEsperar(status().isNotFound());
	}

	@Test
	public void getDistritoComMunicipioTest() throws Exception{

		//dado que eu tenha o id do municipio
		String municipio =  "10000000917";

		//dado que eu tenha o nome do distrito
		String distrito = "ABAIARA";

		//quando eu fizer uma requisicao para buscar o distrito tendo um municipio
		quandoEuFizerUmaRequisicaoParaBuscarODistritoTendoUmMunicipio(distrito, municipio);

		//entao devo receber o status ok
		entaoEuDevoEsperar(status().isOk());
	}

	@Test
	public void getDistritoComMunicipioNaoEncontradoTest() throws Exception{

		//dado que eu tenha o id do municipio
		String municipio =  "10000001108";

		//dado que eu tenha o nome do distrito
		String distrito = "fortaleza";

		//quando eu fizer uma requisicao para buscar o municipio tendo um territorio
		quandoEuFizerUmaRequisicaoParaBuscarODistritoTendoUmMunicipio(distrito, municipio);

		//entao devo receber o status ok
		entaoEuDevoEsperar(status().isNotFound());
	}

	@Test
	public void getLocalidadesPorMunicipioTest() throws Exception{
		//dado que eu tenha o id do municipio
		String municipio =  "10000001076";

		//quando eu fizer uma requisicao para buscar o distrito tendo um municipio
		quandoEuFizerUmaRequisicaoParaBuscarLocalidadesTendoUmMunicipio(municipio);

		//entao devo receber o status ok
		entaoEuDevoEsperar(status().isOk());
	}


	private void quandoEuFizerUmaRequisicaoParaBuscarLocalidadesTendoUmMunicipio(String municipio) {
		request = get("/api/localidade/municipio/" + municipio)
				.header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf());
	}

	private void quandoEuFizerUmaRequisicaoParaBuscarOMunicipio(String municipio) {
		request = get("/api/municipio/" + municipio)
            .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
			.with(csrf());
	}

	private void quandoEuFizerUmaRequisicaoParaBuscarOTerritorio(String territorio) {
		request = get("/api/territorio/" + territorio)
            .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
			.with(csrf());
	}

	private void quandoEuFizerUmaRequisicaoParaBuscarODistrito(String distrito) {
		request = get("/api/distrito/" + distrito)
            .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
			.with(csrf());
	}

	private void quandoEuFizerUmaRequisicaoParaBuscarOProjeto(String projeto) {
		request = get("/api/projeto/" + projeto)
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf());
	}

	private void quandoEuFizerUmaRequisicaoParaBuscarOMunicipioTendoUmTerritorio (String territorio, String municipio){
		request = get("/api/municipio/" + territorio +"/"+ municipio)
            .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
			.with(csrf());
	}

	private void quandoEuFizerUmaRequisicaoParaBuscarODistritoTendoUmMunicipio (String distrito, String municipio){
		request = get("/api/distrito/" + municipio +"/"+ distrito)
            .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
			.with(csrf());
	}

	private void entaoEuDevoEsperar(ResultMatcher matcher) throws Exception {
		mvc.perform(request).andExpect(matcher);
	}

}
