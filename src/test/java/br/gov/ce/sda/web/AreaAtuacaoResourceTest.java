/*
package br.gov.ce.sda.web;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.transaction.annotation.Transactional;

import br.gov.ce.sda.BaseIntegrationTest;
import br.gov.ce.sda.domain.Municipio;
import br.gov.ce.sda.domain.UsuarioAvancado;
import br.gov.ce.sda.domain.mobile.AreaAtuacao;
import br.gov.ce.sda.repository.AreaAtuacaoRepository;
import br.gov.ce.sda.repository.MunicipioRepository;
import br.gov.ce.sda.repository.UsuarioAvancadoRepository;
import br.gov.ce.sda.web.rest.dto.TecnicoAreaAtuacaoDTO;

public class AreaAtuacaoResourceTest extends BaseIntegrationTest {

	private static final long USUARIO_AVANCADO_ID = 10013477715L;

	private static final String URL = "/api/areasatuacao/";

	@Inject
	private UsuarioAvancadoRepository usuarioAvancadoRepository;

	@Inject
	private MunicipioRepository municipioRepository;

	@Inject
	private AreaAtuacaoRepository areaAtuacaoRepository;

	private MockHttpServletRequestBuilder request;

	private Set<AreaAtuacao> areasAtuacao = new HashSet<>();

	private TecnicoAreaAtuacaoDTO dto;

	@Test
	@Transactional
	public void findAllTest() throws Exception {
		// dado que eu tenho um usuário avançado com 3 áreas de atauação
		dadoQueEuTenhoUmUsuarioAvancadoCom3AreasDeAtuacao();

		// quando eu listar todas as áreas de atuação desse usuário avançado
		quandoEuBuscarTodasAsAreasDeAtuacaoDeUmUsarioAvancadoComId(10013477715L);

		// então eu devo receber uma lista de áreas de atuação de tamanho igual a 3
		entaoEuDevoReceberUmaListaDeAreasDeAtuacaoComTamanho(3);
		areaAtuacaoRepository.delete(areasAtuacao);
		areasAtuacao.clear();
	}

	@Test
	@Transactional
	public void findAllNotFoundTest() throws Exception {
		// dado que eu tenho um usuário avançado sem áreas de atauação
		dadoQueEuTenhoUmUsuarioAvancadoSemAreasDeAtuacao();

		// quando eu listar todas as áreas de atuação desse usuário avançado
		quandoEuBuscarTodasAsAreasDeAtuacaoDeUmUsarioAvancadoComId(10013477768L);

		// então eu devo receber uma lista de áreas de atuação de tamanho igual a 0
		entaoEuDevoEsperar(status().isNotFound());
	}

	@Test
	@Transactional
	public void associarAreasAtuacaoTest() throws Exception {
		dadoQueEuTenhoUmTecnicoCom3MunicipiosParaAssociar();

		quandoEuQuiserAssociarNovasAreasAtuacao();

		entaoEuDevoEsperar(status().isOk());
		deletarAreasAtuacaoDoUsuarioAvancadoComId(USUARIO_AVANCADO_ID);
	}

	@Test
	@Transactional
	public void associarNenhumaAreasAtuacaoTest() throws Exception {
		dadoQueEuTenhoUmTecnicoCom0MunicipiosParaAssociar();

		quandoEuQuiserAssociarNovasAreasAtuacao();

		entaoEuDevoEsperar(status().isOk());
		deletarAreasAtuacaoDoUsuarioAvancadoComId(USUARIO_AVANCADO_ID);
	}

	private void deletarAreasAtuacaoDoUsuarioAvancadoComId(Long id) {
		List<AreaAtuacao> areas = areaAtuacaoRepository.findAllByUsuarioAvancadoId(id);
		areaAtuacaoRepository.delete(areas);
	}

	private void quandoEuQuiserAssociarNovasAreasAtuacao() throws Exception {
		request = post(URL)
				.content(toJson(dto))
				.contentType(contentType)
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
                .with(csrf());
	}

	private void dadoQueEuTenhoUmTecnicoCom0MunicipiosParaAssociar() {
		UsuarioAvancado usuarioAvancado = new UsuarioAvancado();
		usuarioAvancado.setId(USUARIO_AVANCADO_ID);
		dto = TecnicoAreaAtuacaoDTO.builder().usuarioAvancadoId(10013477715L).build();
	}

	private void dadoQueEuTenhoUmTecnicoCom3MunicipiosParaAssociar() {
		UsuarioAvancado usuarioAvancado = new UsuarioAvancado();
		usuarioAvancado.setId(USUARIO_AVANCADO_ID);

		Municipio acarau = municipioRepository.findOne(10000000919L);
		Municipio aquiraz = municipioRepository.findOne(10000000928L);
		Municipio aracati = municipioRepository.findOne(10000000929L);

		List<Municipio> municipios = new ArrayList<>();
		municipios.add(acarau);
		municipios.add(aquiraz);
		municipios.add(aracati);

		dto = TecnicoAreaAtuacaoDTO.builder().usuarioAvancadoId(10013477715L).municipios(municipios).build();
	}

	private void dadoQueEuTenhoUmUsuarioAvancadoSemAreasDeAtuacao() { }

	private void dadoQueEuTenhoUmUsuarioAvancadoCom3AreasDeAtuacao() {
		UsuarioAvancado usuarioAvancado = usuarioAvancadoRepository.findOne(10013477715L);

		Municipio acarau = municipioRepository.findOne(10000000919L);
		Municipio aquiraz = municipioRepository.findOne(10000000928L);
		Municipio aracati = municipioRepository.findOne(10000000929L);

		AreaAtuacao areaAtuacao1 = AreaAtuacao.builder().municipio(acarau).usuarioAvancado(usuarioAvancado).build();
		AreaAtuacao areaAtuacao2 = AreaAtuacao.builder().municipio(aquiraz).usuarioAvancado(usuarioAvancado).build();
		AreaAtuacao areaAtuacao3 = AreaAtuacao.builder().municipio(aracati).usuarioAvancado(usuarioAvancado).build();

		areasAtuacao.add(areaAtuacao1);
		areasAtuacao.add(areaAtuacao2);
		areasAtuacao.add(areaAtuacao3);
		areaAtuacaoRepository.save(areasAtuacao);

		usuarioAvancado.setAreasAtuacao(areasAtuacao);
		usuarioAvancadoRepository.save(usuarioAvancado);
	}

	private void quandoEuBuscarTodasAsAreasDeAtuacaoDeUmUsarioAvancadoComId(Long id) {
		request = get("/api/areasatuacao/")
				.param("usuarioAvancadoId", id.toString())
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
                .with(csrf());
	}

	private void entaoEuDevoReceberUmaListaDeAreasDeAtuacaoComTamanho(Integer size) throws Exception {
		mvc.perform(request).andExpect(jsonPath("$", hasSize(size)));
	}

	private void entaoEuDevoEsperar(ResultMatcher matcher) throws Exception {
		mvc.perform(request).andExpect(matcher);
	}

}
*/
