package br.gov.ce.sda.web;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import br.gov.ce.sda.BaseIntegrationTest;
import br.gov.ce.sda.web.rest.dto.FiltroAgricultorDTO;

public class ElementoResourceTest extends BaseIntegrationTest {

	private MockHttpServletRequestBuilder request;
	private FiltroAgricultorDTO dto;

	@Test
	public void gerElementoPorIdComAgricultorTest() throws Exception{
		//dado que eu tenha o Id de um elemento
		Long elementoId = 10014000992L;

		//quando eu fizer um requisicao para buscar o elemento pelo id com um agricultor
		quandoEuFizerUmaRequisicaoParaBuscarElementoPorIdComAgricultor(elementoId);

		//entao devo receber o status ok
		entaoEuDevoEsperar(status().isOk());

	}

	@Test
 	public void buscarElementosCoordenadasPorFiltroTest() throws Exception{
		// dado que eu tenho um FiltroAgricultorDTO Completo com elemento
		dto = dadoQueEuTenhoUmFiltroAgricultorDtoCompleto();

		//quando eu fizer um requisicao para buscar o elemento coordenadas por filtro
		quandoEuFizerUmaRequisicaoParaBuscarElementosCoordenadasPorFiltro();

		//entao devo receber uma lista com tamanho 1 e status OK
		entaoEuDevoReceberUmTotalDeElementosComTamanho(1, status().isOk());
	}

	@Test
 	public void buscarElementosCoordenadasPorFiltroDeAgricultorSemElementoTest() throws Exception{
		// dado que eu tenho um FiltroAgricultorDTO Completo sem elemento
		dadoQueEuTenhoUmFiltroAgricultorDtoCompletoSemElemento();

		//quando eu fizer um requisicao para buscar o elemento coordenadas por filtro
		quandoEuFizerUmaRequisicaoParaBuscarElementosCoordenadasPorFiltro();

		//entao devo receber o estatus not found
 		entaoEuDevoEsperar(status().isNotFound());
	}

	private void quandoEuFizerUmaRequisicaoParaBuscarElementoPorIdComAgricultor (Long elementoId){
		request = get("/api/elemento/agricultor/"+ elementoId)
            .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
			.with(csrf());
	}

	private void quandoEuFizerUmaRequisicaoParaBuscarElementosCoordenadasPorFiltro() {
 		request = get("/api/elementos/coordenadas/filtro")
 				.params(convertToParameters(dto))
                .header(HttpHeaders.AUTHORIZATION, bearerTokenTecnico)
				.with(csrf())
				.contentType(contentType);
 	}

	private void entaoEuDevoEsperar(ResultMatcher matcher) throws Exception {
		mvc.perform(request).andExpect(matcher);
	}

	private void entaoEuDevoReceberUmTotalDeElementosComTamanho(Integer size, ResultMatcher matcher) throws Exception {
		mvc.perform(request).andExpect(jsonPath("$", hasSize(size))).andExpect(matcher);
	}

	private void dadoQueEuTenhoUmFiltroAgricultorDtoCompletoSemElemento() {
		List<Long> territorios = new ArrayList<Long>();
		territorios.add(10000046262L);

		List<Long> municipio = new ArrayList<Long>();
		municipio.add(10000001137L);

		List<Long> distrito = new ArrayList<Long>();
		distrito.add(10000046398L);

		dto = FiltroAgricultorDTO.builder()
				.nome("ANTONIA")
				.territorio(territorios)
				.municipio(municipio)
				.cpf("00236044338")
				.distrito(distrito)
				.build();
	}


}
