package br.gov.ce.sda.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import javax.inject.Inject;

import org.junit.Test;

import br.gov.ce.sda.BaseIntegrationTest;
import br.gov.ce.sda.domain.PessoaFisica;

public class CustomPessoaRepositoryTest extends BaseIntegrationTest{

	@Inject
	CustomPessoaRepository customPessoaRepository;

	@Test
	public void buscarPessoaPorCpfTest(){

		//dado que eu saiba o cpf de uma pessoa
		String cpf="60390438308";

		//quando eu buscar uma pessoa por cpf
		PessoaFisica pessoaFisica = quandoEuBuscarUmaPessoaPorCPF(cpf);

		//entao eu devo receber uma pessoa com o cpf informado
		entaoEuDevoReceberUmaPessoaComOCPFInformado(pessoaFisica, cpf);
	}

	@Test
	public void buscarPessoaPorCpfInexistenteTest(){

		//dado que eu saiba o cpf de uma pessoa
		String cpf="0";

		//quando eu buscar uma pessoa por cpf
		PessoaFisica pessoaFisica = quandoEuBuscarUmaPessoaPorCPF(cpf);

		//entao eu devo receber uma pessoa nula
		entaoEuDevoReceberUmaPessoaNula(pessoaFisica);
	}

	private PessoaFisica quandoEuBuscarUmaPessoaPorCPF(String cpf){
		return customPessoaRepository.buscarPessoaPorCpf(cpf);
	}

	private void entaoEuDevoReceberUmaPessoaComOCPFInformado(PessoaFisica pessoaFisica, String cpf){
		assertEquals(pessoaFisica.getCpf(), cpf);
	}

	private void entaoEuDevoReceberUmaPessoaNula(PessoaFisica pessoaFisica){
		assertNull(pessoaFisica);
	}
}
