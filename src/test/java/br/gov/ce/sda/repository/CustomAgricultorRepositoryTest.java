package br.gov.ce.sda.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import javax.inject.Inject;

import org.junit.Test;

import br.gov.ce.sda.BaseIntegrationTest;
import br.gov.ce.sda.domain.Agricultor;

public class CustomAgricultorRepositoryTest extends BaseIntegrationTest {
	
	@Inject
	CustomAgricultorRepository customAgricultorRepository;
	
	
	@Test
	public void buscarAgricultorPorCpfTest (){
		
		//dado que eu possua o cpf de um agricultor
		String cpf="11111111111";
		
		//quando eu buscar um agricultor por cpf
		Agricultor agricultor = quandoEuBuscarUmAgricultorPorCpf(cpf);
		
		//entao eu devo receber um agricultor com o mesmo cpf informado
		entaoEuDevoReceberUmAgricultorComOMesmoCPFInformado(agricultor, cpf);
	}
	
	@Test
	public void buscarAgricultorPorCpfInexistenteTest (){
		
		//dado que eu saiba um cpf que nao pertence a nenhum agricultor
		String cpf="0";
		
		//quando eu buscar um agricultor por cpf
		Agricultor agricultor = quandoEuBuscarUmAgricultorPorCpf(cpf);
		
		//entao eu devo receber um agricultor nulo
		entaoEuDevoReceberUmAgricultorNulo(agricultor);
	}
	
	private Agricultor quandoEuBuscarUmAgricultorPorCpf(String cpf){
		
		Agricultor agricultor = customAgricultorRepository.buscarAgricultorPorCpf(cpf);
		return agricultor;
	}
	
	
	private void entaoEuDevoReceberUmAgricultorComOMesmoCPFInformado(Agricultor agricultor, String cpf){
		
		assertEquals(cpf, agricultor.getPessoaFisica().getCpf());
	}
	
	private void entaoEuDevoReceberUmAgricultorNulo (Agricultor agricultor){
		
		assertNull(agricultor);
	}
}
