/*
package br.gov.ce.sda.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.boot.actuate.audit.AuditEventRepository;

import br.gov.ce.sda.BaseIntegrationTest;

public class CustomAuditEventRepositoryTest extends BaseIntegrationTest{

	@Inject
	AuditEventRepository auditEventRepository;

	@Inject
	CustomAuditEventRepository customAuditEventRepository;

	@Inject
	PersistenceAuditEventRepository persistenceAuditEventRepository;

	@Test
	public void addTest (){

		//dado que eu tenha um evento com falha de autorizacao
		AuditEvent event = dadoQueEuTenhaUmEvento("principal", "type", "d=data");

		//Quando eu executar o metodo para adicionar o evento
		quandoEuExecutarOMetodoParaAdicionarOEvento(event);

		//Entao eu devo receber que esse evento foi adicionado
		entaoEuDevoReceberQueEsseEventoFoiAdicionado("principal");
	}

	@Test
	public void addAuthorizationFailureTest (){

		//dado que eu tenha um evento com falha de autorizacao
		AuditEvent event = dadoQueEuTenhaUmEvento("principal", "AUTHORIZATION_FAILURE", "d=data");

		//Quando eu executar o metodo para adicionar o evento
		quandoEuExecutarOMetodoParaAdicionarOEvento(event);

		//Entao eu devo receber que esse evento nao foi adicionado
		entaoEuDevoReceberQueEsseEventoNaoFoiAdicionado("principal");
	}

	@Test
	public void addAnonymousUserTest (){

		//dado que eu tenha um evento com falha de autorizacao
		AuditEvent event = dadoQueEuTenhaUmEvento("anonymousUser", "type", "d=data");

		//Quando eu executar o metodo para adicionar o evento
		quandoEuExecutarOMetodoParaAdicionarOEvento(event);

		//Entao eu devo receber que esse evento nao foi adicionado
		entaoEuDevoReceberQueEsseEventoNaoFoiAdicionado("anonymousUser");
	}

	@Test
	public void findTest(){

		//dado que eu tenha um evento criado apos uma determinada data
		Date date = dadoQueEuTenhaUmEventoCriadoAposUmaDeterminadaData("principalFind", "type", "t=teste");

		//quando eu executar o metodo fidn
		List <AuditEvent> lista = quandoEuExecutarOMetodoFind("principalFind", date);

		//entao eu devo receber uma lista de tamanho 1 e deletar o evento criado
		entaoEuDevoReceberUmaListaDeTamanho1EDeletarOEventoCriado(lista, "principalFind");
	}

	@Test
	public void findPrincipalEDateNullTest(){

		//dado que eu o principal e a date sao nulos

		//quando eu executar o metodo find
		List <AuditEvent> lista = quandoEuExecutarOMetodoFind(null, null);

		//entao eu devo receber uma lista nao vazia
		entaoEuDevoReceberUmaListaNaoVazia(lista);
	}

	private Date dadoQueEuTenhaUmEventoCriadoAposUmaDeterminadaData(String principal, String type, String... data){

		LocalDateTime localDate = LocalDateTime.now();
		Instant instant = localDate.toInstant(ZoneOffset.UTC);
		Date date = Date.from(instant);

		AuditEvent event = dadoQueEuTenhaUmEvento(principal, type, data);

		quandoEuExecutarOMetodoParaAdicionarOEvento(event);

		return date;
	}

	private AuditEvent dadoQueEuTenhaUmEvento(String principal, String type, String... data){
		AuditEvent event = new AuditEvent(principal, type, data);
		return event;
	}

	private void quandoEuExecutarOMetodoParaAdicionarOEvento(AuditEvent event){
		auditEventRepository.add(event);
	}

	private List <AuditEvent> quandoEuExecutarOMetodoFind(String principal, Date date){
		List <AuditEvent> lista = auditEventRepository.find(principal, date);
		return lista;
	}

	private void entaoEuDevoReceberUmaListaNaoVazia(List <AuditEvent> lista){
		assertFalse(lista.isEmpty());
	}

	private void entaoEuDevoReceberUmaListaDeTamanho1EDeletarOEventoCriado(List <AuditEvent> lista, String principal){
		assertEquals(lista.size(), 1);
		persistenceAuditEventRepository.deleteAll(persistenceAuditEventRepository.findByPrincipal(principal));
	}

	private void entaoEuDevoReceberQueEsseEventoNaoFoiAdicionado(String principal){
		assertTrue(persistenceAuditEventRepository.findByPrincipal(principal).isEmpty());
	}

	private void entaoEuDevoReceberQueEsseEventoFoiAdicionado(String principal){
		assertEquals(persistenceAuditEventRepository.findByPrincipal("principal").size(),1);
		persistenceAuditEventRepository.deleteAll(persistenceAuditEventRepository.findByPrincipal(principal));
	}
}
*/
