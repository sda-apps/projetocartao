package br.gov.ce.sda.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;

import br.gov.ce.sda.BaseIntegrationTest;

public class CustomUsuarioAvancadoRepositoryTest extends BaseIntegrationTest {
	
	@Inject
	private CustomUsuarioAvancadoRepository customUsuarioAvancadoRepository;
		
	@Test
	public void buscarTodosOsEmailsTest() throws Exception{
		
		//quando eu buscar todos os emails
		List <String> emails = quandoEuBuscarTodosOsEmails();
		
		//entao eu devo receber uma lista de tamanho 5
		entaoEuDevoReceberUmaListaNaoVazia(emails);
	}
	
	private List<String> quandoEuBuscarTodosOsEmails(){
		
		List <String> emails = customUsuarioAvancadoRepository.buscarTodosEmails();
		return emails;
	}
	
	private void entaoEuDevoReceberUmaListaNaoVazia(List <String> emails){
		
		assertFalse (emails.isEmpty());
		assertEquals(emails.size(), 90);
	}
}