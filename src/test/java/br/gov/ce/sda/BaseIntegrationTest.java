package br.gov.ce.sda;

import br.gov.ce.sda.repository.CustomUsuarioAvancadoRepository;
import br.gov.ce.sda.web.rest.dto.AtendimentoDTO;
import br.gov.ce.sda.web.rest.dto.FiltroAgricultorDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Base64Utils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@Ignore
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
@RunWith(SpringRunner.class)
public class BaseIntegrationTest {

    @Inject
    private WebApplicationContext context;

    @Inject
    private FilterChainProxy springSecurityFilterChain;

    @Inject
    private CustomUsuarioAvancadoRepository usuarioRepository;

    protected MockMvc mvc;

    protected static final MediaType contentType = MediaType.APPLICATION_JSON;

    private ObjectMapper mapper;

    private static final String CLIENT_ID = "projetocartaoweb";

    private static final String CLIENT_SECRET = "D&t$qqY4z2zhqfZg+VRZM6_";

    protected String bearerTokenTecnico;
    protected String bearerTokenTecnicoSemAutoridade;
    protected String bearerTokenSemUsuarioAvancado;


    @Before
    public void setup() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(context).addFilter(springSecurityFilterChain).build();
        mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.registerModule(new Hibernate5Module());
        bearerTokenTecnico = "Bearer " + getAccessToken(mvc, "admin", "admin");
        bearerTokenTecnicoSemAutoridade = "Bearer " + getAccessToken(mvc, "tecnico", "admin");
        bearerTokenSemUsuarioAvancado = "Bearer " + getAccessToken(mvc, "tecnicoSemUA", "admin");
    }

    public String toJson(List<?> list) {
        Objects.requireNonNull(list);
        return new Gson().toJson(list);
    }

    public String toJson(Object src) throws Exception {
        return mapper.writeValueAsString(src);
    }

    public byte[] convertObjectToJsonBytes(Object object) throws Exception {
        return mapper.writeValueAsBytes(object);
    }

    public List<String> convert(List<Long> oldList) {
        List<String> newList = new ArrayList<>();

        if (Objects.nonNull(oldList)) {
            for (Long myInt : oldList) {
                newList.add(String.valueOf(myInt));
            }
        }

        return newList;
    }

    public MultiValueMap<String, String> convertToParameters(FiltroAgricultorDTO dto) {
        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();

        parameters.put("territorio", convert(dto.getTerritorio()));
        parameters.put("municipio", convert(dto.getMunicipio()));
        parameters.put("distrito", convert(dto.getDistrito()));
        parameters.put("projeto", convert(dto.getProjeto()));

        if (Objects.nonNull(dto.getCpf())) {
            parameters.add("cpf", dto.getCpf());
        }

        if (Objects.nonNull(dto.getNome())) {
            parameters.add("nome", dto.getNome());
        }

        if (Objects.nonNull(dto.getStart())) {
            parameters.add("start", dto.getStart().toString());
        }

        if (Objects.nonNull(dto.getLimit())) {
            parameters.add("limit", dto.getLimit().toString());
        }

        if (Objects.nonNull(dto.getOrdenar())) {
            parameters.add("ordenar", dto.getOrdenar().toString());
        }

        if (Objects.nonNull(dto.getOrdem())) {
            parameters.add("ordem", dto.getOrdem());
        }

        return parameters;
    }

    public MultiValueMap<String, String> convertToParameters(AtendimentoDTO dto) {
        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();

        if (Objects.nonNull(dto.getId())) {
            parameters.add("id", dto.getId().toString());
        }

        if (Objects.nonNull(dto.getAno())) {
            parameters.add("ano", dto.getAno().toString());
        }

        if (Objects.nonNull(dto.getAtividade())) {
            parameters.add("atividade", dto.getAtividade());
        }

        if (Objects.nonNull(dto.getMes())) {
            parameters.add("mes", dto.getMes().toString());
        }

        if (Objects.nonNull(dto.getTecnico())) {
            parameters.add("tecnico", dto.getTecnico());
        }

        return parameters;
    }

    public FiltroAgricultorDTO dadoQueEuTenhoUmFiltroAgricultorDtoCompleto() {
        List<Long> territorios = new ArrayList<Long>();
        territorios.add(10000046228L);

        List<Long> municipio = new ArrayList<Long>();
        municipio.add(10000001097L);

        List<Long> distrito = new ArrayList<Long>();
        distrito.add(10000047060L);

        return FiltroAgricultorDTO.builder()
            .nome("Maria")
            .territorio(territorios)
            .municipio(municipio)
            .cpf("11111111111")
            .distrito(distrito)
            .build();
    }

    public String getAccessToken(MockMvc mvc ,String username, String password) throws Exception {
        String authorization = "Basic " + new String(Base64Utils.encode((CLIENT_ID + ":" + CLIENT_SECRET).getBytes()));
        String contentType = MediaType.APPLICATION_JSON + ";charset=UTF-8";

        String content = mvc
            .perform(
                post("/oauth/token")
                    .header("Authorization", authorization)
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .param("username", username)
                    .param("password", password)
                    .param("grant_type", "password")
                    .param("scope", "read write")
                    .param("client_id", CLIENT_ID)
                    .param("client_secret", CLIENT_SECRET))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$.access_token", is(notNullValue())))
            .andExpect(jsonPath("$.token_type", is(equalTo("bearer"))))
            .andExpect(jsonPath("$.refresh_token", is(notNullValue())))
            .andExpect(jsonPath("$.expires_in", is(greaterThan(4000))))
            .andExpect(jsonPath("$.scope", is(equalTo("read write"))))
            .andReturn().getResponse().getContentAsString();

        return content.substring(17, 53);
    }

}
