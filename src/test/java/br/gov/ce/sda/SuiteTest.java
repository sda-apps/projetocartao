package br.gov.ce.sda;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.gov.ce.sda.web.AccountResourceTest;
import br.gov.ce.sda.web.HomeControllerTest;

@RunWith(Suite.class)
@SuiteClasses({
	HomeControllerTest.class,
	AccountResourceTest.class
})
public class SuiteTest { }
