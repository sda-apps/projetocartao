package br.gov.ce.sda.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNull;

import javax.inject.Inject;

import org.junit.Test;

import br.gov.ce.sda.BaseIntegrationTest;
import br.gov.ce.sda.domain.Agricultor;
import br.gov.ce.sda.domain.mapa.AreaGeorreferenciada;
import br.gov.ce.sda.service.AgricultorService;

public class AgricultorServiceTest extends BaseIntegrationTest{
	
	private static final Long AGRICULTOR_ID_SEM_AREA = 10001329122L;
	private static final Long AGRICULTOR_ID_COM_AREA = 10001329151L;
	private static final Long AGRICULTOR_ID_INEXISTENTE = 0L;
	
	@Inject
	private AgricultorService agricultorService;
	
	@Test
	public void getAgricultorComAreasGeograficasTest(){
		//dado que eu tenha um agricultor com areas geograficas
		
		//quando eu buscar  o agricultor com areas geografica
		Agricultor agricultor = quandoEuBuscarOAgricultorComAreasGeograficas(AGRICULTOR_ID_COM_AREA);

		//entao eu devo receber o agricultor com suas areas geograficas e seus poligonos
		entaoEuDevoReceberOAgricultorComSuasAreasGeograficasESeusPoligonos(agricultor);
		
	}
	
	@Test
	public void getAgricultorSemAreasGeograficasTest(){
		//dado que eu tenha um agricultor Sem areas geograficas
		
		//quando eu buscar  o agricultor com areas geografica
		Agricultor agricultor = quandoEuBuscarOAgricultorComAreasGeograficas(AGRICULTOR_ID_SEM_AREA);

		//entao eu devo receber o agricultor com suas areas geograficas e seus poligonos
		entaoEuDevoReceberOAgricultorSemSuasAreasGeograficasESeusPoligonos(agricultor);
	}
	
	@Test
	public void getAreasGeograficasDeAgricultorInexistenteTest(){
		//dado que eu tenha um ID de agricultor invalido
		
		//quando eu buscar  o agricultor com areas geografica
		Agricultor agricultor = quandoEuBuscarOAgricultorComAreasGeograficas(AGRICULTOR_ID_INEXISTENTE);

		//entao eu devo receber nulo
		assertNull(agricultor);
	}
	
	private Agricultor quandoEuBuscarOAgricultorComAreasGeograficas(Long agricultorId){
		
		Agricultor agricultor = agricultorService.getAgricultorComAreasGeograficas(agricultorId);
		
		return agricultor;
	}
	
	private void entaoEuDevoReceberOAgricultorComSuasAreasGeograficasESeusPoligonos(Agricultor agricultor){
		assertNotNull(agricultor);
		assertNotNull(agricultor.getAreasGeorreferenciadas());
		assertFalse(agricultor.getAreasGeorreferenciadas().isEmpty());
		
		for(AreaGeorreferenciada ag:agricultor.getAreasGeorreferenciadas()){
			assertNotNull(ag.getPoligono());
		}
	}
	
	private void entaoEuDevoReceberOAgricultorSemSuasAreasGeograficasESeusPoligonos(Agricultor agricultor){
		assertNotNull(agricultor);
		assertTrue(agricultor.getAreasGeorreferenciadas().isEmpty());
	}
}
