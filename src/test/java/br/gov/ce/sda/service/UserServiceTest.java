package br.gov.ce.sda.service;

import br.gov.ce.sda.BaseIntegrationTest;
import br.gov.ce.sda.domain.UsuarioAvancado;
import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.repository.CustomUsuarioAvancadoRepository;
import br.gov.ce.sda.repository.UserRepository;
import br.gov.ce.sda.service.util.RandomUtil;
import br.gov.ce.sda.web.rest.dto.ManagedUserDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.*;

;

public class UserServiceTest extends BaseIntegrationTest {

    private static final String SENHA = "123789";
    private static final String NOVA_SENHA = "321456";
    private static final String EMAIL_USUARIO_AVANCADO = "projetocartao@gmail.com";
    private static final String EMAIL_INVALIDO = "projetocartao@gmail.co";
    private static final String BASE_URL = "www.projetocartao.gov.ce.br";
    private static final String LOGIN = "userServiceTest";
    private static final Long USUARIO_AVANCADO = 10010073227L;
    private static final Long USUARIO_AVANCADO_SEM_EMAIL = 10010073227L;

    private Usuario user;

    @Inject
    private UserService userService;

    @Inject
    private UserRepository userRepository;

    @Inject
    private PasswordEncoder passwordEncoder;

    @Inject
    private CustomUsuarioAvancadoRepository customUsuarioAvancadoRepository;

    @Before
    public void criarUsuarioInativo() {
        user = userService.createUserInformation(LOGIN, SENHA);
    }

    @After
    public void clean() {
        userService.deleteUserInformation(LOGIN);
    }

    @Test
    public void activateRegistrationTest() {

        //dado que eu possua um usuario inativo

        //quando o usuario receber a chave te ativacao e a chave for ativada
        user = quandoOUsuarioReceberUmaChaveDeAtivacaoEAChaveForAtivada(user);

        //entao eu devo receber true para a verificacao de usuario ativado
        entaoEuDevoReceberUmUsuarioAtivado(user);
    }

    @Test
    public void completePasswordResetTest() {

        //dado que eu tenha uma novaSenha

        //dado que eu tenha chave de reativacao com data valida
        String chave = dadoQueEuTenhaUmUsuarioComChaveDeReativacaoComDataInferiorA24H(user.getId());

        //quando eu executar o metodo de trocar senha por chave
        quandoEuExecutarOMetodoDeTrocarSenhaPorChave(NOVA_SENHA, chave, user.getId());

        //entao eu devo receber que a senha do usuario eh agora a novaSenha
        entaoEuDevoReceberQueASenhaDoUsuarioEhNovaSenha(user.getId(), NOVA_SENHA);
    }

    @Test
    public void completePasswordResetComChaveAntigaTest() {

        //dado que eu tenha uma novaSenha

        //dado que eu tenha chave de reativacao com data invalida
        String chave = dadoQueEuTenhaUmUsuarioComChaveDeReativacaoComDataSuperiorA24H(user.getId());

        //quando eu executar o metodo de trocar senha por chave
        Optional<Usuario> opt = quandoEuExecutarOMetodoDeTrocarSenhaPorChave(NOVA_SENHA, chave, user.getId());

        //entao eu devo receber um opt null
        entaoEuDevoReceberUmOptionalUsuarioNull(opt);
    }

    @Test
    public void completeSignUpTest() {

        //dado que eu tenha chave de Ativacao com data valida desse usuario
        String chave = dadoQueEuTenhaUmUsuarioComChaveDeAtivacaoComDataInferiorA24H(user.getId());

        //quando eu executar o metodo completeSignUp
        Optional<Usuario> opt = quandoEuExecutarOMetodoCompleSignUp(chave, LOGIN, SENHA, BASE_URL);

        user = opt.get();

        //entao eu devo receber um usuario ativado com senha e login correto
        entaoEuDevoReceberUmUsuarioAtivadoComSenhaELoginCorreto(user, SENHA, LOGIN);
    }

    @Test
    public void completeSignUpComChaveAntigaTest() {

        //dado que eu tenha chave de Ativacao com data valida de um usuario
        String chave = dadoQueEuTenhaUmUsuarioComChaveDeReativacaoComDataSuperiorA24H(user.getId());

        //quando eu executar o metodo completeSignUp
        Optional<Usuario> opt = quandoEuExecutarOMetodoCompleSignUp(chave, LOGIN, SENHA, BASE_URL);

        //entao eu devo receber um opt null
        entaoEuDevoReceberUmOptionalUsuarioNull(opt);
    }

    @Test
    @Transactional
    public void createUserInformationTest() {
        //dado que eu tenha um login e uma senha

        //quando eu executar o metodo createUserInformation
        quandoEuExecutarOMetodoCreateUserInformation("LOGIN", "SENHA");

        //entao eu devo receber um usuario inativo
        entaoEuDevoReceberUmUsuarioInativo("LOGIN");

        //quando eu deletar o usuario
        Optional<Usuario> opt = quandoEuDeletarOUsuario("LOGIN");

        //entao eu devo receber um usuario inexistente
        entaoEuDevoReceberUmUsuarioInexistente(opt);
    }

    @Test
    public void requestPasswordResetTest() {
        //dado que o usuario cadastrado esteja ativo e possua um usuario avancado com email
        dadoQueOUsuarioCadastradoEstejaAtivoEPossuaUmUsuarioAvancadoComEmail(user.getId());

        //quando eu executar o metodo requestPasswordReset
        Usuario usuario = quandoEuExecutarOMetodoRequestPasswordReset(EMAIL_USUARIO_AVANCADO, BASE_URL).get();

        //entao eu devo receber um usuario chave de reativacao e data de reativacao valida
        entaoEuDevoReceberUmUsuarioComChaveDeReativacaoEDataDeReativacaoValida(usuario);
    }

    @Test
    public void requestPasswordResetEmailIncorretoTest() {

        //dado que o usuario cadastrado esteja ativo possua um usuario avancado com email
        dadoQueOUsuarioCadastradoEstejaAtivoEPossuaUmUsuarioAvancadoComEmail(user.getId());

        //quando eu executar o metodo requestPasswordReset com um email invalido
        Optional<Usuario> opt = quandoEuExecutarOMetodoRequestPasswordReset(EMAIL_INVALIDO, BASE_URL);

        //entao eu devo receber um usuario inexistente
        entaoEuDevoReceberUmUsuarioInexistente(opt);
    }

    @Test
    public void requestPasswordResetUsuarioInativoTest() {
        //dado que o usuario cadastrado esteja inativo e possua um usuario avancado com email
        dadoQueOUsuarioCadastradoEstejaInativoEPossuaUmUsuarioAvancadoComEmail(user.getId());

        //quando eu executar o metodo requestPasswordReset
        Optional<Usuario> opt = quandoEuExecutarOMetodoRequestPasswordReset("projetocartao@gmail.c", BASE_URL);

        //entao o usuario deve receber um Usuario inativo
        entaoEuDevoReceberUmUsuarioInativo(opt);
    }

    @Test
    public void requestPasswordResetSemEmailTest() {

        //dado que o usuario cadastrado esteja ativo e mas nao possua um usuario avancado com email
        dadoQueOUsuarioCadastradoEstejaAtivoMasNaoPossuaUmUsuarioAvancadoComEmail(user.getId());

        //quando eu executar o metodo requestPasswordReset
        Optional<Usuario> opt = quandoEuExecutarOMetodoRequestPasswordReset("", BASE_URL);

        //entao eu devo receber um usuario inexistente
        entaoEuDevoReceberUmUsuarioInexistente(opt);
    }

    @Test
    @Transactional
    public void criarUsuarioInativoTest() {

        //dado que eu tenha um usuarioAvancado
        UsuarioAvancado usuarioAvancado = dadoQueEuTenhaUmUsuarioAvancado(USUARIO_AVANCADO);

        //quando eu executar o metodo criarUsuarioInativo
        Usuario usuario = quandoEuExecutarOMetodocriarUsuarioInativo(usuarioAvancado);

        //entao eu devo receber um usuario inativo com login, senha e chave de ativacao com data valida
        entaoEuDevoReceberUmUsuarioInativoComLoginComSenhaEComChaveDeAtivacaoValida(usuario);

        //quando eu deletar o usuario
        Optional<Usuario> opt = quandoEuDeletarOUsuario(usuario.getLogin());

        //entao eu devo receber um usuario inexistente
        entaoEuDevoReceberUmUsuarioInexistente(opt);
    }

    @Test
    @Transactional
    public void createUserTest() {
        //dado que eu tenha um ManagedUserDTO com login e authorities
        ManagedUserDTO managedUserDTO = dadoQueEuTenhaUmManagedUserDTOComLoginEAuthority("LOGIN", "ROLE_TECNICO");

        //quando eu executar o metodo createUser
        Usuario usuario = quandoEuExecutarOMetodoCreateUser(managedUserDTO);

        //entao eu devo receber um usuario ativo
        entaoEuDevoReceberUmUsuarioAtivado(usuario);

        //quando eu deletar o usuario
        Optional<Usuario> opt = quandoEuDeletarOUsuario("LOGIN");

        //entao eu devo receber um usuario inexistente
        entaoEuDevoReceberUmUsuarioInexistente(opt);
    }

    @Test
    public void createUserSemAuthoritiesTest() {

        //dado que eu tenha um ManagedUserDTO com login sem authorities
        ManagedUserDTO managedUserDTO = dadoQueEuTenhaUmManagedUserDTOComLoginSemAuthority("LOGIN");

        //quando eu executar o metodo createUser
        Usuario usuario = quandoEuExecutarOMetodoCreateUser(managedUserDTO);

        //entao eu devo receber um usuario ativo
        entaoEuDevoReceberUmUsuarioAtivadoSemAuthorities(usuario);

        //quando eu deletar o usuario
        Optional<Usuario> opt = quandoEuDeletarOUsuario("LOGIN");

        //entao eu devo receber um usuario inexistente
        entaoEuDevoReceberUmUsuarioInexistente(opt);
    }

    @Test
    public void deleteUserInformationTest() {

        //dado que eu possua um usuario

        //quando eu deletar o usuario
        Optional<Usuario> opt = quandoEuDeletarOUsuario(user.getLogin());

        //entao eu devo receber um usuario inexistente
        entaoEuDevoReceberUmUsuarioInexistente(opt);
    }

    @Test
    public void deleteUserInformationUsuarioNaoExistenteTest() {

        // dado que eu saiba um login inexistente
        String login = "LOGIN";

        //quando eu deletar o usuario
        Optional<Usuario> opt = quandoEuDeletarOUsuario(login);

        //entao eu devo receber um usuario inexistente
        entaoEuDevoReceberUmUsuarioInexistente(opt);
    }


    private String dadoQueEuTenhaUmUsuarioComChaveDeReativacaoComDataInferiorA24H(Long id) {

        //dado que eu tenha um usuario
        Optional<Usuario> opt = userRepository.findOneById(id);
        Usuario usuario = opt.get();

        //dado que eu tenha uma chave de reativacao para o usuario
        String chave = RandomUtil.generateResetKey();

        //dado que a chave tenha uma data de geracao de menos de 24h
        LocalDateTime data = LocalDateTime.now();

        //quando o usuario receber a chave de reativacao e a data da chave de reativacao
        usuario.setChaveReativacao(chave);
        usuario.setResetDate(data);
        userRepository.save(usuario);
        opt = userRepository.findOneById(id);
        usuario = opt.get();

        //entao eu devo receber que o usuario possui uma chave
        assertNotNull(usuario.getChaveReativacao());

        //entao eu devo receber que essa chave eh unica
        assertEquals(usuario.getChaveReativacao(), chave);

        return chave;
    }

    private String dadoQueEuTenhaUmUsuarioComChaveDeReativacaoComDataSuperiorA24H(Long id) {

        //dado que eu tenha um usuario
        Optional<Usuario> opt = userRepository.findOneById(id);
        Usuario usuario = opt.get();

        //dado que eu tenha uma chave de reativacao para o usuario
        String chave = RandomUtil.generateResetKey();

        //dado que a chave tenha uma data de geracao maior do que 24h
        LocalDateTime data = LocalDateTime.now().minusHours(26);

        //quando o usuario receber a chave de reativacao e a data da chave de reativacao
        usuario.setChaveReativacao(chave);
        usuario.setResetDate(data);
        userRepository.save(usuario);
        opt = userRepository.findOneById(id);
        usuario = opt.get();

        //entao eu devo receber que o usuario possui uma chave
        assertNotNull(usuario.getChaveReativacao());

        //entao eu devo receber que essa chave eh unica
        assertEquals(usuario.getChaveReativacao(), chave);

        return chave;
    }

    private String dadoQueEuTenhaUmUsuarioComChaveDeAtivacaoComDataInferiorA24H(Long id) {

        //dado que eu tenha um usuario
        Optional<Usuario> opt = userRepository.findOneById(id);
        Usuario usuario = opt.get();

        //dado que eu tenha uma chave de reativacao para o usuario
        String chave = RandomUtil.generateActivationKey();

        //dado que a chave tenha uma data de geracao de menos de 24h
        LocalDateTime data = LocalDateTime.now();

        //quando o usuario receber a chave de reativacao e a data da chave de reativacao
        usuario.setChaveAtivacao(chave);
        usuario.setResetDate(data);
        userRepository.save(usuario);
        opt = userRepository.findOneById(id);
        usuario = opt.get();

        //entao eu devo receber que o usuario possui uma chave
        assertNotNull(usuario.getChaveAtivacao());

        //entao eu devo receber que essa chave eh unica
        assertEquals(usuario.getChaveAtivacao(), chave);

        return chave;
    }

    private void dadoQueEuTenhaUmUsuarioInativoComDataMaiorDoQue3dias(String login, String senha) {
        Usuario usuario = userService.createUserInformation(login, senha);
        LocalDateTime data = LocalDateTime.now().minusDays(3).minusMinutes(1);
        usuario.setCreatedDate(data);
        userRepository.save(usuario);
    }

    private ManagedUserDTO dadoQueEuTenhaUmManagedUserDTOComLoginEAuthority(String login, String authority) {

        Set<String> authorities = new HashSet<String>();
        authorities.add(authority);

        ManagedUserDTO managedUserDTO = new ManagedUserDTO(login, authorities);
        return managedUserDTO;
    }

    private ManagedUserDTO dadoQueEuTenhaUmManagedUserDTOComLoginSemAuthority(String login) {

        Set<String> authorities = new HashSet<String>();

        ManagedUserDTO managedUserDTO = new ManagedUserDTO(login, authorities);
        return managedUserDTO;
    }

    private void dadoQueOUsuarioCadastradoEstejaAtivoEPossuaUmUsuarioAvancadoComEmail(Long id) {
        Usuario usuario = userRepository.findOneById(id).get();
        UsuarioAvancado usuarioAvancado = customUsuarioAvancadoRepository.buscarPorId(USUARIO_AVANCADO);
        usuario.setUsuarioAvancado(usuarioAvancado);
        usuario.setStatus(Usuario.Status.ATIVADO);
        userRepository.saveAndFlush(usuario);
    }

    private void dadoQueOUsuarioCadastradoEstejaInativoEPossuaUmUsuarioAvancadoComEmail(Long id) {
        Optional<Usuario> opt = userRepository.findOneById(id);
        Usuario usuario = opt.get();
        UsuarioAvancado usuarioAvancado = customUsuarioAvancadoRepository.buscarPorId(10013478039L);
        usuario.setUsuarioAvancado(usuarioAvancado);
        userRepository.save(usuario);
    }

    private void dadoQueOUsuarioCadastradoEstejaAtivoMasNaoPossuaUmUsuarioAvancadoComEmail(Long id) {

        Optional<Usuario> opt = userRepository.findOneById(id);
        Usuario usuario = opt.get();
        usuario.setStatus(Usuario.Status.ATIVADO);
        UsuarioAvancado usuarioAvancado = customUsuarioAvancadoRepository.buscarPorId(USUARIO_AVANCADO_SEM_EMAIL);
        usuario.setUsuarioAvancado(usuarioAvancado);
        userRepository.save(usuario);
    }

    private UsuarioAvancado dadoQueEuTenhaUmUsuarioAvancado(Long id) {
        return customUsuarioAvancadoRepository.buscarPorId(id);
    }

    private Usuario quandoOUsuarioReceberUmaChaveDeAtivacaoEAChaveForAtivada(Usuario usuario) {

        //dado que eu tenha o ID do usuario
        Long id = usuario.getId();

        //dado que eu tenha uma chave de ativacao para o usuario
        String chave = RandomUtil.generateActivationKey();

        //quando o usuario receber a chave de ativacao
        usuario.setChaveAtivacao(chave);
        userRepository.save(usuario);

        Optional<Usuario> opt = userRepository.findOneById(id);
        usuario = opt.get();

        //entao eu devo receber que essa chave eh unica
        assertEquals(usuario.getChaveAtivacao(), chave);

        //entao eu devo receber que o usuario possui uma chave
        assertNotNull(usuario.getChaveAtivacao());

        //quando eu executar o metodo de ativacao por chave
        opt = userService.activateRegistration(chave);
        opt = userRepository.findOneById(id);
        usuario = opt.get();

        return usuario;

    }

    private void quandoEuExecutarOMetodoCreateUserInformation(String login, String senha) {
        userService.createUserInformation(login, senha);
    }

    private Optional<Usuario> quandoEuExecutarOMetodoDeTrocarSenhaPorChave(String novaSenha, String chave, Long id) {

        Optional<Usuario> opt = userService.completePasswordReset(novaSenha, chave);

        return opt;
    }

    private Optional<Usuario> quandoEuExecutarOMetodoCompleSignUp(String chave, String login, String senha, String baseUrl) {
        Optional<Usuario> opt = userService.completeSignUp(chave, login, senha, baseUrl);
        return opt;
    }


    private Optional<Usuario> quandoEuDeletarOUsuario(String login) {
        userService.deleteUserInformation(login);
        Optional<Usuario> opt = userRepository.findOneByLogin(login);
        return opt;

    }

    private Optional<Usuario> quandoEuExecutarOMetodoRequestPasswordReset(String email, String baseUrl) {
        Optional<Usuario> opt = userService.requestPasswordReset(email, baseUrl);
        return opt;
    }

    private Usuario quandoEuExecutarOMetodocriarUsuarioInativo(UsuarioAvancado usuarioAvancado) {
        Usuario usuario = userService.criarUsuarioInativo(usuarioAvancado);
        return usuario;
    }

    private Usuario quandoEuExecutarOMetodoCreateUser(ManagedUserDTO managedUserDTO) {
        Usuario usuario = userService.createUser(managedUserDTO);
        return usuario;

    }

    private void entaoEuDevoReceberUmUsuarioInexistente(Optional<Usuario> opt) {
        assertFalse(opt.isPresent());
    }

    private void entaoEuDevoReceberQueASenhaDoUsuarioEhNovaSenha(Long id, String novaSenha) {
        Optional<Usuario> opt = userRepository.findOneById(id);
        Usuario usuario = opt.get();
        assertTrue(passwordEncoder.matches(novaSenha, usuario.getSenha()));
    }

    private void entaoEuDevoReceberUmOptionalUsuarioNull(Optional<Usuario> opt) {
        assertFalse(opt.isPresent());
    }

    private void entaoEuDevoReceberUmUsuarioAtivado(Usuario usuario) {
        assertTrue(usuario.isEnabled());

    }

    private void entaoEuDevoReceberUmUsuarioInativo(Optional<Usuario> opt) {
        Usuario usuario = opt.get();
        assertFalse(usuario.isEnabled());
    }

    private void entaoEuDevoReceberUmUsuarioAtivadoComSenhaELoginCorreto(Usuario usuario, String senha, String login) {
        assertTrue(usuario.isEnabled());
        Long id = usuario.getId();
        entaoEuDevoReceberQueASenhaDoUsuarioEhNovaSenha(id, senha);
        assertEquals(login, usuario.getLogin());
    }

    private void entaoEuDevoReceberUmUsuarioInativo(String login) {
        Optional<Usuario> opt = userRepository.findOneByLogin(login);
        assertNotNull(opt);
        Usuario usuario = opt.get();
        assertFalse(usuario.isEnabled());
    }

    private void entaoEuDevoReceberUmUsuarioComChaveDeReativacaoEDataDeReativacaoValida(Usuario usuario) {
        //entao o usuario deve receber uma chave de reativacao com data
        assertNotNull("O resetDate não deveria ser nulo.", usuario.getResetDate());
        assertNotNull("A chave de reativação não deveria ser nula.", usuario.getChaveReativacao());
        assertTrue("A chave de reativação deveria set menos de 24h.", usuario.getResetDate().isAfter(LocalDateTime.now().minusHours(24)));
    }

    private void entaoEuDevoReceberUmUsuarioInativoComLoginComSenhaEComChaveDeAtivacaoValida(Usuario usuario) {
        assertFalse(usuario.isEnabled());
        assertNotNull(usuario.getChaveAtivacao());
        assertNotNull(usuario.getLogin());
        assertNotNull(usuario.getSenha());
        assertTrue(usuario.getResetDate().isAfter(LocalDateTime.now().minusHours(24)));
    }

    private void entaoEuDevoReceberUmUsuarioAtivadoSemAuthorities(Usuario usuario) {
        assertTrue(usuario.isEnabled());
        assertTrue(usuario.getAutoridades().isEmpty());
    }
}
