package br.gov.ce.sda.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.junit.Test;
import org.springframework.boot.actuate.audit.AuditEvent;

import br.gov.ce.sda.BaseIntegrationTest;

public class AuditEventServiceTest extends BaseIntegrationTest{
	
	@Inject
	private AuditEventService auditEventService;
	
	@Test
	public void findTest() throws Exception {
		// Dado que eu tenho o ID e o PRINCIPAL de uma persistent_audit_event
		final Long AUDIT_EVENT_ID = 1012L;
		final String AUDIT_EVENT_PRINCIPAL = "admin";
		
		// quando eu fizer a busca por id
		AuditEvent auditEvent = auditEventService.find(AUDIT_EVENT_ID).get();
				
		// devo receber um AuditEvent com principal igual a admin
		assertEquals(AUDIT_EVENT_PRINCIPAL, auditEvent.getPrincipal());
	}
	
	@Test
	public void findTestIdInvalido() throws Exception {
		// Dado que eu tenho o ID de uma persistent_audit_event
		final Long AUDIT_EVENT_ID = 0L;
		
		// quando eu fizer a busca por id
		Optional<AuditEvent> opt = auditEventService.find(AUDIT_EVENT_ID);
		
		// devo receber um FALSE ao testar se o Optional possui valor
		assertFalse(opt.isPresent());
	}
	
	@Test
	public void findAllTest() throws Exception{
		// quando eu fizer busca por todas as linhas de persistent_audit_event
		List<AuditEvent> listAudit = auditEventService.findAll();
		
		// devo obter um false ao testar se a lista esta vazia
		assertFalse(listAudit.isEmpty());
	}
	
	@Test
	public void findByDatesTest() throws Exception{
		// dado que eu tenho duas datas
		LocalDateTime toDate = LocalDateTime.of(2016, 8, 25, 0, 0);
		LocalDateTime fromDate = toDate.minusMonths(1);
		
		// quando eu fizer uma buscar por todas as linhas no intervalo das datas
		List<AuditEvent> listAudit = auditEventService.findByDates(fromDate, toDate);
		
		// devo obter um false ao testar se a lista esta não vazia
		assertFalse(listAudit.isEmpty());
	}
	
	@Test
	public void findByDatesIntervaloInvalidoTest() throws Exception{
		// dado que eu tenho duas datas em um intervalo invalido
		LocalDateTime toDate = LocalDateTime.now();
		LocalDateTime fromDate = toDate.plusYears(2);
		
		// quando eu fizer uma buscar por todas as linhas no intervalo das datas
		List<AuditEvent> listAudit = auditEventService.findByDates(fromDate, toDate);
		
		// devo obter um true ao testar se a lista esta vazia
		assertTrue(listAudit.isEmpty());
	}
}
