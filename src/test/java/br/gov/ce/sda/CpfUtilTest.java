package br.gov.ce.sda;

import static org.junit.Assert.*;

import org.junit.Test;

import br.gov.ce.sda.service.util.CpfUtil;

public class CpfUtilTest {

	@Test
	public void deveFormatarCpfComOnzeDigitos() {
		String cpfFormatado = CpfUtil.formatarCpf("123456");
		
		assertEquals("00000123456", cpfFormatado);
	}
	
	@Test
	public void deveConverterCpfParaLong() {
		String cpfFormatado = CpfUtil.converterCpfParaLong("000.001.234-56");
		
		assertEquals("123456", cpfFormatado);
	}
	
	@Test
	public void deveConverterCpfParaLong2() {
		String cpfFormatado = CpfUtil.converterCpfParaLong("100.001.234-56");
		
		assertEquals("10000123456", cpfFormatado);
	}
	
}
