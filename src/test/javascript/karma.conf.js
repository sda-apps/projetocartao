// Karma configuration
// http://karma-runner.github.io/0.10/config/configuration-file.html

module.exports = function (config) {
    config.set({
        // base path, that will be used to resolve files and exclude
        basePath: '../../',

        // testing framework to use (jasmine/mocha/qunit/...)
        frameworks: ['jasmine'],

        // list of files / patterns to load in the browser
        files: [
            // bower:js
            'main/webapp/lib/jquery/dist/jquery.js',
            'main/webapp/lib/angular/angular.js',
            'main/webapp/lib/angular-aria/angular-aria.js',
            'main/webapp/lib/angular-messages/angular-messages.js',
            'main/webapp/lib/angular-animate/angular-animate.js',
            'main/webapp/lib/angular-cookies/angular-cookies.js',
            'main/webapp/lib/angular-resource/angular-resource.js',
            'main/webapp/lib/angular-sanitize/angular-sanitize.js',
            'main/webapp/lib/angular-material/angular-material.js',
            'main/webapp/lib/angular-bootstrap/ui-bootstrap-tpls.js',
            'main/webapp/lib/angular-cache-buster/angular-cache-buster.js',
            'main/webapp/lib/angular-local-storage/dist/angular-local-storage.js',
            'main/webapp/lib/angular-loading-bar/build/loading-bar.js',
            'main/webapp/lib/angular-ui-router/release/angular-ui-router.js',
            'main/webapp/lib/bootstrap/dist/js/bootstrap.js',
            'main/webapp/lib/json3/lib/json3.js',
            'main/webapp/lib/ng-file-upload/ng-file-upload.js',
            'main/webapp/lib/ngInfiniteScroll/build/ng-infinite-scroll.js',
            'main/webapp/lib/ng-file-upload-shim/ng-file-upload-shim.js',
            'main/webapp/lib/angucomplete-alt/angucomplete-alt.js',
            'main/webapp/lib/leaflet/dist/leaflet-src.js',
            'main/webapp/lib/angular-leaflet-directive/dist/angular-leaflet-directive.js',
            'main/webapp/lib/leaflet-plugins/control/Distance.js',
            'main/webapp/lib/leaflet-plugins/control/Layers.Load.js',
            'main/webapp/lib/leaflet-plugins/control/Permalink.js',
            'main/webapp/lib/leaflet-plugins/control/Permalink.Layer.js',
            'main/webapp/lib/leaflet-plugins/control/Permalink.Line.js',
            'main/webapp/lib/leaflet-plugins/control/Permalink.Marker.js',
            'main/webapp/lib/leaflet-plugins/control/Permalink.Overlay.js',
            'main/webapp/lib/leaflet-plugins/layer/Icon.Canvas.js',
            'main/webapp/lib/leaflet-plugins/layer/Layer.Deferred.js',
            'main/webapp/lib/leaflet-plugins/layer/Marker.Rotate.js',
            'main/webapp/lib/leaflet-plugins/layer/Marker.Text.js',
            'main/webapp/lib/leaflet-plugins/layer/OpenStreetBugs.js',
            'main/webapp/lib/leaflet-plugins/layer/vector/GPX.js',
            'main/webapp/lib/leaflet-plugins/layer/vector/GPX.Speed.js',
            'main/webapp/lib/leaflet-plugins/layer/vector/KML.js',
            'main/webapp/lib/leaflet-plugins/layer/vector/OSM.js',
            'main/webapp/lib/leaflet-plugins/layer/vector/TOPOJSON.js',
            'main/webapp/lib/leaflet-plugins/layer/tile/Bing.js',
            'main/webapp/lib/leaflet-plugins/layer/tile/Google.js',
            'main/webapp/lib/leaflet-plugins/layer/tile/Yandex.js',
            'main/webapp/lib/ng-img-crop/compile/minified/ng-img-crop.js',
            'main/webapp/lib/angular-mocks/angular-mocks.js',
            // endbower
            'main/webapp/scripts/app/app.js',
            'main/webapp/scripts/app/**/*.js',
            'main/webapp/scripts/components/**/*.+(js|html)',
            'test/javascript/spec/helpers/module.js',
            'test/javascript/spec/helpers/httpBackend.js',
            'test/javascript/**/!(karma.conf).js'
        ],


        // list of files / patterns to exclude
        exclude: [],

        preprocessors: {
            './**/*.js': ['coverage']
        },

        reporters: ['dots', 'jenkins', 'coverage', 'progress'],

        jenkinsReporter: {
            
            outputFile: '../target/test-results/karma/TESTS-results.xml'
        },

        coverageReporter: {
            
            dir: '../target/test-results/coverage',
            reporters: [
                {type: 'lcov', subdir: 'report-lcov'}
            ]
        },

        // web server port
        port: 9876,

        // level of logging
        // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
        logLevel: config.LOG_INFO,

        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: false,

        // Start these browsers, currently available:
        // - Chrome
        // - ChromeCanary
        // - Firefox
        // - Opera
        // - Safari (only Mac)
        // - PhantomJS
        // - IE (only Windows)
        browsers: ['PhantomJS'],

        // Continuous Integration mode
        // if true, it capture browsers, run tests and exit
        singleRun: false,

        // to avoid DISCONNECTED messages when connecting to slow virtual machines
        browserDisconnectTimeout : 10000, // default 2000
        browserDisconnectTolerance : 1, // default 0
        browserNoActivityTimeout : 4*60*1000 //default 10000
    });
};
