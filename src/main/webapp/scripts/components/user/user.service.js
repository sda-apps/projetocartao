'use strict';

angular.module('projetocartaoApp')
    .factory('User', function ($resource) {
        return $resource('api/users/:id', {id: '@id'}, {
                'query': {method: 'GET', isArray: true},
                'get': {
                    method: 'GET'
                },
                'save': { method:'POST' },
                'update': { method:'PUT' },
                'delete':{ method:'DELETE'}
        });
    })
    .factory('UserPorCpf', function ($resource) {
        return $resource('api/users/requestaccess', {}, {
            'get': {method: 'GET'}
        })
    })
    .factory('UserPublic', function ($resource) {
        return $resource('api/users/public', {}, {
            'save': {method: 'POST'}
        });
    })
    .factory('UserCompany', function ($resource) {
        return $resource('api/users/company/', {}, {
            'query': {method: 'GET', isArray: true},
        });
    });
