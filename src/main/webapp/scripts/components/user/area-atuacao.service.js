'use strict';

angular.module('projetocartaoApp')
    .factory('AreaAtuacaoPorId', function ($resource) {
        return $resource('api/areasatuacao', {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('SalvarAreaAtuacao', function ($resource) {
        return $resource('api/areasatuacao/', {}, {
            'post': {
                method: 'POST'
            }
        });
    });
