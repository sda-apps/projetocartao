'use strict';

angular.module('projetocartaoApp')
    .factory('MultipartFile', function ($resource) {
        return $resource('api/files', {}, {
            'get': {
                method: 'GET',
                params: {
                    bucketName: '@bucketName',
                    fileName: '@fileName'
                },
                headers: {
                    accept: 'multipart/form-data'
                },
                responseType: 'arraybuffer',
                transformResponse: function (data) {
                    return {
                        response: new Blob([data], {type: 'multipart/form-data'})
                    };
                }
            }
        });
    })
    .factory('MultipartFileListURL', function ($resource) {
        return $resource('api/files', {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    });
