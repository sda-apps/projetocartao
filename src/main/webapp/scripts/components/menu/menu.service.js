'use strict';

angular.module('projetocartaoApp')
    .factory('menuService', function ($rootScope, rolesAccess) {
        return {
            menuTravadoAberto: true,
            menus: [
                {
                    name: 'Início',
                    icon: 'ic_home',
                    state: 'main-agricultor'
                },
                {
                    name: 'Dados Pessoais',
                    icon: 'ic_account_box',
                    state: 'dados-agricultor'
                },
                {
                    name: 'DAP',
                    icon: 'ic_folder',
                    state: 'daps-agricultor'
                },
                {
                    name: 'Programas e Projetos',
                    icon: 'ic_description',
                    submenus: [{
                        name: 'Meus Programas e Projetos',
                        state: 'programas-agricultor'
                    },
                        {
                            name: 'Projeto Hora de Plantar',
                            state: 'hora-plantar'
                        },
                        {
                            name: 'PAA Alimentos',
                            state: 'paa-alimentos'
                        },
                        {
                            name: 'PAA Leite',
                            state: 'paa-leite'
                        },
                        {
                            name: 'Cisternas',
                            state: 'cisternas'
                        },
                        {
                            name: 'Segunda Água',
                            state: 'segunda-agua'
                        },
                        {
                            name: 'PNAE',
                            state: 'pnae'
                        },
                        {
                            name: 'Garantia Safra',
                            state: 'garantia-safra'
                        },
                        {
                            name: 'Caju',
                            state: 'caju'
                        },
                        {
                            name: 'Biodiesel',
                            state: 'biodiesel'
                        }]

                },
                {
                    name: 'ATER',
                    icon: 'ic_supervisor',
                    submenus: [{
                        name: 'Técnicos',
                        state: 'ater-tecnicos'
                    },
                        {
                            name: 'Atendimentos',
                            state: 'ater-atendimentos'
                        }
                    ]
                },
                {
                    name: 'Serviços',
                    icon: 'ic_build',
                    state: 'servicos'
                },
                {
                    name: 'Ajuda',
                    icon: 'ic_help',
                    func: function () {
                        $rootScope.introOnStart();
                    },
                    divisorBefore: true
                },
                {
                    name: 'Feedback',
                    icon: 'ic_announcement',
                    func: function () {
                        $rootScope.mostrarFeedback = !$rootScope.mostrarFeedback;
                    },
                    divisorAfter: true
                }
            ],

            menusUA: [
                {
                    name: 'Busca',
                    icon: 'ic_search',
                    state: 'main-usuario-avancado',
                    roles: rolesAccess['MAIN_USUARIO_AVANCADO'].buscar
                },
                {
                    name: 'ATER',
                    icon: 'ic_ater',
                    roles: rolesAccess['MENU_ATER'].abrir,
                    submenus: [{
                        name: 'Planejamento',
                        state: 'listagem-ptas',
                        roles: rolesAccess['PTA'].buscar
                    }, {
                        name: 'Cadastros de Agricultores',
                        state: 'atividade-cadastro',
                        roles: rolesAccess['ATIVIDADE_CADASTRO'].buscar
                    }, {
                        name: 'Registro de Atividades de ATER',
                        state: 'listagem-avaliacao-tarefa',
                        roles: rolesAccess['AVALIACAO_TAREFA'].buscar
                    }
                    ]
                },
                {
                    name: 'Administração',
                    icon: 'ic_supervisor',
                    roles: rolesAccess['MENU_ADMINISTRACAO_ATER'].abrir,
                    submenus: [{
                        name: 'Ação',
                        state: 'listagem-acoes',
                        roles: rolesAccess['ACAO'].buscar
                    },
                        {
                            name: 'Atividade',
                            state: 'listagem-atividades',
                            roles: rolesAccess['ATIVIDADES'].buscar
                        },
                        {
                            name: 'Comunidade',
                            state: 'listagem-comunidades',
                            roles: rolesAccess['COMUNIDADE'].buscar
                        },
                        {
                            name: 'Eixo Temático',
                            state: 'listagem-eixos',
                            roles: rolesAccess['EIXO_TEMATICO'].buscar
                        },
                        {
                            name: 'Entidade e Contrato',
                            state: 'listagem-entidades',
                            roles: rolesAccess['ENTIDADE'].buscar
                        },
                        {
                            name: 'Localidade',
                            state: 'listagem-localidades',
                            roles: rolesAccess['LOCALIDADE'].buscar
                        },
                        {
                            name: 'Tema',
                            state: 'listagem-temas',
                            roles: rolesAccess['TEMA'].buscar
                        },
                        {
                            name: 'Usuário',
                            state: 'user-company',
                            roles: rolesAccess['USER_COMPANY'].buscar
                        }]

                },
                {
                    name: 'Relatórios',
                    icon: 'ic_report',
                    roles: rolesAccess['MENU_RELATORIO'].abrir,
                    submenus: [/*{
                        name: 'Cadastro Resumido',
                        state: 'relatorio-sintetico-resumido',
                        roles: rolesAccess['RELATORIO_CADASTRO_RESUMIDO'].visualizar
                    }, {
                        name: 'Cadastro de Famílias - Sintético',
                        state: 'relatorio-cadastro-agricultores',
                        roles: rolesAccess['RELATORIO_CADASTRO_SINTETICO'].visualizar
                    }, */{
                        name: 'Cadastro de Famílias - Analítico',
                        state: 'relatorio-analitico-familias',
                        roles: rolesAccess['RELATORIO_CADASTRO_ANALITICO'].visualizar
                    }, /*{
                        name: 'Perfil das Famílias',
                        state: 'relatorio-perfil-familias',
                        roles: rolesAccess['RELATORIO_PERFIL_FAMILIAS'].visualizar
                    }, */{
                        name: 'Entidades Parceiras - Sintético',
                        state: 'relatorio-sintetico-atcs',
                        roles: rolesAccess['RELATORIO_PARCEIRAS_SINTETICO'].visualizar
                    }, {
                        name: 'Entidades Parceiras - Analítico',
                        state: 'relatorio-analitico-entidades',
                        roles: rolesAccess['RELATORIO_PARCEIRAS_ANALITICO'].visualizar
                    }, {
                        name: 'Trimestral de Atividades',
                        state: 'relatorio-trimestral-atividades',
                        roles: rolesAccess['RELATORIO_TRIMESTRAL_SIMPLIFICADO'].visualizar
                    }, /*{
                        name: 'Atestes - Sintetico Geral',
                        state: 'relatorio-bi-atestes-sintetico-geral',
                        roles: rolesAccess['RELATORIO_BI'].visualizar
                    }, *//*{
                        name: 'Atestes - Sintético por Atividades',
                        state: 'relatorio-bi-atestes-sintetico-atividades',
                        roles: rolesAccess['RELATORIO_BI'].visualizar
                    }, *//*{
                        name: 'Atestes - Analítico',
                        state: 'relatorio-bi-atestes-analitico',
                        roles: rolesAccess['RELATORIO_BI'].visualizar
                    }, */{
                        name: 'Atestes',
                        state: 'relatorio-bi-atestes',
                        roles: rolesAccess['RELATORIO_BI'].visualizar
                    }, {
                        name: 'Planos de Investimento',
                        state: 'relatorio-bi-planos-investimentos',
                        roles: rolesAccess['RELATORIO_BI'].visualizar
                    }, {
                        name: 'Cadastros de Famílias',
                        state: 'relatorio-bi-cadastro-familia',
                        roles: rolesAccess['RELATORIO_BI'].visualizar
                    },  {
                        name: 'Planos de Investimento',
                        state: 'relatorio-bi-investimento',
                        roles: rolesAccess['RELATORIO_BI'].visualizar
                    }, {
                        name: 'Metas',
                        state: 'relatorio-bi-metas',
                        roles: rolesAccess['RELATORIO_BI'].visualizar
                    }, {
                        name: 'Pesquisa PPF',
                        state: 'relatorio-bi-pesquisa-ppf',
                        roles: rolesAccess['RELATORIO_BI'].visualizar
                    }, {
                        name: 'Contatos Telefônicos',
                        state: 'relatorio-bi-contatos-telefonicos',
                        roles: rolesAccess['RELATORIO_BI'].visualizar
                    }/*, {
                        name: 'Crianças Atendidas - Sintético',
                        state: 'relatorio-bi-criancas-atendidas-sintetico',
                        roles: rolesAccess['RELATORIO_BI'].visualizar
                    }, {
                        name: 'Crianças Atendidas - Analítico',
                        state: 'relatorio-bi-criancas-atendidas-analitico',
                        roles: rolesAccess['RELATORIO_BI'].visualizar
                    }, {
                        name: 'Políticas Públicas - Sintético',
                        state: 'relatorio-bi-politicas-publicas-sintetico',
                        roles: rolesAccess['RELATORIO_BI'].visualizar
                    }, {
                        name: 'Políticas Públicas - Analítico',
                        state: 'relatorio-bi-politicas-publicas-analitico',
                        roles: rolesAccess['RELATORIO_BI'].visualizar
                    }, {
                        name: 'Produção Agrícola - Sintético',
                        state: 'relatorio-bi-producao-agricola-sintetico',
                        roles: rolesAccess['RELATORIO_BI'].visualizar
                    }*/]
                },
                {
                    name: 'Avaliação',
                    icon: 'ic_folder_shared',
                    roles: rolesAccess['MENU_AVALIACAO'].abrir,
                    submenus: [{
                        name: 'Planos Anuais',
                        state: 'listagem-avaliacao-anual',
                        roles: rolesAccess['MENU_AVALIACAO'].abrir
                    }, {
                        name: 'Planos Trimestrais',
                        state: 'avaliacao-planos-trimestrais',
                        roles: rolesAccess['MENU_AVALIACAO'].abrir
                    }, {
                        name: 'Relatórios Trimestrais',
                        state: 'listagem-avaliacao-relatorio-planos-trimestrais',
                        roles: rolesAccess['MENU_AVALIACAO'].abrir
                    }]
                },
                {
                    name: 'Administração Portal',
                    icon: 'ic_build',
                    roles: ['ROLE_ADMIN'],
                    submenus: [{
                        name: 'Usuários',
                        state: 'user-management',
                        roles: ['ROLE_ADMIN']
                    },
                        {
                            name: 'Auditoria',
                            state: 'auditoria-acessos',
                            roles: ['ROLE_ADMIN']
                        }]
                },
                {
                    name: 'Ajuda',
                    icon: 'ic_help',
                    roles: rolesAccess['ALL_ROLES'].buscar,
                    func: function () {
                        $rootScope.introOnStart();
                    },
                    divisorBefore: true

                },
                {
                    name: 'Feedback',
                    icon: 'ic_announcement',
                    roles: rolesAccess['ALL_ROLES'].buscar,
                    func: function () {
                        $rootScope.mostrarFeedback = !$rootScope.mostrarFeedback;
                    },
                    divisorAfter: true
                }
            ]
        }
    });
