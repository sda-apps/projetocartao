'use strict';

angular.module('projetocartaoApp')
    .controller('MenuController', function ($scope, $rootScope, $state, $mdMedia, Auth, Principal, identificadorRoles, menuService, $location) {

        $scope.$state = $state;
        $scope.menus = [];
        var path = $location.path();

        Principal.identity().then(function (account) {
            if (account == null && path != "/signup/finish" && path != "/reset/finish") {
                Auth.logout();
                $state.go('login');
            }

            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;

            if ($rootScope.isAuthenticated()) {
                $rootScope.isAgricultor = identificadorRoles.isAgricultor($rootScope.account.authorities);
            }
            if ($rootScope.isAuthenticated && !$rootScope.isAgricultor && !$rootScope.isNovaAba) {
                carregarMenuUA();
                menuService.menuTravadoAberto = false;
            } else {
                $scope.menus = menuService.menus;
            }
        });

        var carregarMenuUA = function () {
            if (Principal.isAuthenticated()) {
                Principal.hasAuthority('ROLE_ADMIN').then(function (result) {
                    $rootScope.isAdmin = result;

                    if ($rootScope.isAdmin) {
                        $scope.menus = menuService.menusUA;
                    } else {
                        for (var i = 0; i < menuService.menusUA.length; i++) {
                            for (var roleAllow in menuService.menusUA[i].roles) {
                                var role = menuService.menusUA[i].roles[roleAllow];

                                $rootScope.account.authorities.forEach(function (item) {
                                    if (item == role) {
                                        if (menuService.menusUA[i].submenus) {
                                            menuService.menusUA[i].submenus = menuService.menusUA[i].submenus.filter(function (itemSubMenu) {
                                                return itemSubMenu.roles.some(function (roleSub) {
                                                    return roleSub == role;
                                                })
                                            });
                                        }

                                        $scope.menus.push(menuService.menusUA[i]);

                                    }
                                });
                            }
                        }
                    }
                });
            }
        };

        $scope.dispMovel = !$mdMedia('gt-sm');
        menuService.menuTravadoAberto = !$scope.dispMovel;
        $scope.mouseOver = false;

        $scope.isSidebarAberto = function () {
            return (menuService.menuTravadoAberto || $scope.mouseOver);
        };

        $scope.isSelected = function (menu) {
            if (menu.hasOwnProperty("submenus")) {
                for (var index in menu.submenus) {
                    if (menu.submenus[index].state === $state.current.name) {
                        return true;
                    }
                }
            }
            return menu.state === $state.current.name;
        };

        $scope.sidebarClass = function () {
            if ($scope.dispMovel) {
                return menuService.menuTravadoAberto ? "aberto-disp-movel" : "fechado-disp-movel";
            } else {
                return (menuService.menuTravadoAberto || $scope.mouseOver) ? "aberto" : "fechado";
            }
        };

        $scope.clickMenu = function (menu) {
            if (menu.submenus) {
                menu.arrow = !menu.arrow;
            } else {
                if ($scope.dispMovel) {
                    menuService.menuTravadoAberto = false;
                }
                if (menu.func) {
                    menu.func();
                }
                if (menu.state) {
                    $state.go(menu.state);
                }
            }
        };
    });
