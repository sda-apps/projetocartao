'use strict';

angular.module('projetocartaoApp')
    .factory('Entidade', function ($resource) {
        return $resource('api/empresas/:id', {id: '@id'}, {
            'save': { method: 'POST' },
            'get': { method: 'GET' },
            'update': { method:'PUT' }
        });
    })
    .factory('EntidadePorRegiao', function ($resource) {
        return $resource('api/empresas/regiao/:id', {}, {
            'get': { method: 'GET' , isArray: true}
        });
    });
