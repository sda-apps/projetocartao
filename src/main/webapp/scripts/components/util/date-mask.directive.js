'use strict';

angular.module('projetocartaoApp')
    .directive('dateMask', function () {
    	return{
    		require:"ngModel",
    		link: function(scope, element, attr, ctrl){

    			var _formatDate = function(date){
                    if(date && (typeof date) == "string"){
                        date = date.replace(/[^0-9]+/g, "");

        				if(date.length >2){
        					date=date.substring(0,2) + "/" + date.substring(2);

        				}
        				if(date.length>5){
        					date=date.substring(0,5) + "/" + date.substring(5,9);
        				}
        				return date;
                    }
    			}

    			var _validateDate = function(date){
                    var tamanho=0;
                    var valida = true;
                    if(date){
                        tamanho=date.length;
                    }
                    if(tamanho == 10){
                        var dia = parseInt(date.substring(0,2),10);
                        var mes = parseInt(date.substring(3,5),10);
                        if(dia > 31){
                            valida = false;
                        }else if(mes > 12) {
                            valida = false;
                        }else{
                            valida = true;
                        }
                    }

                    ctrl.$setValidity('minimo', tamanho>=1 && tamanho< 10 ? false : true);
                    ctrl.$setValidity('formatoInvalido', valida);
                }
                ctrl.$formatters.push(function(date){
                    date=_formatDate(date);
                    ctrl.$setViewValue(date);
                    ctrl.$render();
                    return date;
                });

                ctrl.$parsers.push(function(date){
                    date = date.replace(/[^0-9]+/g, "");
                    return date;
                });

    			element.bind("keyup", function(){
    				ctrl.$setViewValue(_formatDate(ctrl.$modelValue));
    			    ctrl.$render();
    			});

                element.bind("blur", function(){
                    var date=_formatDate(ctrl.$modelValue);
                    _validateDate(date);
                });
            }
    	};
    });
