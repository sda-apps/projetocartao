'use strict';

angular.module('projetocartaoApp')
    .service('projetosCores', function(projetosConstants){
        this.getCor = function (id) {
            for (var projeto in projetosConstants){
                if (projetosConstants[projeto].id === id){
                    return projetosConstants[projeto].cor;
                }
            }
            return "#bababa";
        };
    });
