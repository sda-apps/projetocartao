'use strict';

angular.module('projetocartaoApp')
    .service('identificadorRoles', function () {
        this.isAgricultor = function (authorities) {
            return (authorities && (authorities.indexOf("ROLE_USER") !== -1 ||
                    authorities.indexOf("ROLE_AGRI_REST") !== -1 ||
                    authorities.indexOf("ROLE_AGRI") !== -1));
        };

        this.isUsuarioATC = function (authorities) {
            return (authorities && authorities.indexOf("ROLE_RESPONSAVEL_ATER") !== -1);
        };

        this.isUsuarioERP = function (authorities) {
            return (authorities && authorities.indexOf("ROLE_USUARIO_ERP") !== -1);
        };

        this.isUsuarioUGP = function (authorities) {
            return (authorities && authorities.indexOf("ROLE_USUARIO_UGP") !== -1);
        };

        this.isAdmin = function (authorities) {
            return (authorities && authorities.indexOf("ROLE_ADMIN") !== -1);
        }
    });
