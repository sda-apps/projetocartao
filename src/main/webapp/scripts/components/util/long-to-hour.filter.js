'use strict';

angular.module('projetocartaoApp')
    .filter('longToHour', function () {
        return function(long) {
        	if(long){
                var hours = Math.floor(long / 3600);

                var minutes = (long % 3600) / 60;

                if(minutes < 10) {
                    minutes = "0" + minutes;
                }

                return hours + "h" + minutes;
			}
		};
    });
