angular.module('projetocartaoApp')
    .directive('phoneMask', function () {
        return{
            require: 'ngModel',
            link: function(scope, element, attrs, ctrl){

                var _formatPhone = function(phone){
                    if(phone){
                        phone = phone.replace(/[^0-9]+/g, "");

                        if(phone.length>2){
                            phone="("+phone.substring(0,2) + ")" + phone.substring(2,13);
                        }

                        if(phone.length>8){
                            phone=phone.substring(0,8) + "-" + phone.substring(8,13);
                        }

                        if(phone.length>13){
                            phone = phone.replace("-","");
                            phone=phone.substring(0,9) + "-" + phone.substring(9,13);
                        }


                        return phone;
                    }
                }

                ctrl.$formatters.push(function(phone){
                    phone=_formatPhone(phone);
                    ctrl.$setViewValue(phone);
                    ctrl.$render();
                    return phone;
                });

                ctrl.$parsers.push(function(phone){
                    phone = phone.replace(/[^0-9]+/g, "");
                    return phone;
                });

                element.bind("keyup", function(){
                    ctrl.$setViewValue(_formatPhone(ctrl.$modelValue));
                    ctrl.$render();
                });
            }
        };
    });
