'use strict';

angular.module('projetocartaoApp')
    .directive('capitalize', function () {
        return{
            require: 'ngModel',
            link: function(scope, element, attrs, ctrl){

                var _formatNome = function(nome){
                    if(nome){
                        var nomesSeparados = nome.split(' ');
                        var nomesFormatados = nomesSeparados.map(
                            function (inputValue) {
                                if (/^(da|de|das|e|dos|ao|aos|)$/.test(inputValue.toLowerCase())) {
                                return inputValue.toLowerCase();
                                }
                                if(/^(PAA|PAPT|PNAE|RH|GS|LTDA|EPP|)$/.test(inputValue.toUpperCase())) {
                                    return inputValue.toUpperCase();
                                }
                                return inputValue.charAt(0).toUpperCase() + inputValue.substring(1).toLowerCase();
                            });
                        var transformedInput = nomesFormatados.join(' ');
                        return transformedInput;
                    }
                }

                var estadoInicial="";

                ctrl.$formatters.push(function(nome){
                    estadoInicial =nome;
                    nome=_formatNome(nome);
                    ctrl.$setViewValue(nome);
                    ctrl.$render();
                    return nome;
                });

                ctrl.$parsers.push(function(nome){
                    nome= estadoInicial;
                    return nome;
                });

                element.bind("keyup", function(){
                    ctrl.$setViewValue(_formatNome(ctrl.$modelValue));
                    ctrl.$render();
                });
            }
        };
    });
