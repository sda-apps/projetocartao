'use strict';

angular.module('projetocartaoApp')
    .directive('cepMask', function () {
        return{
            require: 'ngModel',
            link: function(scope, element, attrs, ctrl){

                var _formatCep = function(cep){
                    if(cep){
                        cep = cep.replace(/[^0-9]+/g, "");

                        if(cep.length>5){
                            cep=cep.substring(0,5) + "-" + cep.substring(5,8);
                            
                        }
                        return cep;
                    }
                }

                ctrl.$formatters.push(function(cep){
                    cep=_formatCep(cep);
                    ctrl.$setViewValue(cep);
                    ctrl.$render();
                    return cep;
                });

                ctrl.$parsers.push(function(cep){
                    cep = cep.replace(/[^0-9]+/g, "");
                    return cep;
                });

                element.bind("keyup", function(){
                    ctrl.$setViewValue(_formatCep(ctrl.$modelValue));
                    ctrl.$render();
                });
            }
        };
    });