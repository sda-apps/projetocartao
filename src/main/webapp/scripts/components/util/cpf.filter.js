angular.module('projetocartaoApp')
	.filter('cpf', function(){ 
		return function(cpf){ 
			if(cpf && cpf.indexOf("-")===-1){
				return	cpf.substr(0, 3) + '.' 
						+ cpf.substr(3, 3) + '.' 
						+ cpf.substr(6, 3) + '-' 
						+ cpf.substr(9,2);  
			}
			else{
				return cpf;	
			} 
		};
	});