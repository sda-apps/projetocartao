'use strict';

angular.module('projetocartaoApp')
    .directive('sexoMask', function () {
    	return{
    		require: 'ngModel',
    		link: function(scope, element, attrs, ctrl){

                var _formatSexo = function(sexo){
                    if(sexo){
                        if(sexo==='F'){
                            var transformedInput = 'Feminino';
                        }
                        else if(sexo==='M'){
                            var transformedInput = 'Masculino';
                        }
                        return transformedInput;
                    }
                }


                ctrl.$formatters.push(function(sexo){
                    sexo=_formatSexo(sexo);
                    ctrl.$setViewValue(sexo);
                    ctrl.$render();
                    return sexo;
                });

                ctrl.$parsers.push(function(sexo){
                    if(sexo.charAt(0)==='F'){
                        sexo="F";
                    }
                    else{
                        sexo="M";
                    }
                    return sexo;
                });

                element.bind("keyup", function(){
                    ctrl.$setViewValue(_formatSexo(ctrl.$modelValue));
                    ctrl.$render();
                });    
    		}
    	};
    });