angular.module('projetocartaoApp').
    filter('sumFilter', function() {
        return function(municipios) {
            var totalFamilias = 0;
                for (var i = 0; i < municipios.length; i++) {
                    if(municipios[i].quantidadeFamilias == ""){
                        municipios[i].quantidadeFamilias = 0;
                    }
                    var valor = parseInt(municipios[i].quantidadeFamilias,10);

                    totalFamilias = totalFamilias + valor;

                }

            return totalFamilias;
        };
    });
