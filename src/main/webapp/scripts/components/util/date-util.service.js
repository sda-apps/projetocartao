'use strict';

angular.module('projetocartaoApp')
    .service('DateUtils', function ($filter) {

    this.convertLocaleDateToServer = function(date) {
        if (date) {
            return $filter('date')(date, 'yyyy-MM-dd');
        } else {
            return null;
        }
    };

    /* Padrão dd/MM/yyyy */
    this.formatDate = function(date){
        if(date && (typeof date) == "string"){
            date = date.replace(/[^0-9]+/g, "");

            if(date.length >2){
                date=date.substring(0,2) + "/" + date.substring(2);

            }
            if(date.length>5){
                date=date.substring(0,5) + "/" + date.substring(5,9);
            }
            return date;
        }
    };

        /* Padrão yyyy-MM-dd */
        this.formatDateUS = function(date){
            if(date && (typeof date) == "string"){
                date = date.replace(/[^0-9]+/g, "");

                date = date.substring(4,8) + "-" + date.substring(2,4) + "-" + date.substring(0,2);

                return date;
            }
        };

    /* Padrão dd/MM/yyyy */
    this.convertFormattedDate = function(dataFormatada){
        var dateString = dataFormatada.split("/");
        var date;
        if (dateString.length == 3) {
            date = new Date(dateString[2], dateString[1] - 1, dateString[0]);
        }
        return date
    };



    this.convertLocaleDateFromServer = function(date) {
        if (date) {
            var dateString = date.split("-");
            return new Date(dateString[0], dateString[1] - 1, dateString[2]);
        }
        return null;
    };

    this.convertDateTimeFromServer = function(date) {
        if (date) {
            return new Date(date);
        } else {
            return null;
        }
    };

    // common date format for all date input fields
    this.dateformat = function() {
        return 'yyyy-MM-dd';
    };
});
