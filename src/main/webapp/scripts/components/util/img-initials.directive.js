'use strict';

angular.module('projetocartaoApp')
.directive('showInitials', function() {
    return function(scope, element, attrs) {
        var userVals = attrs.showInitials;
        var arrUserVals = userVals.split(',');

        element.bind('error', function() {
            modificaFoto();
        });

        attrs.$observe('ngSrc', function(value) {
            if (!value ) {
                modificaFoto();
            }
        });

        var modificaFoto = function(){
            var initialStr = (arrUserVals[0].substr(0, 1)).toUpperCase();;
            var arrayCores=['#F44336','#E91E63','#9C27B0','#673AB7','#3F51B5','#2196F3','#03A9F4','#00BCD4','#009688','#4CAF50','#8BC34A','#FF9800','#FF5722','#795548','#9E9E9E','#607D8B'];
            if(arrUserVals[3] == null) {
                arrUserVals[3] = Math.floor((Math.random() * 15) + 1);
                if(arrUserVals[1] == 80){
                    $(element).context.outerHTML = '<div style="margin: 9px 19px 0px 8px; font-size: '+arrUserVals[2]+'px; line-height: '+arrUserVals[1]+'px; width:'+arrUserVals[1]+'px; height: '+arrUserVals[1]+'px; background-color:'+arrayCores[arrUserVals[3]]+'" class="avatar-framed text">'+initialStr+'</div>';
                } else {
                    $(element).context.outerHTML = '<div style="font-size: '+arrUserVals[2]+'px; line-height: '+arrUserVals[1]+'px; width:'+arrUserVals[1]+'px; height: '+arrUserVals[1]+'px; background-color:'+arrayCores[arrUserVals[3]]+'" class="avatar-framed text">'+initialStr+'</div>';
                }
            } else {
                if(arrUserVals[1] == 80){
                    $(element).context.outerHTML = '<div style="position: absolute; font-size: '+arrUserVals[2]+'px; line-height: '+arrUserVals[1]+'px; width:'+arrUserVals[1]+'px; height: '+arrUserVals[1]+'px; background-color:'+arrayCores[arrUserVals[3]]+'" class="avatar-framed text">'+initialStr+'</div>';   
                } else {
                    $(element).context.outerHTML = '<div style="font-size: '+arrUserVals[2]+'px; line-height: '+arrUserVals[1]+'px; width:'+arrUserVals[1]+'px; height: '+arrUserVals[1]+'px; background-color:'+arrayCores[arrUserVals[3]]+'" class="avatar-framed text">'+initialStr+'</div>';   
                }
            }
        }
    }
});