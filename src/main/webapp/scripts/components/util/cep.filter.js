'use strict';

angular.module('projetocartaoApp')
	.filter('cep', function(){ 
		return function(cep){ 
			return	cep.substr(0, 5) + '-' 
					+ cep.substr(5, 3) 
			}; 
	});