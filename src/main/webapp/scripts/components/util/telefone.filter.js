angular.module('projetocartaoApp')
    .filter('telefone', function () {
        return function (telefone) {
            if (telefone) {
                telefone = telefone.replace(/[^0-9]+/g, "");
                telefone = telefone.indexOf(0) == '0' ? telefone.substring(1) : telefone;
                if (telefone.length === 10) {
                    return '(' + telefone.substr(0, 2) + ') '
                        + telefone.substr(2, 4) + '-'
                        + telefone.substr(6, 4);
                } else if (telefone.length === 8) {
                    return telefone.substr(0, 4) + '-' + telefone.substr(4, 4);
                } else if (telefone.length > 10) {
                    return '(' + telefone.substr(0, 2) + ') '
                        + telefone.substr(2, 5) + '-'
                        + telefone.substr(7, telefone.length);
                } else if (telefone.length === 9) {
                    return telefone.substr(0, 5) + '-' + telefone.substr(5, telefone.length);
                }
                else {
                    return telefone;
                }
            }
            else {
                return telefone;
            }
        };
    });
