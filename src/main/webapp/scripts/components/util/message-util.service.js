'use strict';

angular.module('projetocartaoApp')
    .service('MessageUtils', function ($filter) {

    this.getMessages = function(messages) {
    	var arrayMessages = [];
    	
    	if (messages) {
    		for(var i = 0; i < messages.length; i++) {
        		arrayMessages.push(messages[i].toString());
        	}
    	}
    	
    	return arrayMessages;
    }
   
});
