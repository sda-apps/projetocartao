'use strict';

angular.module('projetocartaoApp')
    .directive('currencyMask', function () {
        return{
            require:"ngModel",
            link: function(scope, element, attr, ctrl) {
                var _formatCurrency = function(currency) {
                    if(currency) {
                        currency = currency.toString();
                        currency = currency.replace(/[^0-9]+/g, "");

                        if(currency.length > 2) {
                            currency = currency.substring(0, (currency.length-2)) + "," + currency.substring((currency.length-2));
                        }

                        var dotPlace = 6;
                        while (currency.length > dotPlace){
                            currency = currency.substring(0, (currency.length-dotPlace)) + "." + currency.substring((currency.length-dotPlace));
                            dotPlace += 4;
                        }

                        return currency;
                    }
                };

                ctrl.$formatters.push(function(currency){
                    currency = _formatCurrency(currency);
                    ctrl.$setViewValue(currency);
                    ctrl.$render();
                    return currency;
                });

                ctrl.$parsers.push(function(currency){
                    currency = currency.replace(/[^0-9]+/g, "");
                    return currency;
                });

                element.bind("keyup", function(){
                    ctrl.$setViewValue(_formatCurrency(ctrl.$modelValue));
                    ctrl.$render();
                });
            }
        };
    });

