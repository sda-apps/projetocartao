angular.module('projetocartaoApp')
	.filter('celular', function(){ 
		return function(celular){ 
			if(celular){
				celular = celular.replace(/[^0-9]+/g, "");
				celular = celular.indexOf(0) == '0' ? celular.substring(1) : celular;
				if (celular.length===11){
					return	'(' + celular.substr(0, 2) + ') ' 
							+ celular.substr(2, 5) + '-' 
							+ celular.substr(7, 4);  
				}
				else if (celular.length===10){
					return	'(' + celular.substr(0, 2) + ') 9' 
							+ celular.substr(2, 4) + '-' 
							+ celular.substr(6, 4);  	
				}
				else if (celular.length===9){
					return	celular.substr(0, 5) + celular.substr(5, 4);  
				}
				else if (celular.length===8){
					return	'9' + celular.substr(0, 4) + celular.substr(4, 4);
				}
				else{
					return celular;
				}
			}
			else{
				return celular;	
			} 
		};
	});