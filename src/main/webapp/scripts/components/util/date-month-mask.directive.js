'use strict';

angular.module('projetocartaoApp')
    .directive('dateMonthMask', function () {
    	return{
    		require:"ngModel",
    		link: function(scope, element, attr, ctrl){

    			var _formatDate = function(date){
                    if(date && (typeof date) == "string"){
                        date = date.replace(/[^0-9]+/g, "");

        				if(date.length >2){
        					date=date.substring(0,2) + "/" + date.substring(2,6);

        				}

        				return date;
                    }
    			}

                ctrl.$formatters.push(function(date){
                    date=_formatDate(date);
                    ctrl.$setViewValue(date);
                    ctrl.$render();
                    return date;
                });

                ctrl.$parsers.push(function(date){
                    date = date.replace(/[^0-9]+/g, "");
                    return date;
                });

    			element.bind("keyup", function(){
    				ctrl.$setViewValue(_formatDate(ctrl.$modelValue));
    			    ctrl.$render();
    			});

                element.bind("blur", function(){
                    var date=_formatDate(ctrl.$modelValue)
                    var tamanho=0;
                    var valida = true;
                    if(date){
                        tamanho=date.length;
                    }
                    if(tamanho == 7){
                        var mes = parseInt(date.substring(0,2),10);
                        if(mes <= 12){
                            valida = true;
                        }else{
                            valida = false;
                        }
                    }
                    ctrl.$setValidity('minimo', tamanho>=1 && tamanho<= 6 ? false : true);
                    ctrl.$setValidity('requerido', tamanho===0 && ctrl.$dirty ? false : true);
                    ctrl.$setValidity('formatoMes', valida);
                });
            }
    	};
    });
