'use strict';

angular.module('projetocartaoApp')
    .directive('contratoMask', function () {
    	return{
    		require:"ngModel",
    		link: function(scope, element, attr, ctrl) {
    			var _formatContrato = function(contrato) {
                    if(contrato) {
                        contrato = contrato.replace(/[^0-9]+/g, "");

        				if(contrato.length > 4) {
                            contrato = contrato.substring(0,4) + "/" + contrato.substring(4,8);
        				}

        				return contrato;
                    }
    			}

                ctrl.$formatters.push(function(contrato){
                    contrato = _formatContrato(contrato);
                    ctrl.$setViewValue(contrato);
                    ctrl.$render();

                    return contrato;
                });

    			element.bind("keyup", function(){
    				ctrl.$setViewValue(_formatContrato(ctrl.$modelValue));
    			    ctrl.$render();
    			});

                element.bind("blur", function(){
                    var contrato = ctrl.$modelValue;
                    var tamanho = 0;

                    if(contrato) {
                        tamanho = contrato.length;
                    }

                    ctrl.$setValidity('minimo', tamanho>=1 && tamanho<= 7 ? false : true);

                });

            }
    	};
    });

