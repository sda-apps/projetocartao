'use strict';

angular.module('projetocartaoApp')
    .directive('cpfMask', function () {
    	return{
    		require:"ngModel",
    		link: function(scope, element, attr, ctrl) {
    			var _formatCpf = function(cpf) {
                    if(cpf) {
                        cpf = cpf.replace(/[^0-9]+/g, "");

        				if(cpf.length > 3) {
        					cpf = cpf.substring(0,3) + "." + cpf.substring(3);
        				}

        				if(cpf.length > 7) {
        					cpf = cpf.substring(0,7) + "." + cpf.substring(7);
        				}

        				if(cpf.length>11) {
        					cpf = cpf.substring(0,11) + "-" + cpf.substring(11,13);
        				}

        				return cpf;
                    }
    			}

                ctrl.$formatters.push(function(cpf){
                    cpf = _formatCpf(cpf);
                    ctrl.$setViewValue(cpf);
                    ctrl.$render();

                    return cpf;
                });

                ctrl.$parsers.push(function(cpf){
                    cpf = cpf.replace(/[^0-9]+/g, "");

                    return cpf;
                });

    			element.bind("keyup", function(){
                    _validarCpf(ctrl.$modelValue);
    				ctrl.$setViewValue(_formatCpf(ctrl.$modelValue));
    			    ctrl.$render();
    			});

                element.bind("blur", function(){
                    var cpf = ctrl.$modelValue;
                    var tamanho = 0;

                    if(cpf) {
                        tamanho = cpf.length;
                    }

                    _validarCpf(ctrl.$modelValue);

                    ctrl.$setValidity('minimo', tamanho>=1 && tamanho<= 10 ? false : true);
                    ctrl.$setValidity('requerido', tamanho===0 && ctrl.$dirty ? false : true);
                });

                var _validarCpf = function customValidator(ngModelValue) {
                    var BLACKLIST = [
                        "00000000000", "11111111111", "22222222222", "33333333333", 
                        "44444444444", "55555555555", "66666666666", "77777777777", 
                        "88888888888", "99999999999", "12345678909"
                    ];

                    function getFirstDigit(v) {
                        var matriz = [10, 9, 8, 7, 6, 5, 4, 3, 2];
                        var total = 0,
                            verifc;
                        for (var i = 0; i < 9; i++) {
                            total += v[i] * matriz[i];
                        }
                        verifc = ((total % 11) < 2) ? 0 : (11 - (total % 11));
                        return verifc;
                    }
                    
                    function getSecondDigit(v) {
                        var matriz = [11, 10, 9, 8, 7, 6, 5, 4, 3, 2];
                        var total = 0,
                            verifc;
                        for (var i = 0; i < 10; i++) {
                            total += v[i] * matriz[i];
                        }
                        verifc = ((total % 11) < 2) ? 0 : (11 - (total % 11));
                        return verifc;
                    }
                    
                    if (ngModelValue != null && ngModelValue.length >= 11) {
                        ctrl.$setValidity('cpfIncomplet', true);
                        var digits = ngModelValue.replace(/\D+/g, '');
                        var dig1 = getFirstDigit(digits.substr(0, 9));                    
                        var dig2 = getSecondDigit(digits.substr(0, 10));
                        var final = digits.substr(9,2);
                        var val = "" + dig1 + dig2;

                        if (final === val) {
                            if(BLACKLIST.indexOf(ngModelValue) >= 0) {
                                ctrl.$setValidity('cpfInvalid', false);
                            } else {
                                ctrl.$setValidity('cpfInvalid', true);
                            }

                        } else {
                            ctrl.$setValidity('cpfInvalid', false);
                        }

                    } else {
                        ctrl.$setValidity('cpfIncomplet', false); 
                    }

                    return ngModelValue;
                }
            }
    	};
    });

    