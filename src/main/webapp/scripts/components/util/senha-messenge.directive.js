'use strict';

angular.module('projetocartaoApp')
    .directive('senhaMessenge', function () {
    	return{
    		require:"ngModel",
    		link: function(scope, element, attr, ctrl){

                element.bind("blur", function(){
                    var input=ctrl.$modelValue;
                    var tamanho=0;
                    if(input){
                        tamanho=input.length;
                    }
                    ctrl.$setValidity('requerido', tamanho===0 && ctrl.$dirty ? false : true);
                    ctrl.$setValidity('minimo', tamanho>=1 && tamanho<= 4 ? false : true);
                    ctrl.$setValidity('maximo', tamanho>50 ? false : true);
                });    			
            }
    	};
    });