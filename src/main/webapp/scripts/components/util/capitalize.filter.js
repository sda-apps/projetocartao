'use strict';

angular.module('projetocartaoApp')
    .filter('capitalize', function () {
        return function(input) {
        	if(input){
			    var nomesSeparados = input.split(' ');
			    var nomesFormatados = nomesSeparados.map(function (nome) {
			      if (/^(da|do|de|das|e|dos|)$/.test(nome.toLowerCase())) {
			        return nome.toLowerCase();
			      }
			      if(/^(PAA|PAPT|PNAE|RH|GS|LTDA|EPP|)$/.test(nome.toUpperCase())) {
			        return nome.toUpperCase();
			      }
			      return nome.charAt(0).toUpperCase() + nome.substring(1).toLowerCase();
			    });
			    return nomesFormatados.join(' ');
			}
		};
    });
