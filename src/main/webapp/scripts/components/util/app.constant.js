'use strict';

angular.module('projetocartaoApp')
    .constant('constants', {
        'USUARIO_ATER': 'A'
    })

    .constant('projetosConstants', {
        'PAA_ALIMENTOS': {id: 1, cor: "#a2799e", hasExtrato: true, urlExtrato: 'api/servicos/extrato/paa-alimentos/'},
        'HORA_PLANTAR': {id: 3, cor: "#766795", hasExtrato: true, urlExtrato: 'api/servicos/extrato/horadeplantar/'},
        "CAJU": {id: 4, cor: "#bbdae3"},
        'HOROSAZONAL': {id: 5, cor: "#bababa"},
        'BIODIESEL': {id: 7, cor: "#94b967"},
        "PAA_LEITE": {id: 8, cor: "#80bbd4", hasExtrato: true, urlExtrato: 'api/servicos/extrato/paa-leite/'},
        'GARANTIA_SAFRA': {id: 10, cor: "#bababa"},
        'PENAE': {id: 14, cor: "#bababa", hasExtrato: true, urlExtrato: 'api/servicos/extrato/pnae/'},
        'DRITRIBUICAO_MILHO': {id: 17, cor: "#bababa"},
        'FEDAF': {id: 29, cor: "#bababa"},
        'UM_MILHAO_CISTERNAS': {id: 28, cor: "#c8a1b3"},
        'CISTERNAS_ENXURRADA': {id: 26, cor: "#c8a1b3"},
        'CISTERNAS_PLACAS': {id: 25, cor: "#c8a1b3"},
        'AGUA_PARA_TODOS': {id: 22, cor: "#c8a1b3"},
        'CISTERNAS_DE_PLACA': {tipo: 'P', codigoSda: 25},
        'CISTERNAS_DE_ENXURRADA': {tipo: 'E', codigoSda: 26},
        'CISTERNAS_DE_POLIETILENO': {tipo: 'I', codigoSda: 22}
    })

    .constant('planoTrimestralConstants', {
        '1': {desc: 'Em Elaboração ATC', color: '#FB8C00'},
        '2': {desc: 'Finalizado ATC', color: '#F57C00'},
        '3': {desc: 'Em Avaliação ERP', color: '#2196F3'},
        '4': {desc: 'Aprovado ERP', color: '#1565C0'},
        '5': {desc: 'Em Avaliação UGP', color: '#4CAF50'},
        '6': {desc: 'Aprovado UGP', color: '#2E7D32'}
    })

    .constant('iconStatusFile', {
        'A': {icon: 'description', color: '#2E7D32'},
        'R': {icon: 'assignment_late', color: '#e53935'},
        'E': {icon: 'find_in_page'},
        'null': {icon: 'publish'}
    })

    .constant('relatorioPlanoTrimestralConstants', {
        'E': {desc: 'Em Avaliação', color: '#2196F3'},
        'A': {desc: 'Aprovado', color: '#2E7D32'},
        'R': {desc: 'Recusado', color: '#e53935'}
    })

    .constant('planoAnualConstants', {
        '1': {desc: 'Em Elaboração ATC', color: '#FB8C00'},
        '2': {desc: 'Finalizado ATC', color: ' #F57C00'},
        '3': {desc: 'Em Avaliação ERP', color: '#2196F3'},
        '4': {desc: 'Aprovado ERP', color: '#1565C0'},
        '5': {desc: 'Em Avaliação UGP', color: '#4CAF50'},
        '6': {desc: 'Aprovado UGP', color: '#2E7D32'}
    })

    .constant('produtosConstants', {
        'LEITE_BOVINO': {id: 182},
        'LEITE_CAPRINO': {id: 183}
    })

    .constant('grupoProdutivoEnum', {
        'null': {desc: 'Não Respondido'},
        '1': {desc: 'Agricultor Familiar'},
        '2': {desc: 'Assentado de Reforma Agrária'},
        '3': {desc: 'Pescador'},
        '4': {desc: 'Marisqueira'},
        '5': {desc: 'Aquicultor'},
        '6': {desc: 'Artesão'},
        '7': {desc: 'Extrativista'},
        '8': {desc: 'Outros'}
    })

    .constant('grupoEtnicoEnum', {
        'null': {desc: 'Não Respondido'},
        'I': {desc: 'Indígena'},
        'Q': {desc: 'Quilombola'},
        'N': {desc: 'Não se indentifica'}
    })

    .constant('racaEnum', {
        'null': {desc: 'Não Respondido'},
        'A': {desc: 'Amarela'},
        'B': {desc: 'Branca'},
        'I': {desc: 'Indígena'},
        'P': {desc: 'Parda'},
        'R': {desc: 'Preta'}
    })

    .constant('parentescoEnum', {
        'F': {desc: 'Filho/a'},
        'P': {desc: 'Pai'},
        'M': {desc: 'Mãe'},
        'O': {desc: 'Avô'},
        'H': {desc: 'Avó'},
        'N': {desc: 'Neto/a'},
        'I': {desc: 'Irmão/ã'},
        'C': {desc: 'Cônjuge'},
        'E': {desc: 'Enteado'},
        'A': {desc: 'Agregado'}
    })
    .constant('atividadeEconomicaEnum', {
        'null': {desc: 'Não Respondido'},
        '1': {desc: 'Atividade Agrícola Formal'},
        '2': {desc: 'Atividade Agrícola Informal'},
        '3': {desc: 'Atividade Não Agrícola Formal'},
        '4': {desc: 'Atividade Não Agrícola Informal'},
        '5': {desc: 'Estudante'},
        '6': {desc: 'Outras'}
    })


    .constant('beneficioSocialEnum', {
        '1': {desc: 'Programa Bolsa Família'},
        '2': {desc: 'Beneficio de Prestação Continuada'},
        '3': {desc: 'Outro'},
        '4': {desc: 'Não Recebe'}
    })

    .constant('tipoAposentadoriaEnum', {
        'null': {desc: 'Não Respondido'},
        'R': {desc: 'Rural'},
        'V': {desc: 'Vínculo empregatício'},
        'M': {desc: '"Mais de um tipo na família'}
    })

    .constant('telefoneEnum', {
        '1': {desc: 'Celular'},
        '2': {desc: 'Residencial'},
        '3': {desc: 'Recados'},
        '4': {desc: 'Comunitário'},
        '5': {desc: 'Comercial'},
        '6': {desc: 'Celular/WhatsApp'}
    })

    .constant('temaEnum', {
        'A': {desc: 'Associativismo'},
        'C': {desc: 'Cooperativismo'},
        'G': {desc: 'Gênero'},
        'P': {desc: 'Atividade Produtiva'},
        'E': {desc: 'Agroecologia'},
        'R': {desc: 'Gerenciamento de Recursos Hídricos'},
        'O': {desc: 'Outros'}
    })

    .constant('orgaoEnum', {
        '1': {desc: 'EMATERCE'},
        '2': {desc: 'Prefeitura Municipal'},
        '3': {desc: 'Sindicato'},
        '4': {desc: 'Igreja'},
        '5': {desc: 'ONGs'},
        '6': {desc: 'SEBRAE'},
        '7': {desc: 'SENAR'},
        '8': {desc: 'Outros'}
    })

    .constant('oraganizacaoLocalEnum', {
        'S': {desc: 'Sindicato Rural'},
        'A': {desc: 'Associação Comunitária'},
        'C': {desc: 'Cooperativa'},
        'G': {desc: 'Grupo ou Pastoral da Igreja'},
        'P': {desc: 'Partido Político'},
        'M': {desc: 'Grupo de Mulheres'},
        'J': {desc: 'Grupo de Jovens'},
        'V': {desc: 'Movimento Social'},
        'O': {desc: 'Outros'}
    })

    .constant('subsistemaEnum', {
        'Q': {desc: 'Quintal'},
        'R': {desc: 'Roçado'},
        'P': {desc: 'Pecuária'},
        'F': {desc: 'Sistema Agroflorestal'},
        'E': {desc: 'Extrativismo'},
        'A': {desc: 'Sistema Agrossilvopastoril'},
        'H': {desc: 'Horta'},
        'M': {desc: 'Mandala'},
        'S': {desc: 'PAIS'},
        'O': {desc: 'Fruticultura'},
        'C': {desc: 'Pesca'},
        '0': {desc: 'Artesanato'},
        '1': {desc: 'Apicultura'},
        '2': {desc: 'Avicultura'},
        '3': {desc: 'Aquicultura'},
        '4': {desc: 'Pecuária Bovina'},
        '5': {desc: 'Pecuária Caprina'},
        '6': {desc: 'Pecuária Ovina'},
        '7': {desc: 'Pecuária Suína'}

    })

    .constant('situacaoFundiariaEnum', {
        'null': {desc: 'Não Respondido'},
        'P': {desc: 'Proprietário'},
        'O': {desc: 'Posseiro'},
        'U': {desc: 'Usufrutuário'},
        'A': {desc: 'Arrendatário'},
        'R': {desc: 'Parceiro'},
        'T': {desc: 'Sem Terra'},
        'S': {desc: 'Assentado'}
    })

    // Constantes para Politicas Publicas
    .constant('modalidadePronafEnum', {
        'null': {desc: 'Não Respondido'},
        'A': {desc: 'PRONAF A'},
        'C': {desc: 'PRONAF A/C'},
        'B': {desc: 'PRONAF B'},
        'V': {desc: 'PRONAF Variável'},
        'J': {desc: 'PRONAF Jovem'},
        'M': {desc: 'PRONAF Mulher'},
        'S': {desc: 'PRONAF Semi-Árido'},
        'G': {desc: 'PRONAF Agroecologia'},
        'F': {desc: 'PRONAF Florestal'},
        'E': {desc: 'PRONAF Estiagem'},
        'L': {desc: 'Mais de uma linha de crédito PRONAF'}
    })

    .constant('modalidadePaaEnum', {
        'null': {desc: 'Não Respondido'},
        'S': {desc: 'Compra da Agricultura Familiar para Doação Simultânea (MDS)'},
        'D': {desc: 'Compra Direta da Agricultura Familiar – CDAF (MDS/MDA)'},
        'I': {desc: 'Compra Institucional'}
    })

    .constant('categoriaHabitacaoRuralEnum', {
        'null': {desc: 'Não Respondido'},
        'F': {desc: 'Federal'},
        'E': {desc: 'Estadual'},
        'M': {desc: 'Municipal'}
    })

    .constant('categoriaBanheiroPopularEnum', {
        'null': {desc: 'Não Respondido'},
        'F': {desc: 'Federal'},
        'E': {desc: 'Estadual'},
        'M': {desc: 'Municipal'}
    })

    .constant('tipoHabitacaoEnum', {
        'null': {desc: 'Não Respondido'},
        'A': {desc: 'Alvenaria'},
        'T': {desc: 'Taipa'},
        'O': {desc: 'Outro'}
    })

    .constant('situacaoMoradiaEnum', {
        'null': {desc: 'Não Respondido'},
        'P': {desc: 'Própria'},
        'C': {desc: 'Cedida'},
        'O': {desc: 'Ocupada'},
        'A': {desc: 'Alugada'}
    })

    .constant('tipoEsgotamentoSanitarioEnum', {
        'null': {desc: 'Não Respondido'},
        'E': {desc: 'Ligação à rede de esgoto'},
        'S': {desc: 'Fossa séptica'},
        'V': {desc: 'Fossa verde'},
        'A': {desc: 'A céu aberto'}
    })

    .constant('qualEnergiaAlternativaEnum', {
        'null': {desc: 'Não Respondido'},
        'S': {desc: 'Solar'},
        'E': {desc: 'Eólica'},
        'B': {desc: 'Biodigestor'}
    })

    .constant('tipoSociedadeEnum', {
        'S': {id: 'S', desc: 'Organização da Sociedade Civil Sem Fins Lucrativos'},
        'E': {id: 'E', desc: 'Empresa Privada'},
        'P': {id: 'P', desc: 'Empresa Pública'},
        'O': {id: 'O', desc: 'Órgão Público'}
    })

    .constant('perfilTecnicoEnum', {
        'P': {id: 'P', desc: 'Assessor Produtivo'},
        'S': {id: 'S', desc: 'Assessor Social'},
        'C': {id: 'C', desc: 'Técnico Campo'}
    })

    .constant('localComercializacaoEnum', {
        'F': {id: 'F', desc: 'Feira Municipal'},
        'A': {id: 'A', desc: 'Feira da Agricultura Familiar'},
        'G': {id: 'G', desc: 'Feira Agroecológica'},
        'M': {id: 'M', desc: 'Mercado Institucional (PAA, PNAE)'},
        'C': {id: 'C', desc: 'CEASA'},
        'O': {id: 'O', desc: 'Comunidade'},
        'L': {id: 'L', desc: 'Mercado Local'},
        'P': {id: 'P', desc: 'Cooperativa'},
        'T': {id: 'T', desc: 'Atravessador'}
    })

    .constant('certificacaoEnum', {
        'M': {desc: 'SIM'},
        'E': {desc: 'SIE'},
        'F': {desc: 'SIF'}
    })

    .constant('atividadeCadastroStatus', {
        'T': {id: 'T', desc: 'Não Avaliado', color: '#594300', background: '#ffc107'},
        'E': {id: 'E', desc: 'Em Avaliação', color: 'white', background: '#3783c2'},
        'A': {id: 'A', desc: 'Aceito', color: 'white', background: '#00897b'},
        'R': {id: 'R', desc: 'Recusado', color: 'white', background: '#e53935'},
        'V': {id: 'V', desc: 'Revisado', color: 'white', background: '#3F51B5'}
    })

    .constant('tarefaStatus', {
        'T': {id: 'T', desc: 'Não Avaliado', color: '#594300', background: '#ffc107'},
        'E': {id: 'E', desc: 'Em Avaliação', color: 'white', background: '#3783c2'},
        'A': {id: 'A', desc: 'Aceito', color: 'white', background: '#00897b'},
        'R': {id: 'R', desc: 'Recusado', color: 'white', background: '#e53935'},
        'V': {id: 'V', desc: 'Revisado', color: 'white', background: '#3F51B5'}
    })

    .constant('abrangenciaEnum', {
        '1': {id: '1', desc: 'Comunitária/Local'},
        '2': {id: '2', desc: 'Municipal'},
        '3': {id: '3', desc: 'Intermunicipal'},
        '4': {id: '4', desc: 'Regional'},
        '5': {id: '5', desc: 'Nacional'},
        '6': {id: '6', desc: 'Internacional'},
        '7': {id: '7', desc: 'Territorial'}
    })

    .constant('tarefaTipo', {
        'PL': {id: 'PL', desc: 'Planejada'},
        'NP': {id: 'NP', desc: 'Não Planejada'},
        'PA': {id: 'PA', desc: 'Participante'}
    })

    .constant('TiposDeComunidadeEnum', {
        'A': {id: 'A', desc: 'Agricultura Familiar'},
        'S': {id: 'S', desc: 'Assentamento de Reforma Agrária'},
        'P': {id: 'P', desc: 'Pesca Artesanal'},
        'I': {id: 'I', desc: 'Indígena'},
        'Q': {id: 'Q', desc: 'Quilombola'}
    })

    .constant('rolesAccess', {
        'ALL_ROLES': {
            buscar: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_ATER_TECNICO',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO'],
            visualizar: [
                'ROLE_ADMIN'],
            incluir: [
                'ROLE_ADMIN'],
            editar: [
                'ROLE_ADMIN'],
            excluir: [
                'ROLE_ADMIN']
        },
        'MAIN_USUARIO_AVANCADO': {
            buscar: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_ATER_TECNICO',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO'],
            visualizar: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_ATER_TECNICO',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO'],
            incluir: ['ROLE_ADMIN'],
            editar: ['ROLE_ADMIN'],
            excluir: ['ROLE_ADMIN']
        },
        'MENU_ATER': {
            abrir: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_USUARIO_ATER_TECNICO',
                'ROLE_OBSERVATORIO'
            ]
        },
        'PTA': {
            visualizar: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO'],
            buscar: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO'],
            incluir: [
                'ROLE_ADMIN',
                'ROLE_USUARIO_UGP'],
            editar: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_UGP'],
            excluir: ['ROLE_ADMIN'],
            finalizar: [
                'ROLE_ADMIN',
                'ROLE_USUARIO_UGP',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_COORDENADOR_ATER'],
            reabrir: [
                'ROLE_ADMIN',
                'ROLE_USUARIO_UGP']
        },
        'PTA_TECNICO': {
            visualizar: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO'],
            buscar: [],
            incluir: ['ROLE_ADMIN', 'ROLE_USUARIO_ATER_ASSESSOR', 'ROLE_USUARIO_UGP', 'ROLE_USUARIO_COORDENADOR_ATER'],
            editar: ['ROLE_ADMIN', 'ROLE_USUARIO_ATER_ASSESSOR', 'ROLE_USUARIO_UGP', 'ROLE_USUARIO_COORDENADOR_ATER'],
            excluir: ['ROLE_ADMIN', 'ROLE_USUARIO_ATER_ASSESSOR', 'ROLE_USUARIO_UGP', 'ROLE_USUARIO_COORDENADOR_ATER']
        },
        'PTA_PLANO_TRIMESTRAL': {
            buscar: [],
            visualizar: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO'],
            incluir: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_UGP',
                'ROLE_USUARIO_COORDENADOR_ATER'],
            editar: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_UGP',
                'ROLE_USUARIO_COORDENADOR_ATER'],
            excluir: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_UGP',
                'ROLE_USUARIO_COORDENADOR_ATER'],
            finalizar: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_UGP',
                'ROLE_USUARIO_COORDENADOR_ATER']
        },
        'ATIVIDADE_CADASTRO': {
            visualizar: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_USUARIO_ATER_TECNICO',
                'ROLE_OBSERVATORIO'],
            buscar: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_USUARIO_ATER_TECNICO',
                'ROLE_OBSERVATORIO'],
            incluir: [],
            editar: [],
            excluir: [],
            avaliar: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_UGP',
                'ROLE_USUARIO_ATER_ASSESSOR']
        },
        'AVALIACAO_TAREFA': {
            visualizar: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_ERP', 'ROLE_USUARIO_UGP',
                'ROLE_USUARIO_ATER_TECNICO',
                'ROLE_OBSERVATORIO'],
            buscar: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_ERP', 'ROLE_USUARIO_UGP',
                'ROLE_USUARIO_ATER_TECNICO',
                'ROLE_OBSERVATORIO'],
            incluir: [],
            editar: [],
            excluir: [],
            avaliar: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_UGP',
                'ROLE_USUARIO_ATER_ASSESSOR']
        },
        'MENU_ADMINISTRACAO_ATER': {
            abrir: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_ATER_TECNICO',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO']
        },
        'ACAO': {
            buscar: ['ROLE_ADMIN', 'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO'],
            visualizar: ['ROLE_ADMIN', 'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO'],
            incluir: ['ROLE_ADMIN'],
            editar: ['ROLE_ADMIN'],
            excluir: ['ROLE_ADMIN']
        },
        'ATIVIDADES': {
            buscar: ['ROLE_ADMIN', 'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO'],
            visualizar: ['ROLE_ADMIN', 'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO'],
            incluir: ['ROLE_ADMIN'],
            editar: ['ROLE_ADMIN'],
            excluir: ['ROLE_ADMIN']
        },
        'EIXO_TEMATICO': {
            buscar: ['ROLE_ADMIN', 'ROLE_USUARIO_UGP', 'ROLE_OBSERVATORIO'],
            visualizar: ['ROLE_ADMIN', 'ROLE_USUARIO_UGP', 'ROLE_OBSERVATORIO'],
            incluir: ['ROLE_ADMIN'],
            editar: ['ROLE_ADMIN'],
            excluir: ['ROLE_ADMIN']
        },
        'TEMA': {
            buscar: ['ROLE_ADMIN', 'ROLE_USUARIO_UGP', 'ROLE_OBSERVATORIO'],
            visualizar: ['ROLE_ADMIN', 'ROLE_USUARIO_UGP', 'ROLE_OBSERVATORIO'],
            incluir: ['ROLE_ADMIN'],
            editar: ['ROLE_ADMIN'],
            excluir: ['ROLE_ADMIN']
        },
        'COMUNIDADE': {
            buscar: ['ROLE_ADMIN', 'ROLE_USUARIO_UGP', 'ROLE_OBSERVATORIO'],
            visualizar: ['ROLE_ADMIN', 'ROLE_USUARIO_UGP', 'ROLE_OBSERVATORIO'],
            incluir: ['ROLE_ADMIN'],
            editar: ['ROLE_ADMIN'],
            excluir: ['ROLE_ADMIN']
        },
        'ENTIDADE': {
            visualizar: ['ROLE_ADMIN', 'ROLE_USUARIO_ERP', 'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO'],
            buscar: ['ROLE_ADMIN', 'ROLE_USUARIO_ERP', 'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO'],
            incluir: ['ROLE_ADMIN', 'ROLE_USUARIO_UGP'],
            editar: ['ROLE_ADMIN', 'ROLE_USUARIO_UGP'],
            excluir: ['ROLE_ADMIN', 'ROLE_USUARIO_UGP']
        },
        'CONTRATO': {
            visualizar: ['ROLE_ADMIN', 'ROLE_USUARIO_ERP', 'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO'],
            buscar: ['ROLE_ADMIN', 'ROLE_USUARIO_ERP', 'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO'],
            incluir: ['ROLE_ADMIN', 'ROLE_USUARIO_UGP'],
            editar: ['ROLE_ADMIN', 'ROLE_USUARIO_UGP'],
            excluir: ['ROLE_ADMIN', 'ROLE_USUARIO_UGP']
        },
        'LOCALIDADE': {
            buscar: ['ROLE_ADMIN', 'ROLE_USUARIO_UGP', 'ROLE_OBSERVATORIO'],
            visualizar: ['ROLE_ADMIN', 'ROLE_USUARIO_UGP', 'ROLE_OBSERVATORIO'],
            incluir: ['ROLE_ADMIN'],
            editar: ['ROLE_ADMIN'],
            excluir: ['ROLE_ADMIN']
        },
        'USER_COMPANY': {
            buscar: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_OBSERVATORIO'],
            visualizar: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO'],
            incluir: [
                'ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP'],
            editar: [
                'ROLE_ADMIN',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_UGP'],
            excluir: ['ROLE_ADMIN']
        },
        'MENU_RELATORIO': {
            abrir: ['ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_ATER_TECNICO',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO']
        },
        'RELATORIO_CADASTRO_RESUMIDO': {
            visualizar: ['ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_ATER_TECNICO',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO']
        },
        'RELATORIO_CADASTRO_SINTETICO': {
            visualizar: ['ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_ATER_TECNICO',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO']
        },
        'RELATORIO_CADASTRO_ANALITICO': {
            visualizar: ['ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_ATER_TECNICO',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO']
        },
        'RELATORIO_PERFIL_FAMILIAS': {
            visualizar: ['ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_ATER_TECNICO',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO']
        },
        'RELATORIO_PARCEIRAS_SINTETICO': {
            visualizar: ['ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_ATER_TECNICO',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO']
        },
        'RELATORIO_PARCEIRAS_ANALITICO': {
            visualizar: ['ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_ATER_TECNICO',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO']
        },
        'RELATORIO_TRIMESTRAL_SIMPLIFICADO': {
            visualizar: ['ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_ATER_TECNICO',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO']
        },
        'RELATORIO_BI': {
            visualizar: ['ROLE_ADMIN',
                'ROLE_RESPONSAVEL_ATER',
                'ROLE_USUARIO_ATER',
                'ROLE_USUARIO_ATER_ASSESSOR',
                'ROLE_USUARIO_ATER_TECNICO',
                'ROLE_USUARIO_COORDENADOR_ATER',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO']
        },
        'MENU_AVALIACAO': {
            abrir: ['ROLE_ADMIN',
                'ROLE_USUARIO_ERP',
                'ROLE_USUARIO_UGP',
                'ROLE_OBSERVATORIO']
        }
    })

    .constant('projetosEnum', {
        '1': {id: '1', desc: 'Projeto Paulo Freire'},
        '2': {id: '2', desc: 'Projeto São José'}
    })

    .constant('abreviacaoAutoridadesConstants', {
        'ROLE_USUARIO_UGP': {desc: 'UGP'},
        'ROLE_USUARIO_ERP': {desc: 'ERP'}
    });
