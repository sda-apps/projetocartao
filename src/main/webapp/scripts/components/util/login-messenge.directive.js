'use strict';

angular.module('projetocartaoApp')
    .directive('loginMessenge', function () {
    	return{
    		require:"ngModel",
    		link: function(scope, element, attr, ctrl){

                element.bind("blur", function(){
                    var input=ctrl.$modelValue;
                    var tamanho=0;
                    if(input){
                        tamanho=input.length;
                    }
                    ctrl.$setValidity('requerido', tamanho===0 && ctrl.$dirty ? false : true);
                });
    			
            }
    	};
    });