'use strict';

angular.module('projetocartaoApp')
    .factory('ElementoCoordenadas', function ($resource) {
        return $resource('api/elementos/coordenadas/filtro', {}, {
            'query': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('ElementoAgricultor', function ($resource) {
        return $resource('api/elemento/agricultor/:elementoId', {}, {
            'query': {
                method: 'GET'
            }
        });
    });
