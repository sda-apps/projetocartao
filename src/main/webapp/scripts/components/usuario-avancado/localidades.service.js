'use strict';

angular.module('projetocartaoApp')
    .factory('LocalidadesFiltro', function ($resource) {
        return $resource('api/localidades/filtro', {}, {
            'query': {method: 'GET', isArray: true}
        });
    })
    .factory('Localidade', function ($resource) {
        return $resource('api/localidades/:id', {id: '@id'}, {
            'save': { method:'POST' },
            'update': { method:'PUT' },
            'delete':{ method:'DELETE'}
        });
    })
    .factory('LocalidadePorComunidade', function ($resource) {
        return $resource('api/localidades/comunidade/:id', {}, {
            'get': { method: 'GET' , isArray: true}
        });
    });
