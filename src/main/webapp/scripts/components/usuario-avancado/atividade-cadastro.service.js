'use strict';

angular.module('projetocartaoApp')
    .factory('AtividadeCadastro', function ($resource) {
        return $resource('api/mobile/cadastro/:id', {id: '@id'}, {
            'get': {
                method: 'GET'
            }
        });
    })
    .factory('AtividadeCadastroResumo', function ($resource) {
        return $resource('api/mobile/cadastro/resumo/:id', {}, {
            'get': { method: 'GET' }
        });
    })

    .factory('AtividadeCadastroPoliticasPublicas', function ($resource) {
        return $resource('api/mobile/cadastro/acesso-politicas-publicas/:id', {}, {
            'get': { method: 'GET' }
        });
    })
    .factory('AtividadeCadastroProducao', function ($resource) {
        return $resource('api/mobile/cadastro/producao/:id', {}, {
            'get': { method: 'GET' }
        });
    })
    .factory('AtividadeCadastroCapacitacao', function ($resource) {
        return $resource('api/mobile/cadastro/capacitacao/:id', {}, {
            'get': { method: 'GET' }
        });
    })

    .factory('BuscaAtividadeCadastro', function ($resource) {
        return $resource('api/mobile/cadastro/', {}, {
            'query': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('AvaliarCadastro', function ($resource) {
        return $resource('api/mobile/cadastro/avaliar/:id', {id: '@id'}, {
            'update': {
                method: 'PUT'
            }
        });
    })

    .factory('CountAtividadeCadastro', function ($resource) {
        return $resource('api/mobile/cadastro/count-atividades-cadastro', {}, {
            'query': {
                method: 'GET',
                isArray: true
            }
        });
    });
