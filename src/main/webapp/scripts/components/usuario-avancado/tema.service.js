'use strict';

angular.module('projetocartaoApp')
    .factory('TemaFiltro', function ($resource) {
        return $resource('api/ater/tema/filtro', {}, {
            'query': {method: 'GET', isArray: true}
        });
    })
    .factory('Tema', function ($resource) {
        return $resource('api/ater/tema/:id', {id: '@id'}, {
            'save': { method:'POST' },
            'update': { method:'PUT' },
            'delete':{ method:'DELETE'}
        });
    });
