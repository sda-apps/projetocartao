'use strict';

angular.module('projetocartaoApp')
    .factory('ContratoService', function ($resource) {
        return $resource('api/contratos/:id', {id: '@id'}, {
            'query': {method: 'GET', isArray: true},
            'get': {method: 'GET'},
            'save': { method:'POST' },
            'update': { method:'PUT' },
            'delete':{ method:'DELETE'}
        });
    })
    .factory('ContratosByEmprestaService', function ($resource) {
        return $resource('api/contratos/empresa/:id', {id: '@id'}, {
            'query': {method: 'GET', isArray: true}
        });
    })
    .factory('Regiao', function ($resource) {
        return $resource('api/ater/regiao', {}, {
            'query': {method: 'GET', isArray: true}
        });
    })
    .factory('MunicipioPorEmpresa', function ($resource) {
        return $resource('api/contratos/municipio-empresa/:id', {}, {
            'get': { method: 'GET' , isArray: true}
        });
    });
