'use strict';

angular.module('projetocartaoApp')
    .factory('ComunidadesFiltro', function ($resource) {
        return $resource('api/ater/comunidade/filtro', {}, {
            'query': {method: 'GET', isArray: true}
        });
    })
    .factory('Comunidade', function ($resource) {
        return $resource('api/ater/comunidade/:id', {id: '@id'}, {
            'save': { method:'POST' },
            'update': { method:'PUT' },
            'delete':{ method:'DELETE'}
        });
    })
    .factory('ComunidadePorMunicipio', function ($resource) {
        return $resource('api/ater/comunidade/municipio/:id', {}, {
            'get': { method: 'GET' , isArray: true}
        });
    })
    .factory('ComunidadePorMunicipioEAreaAtuacao', function ($resource) {
        return $resource('api/ater/comunidade/municipio/areaatuacao/:id', {}, {
            'get': { method: 'GET' , isArray: true}
        });
    });
