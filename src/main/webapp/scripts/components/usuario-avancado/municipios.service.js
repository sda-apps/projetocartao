'use strict';

angular.module('projetocartaoApp')
    .factory('MunicipiosPorRegiao', function ($resource) {
        return $resource('api/municipio/regiao/:regioes', {}, {
            'query': {
                method: 'GET',
                isArray: true
            }
        });
    });
