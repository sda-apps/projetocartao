'use strict';

angular.module('projetocartaoApp')
    .factory('PowerBI', function ($resource) {
        return $resource('api/powerbi', {}, {
            'post': {
                method: 'POST'
            }
        });
    });
