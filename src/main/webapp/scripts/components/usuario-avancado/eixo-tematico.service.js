'use strict';

angular.module('projetocartaoApp')
    .factory('EixoTematicoFiltro', function ($resource) {
        return $resource('api/ater/eixo-tematico/filtro', {}, {
            'query': {method: 'GET', isArray: true}
        });
    })
    .factory('EixoTematico', function ($resource) {
        return $resource('api/ater/eixo-tematico/:id', {id: '@id'}, {
            'save': { method:'POST' },
            'update': { method:'PUT' },
            'delete':{ method:'DELETE'}
        });
    })
    .factory('EixoTematicoTodos', function ($resource) {
        return $resource('api/ater/eixo-tematico', {}, {
            'query': {
                method: 'GET',
                isArray: true
            }
        });
    });
