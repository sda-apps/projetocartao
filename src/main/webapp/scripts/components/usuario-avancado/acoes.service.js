'use strict';

angular.module('projetocartaoApp')
    .factory('AcoesFiltro', function ($resource) {
        return $resource('api/ater/acao/filtro', {}, {
            'query': {method: 'GET', isArray: true}
        });
    })
    .factory('AcaoPTA', function ($resource) {
        return $resource('api/ater/acao/:id', {id: '@id'}, {
            'save': { method:'POST' },
            'update': { method:'PUT' },
            'delete':{ method:'DELETE'}
        });
    });
