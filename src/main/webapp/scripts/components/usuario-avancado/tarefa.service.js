'use strict';

angular.module('projetocartaoApp')
    .factory('BuscaTarefa', function ($resource) {
        return $resource('api/ater/tarefa/filtro', {}, {
            'query': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('BuscaTotalTarefa', function ($resource) {
        return $resource('api/ater/tarefa/filtro/total', {}, {
            'get': {
                method: 'GET'
            }
        });
    })
    .factory('Tarefa', function ($resource) {
        return $resource('api/ater/tarefa/:id', {}, {
            'get': {
                method: 'GET'
            }
        });
    })
    .factory('TarefaComplemento', function ($resource) {
        return $resource('api/ater/tarefa/complemento/:id', {}, {
            'get': {
                method: 'GET'
            }
        });
    })
    .factory('AtualizarTarefa', function ($resource) {
        return $resource('api/ater/tarefa/:id', {id: '@id'}, {
            'update': {
                method: 'PUT'
            }
        });
    })
    .factory('AnexoTarefa', function ($resource) {
        return $resource('api/ater/tarefa/anexo/:id', {id: '@id'}, {
            'update': {
                method: 'PUT'
            }
        })
    });
