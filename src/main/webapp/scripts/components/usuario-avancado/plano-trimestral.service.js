'use strict';

angular.module('projetocartaoApp')
    .factory('PlanoTrimestral', function ($resource) {
        return $resource('api/planotrimestral/:id', {id: '@id'}, {
            'query': {method: 'GET', isArray: true},
            'get': {method: 'GET', isArray: true},
            'save': {method: 'POST'},
            'update': {method: 'PUT'},
            'delete': {method: 'DELETE'}
        });
    })
    .factory('PlanoTrimestralUpdateSummary', function ($resource) {
        return $resource('api/planotrimestral/summary/:id', {id: '@id'}, {
            'update': {method: 'PUT'}
        });
    })
    .factory('PlanoTrimestralCopia', function ($resource) {
        return $resource('api/planotrimestral/copy/:id', {id: '@id'}, {
            'copy': {method: 'GET'}
        });
    })

    .factory('PlanoTrimestralPorPTA', function ($resource) {
        return $resource('api/planotrimestral/pta/:id', {}, {
            'get': {method: 'GET', isArray: true}
        });
    })

    .factory('PlanoTrimestralPorPTALista', function ($resource) {
        return $resource('api/planotrimestral/pta-lista/:id', {}, {
            'get': {method: 'GET', isArray: true}
        });
    })

    .factory('RelatorioPlanoTrimestal', function ($http, $window) {
        return {
            gerarRelatorioPlanoTrimestral:function(url, planoTrimestralId) {
                var newWindow = $window.open('#/loading', '_blank');

                $http.get(url + planoTrimestralId, {responseType:'arraybuffer'})
                    .success(function (response) {
                        var file = new Blob([response], {type: 'application/pdf'});
                        var fileURL = URL.createObjectURL(file);

                        newWindow.location.href = fileURL;
                    });
            }
        };
    })

    .factory('BuscaPlanosTrimestrais', function ($resource) {
        return $resource('api/planotrimestral/avaliacao/filtro', {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    });
