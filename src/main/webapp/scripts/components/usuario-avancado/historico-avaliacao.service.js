'use strict';

angular.module('projetocartaoApp')
    .factory('HistoricoAvaliacao', function ($resource) {
        return $resource('api/mobile/historico-avaliacao/:id', {id: '@id'}, {
            'query': {method: 'GET', isArray: true},
            'get': {method: 'GET'},
            'save': {method: 'POST'},
            'update': {method: 'PUT'}
        });
    })

    .factory('HistoricoAvaliacaoPlanos', function ($resource) {
        return $resource('api/ater/historico-avaliacao-planos/:id', {}, {
            'query': {method: 'GET', isArray: true},
            'save': {method: 'POST'}
        });
    })
    .factory('HistoricoAvaliacaoRelatorio', function ($resource) {
        return $resource('api/ater/historico-avaliacao-planos/file/:id', {id: '@id'}, {
            'query': {method: 'GET', isArray: true},
            'save': {method: 'POST'}
        });
    })
    .factory('HistoricoAvaliacaoTarefa', function ($resource) {
        return $resource('api/ater/historico-avaliacao-tarefa/:id', {}, {
            'query': {method: 'GET', isArray: true},
            'save': {method: 'POST'}
        });
    });
