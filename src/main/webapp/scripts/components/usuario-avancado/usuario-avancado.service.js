'use strict';

angular.module('projetocartaoApp')
    .factory('UsuarioAvancadoCpf', function ($resource) {
        return $resource('api/usuarioavancado/buscarcpf/:cpf', {}, {
            'get': {
                method: 'GET'
            }
        });
    })
    .factory('UsuarioAvancadoPorId', function ($resource) {
        return $resource('api/usuarioavancado/:id', {}, {
            'get': {
                method: 'GET'
            }
        });
    })
    .factory('SignUp', function ($resource) {
        return $resource('api/usuarioavancado/sendbroadcastemail', {}, {
            'post': {
                method: 'POST'
            }
        });
    })
    .factory('AprovaUsuario', function ($resource) {
        return $resource('api/account/approve/accessrequest', {}, {
            'post': {
                method: 'POST'
            }
        });
    })
    .factory('InativaUsuario', function ($resource) {
        return $resource('api/account/inactive', {}, {
            'post': {
                method: 'POST'
            }
        });
    })
    .factory('AtivaUsuario', function ($resource) {
        return $resource('api/account/activate', {}, {
            'post': {
                method: 'POST'
            }
        });
    });
