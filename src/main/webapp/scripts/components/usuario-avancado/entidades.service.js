'use strict';

angular.module('projetocartaoApp')
    .factory('EntidadesFiltro', function ($resource) {
        return $resource('api/empresas/filtro', {}, {
            'query': {method: 'GET', isArray: true}
        });
    });
