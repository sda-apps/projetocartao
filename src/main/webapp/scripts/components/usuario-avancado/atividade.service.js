'use strict';

angular.module('projetocartaoApp')
    .factory('Atividade', function ($resource) {
        return $resource('api/ater/atividade/:id', {id: '@id'}, {
            'get': {method: 'GET'},
            'update': { method:'PUT' },
            'delete':{ method:'DELETE'}
        });
    });
