'use strict';

angular.module('projetocartaoApp')
    .factory('BuscaPlanosAnuais', function ($resource) {
        return $resource('api/pta/', {}, {
            'query': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('BuscaPlanoAnualEmpresa', function ($resource) {
        return $resource('api/pta/empresa/:empresaId', {}, {
            'query': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('BuscaPlanoAnual', function ($resource) {
        return $resource('api/pta/filtro', {}, {
            'query': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('BuscaPlanoAnualAvaliacao', function ($resource) {
        return $resource('api/pta/avaliacao/filtro', {}, {
            'query': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('BuscaRegioesPlanoAnualPorTecnico', function ($resource) {
        return $resource('api/pta/tecnico-pta/regiao/:tecnicoId', {}, {
            'get': { method: 'GET', isArray: true }
        });
    });
