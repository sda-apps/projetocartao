'use strict';

angular.module('projetocartaoApp')
    .factory('AtividadesFiltro', function ($resource) {
        return $resource('api/ater/atividade/filtro', {}, {
            'query': {method: 'GET', isArray: true}
        });
    })
    .factory('Atividade', function ($resource) {
        return $resource('api/ater/atividade/:id', {id: '@id'}, {
            'save': { method:'POST' },
            'update': { method:'PUT' },
            'delete':{ method:'DELETE'}
        });
    });
