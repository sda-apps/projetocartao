'use strict';

angular.module('projetocartaoApp')
    .factory('Mensagem', function ($resource) {
        return $resource('/api/mensagens', {}, {
            'get': { method: 'GET', params:{de:'', para:''}, isArray:true},
            'save': { method:'POST' },
            'update': { method:'PUT' },
            'delete':{ method:'DELETE'}
        });
    });
