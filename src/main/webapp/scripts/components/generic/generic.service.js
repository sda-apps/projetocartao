'use strict';

angular.module('projetocartaoApp')
    .factory('EstadoCivil', function ($resource) {
        return $resource('api/estadocivil', {}, {
            'query': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('Uf', function ($resource) {
        return $resource('api/uf', {}, {
            'query': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('Escolaridade', function ($resource) {
        return $resource('api/escolaridade', {}, {
            'query': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('Cargo', function ($resource) {
        return $resource('api/cargo', {}, {
            'query': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('Territorio', function ($resource) {
        return $resource('api/territorio', {}, {
            'query': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('Acao', function ($resource) {
        return $resource('api/acao', {}, {
            'query': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('Regiao', function ($resource) {
        return $resource('api/regiao', {}, {
            'query': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('Empresa', function ($resource) {
        return $resource('api/empresas/paulo-freire', {}, {

        });
    })
    .factory('AssociacaoComunitaria', function ($resource) {
        return $resource('api/ater/associacoes', {}, {
            'findAll': {
                method: 'GET',
                isArray: true
            }
        });
    });
