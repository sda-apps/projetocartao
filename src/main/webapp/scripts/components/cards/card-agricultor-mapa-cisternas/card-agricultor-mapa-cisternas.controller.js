'use strict';

 angular.module('projetocartaoApp')
    .controller('CardAgricultorMapaCisternasController', function ($scope, $rootScope, Principal, AgricultorCisterna, $timeout, Toast, menuService) {
        Principal.identity().then(function(account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        $scope.menuService = menuService;

        function initMap() {
          $scope.mapGoogle = new google.maps.Map(document.getElementById('mapa'), {
            center: {lat: -5.00339434502215,lng: -38.8421630859375},
            mapTypeId: google.maps.MapTypeId.SATELLITE,
            scrollwheel: true,
            zoom: 18
          });
        };

        angular.extend($scope, {
            center: {
                lat: -5.00339434502215,
                lng: -38.8421630859375,
                zoom: 6
            },
            markers: {},
            icon: {},
            layers: {
                baselayers: {
                    bingAerialWithLabels: {
                        name: 'Satélite',
                        type: 'bing',
                        key: 'Aj6XtE1Q1rIvehmjn2Rh1LR2qvMGZ-8vPS9Hn3jCeUiToM77JFnf-kFRzyMELDol',
                        layerOptions: {
                            type: 'AerialWithLabels'
                        }
                    },
                    xyz: {
                        name: 'OpenStreetMap',
                        url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                        type: 'xyz'
                    }
                }
            }
        });

        $scope.iniciarGoogleMaps = function() {
        	initMap();
    		$scope.buscarPontoCistenaAgricultor();
    		$scope.atualizaImagensGoogleMaps(100);
        };

        $scope.$watch('menuService.getState()', function(newState){
            if (newState == 'cisternas') {
                $scope.buscarPontoCistenaAgricultor();
                $scope.atualizaImagensGoogleMaps(100);
            }

        }, true);

        $scope.buscarPontoCistenaAgricultor = function() {
            try {
                var agricultor = $rootScope.account.agricultor;

                if(agricultor) {
                    AgricultorCisterna.get({agricultorId: agricultor.id}, function (cisterna) {
                        if(cisterna.coordenada) {
                            $scope.marcarMapa(cisterna.coordenada, 18, true);
                        } else if(agricultor.pessoaFisica.pessoa.localidade != null && agricultor.pessoaFisica.pessoa.localidade.coordenada != null) {
                            $scope.marcarMapa(agricultor.pessoaFisica.pessoa.localidade.coordenada, 16, false);
                        } else if(agricultor.pessoaFisica.pessoa.distrito.coordenada) {
                            $scope.marcarMapa(agricultor.pessoaFisica.pessoa.distrito.coordenada, 12, false);
                        } else if(agricultor.pessoaFisica.pessoa.municipio.coordenada) {
                            $scope.marcarMapa(agricultor.pessoaFisica.pessoa.municipio.coordenada, 10, false);
                        } else {
                            $scope.atualizaImagensGoogleMaps(400);
                        }
                    });
                }
            } catch(e) {
                $scope.atualizaImagensGoogleMaps(400);
            }
        };

        $scope.marcarMapa = function(coordenada, zoom, comMarcador) {
            try {
                if(comMarcador) {
                    var pinIcon = {
                        url: 'assets/images/pin.svg',
                        size: new google.maps.Size(29, 40)
                    };
                    var marker = new google.maps.Marker({
                        position: {lat: Number((coordenada.latitude)), lng: Number((coordenada.longitude))},
                        map: $scope.mapGoogle,
                        icon: pinIcon
                    });
                }

                if($scope.mapGoogle) {
                    $scope.atualizaImagensGoogleMapsComCoordenada(coordenada);
                }

            } catch(e) {
                $scope.atualizaImagensGoogleMaps(400);
            }
        };

        $scope.atualizaImagensGoogleMaps = function(timeOut) {
            $timeout(function() {
                google.maps.event.trigger($scope.mapGoogle, 'resize');
            }, timeOut);
        };

        $scope.atualizaImagensGoogleMapsComCoordenada = function(coordenada) {
            $timeout(function() {
                google.maps.event.trigger($scope.mapGoogle, 'resize');
                $scope.mapGoogle.setCenter(new google.maps.LatLng({lat: Number((coordenada.latitude)), lng: Number((coordenada.longitude))}));
            }, 100);
        };

    });
