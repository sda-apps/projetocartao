'use strict';

 angular.module('projetocartaoApp')
    .controller('CardAgricultorMapaController', function ($scope, $rootScope, Principal, AgricultorCisterna, AgricultorPoligono, leafletData, $timeout, Toast) {
        Principal.identity().then(function(account) {
            $rootScope.account = account;

            $rootScope.isAuthenticated = Principal.isAuthenticated;

            $scope.buscarPontoCistenaAgricultor();
            
            $scope.carregarPoligonoPorPontos();

        });
        $scope.mostraMapa = "leaflet";

        function initMap() {
            $scope.mapGoogle = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -5.00339434502215,lng: -38.8421630859375},
                mapTypeId: google.maps.MapTypeId.SATELLITE,
                scrollwheel: true,
                zoom: 18
            });
        };

        angular.extend($scope, {
            center: {
                lat: -5.00339434502215,
                lng: -38.8421630859375,
                zoom: 6
            },
            markers: {},
            icon: {},
            layers: {
                baselayers: {
                    bingAerialWithLabels: {
                        name: 'Satélite',
                        type: 'bing',
                        key: 'Aj6XtE1Q1rIvehmjn2Rh1LR2qvMGZ-8vPS9Hn3jCeUiToM77JFnf-kFRzyMELDol',
                        layerOptions: {
                            type: 'AerialWithLabels'
                        }
                    },
                    xyz: {
                        name: 'OpenStreetMap',
                        url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                        type: 'xyz'
                    }
                }
            }
        });

        $scope.iniciaGoogleMaps = function() {
            $scope.mostraMapa = 'googlemaps';

            initMap();

            $scope.buscarPontoCistenaAgricultor();

            $scope.carregarPoligonoPorPontos();

            $scope.atualizaImagensGoogleMaps(100);

        };

        $scope.buscarPontoCistenaAgricultor = function() {
            try {
                var agricultor = $scope.account.agricultor;

                if(agricultor) {
                    AgricultorCisterna.get({agricultorId: agricultor.id}).$promise
                        .then(function (cisterna) {
                            if(cisterna.coordenada) {
                                $scope.marcarMapa(cisterna.coordenada, 18, true);
                            }
                        })
                        .catch(function() {
                            if(agricultor.pessoaFisica.pessoa.localidade != null && agricultor.pessoaFisica.pessoa.localidade.coordenada != null) {
                                $scope.marcarMapa(agricultor.pessoaFisica.pessoa.localidade.coordenada, 16, false);

                            } else if(agricultor.pessoaFisica.pessoa.distrito.coordenada) {
                                $scope.marcarMapa(agricultor.pessoaFisica.pessoa.distrito.coordenada, 14, false);

                            } else if(agricultor.pessoaFisica.pessoa.municipio.coordenada) {
                                $scope.marcarMapa(agricultor.pessoaFisica.pessoa.municipio.coordenada, 10, false);

                            } else {
                                $scope.atualizarImagensMapa(400);

                            }
                        });
                }
            } catch(e) {
                $scope.atualizarImagensMapa(400);
            }
        };

        $scope.marcarMapa = function(coordenada, zoom, comMarcador) {
            try {
                if(comMarcador) {
                    if($scope.mostraMapa == 'leaflet') {
                        angular.extend($scope, {
                            markers: {
                                m1: {
                                    lat: Number((coordenada.latitude)),
                                    lng: Number((coordenada.longitude)),
                                    icon: {
                                        iconUrl: 'assets/images/pin.svg',
                                        shadowUrl: 'assets/images/pin-sombra.png',
                                        iconSize:     [29, 48],
                                        shadowSize:   [44, 44],
                                        iconAnchor:   [9, 40],
                                        popupAnchor:  [3, -40]
                                    }
                                }
                            }
                        });
                    }

                    if($scope.mostraMapa == 'googlemaps') {
                        var pinIcon = {
                            url: 'assets/images/pin.svg',
                            size: new google.maps.Size(29, 40)
                        };

                        var marker = new google.maps.Marker({
                            position: {
                                lat: Number((coordenada.latitude)), 
                                lng: Number((coordenada.longitude))
                            },
                            map: $scope.mapGoogle,
                            icon: pinIcon
                        });
                    }
                }

                if($scope.mostraMapa == 'leaflet') {
                    $scope.atualizarImagensMapaComCoordenada(coordenada, zoom);
                }

                if($scope.mapGoogle) {
                    $scope.atualizaImagensGoogleMapsComCoordenada(coordenada);
                }

            } catch(e) {
                $scope.atualizarImagensMapa(400);
            }
        };

        $scope.atualizarImagensMapa = function(timeOut) {
            $timeout(function() {
                leafletData.getMap("card-mapa-leaflet").then(function(map) {
                    map.invalidateSize();
                });
            }, timeOut);
        };

        $scope.atualizaImagensGoogleMaps = function(timeOut) {
            $timeout(function() {
                google.maps.event.trigger($scope.mapGoogle, 'resize');
            }, timeOut);
        };

        $scope.atualizaImagensGoogleMapsComCoordenada = function(coordenada) {
            $timeout(function() {
                google.maps.event.trigger($scope.mapGoogle, 'resize');

                $scope.mapGoogle.setCenter(new google.maps.LatLng({
                    lat: Number((coordenada.latitude)), 
                    lng: Number((coordenada.longitude))
                }));
            }, 100);
        };

        $scope.atualizarImagensMapaComCoordenada = function(coordenada, zoom) {
            $timeout(function() {
                leafletData.getMap("card-mapa-leaflet").then(function(map) {
                    map.invalidateSize(true);

                    map._onResize();

                    map.setView({lat: coordenada.latitude, lon: coordenada.longitude}, zoom);
                });
            }, 700);
        };

        $scope.carregarPoligonoPorPontos = function() {
            if($scope.account.agricultor) {
                AgricultorPoligono.get({agricultorId: $scope.account.agricultor.id}, function (agricultor) {
                    var poligono = [];

                    for(var areas in agricultor.areasGeorreferenciadas) {
                        var area = agricultor.areasGeorreferenciadas[areas];

                        var vertices = [];

                        for(var i in area.poligono) {
                            var coordenada  = area.poligono[i].coordenada,
                                latLng      = new L.LatLng(coordenada.latitude, coordenada.longitude);

                            vertices.push(latLng);
                        }

                        poligono.push(vertices);

                        if($scope.mostraMapa == 'leaflet'){
                            leafletData.getMap("card-mapa-leaflet").then(function(map) {
                                map.addLayer(new L.Polygon(poligono));
                            });
                        }

                        if($scope.mapGoogle){
                            var poligonoGoogle = new google.maps.Polygon({
                                paths           : poligono,
                                strokeColor     : '#0e1cdf',
                                fillColor       : '#0e1cdf',
                                strokeOpacity   : 0.8,
                                strokeWeight    : 2,
                                fillOpacity     : 0.35
                            });
                            poligonoGoogle.setMap($scope.mapGoogle);
                        }
                    }
                });
            }

            $scope.atualizarImagensMapa(1000);
        };

        $scope.carregarPoligonoPorKML = function() {
            var track = new L.KML("../../../assets/kml/Piquet_Carneiro.kml", {async: true});

            track.on("loaded", function(e) {
                map.fitBounds(e.target.getBounds());
            });

            map.addLayer(track);

            //map.addControl(new L.Control.Layers({}, {'Track':track}));
        };
    });
