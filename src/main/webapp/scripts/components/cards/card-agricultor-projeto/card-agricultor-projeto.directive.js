angular.module('projetocartaoApp')
    .directive('cardAgricultorProjeto', function () {
	return{
		templateUrl:"scripts/components/cards/card-agricultor-projeto/card-agricultor-projeto.html",
		restrict: 'E'
	};
});