'use strict';

angular.module('projetocartaoApp')
    .controller('CardAgricultorProjetoController',
        function ($scope, $state, Principal, AgricultorProjetosPorAno, $filter, projetosCores) {
        	$scope.user = {};
            $scope.errors = {};
            $scope.colors = projetosCores;
            $scope.projetosAgricultor = [];

            Principal.identity().then(function(account) {
                $scope.account = account;
                $scope.isAuthenticated = Principal.isAuthenticated;
                getProjetosAgricultor();
            });

            var getProjetosAgricultor = function() {
                try {
                    AgricultorProjetosPorAno.get({agricultorId: $scope.account.agricultor.id, 'agricultorProjetoAno': 2016}, function (result){
                        $scope.projetosAgricultor = result;
                        angular.forEach($scope.projetosAgricultor, function(value, key) {
                            value.projeto.nome = $filter('capitalize')(value.projeto.nome);
                        });
                    });
                } catch(e) {
                    console.log("ERRO AgricultorProjetosPorAno: ",e);
                }
            };
});
