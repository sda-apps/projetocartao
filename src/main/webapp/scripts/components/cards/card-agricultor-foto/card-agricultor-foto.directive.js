angular.module('projetocartaoApp')
    .directive('cardAgricultorFoto', function () {
	return{
		templateUrl:"scripts/components/cards/card-agricultor-foto/card-agricultor-foto.html",
		restrict: 'E'
	};
});