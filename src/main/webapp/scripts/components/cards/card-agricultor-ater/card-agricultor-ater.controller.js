'use strict';

angular.module('projetocartaoApp')
    .controller('CardAgricultorAterController',
    	function ($scope, $state, $rootScope, Principal, AgricultorAter, UsuarioAvancadoCpf, $filter, AtendimentosPorAgricultor, identificadorRoles) {
        	$scope.user = {};
            $scope.errors = {};
            $scope.resultado = [];

            Principal.identity().then(function(account) {
                $rootScope.account = account;
                $rootScope.isAuthenticated = Principal.isAuthenticated;
                if ($rootScope.isAuthenticated()){
                    $rootScope.isAgricultor = identificadorRoles.isAgricultor($rootScope.account.authorities);
                }
            });

            $scope.bucarAter = function() {
                AgricultorAter.get(function (result) {
                    angular.forEach(result, function(assistencia){
                        $scope.buscarTecnicoPorCpf(assistencia);
                    });
                });
            };

            $scope.buscarTecnicoPorCpf = function(assistencia){
                UsuarioAvancadoCpf.get({cpf: assistencia.cpfTecnico}, function(tecnico){
                    assistencia.tecnico = tecnico;
                    $scope.resultado.push(assistencia);
                    return assistencia;
                })
            };

            var getAtendimentos = function() {
                $scope.filtro = {
                    id: $rootScope.account.agricultor.id,
                    ano: [],
                    mes: [],
                    tecnico: [],
                    atividade: []
                };

                AtendimentosPorAgricultor.query($scope.filtro).$promise.then(function (result) {
                    $scope.resultado = result;
                })
                .catch(function() {

                });
            };


            getAtendimentos();
});
