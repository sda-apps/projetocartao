angular.module('projetocartaoApp')
    .directive('cardAgricultorAter', function () {
	return{
		templateUrl:"scripts/components/cards/card-agricultor-ater/card-agricultor-ater.html",
		restrict: 'E'
	};
});