    'use strict';

angular.module('projetocartaoApp')
    .controller('CardAgricultorTemperaturaController',
        function ($scope, $state, $rootScope, Principal, AgricultorTemperatura, $filter) {
            $scope.user = {};
            Principal.identity().then(function(account) {
                $rootScope.account = account;
                $rootScope.isAuthenticated = Principal.isAuthenticated;
                $scope.bucarTemperatura();
            });

            $scope.bucarTemperatura = function() {
                $scope.municipio = $rootScope.account.agricultor.pessoaFisica.pessoa.municipio? $rootScope.account.agricultor.pessoaFisica.pessoa.municipio.nome : "Fortaleza";
                AgricultorTemperatura.get({municipioNome: $scope.municipio},function (result) {
                   $scope.temperaturaAtual = result.main.temp - 273.15;
                   $scope.temperaturaMax = result.main.temp_max - 273.15;
                   $scope.temperaturaMin = result.main.temp_min - 273.15;
                   $scope.imagemTemperatura = result.weather[0].icon;
                });
            };

});
