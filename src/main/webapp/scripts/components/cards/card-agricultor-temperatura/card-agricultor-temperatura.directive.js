angular.module('projetocartaoApp')
    .directive('cardAgricultorTemperatura', function () {
	return{
		templateUrl:"scripts/components/cards/card-agricultor-temperatura/card-agricultor-temperatura.html",
		restrict: 'E'
	};
});