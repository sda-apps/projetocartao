'use strict';

angular.module('projetocartaoApp')
    .controller('CardAgricultorDadosController',
    	function ($scope, $rootScope, $state, Principal, $filter, AgricultorDaps) {

            $scope.dadosAgricultor=[];
            Principal.identity().then(function(account) {
                $rootScope.account = account;
                $rootScope.isAuthenticated = Principal.isAuthenticated;
                preencheDados();
            });

            var preencheDados = function () {
                $scope.dadosAgricultor.push({dado: $filter('cpf')($rootScope.account.agricultor.pessoaFisica.cpf), descricao:'CPF'});
                $scope.dadosAgricultor.push({dado: $filter('date')($rootScope.account.agricultor.pessoaFisica.dataNascimento, "dd/MM/yyyy"), descricao:'Data de Nascimento'});
                if($rootScope.account.agricultor.pessoaFisica.sexo === 'F'){
                    $scope.dadosAgricultor.push({dado: 'Feminino', descricao: 'Sexo'});
                }else if ($rootScope.account.agricultor.pessoaFisica.sexo === 'M'){
                    $scope.dadosAgricultor.push({dado: 'Masculino', descricao: 'Sexo'});
                }else {
                    $scope.dadosAgricultor.push({dado: "", descricao: 'Sexo'});
                }
                if ($rootScope.account.agricultor.pessoaFisica.estadoCivil){
                    $scope.dadosAgricultor.push({dado: $filter('capitalize')($rootScope.account.agricultor.pessoaFisica.estadoCivil.descricao), descricao:'Estado Civil'});
                }else{
                    $scope.dadosAgricultor.push({dado: "", descricao:'Estado Civil'});
                }
                if ($rootScope.account.agricultor.pessoaFisica.pessoa.municipio){
                    $scope.dadosAgricultor.push({dado: $filter('capitalize')($rootScope.account.agricultor.pessoaFisica.pessoa.municipio.nome), descricao: 'Município'});
                }else{
                    $scope.dadosAgricultor.push({dado: "", descricao: 'Município'});
                }
                if ($rootScope.account.agricultor.pessoaFisica.pessoa.distrito){
                    $scope.dadosAgricultor.push({dado: $filter('capitalize')($rootScope.account.agricultor.pessoaFisica.pessoa.distrito.nome), descricao: 'Distrito'});
                }
                else{
                    $scope.dadosAgricultor.push({dado: "", descricao: 'Distrito'});
                }
                /*if ($rootScope.account.agricultor.pessoa.celular1){ //TODO remover para lista de telefones
                    $scope.dadosAgricultor.push({dado: $filter('celular')($rootScope.account.agricultor.pessoa.celular1), descricao: 'Celular'});
                }
                else if($rootScope.account.agricultor.pessoa.celular2){
                    $scope.dadosAgricultor.push({dado: $filter('celular')($rootScope.account.agricultor.pessoa.celular2), descricao: 'Celular'});
                }
                else if($rootScope.account.agricultor.pessoa.telefone){
                    $scope.dadosAgricultor.push({dado: $filter('telefone')($rootScope.account.agricultor.pessoa.telefone), descricao: 'Telefone'});
                }
                else{
                    $scope.dadosAgricultor.push({dado: "", descricao: 'Celular'});
                }*/
            }
        }
    );
