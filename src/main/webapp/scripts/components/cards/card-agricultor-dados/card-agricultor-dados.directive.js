angular.module('projetocartaoApp')
    .directive('cardAgricultorDados', function () {
	return{
		templateUrl:"scripts/components/cards/card-agricultor-dados/card-agricultor-dados.html",
		restrict: 'E'
	};
});