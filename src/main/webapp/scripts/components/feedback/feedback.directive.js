'use strict';

angular.module("projetocartaoApp").directive('feedback', function() {
    return {
        restrict: 'E',
        templateUrl: 'scripts/components/feedback/feedback.html',
        controller: 'FeedbackController'
    }
});
