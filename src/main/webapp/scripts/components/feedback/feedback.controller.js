'use strict';

angular.module('projetocartaoApp')
    .controller('FeedbackController', function ($scope, $rootScope, FeedbackService, Toast, $mdMedia,deviceDetector) {

	$scope.avaliacoes = ['Problema', 'Reclamação', 'Sugestão', 'Elogio'];

	$scope.avaliacao = null;

    $scope.mensagem = "";

    $rootScope.mostrarFeedback = false;

    $scope.ratings = [{
        current: 1,
        max: 5
    }];

    $scope.dispMovel = $mdMedia('xs');

    $scope.isOpen = false;

    $scope.enviarFeedback = function(form) {
        $scope.validarFormulario(form);

        if (form.$valid) {
            var feedback = $scope.montarFeedback();

            FeedbackService.save(feedback).$promise.then(function(){
                $scope.limparCampos(form);
                Toast.sucesso('Sua mensagem foi enviada com sucesso.');
            }).catch(function(erro){

            });
        }
    };

    $scope.validarFormulario = function (form) {
        if (!$scope.avaliacao) {
            form.avaliacao.$setValidity('required', false);
        } else {
            form.avaliacao.$setValidity('required', true);
        }

        if (!$scope.mensagem) {
            form.mensagem.$setValidity('required', false);
        } else {
            form.mensagem.$setValidity('required', true);
        }
    };

    $scope.montarFeedback = function () {
        var nome = '';
        var email = '';

        if ($rootScope.isAgricultor) {
            nome = $rootScope.account.agricultor.pessoaFisica.pessoa.nome;
            if ($rootScope.account.agricultor.pessoaFisica.pessoa.email) {
            	email = $rootScope.account.agricultor.pessoaFisica.pessoa.email;
            }
        } else {
            nome = $rootScope.account.usuarioAvancado.pessoaFisica.pessoa.nome;
            email = $rootScope.account.usuarioAvancado.emailInstitucional;
        }

        var feedback = {
            message: $scope.mensagem,
            rating: $scope.ratings[0].current,
            type: $scope.avaliacao,
            userName: nome,
            email: email,
            informacoes: montarInformacoesCliente()
        };

        return feedback;
    };

    var montarInformacoesCliente = function() {
        return "Browser: " + deviceDetector.browser +" - "+ deviceDetector.browser_version+ "\n" +
               "Dispositivo: "+ deviceDetector.device+ "\n" +
               "Sistema Operacional: " +deviceDetector.os +" - "+ deviceDetector.os_version;
    };

    $scope.limparCampos = function(form) {
        form.mensagem.$setValidity('required', true);
        form.avaliacao.$setValidity('required', true);
        $rootScope.mostrarFeedback = false;
    	$scope.mensagem = "";
    	$scope.ratings = [{
            current: 1,
            max: 5
        }];
    	$scope.avaliacao = null;
    }
});
