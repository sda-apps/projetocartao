'use strict';

// Diretiva para gerar as estrelas
angular.module('projetocartaoApp').directive('starRating', function () {
    return {
        restrict: 'A',
        template: 
        	'<ul class="rating">' +
            '<li ng-repeat="star in stars" ng-class="star" ng-click="toggle($index)">\u2605<md-tooltip md-direction="top">{{tooltip($index)}}</md-tooltip></li>' +
            '</ul>',
        scope: {
            ratingValue: '=',
            max: '=',
            onRatingSelected: '&'
        },
        link: function (scope, elem, attrs) {

            var updateStars = function () {
                scope.stars = [];
                for (var i = 0; i < scope.max; i++) {
                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
            };

            scope.toggle = function (index) {
                scope.ratingValue = index + 1;
                scope.onRatingSelected({
                    rating: index + 1
                });
            };

            scope.$watch('ratingValue', function (oldVal, newVal) {
                if (newVal) {
                    updateStars();
                }
            });
            
            scope.tooltip = function (index) {
            	
            	var tooltipValue = '';
            	switch (index) {
				case 0:
					tooltipValue = 'Ruim'
					break;

				case 1:
					tooltipValue = 'Regular'
					break;
					
				case 2:
					tooltipValue = 'Bom'
					break;
					
				case 3:
					tooltipValue = 'Muito bom'
					break;
					
				case 4:
					tooltipValue = 'Excelente'
					break;
				default:
					break;
				}
            	
            	return tooltipValue;
            };
        }
    }
});