'use strict';

angular.module('projetocartaoApp')
    .factory('FeedbackService', function ($resource) {
            return $resource('api/feedback', {}, {
                'save': { method:'POST' }
            });
        }
    );
