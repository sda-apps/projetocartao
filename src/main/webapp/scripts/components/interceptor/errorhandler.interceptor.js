'use strict';

angular.module('projetocartaoApp')
    .factory('errorHandlerInterceptor', function ($q, $rootScope, $injector) {
        return {
            'responseError': function (response) {
                if (!(response.status == 401 && response.data.path.indexOf("/api/account") == 0 )){
	                $rootScope.$emit('projetocartaoApp.httpError', response);
	            }
                var state = $injector.get('$state');
                $injector.get('$timeout')(function (){
                    if(state.current.data.authorities && state.current.data.authorities.length > 0 && response.status == 401) {
                        state.go('login-agricultor');
                    }
                }, 200);
                return $q.reject(response);
            }
        };
    });
