 'use strict';

angular.module('projetocartaoApp')
    .factory('notificationInterceptor', function ($q, AlertService, $injector) {
        return {
            response: function(response) {
                var message = response.headers('X-projetocartaoApp-message'),
                	status 	= response.headers('X-projetocartaoApp-status');
                
                if (angular.isString(message)) {
                	var Toast = $injector.get('Toast');
                	
                	switch (status) {
					case 'S':
						Toast.sucesso(message);
						break;
					
					case 'I':
						Toast.alerta(message);
						break;
						
					case 'W':
						Toast.novamsg(message);
						break;
						
					case 'E':
						Toast.erro(message);
						break;
					default:
						break;
					}
                	
                }
                return response;
            }
        };
    });
