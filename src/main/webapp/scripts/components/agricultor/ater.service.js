'use strict';

angular.module('projetocartaoApp')
    .factory('TecnicosAssociadosPorAgricultor', function ($resource) {
        return $resource('api/ater/tecnico/:agricultorId', {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('AtendimentosPorAgricultor', function ($resource) {
        return $resource('api/ater/atendimento', {}, {
            'query': {method: 'GET',isArray: true}
        });
    })
    .factory('IndicadoresPorAtendimento', function ($resource) {
        return $resource('api/ater/atendimento/indicadores/:atendimentoId', {}, {
        'get': {
            method: 'GET',
            isArray: true
        }
    });
});
