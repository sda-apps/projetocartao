'use strict';

angular.module('projetocartaoApp')
    
    .factory('AgricultorAter', function ($resource) {
        return $resource('assets/json/ater.json', {}, {
            'get': {
                method: 'GET',
                async: true,
                crossDomain: true,
                isArray: true
            }
        });
    });