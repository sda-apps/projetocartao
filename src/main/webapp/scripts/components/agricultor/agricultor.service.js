'use strict';

angular.module('projetocartaoApp')
    .factory('AgricultorBusca', function ($resource) {
        return $resource('api/agricultores/filtro', {}, {
            'query': {method: 'GET'}
        });
    })
    .factory('AgricultorBuscaTotal', function ($resource) {
        return $resource('api/agricultores/filtro/total', {}, {
            'query': {method: 'GET'}
        });
    })
    .factory('AgricultorCisterna', function ($resource) {
        return $resource('api/agricultor/cisterna/:agricultorId', {}, {
            'get': {
                method: 'GET'
            }
        });
    })
    .factory('Cisterna', function ($resource) {
        return $resource('api/cisterna/agricultor/:agricultorId', {}, {
            'get': {
                method: 'GET',
                isArray:true
            }
        });
    })
    .factory('AgricultorPoligono', function ($resource) {
        return $resource('api/agricultor/lote/poligono/:agricultorId', {}, {
            'get': {
                method: 'GET'
            }
        });
    })
    .factory('AgricultorAreaGeorreferenciada', function ($resource) {
        return $resource('api/agricultores/lotes/poligonos', {}, {
            'query': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('Agricultor', function ($resource) {
        return $resource('api/agricultor/:agricultorId', {}, {
            'get': {
                method: 'GET'
            }
        });
    })
    .factory('AgricultorProjetosPorAno', function ($resource) {
        return $resource('api/agricultor/projetos/:agricultorId/:agricultorProjetoAno', {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('AgricultorProjetos', function ($resource) {
        return $resource('api/agricultor/projetos/:agricultorId', {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('AgricultorProjetosProvider', function AgricultorProjetosProvider() {
        return {
            listaAgricultorProjetos: function(result) {
                var projetos = [];

                for (var aux = 0; aux< result.length; aux++) {
                    if (projetos[result[aux].projeto.codigoSda]) {
                        projetos[result[aux].projeto.codigoSda].ano = Math.min(projetos[result[aux].projeto.codigoSda].ano, result[aux].ano);
                        if (result[aux].ano === new Date().getFullYear()) {
                            projetos[result[aux].projeto.codigoSda].status = "Ativo";
                        }

                        projetos[result[aux].projeto.codigoSda].valorTotal += result[aux].valorTotal + 0;
                    } else {
                        projetos[result[aux].projeto.codigoSda] = ({
                            codigoSda:result[aux].projeto.codigoSda,
                            nome:result[aux].projeto.nome,
                            abreviacao: result[aux].projeto.abreviacao,
                            status: result[aux].ano === new Date().getFullYear()? "Ativo" : "Inativo",
                            ano:result[aux].ano,
                            valorTotal: (result[aux].valorTotal + 0)
                        });
                    }
                }

                var projetosOrdenados = [];

                for (var aux in projetos) {
                    if (projetos[aux].status.localeCompare("Ativo") === 0 ) {
                        projetosOrdenados.unshift(projetos[aux]);
                    } else {
                        projetosOrdenados.push(projetos[aux]);
                    }
                }

                return projetosOrdenados;
            }
        };
    })
    .factory('NotasProjetos', function ($resource) {
        return $resource('api/agricultor/notas-projetos/:agricultorId/:projetoCodigoSDA', {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('AgricultorFamilia', function ($resource) {
        return $resource('api/agricultor/familia/:agricultorId', {}, {
            'get': {
                method: 'GET'
            }
        });
    })
    .factory('AgricultorDaps', function ($resource) {
        return $resource('api/agricultor/daps/:agricultorId', {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('AgricultorProjetosPorProjeto', function ($resource) {
        return $resource('api/projeto/agricultor-projeto/:agricultorId/:projetoCodigoSDA', {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    })
    .factory('AgricultorEtnia', function ($resource) {
    	return $resource('api/agricultor/etnia', {}, {
	        'get': {
	            method: 'GET',
	            isArray: true
	        }
	    });
	})
    .factory('Agricultor', function ($resource) {
        return $resource('api/agricultor/:id', {id: '@id'}, {
            'update': {
                method: 'PUT'
            }
        });
    })
    .factory('AgricultorJSON', function ($resource) {
        return $resource('assets/json/agricultor.json', {}, {
            'get': {
                method: 'GET',
                async: true,
                crossDomain: true,
                isArray: true
            }
        });
    })
    .factory('AgricultorCaju', function ($resource) {
        return $resource('api/agricultor/caju/:agricultorId', {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    });
