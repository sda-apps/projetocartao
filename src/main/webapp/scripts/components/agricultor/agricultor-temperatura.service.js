'use strict';

angular.module('projetocartaoApp')
    
    .factory('AgricultorTemperatura', function ($resource) {
        return $resource('http://api.openweathermap.org/data/2.5/weather?q=:municipioNome,br&APPID=013cc58fabaf6718eb8e34bcd66432b6', {}, {
            'get': {
                method: 'GET',
                async: true,
                crossDomain: true
            }
        });
    });