'use strict';

angular.module('projetocartaoApp')
	.directive('linhaTempo', function() {
		return {
	    	restrict: 'E',
	    	scope: {
		    	eventos: '=',
		    	clickEvento: '&'
		    },
	    	templateUrl: 'scripts/components/linha-tempo/linha-tempo.html'
	  	};
	});