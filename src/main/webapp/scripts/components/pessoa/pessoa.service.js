'use strict';

angular.module('projetocartaoApp')
    .factory('Pessoa', function ($resource) {
        return $resource('api/pessoa/:id', {id: '@id'}, {
            'update': {
                method: 'PUT'
            }
        });
    })
    .factory('DadosBancarios', function ($resource) {
        return $resource('api/pessoa/dados-bancarios/:pessoaId', {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    });
