'use strict';

angular.module('projetocartaoApp')
	.factory('Toast', function ($mdToast) {
		return{
			sucesso:function(mensagem){
	        	$mdToast.show({
				    templateUrl:'scripts/components/toast/toast.html',
				    locals:{
				        msg:mensagem,
				        tipo:'sucesso'
				    },
				    hideDelay:8000,
				    position: "bottom right",
				    controller:
				    function($scope, msg, tipo){
				        $scope.msg=msg;
				        $scope.tipo=tipo;
				    }
	       		})
			},

			alerta:function(mensagem){
	        	$mdToast.show({
				    templateUrl:'scripts/components/toast/toast.html',
				    locals:{
				        msg:mensagem,
				        tipo:'alerta'
				    },
				    hideDelay:8000,
				    position: "bottom right",
				    controller:
				    function($scope, msg, tipo){
				        $scope.msg=msg;
				        $scope.tipo=tipo;
				    }
	       		})
			},

			novamsg:function(mensagem){
	        	$mdToast.show({
				    templateUrl:'scripts/components/toast/toast.html',
				    locals:{
				        msg:mensagem,
				        tipo:'novamsg'
				    },
				    hideDelay:8000,
				    position: "bottom right",
				    controller:
				    function($scope, msg, tipo){
				        $scope.msg=msg;
				        $scope.tipo=tipo;
				    }
	       		})
			},

			erro:function(mensagem){
	        	$mdToast.show({
				    templateUrl:'scripts/components/toast/toast.html',
				    locals:{
				        msg:mensagem,
				        tipo:'erro'
				    },
				    hideDelay:8000,
				    position: "bottom right",
				    controller:
				    function($scope, msg, tipo){
				        $scope.msg=msg;
				        $scope.tipo=tipo;
				    }
	       		})
			},

            info:function(mensagem){
                $mdToast.show({
                    templateUrl:'scripts/components/toast/toast.html',
                    locals:{
                        msg:mensagem,
                        tipo:'info'
                    },
                    hideDelay:8000,
                    position: "bottom right",
                    controller:
                        function($scope, msg, tipo){
                            $scope.msg=msg;
                            $scope.tipo=tipo;
                        }
                })
            }
    	};
	});
