'use strict';

angular.module('projetocartaoApp').factory('AuthPowerBI', function ($resource) {
    return $resource('api/powerbi/:id', {id: '@id'}, {
        'login': {method: 'GET'}
    });
});
