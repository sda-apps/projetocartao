'use strict';

angular.module('projetocartaoApp')
    .factory('SignupFinish', function ($resource) {
        return $resource('api/account/signup/finish', {}, {
        });
    });