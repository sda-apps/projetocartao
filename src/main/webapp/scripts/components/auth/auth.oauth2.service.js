'use strict';

angular.module('projetocartaoApp')
    .factory('AuthServerProvider', function loginService($http, $rootScope, localStorageService) {
        return {
            login: function(usuario) {
                var data = "username=" +  encodeURIComponent(usuario.username) + "&password="
                    + encodeURIComponent(usuario.senha) +
                    "&grant_type=password&scope=read%20write&" +
                    "client_secret=D&t$qqY4z2zhqfZg+VRZM6_&client_id=projetocartaoweb";
                return $http.post('oauth/token', data, {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded",
                        "Accept": "application/json",
                        "Authorization": "Basic " + btoa("projetocartaoweb" + ':' + "D&t$qqY4z2zhqfZg+VRZM6_")
                    }
                }).success(function (response) {
                    var expiredAt = new Date();
                    expiredAt.setSeconds(expiredAt.getSeconds() + response.expires_in);
                    response.expires_at = expiredAt.getTime();
                    localStorageService.set('token', response);
                    return response;
                });
            },
            logout: function() {
                // logout from the server
                $http.post('api/logout').then(function() {
                    localStorageService.clearAll();
                });
            },
            getToken: function () {
                return localStorageService.get('token');
            },
            hasValidToken: function () {
                var token = this.getToken();
                return token && token.expires_at && token.expires_at > new Date().getTime();
            }
        };
    });
