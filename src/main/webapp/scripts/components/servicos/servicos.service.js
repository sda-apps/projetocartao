'use strict';

angular.module('projetocartaoApp')
	.factory('ServicosExtrato', function ($http, $window) {
		return {
			getExtratoPrograma:function(url, agricultorId) {
                //var newWindow = $window.open('about:blank', '_blank');

                var newWindow = $window.open('#/loading', '_blank');

	        	$http.get(url + agricultorId, {responseType:'arraybuffer'})
					.success(function (response) {
						var file = new Blob([response], {type: 'application/pdf'});
						var fileURL = URL.createObjectURL(file);

                        newWindow.location.href = fileURL;
					});
			}
		};
	});
