'use strict';

angular.module('projetocartaoApp')
    .controller('CadastrarLocalidadeController', function ($rootScope, $filter, $scope, $mdDialog, localidadeToEdit, DateUtils, Toast, Localidade, $http, Principal, rolesAccess) {
        $scope.hasLocalidade = false;

        if (localidadeToEdit) {
            $scope.title = "Editar Localidade";
            $scope.localidade = localidadeToEdit;
            $scope.municipioSelecionado = localidadeToEdit.distrito.municipio;
            $scope.distritoSelecionado = localidadeToEdit.distrito;
            $scope.hasLocalidade = true;
        } else {
            $scope.title = "Cadastrar Localidade"
        }

        $scope.podeEditar = function () {
            return Principal.hasAnyAuthority(rolesAccess['LOCALIDADE'].editar);
        };

        $scope.buscarAutoCompletePorFiltro = function (searchText, url, filtro) {
            try {
                var search;
                if (filtro == null) {
                    search = url + searchText
                } else {
                    search = url + filtro.id + "/" + searchText;
                }

                var data = $http({
                    method: 'GET',
                    url: search
                }).then(function successCallback(response) {
                    return response.data;
                }, function errorCallback(response) {
                    return response.data;
                });

                return data;
            } catch (e) {
                console.log(e);
            }
        };

        $scope.salvar = function () {
            if ($scope.hasLocalidade) {
                $scope.localidade.distrito = $scope.distritoSelecionado;
                $scope.localidade.distrito.municipio = $scope.municipioSelecionado;
                Localidade.update($scope.localidade).$promise.then(function (result) {
                    Toast.sucesso("Localidade atualizada com sucesso.");
                    $mdDialog.cancel();
                }).catch(function (erro) {
                    Toast.erro("Erro ao atualizar localidade.");
                });
            } else {
                $scope.localidade.distrito = $scope.distritoSelecionado;
                $scope.localidade.distrito.municipio = $scope.municipioSelecionado;
                Localidade.save($scope.localidade).$promise.then(function (result) {
                    Toast.sucesso("Localidade salva com sucesso.");
                    $mdDialog.cancel();
                }).catch(function (error) {
                    if (error.data.messages) {
                        Toast.erro(error.data.messages[0]);
                    } else {
                        Toast.erro("Erro ao adicionar localidade.");
                    }
                });
            }
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
    });
