'use strict';

angular.module('projetocartaoApp')
    .controller('ContratoController', function ($scope, $http, $state, ContratoService, $mdDialog, Entidade, contrato, $filter, DateUtils, Principal, rolesAccess,
                                                Territorio, Toast, Regiao, entidade, projetosEnum, AssociacaoComunitaria) {
        $scope.regioes = [];
        $scope.contrato = {};
        $scope.entidade = entidade;
        $scope.projetos = projetosEnum;
        $scope.associacoes = [];

        $scope.podeEditar = function () {
            if(contrato) {
                return Principal.hasAnyAuthority(rolesAccess['CONTRATO'].editar);
            } else {
                return true;
            }
        };

        Regiao.query({}, function (result) {
            $scope.regioes = result;
        });

        AssociacaoComunitaria.findAll({}, function (result) {
            $scope.associacoes = result;
        });

        if(contrato !== undefined){
            var valor = contrato.valor.toFixed(2);
            $scope.contrato = contrato;
            $scope.contrato.valor = valor;

            $scope.dataAssinatura = $filter('date')($scope.contrato.dataAssinatura, 'dd/MM/yyyy');
            $scope.dataEncerramento = $filter('date')($scope.contrato.dataEncerramento, 'dd/MM/yyyy');
        } else {
            $scope.contrato.abrangencia = [];
        }

        $scope.$watch('contrato.projeto', function(contrato) {
            if(contrato != "2") {
                $scope.contrato.associacaoComunitaria = null;
            }
        });

        $scope.buscarAutoCompletePorFiltro = function(searchText, url, filtro) {
            var search;
            if (filtro == null){
                search = url + searchText
            }else{
                search = url + searchText +"/"+filtro;
            }

            var data = $http({
                method: 'GET',
                url: search
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                return response.data;
            });

            return data;
        };

        $scope.cancel = function (){
            $mdDialog.cancel();
        };

        $scope.verificaBloqueioRegiao = function (bloqueioRegiao) {
            if(bloqueioRegiao) {
                Toast.info("Para alterar região exclua os municípios adicionados.")
            }
        };

        $scope.save = function() {
            if($scope.dataAssinatura) {
                var dataFormatada = DateUtils.formatDate($scope.dataAssinatura);
                var dataConvertida = DateUtils.convertFormattedDate(dataFormatada);
                $scope.contrato.dataAssinatura = DateUtils.convertLocaleDateToServer(dataConvertida);
            }

            if($scope.dataEncerramento) {
                dataFormatada = DateUtils.formatDate($scope.dataEncerramento);
                dataConvertida  = DateUtils.convertFormattedDate(dataFormatada);
                $scope.contrato.dataEncerramento =  DateUtils.convertLocaleDateToServer(dataConvertida);
            }

            $scope.contrato.empresa = { id: $scope.entidade.id };

            if($scope.contrato.dataAssinatura > $scope.contrato.dataEncerramento) {
                Toast.alerta("Data de assinatura é maior que data de encerramento do contrado.");
            } else if($scope.contrato.dataAssinatura && parseInt($scope.contrato.dataAssinatura.substr(0,4),10) > new Date().getFullYear()) {
                Toast.alerta("Data de assinatura possui o ano maior que o atual.");
            } else {
                if($scope.contrato.valor && $scope.contrato.valor.length > 2 && $scope.contrato.valor.match(/\./g) == null) {
                    $scope.contrato.valor = $scope.contrato.valor.substring(0, ($scope.contrato.valor.length-2)) + "." + $scope.contrato.valor.substring(($scope.contrato.valor.length-2));
                }

                if(contrato != undefined) {
                    ContratoService.update($scope.contrato).$promise.then(function (result) {
                        Toast.sucesso("Contrato atualizado com sucesso.");
                        $mdDialog.hide(true);
                    }).catch(function (error) {
                        console.log(error);
                        if(error.data.messages) {
                            Toast.erro(error.data.messages[0]);
                        } else {
                            Toast.erro("Erro ao atualizado contrato.");
                        }
                    });

                } else {
                    ContratoService.save($scope.contrato).$promise.then(function (result) {
                        Toast.sucesso("Contrato salvo com sucesso.");
                        $mdDialog.hide(true);
                    }).catch(function (error) {
                        console.log(error);
                        if(error.data.messages) {
                            Toast.erro(error.data.messages[0]);
                        } else {
                            Toast.erro("Erro ao salvar contrato.");
                        }
                    });
                }
            }

        }
    });
