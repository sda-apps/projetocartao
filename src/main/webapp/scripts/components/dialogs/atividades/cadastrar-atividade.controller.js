'use strict';

angular.module('projetocartaoApp')
    .controller('CadastrarAtividadeController', function (
        $rootScope, $filter, $scope, $mdDialog, atividadeToEdit, DateUtils, Toast, Atividade, $http, Acao,
        Principal, rolesAccess
    ) {
        $scope.hasAtividade = false;

        if (atividadeToEdit){
            $scope.title = "Editar Atividade";
            $scope.atividade = atividadeToEdit;
            $scope.hasAtividade = true;
        } else{
            $scope.title = "Cadastrar Atividade"
        }

        $scope.podeEditar = function () {
            return Principal.hasAnyAuthority(rolesAccess['ATIVIDADES'].editar);
        };

        Acao.query({}, function (result) {
            $scope.acoes = result;
        });

        $scope.buscarAutoCompletePorFiltro = function(searchText, url, filtro) {
            try {
                var search;
                if (filtro == null){
                    search = url + searchText
                } else{
                    search = url + filtro.id +"/"+ searchText;
                }

                var data = $http({
                    method: 'GET',
                    url: search
                }).then(function successCallback(response) {
                    return response.data;
                }, function errorCallback(response) {
                    return response.data;
                });

                return data;
            } catch (e) {
                console.log(e);
            }
        };

        $scope.salvar = function () {
            if($scope.hasAtividade) {
                Atividade.update($scope.atividade).$promise.then(function (result) {
                    Toast.sucesso("Atividade atualizada com sucesso.");
                    $mdDialog.cancel();
                }).catch(function (erro) {
                    Toast.erro("Erro ao atualizar atividade.");
                });
            } else {
                Atividade.save($scope.atividade).$promise.then(function (result) {
                    Toast.sucesso("Atividade salva com sucesso.");
                    $mdDialog.cancel();
                }).catch(function (error) {
                    if(error.data.messages){
                        Toast.erro(error.data.messages[0]);
                    }else {
                        Toast.erro("Erro ao adicionar atividade.");
                    }
                });
            }
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };
    });
