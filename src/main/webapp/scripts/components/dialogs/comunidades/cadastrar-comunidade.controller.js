'use strict';

angular.module('projetocartaoApp')
    .controller('CadastrarComunidadesController', function ($scope, $rootScope, $http, $mdDialog, Toast, Comunidade, currentComunidade, TiposDeComunidadeEnum, rolesAccess, Principal) {
        $scope.comunidade = {};
        $scope.comunidade.localidades = [];
        $scope.tiposDeComunidade = TiposDeComunidadeEnum;

        if (currentComunidade) {
            $scope.comunidade = currentComunidade;
            $scope.dialogTitle = "Editar Comunidades"
        } else {
            $scope.dialogTitle = "Cadastrar Comunidades"
        }

        $scope.podeIncluir = function () {
            return Principal.hasAnyAuthority(rolesAccess['COMUNIDADE'].incluir);
        };

        $scope.podeEditar = function () {
            return Principal.hasAnyAuthority(rolesAccess['COMUNIDADE'].editar);
        };

        $scope.podeExcluir = function () {
            return Principal.hasAnyAuthority(rolesAccess['COMUNIDADE'].excluir);
        };

        $scope.buscarAutoCompletePorFiltro = function (searchText, url, filtro) {
            var search;

            if (!filtro) {
                search = url + searchText;
            } else {
                search = url + filtro.id + "/" + searchText;
            }
            return $http.get(search).then(function (retorno) {
                return retorno.data;
            });
        };

        $scope.salvar = function () {
            if ($scope.comunidade.id) {
                Comunidade.update($scope.comunidade).$promise.then(function (result) {
                    Toast.sucesso("Comunidade " + $scope.comunidade.nome + " atualizada com sucesso.");
                    $mdDialog.hide(result);
                }).catch(function (error) {
                    console.log(error);
                    Toast.erro("Erro ao atualizar comunidade.");
                });

            } else {
                Comunidade.save($scope.comunidade).$promise.then(function (result) {
                    Toast.sucesso("Comunidade " + $scope.comunidade.nome + " salva com sucesso.");
                    $mdDialog.hide(result);
                }).catch(function (error) {
                    console.log(error);
                    Toast.erro("Erro ao salvar entidade parceira.");
                });
            }
        };

        if (currentComunidade && currentComunidade.id) {
            getComunidade();
        }

        function getComunidade() {
            Comunidade.get({id: currentComunidade.id}, function (result) {
                $scope.comunidade = result;
                if ($scope.comunidade.nome) {
                    $scope.subToolbar.title = $scope.ecomunidade.nome;
                } else {
                    $scope.subToolbar.title = "Edição Comunidade";
                }
            });
        }

        $scope.verificaBloqueio = function (bloqueio, nome) {
            if (bloqueio) {
                Toast.info("Para alterar " + nome + " exclua as localidades adicionadas.")
            }
        };

        $scope.validatingMunicipioAndDistritoFields = function () {
            if (!$scope.comunidade.municipio && !$scope.comunidade.distrito) {
                Toast.alerta("É preciso primeiro informar os campos município e distrito");
            } else if (!$scope.comunidade.municipio) {
                Toast.alerta("É preciso primeiro informar o campo município");
            } else if (!$scope.comunidade.distrito) {
                Toast.alerta("É preciso primeiro informar o campo distrito");
            }
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

    });
