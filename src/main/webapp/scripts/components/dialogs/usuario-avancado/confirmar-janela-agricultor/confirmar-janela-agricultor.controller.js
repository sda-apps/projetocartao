'use strict';

angular.module('projetocartaoApp')
    .controller('dialogConfirmarJanelaAgricultorController', function ($scope, $window, $mdDialog, agricultorConfirmarDialog, $state) {
        $scope.agricultor = agricultorConfirmarDialog;
        $scope.abrirNovaTab = function () {
            var newWindow = $window.open($state.href('main-agricultor'), '_blank');
            newWindow.agricultor = agricultorConfirmarDialog;
            $mdDialog.cancel();
        };
        $scope.cancel = function () {
            $mdDialog.cancel();
        }
    });
