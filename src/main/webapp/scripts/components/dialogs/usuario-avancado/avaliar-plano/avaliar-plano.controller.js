'use strict';

angular.module('projetocartaoApp')
    .controller('AvaliarPlanoController', function ($scope, $rootScope, plano, historico, tipoAvaliacao,
                                                    titleWindow, $mdDialog, identificadorRoles,
                                                    Toast, HistoricoAvaliacaoPlanos) {

        $scope.dialogTitle = titleWindow;
        $scope.plano = plano;
        $scope.historico = historico && historico[0] && historico[0].motivo === 'E' ? historico[0] : {};
        $scope.disableButtons = false;

        $scope.save = function () {
            $scope.disableButtons = true;
            if ($scope.historico.motivo === 'A') {
                updateToStatusApproved();
            } else if ($scope.historico.motivo === 'R') {
                updateToStatusRecused();
            } else if ($scope.historico.motivo === 'E') {
                updateToStatusInProgress();
            } else {
                Toast.alerta("Informe o status da avaliação do relatório.");
                $scope.disableButtons = false;
                return
            }

            buildAndUpdateHistoric();
        };

        function updateToStatusApproved() {
            if(tipoAvaliacao === 'R') {
                $scope.plano.fileStatus = 'A';
                $scope.historico.fileStatus = 'A';
            } else {
                if (identificadorRoles.isUsuarioERP($rootScope.account.authorities)) {
                    $scope.plano.status = "4"
                } else if (identificadorRoles.isUsuarioUGP($rootScope.account.authorities) || identificadorRoles.isAdmin($rootScope.account.authorities)) {
                    $scope.plano.status = "6";
                    $scope.plano.recusado = false;
                }
            }
        }

        function updateToStatusRecused() {
            if (!$scope.historico.avaliacao) {
                return
            }

            if(tipoAvaliacao === 'R') {
                $scope.plano.fileStatus = 'R';
                $scope.historico.fileStatus = 'R';
            } else {
                $scope.plano.status = "1";
                $scope.plano.recusado = true;
            }

        }

        function updateToStatusInProgress() {
            if(tipoAvaliacao === 'R') {
                $scope.plano.fileStatus = 'E';
                $scope.historico.fileStatus = 'E';
            } else {
                if (identificadorRoles.isUsuarioERP($rootScope.account.authorities)) {
                    $scope.plano.status = "3"
                } else if (identificadorRoles.isUsuarioUGP($rootScope.account.authorities)
                    || identificadorRoles.isAdmin($rootScope.account.authorities)) {
                    $scope.plano.status = "5"
                }
            }
        }

        function buildAndUpdateHistoric() {
            $scope.historico.id = undefined;
            var plano = {id: $scope.plano.id, status: $scope.plano.status, recusado: $scope.plano.recusado};
            if (tipoAvaliacao === 'A') {
                $scope.historico.planoTrabalhoAnual = plano;
            } else if (tipoAvaliacao === 'T') {
                $scope.historico.planoTrimestral = plano;
            } else if (tipoAvaliacao === 'R') {
                $scope.historico.planoTrimestral = plano;
                $scope.historico.planoTrimestral.fileStatus = $scope.plano.fileStatus;
                $scope.historico.objeto = $scope.plano.id;
                $scope.historico.tipo = 'F';
            }

            updateHistoric();
        }

        function updateHistoric() {
            HistoricoAvaliacaoPlanos.save($scope.historico).$promise.then(function (response) {
                updateHistoricSuccess(response);
            }).catch(function (error) {
                requestError(error);
            })
        }

        function updateHistoricSuccess(historico) {
            $mdDialog.hide({
                status: $scope.plano.status,
                recusado: $scope.plano.recusado,
                historicInserted: historico,
                fileStatus: $scope.plano.fileStatus
            });
            Toast.sucesso("Avaliação realizada com sucesso.");
        }

        function requestError(error) {
            $scope.disableButtons = false;
            console.log(error);
        }

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

    });
