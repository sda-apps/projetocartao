'use strict';

angular.module('projetocartaoApp')
    .controller('PlanoTrimestralController', function ($scope, $mdDialog, params, Principal, rolesAccess,
                                                       planoTrimestralConstants, PlanoTrimestral, ComunidadesPorMunicipio,
                                                       Toast, $timeout, RelatorioPlanoTrimestal, $filter, Uf,
                                                       MunicipioPorUf, ComunidadesPorMunicipioEPta) {
        $scope.loading = true;
        $scope.exibir = [];
        $scope.planoTrimestralConstants = planoTrimestralConstants;
        $scope.meses = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
        $scope.planoTrimestral = {status: '1'};
        $scope.isUpdate = false;
        $scope.trimestre = 0;
        $scope.trimestreDescricao = "";
        $scope.municipioAux = null;
        $scope.ufAux = null;

        $scope.abrangencias = [
            {id: '1', desc: 'Comunitária/Local'},
            {id: '2', desc: 'Municipal'},
            {id: '3', desc: 'Intermunicipal'},
            {id: '4', desc: 'Regional'},
            {id: '5', desc: 'Nacional'},
            {id: '6', desc: 'Internacional'}];

        $scope.podeEditarPT = function () {
            if ($scope.isUpdate) {
                return Principal.hasAnyAuthority(rolesAccess['PTA_PLANO_TRIMESTRAL'].editar);
            } else {
                return true;
            }
        };

        $scope.podeFinalizarPT = function () {
            return Principal.hasAnyAuthority(rolesAccess['PTA_PLANO_TRIMESTRAL'].finalizar);
        };

        Uf.query({}, function (result) {
            $scope.uf = result;
        });

        $scope.buscarComunidadePorMunicipio = function (municipio, finishLoading) {
            ComunidadesPorMunicipioEPta.get({municipio: municipio.id, ptaId: $scope.pta.id}).$promise.then(function (result) {
                municipio.comunidades = result;
                if (finishLoading) {
                    $scope.loading = false;
                }
            }).catch(function (error) {
                $scope.loading = false;
                municipio.comunidades = [];
            });
        };

        $scope.buscarMunicipioPorUf = function (uf, finishLoading) {
            MunicipioPorUf.query({uf: uf.id}).$promise.then(function (result) {
                uf.municipios = result;
                if (finishLoading) {
                    $scope.loading = false;
                }
            }).catch(function (error) {
                $scope.loading = false;
            });
        };

        $timeout(function () {
            if (params) {
                $scope.pta = params[0];
                $scope.selectTrimestres = params[1];
                $scope.selectMesesTrimestres = [];
                $scope.trimestre = $scope.pta.planosTrimestrais ? $scope.pta.planosTrimestrais.length + 1 : 1;

                $scope.selectMesesTrimestres = $scope.selectTrimestres.map(function (item) {
                    return item.map(function (item) {
                        if (item.mes) return item.mes;
                    });
                });

                if (params[3]) {
                    var metasTrimestrais = $filter('orderBy')(params[3], 'atividade.nome', false);

                    $scope.trimestreSelecionado = Number(params[2].trimestre) - 1;
                    $scope.planoTrimestral = params[2];
                    $scope.isUpdate = true;
                    $scope.trimestre = $scope.trimestreSelecionado;

                    for (var mt in metasTrimestrais) {
                        var metaTrimestral = metasTrimestrais[mt];

                        if (metaTrimestral.id) {
                            if (metaTrimestral.municipio) {
                                var finishLoading = metasTrimestrais.length === Number(mt) + 1;

                                $scope.loading = false;
                            }

                            for (var map in $scope.pta.metasAtividadesPta) {
                                var metaAnual = $scope.pta.metasAtividadesPta[map];

                                if (!metaAnual.metasAtividadesTrimestral) {
                                    metaAnual.metasAtividadesTrimestral = [];
                                }

                                if (metaAnual.id && metaAnual.id === metaTrimestral.metaAtividadesPTA.id) {
                                    metaAnual.metasAtividadesTrimestral.push(metaTrimestral);
                                }

                            }
                        }
                    }

                    $scope.trimestreDescricao = ($scope.trimestre + 1) + 'º trimestre (' + $scope.selectMesesTrimestres[Number(params[2].trimestre) - 1].join('/') + ')';
                } else {
                    $scope.trimestreDescricao = $scope.trimestre + 'º trimestre (' + $scope.selectMesesTrimestres[Number($scope.trimestre) - 1].join('/') + ')';
                    $scope.loading = false;
                }
            }
        }, 100);

        $scope.cancel = function () {
            $mdDialog.hide();
        };

        $scope.submit = function (isFinishing) {
            if ($scope.planoTrimestral.id) {
                updateMetasOnPlanoTrimestral(isFinishing);
            } else {
                saveNewPlanoTrimestral(isFinishing);
            }
        };

        function saveNewPlanoTrimestral(isFinishing) {
            var metasTrimestrais = [];

            var planoTrimestral = {
                trimestre: $scope.trimestre,
                primeiroMes: new Date($scope.selectTrimestres[$scope.trimestre - 1][0].ano, $scope.selectTrimestres[$scope.trimestre - 1][0].mesNum, "01"),
                terceiroMes: new Date($scope.selectTrimestres[$scope.trimestre - 1][2].ano, $scope.selectTrimestres[$scope.trimestre - 1][2].mesNum, "01"),
                status: '1',
                planoTrabalhoAnual: {id: $scope.pta.id},
                recusado: false
            };

            metasTrimestrais = buildMetasTrimestrais(planoTrimestral, metasTrimestrais);

            if (metasTrimestrais) {
                PlanoTrimestral.save(metasTrimestrais).$promise.then(function (response) {
                    updateLocalPlanoTrimestral(response);
                }).catch(function (error) {
                    var subText = isFinishing ? "finalizar" : "salvar";
                    Toast.erro("Ocorreu algum problema ao " + subText + " o plano");
                    console.log(error);
                });
            }
        }

        function updateMetasOnPlanoTrimestral(isFinishing) {
            var metasTrimestrais = [];

            $scope.planoTrimestral.planoTrabalhoAnual = {id: $scope.pta.id};

            var dto = {
                id: $scope.planoTrimestral.id,
                planoTrimestral: $scope.planoTrimestral,
                metasTrimestrais: buildMetasTrimestrais($scope.planoTrimestral, metasTrimestrais)
            };

            if (dto.metasTrimestrais) {
                PlanoTrimestral.update(dto).$promise.then(function (response) {
                    if (isFinishing) {
                        Toast.sucesso("Plano trimestral finalizado com sucesso!");
                    } else {
                        Toast.sucesso("Metas trimestrais atualizadas com sucesso!");
                    }
                    updateLocalPlanoTrimestral(response);
                }).catch(function (error) {
                    console.log(error);
                    planoTrimestralUpdateError(error);
                });
            }
        }

        function updateLocalPlanoTrimestral(planoTrimestral) {
            if (planoTrimestral) {
                $scope.planoTrimestral = planoTrimestral;
            }
        }

        function planoTrimestralUpdateError(error) {
            if(error.data.messages) {
                Toast.alerta(error.data.messages[0]);
            } else {
                if (error.status === 404) {
                    Toast.alerta("Nenhuma meta trimestral foi inserida para esse trimestre.");
                } else if (error.status === 409) {
                    Toast.erro("Trimestre já cadastrado.");
                } else {
                    Toast.erro("Ocorreu algum problema inesperado");
                }
            }
        }

        function buildMetasTrimestrais(planoTrimestral, metasTrimestrais) {
            try {
                for (var metaAnual in $scope.pta.metasAtividadesPta) {
                    var metasAtividadesPta = angular.copy($scope.pta.metasAtividadesPta[metaAnual]);
                    var totalMetasTrimestrais = $scope.getTotalMetasAtividadesTrimestral(metasAtividadesPta.metasAtividadesTrimestral);

                    if (totalMetasTrimestrais > metasAtividadesPta.quantidadeMeta) {
                        Toast.alerta("O total das metas trimestrais não pode ser maior do que a meta anual.");
                        return false;
                    }

                    $scope.getTotalMetasAtividadesTrimestral(metasAtividadesPta.metasAtividadesTrimestral);

                    for (var metaTrimestral in metasAtividadesPta.metasAtividadesTrimestral) {

                        if (!metasAtividadesPta.metasAtividadesTrimestral[metaTrimestral].municipio.id
                            || metasAtividadesPta.metasAtividadesTrimestral[metaTrimestral].quantidadeMeta <= 0) {
                            Toast.alerta("Preencha corretamente todas as informações da meta trimestral.");

                            return false;
                        }

                        metasAtividadesPta.metasAtividadesTrimestral[metaTrimestral]['metaAtividadesPTA'] = {id: $scope.pta.metasAtividadesPta[metaAnual].id};
                        metasAtividadesPta.metasAtividadesTrimestral[metaTrimestral]['planoTrimestral'] = planoTrimestral;
                        metasAtividadesPta.metasAtividadesTrimestral[metaTrimestral]['atividade'] = metasAtividadesPta.atividade;

                        delete metasAtividadesPta.metasAtividadesTrimestral[metaTrimestral].municipio.comunidades;

                        if (metasAtividadesPta.metasAtividadesTrimestral[metaTrimestral].comunidade && metasAtividadesPta.metasAtividadesTrimestral[metaTrimestral].comunidade.id == null) {
                            metasAtividadesPta.metasAtividadesTrimestral[metaTrimestral].comunidade = null;
                        }

                        metasTrimestrais.push(metasAtividadesPta.metasAtividadesTrimestral[metaTrimestral]);
                    }
                }

                if (metasTrimestrais.length === 0) {
                    Toast.alerta("Informe pelo menos uma meta trimestral.");
                }
            } catch(ex) {
                Toast.erro("Erro ao salvar o plano trimestral: " + ex + ".");
                console.log(ex);
            }


            return metasTrimestrais;
        }

        $scope.adicionarMetaTrimestral = function (indexMetaAtividade) {
            if (!$scope.pta.metasAtividadesPta[indexMetaAtividade].metasAtividadesTrimestral) {
                $scope.pta.metasAtividadesPta[indexMetaAtividade].metasAtividadesTrimestral = [];
            }

            var lengthMetas = $scope.pta.metasAtividadesPta[indexMetaAtividade].metasAtividadesTrimestral.length;

            var objectCeara = $scope.uf.filter(function (item) {
                return item.sigla == "CE";
            });

            if(lengthMetas == 0) {
                $scope.pta.metasAtividadesPta[indexMetaAtividade].metasAtividadesTrimestral.push({
                    municipio: {uf: {id:objectCeara[0].id}},
                    comunidade: {},
                    quantidadeMeta: 0
                });
                var indexUltimoObj = $scope.pta.metasAtividadesPta[indexMetaAtividade].metasAtividadesTrimestral.length-1;

                $scope.aposSelecionarUf($scope.pta.metasAtividadesPta[indexMetaAtividade].metasAtividadesTrimestral[indexUltimoObj].municipio.uf);
            } else {
                var novaMeta = angular.copy($scope.pta.metasAtividadesPta[indexMetaAtividade].metasAtividadesTrimestral[lengthMetas-1]);
                novaMeta.id = null;
                novaMeta.comunidade = {};

                $scope.pta.metasAtividadesPta[indexMetaAtividade].metasAtividadesTrimestral.push(novaMeta);

                $scope.aposSelecionarUf($scope.pta.metasAtividadesPta[indexMetaAtividade].metasAtividadesTrimestral[lengthMetas-1].municipio.uf);
            }
        };

        $scope.removerMetaTrimestral = function (indexMetaTrimestral, indexMetaAtividade) {
            $scope.pta.metasAtividadesPta[indexMetaAtividade].metasAtividadesTrimestral.splice(indexMetaTrimestral, 1);
        };

        $scope.expansaoTrimestre = function (id) {
            $scope.exibir[id] = !$scope.exibir[id];
        };

        $scope.getTotalMetasAtividadesTrimestral = function (metasAtividadesTrimestral) {
            var total = 0;

            angular.forEach(metasAtividadesTrimestral, function (meta) {
                total += Number(meta.quantidadeMeta);
            });

            return total;
        };

        $scope.aposSelecionarMunicipio = function (municipio) {
            if (!$scope.municipioAux) {
                $scope.municipioAux = municipio.id;
            } else {
                if (municipio.id !== $scope.municipioAux) {
                    $scope.municipioAux = municipio.id;
                    $scope.buscarComunidadePorMunicipio(municipio);
                }
            }
        };

        $scope.aposSelecionarUf = function (uf) {
            if (!$scope.ufAux) {
                $scope.ufAux = uf.id;
                $scope.buscarMunicipioPorUf(uf);
            } else {
                if (uf.id !== $scope.ufAux) {
                    $scope.ufAux = uf.id;
                }
                $scope.buscarMunicipioPorUf(uf);
            }
        };

        $scope.aposAbrirUf = function (uf) {
            if (uf) {
                $scope.ufAux = uf.id;
            }
        };

        $scope.finalizarPlano = function () {
            $scope.planoTrimestral.status = '2';
            $scope.submit(true);
        };

        $scope.printPlanoTrimestral = function () {
            RelatorioPlanoTrimestal.gerarRelatorioPlanoTrimestral('/api/servicos/relatorio/plano-trimestral/', $scope.planoTrimestral.id);
        };

        $scope.finalizarPlanoButtonVisibility = function () {
            return $scope.podeFinalizarPT() && $scope.planoTrimestral.status === '1' && $scope.planoTrimestral.id;
        };

        $scope.textCancelButton = function () {
            return $scope.planoTrimestral.status === '1' ? "Cancelar" : "Fechar";
        };

    });
