'use strict';

angular.module('projetocartaoApp')
    .controller('DetalhesAtividadeCadastroController', function ($scope, params, $mdDialog, grupoProdutivoEnum,
                                                                 grupoEtnicoEnum, racaEnum, parentescoEnum, atividadeEconomicaEnum,
                                                                 beneficioSocialEnum, tipoAposentadoriaEnum, telefoneEnum,
                                                                 modalidadePronafEnum, modalidadePaaEnum,
                                                                 categoriaHabitacaoRuralEnum, categoriaBanheiroPopularEnum,
                                                                 tipoHabitacaoEnum, situacaoMoradiaEnum,
                                                                 tipoEsgotamentoSanitarioEnum, qualEnergiaAlternativaEnum,
                                                                 temaEnum, orgaoEnum, oraganizacaoLocalEnum, situacaoFundiariaEnum, subsistemaEnum, localComercializacaoEnum, certificacaoEnum,
                                                                 AtividadeCadastroPoliticasPublicas, AtividadeCadastroProducao, AtividadeCadastroCapacitacao) {

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.atividadeCadastro = params;

        $scope.anoAtual = new Date().getFullYear();

        $scope.beneficiosSociaisList = [];
        $scope.atividadeCadastro.informacaoInicial.agricultor.beneficiosSociais.forEach(function (beneficio) {
            $scope.beneficiosSociaisList.push(beneficio.descricao);
        });
        $scope.beneficiosSociais = $scope.beneficiosSociaisList.join(", ");

        function buscarAcessoPoliticasPublicas() {
            AtividadeCadastroPoliticasPublicas.get({id: $scope.atividadeCadastro.acessoPoliticasPublicas.id}, function (result) {
                $scope.atividadeCadastro.acessoPoliticasPublicas = result;

                if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas) {
                    $scope.tiposBeneficiarioPaaList = [];
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.acessoPaaTipoProdutor) {
                        $scope.tiposBeneficiarioPaaList.push('Produtor');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.acessoPaaTipoReceptor) {
                        $scope.tiposBeneficiarioPaaList.push('Receptor');
                    }
                    $scope.tiposBeneficiarioPaa = $scope.tiposBeneficiarioPaaList.join(", ");

                    $scope.acessoEducacaoList = [];
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.acessoEducacaoComunidade) {
                        $scope.acessoEducacaoList.push("Escola na comunidade");
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.acessoEducacaoOutraComunidade) {
                        $scope.acessoEducacaoList.push("Desloca-se para outra comunidade");
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.acessoEducacaoMunicipio) {
                        $scope.acessoEducacaoList.push("Desloca-se para a sede do município");
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.acessoEducacaoOutroMunicipio) {
                        $scope.acessoEducacaoList.push("Desloca-se para outro município");
                    }
                    $scope.acessosEducacao = $scope.acessoEducacaoList.join(", ");

                    $scope.cisternasConsumoList = [];
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.beneficioCisternaPlaca) {
                        $scope.cisternasConsumoList.push("Placas");
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.beneficioCisternaPolietileno) {
                        $scope.cisternasConsumoList.push("Polietileno");
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.beneficioCisternaFerrocimento) {
                        $scope.cisternasConsumoList.push("Ferrocimento");
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.beneficioCisternaOutra) {
                        $scope.cisternasConsumoList.push("Outra");
                    }
                    $scope.cisternasConsumo = $scope.cisternasConsumoList.join(", ");

                    $scope.acessoSaudeList = [];
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.acessoSaudeComunidade) {
                        $scope.acessoSaudeList.push("Posto de saúde na comunidade");
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.acessoSaudeDistrito) {
                        $scope.acessoSaudeList.push("Posto de saúde na sede do distrito");
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.acessoSaudeMunicipio) {
                        $scope.acessoSaudeList.push("Posto de saúde na sede do município");
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.acessoSaudeOutroMunicipio) {
                        $scope.acessoSaudeList.push("Desloca-se para outro município");
                    }

                    $scope.acessosSaude = $scope.acessoSaudeList.join(", ");

                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.modalidadePronaf) {
                        var modalidadesPronaf = [
                            {id: 'A', desc: 'PRONAF A'},
                            {id: 'C', desc: 'PRONAF A/C'},
                            {id: 'B', desc: 'PRONAF B'},
                            {id: 'V', desc: 'PRONAF Variável'},
                            {id: 'J', desc: 'PRONAF Jovem'},
                            {id: 'M', desc: 'PRONAF Mulher'},
                            {id: 'S', desc: 'PRONAF Semi-Árido'},
                            {id: 'G', desc: 'PRONAF Agroecologia'},
                            {id: 'F', desc: 'PRONAF Florestal'}];
                        $scope.modalidadePronaf = findElementById(modalidadesPronaf, $scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.modalidadePronaf);
                    }

                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.modalidadesPaa) {
                        var modalidadesPaa = [
                            {id: 'S', desc: 'Compra da Agricultura Familiar para Doação Simultânea (MDS)'},
                            {id: 'D', desc: 'Compra Direta da Agricultura Familiar – CDAF (MDS/MDA)'},
                            {id: 'I', desc: 'Compra Institucional'}];
                        $scope.modalidadePaa = findElementById(modalidadesPaa, $scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.modalidadePaa);
                    }

                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.categoriaPaaLeite) {
                        var categorias = [
                            {id: 'B', desc: 'Bovino'},
                            {id: 'C', desc: 'Caprino'}];

                        $scope.categoriaPaaLeite = findElementById(categorias, $scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.categoriaPaaLeite);
                    }

                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.categoriaHabitacaoRural) {
                        var categorias = [
                            {id: 'F', desc: 'Federal'},
                            {id: 'E', desc: 'Estadual'},
                            {id: 'M', desc: 'Municipal'}];

                        $scope.categoriaHabitacaoRural = findElementById(categorias, $scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.categoriaHabitacaoRural);
                    }

                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.categoriaBanheiroPopular) {
                        var categorias = [
                            {id: 'F', desc: 'Federal'},
                            {id: 'E', desc: 'Estadual'},
                            {id: 'M', desc: 'Municipal'}];

                        $scope.categoriaBanheiroPopular = findElementById(categorias, $scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.categoriaBanheiroPopular);
                    }

                    $scope.projetoSaoJoseList = [];
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.saoJoseInfraestrutura) {
                        $scope.projetoSaoJoseList.push('Infraestrutura');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.saoJoseProdutivo) {
                        $scope.projetoSaoJoseList.push('Produtivo');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.saoJoseOutro) {
                        $scope.projetoSaoJoseList.push('Outro');
                    }
                    $scope.projetoSaoJose = $scope.projetoSaoJoseList.join(", ");

                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.tipoAposentadoria) {
                        var tipos = [
                            {id: 'R', desc: 'Rural'},
                            {id: 'V', desc: 'Vínculo empregatício'}];

                        $scope.tipoAposentadoria = findElementById(tipos, $scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.tipoAposentadoria);
                    }

                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.acessoCriancaEducacao) {
                        var acessos = [
                            {id: 'C', desc: 'Escola na comunidade'},
                            {id: 'O', desc: 'Deslocamento para outra comunidade'},
                            {id: 'M', desc: 'Deslocamento para a sede do município'}];

                        $scope.acessoCriancaEducacao = findElementById(acessos, $scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.acessoCriancaEducacao);
                    }

                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.acessoCriancaSaude) {
                        var acessos = [
                            {id: 'C', desc: 'Posto de saúde na comunidade'},
                            {id: 'D', desc: 'Posto de saúde na sede do distrito'},
                            {id: 'M', desc: 'Somente na sede do município'}];

                        $scope.acessoCriancaSaude = findElementById(acessos, $scope.atividadeCadastro.acessoPoliticasPublicas.politicasPublicas.acessoCriancaSaude);
                    }
                }

                if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia) {
                    $scope.tipoEnergia = '';
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.energiaMonofasica) {
                        $scope.tipoEnergia = 'Monofásica';
                    } else if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.energiaTrifasica) {
                        $scope.tipoEnergia = 'Trifásica';
                    }

                    $scope.formasAbastecimentoList = [];
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.abastecimentoRedeMunicipal) {
                        $scope.formasAbastecimentoList.push('Ligação à rede de abastecimento de água municipal');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.abastecimentoSistemaSimplificado) {
                        $scope.formasAbastecimentoList.push('Ligação à sistema simplificado de abastecimento');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.abastecimentoCisterna) {
                        $scope.formasAbastecimentoList.push('Cisterna');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.abastecimentoPocoArtesiano) {
                        $scope.formasAbastecimentoList.push('Poço artesiano');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.abastecimentoCacimba) {
                        $scope.formasAbastecimentoList.push('Cacimba');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.abastecimentoAcude) {
                        $scope.formasAbastecimentoList.push('Açúde');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.abastecimentoBarreiro) {
                        $scope.formasAbastecimentoList.push('Barreiro');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.abastecimentoLagoa) {
                        $scope.formasAbastecimentoList.push('Lagoa');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.abastecimentoRio) {
                        $scope.formasAbastecimentoList.push('Rio');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.abastecimentoRiacho) {
                        $scope.formasAbastecimentoList.push('Riacho');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.abastecimentoTanquePedra) {
                        $scope.formasAbastecimentoList.push('Tanque de Pedra');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.abastecimentoCacimbao) {
                        $scope.formasAbastecimentoList.push('Cacimbão ou Poço Amazonas');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.abastecimentoNascente) {
                        $scope.formasAbastecimentoList.push('Nascente ou olho d\'água');
                    }
                    $scope.formasAbastecimento = $scope.formasAbastecimentoList.join(", ");

                    $scope.destinosLixoList = [];
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.lixoColeta) {
                        $scope.destinosLixoList.push('Coleta Municipal');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.lixoQueima) {
                        $scope.destinosLixoList.push('Queima');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.lixoEnterra) {
                        $scope.destinosLixoList.push('Enterra');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.lixoRecicla) {
                        $scope.destinosLixoList.push('Reciclal');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.lixoCeuAberto) {
                        $scope.destinosLixoList.push('Joga a Céu Aberto');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.lixoCompotagem) {
                        $scope.destinosLixoList.push('Compostagem');
                    }
                    $scope.destinosLixo = $scope.destinosLixoList.join(", ");

                    $scope.combustiveisList = [];
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.cozinhaCarvao) {
                        $scope.combustiveisList.push('Carvão');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.cozinhaLenha) {
                        $scope.combustiveisList.push('Lenha');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.cozinhaGas) {
                        $scope.combustiveisList.push('Gás');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.cozinhaQuerosene) {
                        $scope.combustiveisList.push('Querosene');
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.cozinhaBiogas) {
                        $scope.combustiveisList.push('Biogás');
                    }
                    $scope.combustiveis = $scope.combustiveisList.join(", ");
                }

                if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia) {
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.tipoHabitacao) {
                        var tipos = [
                            {id: 'A', desc: 'Alvenaria'},
                            {id: 'T', desc: 'Taipa'},
                            {id: 'O', desc: 'Outro'}];

                        $scope.tipoHabitacao = findElementById(tipos, $scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.tipoHabitacao);
                    }

                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.situacaoMoradia) {
                        var situacoes = [
                            {id: 'P', desc: 'Própria'},
                            {id: 'C', desc: 'Cedida'},
                            {id: 'O', desc: 'Ocupada'},
                            {id: 'A', desc: 'Alugada'}];

                        $scope.situacaoMoradia = findElementById(situacoes, $scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.situacaoMoradia);
                    }

                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.tipoEsgotamentoSanitario) {
                        var tipos = [
                            {id: 'E', desc: 'Ligação à rede de esgoto'},
                            {id: 'S', desc: 'Fossa séptica'},
                            {id: 'V', desc: 'Fossa verde'},
                            {id: 'A', desc: 'A céu aberto'}];

                        $scope.tipoEsgotamentoSanitario = findElementById(tipos, $scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.tipoEsgotamentoSanitario);
                    }

                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.formaAbastecimentoAgua) {
                        var tipos = [
                            {id: 'M', desc: 'Ligação à rede de abastecimento de água municipal'},
                            {id: 'S', desc: 'Ligação à sistema simplificado de abastecimento'},
                            {id: 'I', desc: 'Cisterna'},
                            {id: 'P', desc: 'Poço artesiano'},
                            {id: 'C', desc: 'Poço Cacimba'},
                            {id: 'A', desc: 'Açúde'},
                            {id: 'L', desc: 'Lagoa'},
                            {id: 'R', desc: 'Rio'}];

                        $scope.formaAbastecimentoAgua = findElementById(tipos, $scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.formaAbastecimentoAgua);
                    }

                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.destinoLixo) {
                        var destinos = [
                            {id: 'M', desc: 'Coleta Municipal'},
                            {id: 'Q', desc: 'Queima'},
                            {id: 'E', desc: 'Enterra'},
                            {id: 'R', desc: 'Recicla'}];


                        $scope.destinoLixo = findElementById(destinos, $scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.destinoLixo);
                    }

                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.combustivelCozimento) {
                        var tipos = [
                            {id: 'C', desc: 'Carvão'},
                            {id: 'L', desc: 'Lenha'},
                            {id: 'G', desc: 'Gás'},
                            {id: 'Q', desc: 'Querosene'},
                            {id: 'B', desc: 'Biogás'}];


                        $scope.combustivelCozimento = findElementById(tipos, $scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.combustivelCozimento);
                    }

                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.qualEnergiaAlternativa) {
                        var tipos = [
                            {id: 'S', desc: 'Solar'},
                            {id: 'E', desc: 'Eólica'},
                            {id: 'B', desc: 'Biodigestor'}];


                        $scope.qualEnergiaAlternativa = findElementById(tipos, $scope.atividadeCadastro.acessoPoliticasPublicas.habitacaoSeneamentoEnergia.qualEnergiaAlternativa);
                    }
                }

                if ($scope.atividadeCadastro.acessoPoliticasPublicas.situacaoFundiaria) {
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.situacaoFundiaria.situacaoFundiaria) {
                        var situacoes = [
                            {id: 'P', desc: 'Proprietário'},
                            {id: 'O', desc: 'Posseiro'},
                            {id: 'U', desc: 'Usufrutuário'},
                            {id: 'A', desc: 'Arrendatário'},
                            {id: 'P', desc: 'Parceiro'},
                            {id: 'S', desc: 'Assentado'},
                            {id: 'T', desc: 'Sem Terra'}];


                        $scope.situacaoFundiaria = findElementById(situacoes, $scope.atividadeCadastro.acessoPoliticasPublicas.situacaoFundiaria.situacaoFundiaria);
                    }
                    if ($scope.atividadeCadastro.acessoPoliticasPublicas.situacaoFundiaria.localUsadoProducao) {
                        var locais = [
                            {id: 'N', desc: 'Não produz'},
                            {id: 'C', desc: 'Reside e produz no mesmo local'},
                            {id: 'O', desc: 'Produz em outro local'}];

                        $scope.localUsadoProducao = findElementById(locais, $scope.atividadeCadastro.acessoPoliticasPublicas.situacaoFundiaria.localUsadoProducao);
                    }
                }

                buscarInformacoesDeProducao();
            });
        }

        function buscarInformacoesDeProducao() {
            AtividadeCadastroProducao.get({id: $scope.atividadeCadastro.producao.id}, function (result) {
                $scope.atividadeCadastro.producao = result;

                if ($scope.atividadeCadastro.producao.atividadeProdutiva) {
                    if ($scope.atividadeCadastro.producao.atividadeProdutiva.insumosProducao) {
                        $scope.insumosProducao = [];
                        var insumos = [
                            {id: "Q", desc: "Adubo Químico"},
                            {id: "O", desc: "Adubo Orgânico"},
                            {id: "V", desc: "Adubação Verde"},
                            {id: "R", desc: "Restos de Cultura"}
                        ];

                        for (var i in $scope.atividadeCadastro.producao.atividadeProdutiva.insumosProducao) {
                            $scope.insumosProducao.push(findElementById(insumos, $scope.atividadeCadastro.producao.atividadeProdutiva.insumosProducao[i].tipo));
                        }
                    }

                    if ($scope.atividadeCadastro.producao.atividadeProdutiva.combatePragas) {
                        $scope.combatePragas = [];
                        var pragas = [
                            {id: "A", desc: "Agrotóxicos"},
                            {id: "P", desc: "Produto Alternativo"},
                            {id: "C", desc: "Controle Agroecológico"}
                        ];

                        for (var i in $scope.atividadeCadastro.producao.atividadeProdutiva.combatePragas) {
                            $scope.combatePragas.push(findElementById(pragas, $scope.atividadeCadastro.producao.atividadeProdutiva.combatePragas[i].tipo));
                        }
                    }

                    if ($scope.atividadeCadastro.producao.atividadeProdutiva.aguaIrrigacoes) {
                        $scope.aguaIrrigacoes = [];
                        var tipos = [
                            {id: "N", desc: "Nascente/Olho D'água"},
                            {id: "P", desc: "Poço"},
                            {id: "C", desc: "Cacimba"},
                            {id: "A", desc: "Açude"},
                            {id: "R", desc: "Rio"},
                            {id: "I", desc: "Cisterna de Produção"},
                            {id: "L", desc: "Canal"},
                            {id: "O", desc: "Outro"}
                        ];

                        for (var i in $scope.atividadeCadastro.producao.atividadeProdutiva.aguaIrrigacoes) {
                            $scope.aguaIrrigacoes.push(findElementById(tipos, $scope.atividadeCadastro.producao.atividadeProdutiva.aguaIrrigacoes[i].tipo));
                        }
                    }

                    if ($scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao) {

                        for (var i in $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao) {
                            $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].origemAgua = [];
                            if ($scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].aguaNascente) {
                                $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].origemAgua.push("Nascente/Olho D'água");
                            }
                            if ($scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].aguaPoco) {
                                $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].origemAgua.push("Poço");
                            }
                            if ($scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].aguaCacimba) {
                                $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].origemAgua.push("Cacimba");
                            }
                            if ($scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].aguaAcude) {
                                $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].origemAgua.push("Açude");
                            }
                            if ($scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].aguaRio) {
                                $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].origemAgua.push("Rio");
                            }
                            if ($scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].aguaCisterna) {
                                $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].origemAgua.push("Cisterna de Produção");
                            }
                            if ($scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].aguaCanal) {
                                $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].origemAgua.push("Canal");
                            }
                            if ($scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].aguaReuso) {
                                $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].origemAgua.push("Reuso");
                            }
                            if ($scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].aguaOutro) {
                                $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].origemAgua.push("Outro");
                            }

                            for (var e in $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].produtosSubsistemas) {
                                $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].produtosSubsistemas[e].locaisComercializacao = [];
                                if ($scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].produtosSubsistemas[e].localFeiraMunicipal) {
                                    $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].produtosSubsistemas[e].locaisComercializacao.push("Feira Municipal");
                                }
                                if ($scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].produtosSubsistemas[e].localFeiraFamiliar) {
                                    $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].produtosSubsistemas[e].locaisComercializacao.push("Feira Familiar");
                                }
                                if ($scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].produtosSubsistemas[e].localFeiraAgroecologica) {
                                    $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].produtosSubsistemas[e].locaisComercializacao.push("Feira Agroecológica");
                                }
                                if ($scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].produtosSubsistemas[e].localMercadoInstitucional) {
                                    $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].produtosSubsistemas[e].locaisComercializacao.push("Mercado Institucional");
                                }
                                if ($scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].produtosSubsistemas[e].localCEASA) {
                                    $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].produtosSubsistemas[e].locaisComercializacao.push("CEASA");
                                }
                                if ($scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].produtosSubsistemas[e].localComunidade) {
                                    $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].produtosSubsistemas[e].locaisComercializacao.push("Comunidade");
                                }
                                if ($scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].produtosSubsistemas[e].localMercadoLocal) {
                                    $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].produtosSubsistemas[e].locaisComercializacao.push("Mercado Local");
                                }
                                if ($scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].produtosSubsistemas[e].localCooperativa) {
                                    $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].produtosSubsistemas[e].locaisComercializacao.push("Coopertiva");
                                }
                                if ($scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].produtosSubsistemas[e].localAtravessador) {
                                    $scope.atividadeCadastro.producao.atividadeProdutiva.subsistemasProducao[i].produtosSubsistemas[e].locaisComercializacao.push("Atravessador");
                                }
                            }
                        }
                    }

                    if ($scope.atividadeCadastro.producao.atividadeProdutiva.criacaoAnimal) {
                        $scope.criacaoAnimal = [];

                        var animal = [
                            {id: "B", desc: "Bovino"},
                            {id: "O", desc: "Ovino"},
                            {id: "C", desc: "Caprino"},
                            {id: "S", desc: "Suino"},
                            {id: "E", desc: "Equino"},
                            {id: "A", desc: "Aves"},
                            {id: "P", desc: "Peixes"},
                            {id: "L", desc: "Abelhas"}
                        ];

                        var unidade = [
                            {id: "C", desc: "Cabeça"},
                            {id: "K", desc: "Kg"},
                            {id: "O", desc: "Colméia"}
                        ];

                        var destino = [
                            {id: "C", desc: "Consumo"},
                            {id: "V", desc: "Venda"}
                        ];

                        for (var i in $scope.atividadeCadastro.producao.atividadeProdutiva.criacaoAnimal) {
                            $scope.atividadeCadastro.producao.atividadeProdutiva.criacaoAnimal[i].animal = findElementById(animal, $scope.atividadeCadastro.producao.atividadeProdutiva.criacaoAnimal[i].animal);
                            $scope.atividadeCadastro.producao.atividadeProdutiva.criacaoAnimal[i].unidade = findElementById(unidade, $scope.atividadeCadastro.producao.atividadeProdutiva.criacaoAnimal[i].unidade);
                            $scope.atividadeCadastro.producao.atividadeProdutiva.criacaoAnimal[i].destinoCriacao = findElementById(destino, $scope.atividadeCadastro.producao.atividadeProdutiva.criacaoAnimal[i].destinoCriacao);
                        }
                    }

                    if ($scope.atividadeCadastro.producao.atividadeProdutiva.reservaAlimentarRebalho) {
                        $scope.reservaAlimentar = [];
                        $scope.atividadeCadastro.producao.atividadeProdutiva.reservaAlimentarRebalho.forEach(function (reserva) {
                            $scope.reservaAlimentar.push(reserva.descricao);
                        });
                        $scope.reservaAlimentar = $scope.reservaAlimentar.join(", ");
                    }

                    if ($scope.atividadeCadastro.producao.atividadeProdutiva.atividadesNaoAgricolas) {
                        $scope.atividadeNaoAgricola = [];

                        var atividade = [
                            {id: "A", desc: "Artesanato"},
                            {id: "C", desc: "Confecção"},
                            {id: "T", desc: "Turismo Rural"},
                            {id: "M", desc: "Comércio"},
                            {id: "P", desc: "Prestação de Serviço"},
                            {id: "O", desc: "Outro"},
                            {id: "N", desc: "Não desenvolve atividade não agrícola"}
                        ];

                        for (var i in $scope.atividadeCadastro.producao.atividadeProdutiva.atividadesNaoAgricolas) {
                            $scope.atividadeNaoAgricola.push(findElementById(atividade, $scope.atividadeCadastro.producao.atividadeProdutiva.atividadesNaoAgricolas[i].tipo));
                        }
                    }

                    if ($scope.atividadeCadastro.producao.atividadeProdutiva.atividadesAmbientais) {
                        $scope.atividadesAmbientais = [];

                        var atividade = [
                            {id: "C", desc: "Manejo da Caatinga"},
                            {id: "R", desc: "Reflorestamento"},
                            {id: "A", desc: "Sistemas Agroflorestais"},
                            {id: "F", desc: "Fogão Eficiente"},
                            {id: "B", desc: "Biodigestor"},
                            {id: "T", desc: "Tratamento de Resíduos Sólidos"},
                            {id: "S", desc: "Conservação de Solos"},
                            {id: "P", desc: "Proteção de Mata Ciliar"},
                            {id: "E", desc: "Casa de Sementes"},
                            {id: "H", desc: "Minhocário/Humus"},
                            {id: "O", desc: "Outros"}
                        ];

                        for (var i in $scope.atividadeCadastro.producao.atividadeProdutiva.atividadesAmbientais) {
                            $scope.atividadesAmbientais.push(findElementById(atividade, $scope.atividadeCadastro.producao.atividadeProdutiva.atividadesAmbientais[i].tipo));
                        }

                        $scope.atividadesAmbientais = $scope.atividadesAmbientais.join(", ");
                    }

                    if ($scope.atividadeCadastro.producao.atividadeProdutiva.tipoTracao) {
                        var tiposTracao = [
                            {id: 'A', desc: 'Animal'},
                            {id: 'M', desc: 'Motorizada'},
                            {id: 'H', desc: 'Humana'},
                            {id: 'U', desc: 'Mais de uma opção de tração'}];

                        $scope.tipoTracaoUsada = findElementById(tiposTracao, $scope.atividadeCadastro.producao.atividadeProdutiva.tipoTracao);
                    }

                    if ($scope.atividadeCadastro.producao.atividadeProdutiva.frequenciaAter) {
                        var frequencias = [
                            {id: 'S', desc: 'Semanal'},
                            {id: 'Q', desc: 'Quinzenal'},
                            {id: 'M', desc: 'Mensal'},
                            {id: 'T', desc: 'Trimestral'},
                            {id: 'L', desc: 'Semestral'},
                            {id: 'A', desc: 'Anual'},
                            {id: 'E', desc: 'Eventual'}];

                        $scope.frequenciaATER = findElementById(frequencias, $scope.atividadeCadastro.producao.atividadeProdutiva.frequenciaAter);
                    }

                    if ($scope.atividadeCadastro.producao.atividadeProdutiva.instituicaoAter) {
                        var instituicoes = [
                            {id: null, desc: ''},
                            {id: 'E', desc: 'EMATERCE'},
                            {id: 'G', desc: 'ONG'},
                            {id: 'I', desc: 'Igreja'},
                            {id: 'S', desc: 'Sindicato'},
                            {id: 'P', desc: 'Prefeitura'},
                            {id: 'O', desc: 'Outros'}];

                        $scope.instituicaoAter = findElementById(instituicoes, $scope.atividadeCadastro.producao.atividadeProdutiva.instituicaoAter);
                    }
                }

                buscarInformacoesDeCapacitacao();

            });
        }

        function buscarInformacoesDeCapacitacao() {
            AtividadeCadastroCapacitacao.get({id: $scope.atividadeCadastro.capacitacao.id}, function (result) {
                $scope.atividadeCadastro.capacitacao = result;

                if ($scope.atividadeCadastro.capacitacao.programa) {
                    $scope.temasList = [];
                    $scope.atividadeCadastro.capacitacao.programa.temas.forEach(function (tema) {
                        $scope.temasList.push(temaEnum[tema.tipo].desc);
                    });
                    $scope.temas = $scope.temasList.join(", ");

                    $scope.orgaosList = [];
                    $scope.atividadeCadastro.capacitacao.programa.orgaos.forEach(function (orgao) {
                        $scope.orgaosList.push(orgaoEnum[orgao.tipo].desc);
                    });
                    $scope.orgaos = $scope.orgaosList.join(", ");

                }

                if ($scope.atividadeCadastro.capacitacao.organizacao) {
                    $scope.organizacaoLocalList = [];
                    $scope.atividadeCadastro.capacitacao.organizacao.organizacaoLocal.forEach(function (orgLocal) {
                        $scope.organizacaoLocalList.push(orgLocal.descricao);
                    });
                    $scope.organizacaoLocal = $scope.organizacaoLocalList.join(", ");

                    var organizacoes = [
                        {id: 'A', desc: 'Associações'},
                        {id: 'C', desc: 'Cooperativas'},
                        {id: 'S', desc: 'Representação Sindical'}];

                    $scope.qualOrganizacaoLocal = findElementById(organizacoes, $scope.atividadeCadastro.capacitacao.organizacao.qualOrganizacaoLocal);
                }
            });

        }

        function findElementById(elements, id) {
            var element = elements.filter(function (element) {
                return element.id == id;
            });

            if (element[0]) {
                return element[0].desc
            }
        }

        $scope.grupoProdutivoEnum = grupoProdutivoEnum;
        $scope.grupoEtnicoEnum = grupoEtnicoEnum;
        $scope.racaEnum = racaEnum;
        $scope.parentescoEnum = parentescoEnum;
        $scope.atividadeEconomicaEnum = atividadeEconomicaEnum;
        $scope.beneficioSocialEnum = beneficioSocialEnum;
        $scope.tipoAposentadoriaEnum = tipoAposentadoriaEnum;
        $scope.telefoneEnum = telefoneEnum;
        $scope.temaEnum = temaEnum;
        $scope.orgaoEnum = orgaoEnum;
        $scope.oraganizacaoLocalEnum = oraganizacaoLocalEnum;
        $scope.situacaoFundiariaEnum = situacaoFundiariaEnum;
        $scope.subsistemaEnum = subsistemaEnum;
        $scope.modalidadePronafEnum = modalidadePronafEnum;
        $scope.modalidadePaaEnum = modalidadePaaEnum;
        $scope.categoriaHabitacaoRuralEnum = categoriaHabitacaoRuralEnum;
        $scope.categoriaBanheiroPopularEnum = categoriaBanheiroPopularEnum;
        $scope.tipoHabitacaoEnum = tipoHabitacaoEnum;
        $scope.situacaoMoradiaEnum = situacaoMoradiaEnum;
        $scope.tipoEsgotamentoSanitarioEnum = tipoEsgotamentoSanitarioEnum;
        $scope.qualEnergiaAlternativaEnum = qualEnergiaAlternativaEnum;
        $scope.localComercializacaoEnum = localComercializacaoEnum;
        $scope.certificacaoEnum = certificacaoEnum;

        buscarAcessoPoliticasPublicas();

    });
