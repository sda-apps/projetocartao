'use strict';

angular.module('projetocartaoApp')
    .controller('AvaliarTarefaController', function ($scope, params, historico, $mdDialog, Toast, $filter, HistoricoAvaliacaoTarefa, AtualizarTarefa) {
        $scope.tarefa = params;
        $scope.tarefaHistorico = $filter('orderBy')(historico, 'criacao', true);
        $scope.historico = {};

        if ($scope.tarefaHistorico.length > 0) {
            $scope.historico.motivo = $scope.tarefaHistorico[0].motivo;
            $scope.historico.avaliacao = $scope.tarefaHistorico[0].avaliacao;
        }

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.salvar = function () {
            if ($scope.historico.motivo === 'A' || $scope.historico.motivo === 'E') {
                updateTarefa();
            } else if ($scope.historico.motivo === 'R') {

                if (!$scope.historico.avaliacao) {
                    return
                }
                $scope.tarefa.recusado = true;
                updateTarefa();

            } else {
                Toast.alerta("Informe o status da avaliação da tarefa.");
            }
        };

        var updateTarefa = function () {
            $scope.historico.tarefa = {
                id: $scope.tarefa.id
            };

            $scope.tarefa.status = $scope.historico.motivo;
            AtualizarTarefa.update($scope.tarefa).$promise.then(function (result) {
                HistoricoAvaliacaoTarefa.save($scope.historico).$promise.then(function (result) {
                    $mdDialog.hide(result);
                }).catch(function (error) {
                    console.log("Erro: ", error);
                });
            }).catch(function (error) {
                console.log("Erro: ", error);
            });
        };
    });

