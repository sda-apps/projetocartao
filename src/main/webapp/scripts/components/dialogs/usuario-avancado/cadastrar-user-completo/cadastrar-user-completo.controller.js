'use strict';

angular.module('projetocartaoApp')
    .controller('CadastrarUserCompletoController', function ($rootScope, $filter, $scope, $mdDialog, userToEdit, canEditPerfil,
                                                             User, $http, EstadoCivil, Uf, Escolaridade, Cargo, telefoneEnum,
                                                             Toast, DateUtils, AtivaUsuario, InativaUsuario) {


        $scope.mostraDiv = "dadosPessoais";
        $scope.isDisabled = false;
        $scope.telefoneEnum = telefoneEnum;
        EstadoCivil.query().$promise
            .then(function (result, headers) {
                $scope.estadoCivilList = result;
            }).catch(function (response) {
        });

        Uf.query().$promise
            .then(function (result, headers) {
                $scope.ufList = result;
            }).catch(function (response) {
        });

        Escolaridade.query().$promise
            .then(function (result, headers) {
                $scope.escolaridadeList = result;
            }).catch(function (response) {
        });

        Cargo.query().$promise
            .then(function (result, headers) {
                $scope.cargoList = result;
            }).catch(function (response) {
        });

        if (userToEdit) {
            $scope.user = userToEdit;
            if ($scope.user.autoridades == undefined && $scope.user.authorities != undefined) {
                $scope.user.autoridades = $scope.user.authorities;
            }

            if ($scope.user.autoridades.length > 0) {
                $scope.autoridades = $scope.user.autoridades[0].nome;
            }
        } else {
            $scope.user = $rootScope.account;
        }

        if (canEditPerfil) {
            $scope.titulo = "Editar Usuário";
            $scope.canEditPerfil = canEditPerfil;
            if ($rootScope.account.authorities[0] == 'ROLE_RESPONSAVEL_ATER') {
                $scope.isDisabled = true;
            }
        } else {
            $scope.titulo = "Meu Perfil";
            $scope.isDisabled = true;
        }

        $scope.user.usuarioAvancado.pessoaFisica.dataNascimento = $filter('date')($scope.user.usuarioAvancado.pessoaFisica.dataNascimento, 'dd/MM/yyyy');
        $scope.user.usuarioAvancado.pessoaFisica.dataExpedicaoRg = $filter('date')($scope.user.usuarioAvancado.pessoaFisica.dataExpedicaoRg, 'dd/MM/yyyy');

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.salvar = function (usuarioAvancadoDadosPessoaisCompletoForm) {
            if (usuarioAvancadoDadosPessoaisCompletoForm.$valid) {
                if ($scope.user.usuarioAvancado.pessoaFisica.pessoa.nome && $scope.user.usuarioAvancado.pessoaFisica.cpf && $scope.user.usuarioAvancado.pessoaFisica.sexo && $scope.user.usuarioAvancado.pessoaFisica.dataNascimento && ($scope.user.authorities || $scope.autoridades) && $scope.user.usuarioAvancado.emailInstitucional) {

                    var usuario = angular.copy($scope.user);

                    usuario.autoridades = [];
                    if ($scope.autoridades) {
                        usuario.autoridades.push({nome: $scope.autoridades});
                    }

                    usuario.primeiroAcesso = false;

                    if (usuario.usuarioAvancado.pessoaFisica.pessoa.telefones == null) {
                        usuario.usuarioAvancado.pessoaFisica.pessoa.telefones = [];
                    }

                    var dataFormatada = DateUtils.formatDate(usuario.usuarioAvancado.pessoaFisica.dataNascimento);
                    if (dataFormatada) {
                        var dataNascimento = DateUtils.convertFormattedDate(dataFormatada);
                        usuario.usuarioAvancado.pessoaFisica.dataNascimento = dataNascimento;
                    }

                    var dataExpedicaoFormatada = DateUtils.formatDate(usuario.usuarioAvancado.pessoaFisica.dataExpedicaoRg);
                    if (dataExpedicaoFormatada) {
                        var dataExpedicaoRg = DateUtils.convertFormattedDate(dataExpedicaoFormatada);
                        usuario.usuarioAvancado.pessoaFisica.dataExpedicaoRg = dataExpedicaoRg;
                    }

                    User.update(usuario).$promise.then(function (result) {
                        $mdDialog.cancel();

                    }).catch(function (error) {
                        if (error.data.messages) {
                            Toast.erro(error.data.messages[0]);
                        }
                    });
                } else {
                    $scope.verificaInputs(usuarioAvancadoDadosPessoaisCompletoForm);
                }
            }
            else {
                usuarioAvancadoDadosPessoaisCompletoForm.$submitted = true;
                Toast.alerta("O cadastro possui pendências e não pode ser salvo.")
            }

        };

        $scope.verificaInputs = function (usuarioAvancadoDadosPessoaisCompletoForm) {

            if ($scope.mostraDiv == "dadosPessoais") {
                if (!$scope.user.usuarioAvancado.pessoaFisica.pessoa.nome) {
                    usuarioAvancadoDadosPessoaisCompletoForm.nome.$setValidity('required', false);
                }

                if (!$scope.user.usuarioAvancado.pessoaFisica.cpf) {
                    usuarioAvancadoDadosPessoaisCompletoForm.cpf.$setValidity('required', false);
                }

                if (!$scope.user.usuarioAvancado.pessoaFisica.sexo) {
                    usuarioAvancadoDadosPessoaisCompletoForm.sexo.$setValidity('required', false);
                }

                if (!$scope.user.usuarioAvancado.pessoaFisica.dataNascimento) {
                    usuarioAvancadoDadosPessoaisCompletoForm.dataNascimento.$setValidity('required', false);
                }

                if (!$scope.autoridades) {
                    usuarioAvancadoDadosPessoaisCompletoForm.autoridades.$setValidity('required', false);
                }

            }

            if ($scope.mostraDiv == "contato") {
                if (!$scope.user.usuarioAvancado.emailInstitucional) {
                    usuarioAvancadoDadosPessoaisCompletoForm.emailInstitucional.$setValidity('required', false);
                }
            }
        };

        $scope.buscarAutoCompletePorFiltro = function (searchText, url, filtro) {
            try {
                var search;
                if (filtro == null) {
                    search = url + searchText
                } else {
                    search = url + searchText + "/" + filtro;
                }

                var data = $http({
                    method: 'GET',
                    url: search
                }).then(function successCallback(response) {
                    return response.data;
                }, function errorCallback(response) {
                    return response.data;
                });

                return data;
            } catch (e) {
                console.log(e);
            }
        };

        $scope.aposSelecionarEstado = function (estado) {
            if (estado != $scope.estadoAux) {
                $scope.user.usuarioAvancado.pessoaFisica.pessoa.municipio = null;
            }
        };

        $scope.aposAbrirEstado = function (estado) {
            $scope.estadoAux = estado;
        };

        $scope.ativarUsuario = function () {
            if ($scope.user.id) {
                AtivaUsuario.post([{id: $scope.user.id}]).$promise.then(function () {
                    $mdDialog.cancel();
                }).catch(function (erro) {
                    var msg = erro.data.status + ' - ' + erro.data.error;
                    Toast.erro(msg);
                });
            }
        };

        $scope.desativarUsuario = function () {
            if ($scope.user.id) {
                InativaUsuario.post([{id: $scope.user.id}]).$promise.then(function () {
                    $mdDialog.cancel();
                }).catch(function (erro) {
                    var msg = erro.data.status + ' - ' + erro.data.error;
                    Toast.erro(msg);
                });
            }
        };

    });
