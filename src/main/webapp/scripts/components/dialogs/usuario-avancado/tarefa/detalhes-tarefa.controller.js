'use strict';

angular.module('projetocartaoApp')
    .controller('DetalhesTarefaController', function ($scope, $mdDialog, tarefa, params, planoTrimestral, tarefaStatus, MultipartFile, abrangenciaEnum, tarefaTipo) {

        $scope.tarefa = tarefa;
        $scope.tarefaComplemento = params;
        $scope.planoTrimestral = planoTrimestral;
        $scope.tarefaStatus = tarefaStatus;
        $scope.abrangenciaEnum = abrangenciaEnum;
        $scope.tarefaTipo = tarefaTipo;
        $scope.revisado = $scope.tarefa.status == 'T' && $scope.tarefa.recusado == true ? "Revisado/" : "";
        $scope.signature = null;
        $scope.isLoadingSignature = true;
        $scope.eixoEspecifiacoes = getEspecificacao($scope.tarefaComplemento.tarefaTemas);
        $scope.eixosTematico = getEixosTematico($scope.tarefaComplemento.tarefaTemas);
        $scope.localidadesParticipante = getLocalidadesParticipantes($scope.tarefa.comunidadeParticipante);
        $scope.municipiosParticipante = getMunicipiosParticipantes($scope.tarefa.comunidadeParticipante);
        getCountParticipantsList($scope.tarefaComplemento.listaPresencaTarefa);

        $scope.duracao = new Date(tarefa.duracao);

        MultipartFile.get({
            bucketName: "cartao/ater_atividades/" + $scope.tarefa.id,
            fileName: "signature.png"
        }).$promise.then(function (result) {
            $scope.isLoadingSignature = false;
            $scope.signature = window.URL.createObjectURL(result.response);
        }).catch(function (erro) {
            $scope.isLoadingSignature = false;
        });

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        function getEspecificacao(tarefaTemas) {
            var eixoEspecifiacoes = [];

            tarefaTemas.forEach(function (tema) {
                var especificacao = null;
                tema.temaEspecificacoes.forEach(function (temaEspecificacao) {
                    if (temaEspecificacao.nome != null) {
                        if (especificacao != null) {
                            especificacao = especificacao + " | " + temaEspecificacao.nome;
                        } else {
                            especificacao = temaEspecificacao.nome;
                        }
                    }
                });
                if (especificacao != null) {
                    if(tema.tema.eixoTematico) {
                        eixoEspecifiacoes.push({nome: especificacao, eixo: tema.tema.eixoTematico.nome});
                    }
                }
            });
            return eixoEspecifiacoes;
        }

        function getEixosTematico(tarefaTemas) {
            $scope.eixos = [];

            tarefaTemas.forEach(function (tema) {
                if(tema.tema.eixoTematico) {
                    $scope.eixos.push(tema.tema.eixoTematico.nome);
                }
            });

            $scope.eixos = $scope.eixos.filter(function (item, pos) {
                return $scope.eixos.indexOf(item) == pos;
            });
            return $scope.eixos;
        }

        function getAge(dateString) {
            var today = new Date();
            var birthDate = new Date(dateString);
            var age = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            return age;
        }

        function getCountParticipantsList(listaPresencaTarefa) {
            $scope.contadorFeminino = 0;
            $scope.contadorJovens = 0;
            listaPresencaTarefa.forEach(function (agricultor) {
                if (agricultor.agricultor.pessoaFisica.sexo == 'F') {
                    $scope.contadorFeminino++;
                }
                var idadeAgricultor = getAge(agricultor.agricultor.pessoaFisica.dataNascimento);
                if (idadeAgricultor > 15 && idadeAgricultor < 30) {
                    $scope.contadorJovens++;
                }
            });
        }

        function getLocalidadesParticipantes(comunidadeParticipante) {
            var localidades = [];
            try {
                comunidadeParticipante.forEach(function (comunidadeParticipante) {
                    comunidadeParticipante.comunidade.localidades.forEach(function (localidade) {
                        if (localidade.nome != null) {
                            localidades.push({
                                localidade: localidade.nome,
                                comunidade: comunidadeParticipante.comunidade.nome,
                                municipio: comunidadeParticipante.comunidade.municipio.nome
                            });
                        }
                    });
                });
            } catch(e) {
                console.log(e);
            }

            return localidades;
        }

        function getMunicipiosParticipantes(comunidadeParticipante) {
            $scope.municipios = [];

            comunidadeParticipante.forEach(function (comunidadeParticipante) {
                if(comunidadeParticipante.comunidade.municipio) {
                    $scope.municipios.push(comunidadeParticipante.comunidade.municipio.nome);
                }
            });

            $scope.municipios = $scope.municipios.filter(function (item, pos) {
                return $scope.municipios.indexOf(item) == pos;
            });
            return $scope.municipios;
        }

    })
;
