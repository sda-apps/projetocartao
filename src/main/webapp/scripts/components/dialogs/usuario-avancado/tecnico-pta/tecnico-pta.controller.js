'use strict';

angular.module('projetocartaoApp')
    .controller('TecnicoPTAController', function ($scope, $mdDialog, $http, $filter, $timeout, params, Toast, perfilTecnicoEnum, SalvarTecnico) {
        $scope.titleDialog = 'Associar Técnico';
        $scope.perfilTecnicoEnum = perfilTecnicoEnum;
        $scope.comunidadeSelecionada = [];

        if (params) {
            $scope.pta = params[0];
            $scope.comunidadesContrato = params[1];

            if (params[2]) {
                $scope.tecnicoSelecionado = params[2];

                if ($scope.tecnicoSelecionado.tecnico.areasAtuacao) {
                    $scope.comunidadeSelecionada = $scope.tecnicoSelecionado.tecnico.areasAtuacao.map(function (key) {
                        return key.comunidade;
                    });
                }

                $scope.perfilSelecionado = $scope.tecnicoSelecionado.perfil;

                $scope.titleDialog = 'Editar ' + $filter('capitalize')($scope.tecnicoSelecionado.tecnico.pessoaFisica.pessoa.nome);
                $scope.modoEdicao = true;
            }
        }

        $scope.buscarAutoCompletePorFiltro = function (searchText, url, filtro) {
            var search;
            if (filtro == null) {
                search = url + searchText
            } else {
                search = url + searchText + "/" + filtro;
            }

            var data = $http({
                method: 'GET',
                url: search
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                return response.data;
            });

            return data;
        };

        $scope.buscaArrayComunidade = function (comunidade) {
            var result = [];
            if ($scope.comunidadesContrato != null) {
                for (var i = 0; i < $scope.comunidadesContrato.length; i++) {
                    if ($scope.comunidadesContrato[i].nome.substr(0, comunidade.length).toUpperCase() == comunidade.toUpperCase()) {
                        result.push($scope.comunidadesContrato[i]);
                    }
                }
            }
            return result;
        };

        $scope.salvar = function () {
            $scope.tecnicoSelecionado.tecnico.areasAtuacao = [];

            if ($scope.perfilSelecionado == 'C') {
                for (var i in $scope.comunidadeSelecionada) {
                    var areaAtuacao = {
                        comunidade: $scope.comunidadeSelecionada[i],
                        municipio: $scope.comunidadeSelecionada[i].municipio,
                        planoTrabalhoAnual: $scope.pta
                    };

                    if (areaAtuacao.municipio) {
                        $scope.tecnicoSelecionado.tecnico.areasAtuacao.push(areaAtuacao);
                    }
                }
            }

            $scope.tecnicoSelecionado.perfil = $scope.perfilSelecionado;

            $scope.tecnicoSelecionado.planoTrabalhoAnual = {
                id: $scope.pta.id
            };

            if ($scope.tecnicoSelecionado.id) {
                SalvarTecnico.update($scope.tecnicoSelecionado).$promise.then(function (result) {
                    Toast.sucesso("Técnico atualizado com sucesso.");
                    $mdDialog.hide(result);
                }).catch(function (error) {
                    console.log(error);
                    Toast.erro("Erro ao atualizar técnico.");
                });
            } else {
                SalvarTecnico.post($scope.tecnicoSelecionado).$promise.then(function (result) {
                    Toast.sucesso("Técnico associado com sucesso.");

                    $mdDialog.hide(result);
                }).catch(function (error) {
                    console.log('Error', error);
                    if (error.status == 409) {
                        Toast.alerta("Técnico já está associado a esse Plano de Trabalho.");
                    } else if (error.status == 500) {
                        Toast.erro("Não é possível adicionar Responsável ou Coordenador ATER.");
                    } else {
                        Toast.erro("Erro ao salvar técnico.");
                    }
                });
            }
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.cadastrarUsuario = function () {
            $mdDialog.show({
                controller: 'CadastrarEditarUserController',
                templateUrl: 'scripts/components/dialogs/usuario-avancado/cadastrar-editar-user/cadastrar-user.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                disableParentScroll: true,
                locals: {
                    userToEdit: null,
                    isUsuarioEmpresa: true,
                    empresa: $scope.pta.contrato.empresa
                },
                fullscreen: true
            }).then(function (answer) {
                $mdDialog.show({
                    controller: 'TecnicoPTAController',
                    templateUrl: 'scripts/components/dialogs/usuario-avancado/tecnico-pta/tecnico-pta.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,
                    disableParentScroll: true,
                    locals: {
                        params: [angular.copy($scope.pta), $scope.comunidadesContrato, angular.copy({tecnico: answer.usuarioAvancado}), params[3]]
                    },
                    fullscreen: true
                }).then(function (answer) {
                    if (params[3]) {
                        if (params[3].tecnicos == undefined || params[3].tecnicos == null || params[3].tecnicos == '') {
                            params[3].tecnicos = [];
                        }

                        params[3].tecnicos.push(answer);
                    }
                }, function () {
                    //cancel
                });

            }, function () {
                //cancel
            });
        };

        $scope.validarAdicaoComunidade = function (item) {
            if (item) {
                var haComunidade = $scope.comunidadeSelecionada.some(function (comunidade) {
                    return comunidade.id == item.id;
                });

                //TODO remoção temporária
                //var haMaisDe3Areas = verificarExisteMaisQue3AreasDeTrabalho(item, haComunidade);

                if (!haComunidade/* && !haMaisDe3Areas*/) {
                    verificarComunidadeEmOutrosTecnicos(item);
                }
            }
        };

        function verificarExisteMaisQue3AreasDeTrabalho(item, haComunidade) {
            if ($scope.comunidadeSelecionada.length >= 3 && haComunidade == false) {
                $scope.comunidadeSelecionada = $scope.comunidadeSelecionada.filter(function (elemento) {
                    return elemento.id !== item.id;
                });

                Toast.info("Técnico já está associado à 3 comunidades.");

                return true;
            } else {
                return false;
            }
        }

        function verificarComunidadeEmOutrosTecnicos(item) {
            for (var i in $scope.pta.tecnicos) {
                var tecnicoPTA = $scope.pta.tecnicos[i];

                if (tecnicoPTA.tecnico) {
                    var haComunidadeNosTecnicos = tecnicoPTA.tecnico.areasAtuacao.some(function (area) {
                        return area.comunidade.id == item.id;
                    });

                    if (haComunidadeNosTecnicos) {
                        Toast.info("Esta comunidade já está associada ao técnico: " + tecnicoPTA.tecnico.pessoaFisica.pessoa.nome);
                        $timeout(function () {
                            $scope.comunidadeSelecionada = $scope.comunidadeSelecionada.filter(function (elemento) {
                                return elemento.id !== item.id;
                            });
                        });

                        break;
                    }
                }
            }
        }
    });
