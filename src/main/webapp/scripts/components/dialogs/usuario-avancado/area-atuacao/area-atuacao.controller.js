'use strict';

angular.module('projetocartaoApp')
    .controller('AreaAtuacaoController', function ($scope, $mdDialog, userToEdit, AreaAtuacaoPorId,SalvarAreaAtuacao, Toast) {

        $scope.municipiosDisponiveis = [{id:10014366545,nome:"Icó"},{id:10014366546,nome:"Iguatú"}];

        if (userToEdit) {
            $scope.user = userToEdit;
        }

        AreaAtuacaoPorId.get({usuarioAvancadoId : $scope.user.usuarioAvancado.id}).$promise.then( function (result) {
        	$scope.municipiosAssociados = result;
        }).catch(function(){
            $scope.municipiosAssociados = [];
        });


        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.associarMunicipio = function (item) {
            $scope.municipiosAssociados.push(item);
            var index = $scope.municipiosDisponiveis.indexOf(item);
            $scope.municipiosDisponiveis.splice(index,1);
        };

        $scope.removerMunicipio = function (item) {
            $scope.municipiosDisponiveis.push(item);
            var index = $scope.municipiosAssociados.indexOf(item);
            $scope.municipiosAssociados.splice(index,1);
        };

        $scope.salvar = function () {
            $scope.dto = {usuarioAvancadoId: $scope.user.usuarioAvancado.id, municipios: $scope.municipiosAssociados }

            SalvarAreaAtuacao.post($scope.dto).$promise.then(function (result) {
                Toast.sucesso("Área de atuação salva com sucesso.")
            })
                .catch(function (error) {
                    console.log(error);
                });

            $mdDialog.cancel();
        };

    });
