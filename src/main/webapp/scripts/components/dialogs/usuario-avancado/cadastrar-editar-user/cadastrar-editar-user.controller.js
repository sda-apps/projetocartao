'use strict';

angular.module('projetocartaoApp')
    .controller('CadastrarEditarUserController', function (
        $rootScope, $filter, $scope, $http, $mdDialog, userToEdit, Principal, rolesAccess,
        isUsuarioEmpresa, empresa, User, DateUtils, Toast, constants) {

        $scope.isUsuarioEmpresa = isUsuarioEmpresa;
        $scope.hasUser = false;
        $scope.isEmpresaDisabled = false;
        $scope.autoridades = ['ROLE_TECNICO', 'ROLE_ADMIN', "ROLE_GESTOR", "ROLE_AGRI", "ROLE_AGRI_REST", 'ROLE_COORDENADOR_ATER'];
        var autoridadesAgricultor = ["ROLE_AGRI", "ROLE_AGRI_REST"];
        var userAutoridades = [];

        if (userToEdit) {
            $scope.hasUser = true;
            $scope.user = userToEdit;
            $scope.userStatus = $scope.user.status;
            for (var auth in $scope.user.autoridades) {
                userAutoridades.push($scope.user.autoridades[auth].nome);
            }
        }

        $scope.podeEditar = function () {
            return Principal.hasAnyAuthority(rolesAccess['USER_COMPANY'].editar);
        };

        if (empresa) {
            $scope.empresa = empresa;
        } else {
            if ($rootScope.account.authorities[0] == 'ROLE_RESPONSAVEL_ATER') {
                $scope.empresa = $rootScope.account.usuarioAvancado.empresa;
                $scope.isEmpresaDisabled = true;
            }
        }

        $scope.toggle = function (autoridade) {
            var isAutoAgricultorNova = autoridadesAgricultor.indexOf(autoridade) > -1;
            for (var aux in userAutoridades) {
                var isAutoAgricultorSelecionada = autoridadesAgricultor.indexOf(userAutoridades[aux]) > -1;
                if ((isAutoAgricultorNova && !isAutoAgricultorSelecionada) || (!isAutoAgricultorNova && isAutoAgricultorSelecionada)) {
                    userAutoridades = [];
                    break;
                }
            }
            var idx = userAutoridades.indexOf(autoridade);
            idx > -1 ? userAutoridades.splice(idx, 1) : userAutoridades.push(autoridade);
        };

        $scope.salvar = function () {
            var dataFormatada = DateUtils.formatDate($scope.dataNascimento);
            if (dataFormatada) {
                var dataNascimento = DateUtils.convertFormattedDate(dataFormatada);
                $scope.user.usuarioAvancado.pessoaFisica.dataNascimento = dataNascimento;
            }
            $scope.user.autoridades = [];
            $scope.user.tipo = constants.USUARIO_ATER;

            for (var aux in userAutoridades) {
                $scope.user.autoridades.push({nome: userAutoridades[aux]});
            }

            if (isUsuarioEmpresa) {
                $scope.user.autoridades = [{nome: $scope.autoridadeEmpresa}];
            }

            if ($scope.empresa) {
                $scope.user.usuarioAvancado.empresa = $scope.empresa;
            }

            if ($scope.hasUser) {
                $scope.user.status = $scope.userStatus;

                User.update($scope.user).$promise.then(function (result) {
                    $mdDialog.cancel();
                }).catch(function (erro) {
                    Toast.erro("Erro ao atualizar usuário.");
                });
            } else {
                User.save($scope.user).$promise.then(function (result) {
                    $mdDialog.hide(result);
                }).catch(function (error) {
                    if (error.status == 409) {
                        Toast.erro("Usuário com CPF informado já está cadastrado.");
                    } else {
                        if (error.data.messages) {
                            Toast.erro(error.data.messages[0]);
                        } else {
                            Toast.erro("Erro ao adicionar usuário.");
                        }
                    }

                });
            }
        };

        $scope.buscarAutoCompletePorFiltro = function (searchText, url, filtro) {
            try {
                var search;
                if (filtro == null) {
                    search = url + searchText
                } else {
                    search = url + searchText + "/" + filtro;
                }

                var data = $http({
                    method: 'GET',
                    url: search
                }).then(function successCallback(response) {
                    return response.data;
                }, function errorCallback(response) {
                    return response.data;
                });

                return data;
            } catch (e) {
                console.log(e);
            }
        };

        $scope.isChecked = function (autoridade) {
            return userAutoridades.indexOf(autoridade) > -1;
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
    });
