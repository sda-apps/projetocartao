'use strict';

angular.module('projetocartaoApp')
    .controller('AvaliarCadastroController', function ($scope, params, $mdDialog, HistoricoAvaliacao, Toast, AvaliarCadastro,$filter) {
        $scope.atividadeCadastro = params;
        $scope.atividadeCadastro.historico = $filter('orderBy')($scope.atividadeCadastro.historico, 'criacao', true);
        $scope.historico = {};

        if(params.historico.length > 0) {
            $scope.historico.motivo =  $scope.atividadeCadastro.historico[0].motivo;
            $scope.historico.avaliacao =  $scope.atividadeCadastro.historico[0].avaliacao;
        }

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.salvar = function () {

            if($scope.historico.motivo === 'A' || $scope.historico.motivo === 'E') {
                updateCadastro();
            } else if ($scope.historico.motivo === 'R') {

                if (!$scope.historico.avaliacao) {
                    return
                }

                $scope.atividadeCadastro.recusado = true;
                updateCadastro();

            } else {
                Toast.alerta("Informe o status da avaliação do cadastro.");
            }

        };

        var updateCadastro = function () {

            $scope.historico.atividadeCadastro = {
                id: $scope.atividadeCadastro.id
            };

            $scope.atividadeCadastro.status = $scope.historico.motivo;

            AvaliarCadastro.update($scope.atividadeCadastro).$promise.then(function () {
                HistoricoAvaliacao.save($scope.historico).$promise.then(function (resultHistorico) {
                    $mdDialog.hide(resultHistorico);
                    Toast.sucesso("Avaliação realizada com sucesso.");
                }).catch(function (erro) {
                });
            }).catch(function (erro) {
            });
        };

    });

