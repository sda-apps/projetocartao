'use strict';

angular.module('projetocartaoApp')
    .controller('dialogAterTecnico', function ($rootScope, $filter, $scope, $mdDialog, resultado) {
        $scope.resultado = resultado;
        console.log($scope.tecnico);
        $scope.textAreaMsg = "No momento a função troca de mensagens não está ativa."

        $scope.cancel = function() {
            $mdDialog.cancel();
        };
    });
