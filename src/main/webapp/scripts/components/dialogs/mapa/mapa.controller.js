'use strict';

angular.module('projetocartaoApp')
    .controller('DialogMapaController', function ($scope, $rootScope, $state, $mdDialog, $mdSidenav, $filter, $mdMedia, Principal, $view, AgricultorDaps, Agricultor, $window) {
        Principal.identity().then(function(account) {
            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;

            getDapsAgricultor();

            $scope.dadosAgricultor=[
                {dado: $filter('cpf')($rootScope.account.agricultor.pessoaFisica.cpf), descricao:'CPF'},
            ];
        });

        var agricultorSelecionado;
        $scope.dialogConfirmarNovaJanela = function() {
            $mdDialog.show({
                templateUrl: 'scripts/components/dialogs/usuario-avancado/confirmar-janela-agricultor/confirmar-janela-agricultor.html',
                locals: {
                    agricultorConfirmarDialog: agricultorSelecionado
                },
                controller: 'dialogConfirmarJanelaAgricultorController',
                backdrop : false,
                escapeToClose: true
            });
        };

        $scope.selecionaAgricultor = function(agricultor){
            Agricultor.get({id: agricultor.id}, function (result) {
                agricultorSelecionado = result;
                $scope.dialogConfirmarNovaJanela();
            });
        };

		function getDapsAgricultor(){
            AgricultorDaps.get({agricultorId: $rootScope.account.agricultor.id}, function (result) {
                var daps = [];

                angular.forEach(result, function(dap){
                    daps.push(dap);
                });

                $rootScope.account.agricultor.daps = daps;

                if(daps.length > 0) {
                    $scope.dadosAgricultor.splice(1, 0, {dado: daps[0].numero, descricao:'DAP'} )
                }
            });
        }
    });
