'use strict';

angular.module('projetocartaoApp')
    .controller('CadastrarEixoTematicoController', function ($rootScope, $filter, $scope, $mdDialog, eixtoTematicoToEdit, DateUtils, Toast, EixoTematico, rolesAccess, Principal) {
        $scope.hasEixoTematico = false;

        if (eixtoTematicoToEdit) {
            $scope.title = "Editar Eixo Temático";
            $scope.eixo = eixtoTematicoToEdit;
            $scope.hasEixoTematico = true;
        } else {
            $scope.title = "Cadastrar Eixo Temático"
        }

        $scope.podeEditar = function () {
            return Principal.hasAnyAuthority(rolesAccess['EIXO_TEMATICO'].editar);
        };

        $scope.salvar = function () {
            if ($scope.hasEixoTematico) {
                EixoTematico.update($scope.eixo).$promise.then(function (result) {
                    Toast.sucesso("Eixo Temático atualizado com sucesso.");
                    $mdDialog.cancel();
                }).catch(function (erro) {
                    Toast.erro("Erro ao atualizar eixo temático.");
                });
            } else {
                EixoTematico.save($scope.eixo).$promise.then(function (result) {
                    Toast.sucesso("Eixo Temático salvo com sucesso.");
                    $mdDialog.cancel();
                }).catch(function (error) {
                    if (error.data.messages) {
                        Toast.erro(error.data.messages[0]);
                    } else {
                        Toast.erro("Erro ao adicionar eixo temático.");
                    }
                });
            }
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
    });
