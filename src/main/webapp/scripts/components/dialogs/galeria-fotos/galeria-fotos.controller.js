'use strict';

angular.module('projetocartaoApp')
    .controller('GaleriaFotosController', function ($scope, fotos, posicao, $mdDialog) {
        $scope.fotos = fotos;
        $scope.posicao = posicao;
        $scope.imgSelecionada = fotos[posicao];

        $scope.mostraFoto = function (posicao, validacao) {
            if (!validacao) {
                $scope.posicao = posicao;
                $scope.imgSelecionada = $scope.fotos[$scope.posicao];
                validaSetas($scope.posicao);
            }
        };

        function validaSetas(posicao) {
            if (posicao == 0) {
                $scope.escondeVoltar = true;
            } else {
                $scope.escondeVoltar = false;
            }

            if (posicao == $scope.fotos.length - 1) {
                $scope.escondeProximo = true;
            } else {
                $scope.escondeProximo = false;
            }
        }

        validaSetas($scope.posicao);

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
    });

