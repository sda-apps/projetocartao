'use strict';

angular.module('projetocartaoApp')
    .controller('CadastrarTemaController', function ($rootScope, $filter, $scope, $mdDialog, temaToEdit, DateUtils, Toast, Tema, EixoTematicoTodos, rolesAccess, Principal) {
        $scope.hasTema = false;

        if (temaToEdit) {
            $scope.title = "Editar Tema";
            $scope.tema = temaToEdit;
            $scope.hasTema = true;
        } else {
            $scope.title = "Cadastrar Tema"
            $scope.tema = {}
        }

        $scope.podeEditar = function () {
            return Principal.hasAnyAuthority(rolesAccess['TEMA'].editar);
        };

        EixoTematicoTodos.query({}, function (result) {
            $scope.eixos = result;
        });

        $scope.salvar = function () {
            if ($scope.hasTema) {
                Tema.update($scope.tema).$promise.then(function (result) {
                    Toast.sucesso("Tema atualizado com sucesso.");
                    $mdDialog.cancel();
                }).catch(function (erro) {
                    Toast.erro("Erro ao atualizar tema.");
                });
            } else {
                Tema.save($scope.tema).$promise.then(function (result) {
                    Toast.sucesso("Tema salvo com sucesso.");
                    $mdDialog.cancel();
                }).catch(function (error) {
                    if (error.data.messages) {
                        Toast.erro(error.data.messages[0]);
                    } else {
                        Toast.erro("Erro ao adicionar tema.");
                    }
                });
            }
        };

        $scope.novaEspecificacao = function () {
            if (!$scope.tema.especificacoes) {
                $scope.tema.especificacoes = [];
            }

            $scope.tema.especificacoes.push({nome: ""});
        };

        $scope.removerEspecificacao = function (index) {
            $scope.tema.especificacoes.splice(index, 1);
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
    });
