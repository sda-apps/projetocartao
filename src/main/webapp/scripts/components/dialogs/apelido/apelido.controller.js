'use strict';

angular.module('projetocartaoApp')
    .controller('ApelidoAgricultorController', function ($rootScope, $filter, $scope, $mdDialog, AgricultorEtnia) {

        if($rootScope.isAgricultor){
            $scope.titulo = $rootScope.account.agricultor.pessoaFisica.sexo === 'F'? 'Como você gostaria de ser chamada?': 'Como você gostaria de ser chamado?';
            $scope.nome = $filter('capitalize')($rootScope.account.agricultor.pessoaFisica.pessoa.nome.split(' ')[0]);
            /*if($rootScope.account.agricultor.etnia != null) {//TODO etnia
                $scope.etniaResposta = $rootScope.account.agricultor.etnia.id;
            }*/
            if($rootScope.account.agricultor.pessoaFisica.pessoa.apelido != null) {
                $scope.apelido = $rootScope.account.agricultor.pessoaFisica.pessoa.apelido;
            }
        }else {
            $scope.titulo = $rootScope.account.usuarioAvancado.pessoaFisica.sexo === 'F'? 'Como você gostaria de ser chamada?': 'Como você gostaria de ser chamado?';
            $scope.nome = $filter('capitalize')($rootScope.account.usuarioAvancado.pessoaFisica.pessoa.nome.split(' ')[0]);
        }

        $scope.setApelido = function() {
            if($rootScope.isAgricultor) {
                //$scope.etniaCompleta = $filter('filter')($scope.etnia, { id: $scope.etniaResposta })[0];
                $scope.resposta = {etnia: '' /*$scope.etniaCompleta*/, apelido: $scope.apelido};
                $mdDialog.hide($scope.resposta);
            }else {
                $mdDialog.hide($scope.apelido);
            }
        };

        /*$scope.getEtnia = function() {
            AgricultorEtnia.get(function (result) {
                $scope.etnia = result;
            });
        }*/

        /*if($rootScope.isAgricultor){
            $scope.getEtnia();
        }*/

    });
