'use strict';

angular.module('projetocartaoApp')
    .controller('CadastrarAcaoController', function ($rootScope, Principal, rolesAccess, $filter, $scope, $mdDialog,
                                                     acaoToEdit, DateUtils, Toast,AcaoPTA) {
        $scope.hasAcao = false;

        if (acaoToEdit) {
            $scope.title = "Editar Ação";
            $scope.acao = acaoToEdit;
            $scope.hasAcao = true;
        } else {
            $scope.title = "Cadastrar Ação"
        }

        $scope.podeEditar = function () {
            return Principal.hasAnyAuthority(rolesAccess['ACAO'].editar);
        };

        $scope.salvar = function () {
            if($scope.hasAcao) {
                AcaoPTA.update($scope.acao).$promise.then(function (result) {
                    Toast.sucesso("Ação atualizada com sucesso.");
                    $mdDialog.cancel();
                }).catch(function (erro) {
                    Toast.erro("Erro ao atualizar ação.");
                });
            } else {
                AcaoPTA.save($scope.acao).$promise.then(function (result) {
                    Toast.sucesso("Ação salva com sucesso.");
                    $mdDialog.cancel();
                }).catch(function (error) {
                    if(error.data.messages){
                        Toast.erro(error.data.messages[0]);
                    }else {
                        Toast.erro("Erro ao adicionar ação.");
                    }
                });
            }
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };
    });
