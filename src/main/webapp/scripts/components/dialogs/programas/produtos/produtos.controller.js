'use strict';

angular.module('projetocartaoApp')
	.controller('produtosController', 
		function ($scope, $state, $mdDialog, $mdMedia, produtos) {
			$scope.produtos = produtos;
			$scope.columns = function (){
				return Math.min($scope.produtos.templates.length, maxColumns());
			};
			var maxColumns = function(){
				if ($mdMedia('max-width: 499px')){
					return 1;
				} else if ($mdMedia('max-width: 749px')){
					return 2;
				} else if ($mdMedia('max-width: 999px')){
					return 3;
				} else {
					return 4;
				}
			}

			$scope.matriz = geraMatriz($scope.produtos.templates, $scope.columns());
			function geraMatriz(arr, size) {
				var novoArr = [];
				for (var i=0; i<arr.length; i+=size) {
					novoArr.push(arr.slice(i, i+size));
				}
				return novoArr;
			}

			$scope.cancel = function() {
				$mdDialog.cancel();
			};
		});
