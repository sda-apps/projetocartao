'use strict';

angular.module('projetocartaoApp')
    .controller('AlterarSenhaController', function ($scope, $rootScope, Auth, Principal, Toast, $mdDialog) {
        Principal.identity().then(function(account) {
            $scope.account = account;
        });

        $scope.success = null;
        $scope.error = null;
        $scope.doNotMatch = null;

        $scope.changePassword = function () {
            if ($scope.senha !== $scope.confirmPassword) {
                $scope.error = null;
                $scope.success = null;
                $scope.doNotMatch = 'ERROR';
                Toast.erro('Senha e confirmação de senha não conferem!');
            } else {
                $scope.doNotMatch = null;

                Auth.changePassword($scope.senha).then(function () {
                    $scope.error=null;
                    $scope.success='OK';
                    Toast.sucesso('Senha alterada com sucesso!');
                    $mdDialog.hide();
                }).catch(function(erro){
                    Toast.erro('Senha não foi alterada!');
                    $scope.success=null;
                    $scope.error='ERROR';

                });
            }
        };

        $scope.alterarSenhaDialoghide = function(){
            $mdDialog.hide();
        };

    });
