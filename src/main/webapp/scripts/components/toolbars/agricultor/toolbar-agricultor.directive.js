'use strict';

angular.module('projetocartaoApp')
    .directive('toolbarAgricultor', function() {
        return {
            restrict: 'E',
            templateUrl: 'scripts/components/toolbars/agricultor/toolbar-agricultor.html',
            controller: 'ToolbarAgricultorController'
        };
    });
