'use strict';

angular.module('projetocartaoApp')
    .controller('ToolbarAgricultorController', function ($scope, $rootScope, $mdDialog, $location, $state, Auth, Principal, identificadorRoles, menuService, $window, Agricultor, Toast) {
        Principal.identity().then(function(account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
            if ($rootScope.isAuthenticated()){
                $rootScope.isAgricultor = identificadorRoles.isAgricultor($rootScope.account.authorities);
            }
            /*if($rootScope.account.agricultor.pessoaFisica.sexo ==='F' && $rootScope.account.agricultor.etnia != null) {
                $scope.mudaEtniaDescricao($rootScope.account.agricultor.etnia);
            }*/
        });

        $scope.logout = function () {
            Auth.logout();
            $state.go('login-agricultor');
        };

        $scope.travarMenu = function () {
            menuService.menuTravadoAberto = !menuService.menuTravadoAberto;
        };

        $scope.fecharJanela = function(){
            $window.close();
        };

        $scope.showDialogCrop= function(){
            $mdDialog.show({
                templateUrl:'scripts/components/dialogs/crop/crop.html',
                backdrop : false,
                escapeToClose: true,
                controller: function($rootScope, $scope, Upload, $timeout, $mdDialog, Toast, Pessoa, $mdMedia){
                    $scope.isGtSm=$mdMedia('gt-xs');
                    // upload on file
                    $scope.upload = function (dataUrl, file) {
                        if(file){
                            if (!file.size>1048576) {
                                Toast.erro('Tamanho máximo da imagem 1MB');
                            }else{
                                var pessoa = $rootScope.account.agricultor.pessoaFisica.pessoa;
                                pessoa.foto=dataUrl;

                                Pessoa.update(pessoa).$promise
                                    .then(function(){
                                        $mdDialog.hide();
                                        $rootScope.account.agricultor.pessoaFisica.pessoa.foto = agricultor.pessoaFisica.pessoa.foto;

                                    })
                                    .catch(function(erro){
                                        $mdDialog.hide();
                                        var msg = erro.data.status + ' - ' + erro.data.error;
                                        Toast.erro(msg);
                                    });
                            }
                        }
                    };
                    $scope.cropDialoghide = function(){
                        $mdDialog.hide();
                    };

                    $scope.validarArquivo = function(file){
                        if(file){
                            if (!file.type.startsWith("image")) {
                                Toast.erro('Formato de arquivo inválido');
                            }
                        }
                    }
                }
            });
        };

        $scope.showDialogApelido= function(){
            $mdDialog.show({
                templateUrl:'scripts/components/dialogs/apelido/apelido.html',
                backdrop : false,
                escapeToClose: false,
                controller: "ApelidoAgricultorController"
            }).then(function(answer) {
                var agricultor = $rootScope.account.agricultor;
                agricultor.pessoaFisica.pessoa.apelido = answer.apelido;
                agricultor.etnia = answer.etnia;
                Agricultor.update(agricultor).$promise.then(function(){
                    $rootScope.account.agricultor.pessoaFisica.pessoa.apelido = answer.apelido;
                    /*$rootScope.account.agricultor.etnia.id = answer.etnia.id;
                    $rootScope.account.agricultor.etnia.nome = answer.etnia.nome;
                    $rootScope.account.agricultor.etnia.codigo = answer.etnia.codigo;
                    if($rootScope.account.agricultor.pessoa.sexo ==='F'){
                        $scope.mudaEtniaDescricao(answer.etnia);
                    }else{
                        $rootScope.account.agricultor.etnia.descricao = answer.etnia.descricao;
                    }*/
                })
                    .catch(function(erro){
                        var msg = erro.data.status + ' - ' + erro.data.error;
                        Toast.erro(msg);
                    });
            }, function() {
                //cancel
            });
        };

        /*$scope.mudaEtniaDescricao = function(etnia){
            switch(etnia.codigo) {
                case 1:
                    $rootScope.account.agricultor.etnia.descricao = "PESCADORA"
                    break;
                case 3:
                    $rootScope.account.agricultor.etnia.descricao = "AQUICULTORA"
                    break;
                case 5:
                    $rootScope.account.agricultor.etnia.descricao = "AGRICULTORA FAMILIAR"
                    break;
                default:
                    $rootScope.account.agricultor.etnia.descricao = etnia.descricao;
            }
        };*/

        $scope.ultimasMensagens = [{
            nome: "Paulo Queiroz",
            tempo: "15 minutos atrás"

        },{
            nome: "Rodrigo",
            tempo: "1 hora atrás"

        },{
            nome: "Paulo Nunez Valente",
            tempo: "1 dia atrás"

        },{
            nome: "Alberto Sales",
            tempo: "2 dias atrás"

        }];

    });
