'use strict';

angular.module('projetocartaoApp')
    .directive('toolbarUA', function() {
        return {
            restrict: 'E',
            templateUrl: 'scripts/components/toolbars/usuario-avancado/toolbar-usuario-avancado.html',
            controller: 'ToolbarUAController'
        };
    });
