'use strict';

angular.module('projetocartaoApp')
    .controller('ToolbarUAController', function ($scope, $rootScope, $mdDialog, $location, $state, Auth, Principal, identificadorRoles, menuService, User) {
        Principal.identity().then(function(account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
            if ($rootScope.isAuthenticated()){
                $rootScope.isAgricultor = identificadorRoles.isAgricultor($rootScope.account.authorities);
            }
        });

        $scope.logout = function () {
            Auth.logout();
            $state.go('login');
        };

        $scope.travarMenu = function () {
        	menuService.menuTravadoAberto = !menuService.menuTravadoAberto;
        };

        $scope.showDialogAlterarSenha = function(){
            $mdDialog.show({
                templateUrl:'scripts/components/dialogs/alterar-senha/alterar-senha.html',
                backdrop : false,
                escapeToClose: false,
                clickOutsideToClose:true,
                controller: "AlterarSenhaController"
            });
        };

        $scope.mostraDialogDadosUsuarioAvancado = function() {
            User.get({id: $rootScope.account.id}).$promise
                .then(function (result) {
                    $mdDialog.show({
                        controller: 'CadastrarUserCompletoController',
                        templateUrl: 'scripts/components/dialogs/usuario-avancado/cadastrar-user-completo/cadastrar-user-completo.html',
                        parent: angular.element(document.body),
                        clickOutsideToClose: true,
                        disableParentScroll: true,
                        locals: {
                            userToEdit: result,
                            canEditPerfil: false
                        },
                        fullscreen: true
                    })
                        .then(function(answer) {

                        }, function() {

                        });
                }).catch(function(response) {
            });

        };

        $scope.showDialogCrop= function(){
            $mdDialog.show({
                templateUrl:'scripts/components/dialogs/crop/crop.html',
                backdrop : false,
                escapeToClose: true,
                controller: function($rootScope, $scope, Upload, $timeout, $mdDialog, Toast, Pessoa, $mdMedia){
                    $scope.isGtSm=$mdMedia('gt-xs');
                    // upload on file
                    $scope.upload = function (dataUrl, file) {
                        if(file){
                            if (!file.size>1048576) {
                                Toast.erro('Tamanho máximo da imagem 1MB');
                            }else{
                            	var pessoa = $rootScope.account.usuarioAvancado.pessoaFisica.pessoa;
                                pessoa.foto = dataUrl;

                                Pessoa.update(pessoa).$promise
                                    .then(function(){
                                        $mdDialog.hide();
                                        $rootScope.account.usuarioAvancado.pessoaFisica.pessoa.foto = pessoa.foto;
                                    })
                                    .catch(function(erro){
                                        $mdDialog.hide();
                                        var msg = erro.data.status + ' - ' + erro.data.error;
                                        Toast.erro(msg);
                                    });
                            }
                        }
                    };
                    $scope.cropDialoghide = function(){
                        $mdDialog.hide();
                    };

                    $scope.validarArquivo = function(file){
                        if(file){
                            if (!file.type.startsWith("image")) {
                                Toast.erro('Formato de arquivo inválido');
                            }
                        }
                    }
                }
            });
        };
    });
