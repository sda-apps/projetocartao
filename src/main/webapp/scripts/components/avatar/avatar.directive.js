'use strict';

angular.module("projetocartaoApp")
    .directive('avatar', function() {
    return {
        restrict: 'E',
        templateUrl: 'scripts/components/avatar/avatar.html',
        scope:{
            name:'@',
            image:'@',
            fontSize:'@',
            size:'@',
            height: '@',
            width: '@',
            colorMode:'@',
            color: '@',
            colorIndex: '@',
            type: '@',
            logo: '@'
        },
        link: function($scope, $element, attr){

            $scope.colors=['#F44336','#E91E63','#9C27B0','#673AB7','#3F51B5','#2196F3','#03A9F4','#00BCD4','#009688','#4CAF50','#8BC34A','#FF9800','#FF5722','#795548','#9E9E9E','#607D8B'];
            $scope.avatarStyle = "";

            if($scope.size){
                $scope.height = $scope.size;
                $scope.width = $scope.size;
            }
            if(!$scope.height){
                $scope.height = "50px";
            }
            if(!$scope.width){
                $scope.width = "50px";
            }
            if(!$scope.fontSize){
                $scope.fontSize = "40px";
            }

            $scope.$watch("name", function(){
                if($scope.name) {
                    $scope.initial = $scope.name.toUpperCase().substr(0, 1);
                }
                $scope.setAvatarStyle();
            });

            $scope.getRandomColor = function(){
                return $scope.colors[Math.floor(Math.random() * $scope.colors.length)];
            }

            $scope.setAvatarStyle = function(){
                var style = "";
                var currentColor = "";
                var height = "", width = "";

                if($scope.color){
                    currentColor = $scope.color;
                }
                else if($scope.colorIndex){
                    currentColor = $scope.colors[$scope.colorIndex];
                }
                else{
                    currentColor = $scope.getRandomColor();
                }

                if($scope.colorMode && $scope.colorMode === "text"){
                    style += "color: " + currentColor + ";";
                }
                else{
                    style += "background-color: " + currentColor + ";";
                }
                style += " height: "+ $scope.height + "!important; width: " + $scope.width + "!important; line-height: " + $scope.height + "; font-size: " + $scope.fontSize + " !important;";
                $scope.avatarStyle = style;
            }

            $scope.setAvatarStyle();

        }
    }
});
