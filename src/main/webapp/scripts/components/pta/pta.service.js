'use strict';

angular.module('projetocartaoApp')
    .factory('SalvarPta', function ($resource) {
        return $resource('api/pta/:id', {id: '@id'}, {
            'post': {method: 'POST'},
            'update': {method: 'PUT'}
        });
    })

    .factory('PlanoTrabalhoAnualPorId', function ($resource) {
        return $resource('api/pta/:id', {}, {
            'get': {
                method: 'GET'
            }
        });
    })

    .factory('SalvarTecnico', function ($resource) {
        return $resource('api/tecnicopta/:id', {id: '@id'}, {
            'post': {method: 'POST'},
            'update': {method: 'PUT'}
        });
    })

    .factory('DeletarTecnico', function ($resource) {
        return $resource('api/tecnicopta/:id', {}, {
            'delete': {
                method: 'DELETE'
            }
        });
    })

    .factory('TecnicosPorPTA', function ($resource) {
        return $resource('api/tecnicopta/pta/:id', {}, {
            'get': {method: 'GET', isArray: true}
        });
    })

    .factory('ComunidadesPorMunicipio', function ($resource) {
        return $resource('api/ater/comunidade', {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    })

    .factory('RelatorioAtcs', function ($resource) {
        return $resource('api/pta/relatorio-atcs/', {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    })

    .factory('RelatorioPTA', function ($http, $window) {
        return {
            gerarRelatorioPTA:function(url, ptaId) {
                var newWindow = $window.open('#/loading', '_blank');

                $http.get(url + ptaId, {responseType:'arraybuffer'})
                    .success(function (response) {
                        var file = new Blob([response], {type: 'application/pdf'});
                        var fileURL = URL.createObjectURL(file);

                        newWindow.location.href = fileURL;
                    });
            }
        };
    })

    .factory('CountMetasPorAtc', function ($resource) {
        return $resource('api/pta/count-metas-atcs', {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    })

    .factory('MunicipioPorUf', function ($resource) {
        return $resource('api/municipios/uf/:uf', {}, {
            'query': {
                method: 'GET',
                isArray: true
            }
        });
    })

    .factory('ComunidadesPorMunicipioEPta', function ($resource) {
        return $resource('api/pta/comunidade/:municipio/:ptaId', {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    });
