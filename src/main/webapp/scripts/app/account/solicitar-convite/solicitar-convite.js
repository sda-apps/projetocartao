'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('solicitar-convite', {
                parent: 'account',
                url: '/solicitar-convite',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/account/solicitar-convite/solicitar-convite.html',
                        controller: 'SolicitarConviteController'
                    }
                },
                resolve: {

                }
            });
    });
