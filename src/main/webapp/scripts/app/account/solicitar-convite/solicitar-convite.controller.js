'use strict';

angular.module('projetocartaoApp')
    .controller('SolicitarConviteController', function ($rootScope, $scope, $state, $timeout, Auth, MessageUtils, UserPorCpf, UserPublic) {
        $scope.exibirTodos = false;
        $scope.botao = "Verificar Cadastro";
        $scope.disableAll = false;
        $scope.desableButton = false;

        $scope.submitForm = function () {
            switch ($scope.botao){
                case "Redefinir Senha":
                    $state.go('requestReset');
                    break;
                case "Solicitar Convite":
                    $scope.user.analise = true;
                    UserPublic.save($scope.user).$promise.then(function (result) {
                        $scope.mensagem = "Obrigado. Estamos analisando sua solicitação, caso aprovada você receberá um convite para acesso ao sistema em sua conta de e-mail";
                        $scope.exibirTodos = false;
                    }).catch(function (error) {
                    	$scope.mensagem = MessageUtils.getMessages(error.data.messages)[0];
                    });

                    break;
                default:
                    UserPorCpf.get({cpf: $scope.user.usuarioAvancado.pessoaFisica.cpf}).$promise.then(function (result) {
                        $scope.mensagem = MessageUtils.getMessages(result.messages)[0];
                        $scope.desableButton = true;
                    })
                    .catch(function (error) {
                        switch(error.status){
                            case 404:
                                $scope.exibirTodos = true;
                                $scope.botao = "Solicitar Convite";
                                break;
                            case 302:
                                $scope.mensagem = MessageUtils.getMessages(error.data.messages)[0];
                                $scope.botao = "Redefinir Senha";
                                break;
                            case 303:
                                $scope.exibirTodos = true;
                                $scope.botao = "Solicitar Convite";
                                $scope.user.usuarioAvancado = error.data;
                                $scope.disableAll = true;
                                break;
                            default:
                                console.log("error", error);
                        }
                    });
            }
        };
    });
