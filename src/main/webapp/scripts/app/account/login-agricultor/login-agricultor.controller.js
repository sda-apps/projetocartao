'use strict';

angular.module('projetocartaoApp')
    .controller('LoginAgricultorController', function ($rootScope, $scope, $state, $timeout, Auth, $window, deviceDetector) {
        $scope.user = {};
        $scope.errors = {};
        $scope.rememberMe = true;
        $scope.browserNaoSuportado = false;
        $scope.versaoNaoSuportada = false;

        $scope.verificaBrowser = function () {
            if(deviceDetector.browser !== "chrome"){
                $scope.browserNaoSuportado = true;
            }else {
                if((deviceDetector.browser_version.substring(0,2) < 52 && deviceDetector.browser == "chrome")){
                    $scope.versaoNaoSuportada = true;
                }
            }
        };

        $scope.verificaBrowser();

        $scope.login = function (event, form) {
            if($scope.username){
                if($scope.senha){
                    event.preventDefault();
                    Auth.login({
                        username: $scope.username,
                        senha: $scope.senha,
                        rememberMe: $scope.rememberMe
                    }).then(function () {
                        $scope.authenticationError = false;
                        if ($rootScope.previousStateName === 'register') {
                            $state.go('main-agricultor');
                        } else {
                            $rootScope.back();
                        }
                    }).catch(function () {
                        if($scope.username != null && $scope.senha != null)
                        $scope.authenticationError = true;
                    });
                }
                else{
                    form.senha.$setValidity('requerido', false);
                }
            }
            else{
               form.username.$setValidity('requerido', false);
            }
        };
    });
