'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('login-agricultor', {
                parent: 'account',
                url: '/login-agricultor',
                data: {
                    authorities: [],
                    pageTitle: 'Acesso'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/account/login-agricultor/login-agricultor.html',
                        controller: 'LoginAgricultorController'
                    }
                },
                resolve: {

                }
            });
    });
