'use strict';

angular.module('projetocartaoApp')
    .controller('RequestResetController', function ($rootScope, $scope, $state, $timeout, Auth, MessageUtils) {

        $scope.success = null;
        $scope.email = null;
        $scope.errorMessages = [];
        $scope.sucessMessages = [];
        $scope.error = null;

        $scope.requestReset = function () {
        	$scope.errorMessages = [];
            $scope.sucessMessages = [];
            
            Auth.resetPasswordInit($scope.email).then(function (response) {
                $scope.success = 'OK';
                $scope.error = null;
                $scope.sucessMessages = MessageUtils.getMessages(response.messages);
            }).catch(function (response) {
                $scope.success = null;
                $scope.error = 'ERROR';
                $scope.errorMessages = MessageUtils.getMessages(response.data.messages);
            });
        }

    });
