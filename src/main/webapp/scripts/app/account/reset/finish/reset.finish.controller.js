'use strict';

angular.module('projetocartaoApp')
    .controller('ResetFinishController', function ($scope, $stateParams, $timeout, Auth, Toast, $state) {
        $scope.keyMissing = $stateParams.key === undefined;
        $scope.doNotMatch = null;

        if($scope.keyMissing){
            Toast.erro('Está faltando a chave de redefinição de senha.');
        }

        $scope.resetAccount = {};

        $scope.finishReset = function() {
            if ($scope.resetAccount.senha !== $scope.confirmPassword) {
                $scope.doNotMatch = 'ERROR';
                Toast.erro('A senha e sua confirmação não coincidem!');
            } else {
                Auth.resetPasswordFinish({key: $stateParams.key, newPassword: $scope.resetAccount.senha}).then(function () {
                    $scope.success = 'OK';
                    Toast.sucesso('Sua senha foi redefinida.');
                    $state.go('login');

                }).catch(function (response) {
                    $scope.success = null;
                    $scope.error = 'ERROR';
                    Toast.erro('Sua senha não pode ser trocada. Lembre-se que um pedido de senha é válido apenas por 24 horas.');

                });
            }

        };
    });
