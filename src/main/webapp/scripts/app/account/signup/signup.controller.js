'use strict';

angular.module('projetocartaoApp')
    .controller('SignupFinishController', function ($scope, $state, $stateParams, $timeout, Auth, MessageUtils, Toast, Principal) {

        $scope.keyMissing = $stateParams.key === undefined;
        $scope.doNotMatch = null;

        $scope.errors = {};
        $scope.errorMessages = [];

        if ($scope.keyMissing) {
            $state.go('home');
        }

        Principal.identity().then(function(account) {
            if (Principal.isAuthenticated){
            	Auth.logout();
            }
        });

        $scope.finishSignup = function(event, form) {
        	$scope.errorMessages = [];
            if ($scope.password !== $scope.confirmPassword) {
                $scope.doNotMatch = 'ERROR';
            } else {
            	$scope.doNotMatch = null;
                Auth.signupFinish({key: $stateParams.key, login: $scope.username, password: $scope.password})
                    .then(function () {
                    $scope.success = 'OK';
                    $scope.error = null;
                    $state.go('login');
                    Toast.sucesso("Seu cadastro foi efetuado com sucesso.");
                }).catch(function (response) {
                    $scope.success = null;
                    $scope.error = 'ERROR';
                    $scope.errorMessages = MessageUtils.getMessages(response.data.messages);
                });
            }
        };

        $scope.$watch('username', function(newUsername){
        	var regex = new RegExp(/^[a-z0-9\.]+$/i);
        	if (regex.test(newUsername)) {
				$scope.signupForm.username.$setValidity('invalid', true);
			} else {
				$scope.signupForm.username.$setValidity('invalid', false);
			}

        	if (newUsername && newUsername.length >= 6 && newUsername.length <= 50) {
				$scope.signupForm.username.$setValidity('invalidLength', true);
			} else {
				$scope.signupForm.username.$setValidity('invalidLength', false);
			}
        }, true);

        $scope.$watch('password', function(newPassword){
        	if (newPassword && newPassword.length >= 6 && newPassword.length <= 50) {
				$scope.signupForm.password.$setValidity('invalid', true);
			} else {
				$scope.signupForm.password.$setValidity('invalid', false);
			}
        }, true);

    });
