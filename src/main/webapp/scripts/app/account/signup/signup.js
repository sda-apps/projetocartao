'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('finishSignup', {
                parent: 'account',
                url: '/signup/finish?key',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/account/signup/signup.html',
                        controller: 'SignupFinishController'
                    }
                },
                resolve: {
                    
                }
            });
    });
