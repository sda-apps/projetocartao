'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('settings', {
                parent: 'account',
                url: '/settings',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_AGRI', 'ROLE_AGRI_REST'],
                    pageTitle: 'Configurações'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/account/settings/settings.html',
                        controller: 'SettingsController'
                    }
                },
                resolve: {

                }
            });
    });
