'use strict';

angular.module('projetocartaoApp')
    .controller('DapsAgricultorController',
        function ($scope, $rootScope, $state, Principal, $filter, AgricultorDaps, $mdMedia, identificadorRoles, $timeout) {

            $scope.exibir=[false,false];

            Principal.identity().then(function(account) {
                $rootScope.account = account;
                $rootScope.isAuthenticated = Principal.isAuthenticated;
                if ($rootScope.isAuthenticated()){
                    $rootScope.isAgricultor = identificadorRoles.isAgricultor($rootScope.account.authorities);
                }
                getDapsAgricultor();
            });

            var getDapsAgricultor = function() {
                AgricultorDaps.get({agricultorId: $rootScope.account.agricultor.id}).$promise.then( function (result) {
                    var daps = [];
                    angular.forEach(result, function(dap){
                        daps.push(dap);
                        $scope.exibir.push(false);
                    });
                    $rootScope.account.agricultor.daps=daps;
                })
                .catch(function(){
                    $rootScope.account.agricultor.daps = []
                });
            };

            $scope.expansaoDaps = function(id) {
                $scope.exibir[id] = !$scope.exibir[id];
            };

            $scope.screenIsSmall = $mdMedia('xs');
            $scope.introOptions = {
                steps:[
                    {
                        element: '#step1',
                        intro: 'Aqui você visualiza as informações da sua DAP atual válida.',
                        position: 'auto'
                    },
                    {
                        element: $scope.screenIsSmall ? '#step3' : '#step2',
                        intro: 'No histórico de DAPs você encontra todas as DAPs emitidas, com a data de emissão, data de expiração e o modelo da dap.',
                        position: 'top'
                    }
                ],
                showStepNumbers: false,
                exitOnOverlayClick: false,
                exitOnEsc: true,
                nextLabel: 'Avançar',
                prevLabel: 'Voltar',
                skipLabel: 'Encerrar',
                doneLabel: 'Encerrar'
            };

            var hasDap = true;
            $scope.introOnStart = function(){
                if(!$rootScope.account.agricultor.daps || $rootScope.account.agricultor.daps.length == 0){
                    hasDap = false;
                    $rootScope.account.agricultor.daps = [];
                    $rootScope.account.agricultor.daps.push(
                        {
                            modelo: "9.2.2 - GS 2014 Nova",
                            dataEmissao: "2016-04-05T03:00:00.000+0000",
                            dataExpiracao: "2016-04-14T03:00:00.000+0000",
                            numero: "CE99999999999999999999999",
                            tempoValidade: "5 Ano(s)"
                        }
                    );
                }
                $rootScope.modoTutorial = true;

                $timeout(function () {
                    $scope.introStart();
                }, 100);
            };

            $scope.introOnExit = function () {
                if(!hasDap){
                    hasDap=true;
                    $rootScope.account.agricultor.daps = [];
                }
                $rootScope.modoTutorial = false;
            };

            $rootScope.introOnStart = $scope.introOnStart;
        }
    );
