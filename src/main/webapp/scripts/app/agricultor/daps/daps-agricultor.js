'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('daps-agricultor', {
                parent: 'agricultor',
                url: '/daps-agricultor',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_AGRI', 'ROLE_AGRI_REST', 'ROLE_GESTOR', 'ROLE_TECNICO', 'ROLE_ADMIN'],
                    pageTitle: 'Histórico de Daps'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/agricultor/daps/daps-agricultor.html',
                        controller: 'DapsAgricultorController'
                    }
                },
                resolve: {

                }
            });
    });
