'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('programas-agricultor', {
                parent: 'programas',
                url: '/programas-agricultor',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_AGRI', 'ROLE_AGRI_REST', 'ROLE_GESTOR', 'ROLE_TECNICO', 'ROLE_ADMIN', 'ROLE_COORDENADOR_ATER'],
                    pageTitle: 'Meus Programas e Projetos'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/agricultor/programas/meus-programas/meus-programas.html',
                        controller: 'ProgramasAgricultorController'
                    }
                },
                resolve: {

                }
            });
    });
