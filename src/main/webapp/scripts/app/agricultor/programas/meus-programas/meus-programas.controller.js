'use strict';

angular.module('projetocartaoApp')
	.controller('ProgramasAgricultorController',
		function ($scope, $rootScope, $state, Principal, Auth, $filter, $mdDialog, $mdMedia, AgricultorProjetos, AgricultorProjetosProvider, projetosConstants, $sce, projetosCores, identificadorRoles, $timeout) {

            $scope.agricultorProjetos = [];

            Principal.identity().then(function(account) {
                $rootScope.account = account;
                $rootScope.isAuthenticated = Principal.isAuthenticated;
                if ($rootScope.isAuthenticated()){
                    $rootScope.isAgricultor = identificadorRoles.isAgricultor($rootScope.account.authorities);
                }
            });
			var getAgricultorProjetos = function() {
                try {
                    AgricultorProjetos.get({agricultorId: $rootScope.account.agricultor.id}, function (result){
                        $scope.agricultorProjetos = AgricultorProjetosProvider.listaAgricultorProjetos(result);
                        $scope.projetosAno = templateProjetos(result);
                    });
                } catch(e) {
                    console.log("ERRO AgricultorProjetos", e);
                }
            };
            
            $scope.isProgramaCisterna = function(codigoSda) {
            	var result = false;
            	
            	if (codigoSda == projetosConstants.UM_MILHAO_CISTERNAS.id ||
        			codigoSda == projetosConstants.CISTERNAS_ENXURRADA.id ||
        			codigoSda == projetosConstants.CISTERNAS_PLACAS.id ||
        			codigoSda == projetosConstants.AGUA_PARA_TODOS.id) {
            		result = true;
				}
            	
            	return result;
            };

            $scope.listSelecao = function(codigoSda){
                switch(codigoSda){
                    case projetosConstants.HORA_PLANTAR.id:
                        $state.go('hora-plantar');
                        break;
                    case projetosConstants.PAA_ALIMENTOS.id:
                        $state.go('paa-alimentos');
                        break;
                    case projetosConstants.PAA_LEITE.id:
                        $state.go('paa-leite');
                        break;
                    case projetosConstants.UM_MILHAO_CISTERNAS.id:
                    case projetosConstants.CISTERNAS_ENXURRADA.id:
                        $state.go('segunda-agua');
                        break;
                    case projetosConstants.CISTERNAS_PLACAS.id:
                        $state.go('cisternas');
                        break;
                    case projetosConstants.AGUA_PARA_TODOS.id:
                        $state.go('cisternas');
                        break;
                    case projetosConstants.PENAE.id:
                        $state.go('pnae');
                        break;
                    case projetosConstants.GARANTIA_SAFRA.id:
                        $state.go('garantia-safra');
                        break;
                    case projetosConstants.CAJU.id:
                        $state.go('caju');
                        break;
                    case projetosConstants.BIODIESEL.id:
                        $state.go('biodiesel');
                        break;
                    default:

                }
            };

            var templateProjetos = function(projetos){
                var templates = [];
                for (var i=0;i<projetos.length;i++){
                    if(!templates[projetos[i].ano]){
                        templates[projetos[i].ano] =
                            '<ul>' +
                            '   <li style="font-size: 18px; color: '+projetosCores.getCor(projetos[i].projeto.codigoSda)+'">' +
                            '       <span style="font-size: 13px; color: black;">' +$filter('capitalize')(projetos[i].projeto.nome) +'</span>' +
                            '   </li>';
                    }else{
                        templates[projetos[i].ano] = templates[projetos[i].ano].concat(
                            '<li style="font-size: 18px; color: '+projetosCores.getCor(projetos[i].projeto.codigoSda)+'">' +
                            '   <span style="font-size: 13px; color: black;">'+ $filter('capitalize')(projetos[i].projeto.nome) +'</span>' +
                            '</li>');
                    }
                }
                var templeteProjetos = [];
                for (var i in templates){
                    templeteProjetos.unshift({
                        ano:i,
                        template: $sce.trustAsHtml(templates[i]+'</ul>')
                    });
                }
                return templeteProjetos;
            };

            getAgricultorProjetos();

            $scope.introOptions = {
                steps:[
                    {
                        element: '#steps13',
                        intro: 'Em meus programas e projetos você tem acesso aos programas que você participa ou já participou. Na lista, você visualiza a sua situação atual e qual foi o ano de entrada naquele programa.',
                        position: 'auto'
                    },
                    {
                        element: '#step2',
                        intro: 'Você pode visualizar os resultados em forma de lista ou linha do tempo.',
                        position: 'auto'
                    },
                    {
                        element: '#steps13',
                        intro: 'Na linha do tempo você verá um resumo dos programas que você participa/participou em cada ano.',
                        position: 'auto'
                    }
                ],
                showStepNumbers: false,
                exitOnOverlayClick: false,
                exitOnEsc: true,
                nextLabel: 'Avançar',
                prevLabel: 'Voltar',
                skipLabel: 'Encerrar',
                doneLabel: 'Encerrar'
            };

            var hasPrograma = true;
            $scope.introOnStart = function(){
                if(!$scope.agricultorProjetos || $scope.agricultorProjetos == 0){
                    hasPrograma = false;
                    $scope.agricultorProjetos = [
                        {"codigoSda":10,"nome":"GARANTIA SAFRA","status":"Ativo","ano":2015},
                        {"codigoSda":8,"nome":"PAA LEITE","status":"Ativo","ano":2015},
                        {"codigoSda":3,"nome":"PROGRAMA HORA DE PLANTAR","status":"Ativo","ano":2016},
                        {"codigoSda":1,"nome":"PAA ALIMENTOS","status":"Ativo","ano":2016}
                    ];
                    $scope.projetosAno = [
                        {
                            "ano":"2016",
                            "template": '<ul><li style="font-size: 18px; color: #c8a1b3"><span style="font-size: 13px; color: black;">PAA Alimentos</span></li><li style="font-size: 18px; color: #c8a1b3"><span style="font-size: 13px; color: black;">Programa Hora de Plantar</span></li></ul>'
                        },
                        {
                            "ano":"2015",
                            "template": '<ul><li style="font-size: 18px; color: #c8a1b3"><span style="font-size: 13px; color: black;">Gantia Safra</span></li><li style="font-size: 18px; color: #c8a1b3"><span style="font-size: 13px; color: black;">PAA Leite</span></li></ul>'
                        }
                    ]
                }
                $rootScope.modoTutorial = true;
                $timeout(function () {
                    $scope.introStart();
                }, 100);
            };
            $scope.introOnExit = function () {
                $scope.switch = false;
                if(!hasPrograma){
                    hasPrograma=true;
                    $scope.projetosAno = [];
                    $scope.agricultorProjetos = [];
                }
                $rootScope.modoTutorial = false;
            };
            $scope.introOnBeforeChange = function (targetElement, scope) {
                switch (this._currentStep){
                    case 0:
                        $scope.switch = false;
                        break;
                    case 1:
                        $scope.switch = !$scope.switch;
                        break;
                    case 2:
                        $scope.switch = true;
                        break;
                }
            };
            $rootScope.introOnStart = $scope.introOnStart;
		}
	);
