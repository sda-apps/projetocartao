'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('paa-leite', {
                parent: 'programas',
                url: '/paa-leite',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_AGRI', 'ROLE_AGRI_REST', 'ROLE_GESTOR', 'ROLE_TECNICO', 'ROLE_ADMIN', 'ROLE_COORDENADOR_ATER'],
                    pageTitle: 'PAA Leite'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/agricultor/programas/paa-leite/paa-leite.html',
                        controller: 'paaLeiteController'
                    }
                }
            });
    });
