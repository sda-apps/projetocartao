'use strict';

angular.module('projetocartaoApp')
	.controller('paaLeiteController',
		function ($scope, $rootScope, $state, $mdDialog, $mdMedia, $sce, NotasProjetos, $filter, projetosConstants, produtosConstants, ServicosExtrato, identificadorRoles, Principal) {
            Principal.identity().then(function(account) {
                $rootScope.account = account;
                $rootScope.isAuthenticated = Principal.isAuthenticated;
                if ($rootScope.isAuthenticated()){
                    $rootScope.isAgricultor = identificadorRoles.isAgricultor($rootScope.account.authorities);
                }
            });

            $scope.dispMovel = $mdMedia('xs');
            $scope.showDialogProdutos = function (ano){
                var produtos = {ano: ano, templates:$scope.produtosAno[ano]};
                $mdDialog.show({
                    controller: 'produtosController',
                    templateUrl: 'scripts/components/dialogs/programas/produtos/produtos.html',
                    parent: angular.element(document.body),
                    locals: {
                        produtos: produtos
                    },
                    clickOutsideToClose: true,
                    disableParentScroll: true,
                    fullscreen: false
                })
                .then(function(answer) {
                    //return
                }, function() {
                    //cancel
                });
            };

            $scope.showDialogSobre = function(ev) {
                $mdDialog.show({
                    controller: 'paaLeiteController',
                    templateUrl: 'scripts/components/dialogs/programas/paa-leite/paa-leite.template.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    disableParentScroll: true,
                    fullscreen: true
                })
                .then(function(answer) {
                    //return
                }, function() {
                    //cancel
                });
            };

            $scope.onChangeSwitch = function(){
                $scope.notasAno.reverse();
            };

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            var getNotasProjeto = function() {
                try {
                    NotasProjetos.get({agricultorId: $rootScope.account.agricultor.id, projetoCodigoSDA: projetosConstants.PAA_LEITE.id}, function (result){
                        $scope.notasAno = getTemplatesNotasAno(result);
                        $scope.produtosAno = getTemplatesProdutos(result);
                    });
                } catch(e) {
                    console.log("ERRO notasProjeto", e);
                }
            };

            var getTemplatesNotasAno = function(notas){
                var notasAno = [];

                for (var i=0;i<notas.length;i++){
                    if(notasAno[notas[i].ano]){
                        notasAno[notas[i].ano].valorTotal += notas[i].valorTotal;
                    } else {
                        notasAno[notas[i].ano] = {
                            valorTotal: notas[i].valorTotal,
                            situacao: notas[i].situacao,
                            litros: 0,
                            haBovino: '',
                            haCaprino: ''
                        };
                    }

                    for (var j in notas[i].itensNota){
                        notasAno[notas[i].ano].litros += notas[i].itensNota[j].quantidade;

                        if(!notasAno[notas[i].ano].haBovino) {
                            notasAno[notas[i].ano].haBovino = (notas[i].itensNota[j].produtoEspecificacao.codigoSda == produtosConstants.LEITE_BOVINO.id ? 'BOVINO' : '');
                        }

                        if(!notasAno[notas[i].ano].haCaprino) {
                            notasAno[notas[i].ano].haCaprino = (notas[i].itensNota[j].produtoEspecificacao.codigoSda == produtosConstants.LEITE_CAPRINO.id ? 'CAPRINO' : '');
                        }
                    }
                }

                var templatesNotasAno = [];

                for (var i in notasAno){
                    var separador = (notasAno[i].haBovino != '' && notasAno[i].haCaprino != '') ? ' e ' : '';

                    templatesNotasAno.unshift({
                        ano: i,
                        template:   '<p>Tipo: <b>'+notasAno[i].haBovino + separador + notasAno[i].haCaprino+'</b></p>'+
                                    '<p>Valor: <b>'+$filter('currency')(notasAno[i].valorTotal, 'R$ ', 2) +'</b></p>'+
                                    '<p>Litros: <b>'+ notasAno[i].litros +'</b><p>'
                    });
                }
                return templatesNotasAno;
            };

            var getTemplatesProdutos = function(notas){
                var templatesProdutos = [];
                for (var i=0;i<notas.length;i++){
                    if(!templatesProdutos[notas[i].ano]){
                        templatesProdutos[notas[i].ano] = [];
                    }
                    for(var j in notas[i].itensNota){
                        var produtoAux = {
                            tanque: notas[i].itensNota[j].tanque,
                            nome: notas[i].itensNota[j].produtoEspecificacao.produto.nome,
                            especificacao: notas[i].itensNota[j].produtoEspecificacao.descricao,
                            quantidade: notas[i].itensNota[j].quantidade,
                            unidade: notas[i].itensNota[j].produtoEspecificacao.unidade.sigla,
                            recebimento: $filter('date')(notas[i].dataRecebimento, "dd/MM/yyyy"),
                            valor: notas[i].itensNota[j].valor
                        };
                        templatesProdutos[notas[i].ano].push({
                            template:   '<p>'+(produtoAux.recebimento ? produtoAux.recebimento : "")+'</p>'+
                                        '<span>Tanque: <b>'+(produtoAux.tanque ? produtoAux.tanque : "")+'</b></span>'+
                                        '<span>Especificação: <b>'+produtoAux.especificacao+'</b></span>'+
                                        '<span>Quantidade: <b>'+$filter('number')(produtoAux.quantidade, 1)+ ' ' +produtoAux.unidade+'</b></span>'+
                                        '<span>Valor Unitário: <b>'+$filter('currency')(produtoAux.valor, 'R$ ', 2)+'</b></span>'+
                                        '<span>Valor Total: <b>'+$filter('currency')((produtoAux.valor * produtoAux.quantidade), 'R$ ', 2)+'</b></span>'
                        });
                    }
                }
                return templatesProdutos;
            };

            $scope.emitirExtratoPAALeite = function () {
                ServicosExtrato.getExtratoPrograma('api/servicos/extrato/paa-leite/', $rootScope.account.agricultor.id);
            };
            getNotasProjeto();

            $scope.introOptions = {
                steps:[
                    {
                        element: '#sinopse',
                        intro: 'Aqui você tem um resumo sobre o programa e pode visualizar mais detalhes clicando em Ver Mais.'
                    },
                    {
                        element: $scope.dispMovel? '#step2b': '#step2a',
                        intro: 'Clicando no manual você pode baixar o arquivo pro seu computador, visualizar online ou imprimir.'
                    },
                    {
                        element: '#layout-programas-emitir-extrato',
                        intro: 'Aqui você pode emitir um extrato que detalha sua participação neste programa.'
                    },
                    {
                        element: '#step4',
                        intro: 'É possível ordenar a linha do tempo do ano mais recente pro mais antigo ou do mais antigo para o mais recente.'
                    },
                    {
                        element: 'linha-tempo',
                        intro: 'Na linha do tempo você verá um resumo das quantidades e valores movimentados por você em cada ano neste programa.',
                        position: 'top'
                    },
                    {
                        element: 'linha-tempo #caixa div',
                        intro: 'Ao clicar no resumo você terá uma informação mais detalhada de sua movimentação no ano.'
                    }

                ],
                showStepNumbers: false,
                exitOnOverlayClick: false,
                exitOnEsc: true,
                nextLabel: 'Avançar',
                prevLabel: 'Voltar',
                skipLabel: 'Encerrar',
                doneLabel: 'Encerrar',
                disableInteraction: true
            };

            var hasPrograma = true;
            $scope.introOnStart = function(){
                if(!$scope.notasAno || $scope.notasAno.length == 0){
                    hasPrograma = false;
                    $scope.notasAno = [
                        {
                            ano: "2016",
                            template : "<p>Tipo: <b>CAPRINO</b></p><p>Valor: <b>R$ 246,46</b></p><p>Litros: <b>2846</b><p>"
                        },
                        {
                            ano: "2015",
                            template : "<p>Tipo: <b>BOVINO e CAPRINO</b></p><p>Valor: <b>R$ 135,25</b></p><p>Litros: <b>1378</b><p>"
                        }
                    ];
                }
                $rootScope.modoTutorial = true;
                $scope.introStart();
            };
            $scope.introOnExit = function () {
                if (!hasPrograma){
                    hasPrograma = true;
                    $scope.notasAno = [];
                }
                $rootScope.modoTutorial = false;
            };
            $rootScope.introOnStart = $scope.introOnStart;
        });
