'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('pnae', {
                parent: 'programas',
                url: '/pnae',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_AGRI', 'ROLE_AGRI_REST', 'ROLE_GESTOR', 'ROLE_TECNICO', 'ROLE_ADMIN', 'ROLE_COORDENADOR_ATER'],
                    pageTitle: 'PNAE'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/agricultor/programas/pnae/pnae.html',
                        controller: 'PnaeController'
                    }
                }
            });
    });
