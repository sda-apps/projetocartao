'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('hora-plantar', {
                parent: 'programas',
                url: '/hora-plantar',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_AGRI', 'ROLE_AGRI_REST', 'ROLE_GESTOR', 'ROLE_TECNICO', 'ROLE_ADMIN', 'ROLE_COORDENADOR_ATER'],
                    pageTitle: 'Projeto Hora de Plantar'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/agricultor/programas/hora-plantar/hora-plantar.html',
                        controller: 'horaPlantarController'
                    }
                }
            });
    });
