'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('segunda-agua', {
                parent: 'programas',
                url: '/segunda-agua',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_AGRI', 'ROLE_AGRI_REST', 'ROLE_GESTOR', 'ROLE_TECNICO', 'ROLE_ADMIN', 'ROLE_COORDENADOR_ATER'],
                    pageTitle: 'Segunda Água'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/agricultor/programas/segunda-agua/segunda-agua.html',
                        controller: 'SegundaAguaController'
                    },
                    'mapacisternas@segunda-agua': {
                        templateUrl: 'scripts/components/cards/card-agricultor-mapa-cisternas/card-agricultor-mapa-cisternas.html',
                        controller: 'CardAgricultorMapaCisternasController'
                    }
                }
            });
    });
