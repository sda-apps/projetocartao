'use strict';

angular.module('projetocartaoApp')
	.controller('CisternasController',
		function ($scope, $rootScope, $state, $mdDialog, $mdMedia, $sce, $filter, Cisterna, Principal, identificadorRoles, projetosConstants) {

            Principal.identity().then(function(account) {
                $rootScope.account = account;
                $rootScope.isAuthenticated = Principal.isAuthenticated;
                if ($rootScope.isAuthenticated()){
                    $rootScope.isAgricultor = identificadorRoles.isAgricultor($rootScope.account.authorities);
                }
            });

            $scope.dispMovel = $mdMedia('xs');

            $scope.cisternaPlaca = null;
            $scope.cisternaPolietileno = null;

            $scope.showDialogSobre = function(ev) {
                $mdDialog.show({
                    controller: function($rootScope, $scope, $mdDialog){
                        $scope.cancel = function() {
                            $mdDialog.cancel();
                        };
                    },
                    templateUrl: 'scripts/components/dialogs/programas/cisternas/cisternas.template.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    disableParentScroll: true,
                    fullscreen: true
                })
                .then(function(answer) {
                    //return
                }, function() {
                    //cancel
                });
            };

            $scope.buscarCisternasDoAgricultor = function() {
            	var agricultor = $rootScope.account.agricultor;
                Cisterna.get({agricultorId: agricultor.id}, function (cisternas) {

                	for (var i = 0; i < cisternas.length; i++) {
                		var cisterna = cisternas[i];

                		switch (cisterna.projeto.codigoSda) {
	                		case projetosConstants.CISTERNAS_DE_PLACA.codigoSda:
								$scope.cisternaPlaca = cisterna;
								break;
								
	                		case projetosConstants.CISTERNAS_DE_POLIETILENO.codigoSda:
	                			$scope.cisternaPolietileno = cisterna;
								break;
	
							default:
								break;
						}
                		
					}

                });
            };
            $scope.buscarCisternasDoAgricultor();

            $scope.introOptions = {
                steps:[
                    {
                        element: '#sinopse',
                        intro: 'Aqui você tem um resumo sobre o programa e pode visualizar mais detalhes clicando em Ver Mais.'
                    },
                    {
                        element: $scope.dispMovel? '#step2b': '#step2a',
                        intro: 'Clicando no manual você pode baixar o arquivo pro seu computador, visualizar online ou imprimir.'
                    },
                    {
                        element: '#informacoes',
                        intro: 'Nessas abas você tem as informações sobre as suas cisternas.',
                        position: 'top'
                    },
                    {
                        element: '#step4',
                        intro: 'No mapa você visualiza a localização da sua cisterna.',
                        position: 'top'
                    }

                ],
                showStepNumbers: false,
                exitOnOverlayClick: false,
                exitOnEsc: true,
                nextLabel: 'Avançar',
                prevLabel: 'Voltar',
                skipLabel: 'Encerrar',
                doneLabel: 'Encerrar',
                disableInteraction: true
            };

            var hasCisterna = true;
            $scope.introOnStart = function(){
                if(!$scope.cisternaPlaca && !$scope.cisternaPolietileno){
                    hasCisterna = false;
                    $scope.cisternaPlaca = {
                        id: 10015692966,
                        tipoCisterna: "Cisternas de Placa",
                        numero: 99999999,
                        empresa: {
                            nome: "Nome da Empresa"
                        },
                        anoImplantacao: 2014,
                        elemento: {
                            coordenada: {
                                latitude: "-6.791971",
                                longitude: "-39.303808"
                            }
                        }
                    };
                }
                $rootScope.modoTutorial = true;
                $scope.introStart();
            };
            $scope.introOnExit = function () {
                if (!hasCisterna){
                    hasCisterna = true;
                    $scope.cisternaPlaca = null;
                }
                $rootScope.modoTutorial = false;
            };
            $rootScope.introOnStart = $scope.introOnStart;
        });
