'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('cisternas', {
                parent: 'programas',
                url: '/cisternas',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_AGRI', 'ROLE_AGRI_REST', 'ROLE_GESTOR', 'ROLE_TECNICO', 'ROLE_ADMIN', 'ROLE_COORDENADOR_ATER'],
                    pageTitle: 'Cisternas'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/agricultor/programas/cisternas/cisternas.html',
                        controller: 'CisternasController'
                    },
                    'mapacisternas@cisternas': {
                        templateUrl: 'scripts/components/cards/card-agricultor-mapa-cisternas/card-agricultor-mapa-cisternas.html',
                        controller: 'CardAgricultorMapaCisternasController'
                    }
                }
            });
    });
