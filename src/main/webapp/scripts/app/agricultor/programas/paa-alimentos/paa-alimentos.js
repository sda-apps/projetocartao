'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('paa-alimentos', {
                parent: 'programas',
                url: '/paa-alimentos',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_AGRI', 'ROLE_AGRI_REST', 'ROLE_GESTOR', 'ROLE_TECNICO', 'ROLE_ADMIN', 'ROLE_COORDENADOR_ATER'],
                    pageTitle: 'PAA Alimentos'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/agricultor/programas/paa-alimentos/paa-alimentos.html',
                        controller: 'paaAlimentosController'
                    }
                }
            });
    });
