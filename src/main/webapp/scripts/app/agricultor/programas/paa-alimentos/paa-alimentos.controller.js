'use strict';

angular.module('projetocartaoApp')
	.controller('paaAlimentosController',
        function ($scope, $rootScope, $state, $mdDialog, $mdMedia, $sce, NotasProjetos, $filter, projetosConstants, ServicosExtrato, identificadorRoles, Principal) {

            Principal.identity().then(function(account) {
                $rootScope.account = account;
                $rootScope.isAuthenticated = Principal.isAuthenticated;
                if ($rootScope.isAuthenticated()){
                    $rootScope.isAgricultor = identificadorRoles.isAgricultor($rootScope.account.authorities);
                }
            });

            $scope.dispMovel = $mdMedia('xs');
            $scope.showDialogProdutos = function (ano){
                var produtos = {ano: ano, templates:$scope.produtosAno[ano]};
                $mdDialog.show({
                    controller: 'produtosController',
                    templateUrl: 'scripts/components/dialogs/programas/produtos/produtos.html',
                    parent: angular.element(document.body),
                    locals: {
                        produtos: produtos
                    },
                    clickOutsideToClose: true,
                    disableParentScroll: true,
                    fullscreen: false
                })
                .then(function(answer) {
                    //return
                }, function() {
                    //cancel
                });
            };

            $scope.showDialogSobre = function(ev) {
                $mdDialog.show({
                    controller: 'paaAlimentosController',
                    templateUrl: 'scripts/components/dialogs/programas/paa-alimentos/paa-alimentos.template.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    disableParentScroll: true,
                    fullscreen: true
                })
                .then(function(answer) {
                    //return
                }, function() {
                    //cancel
                });
            };

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            var getNotasProjeto = function() {
                try {
                    NotasProjetos.get({agricultorId: $rootScope.account.agricultor.id, projetoCodigoSDA: projetosConstants.PAA_ALIMENTOS.id}, function (result){
                        $scope.notasAno = getTemplatesNotasAno(result);
                        $scope.produtosAno = getTemplatesProdutos(result)
                    });
                } catch(e) {
                    console.log("ERRO notasProjeto", e);
                }
            };

            var getTemplatesNotasAno = function(notas){
                var notasAno = [];
                for (var i=0;i<notas.length;i++){
                    if(notasAno[notas[i].ano]){
                        notasAno[notas[i].ano].valorTotal += notas[i].valorTotal;
                        notasAno[notas[i].ano].produtos += notas[i].itensNota.length;
                    }else {
                        notasAno[notas[i].ano] = {
                            valorTotal: notas[i].valorTotal,
                            produtos: notas[i].itensNota.length
                        };
                    }
                }
                var templatesNotasAno = [];
                for (var i in notasAno){
                    templatesNotasAno.unshift({
                        ano: i,
                        template:   '<p>Valor: <b>'+ $filter('currency')(notasAno[i].valorTotal, 'R$ ', 2) +'</b></p>'+
                                    '<p>Itens: <b>'+ notasAno[i].produtos +'</b><p>'
                    });
                }
                return templatesNotasAno;
            };

            var getTemplatesProdutos = function(notas){
                var templatesProdutos = [];
                for (var i=0;i<notas.length;i++){
                    if(!templatesProdutos[notas[i].ano]){
                        templatesProdutos[notas[i].ano] = [];
                    }
                    for(var j in notas[i].itensNota){
                        console.log(notas[i].itensNota);
                        var produtoAux = {
                            nome: notas[i].itensNota[j].produtoEspecificacao.produto.nome,
                            especificacao: notas[i].itensNota[j].produtoEspecificacao.descricao,
                            quantidade: notas[i].itensNota[j].quantidade,
                            unidade: notas[i].itensNota[j].produtoEspecificacao.unidade.sigla,
                            recebimento: $filter('date')(notas[i].dataEmissao, "dd/MM/yyyy"),
                            valor: notas[i].itensNota[j].valor
                        };
                        templatesProdutos[notas[i].ano].push({
                            template:   '<p>'+produtoAux.nome+'</p>'+
                                        '<span>Especificação: <b>'+produtoAux.especificacao+'</b></span>'+
                                        '<span>Quantidade: <b>'+$filter('number')(produtoAux.quantidade, 1)+ ' ' +produtoAux.unidade+'</b></span>'+
                                        '<span>Valor Unitário: <b>'+$filter('currency')(produtoAux.valor, 'R$ ', 2)+'</b></span>'+
                                        '<span>Valor Total: <b>'+$filter('currency')((produtoAux.valor * produtoAux.quantidade), 'R$ ', 2)+'</b></span>'+
                                        '<span>Recebimento: <b>'+(produtoAux.recebimento ? produtoAux.recebimento : "")+'</b></span>'
                        });
                    }
                }
                return templatesProdutos;
            };

            $scope.onChangeSwitch = function(){
                $scope.notasAno.reverse();
            };

            $scope.emitirExtratoPAAAlimentos = function () {
                ServicosExtrato.getExtratoPrograma('api/servicos/extrato/paa-alimentos/', $rootScope.account.agricultor.id);
            };
            getNotasProjeto();

            $scope.introOptions = {
                steps:[
                    {
                        element: '#sinopse',
                        intro: 'Aqui você tem um resumo sobre o programa e pode visualizar mais detalhes clicando em Ver Mais.'
                    },
                    {
                        element: $scope.dispMovel? '#step2b': '#step2a',
                        intro: 'Clicando no manual você pode baixar o arquivo pro seu computador, visualizar online ou imprimir.'
                    },
                    {
                        element: '#layout-programas-emitir-extrato',
                        intro: 'Aqui você pode emitir um extrato que detalha sua participação neste programa.'
                    },
                    {
                        element: '#step4',
                        intro: 'É possível ordenar a linha do tempo do ano mais recente pro mais antigo ou do mais antigo para o mais recente.'
                    },
                    {
                        element: 'linha-tempo',
                        intro: 'Na linha do tempo você verá um resumo das quantidades e valores movimentados por você em cada ano neste programa.',
                        position: 'top'
                    },
                    {
                        element: 'linha-tempo #caixa div',
                        intro: 'Ao clicar no resumo você terá uma informação mais detalhada de sua movimentação no ano.'
                    }

                ],
                showStepNumbers: false,
                exitOnOverlayClick: false,
                exitOnEsc: true,
                nextLabel: 'Avançar',
                prevLabel: 'Voltar',
                skipLabel: 'Encerrar',
                doneLabel: 'Encerrar',
                disableInteraction: true
            };

            var hasPrograma = true;
            $scope.introOnStart = function(){
                if(!$scope.notasAno || $scope.notasAno.length == 0){
                    hasPrograma = false;
                    $scope.notasAno = [
                        {
                            ano: "2016",
                            template : "<p>Valor: <b>R$ 5537,00</b></p><p>Itens: <b>15</b><p>"
                        },
                        {
                            ano: "2015",
                            template : "<p>Valor: <b>R$ 2127,00</b></p><p>Itens: <b>9</b><p>"
                        }
                    ];
                }
                $rootScope.modoTutorial = true;
                $scope.introStart();
            };
            $scope.introOnExit = function () {
                if (!hasPrograma){
                    hasPrograma = true;
                    $scope.notasAno = [];
                }
                $rootScope.modoTutorial = false;
            };
            $rootScope.introOnStart = $scope.introOnStart;
        });
