'use strict';

angular.module('projetocartaoApp')
    .controller('biodieselController',
        function ($scope, $rootScope, $state, $mdDialog, $mdMedia, $sce, NotasProjetos, $filter, projetosConstants, ServicosExtrato, identificadorRoles, Principal) {
            Principal.identity().then(function(account) {
                $rootScope.account = account;
                $rootScope.isAuthenticated = Principal.isAuthenticated;
                if ($rootScope.isAuthenticated()){
                    $rootScope.isAgricultor = identificadorRoles.isAgricultor($rootScope.account.authorities);
                }
            });

            $scope.dispMovel = $mdMedia('xs');

            $scope.showDialogSobre = function(ev) {
                $mdDialog.show({
                    controller: 'horaPlantarController',
                    templateUrl: 'scripts/components/dialogs/programas/biodiesel/biodiesel.template.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    disableParentScroll: true,
                    fullscreen: true
                })
                    .then(function(answer) {
                        //return
                    }, function() {
                        //cancel
                    });
            };

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            var getNotasProjeto = function() {
                try {
                    NotasProjetos.get({agricultorId: $rootScope.account.agricultor.id, projetoCodigoSDA: projetosConstants.BIODIESEL.id}, function (result){
                        $scope.notasAno = getTemplatesNotasAno(result);
                    });
                } catch(e) {
                    console.log("ERRO notasProjeto", e);
                }
            };

            var getTemplatesNotasAno = function(notas){
                var notasAno = [];
                for (var i=0;i<notas.length;i++){
                    if(notasAno[notas[i].ano]){
                        notasAno[notas[i].ano].valorTotal += notas[i].valorTotal;
                        notasAno[notas[i].ano].quantidade += notas[i].itensNota.quantidade;
                        notasAno[notas[i].ano].unidade =  notas[i].itensNota.produtoEspecificacao.unidade.sigla;

                    }else {
                        notasAno[notas[i].ano] = {
                            valorTotal: notas[i].valorTotal,
                            quantidade: notas[i].itensNota[0].quantidade,
                            unidade: notas[i].itensNota[0].produtoEspecificacao.unidade.sigla
                        };
                    }
                }

                var templatesNotasAno = [];
                for (var i in notasAno){
                    templatesNotasAno.unshift({
                        ano: i,
                        template:
                        '<p>Quantidade Plantada: <b>'+ $filter('number')(notasAno[i].quantidade, 2) +' '+ notasAno[i].unidade +'</b><p>'+
                        '<p>Valor Recebido: <b>' + $filter('currency')(notasAno[i].valorTotal, 'R$ ', 2) +'</b></p>'
                    });
                }
                return templatesNotasAno;
            };



            $scope.onChangeSwitch = function(){
                $scope.notasAno.reverse();
            };

            $scope.emitirExtratoBiodiesel = function () {
                ServicosExtrato.getExtratoPrograma('api/servicos/extrato/biodiesel/', $rootScope.account.agricultor.id);
            };
            getNotasProjeto();

        });
