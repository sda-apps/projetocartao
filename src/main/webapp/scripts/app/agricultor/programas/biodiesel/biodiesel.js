'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('biodiesel', {
                parent: 'programas',
                url: '/biodiesel',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_AGRI', 'ROLE_AGRI_REST', 'ROLE_GESTOR', 'ROLE_TECNICO', 'ROLE_ADMIN', 'ROLE_COORDENADOR_ATER'],
                    pageTitle: 'Programa Biodiesel'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/agricultor/programas/biodiesel/biodiesel.html',
                        controller: 'biodieselController'
                    }
                }
            });
    });
