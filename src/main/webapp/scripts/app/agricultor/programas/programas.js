'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('programas', {
                abstract: true,
                parent: 'agricultor'
            });
    });
