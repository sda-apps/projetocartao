'use strict';

angular.module('projetocartaoApp')
	.controller('cajuController',
        function ($scope, $rootScope, $state, $mdDialog, $mdMedia, $sce, NotasProjetos, $filter, projetosConstants, ServicosExtrato, identificadorRoles, Principal, AgricultorCaju) {

            Principal.identity().then(function(account) {
                $rootScope.account = account;
                $rootScope.isAuthenticated = Principal.isAuthenticated;
                if ($rootScope.isAuthenticated()){
                    $rootScope.isAgricultor = identificadorRoles.isAgricultor($rootScope.account.authorities);
                }
            });

            $scope.dispMovel = $mdMedia('xs');
            $scope.showDialogProdutos = function (ano){
                var produtos = {ano: ano, templates:$scope.produtosAno[ano]};
                $mdDialog.show({
                    controller: 'produtosController',
                    templateUrl: 'scripts/components/dialogs/programas/produtos/produtos.html',
                    parent: angular.element(document.body),
                    locals: {
                        produtos: produtos
                    },
                    clickOutsideToClose: true,
                    disableParentScroll: true,
                    fullscreen: false
                })
                .then(function(answer) {
                    //return
                }, function() {
                    //cancel
                });
            };

            $scope.showDialogSobre = function(ev) {
                $mdDialog.show({
                    controller: 'cajuController',
                    templateUrl: 'scripts/components/dialogs/programas/caju/caju.template.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    disableParentScroll: true,
                    fullscreen: true
                })
                .then(function(answer) {
                    //return
                }, function() {
                    //cancel
                });
            };

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            var getCaju = function() {
                try {
                    AgricultorCaju.get({agricultorId: $rootScope.account.agricultor.id}, function (result){
                        var templates = getTemplatesCajuAno(result);
                        $scope.notasAno = templates[0];
                        $scope.produtosAno = templates[1];
                    });
                } catch(e) {
                    console.log("ERRO agricultorCaju", e);
                }
            };

            var getTemplatesCajuAno = function(notas){

                var cajuAno = [];
                for (var i=0;i<notas.length;i++){
                    if(cajuAno[notas[i].ano]){
                        cajuAno[notas[i].ano].valorTotal += (notas[i].valorCorteAte70 + notas[i].valorCorteAcima70 + notas[i].valorPegadaAte70 + notas[i].valorPegadaAcima70);
                        cajuAno[notas[i].ano].quantidadeCorte += (notas[i].quantidadeCorteAte70+ notas[i].quantidadeCorteAcima70);
                        cajuAno[notas[i].ano].quantidadePegada += (notas[i].quantidadePegadaAte70+notas[i].quantidadePegadaAcima70);

                        cajuAno[notas[i].ano].valorCorteAte70 += notas[i].valorCorteAte70;
                        cajuAno[notas[i].ano].quantidadeCorteAte70 +=notas[i].quantidadeCorteAte70;
                        cajuAno[notas[i].ano].valorCorteAcima70 += notas[i].valorCorteAcima70;
                        cajuAno[notas[i].ano].quantidadeCorteAcima70 +=notas[i].quantidadeCorteAcima70;
                        cajuAno[notas[i].ano].quantidadePegadaAte70 += notas[i].quantidadePegadaAte70;
                        cajuAno[notas[i].ano].valorPegadaAte70 += notas[i].valorPegadaAte70;
                        cajuAno[notas[i].ano].quantidadePegadaAcima70 += notas[i].quantidadePegadaAcima70;
                        cajuAno[notas[i].ano].valorPegadaAcima70 += notas[i].valorPegadaAcima70;

                    }else {
                        cajuAno[notas[i].ano] = {
                            valorTotal: (notas[i].valorCorteAte70 + notas[i].valorCorteAcima70 + notas[i].valorPegadaAte70 + notas[i].valorPegadaAcima70),
                            quantidadeCorte: (notas[i].quantidadeCorteAte70+ notas[i].quantidadeCorteAcima70),
                            quantidadePegada: (notas[i].quantidadePegadaAte70+notas[i].quantidadePegadaAcima70),
                            valorCorteAte70: notas[i].valorCorteAte70,
                            quantidadeCorteAte70: notas[i].quantidadeCorteAte70,
                            valorCorteAcima70: notas[i].valorCorteAcima70,
                            quantidadeCorteAcima70: notas[i].quantidadeCorteAcima70,
                            quantidadePegadaAte70: notas[i].quantidadePegadaAte70,
                            valorPegadaAte70: notas[i].valorPegadaAte70,
                            quantidadePegadaAcima70: notas[i].quantidadePegadaAcima70,
                            valorPegadaAcima70: notas[i].valorPegadaAcima70
                        };
                    }
                }

                var templatesCajuAno = [];
                var templatesProdutos=[];
                for (var i in cajuAno){
                    templatesCajuAno.unshift({
                        ano: i,
                        template:   '<p>Quantidade Cortada: <b>'+ cajuAno[i].quantidadeCorte +'</b><p>' +
                                    '<p>Quantidade Pegada: <b>'+ cajuAno[i].quantidadePegada +'</b><p>' +
                                    '<p>Valor Total Recebido: <b>'+ $filter('currency')(cajuAno[i].valorTotal, 'R$ ', 2) +'</b></p>'
                                    
                    });

                    templatesProdutos[i]=[];
                    if(cajuAno[i].quantidadeCorteAte70>0){
                        templatesProdutos[i].push({
                            template:   '<p>CORTE ATÉ 70 CM</p>'+
                                        '<span>Quantidade: <b>'+cajuAno[i].quantidadeCorteAte70+' pés'+'</b></span>'+
                                        '<span>Valor Recebido: <b>'+$filter('currency')((cajuAno[i].valorCorteAte70), 'R$ ', 2)+'</b></span>'
                        })
                    }
                    if(cajuAno[i].quantidadeCorteAcima70>0){
                        templatesProdutos[i].push({
                            template:   '<p>CORTE ACIMA DE 70 CM</p>'+
                                        '<span>Quantidade: <b>'+cajuAno[i].quantidadeCorteAcima70+' pés'+'</b></span>'+
                                        '<span>Valor Recebido: <b>'+$filter('currency')((cajuAno[i].valorCorteAcima70), 'R$ ', 2)+'</b></span>'
                        })
                    }
                    if(cajuAno[i].quantidadePegadaAte70>0){
                        templatesProdutos[i].push({
                            template:   '<p>PEGA ATÉ 70 CM</p>'+
                                        '<span>Quantidade: <b>'+cajuAno[i].quantidadePegadaAte70+' pés'+'</b></span>'+
                                        '<span>Valor Recebido: <b>'+$filter('currency')((cajuAno[i].valorPegadaAte70), 'R$ ', 2)+'</b></span>'
                        })
                    }
                    if(cajuAno[i].quantidadePegadaAcima70>0){
                        templatesProdutos[i].push({
                            template:'<p>PEGA ACIMA DE 70 CM</p>'+
                                        '<span>Quantidade: <b>'+cajuAno[i].quantidadePegadaAcima70+' pés'+'</b></span>'+
                                        '<span>Valor Recebido: <b>'+$filter('currency')((cajuAno[i].valorPegadaAcima70), 'R$ ', 2)+'</b></span>'
                        })
                    }
                }

                var templates=[templatesCajuAno, templatesProdutos];

                return templates;
            };

            $scope.onChangeSwitch = function(){
                $scope.notasAno.reverse();
            };
            
            getCaju();

            $scope.introOptions = {
                steps:[
                    {
                        element: '#sinopse',
                        intro: 'Aqui você tem um resumo sobre o programa e pode visualizar mais detalhes clicando em Ver Mais.'
                    },
                    {
                        element: $scope.dispMovel? '#step2b': '#step2a',
                        intro: 'Clicando no manual você pode baixar o arquivo pro seu computador, visualizar online ou imprimir.'
                    },
                    {
                        element: '#step4',
                        intro: 'É possível ordenar a linha do tempo do ano mais recente pro mais antigo ou do mais antigo para o mais recente.'
                    },
                    {
                        element: 'linha-tempo',
                        intro: 'Na linha do tempo você verá um resumo das quantidades e valores movimentados por você em cada ano neste programa.',
                        position: 'top'
                    },
                    {
                        element: 'linha-tempo #caixa div',
                        intro: 'Ao clicar no resumo você terá uma informação mais detalhada de sua movimentação no ano.'
                    }

                ],
                showStepNumbers: false,
                exitOnOverlayClick: false,
                exitOnEsc: true,
                nextLabel: 'Avançar',
                prevLabel: 'Voltar',
                skipLabel: 'Encerrar',
                doneLabel: 'Encerrar',
                disableInteraction: true
            };

            var hasPrograma = true;
            $scope.introOnStart = function(){
                if(!$scope.notasAno || $scope.notasAno.length == 0){
                    hasPrograma = false;
                    $scope.notasAno = [
                        {
                            ano: "2016",
                            template : "<p>Valor: <b>R$ 5537,00</b></p><p>Itens: <b>15</b><p>"
                        },
                        {
                            ano: "2015",
                            template : "<p>Valor: <b>R$ 2127,00</b></p><p>Itens: <b>9</b><p>"
                        }
                    ];
                }
                $rootScope.modoTutorial = true;
                $scope.introStart();
            };
            $scope.introOnExit = function () {
                if (!hasPrograma){
                    hasPrograma = true;
                    $scope.notasAno = [];
                }
                $rootScope.modoTutorial = false;
            };
            $rootScope.introOnStart = $scope.introOnStart;
        });
