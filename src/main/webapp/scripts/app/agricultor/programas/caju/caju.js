'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('caju', {
                parent: 'programas',
                url: '/caju',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_AGRI', 'ROLE_AGRI_REST', 'ROLE_GESTOR', 'ROLE_TECNICO', 'ROLE_ADMIN', 'ROLE_COORDENADOR_ATER'],
                    pageTitle: 'Caju'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/agricultor/programas/caju/caju.html',
                        controller: 'cajuController'
                    }
                }
            });
    });
