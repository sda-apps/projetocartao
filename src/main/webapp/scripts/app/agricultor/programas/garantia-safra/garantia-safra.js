'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('garantia-safra', {
                parent: 'programas',
                url: '/garantia-safra',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_AGRI', 'ROLE_AGRI_REST', 'ROLE_GESTOR', 'ROLE_TECNICO', 'ROLE_ADMIN', 'ROLE_COORDENADOR_ATER'],
                    pageTitle: 'Programa Garantia Safra'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/agricultor/programas/garantia-safra/garantia-safra.html',
                        controller: 'GarantiaSafraController'
                    }
                }
            });
    });
