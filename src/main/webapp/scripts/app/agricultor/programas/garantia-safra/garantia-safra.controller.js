'use strict';

angular.module('projetocartaoApp')
    .controller('GarantiaSafraController',
        function ($scope, $rootScope, $state, $mdDialog, $mdMedia, $sce, $filter, AgricultorProjetosPorProjeto, projetosConstants, identificadorRoles, Principal) {
            Principal.identity().then(function(account) {
                $rootScope.account = account;
                $rootScope.isAuthenticated = Principal.isAuthenticated;
                if ($rootScope.isAuthenticated()){
                    $rootScope.isAgricultor = identificadorRoles.isAgricultor($rootScope.account.authorities);
                }
            });

            $scope.dispMovel = $mdMedia('xs');

            $scope.showDialogSobre = function(ev) {
                $mdDialog.show({
                    controller: function($rootScope, $scope, $mdDialog){
                        $scope.cancel = function() {
                            $mdDialog.cancel();
                        };
                    },
                    templateUrl: 'scripts/components/dialogs/programas/garantia-safra/garantia-safra.template.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    disableParentScroll: true,
                    fullscreen: true
                })
                    .then(function(answer) {
                        //return
                    }, function() {
                        //cancel
                    });
            };

            var getValorProjeto = function() {
                try {
                    AgricultorProjetosPorProjeto.get({agricultorId: $rootScope.account.agricultor.id, projetoCodigoSDA: projetosConstants.GARANTIA_SAFRA.id}, function (result){
                        $scope.projetoAno = getTemplatesProjetoAno(result);
                    });
                } catch(e) {
                    console.log("ERRO agricultorProjetosPorProjeto", e);
                }
            };

            var getTemplatesProjetoAno = function(projetos){
                var templateProjetosAno = [];

                for (var i=0;i<projetos.length;i++){
                    templateProjetosAno.unshift({
                        ano: projetos[i].ano,
                        template: '<p><b>Beneficiado</b></p>'+
                        '<p>Valor do benefício: <b>' + $filter('currency')(projetos[i].valorTotal, 'R$ ', 2) +'</b></p>'
                    });
                }
                return templateProjetosAno;
            };

            $scope.onChangeSwitch = function(){
                $scope.projetoAno.reverse();
            };
            getValorProjeto();

            $scope.introOptions = {
                steps:[
                    {
                        element: '#sinopse',
                        intro: 'Aqui você tem um resumo sobre o programa e pode visualizar mais detalhes clicando em Ver Mais.'
                    },
                    {
                        element: $scope.dispMovel? '#step2b': '#step2a',
                        intro: 'Clicando no manual você pode baixar o arquivo pro seu computador, visualizar online ou imprimir.'
                    },
                    {
                        element: '#step3',
                        intro: 'É possível ordenar a linha do tempo do ano mais recente pro mais antigo ou do mais antigo para o mais recente.'
                    },
                    {
                        element: 'linha-tempo',
                        intro: 'Na linha do tempo você verá um resumo das quantidades e valores movimentados por você em cada ano neste programa.',
                        position: 'top'
                    },
                    {
                        element: 'linha-tempo #caixa div',
                        intro: 'Ao clicar no resumo você terá uma informação mais detalhada de sua movimentação no ano.'
                    }

                ],
                showStepNumbers: false,
                exitOnOverlayClick: false,
                exitOnEsc: true,
                nextLabel: 'Avançar',
                prevLabel: 'Voltar',
                skipLabel: 'Encerrar',
                doneLabel: 'Encerrar',
                disableInteraction: true
            };

            var hasPrograma = true;
            $scope.introOnStart = function(){
                if(!$scope.projetoAno || $scope.projetoAno.length == 0){
                    hasPrograma = false;
                    $scope.projetoAno = [
                        {
                            ano: "2016",
                            template : "<p><b>Beneficiado</b></p><p>Valor do benefício: <b>R$ 1.547,65</b></p>"
                        },
                        {
                            ano: "2015",
                            template : "<p><b>Beneficiado</b></p><p>Valor do benefício: <b>R$ 1.027,45</b></p>"
                        }
                    ];
                }
                $rootScope.modoTutorial = true;
                $scope.introStart();
            };
            $scope.introOnExit = function () {
                if (!hasPrograma){
                    hasPrograma = true;
                    $scope.projetoAno = [];
                }
                $rootScope.modoTutorial = false;
            };
            $rootScope.introOnStart = $scope.introOnStart;
        });
