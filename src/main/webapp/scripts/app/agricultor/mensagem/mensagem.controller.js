'use strict';

angular.module('projetocartaoApp')
    .controller('MensagemsController',
    	function ($scope, $rootScope, $state, Mensagem) {

            $scope.dias = ["26 DE AGO", "27 DE AGO", "HOJE"];

            $scope.mensagensPorDia = [
                [{
                    msg: "Lorem ipsum dolor sit amet, vel sapien lobortis augue, urna porttitor mollis leo dapibus quis, commodo est purus mollis.",
                    enviado: true,
                    hora: "10:00"
                },
                {
                    msg: "Porttitor mollis leo dapibus quis, commodo est purus mollis.",
                    enviado: false,
                    hora: "10:00"
                },
                {
                    msg: "Lorem ipsum dolor.",
                    enviado: true,
                    hora: "10:00"
                },
                {
                    msg: "Porttitor mollis.",
                    enviado: false,
                    hora: "10:00"
                },
                {
                    msg: "Lorem ipsum dolor.",
                    enviado: true,
                    hora: "10:00"
                },
                {
                    msg: "Porttitor mollis.",
                    enviado: false,
                    hora: "10:00"
                }],
                [{
                    msg: "Commodo est purus mollis.",
                    enviado: true,
                    hora: "10:00"
                },
                {
                    msg: "M",
                    enviado: false,
                    hora: "10:00"
                }],

                [{
                    msg: "Lorem ipsum dolor.",
                    enviado: true,
                    hora: "10:00"
                },
                {
                    msg: "Porttitor mollis.",
                    enviado: false,
                    hora: "10:00"
                }]
            ];

            $scope.contatos = [
                {
                    nome:"Antonio da Silva",
                    status:"On-line",
                    mensagens:4,
                    horaUltimaMsg:"12:00"
                },
                {
                    nome:"José Oliveira",
                    status:"Off-line",
                    mensagens:0,
                    horaUltimaMsg:"9:00"
                },
                {
                    nome:"Sued de Deus",
                    status:"On-line",
                    mensagens:15,
                    horaUltimaMsg:"08:23"
                }
            ];

            $scope.contatoSelecionado = -1;
            $scope.glued = true;
            
            $scope.mensagensDoContatoSelecionado = [];

            $scope.selecionaContato = function (index) {
                $scope.contatos[index].mensagens=0;
                $scope.contatoSelecionado = index;
                
                Mensagem.get({de: 'davi', para: 'laio'}).$promise.then(function (result) {
                	$scope.mensagensDoContatoSelecionado = result;
                }).catch(function () {
                	console.log('ERROR');
                });
            };

            $scope.enviaMensagem = function () {
                if ($scope.mensagem && $scope.mensagem != "") {
                    var currentdate = new Date();
                    
                    var mensagemDto = {
                    		mensagem: $scope.mensagem,
                    		de: 'laio',
                    		para: 'davi',
                    		enviado: true,
                    		dataEnvio: currentdate,
                    		horaEnvio: currentdate.getHours() + ":" + currentdate.getMinutes()
                    };
                    
                    Mensagem.save(mensagemDto).$promise.then(function () {
                    	console.log('OK');
                    	$scope.mensagensDoContatoSelecionado.push(mensagemDto);
                    }).catch(function (result) {
                    	console.log(result);
                    });
                    
                    $scope.mensagem = "";
                    $scope.glued = true;
                }
            }
    });
