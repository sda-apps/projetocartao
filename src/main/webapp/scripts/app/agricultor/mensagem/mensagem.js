'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('mensagem', {
                parent: 'agricultor',
                url: '/mensagem',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_AGRI', 'ROLE_AGRI_REST', 'ROLE_GESTOR', 'ROLE_TECNICO', 'ROLE_ADMIN', 'ROLE_COORDENADOR_ATER'],
                    pageTitle: 'Mensagens'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/agricultor/mensagem/mensagem.html',
                        controller: 'MensagemsController'
                    }
                },
                resolve: {

                }
            });
    });
