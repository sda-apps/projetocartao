'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('agricultor', {
                abstract: true,
                parent: 'site'
            });
    });
