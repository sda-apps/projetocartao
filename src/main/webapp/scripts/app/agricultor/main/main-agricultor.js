'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('main-agricultor', {
                parent: 'agricultor',
                url: '/agricultor',
                data: {
                    authorities: rolesAccess['ALL_ROLES'].buscar,
                    pageTitle: 'Início'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/agricultor/main/main-agricultor.html',
                        controller: 'MainAgricultorController'
                    },
                    'cardagricultorfoto@main-agricultor': {
                        templateUrl: 'scripts/components/cards/card-agricultor-foto/card-agricultor-foto.html',
                        controller: 'CardAgricultorFotoController'
                    },
                    'cardagricultordados@main-agricultor': {
                        templateUrl: 'scripts/components/cards/card-agricultor-dados/card-agricultor-dados.html',
                        controller: 'CardAgricultorDadosController'
                    },
                    'cardagricultormapa@main-agricultor': {
                        templateUrl: 'scripts/components/cards/card-agricultor-mapa/card-agricultor-mapa.html',
                        controller: 'CardAgricultorMapaController'
                    },
                    'cardagricultorprojeto@main-agricultor': {
                        templateUrl: 'scripts/components/cards/card-agricultor-projeto/card-agricultor-projeto.html',
                        controller: 'CardAgricultorProjetoController'
                    },
                    'cardagricultorater@main-agricultor': {
                        templateUrl: 'scripts/components/cards/card-agricultor-ater/card-agricultor-ater.html',
                        controller: 'CardAgricultorAterController'
                    },
                    'cardagricultortemperatura@main-agricultor': {
                        templateUrl: 'scripts/components/cards/card-agricultor-temperatura/card-agricultor-temperatura.html',
                        controller: 'CardAgricultorTemperaturaController'
                    }

                },
                resolve: {

                }
            });
    });
