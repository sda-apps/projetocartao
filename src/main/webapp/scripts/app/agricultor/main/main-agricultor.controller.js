'use strict';

angular.module('projetocartaoApp')
    .controller('MainAgricultorController',
        function ($scope, $window, $rootScope, $state, Principal, Auth, $filter, $mdDialog, $mdMedia, identificadorRoles, Toast, Agricultor) {

        Principal.identity().then(function(account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
            if ($rootScope.isAuthenticated()){
                $rootScope.isAgricultor = identificadorRoles.isAgricultor($rootScope.account.authorities);
            }
            if(!$rootScope.isAgricultor && $window.agricultor){
                $rootScope.account.agricultor = $window.agricultor;
                $rootScope.isNovaAba = true;
            }
        });

        var isPrimeiraVezContentLoaded = true;
        $scope.$on('$viewContentLoaded', function(){
            if(isPrimeiraVezContentLoaded) {
                isPrimeiraVezContentLoaded = false;

                /*if ($rootScope.isAgricultor && !$rootScope.account.agricultor.etnia){ //TODO etnia
                    $scope.showDialogApelido(true);
                }*/
            }
        });

        $scope.showDialogApelido = function(tutorial){
            $mdDialog.show({
                templateUrl:'scripts/components/dialogs/apelido/apelido.html',
                backdrop : false,
                escapeToClose: false,
                controller: "ApelidoAgricultorController"
            }).then(function(answer) {
                var agricultor = $rootScope.account.agricultor;
                agricultor.pessoaFisica.pessoa.apelido = answer.apelido;
                //agricultor.etnia = answer.etnia; //TODO remover etnia verificar grupo etnico ou raca
                Agricultor.update(agricultor).$promise.then(function(){
                    $rootScope.account.agricultor.pessoaFisica.pessoa.apelido = answer.apelido;
                    //$rootScope.account.agricultor.etnia.id = answer.etnia.id;
                    //$rootScope.account.agricultor.etnia.nome = answer.etnia.nome;
                    //$rootScope.account.agricultor.etnia.codigo = answer.etnia.codigo;
                    /*if($rootScope.account.agricultor.pessoaFisica.sexo ==='F'){
                        $scope.mudaEtniaDescricao(answer.etnia);
                    }else{
                        $rootScope.account.agricultor.etnia.descricao = answer.etnia.descricao;
                    }*/

                    if (tutorial) $scope.introOnStart();
                })
                .catch(function(erro){
                    var msg = erro.data.status + ' - ' + erro.data.error;
                    Toast.erro(msg);
                });
            }, function() {
                //cancel
            });
        };

        /*$scope.mudaEtniaDescricao = function(etnia){
            switch(etnia.codigo) {
                case 1:
                    $rootScope.account.agricultor.etnia.descricao = "PESCADORA"
                    break;
                case 3:
                    $rootScope.account.agricultor.etnia.descricao = "AQUICULTORA"
                    break;
                case 5:
                    $rootScope.account.agricultor.etnia.descricao = "AGRICULTORA FAMILIAR"
                    break;
                default:
                    $rootScope.account.agricultor.etnia.descricao = etnia.descricao;
            }
        };*/

        $rootScope.tutorial = false;
        $scope.introOptions = {
            steps:[
                {
                    element: '#step1',
                    intro: "Olá, agricultor(a) ! Aqui você pode inserir sua foto de perfil, visualizar sua identificação (agricultor(a), pescador(a), aquicultor(a), indígena ou quilombola) e alterá-la."
                },
                {
                    element: '#step2',
                    intro: "Aqui, você acompanha as condições climáticas do seu município, temperatura e previsão de chuva ou sol."
                },
                {
                    element: '#step3',
                    intro: "No card Dados Pessoais você tem um resumo sobre suas informações junto à Secretaria do Desenvolvimento Agrário (SDA)."
                },
                {
                    element: '#step4',
                    intro: "No card do mapa você visualizará as coordenadas ou polígono da sua propriedade ou domicílio.",
                    position: 'top'
                },
                {
                    element: '#step5',
                    intro: "Neste card, você terá informações de quais programas da SDA participa atualmente.",
                    position: 'top'
                },
                {
                    element: '#step6',
                    intro: "Neste card você terá informações da Assistência Técnica e Extensão Rural (ATER), quais técnicos(as) lhe acompanham e quais os últimos atendimentos recebidos.",
                    position: 'top'
                }
            ],
            showStepNumbers: false,
            exitOnOverlayClick: false,
            exitOnEsc: true,
            nextLabel: 'Avançar',
            prevLabel: 'Voltar',
            skipLabel: 'Encerrar',
            doneLabel: 'Encerrar'
        };

        $scope.introOnStart = function(){
            $rootScope.modoTutorial = true;
            $scope.introStart();
        };
        $scope.introOnExit = function () {
            $rootScope.modoTutorial = false;
        };
        $rootScope.introOnStart = $scope.introOnStart;
    });
