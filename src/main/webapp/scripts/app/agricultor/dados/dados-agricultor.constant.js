'use strict';

angular.module('projetocartaoApp')
    .constant(
        'parentesConstants', {
            'FILHO':{tipo: 'F', descricao: 'Filho/a'},
            'PAI':{tipo: 'P', descricao: 'Pai'},
            'MAE':{tipo: 'M', descricao: 'Mãe'},
            'AVO':{tipo: 'O', descricao: 'Avô'},
            'AVOH':{tipo: 'H', descricao: 'Avó'},
            'CONJUGE':{tipo: 'C', descricao: 'Cônjuge'},
            'ENTEADO':{tipo: 'E', descricao: 'Enteado'},
            'AGREGADO':{tipo: 'A', descricao: 'Agregado'},
            'TUTOR':{tipo: 'T', descricao: 'Tutor'}})
    .constant(
        'telefoneConstants', {
            'CELULAR':{tipo: '1', descricao: 'Celular'},
            'RESIDENCIAL':{tipo: '2', descricao: 'Residencial'},
            'RECADOS':{tipo: '3', descricao: 'Recados'},
            'COMUNITARIO':{tipo: '4', descricao: 'Comunitário'},
            'COMERCIAL':{tipo: '5', descricao: 'Comercial'},
            'WHATSAPP':{tipo: '6', descricao: 'Celular/WhatsApp'}});



