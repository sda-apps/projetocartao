'use strict';

angular.module('projetocartaoApp')
    .controller('AgricultorDadosController',
    	function ($scope, $rootScope, $state, $mdMedia, Principal, Auth, AgricultorFamilia, $filter, AgricultorDaps, DadosBancarios,parentesConstants,telefoneConstants) {
        Principal.identity().then(function(account) {
            $rootScope.account = account;
            $scope.dataNascimento= $filter('date')($rootScope.account.agricultor.pessoaFisica.dataNascimento, 'dd/MM/yyyy');
            $rootScope.isAuthenticated = Principal.isAuthenticated;

            for(var index in $rootScope.account.agricultor.pessoaFisica.pessoa.telefones){
                if($rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].tipo == telefoneConstants.CELULAR.tipo){
                    $rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].tipo = telefoneConstants.CELULAR.descricao;
                    $rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].numero = $filter('celular')($rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].numero);
                }
                if($rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].tipo == telefoneConstants.COMERCIAL.tipo){
                    $rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].tipo = telefoneConstants.COMERCIAL.descricao;
                    $rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].numero = $filter('telefone')($rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].numero);
                }
                if($rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].tipo == telefoneConstants.COMUNITARIO.tipo){
                    $rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].tipo = telefoneConstants.COMUNITARIO.descricao;
                    $rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].numero = $filter('telefone')($rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].numero);
                }
                if($rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].tipo == telefoneConstants.RECADOS.tipo){
                    $rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].tipo = telefoneConstants.RECADOS.descricao;
                    $rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].numero = $filter('telefone')($rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].numero);
                }
                if($rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].tipo == telefoneConstants.RESIDENCIAL.tipo){
                    $rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].tipo = telefoneConstants.RESIDENCIAL.descricao;
                    $rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].numero = $filter('telefone')($rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].numero);
                }
                if($rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].tipo == telefoneConstants.WHATSAPP.tipo){
                    $rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].tipo = telefoneConstants.WHATSAPP.descricao;
                    $rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].numero = $filter('celular')($rootScope.account.agricultor.pessoaFisica.pessoa.telefones[index].numero);
                }
            }
        });
        $scope.dadosBancarios = [];
        $scope.dispMovel = $mdMedia('xs');

        function buscarFamiliaAgricultor() {
            AgricultorFamilia.get({agricultorId: $rootScope.account.agricultor.id}, function (result) {
                $rootScope.account.agricultor.familia=result;
                for(var index in $rootScope.account.agricultor.familia.parentes){
                    $rootScope.account.agricultor.familia.parentes[index].agricultor.pessoaFisica.cpf = $filter('cpf')($rootScope.account.agricultor.familia.parentes[index].agricultor.pessoaFisica.cpf);

                    if($rootScope.account.agricultor.familia.parentes[index].parentesco == parentesConstants.FILHO.tipo ){
                        $rootScope.account.agricultor.familia.parentes[index].parentesco = parentesConstants.FILHO.descricao;
                    }else if($rootScope.account.agricultor.familia.parentes[index].parentesco == parentesConstants.PAI.tipo ){
                        $rootScope.account.agricultor.familia.parentes[index].parentesco = parentesConstants.PAI.descricao;
                    }else if($rootScope.account.agricultor.familia.parentes[index].parentesco == parentesConstants.MAE.tipo ){
                        $rootScope.account.agricultor.familia.parentes[index].parentesco = parentesConstants.MAE.descricao;
                    }else if($rootScope.account.agricultor.familia.parentes[index].parentesco == parentesConstants.AVO.tipo ){
                        $rootScope.account.agricultor.familia.parentes[index].parentesco = parentesConstants.AVO.descricao;
                    }else if($rootScope.account.agricultor.familia.parentes[index].parentesco == parentesConstants.AVOH.tipo ){
                        $rootScope.account.agricultor.familia.parentes[index].parentesco = parentesConstants.AVOH.descricao;
                    }else if($rootScope.account.agricultor.familia.parentes[index].parentesco == parentesConstants.CONJUGE.tipo ){
                        $rootScope.account.agricultor.familia.parentes[index].parentesco = parentesConstants.CONJUGE.descricao;
                    }else if($rootScope.account.agricultor.familia.parentes[index].parentesco == parentesConstants.ENTEADO.tipo ){
                        $rootScope.account.agricultor.familia.parentes[index].parentesco = parentesConstants.ENTEADO.descricao;
                    }else if($rootScope.account.agricultor.familia.parentes[index].parentesco == parentesConstants.AGREGADO.tipo ){
                        $rootScope.account.agricultor.familia.parentes[index].parentesco = parentesConstants.AGREGADO.descricao;
                    }else if($rootScope.account.agricultor.familia.parentes[index].parentesco == parentesConstants.TUTOR.tipo ){
                        $rootScope.account.agricultor.familia.parentes[index].parentesco = parentesConstants.TUTOR.descricao;
                    }
                }
            });
        }

        function getDapsAgricultor() {
            AgricultorDaps.get({agricultorId: $rootScope.account.agricultor.id}, function (result) {
                var daps = [];
                angular.forEach(result, function(dap){
                    daps.push(dap);
                });
                $rootScope.account.agricultor.daps=daps;
            });
        }

        function getDadoBancarios() {
            DadosBancarios.get({pessoaId: $rootScope.account.agricultor.pessoaFisica.pessoa.id}, function (result) {
                $scope.dadosBancarios = result;
                for(var dado in $scope.dadosBancarios){
                    $scope.dadosBancarios[dado].ativo = $scope.dadosBancarios[dado].ativo? "Ativa" : "Inativa";
                }
            })
        }
        buscarFamiliaAgricultor();
        getDapsAgricultor();
        getDadoBancarios();

        $scope.introOptions = {
            steps:[
                {
                    element: 'md-tabs-wrapper',
                    intro: 'Aqui você pode verificar seus dados cadastrados junto a SDA: Dados Pessoais, Endereço, Contato, Dados do Conjuge e Dados Financeiros.'
                }
            ],
            showStepNumbers: false,
            exitOnOverlayClick: false,
            exitOnEsc: true,
            showBullets: false,
            nextLabel: 'Avançar',
            prevLabel: 'Voltar',
            skipLabel: 'Encerrar',
            doneLabel: 'Encerrar'
        };

        $scope.introOnStart = function(){
            $rootScope.modoTutorial = true;
            $scope.introStart();
        };
        $scope.introOnExit = function () {
            $rootScope.modoTutorial = false;
        };
        $rootScope.introOnStart = $scope.introOnStart;
    });
