'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('dados-agricultor', {
                parent: 'agricultor',
                url: '/dados-agricultor',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_AGRI', 'ROLE_AGRI_REST', 'ROLE_GESTOR', 'ROLE_TECNICO', 'ROLE_ADMIN', 'ROLE_COORDENADOR_ATER'],
                    pageTitle: 'Dados Pessoais'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/agricultor/dados/dados-agricultor.html',
                        controller: 'AgricultorDadosController'
                    }
                },
                resolve: {

                }
            });
    });
