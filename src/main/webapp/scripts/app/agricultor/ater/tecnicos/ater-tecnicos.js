'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('ater-tecnicos', {
                parent: 'ater',
                url: '/ater-tecnicos',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_AGRI', 'ROLE_AGRI_REST', 'ROLE_GESTOR', 'ROLE_TECNICO', 'ROLE_ADMIN', 'ROLE_COORDENADOR_ATER'],
                    pageTitle: 'Técnicos'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/agricultor/ater/tecnicos/ater-tecnicos.html',
                        controller: 'AterTecnicosController'
                    }
                },
                resolve: {

                }
            });
    });
