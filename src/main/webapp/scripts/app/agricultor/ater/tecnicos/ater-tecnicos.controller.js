'use strict';

angular.module('projetocartaoApp')
    .controller('AterTecnicosController',
        function ($scope, $rootScope, $state, $timeout, Principal, identificadorRoles, TecnicosAssociadosPorAgricultor,UsuarioAvancadoPorId, $mdDialog) {

            $scope.anoAtual = new Date().getFullYear();

            $scope.tecnicos = [];

            Principal.identity().then(function(account) {
                $rootScope.account = account;
                $rootScope.isAuthenticated = Principal.isAuthenticated;
                if ($rootScope.isAuthenticated()){
                    $rootScope.isAgricultor = identificadorRoles.isAgricultor($rootScope.account.authorities);
                }
            });

            var getTecnicosAssociados = function() {
                try {
                    TecnicosAssociadosPorAgricultor.get({agricultorId: $rootScope.account.agricultor.id}, function (result){
                        $scope.tecnicos = result;
                    });
                } catch(e) {
                    console.log("ERRO TecnicosAssociados", e);
                }
            };

            getTecnicosAssociados();

            $scope.getDadosTecnico = function(id,tipoATER) {
                try {
                    UsuarioAvancadoPorId.get({id: id}, function (result) {
                        $scope.showDialogInfoTecnico(result, tipoATER);
                    });
                } catch(e) {
                    console.log("ERRO Tecnicos", e);
                }
            };

            $scope.showDialogInfoTecnico = function (result,tipoATER){
                var tecnico = {tecnico: result, tipoATER: tipoATER};
                $mdDialog.show({
                    controller: 'dialogAterTecnico',
                    templateUrl: 'scripts/components/dialogs/ater/ater.html',
                    parent: angular.element(document.body),
                    locals: {
                        resultado: tecnico
                    },
                    clickOutsideToClose: true,
                    disableParentScroll: true,
                    fullscreen: false
                })
                    .then(function(answer) {
                        //return
                    }, function() {
                        //cancel
                    });
            };

            $scope.introOptions = {
                steps:[
                    {
                        element: '#tecnico-atual-tutorial',
                        intro: 'Aqui você terá um resumo com as informações dos(as) técnicos(as) associados(as) a você e que lhe prestam assistência atualmente, ao clicar você pode enviar uma mensagem para o(a) técnico(a) e ver mais detalhes sobre ele(a).'
                    }, {
                    	element: '#tecnico-passado-tutorial',
                    	intro: 'Já aqui, você tem os(as) técnicos(as) que já lhe assistiram no passado mas não estão mais associados(as) a você.'
                    }
                ],
                showStepNumbers: false,
                exitOnOverlayClick: false,
                exitOnEsc: true,
                showBullets: false,
                nextLabel: 'Avançar',
                prevLabel: 'Voltar',
                skipLabel: 'Encerrar',
                doneLabel: 'Encerrar'
            };

            $scope.introOnStart = function(){
            	$rootScope.modoTutorial = true;
                $timeout(function() {
                    $scope.introStart();
                }, 100);
            };

            $scope.introOnExit = function () {
                $rootScope.modoTutorial = false;
            };

            $rootScope.introOnStart = $scope.introOnStart;

        }
    );
