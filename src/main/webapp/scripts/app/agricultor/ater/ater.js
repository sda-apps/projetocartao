'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('ater', {
                abstract: true,
                parent: 'agricultor'
            });
    });
