'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('ater-atendimentos', {
                parent: 'ater',
                url: '/ater-atendimentos',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_AGRI', 'ROLE_AGRI_REST', 'ROLE_GESTOR', 'ROLE_TECNICO', 'ROLE_ADMIN', 'ROLE_COORDENADOR_ATER'],
                    pageTitle: 'Atendimentos'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/agricultor/ater/atendimentos/ater-atendimentos.html',
                        controller: 'AterAtendimentosController'
                    }
                },
                resolve: {

                }
            });
    });
