'use strict';

angular.module('projetocartaoApp')
    .controller('AterAtendimentosController',
        function ($scope, $rootScope, $state, $filter, $timeout, Principal, identificadorRoles, AtendimentosPorAgricultor, IndicadoresPorAtendimento, AgricultorAter) {
            $scope.exibir = [];
            $scope.anos = [];
            $scope.meses = [{id: 1, nome: "Janeiro"},{id: 2, nome: "Fevereiro"},{id: 3, nome: "Março"},{id: 4, nome: "Abril"},{id: 5, nome: "Maio"},{id: 6, nome: "Junho"},{id: 7, nome: "Julho"},{id: 8, nome: "Agosto"},{id: 9, nome: "Setembro"},{id: 10, nome: "Outubro"},{id: 11, nome: "Novembro"},{id: 12, nome: "Dezembro"}];
            $scope.tecnicos = [];
            $scope.atividades = [];
            $scope.mesSelecionado = [];
            $scope.anoSelecionado = [];
            $scope.tecnicoSelecionado = [];
            $scope.atividadeSelecionada = [];
            $scope.indicadores = [];

            $scope.atendimentosMockados = false;

            Principal.identity().then(function(account) {
                $rootScope.account = account;
                $rootScope.isAuthenticated = Principal.isAuthenticated;
                if ($rootScope.isAuthenticated()){
                    $rootScope.isAgricultor = identificadorRoles.isAgricultor($rootScope.account.authorities);
                }
            });

            $scope.mensagemErro=[];



            var getAtendimentos = function() {

                $scope.filtro = {
                    id: $rootScope.account.agricultor.id,
                    ano: [],
                    mes: [],
                    tecnico: [],
                    atividade: []
                };

                AtendimentosPorAgricultor.query($scope.filtro).$promise.then(function (result) {
                    $scope.atendimentos = result;
                    for(var i=0; i<result.length; i++) {
                        $scope.anos.push($filter('date')(result[i].dataAtendimento, 'yyyy'));
                        $scope.tecnicos.push(result[i].tecnico.pessoaFisica.pessoa.nome);
                        $scope.atividades.push(result[i].atividadeEspecifica.nome);
                    }
                    $scope.anos = $.unique($scope.anos);
                    $scope.tecnicos = $.unique($scope.tecnicos);
                    $scope.atividades = $.unique($scope.atividades);
                })
                .catch(function() {
                    $scope.atendimentos = [];
                });
            };

            $scope.getIndicadores = function(atendimentoId, id) {
                try {
                    if($scope.indicadores[atendimentoId] == null) {
                        IndicadoresPorAtendimento.get({atendimentoId: atendimentoId}).$promise
                            .then(function (result) {
                                $scope.indicadores[atendimentoId] = result;
                                $scope.mensagemErro[id]=false;
                            })
                            .catch(function(){
                                $scope.mensagemErro[id]=true;
                            });
                    }
                    else{
                        $scope.mensagemErro[id]=false;
                    }
                } catch(e) {
                    console.log("ERRO Indicadores", e);                    
                }
            };

            $scope.getAtendimentosComFiltro = function() {
                $scope.filtro = {
                    id: $rootScope.account.agricultor.id,
                    ano: $scope.anoSelecionado,
                    mes: $scope.mesSelecionado,
                    tecnico: $scope.tecnicoSelecionado,
                    atividade: $scope.atividadeSelecionada
                };

                AtendimentosPorAgricultor.query($scope.filtro).$promise.then(function (result) {
                    $scope.atendimentos = result;
                    $scope.msgFiltro = false;
                })
                .catch(function() {
                    $scope.atendimentos = [];
                    $scope.msgFiltro = true;
                });
            };

            getAtendimentos();

            $scope.expansaoAtendimento = function(id) {
                $scope.exibir[id] = !$scope.exibir[id];
            };

            $scope.introOptions = {
                steps:[
                    {
                        element: '#step1',
                        intro: 'Você pode buscar os atendimentos recebido filtrando por ano, mês, técnico(a) e atividade.'
                    }, {
                    	element: 'md-list-item',
                    	intro: 'Nesta lista, você pode visualizar o histórico de atividades (atendimentos) de ATER realizadas para ter um controle e registro do que foi realizado.'
                    }
                ],
                showStepNumbers: false,
                exitOnOverlayClick: false,
                exitOnEsc: true,
                showBullets: false,
                nextLabel: 'Avançar',
                prevLabel: 'Voltar',
                skipLabel: 'Encerrar',
                doneLabel: 'Encerrar'
            };

            $scope.introOnStart = function(){
                $rootScope.modoTutorial = true;

                if ($scope.atendimentos == null || $scope.atendimentos.length == 0) {
                	AgricultorAter.get(function (result) {
                    	$scope.atendimentos = result;
                    	$scope.atendimentosMockados = true;
                    });
                }

	            $timeout(function() {
	                $scope.introStart();
	            }, 100);
            };

            $scope.introOnExit = function () {
                $rootScope.modoTutorial = false;
                if ($scope.atendimentosMockados) {
                	$scope.atendimentos = [];
                }
            };

            $rootScope.introOnStart = $scope.introOnStart;
        }
    );
