'use strict';

angular.module('projetocartaoApp')
    .controller('ServicosController', function ($scope, $state, $rootScope, Principal, ServicosExtrato, identificadorRoles, $mdMedia, AgricultorProjetos, AgricultorProjetosProvider, AgricultorProjetosPorAno, $filter, projetosConstants, $timeout) {

        Principal.identity().then(function(account) {
            $rootScope.account = account;

            $rootScope.isAuthenticated = Principal.isAuthenticated;

            if ($rootScope.isAuthenticated()) {
                $rootScope.isAgricultor = identificadorRoles.isAgricultor($rootScope.account.authorities);
            }
        });

        $scope.dispMovel            = $mdMedia('xs');
        $scope.cssAnoExtrato        = $scope.dispMovel ? 'ano-extrato-mobile' : 'ano-extrato'
        $scope.extratoProgramas     = [];
        $scope.anos                 = [];

        $scope.getAgricultorProjetos = function() {
                if($scope.anoSelecionado) {

                    AgricultorProjetosPorAno.get({agricultorId: $rootScope.account.agricultor.id, agricultorProjetoAno: $scope.anoSelecionado}).$promise.then( function (result) {
                    	$scope.programasAgricultor = montarPedacosGrafico(AgricultorProjetosProvider.listaAgricultorProjetos(result));
                        montarGrafico();
                    }).catch(function(){
                        $scope.programasAgricultor = [];
                    });
                } else {
                    AgricultorProjetos.get({agricultorId: $rootScope.account.agricultor.id}).$promise.then( function (result) {
                        $scope.programasAgricultor = montarPedacosGrafico(AgricultorProjetosProvider.listaAgricultorProjetos(result));
                        for (var i = 0; i < result.length; i++) {
                        	$scope.anos.push(result[i].ano);
						}

                        $scope.anos = $.unique($scope.anos);
                        montarGrafico();
                        verificarExtratosExistentes();
                    }).catch(function(){
                        $scope.programasAgricultor = [];
                    });
                }
        };

        var montarGrafico = function() {
            $scope.graficoParticipacaoProgramas = {
                type        : "PieChart",
                pieSliceText: "value",
                data: {
                    "cols": [{
                        id      : "t",
                        label   : "Topping",
                        type    : "string"
                    }, {
                        id      : "s",
                        label   : "Slices",
                        type    : "number"
                    }],
                    "rows": $scope.programasAgricultor
                },
                formatters: {
                    number: [{
                        columnNum   : 1,
                        prefix      : 'R$ '
                    }]
                },
                options: {
                    colors      : ['#766795','#80bbd4', '#a2799e', '#c8a1b3', '#f3deb0', '#94b967', '#bbdae3'],
                    showLables  : 'true',
                    pieSliceText: 'value',
                    pieSliceTextStyle: {
                        color   : 'white',
                        fontSize: 12
                    },
                    legend: {
                        position: $scope.dispMovel ? 'none' : 'left',
                    },
                    tooltip: {
                        textStyle       : {color: '#757575'},
                        showColorCode   : true
                    },
                    chartArea:{
                        left    : 20,
                        top     : 20,
                        width   : '100%',
                        height  : '85%'
                    }
                }
            };
        };

        var montarPedacosGrafico = function(programas) {
            return programas.map(function(agricultorPrograma) {
                try {
                    var pedaco = {
                        c: [{
                            v: $filter('capitalize')(agricultorPrograma.abreviacao)
                        }, {
                            v: agricultorPrograma.valorTotal
                        }],
                        programa    : $filter('capitalize')(agricultorPrograma.nome),
                        codigoSda   : agricultorPrograma.codigoSda,
                        total       : agricultorPrograma.valorTotal,
                        ano         : agricultorPrograma.ano,
                        abreviacao  : $filter('capitalize')(agricultorPrograma.abreviacao)
                    };

                    return pedaco;
                } catch (e) {
                    console.log(e);
                }
            });
        };

        var verificarExtratosExistentes = function() {
            if($scope.extratoProgramas.length == 0) {
                for(var i in $scope.programasAgricultor) {
                    var pa = $scope.programasAgricultor[i];

                    for (var property in projetosConstants) {
                        if(projetosConstants[property].hasExtrato && projetosConstants[property].id == $scope.programasAgricultor[i].codigoSda) {
                            pa.urlExtrato = projetosConstants[property].urlExtrato;

                            $scope.extratoProgramas.push(pa);
                        }
                    }
                }
            }

        };

        $scope.introOptions = {
            steps:[
                {
                    element: '#google-chart',
                    intro: "Na tela de serviços você pode visualizar a quantidade de recursos movimentados por você nos projetos da SDA em um ano específico. Cada fatia do gráfico de pizza representa um projeto e o tamanho da fatia da pizza o volume relativo de recursos movimentados no período."
                },
                {
                    element: '#input-ano',
                    intro: "Aqui você pode selecionar um ano específico para filtrar o volume de recursos movimentados por projetos no ano selecionado."
                },
                {
                    element: '#card-extrato',
                    intro: "Ainda na tela de serviços você pode baixar arquivos PDF com o extrato de sua movimentação em cada projeto da SDA que você participa atualmente ou participou no passado."
                }


            ],
            showStepNumbers: false,
            exitOnOverlayClick: false,
            exitOnEsc: true,
            nextLabel: "Próximo",
            prevLabel: "Anterior",
            skipLabel: "Encerrar",
            doneLabel: "Terminar",
            showBullets: false,
            disableInteraction: true

        };

        $scope.introOnStart = function () {
            if($scope.programasAgricultor.length == 0){
                $scope.adicionadoValorficticio = true;
                $scope.programasAgricultor[0] = {"c":[{"v":"Projeto Hora de Plantar"},{"v":64}],"programa":"Projeto Hora de Plantar","codigoSda":3,"total":64,"ano":2011,"abreviacao":"Projeto Hora de Plantar"};
                montarGrafico();
                $scope.extratoProgramas[0] = {"c":[{"v":"Projeto Hora de Plantar"},{"v":64}],"programa":"Projeto Hora de Plantar","codigoSda":3,"total":64,"ano":2011,"abreviacao":"Projeto Hora de Plantar","urlExtrato":"api/servicos/extrato/horadeplantar/"};
            }

            $timeout(function () {
                $scope.introStart();
            }, 100);
        };

        $scope.introOnExit = function () {
            if($scope.adicionadoValorficticio)
            {
                $scope.programasAgricultor = [];
                $scope.extratoProgramas = [];
            }
        };

        $rootScope.introOnStart = $scope.introOnStart;


        $scope.emitirExtrato = function (url) {
            ServicosExtrato.getExtratoPrograma(url, $rootScope.account.agricultor.id);
        };

        $scope.getAgricultorProjetos();


    });
