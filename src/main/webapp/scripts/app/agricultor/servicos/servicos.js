'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('servicos', {
                parent: 'agricultor',
                url: '/servicos',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_AGRI', 'ROLE_AGRI_REST', 'ROLE_GESTOR', 'ROLE_TECNICO', 'ROLE_ADMIN', 'ROLE_COORDENADOR_ATER'],
                    pageTitle: 'Serviços'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/agricultor/servicos/servicos.html',
                        controller: 'ServicosController'
                    }
                },
                resolve: {

                }
            });
    });
