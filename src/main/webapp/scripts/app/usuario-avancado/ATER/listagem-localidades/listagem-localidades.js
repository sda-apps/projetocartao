'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('listagem-localidades', {
                parent: 'aterUA',
                url: '/listagem-localidades',
                data: {
                    authorities: rolesAccess['LOCALIDADE'].visualizar,
                    pageTitle: 'Administração - Localidades'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/listagem-localidades/listagem-localidades.html',
                        controller: 'ListagemLocalidadesController'
                    }
                },
                resolve: {

                }
            });
    });
