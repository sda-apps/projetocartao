'use strict';

angular.module('projetocartaoApp')
    .controller('ListagemLocalidadesController', function ($rootScope, $scope, LocalidadesFiltro, $timeout, $http, $state, $mdDialog, Localidade, Toast, Principal, rolesAccess) {

        $scope.mostraInput = 'localidade';
        $scope.filtros = [];
        $scope.localidade = [];
        $scope.semResultado = false;
        $scope.dto = {};
        $scope.paginacao = 10;
        $scope.start = 0;
        $scope.limit = 10;
        $scope.exibeVerMais = false;

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        $scope.podeIncluir = function () {
            return Principal.hasAnyAuthority(rolesAccess['LOCALIDADE'].incluir);
        };

        $scope.podeVisualizar = function () {
            return Principal.hasAnyAuthority(rolesAccess['LOCALIDADE'].visualizar);
        };

        $scope.podeEditar = function () {
            return Principal.hasAnyAuthority(rolesAccess['LOCALIDADE'].editar);
        };

        $scope.podeExcluir = function () {
            return Principal.hasAnyAuthority(rolesAccess['LOCALIDADE'].excluir);
        };

        if ($scope.podeIncluir()) {
            $rootScope.fabAdd = function () {
                $scope.carregarLocalidade(null);
            };
        }

        function setFormsPristine() {
            $scope.regiaoForm.$setPristine();
            $scope.municipioForm.$setPristine();
            $scope.localidadeForm.$setPristine();
            $scope.distritoForm.$setPristine();
            $scope.territorioForm.$setPristine();
        }

        $scope.$watch('mostraInput', function (input) {
            switch (input) {
                case 'regiao':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-regiao')).focus();
                    });
                    break;
                case 'municipio':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-municipio')).focus();
                    });
                    break;
                case 'localidade':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-localidade')).focus();
                    });
                    break;
                case 'distrito':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-distrito')).focus();
                    });
                    break;
                case 'territorio':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-territorio')).focus();
                    });
                    break;
                default:
                    break;
            }
        }, true);

        function buscarLocalidadePorFiltros() {
            LocalidadesFiltro.query($scope.dto).$promise.then(function (result) {
                $scope.semResultado = false;
                $scope.localidade = $scope.localidade.concat(result);
                $scope.start = $scope.localidade.length;
                $scope.exibeVerMais = result.length >= $scope.paginacao;
            })
                .catch(function () {
                    $scope.semResultado = true;
                    $scope.exibeVerMais = false;
                    $scope.localidade = new Array();
                });
        }

        $scope.carregaMais = function () {
            $scope.start = $scope.limit;
            $scope.limit = $scope.limit + $scope.paginacao;

            $scope.dto = construirELimparFiltros($scope.filtros);

            $scope.dto.start = $scope.start;
            $scope.dto.limit = $scope.limit;

            buscarLocalidadePorFiltros();
        };

        $scope.limparFiltros = function () {
            $scope.filtros = [];
            $scope.localidade = [];
            $scope.semResultado = false;
            $scope.exibeVerMais = false;
            $scope.start = 0;
            $scope.limit = 10;
            $scope.dto = {};

            buscarLocalidadePorFiltros();
        };

        function construirELimparFiltros() {
            var filtro = {};
            $scope.filtros.forEach(function (elemento) {
                if (filtro.hasOwnProperty(elemento.tipo)) {
                    filtro[elemento.tipo].push(elemento.id);
                } else {
                    filtro[elemento.tipo] = [elemento.id];
                }
            });
            return filtro;
        }

        function validarAdicaoFiltro(filtro) {
            return filtro.id && filtro.tipo;
        }

        $scope.removerFiltroChip = function () {
            $scope.localidade = new Array();

            $scope.dto = construirELimparFiltros();

            if ($scope.filtros.length >= 1) {
                buscarLocalidadePorFiltros();
            } else {
                $scope.limparFiltros();
            }
        };

        $scope.adicioarFiltroChip = function (filtro) {
            if (validarAdicaoFiltro(filtro)) {
                $scope.filtros.push(filtro);
                $scope.localidade = new Array();
                $scope.dto = construirELimparFiltros();

                setFormsPristine();

                buscarLocalidadePorFiltros();
                switch (filtro.tipo) {
                    case "regiao":
                        $scope.regiaoSelecionado = undefined;
                        break;
                    case "municipio":
                        $scope.municipioSelecionado = undefined;
                        break;
                    case "empresa":
                        $scope.localidadeSelecionada = undefined;
                        break;
                    case "distrito":
                        $scope.distritoSelecionado = undefined;
                        break;
                    case "territorio":
                        $scope.territorioSelecionado = undefined;
                        break;
                }
            }
        };

        $scope.buscarAutoCompletePorFiltro = function (searchText, url) {
            var data = $http({
                method: 'GET',
                url: url + searchText
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                return response.data;
            });

            return data;
        };

        buscarLocalidadePorFiltros();

        $scope.deletarLocalidade = function (index, item) {
            var confirm = $mdDialog.confirm()
                .title('Você gostaria de excluir a localidade ' + item.nome + '?')
                .ok('Excluir')
                .cancel('Cancelar')
                .theme('themeButtonUA');

            $mdDialog.show(confirm).then(function () {
                Localidade.delete({id: item.id}).$promise.then(function (result) {
                    Toast.sucesso("Localidade excluída com sucesso.");
                    $scope.localidade.splice(index, 1);
                }).catch(function (error) {
                    console.log(error);
                    Toast.erro("Erro ao excluir localidade.");
                });
            }, function () {

            });
        };

        $scope.carregarLocalidade = function (localidade) {
            if ($scope.podeVisualizar()) {
                $mdDialog.show({
                    controller: 'CadastrarLocalidadeController',
                    templateUrl: 'scripts/components/dialogs/localidades/cadastrar-localidade.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    disableParentScroll: true,
                    locals: {
                        localidadeToEdit: angular.copy(localidade)
                    },
                    fullscreen: true
                }).then(function (answer) {

                }, function () {
                    $scope.localidade = [];
                    buscarLocalidadePorFiltros();
                });
            }
        };

    });
