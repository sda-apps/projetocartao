'use strict';

angular.module('projetocartaoApp')
    .controller('ListagemAcoesController', function (
        $rootScope, $scope, AcoesFiltro, $timeout, $http, rolesAccess,
        $state, $mdDialog, Toast, AcaoPTA, Principal)
    {

        $scope.filtros = [];
        $scope.acao = [];
        $scope.semResultado = false;
        $scope.dto = {};
        $scope.paginacao = 10;
        $scope.start = 0;
        $scope.limit = 10;
        $scope.exibeVerMais = false;

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        $scope.podeIncluir = function () {
            return Principal.hasAnyAuthority(rolesAccess['ACAO'].incluir);
        };

        $scope.podeVisualizar = function () {
            return Principal.hasAnyAuthority(rolesAccess['ACAO'].visualizar);
        };

        $scope.podeEditar = function () {
            return Principal.hasAnyAuthority(rolesAccess['ACAO'].editar);
        };

        $scope.podeExcluir = function () {
            return Principal.hasAnyAuthority(rolesAccess['ACAO'].excluir);
        };

        if($scope.podeIncluir()) {
            $rootScope.fabAdd = function () {
                $scope.carregarAcao(null);
            };
        }

        function setFormsPristine() {
            $scope.acaoForm.$setPristine();
        }

        function buscarAcaoPorFiltros() {
            AcoesFiltro.query($scope.dto).$promise.then(function (result) {
                $scope.semResultado = false;
                $scope.acao = $scope.acao.concat(result);
                $scope.start = $scope.acao.length;
                $scope.exibeVerMais = result.length >= $scope.paginacao;
            })
                .catch(function () {
                    $scope.semResultado = true;
                    $scope.exibeVerMais = false;

                    if ($scope.filtros.length > 0) {
                        $scope.acao = [];
                    }
                });
        }

        $scope.carregaMais = function () {
            $scope.start = $scope.limit;
            $scope.limit = $scope.limit + $scope.paginacao;

            $scope.dto = construirELimparFiltros($scope.filtros);

            $scope.dto.start = $scope.start;
            $scope.dto.limit = $scope.limit;

            buscarAcaoPorFiltros();
        };

        $scope.limparFiltros = function () {
            $scope.filtros = [];
            $scope.acao = [];
            $scope.semResultado = false;
            $scope.exibeVerMais = false;
            $scope.start = 0;
            $scope.limit = 10;
            $scope.dto = {};

            buscarAcaoPorFiltros();
        };

        function construirELimparFiltros() {
            var filtro = {};
            $scope.filtros.forEach(function (elemento) {
                if (filtro.hasOwnProperty(elemento.tipo)) {
                    filtro[elemento.tipo].push(elemento.id);
                } else {
                    filtro[elemento.tipo] = [elemento.id];
                }
            });
            return filtro;
        }

        function validarAdicaoFiltro(filtro) {
            return filtro.id && filtro.tipo;
        }

        $scope.removerFiltroChip = function () {
            $scope.acao = [];

            $scope.dto = construirELimparFiltros();

            if ($scope.filtros.length >= 1) {
                buscarAcaoPorFiltros();
            } else {
                $scope.limparFiltros();
            }
        };

        $scope.adicioarFiltroChip = function (filtro) {
            if (validarAdicaoFiltro(filtro)) {
                $scope.filtros.push(filtro);
                $scope.acao = [];
                $scope.dto = construirELimparFiltros();

                setFormsPristine();

                buscarAcaoPorFiltros();
                switch (filtro.tipo) {
                    case "acao":
                        $scope.acaoSelecionada = undefined;
                        break;
                }
            }
        };


        buscarAcaoPorFiltros();

        $scope.deletarAcao = function (index, item) {
            if($scope.podeExcluir()) {
                var confirm = $mdDialog.confirm()
                    .title('Você gostaria de excluir a ação ' + item.nome + '?')
                    .ok('Excluir')
                    .cancel('Cancelar')
                    .theme('themeButtonUA');

                $mdDialog.show(confirm).then(function () {
                    AcaoPTA.delete({id: item.id}).$promise.then(function (result) {
                        Toast.sucesso("Ação excluída com sucesso.");
                        $scope.acao.splice(index, 1);
                    }).catch(function (error) {
                        console.log(error);
                        Toast.erro("Erro ao excluir ação.");
                    });
                }, function () {

                });
            }
        };

        $scope.carregarAcao = function (acao) {
            if($scope.podeVisualizar()) {
                $mdDialog.show({
                    controller: 'CadastrarAcaoController',
                    templateUrl: 'scripts/components/dialogs/acoes/cadastrar-acao.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    disableParentScroll: true,
                    locals: {
                        acaoToEdit: angular.copy(acao)
                    },
                    fullscreen: true
                }).then(function (answer) {}, function () {
                    $scope.acao = [];
                    buscarAcaoPorFiltros();
                });
            }

        };

    });
