'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('listagem-acoes', {
                parent: 'aterUA',
                url: '/listagem-acoes',
                data: {
                    authorities: rolesAccess['ACAO'].visualizar,
                    pageTitle: 'Administração - Ação'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/listagem-acoes/listagem-acoes.html',
                        controller: 'ListagemAcoesController'
                    }
                },
                resolve: {

                }
            });
    });
