'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('avaliacao-plano-trimestral', {
                parent: 'aterUA',
                url: '/avaliacao-plano-trimestral',
                data: {
                    authorities: rolesAccess['MENU_AVALIACAO'].abrir,
                    pageTitle: 'Avaliação - Planos Trimestrais'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/avaliacoes-pt/avaliacao-plano-trimestral/avaliacao-plano-trimestral.html',
                        controller: 'AvaliacaoPlanoTrimestralController'
                    }
                },
                params: {
                    plano: null
                }
            });
    });
