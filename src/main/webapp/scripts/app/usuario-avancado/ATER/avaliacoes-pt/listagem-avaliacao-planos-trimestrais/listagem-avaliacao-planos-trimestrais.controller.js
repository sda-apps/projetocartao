'use strict';

angular.module('projetocartaoApp')
    .controller('AvaliacaoPlanosTrimestraisController', function ($rootScope, $scope, $http, BuscaPlanosTrimestrais, planoTrimestralConstants, Toast, $state, Principal) {
        $scope.planoTrimestralConstants = planoTrimestralConstants;
        $scope.mostraInput = "status";
        $scope.statusSelect = [
            {id: 1, nome: 'Em Elaboração ATC'},
            {id: 2, nome: 'Finalizado ATC'},
            {id: 3, nome: 'Em Avaliação ERP'},
            {id: 4, nome: 'Aprovado ERP'},
            {id: 5, nome: 'Em Avaliação UGP'},
            {id: 6, nome: 'Aprovado UGP'}
        ];
        $scope.planos = [];
        $scope.filters = [];
        $scope.semResultado = false;
        $scope.exibeVerMais = false;
        var dto = {};
        var start = 0;
        var limit = 10;
        const paginacao = 10;

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        $scope.searchAutoCompleteByFilter = function (searchText, url) {
            return $http({
                method: 'GET',
                url: url + searchText
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                return response.data;
            });
        };

        $scope.addFilterChip = function (filter) {
            if (validateFilter(filter)) {
                $scope.filters.push(filter);
                $scope.planos = [];
                dto = buildAndClearFilters();
                searchPlanosTrimestrais();
            }
        };

        $scope.removeFilterChip = function () {
            $scope.planos = [];

            dto = buildAndClearFilters();

            if ($scope.filters.length >= 1) {
                searchPlanosTrimestrais();
            } else {
                $scope.clearFilter();
            }
        };

        $scope.clearFilter = function () {
            $scope.filters = [];
            $scope.planos = [];
            $scope.semResultado = false;
            $scope.exibeVerMais = false;
            start = 0;
            limit = 10;
            dto = {};

            searchPlanosTrimestrais();
        };

        function validateFilter(filter) {
            var filterTypeAlreadyAdded = $scope.filters.some(function (element) {
                return element.tipo === filter.tipo;
            });

            if (filterTypeAlreadyAdded) {
                Toast.alerta('Filtro deste tipo já adicionado! Não pode haver multiplus filtros do mesmo tipo');

                return false;
            }

            return true;
        }

        function buildAndClearFilters() {
            var filter = {};
            $scope.filters.forEach(function (element) {
                filter[element.tipo] = element.id;
            });

            return filter;
        }

        $scope.loadMore = function () {
            start = limit;
            limit = limit + paginacao;

            dto = buildAndClearFilters($scope.filters);

            dto.start = start;
            dto.limit = limit;

            searchPlanosTrimestrais();
        };

        function searchPlanosTrimestrais() {
            BuscaPlanosTrimestrais.get(dto).$promise.then(function (result) {
                $scope.semResultado = false;
                $scope.planos = $scope.planos.concat(result);
                start = $scope.planos.length;
                $scope.exibeVerMais = result.length >= paginacao;
            }).catch(function () {
                $scope.semResultado = true;
                $scope.exibeVerMais = false;

                if ($scope.filters.length > 0) {
                    $scope.planos = [];
                }
            });
        }

        $scope.isRecused = function (plano) {
            return plano.recusado === true;
        };

        $scope.isRevised = function (plano) {
            return plano.status !== '1' && plano.recusado === true;
        };

        $scope.loadPlanoTrimestral = function (plano) {
            $state.go('avaliacao-plano-trimestral', {plano: plano});
        };

        searchPlanosTrimestrais();
    });
