'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('avaliacao-planos-trimestrais', {
                parent: 'aterUA',
                url: '/avaliacao-planos-trimestrais',
                data: {
                    authorities: rolesAccess['MENU_AVALIACAO'].abrir,
                    pageTitle: 'Avaliação - Planos Trimestrais'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/avaliacoes-pt/listagem-avaliacao-planos-trimestrais/listagem-avaliacao-planos-trimestrais.html',
                        controller: 'AvaliacaoPlanosTrimestraisController'
                    }
                }
            });
    });
