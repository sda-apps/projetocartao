'use strict';

angular.module('projetocartaoApp')
    .controller('AvaliacaoPlanoTrimestralController', function ($scope, $rootScope, $state, PlanoTrimestral, HistoricoAvaliacaoRelatorio,
                                                                $stateParams, planoTrimestralConstants, identificadorRoles, $mdDialog,
                                                                $filter, abreviacaoAutoridadesConstants, abrangenciaEnum, Principal) {

        $scope.historico = [];
        $scope.planoTrimestralConstants = planoTrimestralConstants;
        $scope.plano = $stateParams.plano;
        $scope.showDetail = [];
        $rootScope.subToolbar = {
            title: "Avaliação de Planos Trimestrais",
            backButton: function () {
                $state.go('avaliacao-planos-trimestrais');
            },
            cancelButton: function () {
                $state.go('avaliacao-planos-trimestrais');
            }
        };

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        $scope.expandDetail = function (id) {
            $scope.showDetail[id] = !$scope.showDetail[id];
        };

        function getMetas() {
            PlanoTrimestral.get({id: $scope.plano.id}).$promise.then(function (metasAtividadesTrimestre) {
                $scope.planosGrouped = groupedBy(metasAtividadesTrimestre);
                if ($scope.podeAvaliar()) {
                    $rootScope.subToolbar["evaluateButton"] = $scope.showAvaliacaoDialog;
                }
            }).catch(function (error) {
                console.log(error);
            });
        }

        function getHistorico() {
            HistoricoAvaliacaoRelatorio.query({id: $scope.plano.id}).$promise.then(function (response) {
                $scope.historico = historicRefactored(response);
                $scope.historico = $filter('orderBy')($scope.historico, 'criacao', true);
                $scope.approvedsOrRejects = historicHasElementApprovedOrRejected();
            }).catch(function (error) {
                console.log(error);
            });
        }

        function historicRefactored(historico) {
            return historico.map(function (item) {
                item.usuarioName = item.usuario.usuarioAvancado.pessoaFisica.pessoa.nome;
                if (item.motivo === 'A') {
                    item.motivoDesc = "Aprovado";
                } else if (item.motivo === 'R') {
                    item.motivoDesc = "Recusado";
                }

                return item;
            })
        }

        function historicHasElementApprovedOrRejected() {
            return $scope.historico.filter(function (item) {
                return item.motivo === 'A' || item.motivo === 'R';
            })
        }

        //Montando a lista de planos e metas trimestrais de maneira mais simples para exibir no front
        function groupedBy(planos) {
            return planos.reduce(function (atividades, item) {
                if (!atividades.find(function (el) {
                        return el.id === item.metaAtividadesPTA.atividade.id
                    })) {
                    var metas = planos.filter(function (el) {
                        return el.metaAtividadesPTA.atividade.id === item.metaAtividadesPTA.atividade.id
                    });
                    atividades.push({
                        id: item.metaAtividadesPTA.atividade.id,
                        nome: item.metaAtividadesPTA.atividade.nome,
                        metaAnual: item.metaAtividadesPTA.quantidadeMeta,
                        metas: metas,
                        metaTrimestral: metas.reduce(function (sum, curr) {
                            sum += curr.quantidadeMeta;
                            return sum;
                        }, 0)
                    });
                }
                return atividades;
            }, []);
        }

        $scope.isRecused = function (plano) {
            return plano && plano.recusado === true;
        };

        $scope.isRevised = function (plano) {
            return plano && plano.status !== '1' && plano.recusado === true;
        };

        $scope.podeAvaliar = function () {
            if ($scope.plano) {
                switch ($scope.plano.status) {
                    case "1":
                        return false;
                    case "2":
                        return identificadorRoles.isUsuarioERP($rootScope.account.authorities) ||
                            identificadorRoles.isUsuarioUGP($rootScope.account.authorities) ||
                            identificadorRoles.isAdmin($rootScope.account.authorities);
                    case "3":
                        return identificadorRoles.isUsuarioERP($rootScope.account.authorities) ||
                            identificadorRoles.isUsuarioUGP($rootScope.account.authorities) ||
                            identificadorRoles.isAdmin($rootScope.account.authorities);
                    case "4":
                        return identificadorRoles.isUsuarioUGP($rootScope.account.authorities) || identificadorRoles.isAdmin($rootScope.account.authorities);
                    case "5":
                        return identificadorRoles.isUsuarioUGP($rootScope.account.authorities) || identificadorRoles.isAdmin($rootScope.account.authorities);
                    case "6":
                        return false;
                }
            }
        };

        $scope.showAvaliacaoDialog = function () {
            $mdDialog.show({
                controller: 'AvaliarPlanoController',
                templateUrl: 'scripts/components/dialogs/usuario-avancado/avaliar-plano/avaliar-plano.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                disableParentScroll: true,
                locals: {
                    plano: angular.copy($scope.plano),
                    historico: angular.copy($scope.historico),
                    tipoAvaliacao: 'T',
                    titleWindow: "Avaliar Plano Trimestral"
                },
                fullscreen: true
            }).then(function (answer) {
                $scope.plano.status = answer.status;
                $scope.plano.recusado = answer.recusado;
                $scope.historico.unshift(answer.historicInserted);
                $scope.historico = historicRefactored($scope.historico);
                $rootScope.subToolbar["evaluateButton"] = undefined;
            }, function () {
                //cancel
            });
        };

        if ($scope.plano && $scope.plano.id) {
            getHistorico();
            getMetas();
        } else {
            $state.go('avaliacao-planos-trimestrais');
        }

        $scope.getAbreviacaoAutoridade = function (item) {
            if(abreviacaoAutoridadesConstants[item]) {
                return abreviacaoAutoridadesConstants[item].desc;
            }
        };

        $scope.getDescricaoAbrangencia = function (item) {
            return abrangenciaEnum[item].desc;
        };

    });
