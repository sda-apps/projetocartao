'use strict';

angular.module('projetocartaoApp')
    .controller('ListagemPTAsController', function ($scope, $timeout, $http, BuscaPlanoAnual, $state, $rootScope, Principal, planoAnualConstants, rolesAccess) {
        $scope.mostraInput = "ano";
        $scope.filtros = [];
        $scope.planosDeTrabalho = [];
        $scope.semResultado = false;
        $scope.dto = {};
        $scope.paginacao = 10;
        $scope.start = 0;
        $scope.limit = 10;
        $scope.exibeVerMais = false;
        $scope.ptaConstants = planoAnualConstants;
        $scope.statusSelect = [];

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        for (var s in $scope.ptaConstants) {
            $scope.statusSelect.push({id: "" + s, desc: $scope.ptaConstants["" + s].desc});
        }

        $scope.$watch('mostraInput', function (input) {

            switch (input) {
                case 'ano':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-ano')).focus();
                    });
                    break;

                case 'regiao':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-regiao')).focus();
                    });
                    break;

                case 'municipio':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-municipio')).focus();
                    });
                    break;

                case 'parceira':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-parceira')).focus();
                    });
                    break;

                case 'contrato':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-contrato')).focus();
                    });
                    break;

                default:
                    break;
            }

        }, true);

        $scope.podeIncluir = function () {
            return Principal.hasAnyAuthority(rolesAccess['PTA'].incluir);
        };

        $scope.podeVisualizar = function () {
            return Principal.hasAnyAuthority(rolesAccess['PTA'].visualizar);
        };

        $scope.buscarAutoCompletePorFiltro = function (searchText, url) {
            return $http({
                method: 'GET',
                url: url + searchText
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                return response.data;
            });
        };

        $scope.buscarAutoCompleteMunicipioPorRegiao = function (searchText, url) {
            var regioes = $scope.filtros.filter(function (filtro) {
                return filtro.tipo == 'regiao';
            }).map(function (regiao) {
                return regiao.id;
            });

            if (regioes.length > 0) {
                url += 'regiao/';
            }

            return $http.get(url + searchText + '/' + regioes).then(function (retorno) {
                return retorno.data;
            });
        };

        $scope.adicioarFiltroChip = function (filtro) {
            $scope.filtros.push(filtro);

            $scope.planosDeTrabalho = [];

            $scope.dto = construirELimparFiltros();

            buscarPlanosAnuais();
        };

        $scope.removerFiltroChip = function () {
            $scope.planosDeTrabalho = [];

            $scope.dto = construirELimparFiltros();

            if ($scope.filtros.length >= 1) {
                buscarPlanosAnuais();
            } else {
                $scope.limparFiltros();
            }
        };

        $scope.limparFiltros = function () {
            $scope.filtros = [];
            $scope.planosDeTrabalho = [];
            $scope.semResultado = false;
            $scope.exibeVerMais = false;
            $scope.start = 0;
            $scope.limit = 10;
            $scope.dto = {};

            buscarPlanosAnuais();
        };

        function construirELimparFiltros() {
            var filtro = {};

            $scope.filtros.forEach(function (elemento) {
                if (filtro[elemento.tipo]) {
                    filtro[elemento.tipo].push(elemento.id);
                } else {
                    filtro[elemento.tipo] = [];
                    filtro[elemento.tipo].push(elemento.id);
                }
            });

            return filtro;
        }

        $scope.carregaMais = function () {
            $scope.start = $scope.limit;
            $scope.limit = $scope.limit + $scope.paginacao;

            $scope.dto = construirELimparFiltros($scope.filtros);

            $scope.dto.start = $scope.start;
            $scope.dto.limit = $scope.limit;

            buscarPlanosAnuais();
        };

        function buscarPlanosAnuais() {
            BuscaPlanoAnual.query($scope.dto).$promise.then(function (result) {
                $scope.semResultado = false;

                $scope.planosDeTrabalho = $scope.planosDeTrabalho.concat(result);

                $scope.start = $scope.planosDeTrabalho.length;

                $scope.exibeVerMais = result.length >= $scope.paginacao;
            })
                .catch(function () {
                    $scope.semResultado = true;
                    $scope.exibeVerMais = false;
                    $scope.planosDeTrabalho = [];
                });
        }

        $scope.carregarPlanoAnual = function (plano) {
            $state.go('pta', {planoAnualId: plano.id});
        };

        buscarPlanosAnuais();

        if ($scope.podeIncluir()) {
            $rootScope.fabAdd = function () {
                $state.go('pta', {});
            };
        }
    });
