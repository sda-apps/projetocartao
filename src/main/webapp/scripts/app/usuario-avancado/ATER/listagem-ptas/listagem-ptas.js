'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('listagem-ptas', {
                parent: 'aterUA',
                url: '/listagem-ptas',
                data: {
                    authorities: rolesAccess['PTA'].buscar,
                    pageTitle: 'ATER - Planejamento'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/listagem-ptas/listagem-ptas.html',
                        controller: 'ListagemPTAsController'
                    }
                },
                resolve: {

                }
            });
    });
