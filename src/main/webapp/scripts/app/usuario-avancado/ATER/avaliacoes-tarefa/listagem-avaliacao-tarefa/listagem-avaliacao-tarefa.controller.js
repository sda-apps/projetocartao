'use strict';

angular.module('projetocartaoApp')
    .controller('ListagemAvaliacaoTarefaController', function ($scope, $timeout, $http, $window, BuscaTarefa,
                                                               BuscaTotalTarefa, $state, $rootScope, Principal,
                                                               tarefaStatus, PlanoTrimestralPorPTALista, tarefaTipo,
                                                               abrangenciaEnum) {
        $scope.mostraInput = "municipio";
        $scope.statusSelect = [
            {id: 'T', nome: 'Não Avaliados'},
            {id: 'E', nome: 'Em Avaliação'},
            {id: 'A', nome: 'Aceitos'},
            {id: 'R', nome: 'Recusados'},
            {id: 'V', nome: 'Revisados'}
        ];
        $scope.filtros = [];
        $scope.total = 0;
        $scope.tarefas = [];
        $scope.semResultado = false;
        $scope.dto = {};
        $scope.paginacao = 5;
        $scope.start = 0;
        $scope.limit = 5;
        $scope.exibeVerMais = false;
        $scope.tarefaStatus = tarefaStatus;
        $scope.tarefaTipo = tarefaTipo;

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        $scope.$watch('mostraInput', function (input) {

            switch (input) {
                case 'plano-anual':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-plano-anual')).focus();
                    });
                    break;

                case 'plano-trimestral':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-plano-trimestral')).focus();
                    });
                    break;

                case 'municipio':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-municipio')).focus();
                    });
                    break;

                case 'comunidade':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-comunidade')).focus();
                    });
                    break;

                case 'atividade':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-atividade')).focus();
                    });
                    break;

                case 'parceira':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-parceira')).focus();
                    });
                    break;

                default:
                    break;
            }

        }, true);

        $scope.buscarAutoCompletePorFiltro = function (searchText, url) {
            return $http({
                method: 'GET',
                url: url + searchText
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                return response.data;
            });
        };

        $scope.buscarAutoCompleteComunidadePorMunicipio = function (searchText, url) {
            var municipios = $scope.filtros.filter(function (filtro) {
                return filtro.tipo == 'municipio';
            }).map(function (municipio) {
                return municipio.id;
            });

            if (municipios.length > 0) {
                url += 'municipio/';
            }

            return $http.get(url + searchText + '/' + municipios).then(function (retorno) {
                return retorno.data;
            });
        };

        $scope.adicionarFiltroChip = function (filtro) {
            $scope.filtros.push(filtro);

            $scope.tarefas = [];

            $scope.dto = construirELimparFiltros();
            buscarTarefas();
        };

        $scope.removerFiltroChip = function () {
            $scope.tarefas = [];

            $scope.dto = construirELimparFiltros();

            if ($scope.filtros.length >= 1) {
                buscarTarefas();
            } else {
                $scope.limparFiltros();
            }
        };

        $scope.limparFiltros = function () {
            $scope.filtros = [];
            $scope.atividades = [];
            $scope.semResultado = false;
            $scope.exibeVerMais = false;
            $scope.start = 0;
            $scope.limit = 10;
            $scope.dto = {};

            buscarTarefas();
        };

        function construirELimparFiltros() {
            var filtro = {};

            $scope.filtros.forEach(function (elemento) {
                if (filtro[elemento.tipo]) {
                    filtro[elemento.tipo].push(elemento.id);
                } else {
                    if (elemento.tipo === 'agricultor' || elemento.tipo === 'status') {
                        filtro[elemento.tipo] = elemento.id;
                    } else {
                        filtro[elemento.tipo] = [];
                        filtro[elemento.tipo].push(elemento.id);
                    }
                }
            });

            return filtro;
        }

        $scope.carregaMais = function () {
            $scope.start = $scope.limit;
            $scope.limit = $scope.limit + $scope.paginacao;

            $scope.dto = construirELimparFiltros($scope.filtros);

            $scope.dto.start = $scope.start;
            $scope.dto.limit = $scope.limit;

            buscarTarefas();
        };

        function formatarMesesTrimestre(result) {
            for (var i in result) {
                if (result[i].mes != null) {
                    result[i].mesFormatado = getMesPorNumero(result[i].mes) + "/" + getMesPorNumero(result[i].mes + 1) + "/" + getMesPorNumero(result[i].mes + 2)
                }
            }
            return result;
        }

        function buscarTarefas() {
            BuscaTotalTarefa.get($scope.dto, function(result) {
                $scope.total = result.total;
            });

            BuscaTarefa.query($scope.dto).$promise.then(function (result) {
                $scope.semResultado = false;
                result = formatarMesesTrimestre(result);
                $scope.tarefas = $scope.tarefas.concat(result);
                $scope.start = $scope.tarefas.length;
                $scope.exibeVerMais = result.length >= $scope.paginacao;

            }).catch(function () {
                $scope.exibeVerMais = false;
                $scope.semResultado = true;

                if ($scope.filtros.length > 0) {
                    $scope.tarefas = [];
                }
            });
        }

        $scope.carregarTarefa = function (tarefa) {
            //$state.go('avaliacao-tarefa', {tarefaId: tarefa.id});
            var newWindow = $window.open($state.href('avaliacao-tarefa'), '_blank');
            newWindow.tarefaId = tarefa.id;
            $mdDialog.cancel();
        };

        buscarTarefas();

        function calculaMes() {
            for (var x in $scope.ptList) {
                if ($scope.ptList[x].primeiroMes != null) {
                    var mes = new Date($scope.ptList[x].primeiroMes).getMonth();
                    $scope.ptList[x].mes = mes + 1;
                }
            }
        }

        $scope.$watch('filtros', function () {
            if ($scope.filtros.length > 0) {
                var ptas = $scope.filtros.filter(function (filtro) {
                    return filtro.tipo == 'planoTrabalhoAnual';
                }).map(function (pta) {
                    return pta.id;
                });
                if (ptas.length > 0) {
                    PlanoTrimestralPorPTALista.get({id: ptas}).$promise.then(function (result) {
                        $scope.ptList = result;
                        calculaMes();
                        $scope.ptList = formatarMesesTrimestre($scope.ptList);
                    }).catch(function () {
                    });
                }
            }
        }, true);

        function getMesPorNumero(mes) {
            switch (mes) {
                case 1:
                    return "Jan";
                case 2:
                    return "Fev";
                case 3:
                    return "Mar";
                case 4:
                    return "Abr";
                case 5:
                    return "Mai";
                case 6:
                    return "Jun";
                case 7:
                    return "Jul";
                case 8:
                    return "Ago";
                case 9:
                    return "Set";
                case 10:
                    return "Out";
                case 11:
                    return "Nov";
                case 12:
                    return "Dez";
                case 13:
                    return "Jan";
                case 14:
                    return "Fev";
            }
        }

        $scope.getDescricaoTipo = function (item) {
            return tarefaTipo[item].desc;
        };

        $scope.getLocalization = function (item) {
            if (item.comunidade) {
                return item.comunidade + ", " + item.municipio + ", " + item.uf
            } else {
                return item.municipio + ", " + item.uf
            }
        };

        $scope.getDescricaoAbrangencia = function (item) {
            if (item != null) {
                return abrangenciaEnum[item].desc;
            } else {
                return null;
            }
        };
    });
