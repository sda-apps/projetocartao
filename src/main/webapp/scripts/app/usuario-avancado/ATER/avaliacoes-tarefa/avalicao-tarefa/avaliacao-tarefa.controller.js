'use strict';

angular.module('projetocartaoApp')
    .controller('AvaliacaoTarefaController', function ($scope, $rootScope, $state, $timeout, $window, Tarefa,
                                                       TarefaComplemento, $stateParams, $mdDialog,
                                                       rolesAccess, Principal, $filter, tarefaStatus,
                                                       HistoricoAvaliacaoTarefa, MultipartFile, MultipartFileListURL,
                                                       abrangenciaEnum, tarefaTipo, $http, Toast, AnexoTarefa, $location) {

        $scope.tarefa = {};
        $scope.tarefaStatus = tarefaStatus;
        $scope.tarefaTipo = tarefaTipo;
        $scope.abrangenciaEnum = abrangenciaEnum;
        $scope.historico = [];
        $scope.exibirHistorico = false;
        $scope.fotos = [];
        $scope.showMensagemErro = false;

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        $scope.podeAvaliar = function () {
            return Principal.hasAnyAuthority(rolesAccess['AVALIACAO_TAREFA'].avaliar) && ($scope.tarefa.status == tarefaStatus.T.id || $scope.tarefa.status == tarefaStatus.E.id)
        };

        if($location.search().id) {
            getTarefaPorId($location.search().id);
        } else if ($window.tarefaId) {
            getTarefaPorId($window.tarefaId);
        } else {
            $state.go('listagem-avaliacao-tarefa');
        }

        function getTarefaPorId(tarefaId) {
            console.log(tarefaId);
            Tarefa.get({id: tarefaId}).$promise.then(function (result) {
                $scope.tarefa = result;
                criarMapaGoogle();
                $rootScope.subToolbar['title'] = $scope.tarefa.planoTrimestral.planoTrabalhoAnual.codigo + " - " + gerarTitulo();
                $scope.historico = $scope.historico = $filter('orderBy')($scope.tarefa.historico, 'criacao', true);

                if ($scope.podeAvaliar()) {
                    $rootScope.subToolbar["evaluateButton"] = avaliarTarefa;
                }else{
                    $rootScope.subToolbar["evaluateButton"] = null;
                }
            });

            try {
                MultipartFileListURL.get({bucketName: "cartao/ater_atividades/" + tarefaId + "/photos/"}).$promise.then(function (urls) {
                    var root = urls[0];
                    urls.shift();

                    for (var i in urls) {
                        if (typeof(urls[i]) == 'string') {
                            var fileName = urls[i].replace(root, "");
                            MultipartFile.get({
                                bucketName: "cartao/ater_atividades/" + tarefaId + "/photos",
                                fileName: fileName
                            }).$promise.then(function (result) {
                                $scope.fotos.push(window.URL.createObjectURL(result.response));

                            }).catch(function (erro) {
                                $scope.showMensagemErro = true;
                                console.log(erro);

                            });
                        }
                    }
                }).catch(function (erro) {
                    $scope.showMensagemErro = true;
                    console.log(erro);
                });
            } catch (e) {
                console.log(e);
            }

        }

        function gerarTitulo() {
            var mes = new Date($scope.tarefa.dataAteste).getMonth();
            return $scope.tarefa.planoTrimestral.trimestre +
                "º Trimestre(" + getMesPorNumero(mes) + "/" + getMesPorNumero(mes + 1) + "/" + getMesPorNumero(mes + 2) + ")";
        }

        function getMesPorNumero(mes) {
            switch (mes) {
                case 0:
                    return "Jan";
                case 1:
                    return "Fev";
                case 2:
                    return "Mar";
                case 3:
                    return "Abr";
                case 4:
                    return "Mai";
                case 5:
                    return "Jun";
                case 6:
                    return "Jul";
                case 7:
                    return "Ago";
                case 8:
                    return "Set";
                case 9:
                    return "Out";
                case 10:
                    return "Nov";
                case 11:
                    return "Dez";
                case 12:
                    return "Jan";
                case 13:
                    return "Fev";
            }
        }

        function marcarMapa(coordenada) {
            var pinIcon = {
                url: 'assets/images/pin.svg',
                size: new google.maps.Size(29, 40)
            };

            var marker = new google.maps.Marker({
                position: {
                    lat: Number((coordenada.latitude)),
                    lng: Number((coordenada.longitude))
                },
                map: $scope.mapGoogle,
                icon: pinIcon
            });
        }

        function criarMapaGoogle() {
            $scope.mapGoogle = new google.maps.Map(document.getElementById('map-avaliacao-tarefa'), {
                mapTypeId: google.maps.MapTypeId.HYBRID,
                scrollwheel: true,
                zoom: 6
            });

            if ($scope.tarefa.coordenada) {
                marcarMapa($scope.tarefa.coordenada);
                atualizaImagensGoogleMapsComCoordenada($scope.tarefa.coordenada, 16);
            } else {
                atualizaImagensGoogleMaps();
            }
        }

        function atualizaImagensGoogleMapsComCoordenada(coordenada, zoom) {
            $timeout(function () {
                google.maps.event.trigger($scope.mapGoogle, 'resize');
                $scope.mapGoogle.setZoom(zoom);
                $scope.mapGoogle.setCenter(new google.maps.LatLng({
                    lat: Number((coordenada.latitude)),
                    lng: Number((coordenada.longitude))
                }));
            }, 100);
        }

        function atualizaImagensGoogleMaps() {
            $timeout(function () {
                google.maps.event.trigger($scope.mapGoogle, 'resize');
                $scope.mapGoogle.setZoom(7);
                $scope.mapGoogle.setCenter(new google.maps.LatLng({
                    lat: Number(-4.75),
                    lng: Number(-39.2)
                }));
            }, 400);
        }

        function avaliarTarefa() {
            $mdDialog.show({
                controller: 'AvaliarTarefaController',
                templateUrl: 'scripts/components/dialogs/usuario-avancado/avaliar-tarefa/avaliar-tarefa.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                disableParentScroll: true,
                locals: {
                    params: $scope.tarefa,
                    historico: $scope.historico
                },
                fullscreen: true
            }).then(function (answer) {
                $scope.historico.unshift(answer);
                $scope.fotos = [];
                getTarefaPorId($scope.tarefa.id);
            }, function () {
                //cancel
            });
        }

        $scope.carregaFoto = function (posicao, fotos) {
            $mdDialog.show({
                controller: 'GaleriaFotosController',
                templateUrl: 'scripts/components/dialogs/galeria-fotos/galeria-fotos.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                disableParentScroll: true,
                locals: {
                    fotos: fotos,
                    posicao: posicao

                },
                fullscreen: true
            }).then(function (answer) {

            }, function () {
                //cancel
            });
        };

        $rootScope.subToolbar = {
            backButton: function () {
                $state.go('listagem-avaliacao-tarefa');
            },
            cancelButton: function () {
                $state.go('listagem-avaliacao-tarefa');
            },
            viewButton: function () {
                TarefaComplemento.get({id: $scope.tarefa.id}).$promise.then(function (complemento) {
                    $mdDialog.show({
                        controller: 'DetalhesTarefaController',
                        templateUrl: 'scripts/components/dialogs/usuario-avancado/tarefa/detalhes-tarefa.html',
                        parent: angular.element(document.body),
                        clickOutsideToClose: true,
                        disableParentScroll: true,
                        locals: {
                            tarefa: $scope.tarefa,
                            params: complemento,
                            planoTrimestral: gerarTitulo()
                        },
                        fullscreen: true
                    }).then(function (answer) {
                        //return
                    }, function () {
                        //cancel
                    });
                });
            }
        }

        $scope.selectFile = function () {
            var fileInput = document.getElementById("fileInputTarefa");
            if (fileInput) {
                fileInput.click();
            }
        };

        $scope.$watch('attachTarefa', function (newValue, oldValue) {
            if (newValue) {
                $scope.uploadFile();
            }
        });

        $scope.uploadFile = function () {
            if ($scope.attachTarefa.name) {
                var file = $scope.attachTarefa;
                var filename = $scope.attachTarefa.name;
                var bucketName = 'cartao/portal/tarefa/' + $scope.tarefa.id;
                var url = "api/files";

                var data = new FormData();
                data.append('file', file);
                data.append('bucketName', bucketName);

                var config = {
                    transformRequest: angular.identity,
                    transformResponse: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http.post(url, data, config).then(function (response) {
                    Toast.sucesso("O arquivo " + filename + " foi salvo com sucesso.");
                    $scope.tarefa.anexo = $scope.attachTarefa.name;
                    AnexoTarefa.update($scope.tarefa);
                }).catch(function (error) {
                    console.log(error);
                    Toast.erro("Erro ao salvar o arquivo " + filename + ".");
                });
            }
        };

        $scope.downloadFile = function () {

            var url = "api/files",
                bucketName = 'cartao/portal/tarefa/' + $scope.tarefa.id,
                fileName = $scope.tarefa.anexo;

            if (!bucketName || !fileName) {
                Toast.alerta("O arquivo selecionado não existe.");
                return;
            }

            var data = {
                'bucketName': bucketName,
                'fileName': fileName
            };

            var config = {
                params: data,
                responseType: 'arraybuffer',
                headers: {'Accept': 'multipart/form-data'}
            };

            $http.get(url, config).success(function (data, status, headers) {
                headers = headers();
                var filename = headers['x-filename'];
                var contentType = headers['content-type'];

                var linkElement = document.createElement('a');
                var blob = new Blob([data], {type: 'application/pdf'});
                var url = window.URL.createObjectURL(blob);

                linkElement.setAttribute('href', url);
                linkElement.setAttribute("download", $scope.tarefa.anexo);

                var clickEvent = new MouseEvent("click", {
                    "view": window,
                    "bubbles": true,
                    "cancelable": false
                });
                linkElement.dispatchEvent(clickEvent);
            }).error(function (data) {
                console.log(data);
            });
        };
    });
