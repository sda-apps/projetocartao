'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('avaliacao-tarefa', {
                parent: 'aterUA',
                url: '/avaliacao-tarefa',
                data: {
                    authorities: rolesAccess['AVALIACAO_TAREFA'].visualizar,
                    pageTitle: 'Avaliação - Tarefas'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/avaliacoes-tarefa/avalicao-tarefa/avaliacao-tarefa.html',
                        controller: 'AvaliacaoTarefaController'
                    }
                },
                params: {
                    tarefaId: null
                }
            });
    });
