'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('listagem-avaliacao-tarefa', {
                parent: 'aterUA',
                url: '/listagem-avaliacao-tarefa',
                data: {
                    authorities: rolesAccess['AVALIACAO_TAREFA'].buscar,
                    pageTitle: 'Avaliação - Tarefas'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/avaliacoes-tarefa/listagem-avaliacao-tarefa/listagem-avaliacao-tarefa.html',
                        controller: 'ListagemAvaliacaoTarefaController'
                    }
                },
                resolve: {

                }
            });
    });
