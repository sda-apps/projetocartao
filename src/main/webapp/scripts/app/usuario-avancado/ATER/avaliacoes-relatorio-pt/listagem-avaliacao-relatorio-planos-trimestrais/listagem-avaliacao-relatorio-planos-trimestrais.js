'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('listagem-avaliacao-relatorio-planos-trimestrais', {
                parent: 'aterUA',
                url: '/listagem-avaliacao-relatorio-planos-trimestrais',
                data: {
                    authorities: rolesAccess['MENU_AVALIACAO'].abrir,
                    pageTitle: 'Avaliação - Relatório Trimestral'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/avaliacoes-relatorio-pt/listagem-avaliacao-relatorio-planos-trimestrais/listagem-avaliacao-relatorio-planos-trimestrais.html',
                        controller: 'ListagemAvaliacaoRelatorioPlanoTrimestralController'
                    }
                }
            });
    });
