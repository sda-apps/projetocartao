'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('avaliacao-relatorio-plano-trimestral', {
                parent: 'aterUA',
                url: '/avaliacao-relatorio-plano-trimestral',
                data: {
                    authorities: rolesAccess['MENU_AVALIACAO'].abrir,
                    pageTitle: 'Avaliação - Relatório Trimestral'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/avaliacoes-relatorio-pt/avaliacao-relatorio-plano-trimestral/avaliacao-relatorio-plano-trimestral.html',
                        controller: 'AvaliacaoRelatorioPlanoTrimestralController'
                    }
                },
                params: {
                    plano: null
                }
            });
    });
