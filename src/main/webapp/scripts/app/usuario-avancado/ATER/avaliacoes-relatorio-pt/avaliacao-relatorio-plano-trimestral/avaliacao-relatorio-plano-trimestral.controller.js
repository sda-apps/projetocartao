'use strict';

angular.module('projetocartaoApp')
    .controller('AvaliacaoRelatorioPlanoTrimestralController', function ($scope, $rootScope, $state, PlanoTrimestral, HistoricoAvaliacaoRelatorio,
                                                                $stateParams, planoTrimestralConstants, identificadorRoles, $mdDialog,
                                                                $filter, abreviacaoAutoridadesConstants, abrangenciaEnum, Principal,
                                                                 relatorioPlanoTrimestralConstants, $http) {

        $scope.historico = [];
        $scope.planoTrimestralConstants = planoTrimestralConstants;
        $scope.relatorioPlanoTrimestralConstants = relatorioPlanoTrimestralConstants;
        $scope.plano = $stateParams.plano;
        $scope.showDetail = [];
        $rootScope.subToolbar = {
            title: "Relatório Trimestral",
            backButton: function () {
                $state.go('listagem-avaliacao-relatorio-planos-trimestrais');
            },
            cancelButton: function () {
                $state.go('listagem-avaliacao-relatorio-planos-trimestrais');
            },
            viewButton: function () {
                downloadPTFile($stateParams.plano);
            }
        };

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        $scope.expandDetail = function (id) {
            $scope.showDetail[id] = !$scope.showDetail[id];
        };

        function getHistorico() {
            HistoricoAvaliacaoRelatorio.query({id: $scope.plano.id}).$promise.then(function (response) {
                $scope.historico = historicRefactored(response);
                $scope.historico = $filter('orderBy')($scope.historico, 'criacao', true);
                $scope.approvedsOrRejects = historicHasElementApprovedOrRejected();
            }).catch(function (error) {
                console.log(error);
            });
        }

        function historicRefactored(historico) {
            return historico.map(function (item) {
                item.usuarioName = item.usuario.usuarioAvancado.pessoaFisica.pessoa.nome;
                item.atualizacao = new Date();
                console.log(item);
                //plano.fileStatus
                if (item.motivo === 'A') {
                    item.motivoDesc = "Aprovado";
                } else if (item.motivo === 'R') {
                    item.motivoDesc = "Recusado";
                }

                return item;
            })
        }

        function historicHasElementApprovedOrRejected() {
            return $scope.historico.filter(function (item) {
                return item.motivo === 'A' || item.motivo === 'R';
            })
        }

        $scope.isRecused = function (plano) {
            return plano && plano.recusado === true;
        };

        $scope.isRevised = function (plano) {
            return plano && plano.status !== '1' && plano.recusado === true;
        };

        $scope.podeAvaliar = function () {
            if ($scope.plano) {
                switch ($scope.plano.fileStatus) {
                    case "A":
                        return false;
                    case "R":
                        return false;
                    case "E":
                        return true;
                }
            }
        };

        $scope.showAvaliacaoDialog = function () {
            $mdDialog.show({
                controller: 'AvaliarPlanoController',
                templateUrl: 'scripts/components/dialogs/usuario-avancado/avaliar-plano/avaliar-plano.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                disableParentScroll: true,
                locals: {
                    plano: angular.copy($scope.plano),
                    historico: angular.copy($scope.historico),
                    tipoAvaliacao: 'R',
                    titleWindow: "Avaliar Relatório Trimestral"
                },
                fullscreen: true
            }).then(function (answer) {
                console.log(answer);
                $scope.plano.fileStatus = answer.fileStatus;
                $scope.historico.unshift(answer.historicInserted);
                $scope.historico = historicRefactored($scope.historico);
                $rootScope.subToolbar["evaluateButton"] = undefined;
            }, function () {
                //cancel
            });
        };

        function downloadPTFile (planoTrimestral) {
            var url = "api/files",
                bucketName = planoTrimestral.filePath,
                fileName = planoTrimestral.fileName;

            var data = {
                'bucketName': bucketName,
                'fileName': fileName
            };

            var config = {
                params: data,
                responseType: 'arraybuffer',
                headers: {'Accept': 'multipart/form-data'}
            };

            $http.get(url, config).success(function (data, status, headers) {
                headers = headers();
                var filename = headers['x-filename'];
                var contentType = headers['content-type'];

                var linkElement = document.createElement('a');
                var blob = new Blob([data], {type: 'application/pdf'});
                var url = window.URL.createObjectURL(blob);

                linkElement.setAttribute('href', url);
                linkElement.setAttribute("download", fileName);

                var clickEvent = new MouseEvent("click", {
                    "view": window,
                    "bubbles": true,
                    "cancelable": false
                });
                linkElement.dispatchEvent(clickEvent);
            }).error(function (data) {
                console.log(data);
            });
        }

        if ($scope.plano && $scope.plano.id) {
            getHistorico();
            if ($scope.podeAvaliar()) {
                $rootScope.subToolbar["evaluateButton"] = $scope.showAvaliacaoDialog;
            }
        } else {
            $state.go('listagem-avaliacao-relatorio-planos-trimestrais');
        }

    });
