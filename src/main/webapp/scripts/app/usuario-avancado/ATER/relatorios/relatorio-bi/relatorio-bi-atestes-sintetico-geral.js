'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-bi-atestes-sintetico-geral', {
                parent: 'aterUA',
                url: '/atestes-sintetico-geral',
                data: {
                    authorities: rolesAccess['RELATORIO_BI'].visualizar,
                    pageTitle: 'Atestes - Sintetico Geral'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-bi/relatorio-bi-atestes-sintetico-geral.html',
                        controller: 'RelatorioBIController'
                    }
                },
                resolve: {

                }
            });
    });
