'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-bi-cadastro-familia', {
                parent: 'aterUA',
                url: '/cadastro-familias',
                data: {
                    authorities: rolesAccess['RELATORIO_BI'].visualizar,
                    pageTitle: 'Cadastros de Famílias',
                    groups: "6c59134e-6932-42dd-9223-340ca297d449",
                    reports: "4db8ac99-e444-4002-a519-05105da43316"
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-bi/relatorio-bi-cadastro-familia.html',
                        controller: 'RelatorioBIController'
                    }
                },
                resolve: {

                }
            });
    });
