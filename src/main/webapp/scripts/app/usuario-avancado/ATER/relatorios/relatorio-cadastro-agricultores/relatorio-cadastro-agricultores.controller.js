'use strict';

angular.module('projetocartaoApp')
    .controller('RelatorioCadastroAgricultoresController', function ($rootScope, $scope, $window, $http,
                                                                     Principal, Regiao, EntidadePorRegiao,
                                                                     MunicipioPorEmpresa, ComunidadePorMunicipio,
                                                                     LocalidadePorComunidade, DateUtils, FileSaver,
                                                                     MunicipiosPorRegiao, BuscaRegioesPlanoAnualPorTecnico,
                                                                     TiposDeComunidadeEnum, AreaAtuacaoPorId,
                                                                     ComunidadePorMunicipioEAreaAtuacao) {
        $scope.regiaoList = [];
        $scope.disableAtc = true;
        $scope.disableMunicipio = true;
        $scope.disableComunidade = true;
        $scope.disableLocalidade = true;
        $scope.formatoArquivo = 'pdf';
        $scope.tiposComunidade = Object.values(TiposDeComunidadeEnum);
        $scope.tiposComunidade.splice(0, 0, {id: 0, desc: 'Selecionar Todos'});

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        $scope.isUsuarioRestrito = function () {
            return Principal.hasAnyAuthority(['ROLE_RESPONSAVEL_ATER', 'ROLE_USUARIO_COORDENADOR_ATER', 'ROLE_USUARIO_ATER_ASSESSOR', 'ROLE_USUARIO_ATER_TECNICO'])
        };

        if ($scope.isUsuarioRestrito()) {
            setCamposUsuarioRestrito();
        } else {
            buscarRegioes();
        }

        function buscarRegioes() {
            Regiao.query().$promise
                .then(function (result) {
                    $scope.regiaoList = result;
                    $scope.regiaoList.splice(0, 0, {id: 0, nome: 'Selecionar Todos'});
                }).catch(function (response) {
            });
        }

        function setCamposUsuarioRestrito() {
            $scope.filtro = {
                atc: [$rootScope.account.usuarioAvancado.empresa.id]
            };

            $scope.tecnicoATC = $rootScope.account.usuarioAvancado.empresa.pessoaJuridica.nomeFantasia;

            $scope.disableMunicipio = false;

            buscarRegioes();

            BuscaRegioesPlanoAnualPorTecnico.get({tecnicoId: $rootScope.account.usuarioAvancado.id}).$promise
                .then(function (result) {
                    $scope.filtro.regiao = result;
                    $scope.disableRegiao = true;

                    AreaAtuacaoPorId.get({usuarioAvancadoId: $rootScope.account.usuarioAvancado.id}).$promise.then(function (result) {
                        $scope.municipioList = result;
                    });
                });
        }

        $scope.aposSelecionarRegiao = function (regiao) {
            $scope.atcList = [];
            $scope.municipioList = [];
            var regiaoId = [];

            if (regiao != undefined) {
                if (regiao.indexOf(0) > -1) {
                    $scope.atcList.push({id: 0, nomeFantasia: 'Selecionar Todos'});
                    $scope.municipioList.splice(0, 0, {id: 0, nome: 'Selecionar Todos'});
                    $scope.filtro.regiao = [];
                    $scope.filtro.regiao.push("0");
                } else {
                    for (var i in regiao) {
                        regiaoId.push(parseInt(regiao[i]));
                    }
                    $scope.filtro.regiao = regiaoId;
                    buscarEntidadePorRegiao(regiaoId);
                    buscarMunicipiosPorRegiao(regiaoId);
                }
                $scope.disableAtc = false;
                $scope.disableMunicipio = false;
            } else {
                $scope.disableAtc = true;
                $scope.filtro.atc = [];
                $scope.disableMunicipio = true;
                $scope.filtro.municipio = [];
                $scope.disableComunidade = true;
                $scope.filtro.comunidade = [];
                $scope.disableLocalidade = true;
                $scope.filtro.localidade = [];
            }
        };

        function buscarEntidadePorRegiao(regiaoId) {
            EntidadePorRegiao.get({id: regiaoId}).$promise
                .then(function (result) {
                    $scope.atcList = result;
                    $scope.atcList.splice(0, 0, {id: 0, nomeFantasia: 'Selecionar Todos'});
                }).catch(function (response) {
            });
        }

        function buscarMunicipiosPorRegiao(regiaoId) {
            MunicipiosPorRegiao.query({regioes: regiaoId}).$promise
                .then(function (result) {
                    $scope.municipioList = result;
                    $scope.municipioList.splice(0, 0, {id: 0, nome: 'Selecionar Todos'});
                }).catch(function (response) {
            });
        }

        $scope.aposSelecionarATC = function (empresa) {
            var empresaId = [];

            if (empresa.length > 0) {
                $scope.municipioList = [];
                if (empresa.indexOf(0) > -1) {
                    $scope.municipioList.push({id: 0, nome: 'Selecionar Todos'});
                    $scope.filtro.atc = [];
                    $scope.filtro.atc.push("0");
                } else {
                    for (var i in empresa) {
                        empresaId.push(parseInt(empresa[i]));
                    }

                    $scope.filtro.atc = empresaId;

                    MunicipioPorEmpresa.get({id: empresaId}).$promise
                        .then(function (result) {
                            $scope.municipioList = result;
                            $scope.municipioList.splice(0, 0, {id: 0, nome: 'Selecionar Todos'});
                            $scope.disableMunicipio = false;
                        }).catch(function (response) {
                    });
                }
                $scope.disableMunicipio = false;
            }
        };

        function buscarComunidadesPorMunicipioEAreaAtuacao(municipioId) {
            ComunidadePorMunicipioEAreaAtuacao.get({id: municipioId}).$promise
                .then(function (result) {
                    $scope.comunidadeList = result;

                }).catch(function (response) {
            });
        }

        function buscarComunidadePorMunicipio(municipioId) {
            ComunidadePorMunicipio.get({id: municipioId}).$promise
                .then(function (result) {
                    $scope.comunidadeList = result;
                    $scope.comunidadeList.splice(0, 0, {id: 0, nome: 'Selecionar Todos'});

                }).catch(function (response) {
            });
        }

        $scope.aposSelecionarMunicipio = function (municipio) {
            $scope.comunidadeList = [];
            var municipioId = [];
            if (municipio.length > 0) {
                if (municipio.indexOf(0) > -1) {
                    $scope.comunidadeList.push({id: 0, nome: 'Selecionar Todos'});
                    $scope.filtro.municipio = [];
                    $scope.filtro.municipio.push("0");
                } else {
                    for (var i in municipio) {
                        municipioId.push(parseInt(municipio[i]));
                    }
                    $scope.filtro.municipio = municipioId;

                    if ($scope.isUsuarioRestrito()) {
                        buscarComunidadesPorMunicipioEAreaAtuacao(municipioId);
                    } else {
                        buscarComunidadePorMunicipio(municipioId);
                    }
                }
                $scope.disableComunidade = false;
            } else {
                $scope.disableComunidade = true;
                $scope.filtro.comunidade = [];
                $scope.disableLocalidade = true;
                $scope.filtro.localidade = [];
            }
        };

        $scope.aposSelecionarComunidade = function (comunidade) {
            $scope.localidadeList = [];
            var comunidadeId = [];
            if (comunidade.length > 0) {
                if (comunidade.indexOf(0) > -1) {
                    $scope.localidadeList.splice(0, 0, {id: 0, nome: 'Selecionar Todos'});
                    $scope.filtro.comunidade = [];
                    $scope.filtro.comunidade.push("0");
                } else {
                    for (var i in comunidade) {
                        comunidadeId.push(parseInt(comunidade[i]));
                    }
                    $scope.filtro.comunidade = comunidadeId;
                    LocalidadePorComunidade.get({id: comunidadeId}).$promise
                        .then(function (result) {
                            $scope.localidadeList = result;
                            $scope.localidadeList.splice(0, 0, {id: 0, nome: 'Selecionar Todos'});

                        }).catch(function (response) {
                    });
                }
                $scope.disableLocalidade = false;
            } else {
                $scope.disableLocalidade = true;
                $scope.filtro.localidade = [];
            }
        };

        $scope.aposSelecionarTipoComunidade = function (tipoComunidade) {
            if (tipoComunidade.indexOf(0) > -1) {
                $scope.filtro.tipoComunidade = [];
                $scope.filtro.tipoComunidade.push("0");
            }
        };

        $scope.aposSelecionarLocalidade = function (localidade) {
            if (localidade.indexOf(0) > -1) {
                $scope.filtro.localidade = [];
                $scope.filtro.localidade.push("0");
            }
        };

        $scope.carregaRelatorio = function () {
            if ($scope.filtrosRelatorioForm.$valid) {
                if ($scope.dataInicio) {
                    $scope.filtro.dataInicio = DateUtils.formatDateUS($scope.dataInicio);
                }

                if ($scope.dataFim) {
                    $scope.filtro.dataFim = DateUtils.formatDateUS($scope.dataFim);
                }

                if ($scope.formatoArquivo === 'pdf') {
                    var newWindow = $window.open('#/loading', '_blank');

                    $http.get('api/servicos/relatorio/cadastro-agricultores', {
                        responseType: 'arraybuffer',
                        params: $scope.filtro
                    })
                        .success(function (response) {
                            var file = new Blob([response], {type: 'application/pdf'});
                            var fileURL = URL.createObjectURL(file);

                            newWindow.location.href = fileURL;
                        });
                } else {
                    $http.get('api/servicos/relatorio/cadastro-agricultores/xls', {
                        responseType: 'arraybuffer',
                        params: $scope.filtro
                    }).success(function (response) {
                        var blob = new Blob([response], {type: 'application/vnd.ms-excel'});
                        FileSaver.saveAs(blob, "report.xlsx");
                    });
                }
            }
        };

        $scope.limparFiltros = function () {
            $scope.filtro = [];
            $scope.dataInicio = null;
            $scope.dataFim = null;
            $scope.disableAtc = true;
            $scope.disableMunicipio = true;
            $scope.disableComunidade = true;
            $scope.disableLocalidade = true;
        };
    });
