'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-perfil-familias', {
                parent: 'aterUA',
                url: '/relatorio-perfil-familias',
                data: {
                    authorities: rolesAccess['RELATORIO_PERFIL_FAMILIAS'].visualizar,
                    pageTitle: 'Relatórios - Perfil das Famílias'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-perfil-familias/relatorio-perfil-familias.html',
                        controller: 'RelatorioPerfilFamiliasController'
                    }
                },
                resolve: {

                }
            });
    });
