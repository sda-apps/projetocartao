'use strict';

angular.module('projetocartaoApp')
    .controller('RelatorioAnaliticoEntidadesController', function ($rootScope, $scope, Empresa, Toast, $window, $http, FileSaver, Principal) {
        $scope.filtro = {tipo: 'pdf'};
        $scope.formato = 'pdf';

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        if(Principal.hasAnyAuthority(['ROLE_RESPONSAVEL_ATER', 'ROLE_USUARIO_COORDENADOR_ATER', 'ROLE_USUARIO_ATER_ASSESSOR', 'ROLE_USUARIO_ATER_TECNICO'])) {
            $scope.filtro = {
                parceira: [$rootScope.account.usuarioAvancado.empresa.id]
            };

            $scope.parceiraATC = $rootScope.account.usuarioAvancado.empresa.pessoaJuridica.nomeFantasia;
        }

        Empresa.query().$promise
            .then(function (result, headers) {
                $scope.empresaList = result;
            }).catch(function(response) {
        });

        $scope.carregaRelatorio = function () {
            if ($scope.filtrosRelatorioForm.$valid) {
                if($scope.filtro.equipe == null && $scope.filtro.area == null && $scope.filtro.metas == null && $scope.filtro.planos == null){
                    Toast.info("Campo Exibir é obrigatório.");
                    return;
                }

                if ($scope.formato == 'pdf') {
                    var newWindow = $window.open('#/loading', '_blank');

                    $http.get('api/servicos/relatorio/analitico-parceiras', {
                        responseType: 'arraybuffer',
                        params: $scope.filtro
                    }).success(function (response) {
                            var file = new Blob([response], {type: 'application/pdf'});
                            var fileURL = URL.createObjectURL(file);

                            newWindow.location.href = fileURL;
                    });
                } else {
                    $http.get('api/servicos/relatorio/analitico-parceiras/xls', {
                        responseType: 'arraybuffer',
                        params: $scope.filtro
                    }).success(function (response) {
                        var blob = new Blob([response], {type: 'application/vnd.ms-excel'});
                        FileSaver.saveAs(blob, "report.xlsx");
                    });
                }


            }
        };

        $scope.limparFiltros = function () {
            $scope.filtro = {tipo: 'pdf'};
        };


    });
