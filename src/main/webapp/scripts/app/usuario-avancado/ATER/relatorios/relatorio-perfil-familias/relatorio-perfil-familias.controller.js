'use strict';

angular.module('projetocartaoApp')
    .controller('RelatorioPerfilFamiliasController', function ($rootScope, $scope, Regiao, EntidadePorRegiao, MunicipiosPorRegiao,
                                                               ComunidadePorMunicipio, LocalidadePorComunidade, DateUtils,
                                                               $window,$http, FileSaver, BuscaRegioesPlanoAnualPorTecnico, Principal,
                                                               Toast, TiposDeComunidadeEnum) {
        $scope.regiaoList = [];
        $scope.disableMunicipio = true;
        $scope.disableComunidade = true;
        $scope.formato = 'pdf';
        $scope.visualizacao = 'tabela';
        $scope.visualizacaoCadastros = 'naoavaliados';
        $scope.filtro = {visualizacao: 'T', linhaBase: 'Cadastro 2017'};
        $scope.linhaBase = ['Cadastro 2017'];
        $scope.tiposComunidade = Object.values(TiposDeComunidadeEnum);
        $scope.tiposComunidade.splice(0,0,{id:0, desc: 'Selecionar Todos'});
        $scope.opcoesDeRelatorio = [
            {id: 0, 'nome': 'Características do(a) Chefe da Família'},
            {id: 1, 'nome': 'Características da População Antendida'},
            {id: 2, 'nome': 'Indicadores Sociais'},
            //{id: 3, 'nome': 'Atividades Produtivas'},
            //{id: 4, 'nome': 'Abastecimento de Água'},
            {id: 5, 'nome': 'Número de Famílias por Subsistema Produtivo'},
            {id: 6, 'nome': 'Renda Familiar Informada Per Capita'}
        ];

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        Regiao.query().$promise
            .then(function (result, headers) {
                $scope.regiaoList = result;
                $scope.regiaoList.splice(0,0,{id:0, nome: 'Selecionar Todos'});

                if(Principal.hasAnyAuthority(['ROLE_RESPONSAVEL_ATER', 'ROLE_USUARIO_COORDENADOR_ATER', 'ROLE_USUARIO_ATER_ASSESSOR', 'ROLE_USUARIO_ATER_TECNICO'])) {
                    BuscaRegioesPlanoAnualPorTecnico.get({tecnicoId: $rootScope.account.usuarioAvancado.id}).$promise
                        .then(function (result, headers) {
                            $scope.filtro.regiao = result;
                            $scope.disableRegiao = true;
                        });
                }
            }).catch(function(response) {
        });

        $scope.aposSelecionarRegiao = function (regiao) {
            $scope.municipioList = [];
            $scope.filtro.municipio = [];
            $scope.filtro.comunidade = [];

            var regiaoId = [];
            if(regiao != undefined) {
                if (regiao.indexOf(0) > -1) {
                    $scope.municipioList.push({id:0, nome: 'Selecionar Todos'});
                    $scope.filtro.regiao = [];
                    $scope.filtro.regiao.push("0");
                } else {
                    for (var i in regiao) {
                        regiaoId.push(parseInt(regiao[i]));
                    }
                    $scope.filtro.regiao = regiaoId;
                    MunicipiosPorRegiao.query({regioes: regiaoId}).$promise
                        .then(function (result, headers) {
                            $scope.municipioList = result;
                            $scope.municipioList.splice(0, 0, {id:0, nome: 'Selecionar Todos'});
                        }).catch(function (response) {
                    });
                }
                $scope.disableMunicipio = false;
            }else{
                $scope.disableMunicipio = true;
                $scope.filtro.municipio = [];
                $scope.disableComunidade = true;
                $scope.filtro.comunidade = [];
            }
        };

        $scope.aposSelecionarMunicipio = function (municipio) {
            $scope.comunidadeList = [];
            $scope.filtro.comunidade = [];

            var municipioId = [];
            if(municipio.length > 0) {
                if (municipio.indexOf(0) > -1) {
                    $scope.comunidadeList.push({id: 0, nome: 'Selecionar Todos'});
                    $scope.filtro.municipio = [];
                    $scope.filtro.municipio.push("0");
                } else {
                    for (var i in municipio) {
                        municipioId.push(parseInt(municipio[i]));
                    }
                    $scope.filtro.municipio = municipioId;
                    ComunidadePorMunicipio.get({id: municipioId}).$promise
                        .then(function (result, headers) {
                            $scope.comunidadeList = result;
                            $scope.comunidadeList.splice(0, 0, {id: 0, nome: 'Selecionar Todos'});
                        }).catch(function (response) {
                    });
                }
                $scope.disableComunidade = false;

            }else{
                $scope.disableComunidade = true;
                $scope.filtro.comunidade = [];
            }
        };

        $scope.aposSelecionarComunidade = function (comunidade) {
            if (comunidade.indexOf(0) > -1){
                $scope.filtro.comunidade = [];
                $scope.filtro.comunidade.push("0");
            }
        };

        $scope.aposSelecionarTipoComunidade = function (comunidade) {
            if (comunidade.indexOf(0) > -1){
                $scope.filtro.tipoComunidade = [];
                $scope.filtro.tipoComunidade.push("0");
            }
        };

        $scope.gerarRelatorio = function () {
            if ($scope.filtrosRelatorioForm.$valid) {

                if($scope.visualizacao == 'grafico' && $scope.filtro.municipio && $scope.filtro.comunidade) {
                    if(($scope.filtro.municipio.indexOf("0") == -1 && $scope.filtro.comunidade.length >= 1 && $scope.filtro.municipio.length != 1)
                            || ($scope.filtro.municipio.indexOf("0")  === 0 && $scope.filtro.comunidade.indexOf("0")  === 0)) {
                            Toast.alerta('Gráfico por comunidade é permitido apenas para um município.');

                            return false;
                    }
                }

                $scope.filtro.tipoRelatorio = $scope.filtro.opcoes;

                if ($scope.dataInicio) {
                    $scope.filtro.dataInicio = DateUtils.formatDateUS($scope.dataInicio);
                }

                if ($scope.dataFim) {
                    $scope.filtro.dataFim = DateUtils.formatDateUS($scope.dataFim);
                }

                $scope.filtro.visualizacaoCadastros = $scope.visualizacaoCadastros;

                if($scope.formato == 'pdf') {

                    var newWindow = $window.open('#/loading', '_blank');

                    if($scope.visualizacao == 'grafico') {
                        $http.get('api/servicos/relatorio/perfil-das-familias/grafico', {
                            responseType: 'arraybuffer',
                            params: $scope.filtro
                        }).success(function (response) {
                            var file = new Blob([response], {type: 'application/pdf'});
                            var fileURL = URL.createObjectURL(file);

                            newWindow.location.href = fileURL;
                        });
                    }

                    if($scope.visualizacao == 'tabela') {
                        $http.get('api/servicos/relatorio/perfil-das-familias', {
                            responseType: 'arraybuffer',
                            params: $scope.filtro
                        }).success(function (response) {
                            var file = new Blob([response], {type: 'application/pdf'});
                            var fileURL = URL.createObjectURL(file);

                            newWindow.location.href = fileURL;
                        });
                    }
                }

                if ($scope.formato == 'xls') {

                    if($scope.visualizacao == 'grafico') {
                        $http.get('api/servicos/relatorio/perfil-das-familias/grafico/xls', {
                            responseType: 'arraybuffer',
                            params: $scope.filtro
                        }).success(function (response) {
                            var blob = new Blob([response], {type: 'application/vnd.ms-excel'});
                            FileSaver.saveAs(blob, "report.xlsx");
                        });
                    }

                    if($scope.visualizacao == 'tabela') {
                        $http.get('api/servicos/relatorio/perfil-das-familias/xls', {
                            responseType: 'arraybuffer',
                            params: $scope.filtro
                        }).success(function (response) {
                            var blob = new Blob([response], {type: 'application/vnd.ms-excel'});
                            FileSaver.saveAs(blob, "report.xlsx");
                        });
                    }

                }
            }
        };

        $scope.limparFiltros = function () {
            $scope.filtro = {visualizacao: 'T', linhaBase: 'Cadastro 2017'};
            $scope.disableMunicipio = true;
            $scope.disableComunidade = true;
        };

    });
