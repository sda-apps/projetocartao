'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-analitico-familias', {
                parent: 'aterUA',
                url: '/relatorio-analitico-familias',
                data: {
                    authorities: rolesAccess['RELATORIO_CADASTRO_ANALITICO'].visualizar,
                    pageTitle: 'Relatórios - Cadastro de Famílias - Analítico'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-analitico-familias/relatorio-analitico-familias.html',
                        controller: 'RelatorioAnaliticoFamiliasController'
                    }
                },
                resolve: {

                }
            });
    });
