'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-bi-cadastro-familias', {
                parent: 'aterUA',
                url: '/cadastro-familias',
                data: {
                    authorities: rolesAccess['RELATORIO_BI'].visualizar,
                    pageTitle: 'Cadastros de Famílias'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-bi/relatorio-bi-cadastro-familias.html',
                        controller: 'RelatorioBIController'
                    }
                },
                resolve: {

                }
            });
    });
