'use strict';

angular.module('projetocartaoApp')
    .controller('RelatorioSinteticoResumidoController', function ($scope, $timeout, $http,  $state, CountAtividadeCadastro,CountMetasPorAtc, Principal) {

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        CountAtividadeCadastro.query().$promise
            .then(function (result, headers) {
                $scope.empresaList = result;
                $scope.quantidadeTotal = 0;
                for (var i = 0, len = $scope.empresaList.length; i < len; i++) {
                    var quantidade = parseInt($scope.empresaList[i].quantidade);
                    $scope.quantidadeTotal += quantidade;
                }
                buscaQtdMetas();
            }).catch(function(response) {
        });

        var buscaQtdMetas = function () {
            CountMetasPorAtc.query().$promise
                .then(function (result, headers) {
                    for (var i = 0; i < $scope.empresaList.length; i++) {
                        for (var e = 0; e < result.length; e++) {
                            if ($scope.empresaList[i].nomeFantasia == result[e].nomeFantasia) {
                                $scope.empresaList[i].quantidadeMeta = result[e].qtdMetas;
                            }
                        }
                    }
                }).catch(function (response) {
            });
        }
    });
