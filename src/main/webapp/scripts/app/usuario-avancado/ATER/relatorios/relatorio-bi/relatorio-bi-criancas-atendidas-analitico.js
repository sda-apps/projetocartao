'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-bi-criancas-atendidas-analitico', {
                parent: 'aterUA',
                url: '/criancas-atendidas-analitico',
                data: {
                    authorities: rolesAccess['RELATORIO_BI'].visualizar,
                    pageTitle: 'Crianças Atendidas - Analítico'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-bi/relatorio-bi-criancas-atendidas-analitico.html',
                        controller: 'RelatorioBIController'
                    }
                },
                resolve: {

                }
            });
    });
