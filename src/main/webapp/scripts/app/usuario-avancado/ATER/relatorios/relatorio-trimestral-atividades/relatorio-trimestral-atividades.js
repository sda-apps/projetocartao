'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-trimestral-atividades', {
                parent: 'aterUA',
                url: '/relatorio-trimestral-atividades',
                data: {
                    authorities: rolesAccess['RELATORIO_TRIMESTRAL_SIMPLIFICADO'].visualizar,
                    pageTitle: 'Relatórios - Trimestral de Atividades - Simplificado'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-trimestral-atividades/relatorio-trimestral-atividades.html',
                        controller: 'RelatorioTrimestralAtividadesController'
                    }
                },
                resolve: {

                }
            });
    });
