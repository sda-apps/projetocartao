'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-analitico-entidades', {
                parent: 'aterUA',
                url: '/relatorio-analitico-entidades',
                data: {
                    authorities: rolesAccess['RELATORIO_PARCEIRAS_ANALITICO'].visualizar,
                    pageTitle: 'Relatórios - Entidades Parceiras - Analítico'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-analitico-entidades/relatorio-analitico-entidades.html',
                        controller: 'RelatorioAnaliticoEntidadesController'
                    }
                },
                resolve: {

                }
            });
    });
