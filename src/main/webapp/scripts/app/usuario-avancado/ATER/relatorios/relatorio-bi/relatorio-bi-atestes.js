'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-bi-atestes', {
                parent: 'aterUA',
                url: '/atestes',
                data: {
                    authorities: rolesAccess['RELATORIO_BI'].visualizar,
                    pageTitle: 'Atestes',
                    groups: "6c59134e-6932-42dd-9223-340ca297d449",
                    reports: "7bc99edc-706e-4698-ac32-2782bda68b9c"
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-bi/relatorio-bi-atestes.html',
                        controller: 'RelatorioBIController'
                    }
                },
                resolve: {

                }
            });
    });
