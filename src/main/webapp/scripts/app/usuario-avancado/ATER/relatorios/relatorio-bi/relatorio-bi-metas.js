'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-bi-metas', {
                parent: 'aterUA',
                url: '/metas',
                data: {
                    authorities: rolesAccess['RELATORIO_BI'].visualizar,
                    pageTitle: 'Metas',
                    groups: "6c59134e-6932-42dd-9223-340ca297d449",
                    reports: "dbe643f5-ae96-41be-9ad8-bacc78b438b4"
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-bi/relatorio-bi-metas.html',
                        controller: 'RelatorioBIController'
                    }
                },
                resolve: {

                }
            });
    });
