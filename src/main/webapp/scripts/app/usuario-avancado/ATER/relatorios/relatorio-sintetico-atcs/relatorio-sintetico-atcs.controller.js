'use strict';

angular.module('projetocartaoApp')
    .controller('RelatorioSinteticoAtcsController', function ($scope, $timeout, $http,  $state, RelatorioAtcs, Principal) {

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        RelatorioAtcs.get().$promise
            .then(function (result, headers) {
                $scope.relatorio = result;
                $scope.relatorio = $scope.relatorio.groupBy('regiao');
                $scope.relatorioRegiao = Object.keys($scope.relatorio);

            }).catch(function(response) {
        });

        Array.prototype.groupBy = function(atributo) {
            return this.reduce(function(grupos, item) {
                if(item[atributo] != undefined) {
                    var val = item[atributo];

                    grupos[val] = grupos[val] || [];
                    grupos[val].push(item);

                    return grupos;
                }
            }, {});
        };

        $scope.getContagem = function(entidades,tipo){
            var total = 0;

            angular.forEach(entidades, function(entidade){
                if(tipo == 'tecnicos' && !isNaN(entidade.qtdTecnicos)) {
                    total += Number(entidade.qtdTecnicos);
                }

                if(tipo == 'assessores' && !isNaN(entidade.qtdAssessor)) {
                    total += Number(entidade.qtdAssessor);
                }

                if(tipo == 'municipios' && !isNaN(entidade.qtdMunicipio)) {
                    total += Number(entidade.qtdMunicipio);
                }

                if(tipo == 'comunidades' && !isNaN(entidade.qtdComunidade)) {
                    total += Number(entidade.qtdComunidade);
                }

                if(tipo == 'familias' && !isNaN(entidade.qtdFamilias)) {
                    total += Number(entidade.qtdFamilias);
                }

            });

            return total;
        };


    });
