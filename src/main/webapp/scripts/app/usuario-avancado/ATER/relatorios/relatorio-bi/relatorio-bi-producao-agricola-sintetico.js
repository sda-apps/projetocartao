'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-bi-producao-agricola-sintetico', {
                parent: 'aterUA',
                url: '/producao-agricola-sintetico',
                data: {
                    authorities: rolesAccess['RELATORIO_BI'].visualizar,
                    pageTitle: 'Produção Agrícola - Sintético'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-bi/relatorio-bi-producao-agricola-sintetico.html',
                        controller: 'RelatorioBIController'
                    }
                },
                resolve: {

                }
            });
    });
