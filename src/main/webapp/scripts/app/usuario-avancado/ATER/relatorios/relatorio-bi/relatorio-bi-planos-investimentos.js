'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-bi-planos-investimentos', {
                parent: 'aterUA',
                url: '/planos-investimentos',
                data: {
                    authorities: rolesAccess['RELATORIO_BI'].visualizar,
                    pageTitle: 'Planos de Investimento'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-bi/relatorio-bi-planos-investimentos.html',
                        controller: 'RelatorioBIController'
                    }
                },
                resolve: {

                }
            });
    });
