'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-sintetico-resumido', {
                parent: 'aterUA',
                url: '/relatorio-sintetico-resumido',
                data: {
                    authorities: rolesAccess['RELATORIO_CADASTRO_RESUMIDO'].visualizar,
                    pageTitle: 'Relatórios - Cadastro Resumido'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-sintetico-resumido/relatorio-sintetico-resumido.html',
                        controller: 'RelatorioSinteticoResumidoController'
                    }
                },
                resolve: {

                }
            });
    });
