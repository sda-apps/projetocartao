'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-cadastro-agricultores', {
                parent: 'aterUA',
                url: '/relatorio-cadastro-agricultores',
                data: {
                    authorities: rolesAccess['RELATORIO_CADASTRO_SINTETICO'].visualizar,
                    pageTitle: 'Relatórios - Cadastro de Famílias - Sintético'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-cadastro-agricultores/relatorio-cadastro-agricultores.html',
                        controller: 'RelatorioCadastroAgricultoresController'
                    }
                },
                resolve: {

                }
            });
    });
