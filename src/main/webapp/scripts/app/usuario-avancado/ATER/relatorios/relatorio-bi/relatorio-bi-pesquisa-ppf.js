'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-bi-pesquisa-ppf', {
                parent: 'aterUA',
                url: '/pesquisa-ppf',
                data: {
                    authorities: rolesAccess['RELATORIO_BI'].visualizar,
                    pageTitle: 'Pesquisa PPF',
                    groups: "6c59134e-6932-42dd-9223-340ca297d449",
                    reports: "4d583032-19a0-47e3-adac-7fffcb32120c"
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-bi/relatorio-bi-pesquisa-ppf.html',
                        controller: 'RelatorioBIController'
                    }
                },
                resolve: {

                }
            });
    });
