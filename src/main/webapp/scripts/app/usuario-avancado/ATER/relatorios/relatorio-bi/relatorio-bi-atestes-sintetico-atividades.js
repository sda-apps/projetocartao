'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-bi-atestes-sintetico-atividades', {
                parent: 'aterUA',
                url: '/atestes-sintetico-atividades',
                data: {
                    authorities: rolesAccess['RELATORIO_BI'].visualizar,
                    pageTitle: 'Atestes - Sintético por Atividades'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-bi/relatorio-bi-atestes-sintetico-atividades.html',
                        controller: 'RelatorioBIController'
                    }
                },
                resolve: {

                }
            });
    });
