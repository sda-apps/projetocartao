'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-bi-atestes-analitico', {
                parent: 'aterUA',
                url: '/atestes-analitico',
                data: {
                    authorities: rolesAccess['RELATORIO_BI'].visualizar,
                    pageTitle: 'Atestes - Analítico'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-bi/relatorio-bi-atestes-analitico.html',
                        controller: 'RelatorioBIController'
                    }
                },
                resolve: {

                }
            });
    });
