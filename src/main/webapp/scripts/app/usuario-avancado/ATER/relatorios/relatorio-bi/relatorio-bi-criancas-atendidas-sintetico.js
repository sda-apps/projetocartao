'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-bi-criancas-atendidas-sintetico', {
                parent: 'aterUA',
                url: '/criancas-atendidas-sintetico',
                data: {
                    authorities: rolesAccess['RELATORIO_BI'].visualizar,
                    pageTitle: 'Crianças Atendidas - Sintético'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-bi/relatorio-bi-criancas-atendidas-sintetico.html',
                        controller: 'RelatorioBIController'
                    }
                },
                resolve: {

                }
            });
    });
