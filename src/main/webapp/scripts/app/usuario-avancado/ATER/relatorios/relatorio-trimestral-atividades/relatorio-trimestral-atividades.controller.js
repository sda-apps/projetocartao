'use strict';

angular.module('projetocartaoApp')
    .controller('RelatorioTrimestralAtividadesController', function ($rootScope, $scope, Principal, Regiao, EntidadePorRegiao, BuscaPlanosAnuais, BuscaPlanoAnualEmpresa,
                                                                     PlanoTrimestralPorPTA, $filter, $window, $http) {

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        $scope.atcList = [];
        $scope.regiaoList = [];
        $scope.ptas = [];
        $scope.filtro = {tipo: "analitico"};
        $scope.disableRegiao = false;
        $scope.disablePta = false;
        $scope.disableAtc = true;

        $scope.isUsuarioRestrito = function () {
            return Principal.hasAnyAuthority(['ROLE_RESPONSAVEL_ATER', 'ROLE_USUARIO_COORDENADOR_ATER', 'ROLE_USUARIO_ATER_ASSESSOR', 'ROLE_USUARIO_ATER_TECNICO'])
        };

        if ($scope.isUsuarioRestrito()) {
            $scope.filtro = {
                atc: [$rootScope.account.usuarioAvancado.empresa.id]
            };

            $scope.parceiraATC = $rootScope.account.usuarioAvancado.empresa.pessoaJuridica.nomeFantasia;

            BuscaPlanoAnualEmpresa.query({empresaId: $rootScope.account.usuarioAvancado.empresa.id}).$promise.then(function (result) {
                $scope.ptas = result
            });

            $scope.disableRegiao = true;
        } else {
            BuscaPlanosAnuais.query().$promise.then(function (result) {
                $scope.ptas = result
            });
        }

        Regiao.query().$promise
            .then(function (result, headers) {
                $scope.regiaoList = result;
                $scope.regiaoList.splice(0, 0, {id: 0, nome: 'Selecionar Todos'});
            }).catch(function (response) {
        });

        $scope.aposSelecionarPta = function (pta) {
            if (pta != undefined) {
                $scope.filtro.pta.planosTrimestrais = [];

                PlanoTrimestralPorPTA.get({id: pta.id}, function (result) {
                    $scope.filtro.pta.planosTrimestrais = $filter('orderBy')(result, 'trimestre', false);
                });

                $scope.filtro.regiao = [pta.contrato.regiao.id];
                var empresa = {
                    id: pta.contrato.empresa.id,
                    nomeFantasia: pta.contrato.empresa.pessoaJuridica.nomeFantasia
                };
                $scope.atcList = [empresa];
                $scope.filtro.atc = [pta.contrato.empresa.id];
                $scope.disableRegiao = true;
                $scope.disableAtc = true;
            }
        };

        $scope.aposSelecionarRegiao = function (regiao) {
            $scope.atcList = [];
            var regiaoId = [];
            if (regiao != undefined) {
                if (regiao.indexOf(0) > -1) {
                    $scope.atcList.push({id: 0, nomeFantasia: 'Selecionar Todos'});
                    $scope.filtro.regiao = [];
                    $scope.filtro.regiao.push("0");
                } else {
                    for (var i in regiao) {
                        regiaoId.push(parseInt(regiao[i]));
                    }
                    $scope.filtro.regiao = regiaoId;
                    EntidadePorRegiao.get({id: regiaoId}).$promise
                        .then(function (result, headers) {
                            $scope.atcList = result;
                            $scope.atcList.splice(0, 0, {id: 0, nomeFantasia: 'Selecionar Todos'});
                        }).catch(function (response) {
                    });
                }
                $scope.filtro.pta = null;
                $scope.filtro.pt = null;
                $scope.disablePta = true;
                $scope.disableAtc = false;
            } else {
                $scope.filtro.atc = [];
                $scope.disableAtc = true;
            }
        };

        $scope.aposSelecionarATC = function (empresa) {
            var empresaId = [];
            if (empresa != undefined && empresa.length > 0) {
                if (empresa.indexOf(0) > -1) {
                    $scope.filtro.atc = [];
                    $scope.filtro.atc.push("0");
                } else {
                    for (var i in empresa) {
                        empresaId.push(parseInt(empresa[i]));
                    }
                    $scope.filtro.atc = empresaId;
                }
            }
        };

        $scope.gerarRelatorio = function () {
            if ($scope.filtrosRelatorioForm.$valid) {
                var newWindow = $window.open('#/loading', '_blank');
                $scope.filtro.ptaCodigo = $scope.filtro.pta.codigo;
                $scope.filtro.ptTrimestre = $scope.filtro.pt.trimestre+"º Trimestre";
                $scope.filtro.ptId = $scope.filtro.pt.id;

                $http.get('api/servicos/relatorio/trimestral-atividades/analitico', {
                    responseType: 'arraybuffer',
                    params: $scope.filtro
                }).success(function (response) {
                    var file = new Blob([response], {type: 'application/pdf'});
                    var fileURL = URL.createObjectURL(file);

                    newWindow.location.href = fileURL;
                });

                /*if ($scope.filtro.tipo == 'analitico') {
                    $http.get('api/servicos/relatorio/trimestral-atividades/analitico', {
                        responseType: 'arraybuffer',
                        params: $scope.filtro
                    }).success(function (response) {
                        var file = new Blob([response], {type: 'application/pdf'});
                        var fileURL = URL.createObjectURL(file);

                        newWindow.location.href = fileURL;
                    });
                } else {
                    $http.get('api/servicos/relatorio/trimestral-atividades/sintetico', {
                        responseType: 'arraybuffer',
                        params: $scope.filtro
                    }).success(function (response) {
                        var file = new Blob([response], {type: 'application/pdf'});
                        var fileURL = URL.createObjectURL(file);

                        newWindow.location.href = fileURL;
                    });
                }*/
            }
        };

        $scope.limparFiltros = function () {
            $scope.filtro = {tipo: "analitico"};
            $scope.disablePta = false;
            $scope.disableRegiao = false;
            $scope.disableAtc = true;
        };

    });
