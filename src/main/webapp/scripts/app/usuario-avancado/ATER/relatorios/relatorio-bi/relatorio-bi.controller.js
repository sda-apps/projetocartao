'use strict';

angular.module('projetocartaoApp')
    .controller('RelatorioBIController', function ($rootScope, $scope, $window, Principal, AuthPowerBI, PowerBI, Toast) {

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        AuthPowerBI.login().$promise.then(function (token){
            var dto = {
                token: token.accessToken,
                groups: $rootScope.toState.data.groups,
                reports: $rootScope.toState.data.reports
            };

            PowerBI.post(dto).$promise.then(function (result) {
                $scope.reportId = result.token;

                var config = {
                    id: dto.reports,
                    type: 'report',
                    accessToken: result.token,
                    embedUrl: "https://app.powerbi.com/reportEmbed",
                    tokenType: 1
                };

                var reportContainer = document.getElementById('reportContainer');

                var report = $window.powerbi.embed(reportContainer, config);
            }).catch(function (erro) {
                console.log(erro);
                var msg = erro.data.status + ' - ' + erro.data.error;
                Toast.erro(msg);
            });
        }).catch(function (erro) {
            console.log(erro);
            var msg = erro.data.status + ' - ' + erro.data.error;
            Toast.erro(msg);
        });

    });
