'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-sintetico-atcs', {
                parent: 'aterUA',
                url: '/relatorio-sintetico-atcs',
                data: {
                    authorities: rolesAccess['RELATORIO_PARCEIRAS_SINTETICO'].visualizar,
                    pageTitle: 'Relatórios - Entidades Parceiras - Sintético'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-sintetico-atcs/relatorio-sintetico-atcs.html',
                        controller: 'RelatorioSinteticoAtcsController'
                    }
                },
                resolve: {

                }
            });
    });
