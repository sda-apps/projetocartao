'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-bi-contatos-telefonicos', {
                parent: 'aterUA',
                url: '/contatos-telefonicos',
                data: {
                    authorities: rolesAccess['RELATORIO_BI'].visualizar,
                    pageTitle: 'Contatos Telefônicos'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-bi/relatorio-bi-contatos-telefonicos.html',
                        controller: 'RelatorioBIController'
                    }
                },
                resolve: {

                }
            });
    });
