'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-bi-investimento', {
                parent: 'aterUA',
                url: '/investimentos',
                data: {
                    authorities: rolesAccess['RELATORIO_BI'].visualizar,
                    pageTitle: 'Planos de Investimento',
                    groups: "6c59134e-6932-42dd-9223-340ca297d449",
                    reports: "add512e3-d978-4a4e-b812-d46c539f7274"
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-bi/relatorio-bi-investimento.html',
                        controller: 'RelatorioBIController'
                    }
                },
                resolve: {

                }
            });
    });
