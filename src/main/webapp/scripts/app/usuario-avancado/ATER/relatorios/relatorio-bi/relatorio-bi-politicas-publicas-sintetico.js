'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('relatorio-bi-politicas-publicas-sintetico', {
                parent: 'aterUA',
                url: '/politicas-publicas-sintetico',
                data: {
                    authorities: rolesAccess['RELATORIO_BI'].visualizar,
                    pageTitle: 'Políticas Públicas - Sintético'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/relatorios/relatorio-bi/relatorio-bi-politicas-publicas-sintetico.html',
                        controller: 'RelatorioBIController'
                    }
                },
                resolve: {

                }
            });
    });
