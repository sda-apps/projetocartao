'use strict';

angular.module('projetocartaoApp')
    .controller('ListagemEntidadesController', function ($rootScope, $scope, EntidadesFiltro, $timeout, $http, $state, Principal, rolesAccess) {

        $scope.mostraInput = 'parceira';
        $scope.filtros      = [];
        $scope.entidades = [];
        $scope.semResultado = false;
        $scope.dto          = {};
        $scope.paginacao    = 10;
        $scope.start        = 0;
        $scope.limit        = 10;
        $scope.exibeVerMais  = false;

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        if(Principal.hasAnyAuthority(rolesAccess['ENTIDADE'].incluir)) {
            $rootScope.fabAdd = function () {
                $state.go('entidades', {});
            };
        }

        $scope.podeVisualizar = function () {
            return Principal.hasAnyAuthority(rolesAccess['ENTIDADE'].visualizar);
        };

        function setFormsPristine() {
            $scope.regiaoForm.$setPristine();
            $scope.municipioForm.$setPristine();
            $scope.parceiraForm.$setPristine();
        }

        $scope.$watch('mostraInput', function(input){
            switch (input) {
                case 'regiao':
                    $timeout(function (){angular.element(document.getElementById('input-filtro-regiao')).focus();});
                    break;
                case 'municipio':
                    $timeout(function (){angular.element(document.getElementById('input-filtro-municipio')).focus();});
                    break;
                case 'parceira':
                    $timeout(function (){angular.element(document.getElementById('input-filtro-parceira')).focus();});
                    break;
                default:
                    break;
            }
        }, true);

        function buscarEntidadesPorFiltros() {
            EntidadesFiltro.query($scope.dto).$promise.then( function (result) {
                $scope.semResultado = false;
                $scope.entidades = $scope.entidades.concat(result);
                $scope.start = $scope.entidades.length;
                $scope.exibeVerMais = result.length >= $scope.paginacao;
            })
            .catch(function(){
                $scope.semResultado = true;
                $scope.exibeVerMais = false;
                $scope.entidades = new Array();
            });
        }

        $scope.carregaMais = function() {
            $scope.start = $scope.limit;
            $scope.limit = $scope.limit + $scope.paginacao;

            $scope.dto = construirELimparFiltros($scope.filtros);

            $scope.dto.start = $scope.start;
            $scope.dto.limit = $scope.limit;

            buscarEntidadesPorFiltros();
        };

        $scope.limparFiltros = function () {
            $scope.filtros = [];
            $scope.entidades = [];
            $scope.semResultado = false;
            $scope.exibeVerMais = false;
            $scope.start        = 0;
            $scope.limit        = 10;
            $scope.dto          = {};

            buscarEntidadesPorFiltros();
        };

        function construirELimparFiltros() {
            var filtro = {};
            $scope.filtros.forEach(function(elemento) {
                if (filtro.hasOwnProperty(elemento.tipo)){
                    filtro[elemento.tipo].push( elemento.id);
                } else {
                    filtro[elemento.tipo] = [elemento.id];
                }
            });
            return filtro;
        }

        function validarAdicaoFiltro(filtro) {
            return filtro.id && filtro.tipo;
        }

        $scope.removerFiltroChip = function() {
            $scope.entidades = new Array();

            $scope.dto = construirELimparFiltros();

            if($scope.filtros.length >= 1) {
                buscarEntidadesPorFiltros();
            } else {
                $scope.limparFiltros();
            }
        };

        $scope.adicioarFiltroChip = function(filtro) {
            if(validarAdicaoFiltro(filtro)) {
                $scope.filtros.push(filtro);
                $scope.entidades = new Array();
                $scope.dto = construirELimparFiltros();

                setFormsPristine();

                buscarEntidadesPorFiltros();
                switch (filtro.tipo){
                    case "regiao":
                        $scope.regiaoSelecionado = undefined;
                        break;
                    case "municipio":
                        $scope.municipioSelecionado = undefined;
                        break;
                    case "empresa":
                        $scope.parceiraSelecionada = undefined;
                        break;
                }
            }
        };

        $scope.buscarAutoCompletePorFiltro = function(searchText, url) {
            var data = $http({
                method: 'GET',
                url: url + searchText
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                return response.data;
            });

            return data;
        };

        $scope.carregarEntidade = function(entidade) {
            if($scope.podeVisualizar()) {
                $state.go('entidades', {entidadeId: entidade.id});
            }
        };

        buscarEntidadesPorFiltros();

        $scope.getRegioesContrato = function(contratoRegioes) {
            var regioes = []

            for (var idx in contratoRegioes){
                regioes.push(contratoRegioes[idx].nome);
            }

            regioes = regioes.filter( function( item, index, inputArray ) {
                return inputArray.indexOf(item) == index;
            });

            return regioes;
        }
    });
