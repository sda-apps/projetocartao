'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('listagem-entidades', {
                parent: 'aterUA',
                url: '/listagem-entidades',
                data: {
                    authorities: rolesAccess['ENTIDADE'].visualizar,
                    pageTitle: 'Administração - Entidades e Contratos'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/listagem-entidades/listagem-entidades.html',
                        controller: 'ListagemEntidadesController'
                    }
                },
                resolve: {

                }
            });
    });
