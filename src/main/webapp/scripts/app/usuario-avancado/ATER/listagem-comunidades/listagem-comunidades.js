'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('listagem-comunidades', {
                parent: 'aterUA',
                url: '/listagem-comunidades',
                data: {
                    authorities: rolesAccess['COMUNIDADE'].visualizar,
                    pageTitle: 'Administração - Comunidades'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/listagem-comunidades/listagem-comunidades.html',
                        controller: 'ListagemComunidadesController'
                    }
                },
                resolve: {

                }
            });
    });
