'use strict';

angular.module('projetocartaoApp')
    .controller('ListagemComunidadesController', function ($rootScope, $scope, ComunidadesFiltro, $timeout, $http, $state, $mdDialog, Comunidade, Toast, $filter, Principal, rolesAccess) {

        $scope.mostraInput = 'comunidade';
        $scope.filtros = [];
        $scope.comunidades = [];
        $scope.semResultado = false;
        $scope.dto = {};
        $scope.paginacao = 10;
        $scope.start = 0;
        $scope.limit = 10;
        $scope.exibeVerMais = false;

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        $scope.podeIncluir = function () {
            return Principal.hasAnyAuthority(rolesAccess['COMUNIDADE'].incluir);
        };

        $scope.podeVisualizar = function () {
            return Principal.hasAnyAuthority(rolesAccess['COMUNIDADE'].visualizar);
        };

        $scope.podeEditar = function () {
            return Principal.hasAnyAuthority(rolesAccess['COMUNIDADE'].editar);
        };

        $scope.podeExcluir = function () {
            return Principal.hasAnyAuthority(rolesAccess['COMUNIDADE'].excluir);
        };

        if($scope.podeIncluir()) {
            $rootScope.fabAdd = function () {
                $scope.carregarComunidade();
            };
        }

        function setFormsPristine() {
            $scope.regiaoForm.$setPristine();
            $scope.municipioForm.$setPristine();
            $scope.localidadeForm.$setPristine();
            $scope.distritoForm.$setPristine();
            $scope.comunidadeForm.$setPristine();
        }

        $scope.$watch('mostraInput', function (input) {
            switch (input) {
                case 'regiao':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-regiao')).focus();
                    });
                    break;
                case 'municipio':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-municipio')).focus();
                    });
                    break;
                case 'localidade':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-localidade')).focus();
                    });
                    break;
                case 'distrito':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-distrito')).focus();
                    });
                    break;
                case 'comunidade':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-comunidade')).focus();
                    });
                    break;
                default:
                    break;
            }
        }, true);

        function buscarComunidadePorFiltros() {
            ComunidadesFiltro.query($scope.dto).$promise.then(function (result) {
                $scope.semResultado = false;
                $scope.comunidades = $scope.comunidades.concat(result);
                $scope.start = $scope.comunidades.length;
                $scope.exibeVerMais = result.length >= $scope.paginacao;
            })
            .catch(function () {
                $scope.semResultado = true;
                $scope.exibeVerMais = false;

                if ($scope.filtros.length > 0) {
                    $scope.comunidades = [];
                }
            });
        }

        $scope.carregaMais = function () {
            $scope.start = $scope.limit;
            $scope.limit = $scope.limit + $scope.paginacao;

            $scope.dto = construirELimparFiltros($scope.filtros);

            $scope.dto.start = $scope.start;
            $scope.dto.limit = $scope.limit;

            buscarComunidadePorFiltros();
        };

        $scope.limparFiltros = function () {
            $scope.filtros = [];
            $scope.comunidades = [];
            $scope.semResultado = false;
            $scope.exibeVerMais = false;
            $scope.start = 0;
            $scope.limit = 10;
            $scope.dto = {};

            buscarComunidadePorFiltros();
        };

        function construirELimparFiltros() {
            var filtro = {};
            $scope.filtros.forEach(function (elemento) {
                if (filtro.hasOwnProperty(elemento.tipo)) {
                    filtro[elemento.tipo].push(elemento.id);
                } else {
                    filtro[elemento.tipo] = [elemento.id];
                }
            });
            return filtro;
        }

        function validarAdicaoFiltro(filtro) {
            return filtro.id && filtro.tipo;
        }

        $scope.removerFiltroChip = function () {
            $scope.comunidades = [];

            $scope.dto = construirELimparFiltros();

            if ($scope.filtros.length >= 1) {
                buscarComunidadePorFiltros();
            } else {
                $scope.limparFiltros();
            }
        };

        $scope.adicioarFiltroChip = function (filtro) {
            if (validarAdicaoFiltro(filtro)) {
                $scope.filtros.push(filtro);
                $scope.comunidades = [];
                $scope.dto = construirELimparFiltros();

                setFormsPristine();

                buscarComunidadePorFiltros();
                switch (filtro.tipo) {
                    case "regiao":
                        $scope.regiaoSelecionado = undefined;
                        break;
                    case "municipio":
                        $scope.municipioSelecionado = undefined;
                        break;
                    case "empresa":
                        $scope.localidadeSelecionada = undefined;
                        break;
                    case "distrito":
                        $scope.distritoSelecionado = undefined;
                        break;
                    case "comunidade":
                        $scope.comunidadeSelecionada = undefined;
                        break;
                }
            }
        };

        $scope.buscarAutoCompletePorFiltro = function (searchText, url) {
            return $http({
                method: 'GET',
                url: url + searchText
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                return response.data;
            });
        };

        buscarComunidadePorFiltros();

        $scope.deletarComunidade = function (index, item) {
            var confirm = $mdDialog.confirm()
                .title('Você gostaria de excluir a comunidade ' + item.nome + '?')
                .ok('Excluir')
                .cancel('Cancelar')
                .theme('themeButtonUA');

            $mdDialog.show(confirm).then(function () {
                Comunidade.delete({id: item.id}).$promise.then(function () {
                    Toast.sucesso("Comunidade excluída com sucesso.");
                    $scope.comunidades.splice(index, 1);
                }).catch(function (error) {
                    console.log(error);
                    Toast.erro("Erro ao excluir comunidade.");
                });
            }, function () {

            });
        };

        $scope.carregarComunidade = function (position, comunidade) {
            if($scope.podeVisualizar()) {
                $mdDialog.show({
                    controller: 'CadastrarComunidadesController',
                    templateUrl: 'scripts/components/dialogs/comunidades/cadastrar-comunidade.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,
                    disableParentScroll: true,
                    locals: {
                        currentComunidade: angular.copy(comunidade)
                    },
                    fullscreen: true
                }).then(function (answer) {
                    if(position !== undefined) {
                        $scope.comunidades.splice(position, 1, answer)
                    }
                }, function () {
                    //cancel
                });
            }
        };

        $scope.getLocalidades = function (comunidade) {
            var localidades = [];
            if (comunidade.localidades.length > 0) {
                for (var idxCont in comunidade.localidades) {
                    var localidade = comunidade.localidades[idxCont];

                    if (localidade) {
                        if (localidade.nome) {
                            localidades.push($filter('capitalize')(localidade.nome));
                        }
                    }
                }

                localidades = localidades.filter(function (item, index, inputArray) {
                    return inputArray.indexOf(item) == index;
                });
            } else {
                localidades.push("Não possui localidade");
            }
            return localidades
        }

    });
