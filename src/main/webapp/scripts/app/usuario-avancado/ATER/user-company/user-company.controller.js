'use strict';

angular.module('projetocartaoApp')
    .controller('UserCompanyController', function ($scope, $rootScope, $timeout, $http, Toast, UserCompany, $filter, $mdDialog, User, Principal, rolesAccess) {
        $scope.mostraInput = "usuario";
        $scope.filtros = [];
        $scope.usuarios = [];
        $scope.semResultado = false;
        $scope.dto = {};
        $scope.paginacao = 10;
        $scope.start = 0;
        $scope.limit = 10;
        $scope.exibeVerMais = false;
        $scope.statusSelect = [
            {id: 'A', nome: 'Ativo'},
            {id: 'I', nome: 'Inativo'},
            {id: 'B', nome: 'Bloqueado'}
        ];

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        $scope.podeIncluir = function () {
            return Principal.hasAnyAuthority(rolesAccess['USER_COMPANY'].incluir);
        };

        $scope.podeVisualizar = function () {
            return Principal.hasAnyAuthority(rolesAccess['USER_COMPANY'].visualizar);
        };

        $scope.podeEditar = function () {
            return Principal.hasAnyAuthority(rolesAccess['USER_COMPANY'].editar);
        };

        $scope.podeExcluir = function () {
            return Principal.hasAnyAuthority(rolesAccess['USER_COMPANY'].excluir);
        };

        if($scope.podeIncluir()) {
            $rootScope.fabAdd = function () {
                $scope.exibirDialogUser(null);
            };
        }

        $scope.$watch('mostraInput', function (input) {

            switch (input) {
                case 'usuario':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-usuario')).focus();
                    });
                    break;

                case 'cpf':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-cpf')).focus();
                    });
                    break;

                case 'organizacao':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-organizacao')).focus();
                    });
                    break;

                case 'funcao':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-funcao')).focus();
                    });
                    break;

                case 'status':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-status')).focus();
                    });
                    break;

                default:
                    break;
            }

        }, true);

        $scope.buscarAutoCompletePorFiltro = function (searchText, url) {
            var data = $http({
                method: 'GET',
                url: url + searchText
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                return response.data;
            });

            return data;
        };

        $scope.adicioarFiltroChip = function (filtro) {
            if (validarAdicaoFiltro(filtro)) {
                $scope.filtros.push(filtro);

                $scope.usuarios = new Array();

                $scope.dto = construirELimparFiltros();

                buscarUsuariosPorFiltros();

                setFormsPristine();
            }
        };

        function setFormsPristine() {
            $scope.usuarioForm.$setPristine();
            $scope.organizacaoForm.$setPristine();
            $scope.funcaoForm.$setPristine();
        }

        $scope.removerFiltroChip = function () {
            $scope.usuarios = new Array();

            $scope.dto = construirELimparFiltros();

            if ($scope.filtros.length >= 1) {
                buscarUsuariosPorFiltros();
            } else {
                $scope.limparFiltros();
            }
        };

        function validarAdicaoFiltro(filtro) {
            if (!filtro.id) {
                return false;
            }

            var existeFiltro = $scope.filtros.some(function (elemento) {
                return (elemento.tipo === filtro.tipo);
            });

            if (existeFiltro) {
                Toast.alerta('Filtro já adicionado');

                return false;
            }

            return true;
        }

        function construirELimparFiltros() {
            var filtro = {};

            $scope.filtros.forEach(function (elemento) {
                filtro[elemento.tipo] = elemento.id;
            });

            return filtro;
        }

        $scope.limparFiltros = function () {
            $scope.filtros = [];
            $scope.usuarios = [];
            $scope.semResultado = false;
            $scope.exibeVerMais = false;
            $scope.start = 0;
            $scope.limit = 10;
            $scope.dto = {};

            buscarUsuariosPorFiltros();

            setFormsPristine();
        };

        $scope.carregaMais = function () {
            $scope.start = $scope.limit;
            $scope.limit = $scope.limit + $scope.paginacao;

            $scope.dto = construirELimparFiltros($scope.filtros);

            $scope.dto.start = $scope.start;
            $scope.dto.limit = $scope.limit;

            buscarUsuariosPorFiltros();
        };

        function buscarUsuariosPorFiltros() {
            UserCompany.query($scope.dto).$promise.then(function (result) {
                $scope.semResultado = false;

                $scope.usuarios = $scope.usuarios.concat(result);
                $scope.start = $scope.usuarios.length;

                $scope.exibeVerMais = result.length >= $scope.paginacao;
            })
                .catch(function () {
                    $scope.semResultado = true;
                    $scope.exibeVerMais = false;

                    if ($scope.filtros.length > 0) {
                        $scope.usuarios = new Array();
                    }
                });
        }

        $scope.carregarUsuario = function (user) {
            if ($scope.podeVisualizar) {
                User.get({id: user.usuario}).$promise
                    .then(function (result) {
                        $mdDialog.show({
                            controller: 'CadastrarUserCompletoController',
                            templateUrl: 'scripts/components/dialogs/usuario-avancado/cadastrar-user-completo/cadastrar-user-completo.html',
                            parent: angular.element(document.body),
                            clickOutsideToClose: true,
                            disableParentScroll: true,
                            locals: {
                                userToEdit: result,
                                canEditPerfil: true
                            },
                            fullscreen: true
                        }).then(function (answer) {

                        }, function () {
                            $scope.usuarios = [];
                            buscarUsuariosPorFiltros();
                        });
                    }).catch(function (response) {
                });
            }
        };

        $scope.exibirDialogUser = function (user) {
            if ($scope.podeVisualizar) {
                $mdDialog.show({
                    controller: 'CadastrarEditarUserController',
                    templateUrl: 'scripts/components/dialogs/usuario-avancado/cadastrar-editar-user/cadastrar-user.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    disableParentScroll: true,
                    locals: {
                        userToEdit: user,
                        isUsuarioEmpresa: true,
                        empresa: null

                    },
                    fullscreen: true
                }).then(function (answer) {
                    $scope.usuarios = [];
                    buscarUsuariosPorFiltros();
                }, function () {
                    //cancel
                });
            }
        };

        $scope.exibirDialogAreaAtuacao = function (user) {
            $mdDialog.show({
                controller: 'AreaAtuacaoController',
                templateUrl: 'scripts/components/dialogs/usuario-avancado/area-atuacao/area-atuacao.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                disableParentScroll: true,
                locals: {
                    userToEdit: user
                },
                fullscreen: true
            })
                .then(function (answer) {
                    $scope.usuarios = [];
                    buscarUsuariosPorFiltros();
                }, function () {
                    //cancel
                });
        };

        $scope.podeVisualizar = function () {
            return Principal.hasAnyAuthority(rolesAccess['USER_COMPANY'].visualizar);
        };

        $scope.temAutoridade = function (autoridades) {
            return Principal.hasAnyAuthority(autoridades);
        };

        buscarUsuariosPorFiltros();

    });

