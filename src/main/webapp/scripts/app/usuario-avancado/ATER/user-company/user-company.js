'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('user-company', {
                parent: 'aterUA',
                url: '/usuarios-empresa',
                data: {
                    authorities: rolesAccess['USER_COMPANY'].buscar,
                    pageTitle: 'Administração - Usuários'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/user-company/user-company.html',
                        controller: 'UserCompanyController'
                    }
                },
                resolve: {

                }
            });
    });
