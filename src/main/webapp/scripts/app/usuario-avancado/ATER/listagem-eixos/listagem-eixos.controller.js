'use strict';

angular.module('projetocartaoApp')
    .controller('ListagemEixosController', function ($rootScope, $scope, Principal, EixoTematicoFiltro, $timeout, $http, $state, $mdDialog, Toast, EixoTematico, rolesAccess) {

        $scope.filtros = [];
        $scope.eixo = [];
        $scope.semResultado = false;
        $scope.dto = {};
        $scope.paginacao = 10;
        $scope.start = 0;
        $scope.limit = 10;
        $scope.exibeVerMais = false;

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        $scope.podeIncluir = function () {
            return Principal.hasAnyAuthority(rolesAccess['EIXO_TEMATICO'].incluir);
        };

        $scope.podeVisualizar = function () {
            return Principal.hasAnyAuthority(rolesAccess['EIXO_TEMATICO'].visualizar);
        };

        $scope.podeEditar = function () {
            return Principal.hasAnyAuthority(rolesAccess['EIXO_TEMATICO'].editar);
        };

        $scope.podeExcluir = function () {
            return Principal.hasAnyAuthority(rolesAccess['EIXO_TEMATICO'].excluir);
        };

        if ($scope.podeIncluir()) {
            $rootScope.fabAdd = function () {
                $scope.carregarEixo(null);
            };
        }

        function setFormsPristine() {
            $scope.eixoTematicoForm.$setPristine();
        }

        function buscarEixoTematicoPorFiltros() {
            EixoTematicoFiltro.query($scope.dto).$promise.then(function (result) {
                $scope.semResultado = false;
                $scope.eixo = $scope.eixo.concat(result);
                $scope.start = $scope.eixo.length;
                $scope.exibeVerMais = result.length >= $scope.paginacao;
            })
                .catch(function () {
                    $scope.semResultado = true;
                    $scope.exibeVerMais = false;

                    if ($scope.filtros.length > 0) {
                        $scope.eixo = [];
                    }
                });
        }

        $scope.carregaMais = function () {
            $scope.start = $scope.limit;
            $scope.limit = $scope.limit + $scope.paginacao;

            $scope.dto = construirELimparFiltros($scope.filtros);

            $scope.dto.start = $scope.start;
            $scope.dto.limit = $scope.limit;

            buscarEixoTematicoPorFiltros();
        };

        $scope.limparFiltros = function () {
            $scope.filtros = [];
            $scope.eixo = [];
            $scope.semResultado = false;
            $scope.exibeVerMais = false;
            $scope.start = 0;
            $scope.limit = 10;
            $scope.dto = {};

            buscarEixoTematicoPorFiltros();
        };

        function construirELimparFiltros() {
            var filtro = {};
            $scope.filtros.forEach(function (elemento) {
                if (filtro.hasOwnProperty(elemento.tipo)) {
                    filtro[elemento.tipo].push(elemento.id);
                } else {
                    filtro[elemento.tipo] = [elemento.id];
                }
            });
            return filtro;
        }

        function validarAdicaoFiltro(filtro) {
            return filtro.id && filtro.tipo;
        }

        $scope.removerFiltroChip = function () {
            $scope.eixo = [];
            $scope.dto = construirELimparFiltros();

            if ($scope.filtros.length >= 1) {
                buscarEixoTematicoPorFiltros();
            } else {
                $scope.limparFiltros();
            }
        };

        $scope.adicioarFiltroChip = function (filtro) {
            if (validarAdicaoFiltro(filtro)) {
                $scope.filtros.push(filtro);
                $scope.eixo = [];
                $scope.dto = construirELimparFiltros();

                setFormsPristine();

                buscarEixoTematicoPorFiltros();
                switch (filtro.tipo) {
                    case "eixo":
                        $scope.eixoSelecionado = undefined;
                        break;
                }
            }
        };

        buscarEixoTematicoPorFiltros();

        $scope.deletarEixo = function (index, item) {
            var confirm = $mdDialog.confirm()
                .title('Você gostaria de excluir o eixo temático ' + item.nome + '?')
                .ok('Excluir')
                .cancel('Cancelar')
                .theme('themeButtonUA');

            $mdDialog.show(confirm).then(function () {
                EixoTematico.delete({id: item.id}).$promise.then(function (result) {
                    Toast.sucesso("Eixo temático excluído com sucesso.");
                    $scope.eixo.splice(index, 1);
                }).catch(function (error) {
                    console.log(error);
                    Toast.erro("Erro ao excluir eixo temático.");
                });
            }, function () {

            });
        };

        $scope.carregarEixo = function (eixoTematico) {
            if ($scope.podeVisualizar()) {
                $mdDialog.show({
                    controller: 'CadastrarEixoTematicoController',
                    templateUrl: 'scripts/components/dialogs/eixo-tematico/cadastrar-eixo-tematico.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    disableParentScroll: true,
                    locals: {
                        eixtoTematicoToEdit: angular.copy(eixoTematico)
                    },
                    fullscreen: true
                }).then(function (answer) {

                }, function () {
                    $scope.limparFiltros()
                });
            }
        };


    });
