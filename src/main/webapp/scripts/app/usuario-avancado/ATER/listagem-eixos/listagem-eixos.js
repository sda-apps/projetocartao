'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('listagem-eixos', {
                parent: 'aterUA',
                url: '/listagem-eixos',
                data: {
                    authorities: rolesAccess['EIXO_TEMATICO'].visualizar,
                    pageTitle: 'Administração - Eixo Temático'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/listagem-eixos/listagem-eixos.html',
                        controller: 'ListagemEixosController'
                    }
                },
                resolve: {

                }
            });
    });
