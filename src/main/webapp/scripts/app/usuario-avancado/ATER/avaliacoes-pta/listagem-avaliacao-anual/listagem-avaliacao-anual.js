'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('listagem-avaliacao-anual', {
                parent: 'aterUA',
                url: '/listagem-avaliacao-anual',
                data: {
                    authorities: rolesAccess['MENU_AVALIACAO'].abrir,
                    pageTitle: 'Avaliação - Planos Anuais'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/avaliacoes-pta/listagem-avaliacao-anual/listagem-avaliacao-anual.html',
                        controller: 'ListagemAvaliacaoAnualController'
                    }
                },
                resolve: {

                }
            });
    });
