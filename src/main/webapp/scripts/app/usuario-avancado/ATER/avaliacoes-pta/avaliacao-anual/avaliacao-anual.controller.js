'use strict';

angular.module('projetocartaoApp')
    .controller('AvaliacaoAnualController', function ($scope, $rootScope, $mdDialog, $stateParams, $state, PlanoTrabalhoAnualPorId, ComunidadesPorMunicipio,
                                                      planoAnualConstants, identificadorRoles, HistoricoAvaliacaoPlanos, abreviacaoAutoridadesConstants, Principal) {

        $scope.meses = ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'];
        $scope.exibirDetalhe = [];
        $scope.planoAnualConstants = planoAnualConstants;
        $scope.pta = {status: "1"};
        $scope.historico = [];
        $scope.exibirHistorico = false;

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        $rootScope.subToolbar = {
            title: "Avaliação dos Planos Anuais",
            backButton: function () {
                $state.go('listagem-avaliacao-anual');
            },
            cancelButton: function () {
                $state.go('listagem-avaliacao-anual');
            }
        };

        $scope.expandirDetalhe = function (id) {
            $scope.exibirDetalhe[id] = !$scope.exibirDetalhe[id];
        };

        Array.prototype.groupBy = function (atributo) {
            return this.reduce(function (grupos, item) {
                if (item[atributo] != undefined && item[atributo].id != undefined) {
                    var val = item[atributo].id;

                    grupos[val] = grupos[val] || [];
                    grupos[val].push(item);

                    return grupos;
                }
            }, {});
        };

        $scope.podeAvaliar = function () {
            switch ($scope.pta.status) {
                case "1":
                    return false;
                case "2":
                    return identificadorRoles.isUsuarioERP($rootScope.account.authorities) ||
                        identificadorRoles.isUsuarioUGP($rootScope.account.authorities) ||
                        identificadorRoles.isAdmin($rootScope.account.authorities);
                case "3":
                    return identificadorRoles.isUsuarioERP($rootScope.account.authorities) ||
                        identificadorRoles.isUsuarioUGP($rootScope.account.authorities) ||
                        identificadorRoles.isAdmin($rootScope.account.authorities);
                case "4":
                    return identificadorRoles.isUsuarioUGP($rootScope.account.authorities) || identificadorRoles.isAdmin($rootScope.account.authorities);
                case "5":
                    return identificadorRoles.isUsuarioUGP($rootScope.account.authorities) || identificadorRoles.isAdmin($rootScope.account.authorities);
                case "6":
                    return false;
            }

        };
        if ($stateParams.planoAnualId) {
            getHistorico($stateParams.planoAnualId);
            getPlanoTrabalhoAnualPorId($stateParams.planoAnualId);
        } else {
            $state.go('listagem-avaliacao-anual');
        }

        function formatarData(data) {
            return $scope.meses[Number(data.substr(5, 2)) - 1] + "/" + data.substr(0, 4);
        }

        function construirMetasFamilias(metasFamiliasPta) {
            $scope.metasFamiliasPta = metasFamiliasPta;

            for (var meta in $scope.metasFamiliasPta) {
                if ($scope.metasFamiliasPta[meta].municipio) {
                    $scope.buscarComunidadePorMunicipio($scope.metasFamiliasPta[meta].municipio);
                }
            }

            $scope.metasFamiliasPta = $.map($scope.metasFamiliasPta.groupBy('municipio'), function (value, index) {
                return [value];
            });
        }

        $scope.buscarComunidadePorMunicipio = function (municipio) {
            ComunidadesPorMunicipio.get({municipios: municipio.id}, function (result) {
                municipio.comunidades = result;
            });
        };

        function getHistorico(planoId) {
            HistoricoAvaliacaoPlanos.query({id: planoId}).$promise.then(function (result) {
                $scope.historico = result;
            }).catch(function (error) {
                console.log(error);
            });
        }

        function getPlanoTrabalhoAnualPorId(planoId) {
            PlanoTrabalhoAnualPorId.get({id: planoId}, function (result) {
                $scope.pta = result;

                if ($scope.podeAvaliar()) {
                    $rootScope.subToolbar["evaluateButton"] = $scope.showAvaliacaoDialog;
                }

                construirMetasFamilias($scope.pta.metasFamiliasPta);
                $scope.dataInicio = formatarData($scope.pta.dataInicio);
                $scope.dataEncerramento = formatarData($scope.pta.dataEncerramento);
            });
        }

        $scope.getTotalComunidadesAtendidas = function () {
            var total = 0;

            if ($scope.metasFamiliasPta) {
                $scope.metasFamiliasPta.forEach(function (metas) {
                    metas.forEach(function (meta) {
                        total++;
                    });
                });
            }

            return total;
        };

        $scope.getTotalFamiliasAtendidas = function () {
            var total = 0;

            if ($scope.metasFamiliasPta) {
                $scope.metasFamiliasPta.forEach(function (metas) {
                    metas.forEach(function (meta) {
                        if (!isNaN(meta.quantidadeFamilias)) {
                            total += meta.quantidadeFamilias;
                        }
                    });
                });
            }

            return total;
        };

        $scope.showAvaliacaoDialog = function () {
            $mdDialog.show({
                controller: 'AvaliarPlanoController',
                templateUrl: 'scripts/components/dialogs/usuario-avancado/avaliar-plano/avaliar-plano.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                disableParentScroll: true,
                locals: {
                    plano: angular.copy($scope.pta),
                    historico: angular.copy($scope.historico),
                    tipoAvaliacao: 'A',
                    titleWindow: "Avaliar Plano de Trabalho Anual"
                },
                fullscreen: true
            }).then(function (answer) {
                $scope.pta.status = answer.status;
                $scope.pta.recusado = answer.recusado;
                $scope.historico.unshift(answer.historicInserted);
                $rootScope.subToolbar["evaluateButton"] = undefined;
            }, function () {
                //cancel
            });
        };

        $scope.getAbreviacaoAutoridade = function (item) {
            if(abreviacaoAutoridadesConstants[item]) {
                return abreviacaoAutoridadesConstants[item].desc;
            }
        };

    });
