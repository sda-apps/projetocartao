'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('avaliacao-anual', {
                parent: 'aterUA',
                url: '/avaliacao-anual',
                data: {
                    authorities: rolesAccess['MENU_AVALIACAO'].abrir,
                    pageTitle: 'Avaliação de Planos Anuais'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/avaliacoes-pta/avaliacao-anual/avaliacao-anual.html',
                        controller: 'AvaliacaoAnualController'
                    }
                },
                params: {
                    planoAnualId: null
                },
                resolve: {

                }
            });
    });
