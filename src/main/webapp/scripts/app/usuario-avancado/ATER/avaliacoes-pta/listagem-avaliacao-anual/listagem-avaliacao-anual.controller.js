'use strict';

angular.module('projetocartaoApp')
    .controller('ListagemAvaliacaoAnualController', function ($rootScope, $scope, $timeout, $http, BuscaPlanoAnualAvaliacao, $state, planoAnualConstants, Toast, Principal) {

        $scope.mostraInput = "status";
        $scope.ptaConstants = planoAnualConstants;
        $scope.filtros = [];
        $scope.planosDeTrabalho = [];
        $scope.semResultado = false;
        $scope.dto = {};
        $scope.paginacao = 10;
        $scope.start = 0;
        $scope.limit = 10;
        $scope.exibeVerMais = false;
        $scope.statusSelect = [];

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        for (var s in $scope.ptaConstants) {
            $scope.statusSelect.push({id: "" + s, desc: $scope.ptaConstants["" + s].desc});
        }

        $scope.$watch('mostraInput', function (input) {
            switch (input) {
                case 'regiao':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-regiao')).focus();
                    });
                    break;
                case 'parceira':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-parceira')).focus();
                    });
                    break;
                case 'contrato':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-contrato')).focus();
                    });
                    break;
                default:
                    break;
            }

        }, true);

        $scope.buscarAutoCompletePorFiltro = function (searchText, url) {
            return $http({
                method: 'GET',
                url: url + searchText
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                return response.data;
            });
        };

        $scope.adicioarFiltroChip = function (filtro) {
            if (validarAdicaoFiltro(filtro)) {
                $scope.filtros.push(filtro);

                $scope.planosDeTrabalho = [];

                $scope.dto = construirELimparFiltros();

                buscarPlanosAnuais();
            }
        };

        function validarAdicaoFiltro(filtro) {
            if (!filtro.id) {
                return false;
            }

            var existeFiltro = $scope.filtros.some(function (elemento) {
                return (elemento.nome === filtro.nome) && (elemento.tipo === filtro.tipo);
            });

            if (existeFiltro) {
                Toast.alerta('Filtro já adicionado');

                return false;
            }

            return true;
        }

        $scope.removerFiltroChip = function () {
            $scope.planosDeTrabalho = [];

            $scope.dto = construirELimparFiltros();

            if ($scope.filtros.length >= 1) {
                buscarPlanosAnuais();
            } else {
                $scope.limparFiltros();
            }
        };

        $scope.limparFiltros = function () {
            $scope.filtros = [];
            $scope.planosDeTrabalho = [];
            $scope.semResultado = false;
            $scope.exibeVerMais = false;
            $scope.start = 0;
            $scope.limit = 10;
            $scope.dto = {};

            buscarPlanosAnuais();
        };

        function construirELimparFiltros() {
            var filtro = {};

            $scope.filtros.forEach(function (elemento) {
                if (filtro[elemento.tipo]) {
                    filtro[elemento.tipo].push(elemento.id);
                } else {
                    filtro[elemento.tipo] = [];
                    filtro[elemento.tipo].push(elemento.id);
                }
            });

            return filtro;
        }

        $scope.carregaMais = function () {
            $scope.start = $scope.limit;
            $scope.limit = $scope.limit + $scope.paginacao;

            $scope.dto = construirELimparFiltros($scope.filtros);

            $scope.dto.start = $scope.start;
            $scope.dto.limit = $scope.limit;

            buscarPlanosAnuais();
        };

        function buscarPlanosAnuais() {
            BuscaPlanoAnualAvaliacao.query($scope.dto).$promise.then(function (result) {
                $scope.semResultado = false;
                $scope.planosDeTrabalho = $scope.planosDeTrabalho.concat(result);
                $scope.start = $scope.planosDeTrabalho.length;
                $scope.exibeVerMais = result.length >= $scope.paginacao;
            }).catch(function () {
                $scope.semResultado = true;
                $scope.exibeVerMais = false;
                $scope.planosDeTrabalho = [];
            });
        }

        $scope.carregarPlanoAnual = function (planoId) {
            $state.go('avaliacao-anual', {planoAnualId: planoId});
        };

        buscarPlanosAnuais();


    });
