'use strict';
//layout-gt-xs="row" layout-wrap
angular.module('projetocartaoApp')
    .controller('PtaController', function ($rootScope, $scope, $http, $filter, $mdDialog, $state,
                                           ContratosByEmprestaService, Atividade, SalvarPta, Toast,
                                           $stateParams, PlanoTrabalhoAnualPorId, RelatorioPTA, HistoricoAvaliacaoRelatorio,
                                           planoTrimestralConstants, ComunidadesPorMunicipio, iconStatusFile,
                                           DeletarTecnico, PlanoTrimestralPorPTA, PlanoTrimestral,
                                           TecnicosPorPTA, perfilTecnicoEnum, Principal, planoAnualConstants,
                                           rolesAccess, PlanoTrimestralCopia, PlanoTrimestralUpdateSummary) {

        $scope.pta = {};
        $scope.pta.tecnicos = [];
        $scope.pta.metasAtividadesPta = [];
        $scope.pta.planosTrimestrais = [];
        $scope.municipios = [];
        $scope.atividades = [];
        $scope.planoTrimestralConstants = planoTrimestralConstants;
        $scope.ptaConstants = planoAnualConstants;
        $scope.meses = ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'];
        $scope.selectTrimestres = [];
        $scope.familiaTotal = 0;
        $scope.exibir = [];
        $scope.perfilTecnicoEnum = perfilTecnicoEnum;
        $scope.iconStatusFile = iconStatusFile;
        $scope.modoEdicao = false;
        $scope.statusFinalizado = false;
        $scope.arquivoPlano = {};
        $scope.subToolbar = {
            title: "Novo Plano de Trabalho Anual",
            backButton: function () {
                $state.go('listagem-ptas');
            },
            cancelButton: function () {
                $state.go('listagem-ptas');
            }
        };

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        $scope.podeIncluir = function () {
            return Principal.hasAnyAuthority(rolesAccess['PTA'].incluir);
        };

        $scope.podeEditar = function () {
            return Principal.hasAnyAuthority(rolesAccess['PTA'].editar);
        };

        $scope.podeFinalizar = function () {
            return Principal.hasAnyAuthority(rolesAccess['PTA'].finalizar);
        };

        $scope.podeReabrir = function () {
            return Principal.hasAnyAuthority(rolesAccess['PTA'].reabrir);
        };

        $scope.podeIncluirTecnicos = function () {
            return Principal.hasAnyAuthority(rolesAccess['PTA_TECNICO'].incluir);
        };

        $scope.podeEditarTecnicos = function () {
            return Principal.hasAnyAuthority(rolesAccess['PTA_TECNICO'].editar);
        };

        $scope.podeExcluirTecnicos = function () {
            return Principal.hasAnyAuthority(rolesAccess['PTA_TECNICO'].excluir);
        };

        $scope.podeVisualizarTecnicos = function () {
            return Principal.hasAnyAuthority(rolesAccess['PTA_TECNICO'].visualizar);
        };

        $scope.podeIncluirPT = function () {
            return Principal.hasAnyAuthority(rolesAccess['PTA_PLANO_TRIMESTRAL'].incluir);
        };

        $scope.podeEditarPT = function () {
            return Principal.hasAnyAuthority(rolesAccess['PTA_PLANO_TRIMESTRAL'].editar);
        };

        $scope.podeExcluirPT = function () {
            return Principal.hasAnyAuthority(rolesAccess['PTA_PLANO_TRIMESTRAL'].excluir);
        };

        $scope.podeVisualizarPT = function () {
            return Principal.hasAnyAuthority(rolesAccess['PTA_PLANO_TRIMESTRAL'].visualizar);
        };

        if ($scope.podeEditar()) {
            $scope.saveCancelButtons = {
                saveButton: function () {
                    checkSave();
                }
            };
        }

        if ($scope.podeFinalizar()) {
            var finalizeObj = {
                finalizeButton: function () {
                    if ($scope.informacoesGeraisForm.$valid) {
                        if ($scope.pta.id) {
                            finalizarPlano();
                        } else {
                            Toast.alerta("O cadastro do PTA precisa ser salvo antes de finalizado.")
                        }
                    } else {
                        $scope.informacoesGeraisForm.$submitted = true;
                        Toast.alerta("O cadastro do PTA tem pendências e não pode ser finalizado.")
                    }
                }
            };
            $scope.saveCancelButtons = angular.merge(finalizeObj, $scope.saveCancelButtons);
        }

        $scope.buildToolbar = function (modeSave) {
            if (modeSave && !$scope.statusFinalizado) {
                $scope.subToolbar = Object.assign($scope.subToolbar, $scope.saveCancelButtons);
            } else {
                $scope.subToolbar.cancelButton = function () {
                    $state.go('listagem-ptas');
                };
                $scope.subToolbar.saveButton = undefined;
                $scope.subToolbar.finalizeButton = undefined;

                if ($scope.podeReabrir() && $scope.pta.status == '6') {
                    if ($scope.pta.reaberto) {
                        $scope.subToolbar.saveButton = function () {
                            checkSave();
                        };
                    } else {
                        $scope.subToolbar.reopenButton = function () {
                            $scope.dialogReabrir();
                        };
                    }
                }
            }
        };

        function checkSave() {
            if ($scope.informacoesGeraisForm.$valid) {
                $scope.salvar();
            } else {
                $scope.informacoesGeraisForm.$submitted = true;
                Toast.alerta("O cadastro do PTA tem pendências e não pode ser salvo.")
            }
        }

        function reabrirPlano() {
            $scope.pta.reaberto = true;

            SalvarPta.update($scope.pta).$promise.then(function (result) {
                $scope.subToolbar.saveButton = function () {
                    checkSave();
                };
                $scope.subToolbar.reopenButton = undefined;

                Toast.sucesso("Plano Anual reaberto com sucesso.");
            }).catch(function (error) {
                $scope.pta.reaberto = false;
                console.log(error);
                Toast.erro("Erro ao reabrir o Plano Anual.");
            });
        }

        $rootScope.subToolbar = Object.assign($scope.subToolbar, $scope.saveCancelButtons);

        if (!$stateParams.planoAnualId) {
            $scope.pta = {};
            $scope.pta.tecnicos = [];
            $scope.pta.metasAtividadesPta = [];
            $scope.pta.status = '1';
            $scope.metasFamiliasPta = [];
            $scope.habilitaTecnicos = false;
            $scope.habilitaPlanosTrimestrais = true;
            $scope.pta.planosTrimestrais = [];
        } else {
            PlanoTrabalhoAnualPorId.get({id: $stateParams.planoAnualId}, function (result) {
                $scope.modoEdicao = true;
                $scope.pta = result;
                $scope.empresaSelecionada = result.contrato.empresa;
                $scope.habilitaTecnicos = true;
                $scope.habilitaPlanosTrimestrais = false;
                setDataInicio($scope.pta.dataInicio);
                $scope.subToolbar.title = construirTextoToolbar($scope.pta);
                $scope.statusFinalizado = verificaStatusFinalizado();
                if ($scope.statusFinalizado) {
                    $scope.buildToolbar(false);
                }

                $scope.pta.metasAtividadesPta = $filter('orderBy')($scope.pta.metasAtividadesPta, 'id', true);
                $scope.pta.metasFamiliasPta = $filter('orderBy')($scope.pta.metasFamiliasPta, 'id', true);

                construirMetasFamilias($scope.pta.metasFamiliasPta);

                TecnicosPorPTA.get({id: $stateParams.planoAnualId}, function (result) {
                    $scope.pta.tecnicos = result;
                });

                PlanoTrimestralPorPTA.get({id: $stateParams.planoAnualId}, function (result) {
                    $scope.pta.planosTrimestrais = $filter('orderBy')(result, 'trimestre', false);
                });

                if ($scope.pta.plano) {
                    $scope.arquivoPlano = new File([], $scope.pta.plano);
                }

                $scope.$watch('arquivoPlano', function(newValue, oldValue) {
                    if(newValue.lastModified !== oldValue.lastModified) {
                        $scope.uploadFile();
                    }
                });
            });
        }

        $scope.buscarAutoCompletePorFiltro = function (searchText, url, filtro) {
            try {
                var search;
                if (filtro == null) {
                    search = url + searchText
                } else {
                    search = url + searchText + "/" + filtro;
                }

                return $http({
                    method: 'GET',
                    url: search
                }).then(function successCallback(response) {
                    return response.data;
                }, function errorCallback(response) {
                    return response.data;
                });
            } catch (e) {
                console.log(e);
            }
        };

        $scope.associarAtividade = function (item) {
            var index = $scope.pta.metasAtividadesPta.indexOf(item);

            for (var i in $scope.atividadeList) {
                if ($scope.atividadeList[i].id === item.atividade.id) {
                    $scope.pta.metasAtividadesPta[index].atividade = $scope.atividadeList[i];
                }
            }
        };

        $scope.cadastrarUsuario = function () {
            exibirDialogUser('scripts/components/dialogs/usuario-avancado/cadastrar-editar-user/cadastrar-user.html', null);
            $scope.coordenador = null;
        };

        var exibirDialogUser = function (template, user) {
            if ($scope.empresaSelecionada) {
                $mdDialog.show({
                    controller: 'CadastrarEditarUserController',
                    templateUrl: 'scripts/components/dialogs/usuario-avancado/cadastrar-editar-user/cadastrar-user.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,
                    disableParentScroll: true,
                    locals: {
                        userToEdit: null,
                        isUsuarioEmpresa: true,
                        empresa: $scope.empresaSelecionada
                    },
                    fullscreen: true
                }).then(function (answer) {
                }, function () {
                    //cancel
                });
            } else {
                Toast.info("Informe o contrato do Plano de Trabalho Anual.")
            }
        };

        $scope.addTecnico = function () {
            $mdDialog.show({
                controller: 'TecnicoPTAController',
                templateUrl: 'scripts/components/dialogs/usuario-avancado/tecnico-pta/tecnico-pta.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                disableParentScroll: true,
                locals: {
                    params: [angular.copy($scope.pta), $scope.comunidadesContrato, null, $scope.pta]
                },
                fullscreen: true
            }).then(function (answer) {
                if (answer) {
                    if (!$scope.pta.tecnicos) {
                        $scope.pta.tecnicos = [];
                    }

                    $scope.pta.tecnicos.push(answer);
                    $scope.habilitaPlanosTrimestrais = null;
                }
            }, function () {
                //cancel
            });
        };

        $scope.editarTecnico = function (index, item) {
            if ($scope.podeEditarTecnicos()) {
                $mdDialog.show({
                    controller: 'TecnicoPTAController',
                    templateUrl: 'scripts/components/dialogs/usuario-avancado/tecnico-pta/tecnico-pta.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,
                    disableParentScroll: true,
                    locals: {
                        params: [angular.copy($scope.pta), $scope.comunidadesContrato, angular.copy(item), $scope.pta]
                    },
                    fullscreen: true
                }).then(function (answer) {
                    if (answer) {
                        $scope.pta.tecnicos[index] = answer;
                        $scope.habilitaPlanosTrimestrais = null;
                    }
                }, function () {
                    //cancel
                });
            }
        };

        $scope.removerTecnico = function (index, tecnico) {
            var confirm = $mdDialog.confirm()
                .title('Você gostaria de excluir o técnico ' + tecnico.tecnico.pessoaFisica.pessoa.nome.split(" ")[0] + ' desse Plano de Trabalho?')
                .textContent('Todas as associações às comunidades também serão excluídas.')
                .ok('Excluir')
                .cancel('Cancelar')
                .theme('themeButtonUA');

            $mdDialog.show(confirm).then(function () {
                DeletarTecnico.delete({id: tecnico.id}).$promise.then(function (result) {
                    Toast.sucesso("Técnico excluído com sucesso.");
                    $scope.pta.tecnicos.splice(index, 1);
                }).catch(function (error) {
                    console.log(error);
                    Toast.erro("Erro ao excluir técnico.");
                });
            }, function () {

            });
        };

        $scope.selectFile = function () {
            var fileInput = document.getElementById("fileInput");
            if (fileInput) {
                fileInput.click();
            }
        };

        $scope.downloadLocalFile = function () {
            var filename = $scope.arquivoPlano.name;

            var linkElement = document.createElement('a');
            var url = window.URL.createObjectURL($scope.arquivoPlano);

            linkElement.setAttribute('href', url);
            linkElement.setAttribute("download", filename);

            var clickEvent = new MouseEvent("click", {
                "view": window,
                "bubbles": true,
                "cancelable": false
            });
            linkElement.dispatchEvent(clickEvent);
        };

        $scope.downloadFile = function () {
            if ($scope.arquivoPlano && $scope.arquivoPlano.size > 0) {
                $scope.downloadLocalFile();
                return;
            }

            var url = "api/files",
                bucketName = 'cartao/portal/plano/' + $scope.pta.id,
                fileName = $scope.pta.plano;

            if (!bucketName || !fileName) {
                Toast.alerta("O arquivo selecionado não existe.");
                return;
            }

            var data = {
                'bucketName': bucketName,
                'fileName': fileName
            };

            var config = {
                params: data,
                responseType: 'arraybuffer',
                headers: {'Accept': 'multipart/form-data'}
            };

            $http.get(url, config).success(function (data, status, headers) {
                headers = headers();
                var filename = headers['x-filename'];
                var contentType = headers['content-type'];

                var linkElement = document.createElement('a');
                var blob = new Blob([data], {type: 'application/pdf'});
                var url = window.URL.createObjectURL(blob);

                linkElement.setAttribute('href', url);
                linkElement.setAttribute("download", $scope.pta.plano);

                var clickEvent = new MouseEvent("click", {
                    "view": window,
                    "bubbles": true,
                    "cancelable": false
                });
                linkElement.dispatchEvent(clickEvent);
            }).error(function (data) {
                console.log(data);
            });
        };

        function downloadPTFile (planoTrimestral) {
            var url = "api/files",
                bucketName = planoTrimestral.filePath,
                fileName = planoTrimestral.fileName;

            var data = {
                'bucketName': bucketName,
                'fileName': fileName
            };

            var config = {
                params: data,
                responseType: 'arraybuffer',
                headers: {'Accept': 'multipart/form-data'}
            };

            $http.get(url, config).success(function (data, status, headers) {
                headers = headers();
                var filename = headers['x-filename'];
                var contentType = headers['content-type'];

                var linkElement = document.createElement('a');
                var blob = new Blob([data], {type: 'application/pdf'});
                var url = window.URL.createObjectURL(blob);

                linkElement.setAttribute('href', url);
                linkElement.setAttribute("download", fileName);

                var clickEvent = new MouseEvent("click", {
                    "view": window,
                    "bubbles": true,
                    "cancelable": false
                });
                linkElement.dispatchEvent(clickEvent);
            }).error(function (data) {
                console.log(data);
            });
        }

        function uploadPTFile (id, file) {
            var file = file;
            var filename = file.name;
            var url = "api/files";
            var bucketName = 'cartao/portal/plano-trimestral/' + id;

            var data = new FormData();
            data.append('file', file);
            data.append('bucketName', bucketName);

            var config = {
                transformRequest: angular.identity,
                transformResponse: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            };

            $http.post(url, data, config).then(function (response) {
                Toast.sucesso("O arquivo " + filename + " foi salvo com sucesso.");

                var planoTrimestral = {
                    id: id,
                    fileName: filename,
                    filePath: bucketName,
                    fileStatus: 'E'
                }

                PlanoTrimestralUpdateSummary.update(planoTrimestral).$promise.then(function () {
                    updatePlanosTrimestrais();
                }).catch(function (error) {
                    console.log(error);
                });
            }).catch(function (error) {
                console.log(error);
                Toast.erro("Erro ao salvar o arquivo " + filename + ".");
            });

        }

        $scope.uploadFile = function () {
            if ($scope.arquivoPlano.name) {
                var file = $scope.arquivoPlano;
                var filename = $scope.arquivoPlano.name;
                var url = "api/files";
                var bucketName = 'cartao/portal/plano/' + $scope.pta.id;

                var data = new FormData();
                data.append('file', file);
                data.append('bucketName', bucketName);

                var config = {
                    transformRequest: angular.identity,
                    transformResponse: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http.post(url, data, config).then(function (response) {
                    Toast.sucesso("O arquivo " + filename + " foi salvo com sucesso.");

                    $scope.pta.plano = $scope.arquivoPlano.name;

                    SalvarPta.update($scope.pta);
                }).catch(function (error) {
                    console.log(error);
                    Toast.erro("Erro ao salvar o arquivo " + filename + ".");
                });
            }

        };

        $scope.salvar = function () {
            if (validForm()) {
                var metasFamilias = new Array();

                $scope.metasFamiliasPta.forEach(function (metas) {
                    metas.forEach(function (meta) {
                        metasFamilias.push(meta);
                    });
                });

                $scope.pta.metasFamiliasPta = metasFamilias;
                if ($scope.dataInicio != null) {
                    $scope.pta.dataInicio = new Date($scope.dataInicio.substr(2, 4) + "/" + $scope.dataInicio.substr(0, 2) + "/" + "15");
                }

                if ($scope.dataEncerramento != null) {
                    $scope.pta.dataEncerramento = new Date($scope.dataEncerramento.substr(2, 4) + "/" + $scope.dataEncerramento.substr(0, 2) + "/" + "15");
                }

                $scope.pta.contrato.empresa = $scope.empresaSelecionada;

                $scope.pta.codigo = $scope.pta.contrato.codigo + "/1";

                if ($scope.arquivoPlano && $scope.arquivoPlano.name) {
                    $scope.pta.plano = $scope.arquivoPlano.name;
                }

                if ($scope.pta.id) {
                    SalvarPta.update($scope.pta).$promise.then(function (result) {
                        Toast.sucesso("Plano de Trabalho Anual atualizado com sucesso.");
                        $scope.pta = result;
                        construirMetasFamilias($scope.pta.metasFamiliasPta);

                    }).catch(function (error) {
                        console.log(error);
                        if (error.data.messages) {
                            Toast.erro(error.data.messages[0]);
                        } else {
                            Toast.erro("Erro ao atualizar o Plano de Trabalho Anual.");
                        }
                    });
                } else {
                    SalvarPta.post($scope.pta).$promise.then(function (result) {
                        Toast.sucesso("Plano de Trabalho Anual salvo com sucesso.");
                        $scope.habilitaTecnicos = true;
                        $scope.subToolbar.title = construirTextoToolbar(result);
                        $scope.pta = result;
                        construirMetasFamilias($scope.pta.metasFamiliasPta);

                    }).catch(function (error) {
                        console.log(error);
                        if (error.data.messages) {
                            Toast.erro(error.data.messages[0]);
                        } else {
                            Toast.erro("Erro ao salvar Plano de Trabalho Anual.");
                        }
                    });
                }
            }
        };

        $scope.addPlanoTrimestral = function () {
            $scope.getMesesTrimestresPTA();

            $scope.pta.metasAtividadesPta = $scope.pta.metasAtividadesPta.filter(function (meta) {
                return meta.atividade != "" && meta.quantidadeMeta != "";
            });

            var ptLength = $scope.pta.planosTrimestrais !== null ? $scope.pta.planosTrimestrais.length : 0;

            if(ptLength > 0) {
                var confirm = $mdDialog.confirm()
                    .title('Você gostaria de copiar o plano trimestral anterior?')
                    .ok('Copiar')
                    .cancel('Não')
                    .theme('themeButtonUA');

                $mdDialog.show(confirm).then(function() {
                    var planoTrimestral = $scope.pta.planosTrimestrais[ptLength-1];

                    PlanoTrimestralCopia.copy(planoTrimestral).$promise.then(function () {
                        updatePlanosTrimestrais();
                    }).catch(function (error) {
                        console.log(error);
                    });
                }, function() {
                    abrirNovoPlanoTrimestral();
                });
            } else {
                abrirNovoPlanoTrimestral();
            }
        };

        $scope.getTiooltipPTFileText = function (planoTrimestral) {
            switch (planoTrimestral.fileStatus) {
                case 'A':
                    return 'Relatório Aprovado';
                    break;
                case 'R':
                    return 'Relatório Reprovado';
                    break;
                case 'E':
                    return 'Relatório em Avaliação';
                    break;
                default:
                    return 'Upload';
            }
        };

        $scope.uploadOrOpenPTFile = function (planoTrimestral) {
            if (!planoTrimestral.fileStatus) {
                selectFilePT(planoTrimestral.id);
            }

            if(planoTrimestral.fileStatus === 'R') {
                try {
                    HistoricoAvaliacaoRelatorio.query({id: planoTrimestral.id}).$promise.then(function (response) {
                        var historico = response[response.length-1];

                        console.log(historico);
                        var confirm = $mdDialog.confirm()
                            .title('O relatório ' + planoTrimestral.fileName + ' foi reprovado')
                            .textContent('Motivo: '+ historico.avaliacao +'. O que você gostaria de fazer?')
                            .ok('Novo Relatório')
                            .cancel('Visualizar')
                            .theme('themeButtonUA');

                        $mdDialog.show(confirm).then(function () {
                            selectFilePT(planoTrimestral.id);
                        }, function () {
                            downloadPTFile(planoTrimestral);
                        });

                    }).catch(function (error) {
                        console.log(error);
                    });


                } catch (e) {
                    console.log(e);
                }

            } else {
                downloadPTFile(planoTrimestral);
            }
        };

        function selectFilePT(id) {
            var itemId = "fileInputPT" + id;
            var fileInput = document.getElementById(itemId);
            if (fileInput) {
                fileInput.click();
            }
        }

        $scope.changePTFile = function (item) {
            var ptId = item.name;
            uploadPTFile(ptId, item.files[0]);
        };

        function abrirNovoPlanoTrimestral() {
            $mdDialog.show({
                controller: 'PlanoTrimestralController',
                templateUrl: 'scripts/components/dialogs/usuario-avancado/plano-trimestral/plano-trimestral.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                disableParentScroll: true,
                locals: {
                    params: [angular.copy($scope.pta), angular.copy($scope.selectTrimestres)]
                },
                fullscreen: true
            }).then(function () {
                updatePlanosTrimestrais();
            }, function () {
                //cancel
            });
        }

        $scope.editarPlanoTrimestral = function (planoTrimestral) {
            if ($scope.podeVisualizarPT()) {
                PlanoTrimestral.get(planoTrimestral).$promise.then(function (metasAtividadesTrimestre) {
                    $scope.getMesesTrimestresPTA();

                    $scope.pta.metasAtividadesPta = $scope.pta.metasAtividadesPta.filter(function (meta) {
                        return meta.atividade != "" && meta.quantidadeMeta != "";
                    });

                    $mdDialog.show({
                        controller: 'PlanoTrimestralController',
                        templateUrl: 'scripts/components/dialogs/usuario-avancado/plano-trimestral/plano-trimestral.html',
                        parent: angular.element(document.body),
                        clickOutsideToClose: false,
                        disableParentScroll: true,
                        locals: {
                            params: [angular.copy($scope.pta), angular.copy($scope.selectTrimestres), planoTrimestral, metasAtividadesTrimestre]
                        },
                        fullscreen: true
                    }).then(function () {
                        updatePlanosTrimestrais();
                    }, function () {
                        //cancel
                    });

                }).catch(function (error) {
                    console.log(error);
                });
            }
        };

        var updatePlanosTrimestrais = function () {
            PlanoTrimestralPorPTA.get({id: $stateParams.planoAnualId}, function (result) {
                $scope.pta.planosTrimestrais = $filter('orderBy')(result, 'trimestre', false);
                if ($scope.pta.planosTrimestrais.length >= 4) {
                    $rootScope.fabAdd = undefined;
                }
            });
        };

        $scope.excluirPlanoTrimestral = function (planoTrimestral, index) {
            var confirm = $mdDialog.confirm()
                .title('Você gostaria de excluir esse trimestre?')
                .textContent('Todas a metas cadastradas para esse trimestre também serão excluídas.')
                .ok('Excluir')
                .cancel('Cancelar')
                .theme('themeButtonUA');

            $mdDialog.show(confirm).then(function () {
                PlanoTrimestral.delete({id: planoTrimestral.id}).$promise.then(function (metasAtividadesTrimestre) {
                    $scope.pta.planosTrimestrais.splice(index, 1);
                    if ($scope.pta.planosTrimestrais.length < 4) {
                        $rootScope.fabAdd = $scope.addPlanoTrimestral;
                    }
                }).catch(function (error) {
                    Toast.erro("Erro ao excluir o plano trimestral.");

                    console.log(error);
                });
            }, function () {

            });
        };

        $scope.getMesesTrimestresPTA = function () {
            $scope.selectTrimestres = convertMesesTrimestre(contruirMesesTrimestres());
        };

        $scope.buscarComunidadePorMunicipio = function (municipio) {
            ComunidadesPorMunicipio.get({municipios: municipio.id}, function (result) {
                municipio.comunidades = result;
            });
        };

        $scope.adicionarMetasFamiliasPta = function () {
            $scope.metasFamiliasPta.push([{municipio: {id: null}, comunidade: {id: null}, quantidadeFamilias: 0}]);
        };

        $scope.excluirMetasFamiliasPta = function (index, municipio) {
            if (municipio == undefined) {
                municipio = {};
                municipio.id = null;
            }

            var confirm = $mdDialog.confirm()
                .title('Você gostaria de remover a meta do município: ' + (municipio.id != null ? $filter('capitalize')(municipio.nome) : 'Não informado'))
                .textContent('Todas as comunidade também serão removidas.')
                .ok('Excluir')
                .cancel('Cancelar')
                .theme('themeButtonUA');

            $mdDialog.show(confirm).then(function () {
                $scope.metasFamiliasPta.splice(index, 1);
            }, function () {

            });
        };

        $scope.adicionarMetaComunidade = function (index, municipio) {
            $scope.metasFamiliasPta[index].push({municipio: municipio, comunidade: {id: null}, quantidadeFamilias: 0});
        };

        $scope.excluirMetaComunidade = function (index, indexParent) {
            $scope.metasFamiliasPta[indexParent].splice(index, 1);
        };

        $scope.expansaoMetasFamiliasPta = function (id) {
            $scope.exibir[id] = !$scope.exibir[id];
        };

        $scope.novaAtividade = function () {
            if ($scope.pta.reaberto) {
                $scope.pta.metasAtividadesPta.push({atividade: "", quantidadeMeta: "", naoPlanejada: true});
            } else {
                $scope.pta.metasAtividadesPta.push({atividade: "", quantidadeMeta: "", naoPlanejada: null});
            }

            $scope.atividades.push({nome: "", unidade: "", meta: "", metasAtividadesTrimestral: []});
        };

        $scope.removerAtividade = function (item) {
            var index = $scope.pta.metasAtividadesPta.indexOf(item);

            $scope.pta.metasAtividadesPta.splice(index, 1);
        };

        Atividade.query({isEdicao: $stateParams.planoAnualId != null}, function (result) {
            $scope.atividadeList = result;
        });

        $scope.getTotalFamiliasAtendidas = function () {
            var total = 0;

            if ($scope.metasFamiliasPta) {
                $scope.metasFamiliasPta.forEach(function (metas) {
                    metas.forEach(function (meta) {
                        if (!isNaN(meta.quantidadeFamilias)) {
                            total += meta.quantidadeFamilias;
                        }
                    });
                });
            }

            return total;
        };

        $scope.getContagemFamiliasAtendidas = function (metas) {
            var total = 0;

            angular.forEach(metas, function (meta) {
                if (!isNaN(meta.quantidadeFamilias)) {
                    total += Number(meta.quantidadeFamilias);
                }
            });

            return total;
        };

        $scope.getTotalComunidadesAtendidas = function () {
            var total = 0;

            if ($scope.metasFamiliasPta) {
                $scope.metasFamiliasPta.forEach(function (metas) {
                    metas.forEach(function (meta) {
                        total++;
                    });
                });
            }

            return total;
        };

        var auxTrocaMunicipio = null;
        var foiAlertadoMudancaMunicipio = false;

        $scope.aposSelecionarMunicipio = function (municipio, index) {
            if (municipio != null && auxTrocaMunicipio != municipio.id) {
                $scope.metasFamiliasPta[index].splice(0, $scope.metasFamiliasPta[index].length);

                $scope.adicionarMetaComunidade(index, municipio);
            }
        };

        $scope.aposAbrirMunicipios = function (municipio, index) {
            if ($scope.metasFamiliasPta[index].length > 1 || $scope.metasFamiliasPta[index][0].comunidade.id != null) {
                if (!foiAlertadoMudancaMunicipio) {
                    Toast.alerta("Ao alterar o município, todas as comunidades serão removidas.")
                }

                auxTrocaMunicipio = municipio;

                foiAlertadoMudancaMunicipio = true;
            }
        };

        $scope.aposSelecionarComunidade = function (comunidadeId, index, indexParent, municipio) {
            var existeComunidade = false;

            for (var i in $scope.pta.metasFamiliasPta) {
                if ($scope.pta.metasFamiliasPta[i].comunidade != undefined && i != index && $scope.pta.metasFamiliasPta[i].comunidade.id == comunidadeId) {
                    existeComunidade = true;
                }
            }

            if (existeComunidade) {
                $scope.excluirMetaComunidade(index, indexParent);
                $scope.adicionarMetaComunidade(indexParent, municipio);

                Toast.alerta("Já existe uma meta para essa comunidade. Selecione outra comunidade.");
            }
        };

        $scope.aposSelecionarAtividade = function (item) {
            var existeAtividade = false;
            var index = $scope.pta.metasAtividadesPta.indexOf(item);

            for (var i in $scope.pta.metasAtividadesPta) {
                if ($scope.pta.metasAtividadesPta[i].atividade != undefined && i != index && $scope.pta.metasAtividadesPta[i].atividade.id == item.atividade.id) {
                    existeAtividade = true;
                }
            }

            if (existeAtividade) {
                $scope.removerAtividade(index);
                $scope.novaAtividade();

                Toast.alerta("Já existe uma meta para essa atividade. Selecione outra atividade.");
            }
        };

        $scope.temAutoridade = function (autoridades) {
            return Principal.hasAnyAuthority(autoridades);
        };

        $scope.clickTecnicos = function () {
            $rootScope.fabAdd = addTecnico;
            buildToolbar(false);
        };

        $scope.printPlanoAnual = function () {
            RelatorioPTA.gerarRelatorioPTA('/api/servicos/relatorio/pta/', $scope.pta.id);
        };

        function setDataInicio(dataInicio) {
            $scope.dataInicio = dataInicio.substr(5, 2) + dataInicio.substr(0, 4);
        }

        function construirMetasFamilias(metasFamiliasPta) {
            $scope.metasFamiliasPta = metasFamiliasPta;

            for (var meta in $scope.metasFamiliasPta) {
                if ($scope.metasFamiliasPta[meta].municipio) {
                    $scope.buscarComunidadePorMunicipio($scope.metasFamiliasPta[meta].municipio);
                }
            }

            $scope.metasFamiliasPta = $.map(groupBy($scope.metasFamiliasPta, 'municipio'), function (value, index) {
                return [value];
            });
        }

        function construirTextoToolbar(pta) {
            var textToolbar = "";

            if (pta.contrato.empresa) {
                if (pta.contrato.empresa.pessoaJuridica.nomeFantasia) {
                    textToolbar += $filter('capitalize')(pta.contrato.empresa.pessoaJuridica.nomeFantasia);
                } else {
                    textToolbar += $filter('capitalize')(pta.contrato.empresa.pessoaJuridica.pessoa.nome);
                }
            }

            if (pta.contrato.regiao) {
                textToolbar += " | " + $filter('capitalize')(pta.contrato.regiao.nome);
            }

            if (pta.dataInicio) {
                textToolbar += " | " + $scope.meses[Number(pta.dataInicio.substr(5, 2)) - 1] + "/" + pta.dataInicio.substr(0, 4);
            }

            if (pta.dataEncerramento) {
                textToolbar += " - " + $scope.meses[Number(pta.dataEncerramento.substr(5, 2)) - 1] + "/" + pta.dataEncerramento.substr(0, 4);
            }

            return textToolbar;
        }

        function validForm() {
            if (!Principal.hasAnyAuthority(['ROLE_USUARIO_UGP'])) {
                if ($scope.metasFamiliasPta.length < 1) {
                    Toast.alerta("Preencha as informações das metas do Plano de Trabalho Anual.");

                    return false;
                }

                $scope.metasFamiliasPta.forEach(function (metas) {
                    metas.forEach(function (meta) {
                        if (meta.comunidade.id == null
                            || meta.municipio.id == null
                            || meta.quantidadeFamilias <= 0) {

                            if (meta.quantidadeFamilias <= 0) {
                                Toast.alerta("A quantidade de famílias deve ser maior que zero.");
                            } else {
                                Toast.alerta("Preencha corretamente todas as informações das metas do Plano de Trabalho Anual.");
                            }

                            return false;
                        }
                    });
                });

                if ($scope.pta.metasAtividadesPta.length < 1) {
                    Toast.alerta("Preencha as informações das atividades do Plano de Trabalho Anual.");

                    return false;
                }

                for (var m in $scope.pta.metasAtividadesPta) {
                    var meta = $scope.pta.metasAtividadesPta[m];

                    if (meta.atividade) {
                        if (meta.atividade.id == null
                            || meta.quantidadeMeta <= 0) {

                            if (meta.quantidadeMeta <= 0) {
                                Toast.alerta("A quantidade de metas das atividades deve ser maior que zero.");
                            } else {
                                Toast.alerta("Preencha corretamente todas as informações das atividades do Plano de Trabalho Anual.");
                            }

                            return false;
                        }
                    }
                }
            }

            return true;
        }

        function contruirMesesTrimestres() {
            if ($scope.dataInicio) {
                var primeiroMes = Number($scope.dataInicio.substr(0, 2)) - 1;
                var trimestres = [];

                for (var trimestre = 0; trimestre < 4; trimestre++) {
                    var mesesTrimestre = [];

                    for (var mes = primeiroMes; mes < primeiroMes + 3; mes++) {
                        if (mesesTrimestre.length < 3) {
                            if (mes < 12) {
                                mesesTrimestre.push(mes);
                            } else {
                                primeiroMes = 0;
                                mes = 0;
                                mesesTrimestre.push(mes);
                            }
                        }
                    }

                    trimestres.push(mesesTrimestre);

                    if (primeiroMes != 0) {
                        primeiroMes = primeiroMes + 3;
                    } else {
                        primeiroMes = trimestres[trimestre][mesesTrimestre.length - 1] + 1;
                    }
                }

                return trimestres;
            }
        }

        function convertMesesTrimestre(arrayTrimestres) {
            if ($scope.dataInicio != null) {
                var anoInicio = Number($scope.dataInicio.substr(2, 6));
            }

            for (var t in arrayTrimestres) {
                for (var m in arrayTrimestres[t]) {
                    if (arrayTrimestres[t][m] == 0 && arrayTrimestres[t][m - 1] != undefined && arrayTrimestres[t][m - 1].mesNum == 11) {
                        arrayTrimestres[t][m] = {
                            mes: $scope.meses[arrayTrimestres[t][m]],
                            ano: anoInicio + 1,
                            mesNum: arrayTrimestres[t][m]
                        }

                        if (arrayTrimestres[t][m].mesNum == 0) {
                            anoInicio = anoInicio + 1;
                        }
                    } else {
                        arrayTrimestres[t][m] = {
                            mes: $scope.meses[arrayTrimestres[t][m]],
                            ano: anoInicio,
                            mesNum: arrayTrimestres[t][m]
                        }
                    }
                }
            }

            return arrayTrimestres;
        }

        function groupBy(array, atributo) {
            return array.reduce(function (grupos, item) {
                if (item[atributo] != undefined && item[atributo].id != undefined) {
                    var val = item[atributo].id;

                    grupos[val] = grupos[val] || [];
                    grupos[val].push(item);

                    return grupos;
                }
            }, {});
        }

        $scope.$watch('pta.contrato.id', function () {
            for (var i in $scope.contratos) {
                if ($scope.contratos[i].id == $scope.pta.contrato.id) {
                    $scope.pta.contrato = $scope.contratos[i];
                }
            }

            if ($scope.pta.contrato != null) {
                var abrangenciaId = $scope.pta.contrato.abrangencia.map(function (key) {
                    return key.id;
                });
            }

            ComunidadesPorMunicipio.get({municipios: abrangenciaId}, function (result) {
                $scope.comunidadesContrato = result;
            });
        });

        $scope.$watch('empresaSelecionada', function (selected) {
            if (selected) {
                ContratosByEmprestaService.query({id: $scope.empresaSelecionada.id}, function (result) {
                    $scope.contratos = result;
                });
            }
        });

        $scope.$watch('dataInicio', function () {
            if ($scope.dataInicio != null && $scope.dataInicio.length == 6) {
                var mes = parseInt($scope.dataInicio.substr(0, 2), 10);
                var ano = parseInt($scope.dataInicio.substr(2, 4), 10);
                var mesEncerramento = mes + 11;

                if (mesEncerramento <= 12) {
                    $scope.dataEncerramento = mesEncerramento.toString() + "/" + $scope.dataInicio.substr(2, 4);
                } else {
                    mesEncerramento = mes - 1;

                    if (mesEncerramento < 10)
                        $scope.dataEncerramento = "0" + mesEncerramento.toString() + "/" + (ano + 1).toString();
                    else {
                        $scope.dataEncerramento = mesEncerramento.toString() + "/" + (ano + 1).toString();
                    }
                }

                var dataInicioTeste = new Date($scope.dataInicio.substr(2, 4) + "/" + $scope.dataInicio.substr(0, 2) + "/" + "03");
                var dataAssinatura = new Date($scope.pta.contrato.dataAssinatura.substr(0, 4) + "/" + $scope.pta.contrato.dataAssinatura.substr(5, 2) + "/" + "02");
                var dataEncerramento = new Date($scope.pta.contrato.dataEncerramento.substr(0, 4) + "/" + $scope.pta.contrato.dataEncerramento.substr(5, 2) + "/" + "02");

                if ($scope.pta.contrato != null && dataInicioTeste <= dataAssinatura) {
                    Toast.alerta("Data de início não pode ser menor que a data de assinatura do contrato: " + $filter('date')($scope.pta.contrato.dataAssinatura, 'dd/MM/yyyy'));
                    $scope.dataInicio = null;
                    $scope.dataEncerramento = null;
                }

                if ($scope.pta.contrato != null && dataInicioTeste >= dataEncerramento) {
                    Toast.alerta("Data de início não pode ser maior que a data de encerramento do contrato: " + $filter('date')($scope.pta.contrato.dataEncerramento, 'dd/MM/yyyy'));
                    $scope.dataInicio = null;
                    $scope.dataEncerramento = null;
                }
            }
        });

        function finalizarPlano() {
            var confirm = $mdDialog.confirm()
                .title('Você tem certeza que deseja finalizar o Plano Anual?')
                .textContent('Essa operação fechará a edição do Plano e o encaminhará para avaliação')
                .ok('Finalizar')
                .cancel('Cancelar')
                .theme('themeButtonUA');

            $mdDialog.show(confirm).then(function () {
                $scope.pta.status = '2';
                SalvarPta.update($scope.pta).$promise.then(function (result) {
                    Toast.sucesso("Plano Anual finalizado com sucesso.");
                    $scope.statusFinalizado = true;
                    $scope.buildToolbar(false);
                }).catch(function (error) {
                    console.log(error);
                    Toast.erro("Erro ao finalizar Plano Anual.");
                });
            }, function () {

            });
        }

        function verificaStatusFinalizado() {
            var statusInt = parseInt($scope.pta.status);
            return (statusInt > 1)
        }

        $scope.dialogReabrir = function (index, item) {
            var confirm = $mdDialog.confirm()
                .title('Ao reabrir um plano, você poderá incluir apenas atividades não previstas. Deseja reabrir?')
                .ok('Reabrir')
                .cancel('Cancelar')
                .theme('themeButtonUA');

            $mdDialog.show(confirm).then(function () {
                reabrirPlano();
            }, function () {
            });
        };


    });
