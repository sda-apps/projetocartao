'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('pta', {
                parent: 'aterUA',
                url: '/pta',
                data: {
                    authorities: rolesAccess['PTA'].visualizar,
                    pageTitle: 'Plano de Trabalho Anual'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/pta/pta.html',
                        controller: 'PtaController'
                    }
                },
                params: {
                    planoAnualId: null
                },
                resolve: {

                }
            });
    });
