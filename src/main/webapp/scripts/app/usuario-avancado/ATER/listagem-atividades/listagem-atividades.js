'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('listagem-atividades', {
                parent: 'aterUA',
                url: '/listagem-atividades',
                data: {
                    authorities: rolesAccess['ATIVIDADES'].visualizar,
                    pageTitle: 'Administração - Atividades'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/listagem-atividades/listagem-atividades.html',
                        controller: 'ListagemAtividadesController'
                    }
                },
                resolve: {

                }
            });
    });
