'use strict';

angular.module('projetocartaoApp')
    .controller('ListagemAtividadesController', function (
        $rootScope, $scope, AtividadesFiltro, $timeout, $http, $state, $mdDialog, Atividade, Toast, Acao,
        Principal, rolesAccess) {

        $scope.mostraInput = 'atividade';
        $scope.filtros = [];
        $scope.atividade = [];
        $scope.semResultado = false;
        $scope.dto = {};
        $scope.paginacao = 10;
        $scope.start = 0;
        $scope.limit = 10;
        $scope.exibeVerMais = false;

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        $scope.podeIncluir = function () {
            return Principal.hasAnyAuthority(rolesAccess['ATIVIDADES'].incluir);
        };

        $scope.podeVisualizar = function () {
            return Principal.hasAnyAuthority(rolesAccess['ATIVIDADES'].visualizar);
        };

        $scope.podeEditar = function () {
            return Principal.hasAnyAuthority(rolesAccess['ATIVIDADES'].editar);
        };

        $scope.podeExcluir = function () {
            return Principal.hasAnyAuthority(rolesAccess['ATIVIDADES'].excluir);
        };

        if($scope.podeIncluir()) {
            $rootScope.fabAdd = function () {
                $scope.carregarAtividade(null);
            };
        }

        Acao.query({}, function (result) {
            $scope.acoes = result;
        });

        function setFormsPristine() {
            $scope.atividadeForm.$setPristine();
            $scope.acaoForm.$setPristine();
        }

        $scope.$watch('mostraInput', function (input) {
            switch (input) {
                case 'atividade':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-atividade')).focus();
                    });
                    break;
                case 'acao':
                    $timeout(function () {
                        angular.element(document.getElementById('input-filtro-acao')).focus();
                    });
                    break;
                default:
                    break;
            }
        }, true);

        function buscarAtividadePorFiltros() {
            AtividadesFiltro.query($scope.dto).$promise.then(function (result) {
                $scope.semResultado = false;
                $scope.atividade = $scope.atividade.concat(result);
                $scope.start = $scope.atividade.length;
                $scope.exibeVerMais = result.length >= $scope.paginacao;
            })
                .catch(function () {
                    $scope.semResultado = true;
                    $scope.exibeVerMais = false;
                    $scope.atividade = new Array();
                });
        }

        $scope.carregaMais = function () {
            $scope.start = $scope.limit;
            $scope.limit = $scope.limit + $scope.paginacao;

            $scope.dto = construirELimparFiltros($scope.filtros);

            $scope.dto.start = $scope.start;
            $scope.dto.limit = $scope.limit;

            buscarAtividadePorFiltros();
        };

        $scope.limparFiltros = function () {
            $scope.filtros = [];
            $scope.atividade = [];
            $scope.semResultado = false;
            $scope.exibeVerMais = false;
            $scope.start = 0;
            $scope.limit = 10;
            $scope.dto = {};

            buscarAtividadePorFiltros();
        };

        function construirELimparFiltros() {
            var filtro = {};
            $scope.filtros.forEach(function (elemento) {
                if (filtro.hasOwnProperty(elemento.tipo)) {
                    filtro[elemento.tipo].push(elemento.id);
                } else {
                    filtro[elemento.tipo] = [elemento.id];
                }
            });
            return filtro;
        }

        function validarAdicaoFiltro(filtro) {
            return filtro.id && filtro.tipo;
        }

        $scope.removerFiltroChip = function () {
            $scope.atividade = new Array();

            $scope.dto = construirELimparFiltros();

            if ($scope.filtros.length >= 1) {
                buscarAtividadePorFiltros();
            } else {
                $scope.limparFiltros();
            }
        };

        $scope.adicioarFiltroChip = function (filtro) {
            if (validarAdicaoFiltro(filtro)) {
                $scope.filtros.push(filtro);
                $scope.atividade = new Array();
                $scope.dto = construirELimparFiltros();

                setFormsPristine();

                buscarAtividadePorFiltros();
                switch (filtro.tipo) {
                    case "atividade":
                        $scope.atividadeSelecionada = undefined;
                        break;
                    case "acao":
                        $scope.acaoSelecionada = undefined;
                        break;
                }
            }
        };

        $scope.buscarAutoCompletePorFiltro = function (searchText, url) {
            var data = $http({
                method: 'GET',
                url: url + searchText
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                return response.data;
            });

            return data;
        };

        buscarAtividadePorFiltros();

        $scope.deletarAtividade = function (index, item) {
            var confirm = $mdDialog.confirm()
                .title('Você gostaria de excluir a atividade ' + item.nome + '?')
                .ok('Excluir')
                .cancel('Cancelar')
                .theme('themeButtonUA');

            $mdDialog.show(confirm).then(function () {
                Atividade.delete({id: item.id}).$promise.then(function (result) {
                    Toast.sucesso("Atividade excluída com sucesso.");
                    $scope.atividade.splice(index, 1);
                }).catch(function (error) {
                    console.log(error);
                    Toast.erro("Erro ao excluir atividade.");
                });
            }, function () {

            });
        };

        $scope.carregarAtividade = function (atividade) {
            $mdDialog.show({
                controller: 'CadastrarAtividadeController',
                templateUrl: 'scripts/components/dialogs/atividades/cadastrar-atividade.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                disableParentScroll: true,
                locals: {
                    atividadeToEdit: angular.copy(atividade)
                },
                fullscreen: true
            })
                .then(function (answer) {

                }, function () {
                    $scope.atividade = [];
                    buscarAtividadePorFiltros();

                });
        };


    });
