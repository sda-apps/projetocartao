'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('aterUA', {
                abstract: true,
                parent: 'usuario-avancado'
            });
    });
