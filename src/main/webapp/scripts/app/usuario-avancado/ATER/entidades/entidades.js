    'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('entidades', {
                parent: 'aterUA',
                url: '/entidades',
                data: {
                    authorities: rolesAccess['ENTIDADE'].visualizar,
                    pageTitle: 'Entidades'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/entidades/entidades.html',
                        controller: 'EntidadesController'
                    }
                },
                params: {
                    entidadeId: null
                },
                resolve: {

                }
            });
    });
