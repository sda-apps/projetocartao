'use strict';

angular.module('projetocartaoApp')
    .controller('EntidadesController', function ($scope, $rootScope, $http, $state, $stateParams, Entidade, Cargo, Principal, rolesAccess,
                                                 telefoneConstants, EstadoCivil, Toast, tipoSociedadeEnum, $mdDialog, ContratoService, ContratosByEmprestaService, BuscaPlanoAnual) {
        $scope.sociedades = [];
        $scope.telefones = [];
        $scope.empresa = {};
        $scope.telefoneTipo = Object.keys(telefoneConstants).map(function (key) {
            return telefoneConstants[key];
        });
        $scope.telefoneTipo.push({descricao: "", tipo: ""});
        $scope.tipoSociedade = tipoSociedadeEnum;
        $scope.desativarContratos = true;
        $scope.saveCancelButtons = {
            cancelButton: function () {
                $state.go('listagem-entidades');
            },
            saveButton: function () {
                if ($scope.entidadesForm.$valid) {
                    $scope.salvar();
                } else {
                    $scope.entidadesForm.$submitted = true;
                }
            }
        };

        $scope.subToolbar = {
            title: "Nova Entidade",
            backButton: function () {
                $state.go('listagem-entidades');
            }
        };

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        $scope.buildToolbar = function (modeSave) {
            if (modeSave) {
                $scope.subToolbar = Object.assign($scope.subToolbar, $scope.saveCancelButtons);
            } else {
                $scope.subToolbar.cancelButton = undefined;
                $scope.subToolbar.saveButton = undefined;
            }
        };

        $scope.podeEditar = function () {
            return Principal.hasAnyAuthority(rolesAccess['ENTIDADE'].editar);
        };

        $scope.podeIncluirContrato = function () {
            return Principal.hasAnyAuthority(rolesAccess['CONTRATO'].incluir);
        };

        $scope.podeEditarContrato = function () {
            return Principal.hasAnyAuthority(rolesAccess['CONTRATO'].editar);
        };

        $scope.podeExcluirContrato = function () {
            return Principal.hasAnyAuthority(rolesAccess['CONTRATO'].excluir);
        };

        $rootScope.subToolbar = Object.assign($scope.subToolbar, $scope.saveCancelButtons);

        EstadoCivil.query({}, function (result) {
            $scope.estadoCivil = result;
        });

        Cargo.query({}, function (result) {
            $scope.cargo = result;
        });

        $scope.novoTelefone = function () {
            if (!$scope.empresa.pessoaJuridica.pessoa.telefones) {
                $scope.empresa.pessoaJuridica.pessoa.telefones = [];
            }

            $scope.empresa.pessoaJuridica.pessoa.telefones.push({numero: ""});
        };

        $scope.removerTelefone = function (index) {
            $scope.empresa.pessoaJuridica.pessoa.telefones.splice(index, 1);
        };

        $scope.buscarAutoCompletePorFiltro = function (searchText, url, filtro) {
            var search;

            if (filtro == null) {
                search = url + searchText;
            } else {
                search = url + filtro.id + "/" + searchText;
            }
            return $http.get(search).then(function (retorno) {
                return retorno.data;
            });
        };

        $scope.salvar = function () {
            var valid = isValid();
            if (!valid.status) {
                Toast.alerta("Preencha corretamente todos os campos: " + valid.mensagem);
            } else {
                $scope.empresa.isProjetoPauloFreire = true;

                if ($scope.empresa.id) {
                    Entidade.update($scope.empresa).$promise.then(function (result) {
                        Toast.sucesso("Entidade " + $scope.empresa.pessoaJuridica.pessoa.nome + " parceira atualizada com sucesso.");
                        if ($scope.empresa.pessoaJuridica.nomeFantasia != null) {
                            $scope.subToolbar.title = $scope.empresa.pessoaJuridica.nomeFantasia;
                        }


                    }).catch(function (error) {
                        console.log(error);
                        Toast.erro("Erro ao atualizar entidade parceira.");
                    });

                } else {
                    Entidade.save($scope.empresa).$promise.then(function (result) {
                        Toast.sucesso("Entidade " + $scope.empresa.pessoaJuridica.pessoa.nome + " parceira salva com sucesso.");
                        $scope.empresa = result;
                        $stateParams.entidadeId = result.id;
                        $scope.desativarContratos = false;
                        if ($scope.empresa.pessoaJuridica.nomeFantasia != null) {
                            $scope.subToolbar.title = $scope.empresa.pessoaJuridica.nomeFantasia;
                        }
                    }).catch(function (error) {
                        console.log(error);
                        if (error.status == 409) {
                            Toast.erro("CNPJ da Entidade já cadastrado.");
                        } else {
                            Toast.erro("Erro ao salvar entidade parceira.");
                        }
                    });
                }
            }
        };

        if ($stateParams.entidadeId) {
            getEmpresa();

            $scope.desativarContratos = false;
        }

        function getEmpresa() {
            Entidade.get({id: $stateParams.entidadeId}, function (result) {
                $scope.empresa = result;

                ContratosByEmprestaService.query({id: result.id}, function (result) {
                    $scope.empresa.contrato = result;
                });
                if ($scope.empresa.pessoaJuridica.nomeFantasia != null) {
                    $scope.subToolbar.title = $scope.empresa.pessoaJuridica.nomeFantasia;
                } else {
                    $scope.subToolbar.title = "Edição Entidade";
                }
            });
        }

        function isValid() {
            var telefones = $scope.empresa.pessoaJuridica.pessoa.telefones;

            var valid = {
                mensagem: "",
                status: true
            };

            for (var fone in telefones) {
                if (telefones[fone].numero) {
                    if (telefones[fone].tipo == null || telefones[fone].tipo == '' || telefones[fone].numero == null || telefones[fone].numero == '') {
                        valid.mensagem = "Telefone";
                        valid.status = false;
                    }
                }
            }

            return valid;
        }

        $scope.getMunicipiosAbrangencia = function (contrato) {
            var str = "";
            var territorios = {};

            if (contrato.abrangencia) {
                for (var idxAbra in contrato.abrangencia) {
                    var abrangencia = contrato.abrangencia[idxAbra];

                    if (abrangencia.territorio) {
                        if (!territorios.hasOwnProperty(abrangencia.territorio.nome)) {
                            territorios[abrangencia.territorio.nome] = [];
                        }
                        territorios[abrangencia.territorio.nome].push(abrangencia.nome);
                    }
                }
            }
            if (!angular.equals(territorios, {})) {
                for (var prop in territorios) {
                    if (str != "") {
                        str += ", ";
                    }
                    str += prop + "(";
                    for (var idx in territorios[prop]) {
                        if (idx != 0) {
                            str += ",";
                        }
                        str += " " + territorios[prop][idx];
                    }
                    str += " )";
                }
            }
            return str;
        };

        $scope.showContratoDialog = function (contrato) {
            $mdDialog.show({
                controller: 'ContratoController',
                templateUrl: 'scripts/components/dialogs/contrato/contrato.html',
                parent: angular.element(document.body),
                locals: {
                    contrato: angular.copy(contrato)
                },
                resolve: {
                    entidade: function () {
                        $scope.empresa.id = $stateParams.entidadeId;
                        return $scope.empresa;
                    }
                },
                clickOutsideToClose: true,
                disableParentScroll: true,
                fullscreen: false
            })
                .then(function () {
                    getEmpresa();
                }, function () {

                });
        };

        $scope.deleteContrato = function (contrato) {
            var dto = {};
            dto["contrato"] = new Array();
            dto["contrato"].push(contrato.id);

            BuscaPlanoAnual.query(dto).$promise.then(function (result) {
                Toast.alerta("Não é possível excluir o contrato, há Plano de Trabalho Anual vinculado.");
            }).catch(function (error) {
                if (error.status == 404) {
                    var confirm = $mdDialog.confirm()
                        .title('Contrato ' + contrato.titulo)
                        .theme('themeButtonUA')
                        .textContent('Deseja excluir este contrato?')
                        .ok('Excluir')
                        .cancel('Cancelar');

                    $mdDialog.show(confirm).then(function () {
                        ContratoService.delete({id: contrato.id}).$promise.then(function (result) {
                            Toast.sucesso("Contrato apagado com sucesso.");
                            getEmpresa();
                        }).catch(function (error) {
                            console.log(error);
                            Toast.erro("Erro ao apagar contrato.");
                        });
                    }, function () {

                    });
                } else {
                    Toast.erro("Erro ao apagar contrato.");
                }
            });
        };

        $scope.$watch('foto', function () {
            if ($scope.foto != null) {
                $scope.empresa.pessoaJuridica.pessoa.foto = $scope.foto;
            }
        });

        $scope.showDialogCrop = function () {
            $mdDialog.show({
                templateUrl: 'scripts/components/dialogs/crop/crop.html',
                backdrop: false,
                escapeToClose: false,
                scope: $scope,
                preserveScope: true,
                controller: function ($rootScope, $scope, $timeout, $mdDialog, Toast, $mdMedia) {
                    $scope.isGtSm = $mdMedia('gt-xs');

                    // upload on file
                    $scope.upload = function (dataUrl, file) {
                        if (file) {
                            if (!file.size > 1048576) {
                                Toast.erro('Tamanho máximo da imagem 1MB');
                            } else {
                                $scope.foto = dataUrl;
                                $mdDialog.hide();
                            }
                            ;
                        }
                    };
                    $scope.cropDialoghide = function () {
                        $mdDialog.hide();
                    };

                    $scope.validarArquivo = function (file) {
                        if (file) {
                            if (!file.type.startsWith("image")) {
                                Toast.erro('Formato de arquivo inválido');
                            }
                        }
                    }
                }
            });
        };
    });
