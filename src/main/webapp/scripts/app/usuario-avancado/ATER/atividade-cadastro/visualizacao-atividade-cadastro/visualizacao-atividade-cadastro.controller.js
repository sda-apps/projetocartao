'use strict';

angular.module('projetocartaoApp')
    .controller('VisualizacaoAtividadeCadastroController', function ($scope, $rootScope, $state, $timeout, $stateParams, $window, AtividadeCadastroResumo,
                                                                     $mdDialog, rolesAccess, Principal, $filter, atividadeCadastroStatus) {
        $scope.atividadeCadastro = {};
        $scope.historico = {};
        $scope.atividadeCadastroStatus = atividadeCadastroStatus;

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        $scope.podeVisualizar = function () {
            return Principal.hasAnyAuthority(rolesAccess['ATIVIDADE_CADASTRO'].visualizar);
        };

        $scope.podeAvaliar = function () {
            return Principal.hasAnyAuthority(rolesAccess['ATIVIDADE_CADASTRO'].avaliar) && ($scope.atividadeCadastro.status == 'T' || $scope.atividadeCadastro.status == 'E')
        };

        if (!$window.atividadeCadastroId) {
            $state.go('atividade-cadastro');
        } else {
            AtividadeCadastroResumo.get({id: $window.atividadeCadastroId}, function (result) {
                $scope.atividadeCadastro = result;
                $window.document.title = $scope.atividadeCadastro.informacaoInicial.agricultor.pessoaFisica.pessoa.nome;

                $scope.historico = $scope.atividadeCadastro.historico.filter(function (item, index, array) {
                    return item.motivo == 'A' || item.motivo == 'R';
                });

                if ($scope.podeAvaliar()) {
                    $rootScope.subToolbar["evaluateButton"] = $scope.avaliarCadastro;
                }

                if ($scope.podeVisualizar()) {
                    $rootScope.subToolbar["viewButton"] = $scope.verCadastro
                }

                $scope.historico = $filter('orderBy')($scope.historico, 'criacao', true);

                calcularTempoPreenchimento();
                criarMapaGoogle();
            });
        }

        function calcularTempoPreenchimento() {
            var tempoPreenchimentoTotal = 0;
            if ($scope.atividadeCadastro.acessoPoliticasPublicas && $scope.atividadeCadastro.acessoPoliticasPublicas.tempoPreenchimento) {
                tempoPreenchimentoTotal += $scope.atividadeCadastro.acessoPoliticasPublicas.tempoPreenchimento;
            }
            if ($scope.atividadeCadastro.informacaoInicial && $scope.atividadeCadastro.informacaoInicial.tempoPreenchimento) {
                tempoPreenchimentoTotal += $scope.atividadeCadastro.informacaoInicial.tempoPreenchimento;
            }
            if ($scope.atividadeCadastro.capacitacao && $scope.atividadeCadastro.capacitacao.tempoPreenchimento) {
                tempoPreenchimentoTotal += $scope.atividadeCadastro.capacitacao.tempoPreenchimento;
            }
            if ($scope.atividadeCadastro.producao && $scope.atividadeCadastro.producao.tempoPreenchimento) {
                tempoPreenchimentoTotal += $scope.atividadeCadastro.producao.tempoPreenchimento;
            }

            $scope.minutosPreenchimento = Math.floor(tempoPreenchimentoTotal / 60);
            $scope.segundosPreenchimento = tempoPreenchimentoTotal - $scope.minutosPreenchimento * 60;

        }

        $scope.avaliarCadastro = function () {
            $mdDialog.show({
                controller: 'AvaliarCadastroController',
                templateUrl: 'scripts/components/dialogs/usuario-avancado/avaliar-cadastro/avaliar-cadastro.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                disableParentScroll: true,
                locals: {
                    params: $scope.atividadeCadastro
                },
                fullscreen: true
            }).then(function (answer) {
                if (answer.motivo != 'E') {
                    $scope.historico.unshift(answer);
                }

                $scope.atividadeCadastro.historico.push(answer);

            }, function () {
                //cancel
            });

        };

        $scope.verCadastro = function () {
            $mdDialog.show({
                controller: 'DetalhesAtividadeCadastroController',
                templateUrl: 'scripts/components/dialogs/usuario-avancado/atividade-cadastro/detalhes-atividade-cadastro.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                disableParentScroll: true,
                locals: {
                    params: $scope.atividadeCadastro
                },
                fullscreen: true
            })
                .then(function (answer) {
                    //return
                }, function () {
                    //cancel
                });

        };

        function marcarMapa(coordenada) {
            var pinIcon = {
                url: 'assets/images/pin.svg',
                size: new google.maps.Size(29, 40)
            };

            var marker = new google.maps.Marker({
                position: {
                    lat: Number((coordenada.latitude)),
                    lng: Number((coordenada.longitude))
                },
                map: $scope.mapGoogle,
                icon: pinIcon
            });

        }

        function criarMapaGoogle() {
            $scope.mapGoogle = new google.maps.Map(document.getElementById('map-visualizacao-cadastro'), {
                mapTypeId: google.maps.MapTypeId.HYBRID,
                scrollwheel: true,
                zoom: 6
            });

            if ($scope.atividadeCadastro.informacaoInicial.agricultor.coordenada) {
                marcarMapa($scope.atividadeCadastro.informacaoInicial.agricultor.coordenada);
                atualizaImagensGoogleMapsComCoordenada($scope.atividadeCadastro.informacaoInicial.agricultor.coordenada, 16);
            } else if ($scope.atividadeCadastro.informacaoInicial.agricultor.distrito && $scope.atividadeCadastro.informacaoInicial.agricultor.distrito.coordenada) {
                atualizaImagensGoogleMapsComCoordenada($scope.atividadeCadastro.informacaoInicial.agricultor.distrito.coordenada, 14);
            } else if ($scope.atividadeCadastro.informacaoInicial.agricultor.pessoaFisica.pessoa.municipio && $scope.atividadeCadastro.informacaoInicial.agricultor.pessoaFisica.pessoa.municipio.coordenada) {
                atualizaImagensGoogleMapsComCoordenada($scope.atividadeCadastro.informacaoInicial.agricultor.pessoaFisica.pessoa.municipio.coordenada, 10);
            } else {
                atualizaImagensGoogleMaps();
            }
        }

        function atualizaImagensGoogleMapsComCoordenada(coordenada, zoom) {
            $timeout(function () {
                google.maps.event.trigger($scope.mapGoogle, 'resize');
                $scope.mapGoogle.setZoom(zoom);
                $scope.mapGoogle.setCenter(new google.maps.LatLng({
                    lat: Number((coordenada.latitude)),
                    lng: Number((coordenada.longitude))
                }));
            }, 100);
        }

        function atualizaImagensGoogleMaps() {
            $timeout(function () {
                google.maps.event.trigger($scope.mapGoogle, 'resize');
            }, 400);
        }

        $rootScope.subToolbar = {
            title: "Avaliação do Cadastro",
            backButton: function () {
                $state.go('atividade-cadastro');
            },
            cancelButton: function () {
                $state.go('atividade-cadastro');
            }
        }
    });
