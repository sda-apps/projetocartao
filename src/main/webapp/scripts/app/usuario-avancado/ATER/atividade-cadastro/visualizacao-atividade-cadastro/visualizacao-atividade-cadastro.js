'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('visualizacao-atividade-cadastro', {
                parent: 'aterUA',
                url: '/atividade-cadastro/visualizacao-atividade-cadastro',
                data: {
                    authorities: rolesAccess['ATIVIDADE_CADASTRO'].visualizar,
                    pageTitle: 'ATER - Cadastros de Agricultores'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/atividade-cadastro/visualizacao-atividade-cadastro/visualizacao-atividade-cadastro.html',
                        controller: 'VisualizacaoAtividadeCadastroController'
                    }
                },
                params: {
                    atividadeCadastroId: null
                },
                resolve: {

                }
            });
    });
