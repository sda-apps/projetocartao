'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('atividade-cadastro', {
                parent: 'aterUA',
                url: '/gerenciamento-cadastros',
                data: {
                    authorities: rolesAccess['ATIVIDADE_CADASTRO'].buscar,
                    pageTitle: 'ATER - Cadastros de Agricultores'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/atividade-cadastro/atividade-cadastro.html',
                        controller: 'AtividadeCadastroController'
                    }
                },
                resolve: {

                }
            });
    });
