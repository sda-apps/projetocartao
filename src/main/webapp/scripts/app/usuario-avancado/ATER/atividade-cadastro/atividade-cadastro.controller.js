'use strict';

angular.module('projetocartaoApp')
    .controller('AtividadeCadastroController', function ($rootScope, $scope, $http, $state, $timeout, $window, BuscaAtividadeCadastro,
                                                         Toast, atividadeCadastroStatus, Principal, rolesAccess) {
        $scope.mostraInput  = "agricultor";
        $scope.statusSelect = [
            { id: 'T', nome: 'Não Avaliados' },
            { id: 'E', nome: 'Em Avaliação' },
            { id: 'A', nome: 'Aceitos' },
            { id: 'R', nome: 'Recusados' },
            { id: 'V', nome: 'Revisados' }
        ];
        $scope.filtros      = [];
        $scope.cadastros    = [];
        $scope.semResultado = false;
        $scope.dto          = {};
        $scope.paginacao    = 10;
        $scope.start        = 0;
        $scope.limit        = 10;
        $scope.exibeVerMais  = false;
        $scope.atividadeCadastroStatus = atividadeCadastroStatus;

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        $scope.podeVisualizar = function () {
            return Principal.hasAnyAuthority(rolesAccess['ATIVIDADE_CADASTRO'].visualizar);
        };

        $scope.$watch('mostraInput', function(input){

        	switch (input) {
                case 'agricultor':
                    $timeout(function (){angular.element(document.getElementById('input-filtro-agricultor')).focus();});
                    break;

    			case 'municipio':
    				$timeout(function (){angular.element(document.getElementById('input-filtro-municipio')).focus();});
    				break;

    			case 'comunidade':
    				$timeout(function (){angular.element(document.getElementById('input-filtro-comunidade')).focus();});
    				break;

    			case 'tecnico':
    				$timeout(function (){angular.element(document.getElementById('input-filtro-tecnico')).focus();});
    				break;

    			default:
    				break;
			}

        }, true);

        $scope.buscarAutoCompletePorFiltro = function(searchText, url) {
            return $http({
                method: 'GET',
                url: url + searchText
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                return response.data;
            });

        };

        $scope.adicionarFiltroChip = function(filtro) {
            if(validarAdicaoFiltro(filtro)) {
                $scope.filtros.push(filtro);

                $scope.cadastros = [];

                $scope.dto = construirELimparFiltros();

                buscarAtividadesCadastroPorFiltros();
            }
        };

        $scope.removerFiltroChip = function() {
            $scope.cadastros = [];

            $scope.dto = construirELimparFiltros();

            if($scope.filtros.length >= 1) {
                buscarAtividadesCadastroPorFiltros();
            } else {
                $scope.limparFiltros();
            }
        };

        function validarAdicaoFiltro(filtro) {
            if(!filtro.id) {
                return false;
            }

            if(filtro.tipo === 'status' || filtro.tipo === 'tecnico' || filtro.tipo === 'agricultor') {
                var filtroMesmoTipo = $scope.filtros.some(function (elemento) {
                    return elemento.tipo === filtro.tipo;
                });

                if(filtroMesmoTipo) {
                    Toast.alerta('Filtro já adicionado! Não pode haver multiplus filtros para Agricultor, Status e Nome do Técnico');

                    return false;
                }
            }

            var existeFiltro = $scope.filtros.some(function (elemento) {
                return (elemento.nome === filtro.nome) && (elemento.tipo === filtro.tipo);
            });

            if(existeFiltro) {
                Toast.alerta('Filtro já adicionado');

                return false;
            }

            return true;
        }

        $scope.limparFiltros = function () {
            $scope.filtros = [];
            $scope.cadastros = [];
            $scope.semResultado = false;
            $scope.exibeVerMais = false;
            $scope.start        = 0;
            $scope.limit        = 10;
            $scope.dto          = {};

            buscarAtividadesCadastroPorFiltros();
        };

        function construirELimparFiltros() {
            var filtro = {};

            $scope.filtros.forEach(function(elemento) {
                if(elemento.tipo === 'municipios' || elemento.tipo === 'comunidades') {
                    if(filtro[elemento.tipo]) {
                        filtro[elemento.tipo].push(elemento.id);
                    } else {
                        filtro[elemento.tipo] = [];
                        filtro[elemento.tipo].push(elemento.id);
                    }
                } else {
                    if(elemento.tipo === 'status' && elemento.id === atividadeCadastroStatus.V.id) {
                        filtro.status = 'T';
                        filtro.recusado = true;
                    } else {
                        filtro[elemento.tipo] = elemento.id;
                    }
                }
            });

            return filtro;
        }

        $scope.carregaMais = function() {
            $scope.start = $scope.limit;
            $scope.limit = $scope.limit + $scope.paginacao;

            $scope.dto = construirELimparFiltros($scope.filtros);

            $scope.dto.start = $scope.start;
            $scope.dto.limit = $scope.limit;

            buscarAtividadesCadastroPorFiltros();
        };

        function buscarAtividadesCadastroPorFiltros() {
            BuscaAtividadeCadastro.query($scope.dto).$promise.then( function (result) {
                $scope.semResultado = false;

                $scope.cadastros = $scope.cadastros.concat(result);

                $scope.start = $scope.cadastros.length;

                $scope.exibeVerMais = result.length >= $scope.paginacao;
            })
            .catch(function(){
                $scope.semResultado = true;
                $scope.exibeVerMais = false;

                if($scope.filtros.length > 0) {
                    $scope.cadastros = [];
                }

            });
        }

        $scope.carregarAtividadeCadastro = function(cadastro) {
            if($scope.podeVisualizar()) {
                var newWindow = $window.open($state.href('visualizacao-atividade-cadastro'), '_blank');
                newWindow.atividadeCadastroId = cadastro.id;
                $mdDialog.cancel();
            }
        };

        buscarAtividadesCadastroPorFiltros();
    });
