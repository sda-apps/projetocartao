'use strict';

angular.module('projetocartaoApp')
    .controller('ListagemTemasController', function ($rootScope, $scope, TemaFiltro, $timeout, $http, $state, $mdDialog, Toast, Tema, EixoTematicoTodos, rolesAccess, Principal) {

        $scope.mostraInput = 'tema';
        $scope.filtros = [];
        $scope.tema = [];
        $scope.semResultado = false;
        $scope.dto = {};
        $scope.paginacao = 10;
        $scope.start = 0;
        $scope.limit = 10;
        $scope.exibeVerMais = false;

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        EixoTematicoTodos.query({}, function (result) {
            $scope.eixos = result;
        });

        $scope.podeIncluir = function () {
            return Principal.hasAnyAuthority(rolesAccess['TEMA'].incluir);
        };

        $scope.podeVisualizar = function () {
            return Principal.hasAnyAuthority(rolesAccess['TEMA'].visualizar);
        };

        $scope.podeEditar = function () {
            return Principal.hasAnyAuthority(rolesAccess['TEMA'].editar);
        };

        $scope.podeExcluir = function () {
            return Principal.hasAnyAuthority(rolesAccess['TEMA'].excluir);
        };

        if ($scope.podeIncluir()) {
            $rootScope.fabAdd = function () {
                $scope.carregarTema(null);
            };
        }

        function setFormsPristine() {
            $scope.temaForm.$setPristine();
        }

        function buscarTemaPorFiltros() {
            TemaFiltro.query($scope.dto).$promise.then(function (result) {
                $scope.semResultado = false;
                $scope.tema = $scope.tema.concat(result);
                $scope.start = $scope.tema.length;
                $scope.exibeVerMais = result.length >= $scope.paginacao;
            })
                .catch(function () {
                    $scope.semResultado = true;
                    $scope.exibeVerMais = false;

                    if ($scope.filtros.length > 0) {
                        $scope.tema = [];
                    }
                });
        }

        $scope.carregaMais = function () {
            $scope.start = $scope.limit;
            $scope.limit = $scope.limit + $scope.paginacao;

            $scope.dto = construirELimparFiltros($scope.filtros);

            $scope.dto.start = $scope.start;
            $scope.dto.limit = $scope.limit;

            buscarTemaPorFiltros();
        };

        $scope.limparFiltros = function () {
            $scope.filtros = [];
            $scope.tema = [];
            $scope.semResultado = false;
            $scope.exibeVerMais = false;
            $scope.start = 0;
            $scope.limit = 10;
            $scope.dto = {};

            buscarTemaPorFiltros();
        };

        function construirELimparFiltros() {
            var filtro = {};
            $scope.filtros.forEach(function (elemento) {
                if (filtro.hasOwnProperty(elemento.tipo)) {
                    filtro[elemento.tipo].push(elemento.id);
                } else {
                    filtro[elemento.tipo] = [elemento.id];
                }
            });
            return filtro;
        }

        function validarAdicaoFiltro(filtro) {
            return filtro.id && filtro.tipo;
        }

        $scope.removerFiltroChip = function () {
            $scope.tema = [];

            $scope.dto = construirELimparFiltros();

            if ($scope.filtros.length >= 1) {
                buscarTemaPorFiltros();
            } else {
                $scope.limparFiltros();
            }
        };

        $scope.adicioarFiltroChip = function (filtro) {
            if (validarAdicaoFiltro(filtro)) {
                $scope.filtros.push(filtro);
                $scope.tema = [];
                $scope.dto = construirELimparFiltros();

                setFormsPristine();

                buscarTemaPorFiltros();
                switch (filtro.tipo) {
                    case "tema":
                        $scope.eixoSelecionado = undefined;
                        break;
                }
            }
        };


        buscarTemaPorFiltros();

        $scope.deletarEixo = function (index, item) {
            var confirm = $mdDialog.confirm()
                .title('Você gostaria de excluir o tema ' + item.nome + '?')
                .ok('Excluir')
                .cancel('Cancelar')
                .theme('themeButtonUA');

            $mdDialog.show(confirm).then(function () {
                Tema.delete({id: item.id}).$promise.then(function (result) {
                    Toast.sucesso("Tema excluído com sucesso.");
                    $scope.tema.splice(index, 1);
                }).catch(function (error) {
                    console.log(error);
                    Toast.erro("Erro ao excluir tema.");
                });
            }, function () {

            });
        };

        $scope.carregarTema = function (tema) {
            if ($scope.podeVisualizar()) {
                $mdDialog.show({
                    controller: 'CadastrarTemaController',
                    templateUrl: 'scripts/components/dialogs/temas/cadastrar-tema.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    disableParentScroll: true,
                    locals: {
                        temaToEdit: angular.copy(tema)
                    },
                    fullscreen: true
                }).then(function (answer) {

                }, function () {
                    $scope.limparFiltros()
                });
            }
        };
    });
