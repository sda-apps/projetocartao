'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('listagem-temas', {
                parent: 'aterUA',
                url: '/listagem-temas',
                data: {
                    authorities: rolesAccess['TEMA'].visualizar,
                    pageTitle: 'Administração - Tema'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/ATER/listagem-temas/listagem-temas.html',
                        controller: 'ListagemTemasController'
                    }
                },
                resolve: {

                }
            });
    });
