'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('usuario-avancado', {
                abstract: true,
                parent: 'site'
            });
    });
