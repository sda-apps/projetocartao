'use strict';

angular.module('projetocartaoApp')
    .controller('UsuarioAvancadoController', function ($scope, $state, $window, $location, $rootScope, Principal, $filter, AgricultorBusca, ElementoCoordenadas, ElementoAgricultor, AgricultorAreaGeorreferenciada, $timeout, $mdDialog, $mdMedia, leafletData, leafletMarkerEvents, identificadorRoles, $compile, Agricultor, Toast, AgricultorBuscaTotal, Pessoa, AgricultorJSON, $http) {
        $scope.showMap = false;
        $scope.mostraBotao = false;
        $scope.semResultado = false;
        $scope.filtros = [];
        $scope.agricultores = [];
        $scope.markerGoogle = [];
        $scope.poligonosGoogle = [];
        $scope.filtro = {};
        $scope.qtdFiltro = 0;
        $scope.start = 0;
        $scope.limit = 10;
        $scope.paginacao = 10;
        $scope.limiteBusca = 10;
        $scope.mostrando = 0;
        $scope.tamanhoChips = 0;
        $scope.mostraInput = "nome";
        $scope.mostraMapa = "leaflet";
        $scope.markerCluster = null;
        $scope.ordenar = false;
        $scope.ordem = "";
        $scope.dispMovel = $mdMedia('xs');
        $scope.verificaTutorial = false;

        $scope.$watch('mostraInput', function (input) {

            switch (input) {
                case 'nome':
                    $timeout(function () {
                        angular.element(document.getElementById('buscarNome')).focus();
                    });
                    break;

                case 'cpf':
                    $timeout(function () {
                        angular.element(document.getElementById('buscarCpf')).focus();
                    });
                    break;

                case 'territorio':
                    $timeout(function () {
                        angular.element(document.getElementById('regiao_value')).focus();
                    });
                    break;

                case 'municipio':
                    $timeout(function () {
                        angular.element(document.getElementById('municipio_value')).focus();
                    });
                    break;

                case 'distrito':
                    $timeout(function () {
                        angular.element(document.getElementById('distrito_value')).focus();
                    });
                    break;

                case 'projeto':
                    $timeout(function () {
                        angular.element(document.getElementById('projeto_value')).focus();
                    });
                    break;

                case 'ater':
                    $timeout(function () {
                        angular.element(document.getElementById('ater_value')).focus();
                    });
                    break;

                default:
                    break;
            }

        }, true);

        angular.extend($scope, {
            center: {
                lat: -5.00339434502215,
                lng: -38.8421630859375,
                zoom: 6
            },
            markers: {},
            paths: {},
            events: {
                markers: {
                    enable: ['click']
                }
            },
            layers: {
                baselayers: {
                    osm: {
                        name: 'OpenStreetMap',
                        type: 'xyz',
                        url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
                    },
                    googleTerrain: {
                        name: 'Google Mapa',
                        layerType: 'TERRAIN',
                        type: 'google'
                    },
                    googleHybrid: {
                        name: 'Google Satélite',
                        layerType: 'HYBRID',
                        type: 'google'
                    }
                },
                overlays: {
                    pins: {
                        name: "Pontos",
                        type: "markercluster",
                        visible: true
                    },
                    poligonos: {
                        name: 'Polígonos',
                        type: 'group',
                        visible: true
                    }
                }
            }
        });

        Principal.identity().then(function (account) {
            $rootScope.account = account;
            $rootScope.isAuthenticated = Principal.isAuthenticated;
        });

        var isPrimeiraVez = true;
        $scope.$on('$viewContentLoaded', function () {
            if (isPrimeiraVez) {
                isPrimeiraVez = !isPrimeiraVez;
                if (!$rootScope.isAgricultor && $rootScope.account.primeiroAcesso) {
                    mostraDialogDadosUsuarioAvancado();
                }
            }
        });

        function mostraDialogDadosUsuarioAvancado() {
            $mdDialog.show({
                controller: 'CadastrarUserCompletoController',
                templateUrl: 'scripts/components/dialogs/usuario-avancado/cadastrar-user-completo/cadastrar-user-completo.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                disableParentScroll: true,
                locals: {
                    userToEdit: $rootScope.account,
                    canEditPerfil: false
                },
                fullscreen: true
            })
                .then(function (answer) {
                    $scope.introStart();
                }, function () {

                });
        }

        /*   $scope.addNome = function() {
         if($scope.buscarNome) {
         var nome = {
         valor: $scope.buscarNome,
         nome: $scope.buscarNome,
         tipo: 'Nome',
         chave: 'nome'
         };
         $scope.salvaTamanhoFiltroAntigo();
         $scope.filtros.push(nome);
         $scope.buscarNome = null;
         $scope.start = 0;
         $scope.buscarAgricultores($scope.filtros);

         }
         };

         $scope.addCpf = function() {
         if($scope.buscarCpf) {
         var cpf = {
         valor: $scope.buscarCpf,
         nome: $filter('cpf')($scope.buscarCpf),
         tipo: 'CPF',
         chave: 'cpf'
         };
         $scope.salvaTamanhoFiltroAntigo();
         $scope.filtros.push(cpf);
         $scope.buscarCpf = null;
         $scope.start = 0;
         $scope.buscarAgricultores($scope.filtros);

         }
         };*/

        $scope.buscarAutoCompletePorFiltro = function (searchText, url, filtro) {
            try {
                var search;
                if (filtro == null || filtro == undefined) {
                    search = url + searchText
                } else {
                    search = url + filtro + "/" + searchText;
                }

                var data = $http({
                    method: 'GET',
                    url: search
                }).then(function successCallback(response) {
                    return response.data;
                }, function errorCallback(response) {
                    return response.data;
                });

                return data;
            } catch (e) {
                console.log(e);
            }
        };

        $scope.adicioarFiltroChip = function (filtro) {
            if (validarAdicaoFiltro(filtro)) {
                $scope.salvaTamanhoFiltroAntigo();
                $scope.filtros.push(filtro);
                $scope.atualizaVariavelBusca($scope.filtros);
                setFormsPristine();
                $scope.buscarAgricultores($scope.filtros);
            }

        };

        function validarAdicaoFiltro(filtro) {
            return filtro.id && filtro.tipo;
        }

        function setFormsPristine() {
            $scope.formTerritorio.$setPristine();
            $scope.formMunicipio.$setPristine();
            $scope.formDistrito.$setPristine();
            $scope.formProjeto.$setPristine();
            $scope.formAter.$setPristine();
        }

        $scope.setOrdenacao = function (ordem) {
            if ($scope.ordem != ordem) {
                $scope.ordem = ordem;
                $scope.ordenar = true;
            } else {
                $scope.ordem = '';
                $scope.ordenar = false;
            }

            var novoFiltro = $scope.construirELimparFiltros($scope.filtros);

            $scope.buscarAgricultoresPorFiltro(novoFiltro, true);
        };

        $scope.salvaTamanhoFiltroAntigo = function () {
            $scope.tamanhoFiltroAntigo = $scope.filtros.length;
        }

        $scope.construirELimparFiltros = function (filtros) {
            var novoFiltro = {
                territorio: [],
                municipio: [],
                distrito: [],
                projeto: [],
                tipoATER: [],
                comunidade: []
            };

            for (var i = 0; i < filtros.length; i++) {
                if (filtros[i].tipo == 'nome') {
                    novoFiltro[filtros[i].tipo] = filtros[i].id;
                } else if (filtros[i].tipo == 'cpf') {
                    novoFiltro[filtros[i].tipo] = filtros[i].id;
                } else {
                    novoFiltro[filtros[i].tipo].push(filtros[i].id);
                }
            }

            novoFiltro.tamanho = filtros.length;
            novoFiltro.paginacao = $scope.paginacao;
            novoFiltro.ordenar = $scope.ordenar;
            novoFiltro.ordem = $scope.ordem;

            $scope.filtro = novoFiltro;

            return novoFiltro;
        };

        $scope.buscarAgricultores = function (filtros) {
            if (filtros.length > 0) {
                $scope.totalAgricultores = '';

                var novoFiltro = $scope.construirELimparFiltros(filtros);

                $scope.buscarAgricultoresPorFiltro(novoFiltro);

                $scope.buscarTotalAgricultoresPorFiltro(novoFiltro);

                if ($scope.showMap) {

                    $scope.buscarElementoCoordenadas(novoFiltro);

                    $scope.buscarAgricultoresAreaGeorreferenciada(novoFiltro);

                    $scope.salvaTamanhoFiltroAntigo();

                }

            } else {
                $scope.agricultores = [];
                $scope.mostraBotao = false;

                if ($scope.markerCluster) {
                    $scope.markerCluster.clearMarkers();
                }

                $scope.markerGoogle = [];

                if ($scope.mapGoogle != null) {
                    $scope.atualizaImagensGoogleMaps(50);
                }
            }
        };

        $scope.carregaMais = function () {
            $scope.limiteBusca = $scope.limiteBusca + $scope.paginacao;

            var novoFiltro = $scope.construirELimparFiltros($scope.filtros);

            $scope.buscarAgricultoresPorFiltro(novoFiltro);

        };

        $scope.setPaginacao = function (tamanho) {
            $scope.paginacao = tamanho;

            if (tamanho > $scope.agricultores.length && $scope.totalAgricultores > tamanho && $scope.filtros.length > 0) {
                var novoFiltro = $scope.construirELimparFiltros($scope.filtros);

                $scope.buscarAgricultoresPorFiltro(novoFiltro);

            }
        };

        $scope.buscarElementoCoordenadas = function (filtro) {
            $scope.showLoading = true;

            ElementoCoordenadas.query(filtro, {}).$promise
                .then(function (result, headers) {
                    $scope.markerGoogle = [];
                    $scope.markers = $scope.pontosEnderecosParaMarcar(result);

                    if ($scope.markerCluster) {
                        $scope.markerCluster.clearMarkers();
                    }

                    if ($scope.mapGoogle) {
                        $scope.markerCluster = new MarkerClusterer($scope.mapGoogle, $scope.markerGoogle);
                        $scope.atualizaImagensGoogleMaps(50);
                    }

                    $scope.setarCentroMapa();

                    $scope.showLoading = false;
                }).catch(function (response) {
                if (response.status == 404) {
                    $scope.markerGoogle = [];
                    $scope.markers = [];
                }
            });

        };

        $scope.buscarAgricultoresAreaGeorreferenciada = function (filtro) {
            AgricultorAreaGeorreferenciada.query(filtro, {}, function (result, headers) {
                var poligonos = $scope.carregarPoligonoPorPontos(result);

                $scope.marcarPoligonoLeaflet(poligonos);
                $scope.marcarPoligonoGoogle(poligonos);

                $scope.atualizarImagensMapa(1000);
            });
        };

        $scope.buscarTotalAgricultoresPorFiltro = function (filtro) {
            AgricultorBuscaTotal.query(filtro, {}, function (result, headers, value) {
                $scope.totalAgricultores = $filter('number')(result.total, 0);
            });
        };

        $scope.buscarAgricultoresPorFiltro = function (filtro, limparLista) {
            if (filtro.tamanho != $scope.tamanhoChips || limparLista) {
                $scope.start = 0;
            } else {
                $scope.start = $scope.agricultores.length;
            }

            $scope.limit = $scope.paginacao;

            if ($scope.start < $scope.limit) {
                $scope.limit = $scope.limit - $scope.start;
            }

            $scope.filtro.start = $scope.start;
            $scope.filtro.limit = $scope.limit;

            $scope.tamanhoChips = filtro.tamanho;

            AgricultorBusca.query(filtro, {}, function (result, headers) {
                if (filtro.tamanho != $scope.qtdFiltro || limparLista == true) {
                    $scope.agricultores = [];
                }

                $scope.agricultores = $scope.agricultores.concat(result.agricultores);
                $scope.mostrando = $scope.agricultores.length;
                $scope.start = $scope.agricultores.length;

                if (result.agricultores.length > 0) {
                    $scope.semResultado = false;
                    if (result.agricultores.length < $scope.filtro.limit) {
                        $scope.mostraBotao = false;
                    } else {
                        $scope.mostraBotao = true;
                    }
                } else {
                    $scope.semResultado = true;
                    $scope.mostraBotao = false;
                }

                $scope.qtdFiltro = filtro.tamanho;
            });
        };

        $scope.novaBusca = function () {
            $scope.atualizaVariavelBusca($scope.filtros);
            $scope.buscarAgricultores($scope.filtros);

            if ($scope.filtros.length < 1) {
                $scope.removerMarcadores();
            }
        };

        $scope.onChangeSwitch = function (showMap) {
            $scope.atualizarImagensMapa(50);
            if (!showMap) {
                $scope.salvaTamanhoFiltroAntigo();
            }
            if (showMap && $scope.filtros.length > 0) {
                if ($scope.filtros.length != $scope.tamanhoFiltroAntigo) {
                    var novoFiltro = $scope.construirELimparFiltros($scope.filtros);
                    $scope.buscarElementoCoordenadas(novoFiltro);
                    $scope.buscarAgricultoresAreaGeorreferenciada(novoFiltro);
                    $scope.salvaTamanhoFiltroAntigo();
                }
            }
        };

        $scope.removerMarcadores = function () {
            $scope.markers = {};
            $scope.paths = {};

            for (var i = 0; i < $scope.markerGoogle.length; i++) {
                $scope.markerGoogle[i].setMap(null);
            }

            for (var i = 0; i < $scope.poligonosGoogle.length; i++) {
                $scope.poligonosGoogle[i].setMap(null);
            }

        };

        $scope.atualizaVariavelBusca = function (filtros) {
            $scope.territoriosBusca = "";
            $scope.municipioBusca = "";
            $scope.distritoBusca = "";
            for (var i = 0; i < filtros.length; i++) {
                if (filtros[i].tipo == 'territorio') {
                    $scope.territoriosBusca = $scope.territoriosBusca + filtros[i].id + '&';
                }

                if (filtros[i].tipo == 'municipio') {
                    $scope.municipioBusca = $scope.municipioBusca + filtros[i].id + '&';
                }

                if (filtros[i].tipo == 'distrito') {
                    $scope.distritoBusca = $scope.distritoBusca + filtros[i].id + '&';
                }
            }
        };

        var agricultorSelecionado;
        $scope.dialogConfirmarNovaJanela = function () {
            $mdDialog.show({
                templateUrl: 'scripts/components/dialogs/usuario-avancado/confirmar-janela-agricultor/confirmar-janela-agricultor.html',
                locals: {
                    agricultorConfirmarDialog: agricultorSelecionado
                },
                controller: 'dialogConfirmarJanelaAgricultorController',
                backdrop: false,
                escapeToClose: true
            });
        };

        $scope.selecionaAgricultor = function (agricultor) {
            Agricultor.get({id: agricultor.id}, function (result) {
                agricultorSelecionado = result;
                $scope.dialogConfirmarNovaJanela();
            });
        };

        $scope.$on("leafletDirectiveMarker.ua-mapa-leaflet.click", function (event, args) {
            args.leafletObject.bindPopup("<p>carregando...</p>");

            if (args.model.elemento.agricultor == null) {
                ElementoAgricultor.get({elementoId: args.model.elemento.id}, function (elemento, headers) {
                    $rootScope.account.agricultor = elemento.agricultor;

                    args.leafletObject.bindPopup($scope.construirMensagemMapaAgricultor(elemento.agricultor)).openPopup();

                    args.model.elemento.agricultor = elemento.agricultor;
                });
            } else {
                args.leafletObject.bindPopup($scope.construirMensagemMapaAgricultor(args.model.elemento.agricultor)).openPopup();
            }
        });

        $scope.atualizarImagensMapa = function (timeOut) {
            $timeout(function () {
                leafletData.getMap("ua-mapa-leaflet").then(function (map) {
                    map.invalidateSize(true);
                });
            }, timeOut);
        };

        $scope.setarCentroMapa = function () {
            leafletData.getMap("ua-mapa-leaflet").then(function (map) {
                map.setZoom(6);
                map.panTo(new L.LatLng(-5.00339434502215, -38.8421630859375));
            });
        };

        $scope.pontosEnderecosParaMarcar = function (elementos) {
            return elementos.map(function (elemento) {
                try {
                    var coordenada = elemento.coordenada;

                    var marker = L.latLng(coordenada.latitude, coordenada.longitude);
                    marker.icon = {
                        iconUrl: 'assets/images/pin.svg',
                        shadowUrl: 'assets/images/pin-sombra.png',
                        iconSize: [29, 48],
                        shadowSize: [44, 44],
                        iconAnchor: [9, 40],
                        popupAnchor: [3, -40]
                    };
                    marker.elemento = elemento;

                    marker.layer = "pins";

                    if ($scope.mapGoogle) {
                        var pinIcon = {
                            url: 'assets/images/pin.svg',
                            size: new google.maps.Size(29, 40)
                        };
                        var markerG = new google.maps.Marker({
                            position: {
                                lat: Number((coordenada.latitude)),
                                lng: Number((coordenada.longitude))
                            },
                            map: $scope.mapGoogle,
                            icon: pinIcon
                        });

                        var infoWindow = new google.maps.InfoWindow({maxWidth: 310});

                        google.maps.event.addListener(
                            markerG,
                            'click',
                            (function (markerG, scope) {
                                return function () {
                                    ElementoAgricultor.get({elementoId: elemento.id}, function (elemento, headers) {
                                        var mensagem = "<div ng-controller=\"DialogMapaController\" ng-include src=\"'scripts/components/dialogs/mapa/mapa.html'\"></div>";
                                        $rootScope.account.agricultor = elemento.agricultor;
                                        var compiled = $compile(mensagem)(scope);
                                        $timeout(function () {
                                            $scope.$apply();
                                        });
                                        infoWindow.setContent(compiled[0]);
                                        infoWindow.open($scope.mapGoogle, markerG);
                                    });
                                };
                            })(markerG, $scope)
                        );

                        $scope.markerGoogle.push(markerG);
                    }

                    return marker;
                } catch (e) {
                    console.log(e);
                }
            });
        };

        function criarMapaGoogle() {
            $scope.mapGoogle = new google.maps.Map(document.getElementById('ua-mapa-google'), {
                center: {
                    lat: -5.00339434502215,
                    lng: -38.8421630859375
                },
                mapTypeId: google.maps.MapTypeId.HYBRID,
                scrollwheel: true,
                zoom: 6
            });
        }

        $scope.inicializarGoogleMaps = function () {
            $scope.mostraMapa = 'googlemaps';

            if ($scope.mapGoogle == null) {
                criarMapaGoogle();

                if ($scope.filtros.length > 0) {

                    var novoFiltro = $scope.construirELimparFiltros($scope.filtros);

                    $scope.buscarElementoCoordenadas(novoFiltro);

                    $scope.buscarAgricultoresAreaGeorreferenciada(novoFiltro);
                }
            }

            $scope.atualizaImagensGoogleMaps(100);
        };

        $scope.atualizaImagensGoogleMaps = function (timeOut) {
            $timeout(function () {
                google.maps.event.trigger($scope.mapGoogle, 'resize');
                $scope.mapGoogle.setZoom(6);
                $scope.mapGoogle.setCenter(
                    new google.maps.LatLng({
                        lat: -5.00339434502215,
                        lng: -38.8421630859375
                    })
                );
            }, 100);
        };

        $scope.construirMensagemMapaAgricultor = function (agricultor) {
            var mensagem = "<div id=\"div-agricultor-pin-mapa\" ng-controller=\"DialogMapaController\" ng-include src=\"'scripts/components/dialogs/mapa/mapa.html'\"></div>";

            return mensagem;
        };

        $scope.carregarPoligonoPorPontos = function (agricultoresComPoligono) {
            var poligonos = [];

            for (var agricultor in agricultoresComPoligono) {
                if (agricultoresComPoligono[agricultor].areaGeorreferenciada) {
                    var area = agricultoresComPoligono[agricultor].areaGeorreferenciada;

                    var vertices = [];

                    for (var i in area.poligono) {
                        var coordenada = area.poligono[i].coordenada,
                            latLng = new L.LatLng(coordenada.latitude, coordenada.longitude);

                        vertices.push(latLng);
                    }

                    poligonos.push(vertices);
                }
            }

            return poligonos;
        };

        $scope.marcarPoligonoLeaflet = function (poligonos) {
            for (var i in poligonos) {
                $scope.paths[i] = {
                    color: '#2e3974',
                    weight: 1,
                    latlngs: poligonos[i],
                    layer: 'poligonos',
                    type: 'polygon'
                }
            }
        };

        $scope.marcarPoligonoGoogle = function (poligonos) {
            for (var i in poligonos) {
                var poligonoGoogle = new google.maps.Polygon({
                    paths: poligonos[i],
                    strokeColor: '#0e1cdf',
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: '#0e1cdf',
                    fillOpacity: 0.35
                });

                poligonoGoogle.setMap($scope.mapGoogle);

                $scope.poligonosGoogle.push(poligonoGoogle);
            }
        };

        $scope.IntroOptions = {
            steps: [
                {
                    element: '#main-usuario-avancado-div-tabs',
                    intro: "Olá, usuário(a)! Aqui você pode buscar os agricultores(as) filtrando por nome, cpf, território, município, distrito e projeto."
                },
                {
                    element: '#main-usuario-avancado-adicionar-button',
                    intro: "Ao digitar o que você procura e clicar em adicionar você insere um tipo de filtro e pode ir adicionando mais, para obter uma busca mais refinada."
                },
                {
                    element: '#main-usuario-avancado-div-paginacao',
                    intro: "Você pode visualizar os resultados da busca de 10 em 10 agricultores(as), de 30 em 30 ou de 60 em 60."
                },
                {
                    element: '#main-usuario-avancado-div-chips',
                    intro: "Os filtros podem ser somados e removidos a qualquer momento."
                },
                {
                    element: '#main-usuario-avancado-div-sort',
                    intro: "Aqui, você pode ordenar sua busca por ordem alfabética, em ordem ascendente ou ordem descendente."
                },
                {
                    element: '#main-usuario-avancado-list',
                    intro: "Na lista de resultados você tem um resumo sobre o(a) agricultor(a) e ao clicar você terá acesso a todo o prontuário.",
                    position: 'right'
                },
                {
                    element: '#main-usuario-avancado-span-total',
                    intro: "Ao fazer uma pesquisa você visualiza os 10, 30 ou 60 primeiros(as) agricultores(as) e o total de resultados encontrados."
                },
                {
                    element: '#main-usuario-avancado-div-switch-mapa',
                    intro: "Ao fazer a busca você pode visualizar os resultados em forma de lista ou mapa."
                },
                {
                    element: '#main-usuario-avancado-div',
                    intro: "Neste mapa você pode visualizar e selecionar agricultores(as) que possuam coordenadas geográficas cadastradas. Clicando neste ícone " + "<md-button class='md-icon-button material-icons main-usuario-avancado-botao-map'>map</md-button>" + " no canto superior direito do mapa você pode selecionar diferentes opções de visualização de imagens de satélite e mapas do Google e Open Street Map.",
                    position: 'left'
                },
                {
                    element: '#main-usuario-avancado-div-button-mapa',
                    intro: "Aqui, você tem a opção de utilizar o aplicativo Google maps e seleciona como quer visualizas os agricultores(as) se por mapa ou street view (caso esta opção esteja disponível para o local)."
                },
                {
                    element: '#ua-mapa-leaflet',
                    intro: "No mapa você visualiza as coordenadas dos(as) agricultores(as) de forma agrupada e a medida que ampliar o zoom o agrupamento diminui, até chegar na coordenada de um único agricultor(a).",
                    position: 'right'
                }


            ],
            showStepNumbers: false,
            exitOnOverlayClick: false,
            exitOnEsc: true,
            nextLabel: "Próximo",
            prevLabel: "Anterior",
            skipLabel: "Encerrar",
            doneLabel: "Terminar",
            showBullets: false

        };

        $scope.introOnBeforeChange = function (targetElement, scope) {

            if (targetElement.id == 'main-usuario-avancado-div-tabs') {
                $scope.showMap = false;
                $scope.filtros = [];
                $scope.agricultores = [];
                $scope.mostraBotao = false;
                $scope.verificaTutorial = false;
            }

            if (targetElement.id == 'main-usuario-avancado-div-chips') {
                if ($scope.verificaTutorial == false) {
                    AgricultorJSON.get(function (result) {
                        $scope.agricultores = result;
                        var nome = {
                            valor: $scope.agricultores[0].pessoaFisica.pessoa.nome,
                            nome: $scope.agricultores[0].pessoaFisica.pessoa.nome,
                            tipo: 'Nome',
                            chave: 'nome'
                        };
                        $scope.filtros.push(nome);
                    });

                    var coordenada = [{
                        "id": 10008911654,
                        "agricultor": null,
                        "coordenada": {
                            "id": 10008910602,
                            "latitude": "-3.146343408152461",
                            "longitude": "-40.52871306426823"
                        },
                        "tipoElemento": null
                    }];
                    $scope.markers = $scope.pontosEnderecosParaMarcar(coordenada);
                    $scope.totalAgricultores = 1;
                    $scope.mostrando = 1;
                    $scope.verificaTutorial = true;
                }

            }

            if (targetElement.id == 'main-usuario-avancado-div-switch-mapa') {
                $scope.showMap = true;
                $scope.onChangeSwitch($scope.showMap);
            }

            if (targetElement.id == 'main-usuario-avancado-span-total') {
                if ($scope.showMap) {
                    $scope.showMap = false;
                    $scope.onChangeSwitch($scope.showMap);
                }
            }
        };


        $scope.introOnExit = function (scope) {
            $scope.showMap = false;
            $scope.filtros = [];
            $scope.agricultores = [];
            $scope.mostraBotao = false;
            $scope.verificaTutorial = false;
            $scope.mostrando = 0;
            $scope.totalAgricultores = 0;
        };

        $scope.introOnStart = function () {
            $scope.introStart();
        };

        $scope.limparFiltros = function () {
            $scope.filtros = [];
            $scope.novaBusca();
        };

        $rootScope.introOnStart = $scope.introOnStart;
    });
