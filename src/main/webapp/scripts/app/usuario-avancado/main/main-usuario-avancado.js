'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider, rolesAccess) {
        $stateProvider
            .state('main-usuario-avancado', {
                parent: 'usuario-avancado',
                url: '/main-usuario-avancado',
                data: {
                    authorities: rolesAccess['ALL_ROLES'].buscar,
                    pageTitle: 'Busca'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/usuario-avancado/main/main-usuario-avancado.html',
                        controller: 'UsuarioAvancadoController'
                    }
                },
                resolve: {

                }
            });
    });
