'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('error', {
                parent: 'comum',
                url: '/error',
                data: {
                    authorities: [],
                    pageTitle: 'Erro!'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/comum/error/error.html'
                    }
                },
                resolve: {

                }
            })
            .state('accessdenied', {
                parent: 'comum',
                url: '/accessdenied',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/comum/error/accessdenied.html'
                    },
                    'contentAu@': {
                        templateUrl: 'scripts/app/comum/error/accessdenied.html'
                    }
                },
                resolve: {

                }
            });
    });
