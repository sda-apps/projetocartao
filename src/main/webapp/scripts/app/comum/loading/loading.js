'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('loading', {
                parent: 'comum',
                url: '/loading',
                data: {
                    authorities: [],
                    pageTitle: 'Carregando'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/comum/loading/loading.html'
                    }
                },
                resolve: {

                }
            });
    });
