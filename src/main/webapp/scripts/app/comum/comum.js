'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('comum', {
                abstract: true,
                parent: 'site'
            });
    });
