'use strict';

angular.module('projetocartaoApp')
    .controller('UserManagementController', function ($scope, $rootScope, Principal, User, ParseLinks, SignUp, $mdDialog, AprovaUsuario, InativaUsuario ,Toast) {
        /*
        $scope.authorities = ["ROLE_USER", "ROLE_ADMIN"];
		Principal.identity().then(function(account) {
            $scope.currentAccount = account;
        });
        $scope.page = 1;
        $scope.loadPage = function (page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.setActive = function (user, isAtivado) {
            user.ativado = isAtivado;
            User.update(user, function () {
                $scope.loadAll();
                $scope.clear();
            });
        };
        $scope.clear = function () {
            $scope.user = {
                id: null, login: null, nome: null, sobrenome: null, email: null,
                ativado: null, createdBy: null, createdDate: null,
                lastModifiedBy: null, lastModifiedDate: null, chaveReativacao: null, authorities: null
            };
            $scope.editForm.$setPristine();
            $scope.editForm.$setUntouched();
        };
        */
        $scope.mostraBotao  = false;
        $scope.usuarios = [];

        $scope.filtro = {
            nome: null,
            ordenarPor: "usuarioAvancado.pessoaFisica.pessoa.nome",
            start: 0,
            limit: 10,
            paginacao: 10,
            ordenar: true,
            ordemAsc: true
        };

        $scope.users = [];
        var usuariosSelecionados = [];

        $scope.loadAll = function (filtro) {
            User.query(filtro,{}).$promise.then(function (result) {
                $scope.users = result;
                $scope.mostraBotao = $scope.users.length>=$scope.filtro.limit;
            })
            .catch(function (error) {
                $scope.users = [];
            });
        };

        $scope.loadAll($scope.filtro);

        $scope.toggle = function (item) {
            var idx = usuariosSelecionados.indexOf(item);
            idx > -1 ? usuariosSelecionados.splice(idx, 1) : usuariosSelecionados.push(item);
        };

        $scope.isAllChecked = function() {
            return usuariosSelecionados.length === $scope.users.length;
        };

        $scope.isChecked = function (item) {
            return usuariosSelecionados.indexOf(item) > -1;
        };

        $scope.isIndeterminate = function() {
            return (usuariosSelecionados.length !== 0 && usuariosSelecionados.length !== $scope.users.length);
        };

        $scope.toggleAll = function() {
            if (usuariosSelecionados.length === $scope.users.length) {
                usuariosSelecionados = [];
            } else if (usuariosSelecionados.length === 0 || usuariosSelecionados.length > 0) {
                usuariosSelecionados = [];
                for (var i = 0; i<$scope.users.length; i++) {
                    usuariosSelecionados.push($scope.users[i].id);
                }
            }
        };

        $scope.listClick = function (user) {
            exibirDialogUser('scripts/components/dialogs/usuario-avancado/cadastrar-editar-user/editar-user.html', user);
        };

        $scope.ordenarLista = function (item){
            if($scope.filtro.ordenarPor == item){
                $scope.filtro.ordemAsc = !$scope.filtro.ordemAsc;
            }else{
                $scope.filtro.ordenarPor = item;
                $scope.filtro.ordemAsc = true;
            }
            $scope.loadAll($scope.filtro);
        };

        $scope.cadastrarUsuario = function () {
            exibirDialogUser('scripts/components/dialogs/usuario-avancado/cadastrar-editar-user/cadastrar-user.html', null);
        };

        $scope.reenviarConvite = function () {
        	//SignUp.post(usuariosSelecionados);
            SignUp.post(usuariosSelecionados).$promise.then(function (result) {
                console.log(result);
                Toast.sucesso("E-mail enviado com sucesso.");
            }).catch(function (error) {
                Toast.erro("Erro ao enviar e-mail para técnico.");
            });
        };

        $scope.desativarUsuario = function () {
        	for(var i in usuariosSelecionados) {
                $scope.usuarios[i] = {
                    id:  usuariosSelecionados[i]
                }
            }

        	InativaUsuario.post($scope.usuarios).$promise.then(function (){
                $scope.loadAll($scope.filtro);
            }).catch(function (erro) {
                var msg = erro.data.status + ' - ' + erro.data.error;
                Toast.erro(msg);
            });

        };

        $scope.aprovarUsuario = function () {
            for(var i in usuariosSelecionados) {
                $scope.usuarios[i] = {
                    id:  usuariosSelecionados[i]
                }
            }

            AprovaUsuario.post($scope.usuarios).$promise.then(function (){
                $scope.loadAll($scope.filtro);
            }).catch(function (erro) {
                var msg = erro.data.status + ' - ' + erro.data.error;
                Toast.erro(msg);
            });
        }

        $scope.pesquisarNome = function (nome) {
            $scope.filtro.nome = nome;
            $scope.filtro.limit = $scope.filtro.paginacao;
            $scope.loadAll($scope.filtro);
        };

        var exibirDialogUser = function (template, user) {
            $mdDialog.show({
                controller: 'CadastrarEditarUserController',
                templateUrl: template,
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                disableParentScroll: true,
                locals: {
                    userToEdit: user,
                    isUsuarioEmpresa: false,
                    empresa: null
                },
                fullscreen: true
            })
            .then(function(answer) {
                //return
            }, function() {
                //cancel
            });
        };

        $scope.carregaMais = function() {
            $scope.filtro.limit = $scope.filtro.limit + $scope.filtro.paginacao;
            $scope.loadAll($scope.filtro);
        };

        $scope.setPaginacao = function(tamanho) {
            $scope.filtro.limit = tamanho;
            $scope.filtro.paginacao = tamanho;
            if(tamanho > $scope.users.length ) {
                $scope.loadAll($scope.filtro);
            }
        };

        $rootScope.introOnStart = null;

    });
