'use strict';

angular.module('projetocartaoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('auditoria-acessos', {
                parent: 'site',
                url: '/auditoria-acessos',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'Administração Portal - Auditoria'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/admin/audits/audits.html',
                        controller: 'AuditsController'
                    }
                },
                resolve: {

                }
            });
    });
