'use strict';

angular.module('projetocartaoApp', ['ngMaterial', 'ngMessages','LocalStorageModule',
    'ngResource', 'ngCookies', 'ngAria', 'ngCacheBuster', 'ngFileUpload', 'ngFileSaver',
    // jhipster-needle-angularjs-add-module JHipster will add new module here
    'ui.bootstrap', 'ui.router',  'infinite-scroll', 'angular-loading-bar','angucomplete-alt','ngSanitize', 'leaflet-directive', 'ngImgCrop', 'angular-intro', 'googlechart','ng.deviceDetector', 'luegg.directives'])

    .run(function ($rootScope, $location, $window, $http, $state,  Auth, Principal, ENV, VERSION, $filter) {

        $rootScope.ENV = ENV;
        $rootScope.VERSION = VERSION;
        $rootScope.corFoto = Math.floor((Math.random() * 15) + 1);
        $rootScope.$on('$stateChangeStart', function (event, toState, toStateParams) {
            $rootScope.toState = toState;
            $rootScope.toStateParams = toStateParams;
            if (Principal.isIdentityResolved()) {
                Auth.authorize();
            }
        });

        $rootScope.$on('$stateChangeSuccess',  function(event, toState, toParams, fromState, fromParams) {
            var titleKey = 'Portal da Agricultura Familiar';

            // Remember previous state unless we've been redirected to login or we've just
            // reset the state memory after logout. If we're redirected to login, our
            // previousState is already set in the authExpiredInterceptor. If we're going
            // to login directly, we don't want to be sent to some previous state anyway

            // if ((toState.name != 'login' || toState.name != 'login-agricultor')&& $rootScope.previousStateName)
            // $rootScope.previousStateName nao existe porque depende dela mesma para ser iniciada. Deve ser verificado
            // se existe um estado anterior atraves do parametro fromState
            if (toState.name != 'login' && toState.name != 'login-agricultor'){
                if(fromState){
                    $rootScope.previousStateName = fromState.name;
                    $rootScope.previousStateParams = fromParams;
                }
            }else{
                $rootScope.previousStateName = null;
                $rootScope.previousStateParams = null;
            }
            // Set the page title key to the one configured in state or use default one
            if (toState.data.pageTitle) {
                titleKey = toState.data.pageTitle + " - " +titleKey;
            }
            if(!$rootScope.isNovaAba){
                $window.document.title = titleKey;
            }

            $rootScope.introOnStart = undefined;
            $rootScope.fabAdd = undefined;
            $rootScope.subToolbar = undefined;
        });

        $rootScope.$watch('isNovaAba', function (value) {
            if (value){
                $window.document.title = $filter('capitalize')($rootScope.account.agricultor.pessoaFisica.pessoa.nome) + " - Portal da Agricultura Familiar";
            }
        });

        $rootScope.back = function() {
            // If previous state is 'activate' or do not exist go to 'home'
            if ($rootScope.previousStateName === 'activate' || $state.get($rootScope.previousStateName) === null) {
                $state.go('login-agricultor');
            } else {
                $state.go($rootScope.previousStateName, $rootScope.previousStateParams);
            }
        };
    })
    .config(function ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider, $mdThemingProvider, httpRequestInterceptorCacheBusterProvider, AlertServiceProvider, $logProvider) {
        // uncomment below to make alerts look like toast
        //AlertServiceProvider.showAsToast(true);
    	//enable CSRF
        $httpProvider.defaults.xsrfCookieName = 'CSRF-TOKEN';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRF-TOKEN';

        //Cache everything except rest api requests
        httpRequestInterceptorCacheBusterProvider.setMatchlist([/.*api.*/, /.*protected.*/], true);

        $urlRouterProvider.otherwise('/login-agricultor');
        $stateProvider.state('site', {
            'abstract': true,
            views: {
                'menu@': {
                    templateUrl: 'scripts/components/menu/menu.html',
                    controller: 'MenuController'
                }
            },
            resolve: {
                authorize: ['Auth',
                    function (Auth) {
                        return Auth.authorize();
                    }
                ]
            }
        });

        $httpProvider.interceptors.push('authExpiredInterceptor');
        $httpProvider.interceptors.push('authInterceptor');
        $httpProvider.interceptors.push('notificationInterceptor');
        // jhipster-needle-angularjs-add-interceptor JHipster will add new application interceptor here

        // TEMA BOTÃO DIALOG
        var backButtonAU = $mdThemingProvider.extendPalette('grey', { 'A100': 'EEEEEE' });
        var primaryButtonAU = $mdThemingProvider.extendPalette('green', { '500': 'FF5252' });
        var accentButtonAU =  $mdThemingProvider.extendPalette('red', { '500': 'FF5252' });

        $mdThemingProvider.definePalette('backButtonUA', backButtonAU);
        $mdThemingProvider.definePalette('primaryButtonUA', primaryButtonAU);
        $mdThemingProvider.definePalette('accentButtonAU', accentButtonAU);

        $mdThemingProvider.theme('themeButtonUA')
            .primaryPalette('primaryButtonUA')
            .accentPalette('accentButtonAU')
            .backgroundPalette('backButtonUA');

        // TEMA USUARIO AVANCADO
        var backAU = $mdThemingProvider.extendPalette('grey', { 'A100': 'EEEEEE' });
        var primaryAU = $mdThemingProvider.extendPalette('green', { '500': '4CAF50' });
        var accentAU =  $mdThemingProvider.extendPalette('red', { '500': 'FF5252' });

        $mdThemingProvider.definePalette('backUA', backAU);
        $mdThemingProvider.definePalette('primaryUA', primaryAU);
        $mdThemingProvider.definePalette('accentAU', accentAU);

        $mdThemingProvider.theme('themeUA')
            .primaryPalette('primaryUA')
            .accentPalette('accentAU')
            .backgroundPalette('backUA');


        var backAUWhite = $mdThemingProvider.extendPalette('grey', { 'A100': 'ffffff' });
        $mdThemingProvider.definePalette('backAUWhite', backAUWhite);
        $mdThemingProvider.theme('themeUAWhite')
            .primaryPalette('primaryUA')
            .accentPalette('blue-grey')
            .backgroundPalette('backAUWhite');

        // TEMA AGRICULTOR
        var back = $mdThemingProvider.extendPalette('grey', { 'A100': 'fafbfb' });
        var primary = $mdThemingProvider.extendPalette('green', { '500': '9cc96b'});

        $mdThemingProvider.definePalette('back', back);
        $mdThemingProvider.definePalette('primary', primary);

        $mdThemingProvider.theme('default')
            .primaryPalette('primary')
            .accentPalette('blue-grey')
            .backgroundPalette('back');

        $mdThemingProvider.theme('dark')
            .dark();

        var backWhite = $mdThemingProvider.extendPalette('grey', { 'A100': 'ffffff' });
        $mdThemingProvider.definePalette('backWhite', backWhite);

        $mdThemingProvider.theme('themeAWhite')
            .primaryPalette('primary')
            .accentPalette('blue-grey')
            .backgroundPalette('backWhite');

        $logProvider.debugEnabled(false);

    })
    // jhipster-needle-angularjs-add-config JHipster will add new application configuration here
    .config(['$urlMatcherFactoryProvider', function($urlMatcherFactory) {
        $urlMatcherFactory.type('boolean', {
            name : 'boolean',
            decode: function(val) { return val == true ? true : val == "true" ? true : false },
            encode: function(val) { return val ? 1 : 0; },
            equals: function(a, b) { return this.is(a) && a === b; },
            is: function(val) { return [true,false,0,1].indexOf(val) >= 0 },
            pattern: /bool|true|0|1/
        });
    }])

    .config(function($mdDateLocaleProvider){
        $mdDateLocaleProvider.formatDate = function(date){
            if(date){
                var day = date.getDate();
                var monthIndex = date.getMonth();
                var year = date.getFullYear();

                return day + '/' + (monthIndex + 1) + '/' + year;
            }
            return "";
        };
    })

    .config(function($mdDateLocaleProvider){
        $mdDateLocaleProvider.parseDate=function(dateString){

            var data = dateString.split('/');
            var d = new Date(data[2], data[1] -1, data[0]);
            return angular.isDate(d) ? d : new Date(NaN);
        }
    });
