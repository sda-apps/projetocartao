package br.gov.ce.sda.service.ater;


import br.gov.ce.sda.domain.ater.Tema;
import br.gov.ce.sda.repository.ater.CustomTemaRepository;
import br.gov.ce.sda.repository.ater.TemaRepository;
import br.gov.ce.sda.web.rest.dto.FiltroTemaDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

@Service
public class TemaService {

    @Inject
    private TemaRepository temaRepository;

    @Inject
    private CustomTemaRepository customTemaRepository;

    @Transactional(readOnly = true)
    public List<Tema> findAll() {
        return temaRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<Tema> findAllByFilter(FiltroTemaDTO filtro) {
        return customTemaRepository.findAllByFilter(filtro);
    }

    @Transactional(readOnly = true)
    public Tema findById(Long id) {
        return temaRepository.findOneById(id);
    }

    @Transactional
    public Tema save(Tema tema) {
        Tema savedTema = temaRepository.save(tema);
        return savedTema;
    }

    @Transactional
    public void update(Tema currentTema) {
        temaRepository.save(currentTema);
    }

    @Transactional
    public void delete(Long id) {
        temaRepository.deleteById(id);
    }

}
