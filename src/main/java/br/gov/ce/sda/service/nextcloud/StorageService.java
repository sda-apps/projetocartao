package br.gov.ce.sda.service.nextcloud;


import br.gov.ce.sda.exceptions.StorageException;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface StorageService {

    void saveFile(String bucketName, MultipartFile multipartFile) throws StorageException;

    Optional<Resource> getFile(String bucketName, String fileName) throws StorageException;

    Optional<List<Resource>> getListFiles(String bucketName) throws StorageException;

    Optional<List<String>>  getListURLFiles(String bucketName) throws StorageException;
}
