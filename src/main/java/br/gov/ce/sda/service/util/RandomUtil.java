package br.gov.ce.sda.service.util;

import org.apache.commons.lang.RandomStringUtils;

/**
 * Utility class for generating random Strings.
 */
public final class RandomUtil {

    private static final int DEF_COUNT = 20;
    
    private static final int PASSWORD_COUNT = 60;

    private RandomUtil() { }
    
    
    /**
     * Generates a login.
     *
     * @return the generated login
     */
    public static String generateLogin() {
        return RandomStringUtils.randomAlphanumeric(DEF_COUNT);
    }
    
    /**
     * Generates a password.
     *
     * @return the generated password
     */
    public static String generatePassword() {
        return RandomStringUtils.randomAlphanumeric(PASSWORD_COUNT);
    }

    /**
     * Generates an activation key.
     *
     * @return the generated activation key
     */
    public static String generateActivationKey() {
        return RandomStringUtils.randomNumeric(DEF_COUNT);
    }

    /**
    * Generates a reset key.
    *
    * @return the generated reset key
    */
    public static String generateResetKey() {
        return RandomStringUtils.randomNumeric(DEF_COUNT);
    }
}
