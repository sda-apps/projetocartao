package br.gov.ce.sda.service.ater;

import br.gov.ce.sda.domain.ater.TecnicoPTA;
import br.gov.ce.sda.domain.mobile.AreaAtuacao;
import br.gov.ce.sda.repository.AreaAtuacaoRepository;
import br.gov.ce.sda.repository.ater.TecnicoPTARepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

@Service
public class TecnicoPTAService {

    @Inject
    private TecnicoPTARepository tecnicoPTARepository;

    @Inject
    private AreaAtuacaoRepository areaAtuacaoRepository;

    @Transactional
    public TecnicoPTA save(TecnicoPTA tecnicoPTA) {
        tecnicoPTA = tecnicoPTARepository.save(tecnicoPTA);

        for (AreaAtuacao areaAtuacao : tecnicoPTA.getTecnico().getAreasAtuacao()) {
            areaAtuacao.setUsuarioAvancado(tecnicoPTA.getTecnico());
            areaAtuacao.setPlanoTrabalhoAnual(tecnicoPTA.getPlanoTrabalhoAnual());
            areaAtuacaoRepository.save(areaAtuacao);
        }

        return tecnicoPTA;
    }

    @Transactional
    public void update(TecnicoPTA tecnicoPTA) {
        tecnicoPTARepository.save(tecnicoPTA);
    }

    @Transactional
    public void delete(Long id) {
        TecnicoPTA tecnico = tecnicoPTARepository.findById(id).get();
        areaAtuacaoRepository.deleteAll(areaAtuacaoRepository.findAllByUsuarioAvancadoId(tecnico.getTecnico().getId()));
        tecnicoPTARepository.deleteById(id);
    }

    public void atualizarAreasTrabalhoTecnico(TecnicoPTA tecnicoPTA) {
        areaAtuacaoRepository.deleteAll(areaAtuacaoRepository.findAllByUsuarioAvancadoId(tecnicoPTA.getTecnico().getId()));

        if (!tecnicoPTA.getTecnico().getAreasAtuacao().isEmpty()) {
            for (AreaAtuacao area : tecnicoPTA.getTecnico().getAreasAtuacao()) {
                area.setUsuarioAvancado(tecnicoPTA.getTecnico());
                area.setPlanoTrabalhoAnual(tecnicoPTA.getPlanoTrabalhoAnual());
            }

            areaAtuacaoRepository.saveAll(tecnicoPTA.getTecnico().getAreasAtuacao());
        }
    }
}
