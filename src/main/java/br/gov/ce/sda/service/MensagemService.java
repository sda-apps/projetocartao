package br.gov.ce.sda.service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.delay.packet.DelayInformation;
import org.jivesoftware.smackx.forward.packet.Forwarded;
import org.jivesoftware.smackx.iqregister.AccountManager;
import org.jivesoftware.smackx.mam.MamManager;
import org.jivesoftware.smackx.mam.MamManager.MamQueryResult;
import org.jivesoftware.smackx.offline.OfflineMessageManager;
import org.jivesoftware.smackx.search.ReportedData;
import org.jivesoftware.smackx.search.UserSearchManager;
import org.jivesoftware.smackx.xdata.Form;
import org.jxmpp.jid.DomainBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.springframework.stereotype.Service;

import br.gov.ce.sda.web.rest.dto.MensagemDTO;

/**
 *
 * @author davi.monteiro
 *
 *	Funcionalidades:
 *	-Login e Logout (OK)
 *	-Enviar mensagens (OK)
 *	-Receber mensagens (OK
 *	-Ler todas as mensagens antigas (OK)
 *	-Número de mensagens não lidas (OK)
 *	-Listar somente mensagem não lidas (OK)
 *	-Adicionar usuários (OK)
 *	-Adicionar os contatos de um usuário
 *
 */
@Service
public class MensagemService implements ChatManagerListener, ChatMessageListener {

	private static final String SERVER_DOMAIN = "@projetocartao";

	private static final String ADMIN = "admin";

	private static final String PROJETO_CARTAO = "projetocartao";

	private static final String HOST = "172.28.3.236";

	private Map<String, AbstractXMPPConnection> connections = new ConcurrentHashMap<String, AbstractXMPPConnection>();

	/**
	 *
	 * @param username
	 * @param password
	 * @throws Exception
	 */
	public XMPPConnection login(String username, String password) throws Exception {
		AbstractXMPPConnection connection = getXMPPTConnection(username, password);
		connection.connect();
		connection.login();
		connections.put(username, connection);
		return connection;
	}

	public AbstractXMPPConnection getXMPPTConnection(String username, String password) throws Exception {
		// Create the configuration for this new connection
		XMPPTCPConnectionConfiguration.Builder configBuilder = XMPPTCPConnectionConfiguration.builder();
		configBuilder.setUsernameAndPassword(username, password);
		configBuilder.setHost(HOST);
		configBuilder.setSecurityMode(SecurityMode.disabled);
		configBuilder.setXmppDomain(JidCreate.from(PROJETO_CARTAO).asDomainBareJid());
		configBuilder.setSendPresence(false);

		//configBuilder.setDebuggerEnabled(true);

		AbstractXMPPConnection connection = new XMPPTCPConnection(configBuilder.build());
		return connection;
	}

	/**
	 *
	 * @param username
	 */
	public void logout(String username) {
		connections.get(username).disconnect();
		connections.remove(username);
	}

	/**
	 *
	 * @param username
	 * @param password
	 * @throws Exception
	 */
	public void createAccount(String username, String password) throws Exception {
		XMPPConnection connection = login(ADMIN, ADMIN);
		AccountManager accountManager = AccountManager.getInstance(connection);
		accountManager.createAccount(JidCreate.from(username).getLocalpartOrNull(), password);
		logout(ADMIN);
	}

	/**
	 *
	 * @param message
	 * @throws Exception
	 */
	public void sendMessage(MensagemDTO message) throws Exception {
		Objects.requireNonNull(message);

		String username = message.getDe();
		if (!isConnected(username)) {
			login(username, username);
		}

		ChatManager chatManager = ChatManager.getInstanceFor(connections.get(message.getDe()));
		Chat chat = chatManager.createChat(JidCreate.from(message.getPara() + SERVER_DOMAIN).asEntityJidIfPossible(), this);

		chat.sendMessage(message.getMensagem());
	}

	/**
	 *
	 * @param from
	 * @param to
	 * @return
	 * @throws Exception
	 */
	public List<MensagemDTO> getAllMessages(String from, String to) throws Exception {
		if (!isConnected(from)) {
			login(from, from);
		}

		List<MensagemDTO> mensagens = new ArrayList<MensagemDTO>();
		MamManager mamManager = MamManager.getInstanceFor(connections.get(from));
		MamQueryResult queryArchive = mamManager.queryArchive(JidCreate.from(to + SERVER_DOMAIN).asBareJid());
		for (Forwarded forwarded : queryArchive.forwardedMessages) {

			if (forwarded.getForwardedStanza() instanceof Message) {
				Message message = (Message) forwarded.getForwardedStanza();

				Boolean enviado = Boolean.FALSE;
				if (message.getFrom().asBareJid().getLocalpartOrNull().equals(to)) {
					enviado = Boolean.TRUE;
				}

				LocalDateTime dateTime = LocalDateTime.ofInstant(forwarded.getDelayInformation().getStamp().toInstant(), ZoneId.systemDefault());

				MensagemDTO dto = MensagemDTO.builder()
						.de(message.getFrom().asBareJid().getLocalpartOrNull().toString())
						.para(message.getTo().asBareJid().getLocalpartOrNull().toString())
						.mensagem(message.getBody())
						.dataEnvio(forwarded.getDelayInformation().getStamp())
						.horaEnvio(dateTime.getHour() + ":" + dateTime.getMinute())
						.enviado(enviado)
						.build();

				mensagens.add(dto);
			}

		}

		return mensagens;
	}

	/**
	 *
	 * @param username
	 * @return
	 * @throws Exception
	 */
	public int getUnreadMessagesCount(String username) throws Exception {
		OfflineMessageManager offlineMessageManager = new OfflineMessageManager(connections.get(username));
		return offlineMessageManager.getMessageCount();
	}

	/**
	 *
	 * @param username
	 * @return
	 * @throws Exception
	 */
	public List<MensagemDTO> getUnreadMessages(String username) throws Exception {
		OfflineMessageManager offlineMessageManager = new OfflineMessageManager(connections.get(username));

		List<MensagemDTO> messages = new ArrayList<MensagemDTO>();

		for (Message message : offlineMessageManager.getMessages()) {
			DelayInformation inf = (DelayInformation) message.getExtension(DelayInformation.ELEMENT, DelayInformation.NAMESPACE);
			LocalDateTime dateTime = LocalDateTime.ofInstant(inf.getStamp().toInstant(), ZoneId.systemDefault());

			MensagemDTO dto = MensagemDTO.builder()
					.de(message.getFrom().asBareJid().toString())
					.para(message.getTo().asBareJid().toString())
					.mensagem(message.getBody())
					.dataEnvio(inf.getStamp())
					.horaEnvio(dateTime.getHour() + ":" + dateTime.getMinute())
					.build();
			messages.add(dto);
		}

		Presence presence = new Presence(Presence.Type.available);
		connections.get(username).sendStanza(presence);
		offlineMessageManager.deleteMessages();

		return messages;
	}

	@Override
	public void chatCreated(Chat chat, boolean createdLocally) {
		chat.addMessageListener(this);
	}

	@Override
	public void processMessage(Chat chat, Message message) {
		System.err.println("processMessage");

		if (message.getType().equals(Message.Type.chat) || message.getType().equals(Message.Type.normal)) {
			System.err.println("Xmpp message received: '"  + message);
	        if (message.getType() == Message.Type.chat && message.getBody() != null) {
	        	System.err.println(message.getBody());
	        }
	    }
	}

	private boolean isConnected(String username) throws Exception {
		boolean isConnected = Objects.nonNull(connections.get(username));
		return isConnected;
	}

	/**
	 * XMPPError: feature-not-implemented - cancel
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	public boolean checkIfUserExists(String user) throws Exception{
		XMPPConnection connection = login(ADMIN, ADMIN);
		connection.sendStanza(new Presence(Presence.Type.available));
	    UserSearchManager search = new UserSearchManager(connection);

	    DomainBareJid searchService = JidCreate.domainBareFrom(PROJETO_CARTAO);

	    Form searchForm = search.getSearchForm(searchService);
	    Form answerForm = searchForm.createAnswerForm();
	    answerForm.setAnswer("Username", true);
	    answerForm.setAnswer("search", user);


	    ReportedData data = search.getSearchResults(answerForm,searchService);
	     if(data.getRows() != null) {
	            List<ReportedData.Row> rows = data.getRows();
	            Iterator<ReportedData.Row> it = rows.iterator();
	             if (it.hasNext()) {
	                 //user exists
	            	 System.out.println("user exists");
	            } else {
	                 //user doesnt exists
	            	System.out.println("user doesnt exists");
	            }
	     }

	     return false;
	}

}
