package br.gov.ce.sda.service;


import br.gov.ce.sda.domain.ater.Comunidade;
import br.gov.ce.sda.repository.ater.ComunidadeRepository;
import br.gov.ce.sda.repository.ater.CustomComunidadeRepository;
import br.gov.ce.sda.web.rest.dto.FiltroComunidadeDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

@Service
public class ComunidadeService {

    @Inject
    private ComunidadeRepository comunidadeRepository;

    @Inject
    private CustomComunidadeRepository customComunidadeRepository;

    @Transactional
    public Comunidade save(Comunidade comunidade) {
        return comunidadeRepository.save(comunidade);
    }

    @Transactional(readOnly = true)
    public List<Comunidade> findAll() {
        return comunidadeRepository.findAll();
    }

    @Transactional
    public void update(Comunidade currentComunidade) {
        comunidadeRepository.save(currentComunidade);
    }

    @Transactional(readOnly = true)
    public Comunidade findById(Long id) {
        return comunidadeRepository.findOneById(id);
    }

    @Transactional(readOnly = true)
    public List<Comunidade> findByMunicipio(List<Long> id) {
        return comunidadeRepository.findAllByMunicipio_IdInOrderByNome(id);
    }

    @Transactional
    public void delete(Long id) {
        comunidadeRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public List<Comunidade> findAllByFilter(FiltroComunidadeDTO filtro) {
        return customComunidadeRepository.findAllByFilter(filtro);
    }

    public List<Comunidade> findByMunicipioAndAreaAtuacao(List<Long> id) {
        return customComunidadeRepository.findByMunicipioAndAreaAtuacao(id);
    }
}
