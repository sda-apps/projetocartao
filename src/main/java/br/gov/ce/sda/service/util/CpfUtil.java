package br.gov.ce.sda.service.util;

import org.apache.commons.lang.StringUtils;

public class CpfUtil {

	public static String formatarCpf(String cpf) {
		cpf = cpf.replaceAll("[.-]", "");
		
		cpf = StringUtils.leftPad(cpf, 11, "0");
		
		return cpf;
    }
	
	public static String converterCpfParaLong(String cpf) {
		cpf = cpf.replaceAll("[.-]", "");
		
		Long cpfTemp = Long.parseLong(cpf);
		
		return cpfTemp.toString();
    }

}
