package br.gov.ce.sda.service.ater;

import br.gov.ce.sda.config.security.AuthoritiesConstants;
import br.gov.ce.sda.config.security.SecurityUtils;
import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.domain.ater.MetaAtividadesTrimestral;
import br.gov.ce.sda.domain.ater.PlanoTrimestral;
import br.gov.ce.sda.repository.MunicipioRepository;
import br.gov.ce.sda.repository.ater.CustomPlanoTrimestralRepository;
import br.gov.ce.sda.repository.ater.MetaAtividadeTrimestralRepository;
import br.gov.ce.sda.repository.ater.PlanoTrimestralRepository;
import br.gov.ce.sda.service.RegiaoService;
import br.gov.ce.sda.web.rest.dto.MetaAtividadesTrimestralDTO;
import br.gov.ce.sda.web.rest.dto.ater.filtros.FiltroPlanoTrimestralDTO;
import br.gov.ce.sda.web.rest.util.HeaderUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class PlanoTrimestralService {

    private final PlanoTrimestralRepository planoTrimestralRepository;
    private final MetaAtividadeTrimestralRepository metaAtividadeRepository;
    private final CustomPlanoTrimestralRepository customPlanoTrimestralRepository;
    private final RegiaoService regiaoService;
    private final MunicipioRepository municipioRepository;
    private final PlanoTrabalhoAnualService planoTrabalhoAnualService;

    public PlanoTrimestralService(
        PlanoTrimestralRepository planoTrimestralRepository,
        MetaAtividadeTrimestralRepository metaAtividadeRepository,
        CustomPlanoTrimestralRepository customPlanoTrimestralRepository,
        RegiaoService regiaoService,
        MunicipioRepository municipioRepository,
        PlanoTrabalhoAnualService planoTrabalhoAnualService
    ) {
        this.planoTrimestralRepository = planoTrimestralRepository;
        this.metaAtividadeRepository = metaAtividadeRepository;
        this.customPlanoTrimestralRepository = customPlanoTrimestralRepository;
        this.regiaoService = regiaoService;
        this.municipioRepository = municipioRepository;
        this.planoTrabalhoAnualService = planoTrabalhoAnualService;
    }

    @Transactional
    public ResponseEntity<PlanoTrimestral> save(List<MetaAtividadesTrimestral> metasTrimestrais) {

        if (metasTrimestrais.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        PlanoTrimestral planoTrimestral = metasTrimestrais.get(0).getPlanoTrimestral();

        List<PlanoTrimestral> planosTrimestrais = planoTrimestralRepository.findByTrimestreAndPlanoTrabalhoAnual_Id(planoTrimestral.getTrimestre(), planoTrimestral.getPlanoTrabalhoAnual().getId());

        if (!planosTrimestrais.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        planoTrimestral = planoTrimestralRepository.save(planoTrimestral);

        for (MetaAtividadesTrimestral meta : metasTrimestrais) {
            meta.setPlanoTrimestral(planoTrimestral);

            metaAtividadeRepository.save(meta);
        }

        return ResponseEntity.ok().headers(HeaderUtil.sucess("Plano Trimestral salvo com sucesso.")).body(planoTrimestral);
    }

    public void delete(Long id) {
        metaAtividadeRepository.deleteAll(customPlanoTrimestralRepository.findById(id));

        planoTrimestralRepository.deleteById(id);
    }

    @Transactional
    public PlanoTrimestral update(PlanoTrimestral planoTrimestralAtual, List<MetaAtividadesTrimestral> metasTrimestrais) {
        PlanoTrimestral planoTrimestral = planoTrimestralRepository.save(planoTrimestralAtual);

        List<MetaAtividadesTrimestral> oldMetas = customPlanoTrimestralRepository.findById(planoTrimestralAtual.getId());

        Set<Long> oldMetasIds = oldMetas.stream().map(MetaAtividadesTrimestral::getId).collect(Collectors.toSet());
        Set<Long> newMetasIds = metasTrimestrais.stream().map(MetaAtividadesTrimestral::getId).collect(Collectors.toSet());

        oldMetasIds.removeAll(newMetasIds);

        oldMetasIds.forEach(metaAtividadeRepository::deleteById);

        metaAtividadeRepository.saveAll(metasTrimestrais);

        return planoTrimestral;
    }

    public List findAll(FiltroPlanoTrimestralDTO filter) {
        Usuario usuario = SecurityUtils.getCurrentUser();
        List planos = null;

        if (usuario.hasAuthorities(AuthoritiesConstants.USUARIO_ERP)) {
            filter.setRegiaoUsuario(regiaoService.findRegiaoBy(usuario));
            planos = getPlanosTrimestraisForErpUsers(filter);
        } else if (usuario.hasAuthorities(AuthoritiesConstants.USUARIO_UGP) || usuario.hasAuthorities(AuthoritiesConstants.ADMIN)) {
            planos = getPlanosTrimestraisForUgpUsers(filter);
        }

        return planos;
    }

    private List getPlanosTrimestraisForErpUsers(FiltroPlanoTrimestralDTO filter) {
        return customPlanoTrimestralRepository.findAllForErpUser(filter);
    }

    private List getPlanosTrimestraisForUgpUsers(FiltroPlanoTrimestralDTO filter) {
        return customPlanoTrimestralRepository.findAllForUgpUser(filter);
    }

    @Transactional
    public void copy(PlanoTrimestral planoTrimestral) throws CloneNotSupportedException {
        PlanoTrimestral planoTrimestralCloned = planoTrimestral.clone();

        planoTrimestralCloned.setId(null);
        planoTrimestralCloned.setTrimestre(planoTrimestral.getTrimestre()+1);
        planoTrimestralCloned.setPrimeiroMes(planoTrimestral.getTerceiroMes().plusMonths(1));
        planoTrimestralCloned.setTerceiroMes(planoTrimestral.getTerceiroMes().plusMonths(3));
        planoTrimestralCloned.setStatus("1");
        planoTrimestralCloned.setFileName(null);
        planoTrimestralCloned.setFilePath(null);
        planoTrimestralCloned.setFileStatus(null);

        PlanoTrimestral planoTrimestralSaved = planoTrimestralRepository.save(planoTrimestralCloned);

        List<MetaAtividadesTrimestral> metas = customPlanoTrimestralRepository.findById(planoTrimestral.getId());

        metas.forEach(meta -> {
            try {
                MetaAtividadesTrimestral metaCloned = meta.clone();
                metaCloned.setId(null);
                metaCloned.setPlanoTrimestral(planoTrimestralSaved);

                metaAtividadeRepository.save(metaCloned);
            } catch (CloneNotSupportedException e) {
                throw new RuntimeException("Erro ao copiar as metas do plano trimestral.");
            }
        });

    }

    public List<MetaAtividadesTrimestralDTO> findById(Long id) {
        List<MetaAtividadesTrimestral> metas = customPlanoTrimestralRepository.findById(id);

        return metas.stream()
            .map(MetaAtividadesTrimestralDTO::mapperTo)
            .map(dto -> {
                dto.getMunicipio().getUf().setMunicipios(
                    municipioRepository.findAllByUf_IdOrderByNomeAsc(
                        dto.getMunicipio().getUf().getId())
                );

                dto.getMunicipio().setComunidades(
                    planoTrabalhoAnualService.findAllComunidadeByMunicipioAndPta(
                        dto.getMunicipio().getId(),
                        dto.getPlanoTrimestral().getPlanoTrabalhoAnual().getId())
                );

                return dto;
            }).collect(Collectors.toList());
    }

    public PlanoTrimestral updateSummary(PlanoTrimestral planoTrimestral, PlanoTrimestral planoTrimestralAtual) {
        planoTrimestralAtual.setFileName(planoTrimestral.getFileName());
        planoTrimestralAtual.setFilePath(planoTrimestral.getFilePath());
        planoTrimestralAtual.setFileStatus(planoTrimestral.getFileStatus());

        return planoTrimestralRepository.save(planoTrimestralAtual);
    }
}
