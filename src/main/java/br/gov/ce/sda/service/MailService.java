package br.gov.ce.sda.service;

import javax.inject.Inject;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Service
public class MailService {

	private final Logger log = LoggerFactory.getLogger(MailService.class);

	@Inject
	private JavaMailSender mailSender;

	@Inject
	private Environment environment;

	@Inject
    private SpringTemplateEngine templateEngine;

	@Async
	public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
			message.setTo(to);
			message.setFrom(environment.getProperty(MailConstants.MAIL_FROM));
			message.setSubject(subject);
			message.setText(content, isHtml);

			message.addInline(MailConstants.LOGO_COMPLETA, new ClassPathResource(MailConstants.PATH_LOGO_COMPLETA), MailConstants.PNG_MIME);
	        message.addInline(MailConstants.LOGO_SECRETARIA, new ClassPathResource(MailConstants.PATH_LOGO_SECRETARIA), MailConstants.PNG_MIME);

			mailSender.send(mimeMessage);
		} catch (Exception e) {
			log.warn(e.getMessage());
		}
	}

	@Async
	public void sendConviteEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
			message.setTo(to);
			message.setFrom(environment.getProperty(MailConstants.MAIL_FROM));
			message.setSubject(subject);
			message.setText(content, isHtml);

			message.addInline(MailConstants.LOGO_COMPLETA, new ClassPathResource(MailConstants.PATH_LOGO_COMPLETA), MailConstants.PNG_MIME);
			message.addInline(MailConstants.IMG_CONVITE_EMAIL_TOPO, new ClassPathResource(MailConstants.PATH_IMG_CONVITE_EMAIL_TOPO), MailConstants.PNG_MIME);
	        message.addInline(MailConstants.IMG_CONVITE_EMAIL_INFERIOR, new ClassPathResource(MailConstants.PATH_IMG_CONVITE_EMAIL_INFERIOR), MailConstants.PNG_MIME);

	        message.addInline(MailConstants.IMG_BARRA_CINZA, new ClassPathResource(MailConstants.PATH_IMG_BARRA_CINZA), MailConstants.PNG_MIME);

			mailSender.send(mimeMessage);
		} catch (Exception e) {
			log.warn(e.getMessage());
		}
	}

	public void sendEmailFeedback(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
			message.setTo(to);
			message.setFrom(environment.getProperty(MailConstants.MAIL_FROM));
			message.setSubject(subject);
			message.setText(content, isHtml);

			message.addInline(MailConstants.LOGO_COMPLETA, new ClassPathResource(MailConstants.PATH_LOGO_COMPLETA), MailConstants.PNG_MIME);
	        message.addInline(MailConstants.LOGO_SECRETARIA, new ClassPathResource(MailConstants.PATH_LOGO_SECRETARIA), MailConstants.PNG_MIME);
	        message.addInline(MailConstants.IMG_BARRA_CINZA, new ClassPathResource(MailConstants.PATH_IMG_BARRA_CINZA), MailConstants.PNG_MIME);
			mailSender.send(mimeMessage);
		} catch (Exception e) {
			log.warn(e.getMessage());
		}
	}

	@Async
    public void sendPasswordResetMail(String nome, String email, String chaveReativacao, String baseUrl) {
        Context context = new Context();
        context.setVariable(MailConstants.NOME_COMPLETO, nome);
        context.setVariable(MailConstants.CHAVE_REATIVACAO, chaveReativacao);
        context.setVariable(MailConstants.BASE_URL, baseUrl);
        String htmlContent = templateEngine.process(MailConstants.TEMPLATE_PASSWORD_RESET_EMAIL, context);
        String subject = MailConstants.ASSUNTO_RECUPERAÇÃO_DE_SENHA;
        sendEmailFeedback(email, subject, htmlContent, true, true);
    }

	@Async
	public void sendSignUpEmail(String nome, String email, String chaveAtivacao, String baseUrl) {
		Context context = new Context();
		context.setVariable(MailConstants.CHAVE_ATIVACAO, chaveAtivacao);
		context.setVariable(MailConstants.BASE_URL, baseUrl);
		String htmlContent = templateEngine.process(MailConstants.TEMPLATE_SIGNUP_EMAIL, context);
		String subject = MailConstants.ASSUNTO_SIGNUP;
		sendConviteEmail(email, subject, htmlContent, true, true);
	}

	@Async
	public void sendFinishSignUpEmail(String username, String password, String email, String baseUrl) {
		Context context = new Context();
		context.setVariable(MailConstants.USERNAME, username);
		context.setVariable(MailConstants.PASSWORD, password);
		context.setVariable(MailConstants.BASE_URL, baseUrl);
		String htmlContent = templateEngine.process(MailConstants.TEMPLATE_FINISH_SIGNUP_EMAIL, context);
		String subject = MailConstants.ASSUNTO_FINISH_SIGNUP;
		sendEmail(email, subject, htmlContent, true, true);
	}

    @Async
    public void sendFinishPasswordResetEmail(String username, String password, String email, String baseUrl) {
        Context context = new Context();
        context.setVariable(MailConstants.USERNAME, username);
        context.setVariable(MailConstants.PASSWORD, password);
        context.setVariable(MailConstants.BASE_URL, baseUrl);
        String htmlContent = templateEngine.process(MailConstants.TEMPLATE_FINISH_PASSWORD_RESET_EMAIL, context);
        String subject = MailConstants.ASSUNTO_ALTERACAO_DE_SENHA;
        sendEmail(email, subject, htmlContent, true, true);
    }

	@Async
	public void sendFeedbackEmail(String nome, String email) {
		Context context = new Context();
        context.setVariable(MailConstants.NOME_COMPLETO, nome);
		String htmlContent = templateEngine.process(MailConstants.TEMPLATE_FEEDBACK_EMAIL, context);
		String subject = MailConstants.ASSUNTO_FEEDBACK;
		sendEmailFeedback(email, subject, htmlContent, true, true);
	}

	private class MailConstants {

		public static final String ASSUNTO_RECUPERAÇÃO_DE_SENHA = "Portal da Agricultura Familiar - Recuperação de Senha";
        public static final String ASSUNTO_ALTERACAO_DE_SENHA = "Portal da Agricultura Familiar - Alteração de Senha";
		public static final String ASSUNTO_SIGNUP = "Convite Portal da Agricultura Familiar";
		public static final String ASSUNTO_FINISH_SIGNUP = "Cadastro";
		public static final String ASSUNTO_FEEDBACK = "Portal da Agricultura Familiar - Avaliação Recebida";


		public static final String TEMPLATE_PASSWORD_RESET_EMAIL = "passwordResetEmail";
		public static final String TEMPLATE_SIGNUP_EMAIL = "signupEmail";
		public static final String TEMPLATE_FINISH_SIGNUP_EMAIL = "finishSignupEmail";
        public static final String TEMPLATE_FINISH_PASSWORD_RESET_EMAIL = "finishPasswordResetEmail";
		public static final String TEMPLATE_FEEDBACK_EMAIL = "feedbackEmail";

		public static final String CHAVE_REATIVACAO = "chaveReativacao";
		public static final String CHAVE_ATIVACAO = "chaveAtivacao";
		public static final String NOME_COMPLETO = "nomeCompleto";
		public static final String USERNAME = "username";
		public static final String PASSWORD = "password";

		public static final String BASE_URL = "baseUrl";

		public static final String LOGO_SECRETARIA = "logo-secretaria";
		public static final String LOGO_COMPLETA = "logo-completa";
		public static final String IMG_CONVITE_EMAIL_TOPO = "conviteEmailTopo";
		public static final String IMG_CONVITE_EMAIL_INFERIOR = "conviteEmailInferior";
		public static final String IMG_BARRA_CINZA = "barraCinza";

	    public static final String PATH_LOGO_COMPLETA = "mails/images/logoCompleta.png";
	    public static final String PATH_LOGO_SECRETARIA = "mails/images/logoSecretaria.png";

	    public static final String PATH_IMG_CONVITE_EMAIL_TOPO = "mails/images/conviteEmailTopo.png";
	    public static final String PATH_IMG_CONVITE_EMAIL_INFERIOR = "mails/images/conviteEmailInferior.png";

	    public static final String PATH_IMG_BARRA_CINZA = "mails/images/barraCinza.png";

	    public static final String PNG_MIME = "image/png";
	    public static final String MAIL_FROM = "mail.from";

	}

}
