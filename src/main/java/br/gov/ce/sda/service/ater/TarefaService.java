package br.gov.ce.sda.service.ater;

import br.gov.ce.sda.domain.ater.Tarefa;
import br.gov.ce.sda.exceptions.StorageException;
import br.gov.ce.sda.repository.ater.CustomTarefaRepository;
import br.gov.ce.sda.repository.ater.TarefaRepository;
import br.gov.ce.sda.service.ida.DataService;
import br.gov.ce.sda.web.rest.dto.ater.FiltroTarefaDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TarefaService {

    private final CustomTarefaRepository customTarefaRepository;
    private final TarefaRepository tarefaRepository;
    private final DataService dataService;

    public TarefaService(
        CustomTarefaRepository customTarefaRepository,
        TarefaRepository tarefaRepository,
        DataService dataService
    ) {
        this.customTarefaRepository = customTarefaRepository;
        this.tarefaRepository = tarefaRepository;
        this.dataService = dataService;
    }

    public List<Tarefa> findByFilters(FiltroTarefaDTO filters) {
        return customTarefaRepository.findByFilters(filters);
    }

    public long findTotalByFilters(FiltroTarefaDTO filtro) {
        return customTarefaRepository.findTotalByFilters(filtro);
    }

    public Tarefa findByIdComplemento(Long id) throws StorageException {
        return customTarefaRepository.findByIdComplemento(id);
    }

    public Tarefa findById(Long id) {
        return customTarefaRepository.findById(id);
    }

    @Transactional
    public Tarefa evaluateTask(Tarefa tarefa, Tarefa tarefaAtual) {
        tarefaAtual.setStatus(tarefa.getStatus());

        if (tarefaAtual.getStatus().equals(Tarefa.Status.ACEITO.getStatus())) {
            tarefaAtual.setRecusado(Boolean.FALSE);
        } else {
            tarefaAtual.setRecusado(tarefa.getRecusado());
        }

        //dataService.enviarInformacoesATERParaIDA(tarefaAtual);

        return tarefaRepository.save(tarefaAtual);
    }

    public Tarefa setAnexo(Tarefa tarefa, Tarefa tarefaAtual) {

        if (!tarefa.getAnexo().isEmpty()) {
            tarefaAtual.setAnexo(tarefa.getAnexo());
        }

        return tarefaRepository.save(tarefaAtual);
    }

    public Tarefa save(Tarefa tarefa) {
        return tarefaRepository.save(tarefa);
    }
}
