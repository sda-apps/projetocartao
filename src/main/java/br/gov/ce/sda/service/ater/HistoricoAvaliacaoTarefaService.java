package br.gov.ce.sda.service.ater;

import br.gov.ce.sda.domain.ater.HistoricoAvaliacaoTarefa;
import br.gov.ce.sda.repository.ater.CustomHistoricoAvaliacaoTarefaRepository;
import br.gov.ce.sda.repository.ater.HistoricoAvaliacaoTarefaRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service
public class HistoricoAvaliacaoTarefaService {

    @Inject
    private CustomHistoricoAvaliacaoTarefaRepository customHistoricoAvaliacaoTarefaRepository;

    @Inject
    private HistoricoAvaliacaoTarefaRepository historicoAvaliacaoTarefaRepository;

    public List<HistoricoAvaliacaoTarefa> findByTarefaId(long id) {
        return customHistoricoAvaliacaoTarefaRepository.findByTarefaId(id);
    }

    public HistoricoAvaliacaoTarefa save(HistoricoAvaliacaoTarefa historicoAvaliacaoTarefa) {
        return historicoAvaliacaoTarefaRepository.save(historicoAvaliacaoTarefa);
    }
}
