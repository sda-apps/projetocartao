package br.gov.ce.sda.service.ida.converters;

import br.gov.ce.sda.domain.ater.Tarefa;
import br.gov.ce.sda.web.rest.dto.ida.IndicadorPlanejamentoExecucao;
import br.gov.ce.sda.web.rest.dto.ida.IndicadorProjeto;
import br.gov.ce.sda.web.rest.dto.ida.IndicadorProjetoBeneficiario;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashSet;
import java.util.Set;

import static java.util.Objects.isNull;
import static org.apache.commons.lang3.StringUtils.trim;

public class TarefaParser implements IndicadorProjetoParcer {

    private final Tarefa tarefa;

    TarefaParser(Tarefa tarefa) {
        this.tarefa = tarefa;
    }

    @Override
    public IndicadorProjeto parse() {
        Set<IndicadorProjetoBeneficiario> indicadorProjetoBeneficiarios = new HashSet<>();
        Set<IndicadorPlanejamentoExecucao> indicadorPlanejamentoExecucoes = new HashSet<>();

        IndicadorProjeto ip = IndicadorProjeto.builder()
            .ano(tarefa.getCriacao().getYear())
            .dataImportacao(LocalDateTime.now())
            .latitude(tarefa.getCoordenada().getLatitude())
            .longitude(tarefa.getCoordenada().getLongitude())
            .indicadorProjetoSituacaoCodigo(SITUACAO_CONCLUIDO)
            .indicador(INDICADOR_CADASTRO_FAMILIA)//TODO ? // Cadastro de familia (526) ou visita técnica (?)
            .solicitado(CNPJ_RESPONSAVEL_INFORMACAO)//TODO ? // CNPJ - responsável pela informação - saber PF ou SDA (07954563000168)
            .solicitante(CNPJ_RESPONSAVEL_INFORMACAO)//TODO ? // CNPJ - responsável pela informação - saber PF ou SDA (07954563000168)
            .tipoIndicador(INDICADOR_BENEFICIARIOS)
            .projetoCodigo(PROJETO_PAULO_FREIRE)
            .unidadeMedidaCodigo(UNIDADE_MEDIDA_UNIDADE)
            .fonteInformacaoCodigo(FONTE_INFORMACAO_PAULO_FREIRE)
            .publico(NAO)
            .paisCodigo(isStringNull(tarefa.getAgricultor().getPessoaFisica().getNacionalidade().getCodigoIbge()))
            .ufCodigo(isStringNull(tarefa.getAgricultor().getPessoaFisica().getPessoa().getMunicipio().getUf().getCodigoIbge()))
            .municipioCodigo(isStringNull(tarefa.getAgricultor().getPessoaFisica().getPessoa().getMunicipio().getCodigoIbge()))
            .distritoCodigo(isStringNull(tarefa.getAgricultor().getPessoaFisica().getPessoa().getDistrito().getCodigoIbge()))
            .localidadeCodigo(isStringNull(tarefa.getAgricultor().getPessoaFisica().getPessoa().getLocalidade().getCodigoSda()))
            .build();

        IndicadorProjetoBeneficiario ipb = IndicadorProjetoBeneficiario.builder()
            .nome(tarefa.getAgricultor().getPessoaFisica().getPessoa().getNome())
            .cpfCnpj(isStringNull(tarefa.getAgricultor().getPessoaFisica().getCpf()))
            .beneficiario(isStringNull(tarefa.getAgricultor().getPessoaFisica().getCpf()))
            .sexo(isStringNull(tarefa.getAgricultor().getPessoaFisica().getSexo()))
            .dataNascimento(tarefa.getAgricultor().getPessoaFisica().getDataNascimento().toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
            .email(isStringNull(tarefa.getAgricultor().getPessoaFisica().getPessoa().getEmail()))
            .cep(isNull(tarefa.getAgricultor().getPessoaFisica().getPessoa().getCep()) ? "00000000" : tarefa.getAgricultor().getPessoaFisica().getPessoa().getCep())
            .endereco(isStringNull(tarefa.getAgricultor().getPessoaFisica().getPessoa().getLogradouro()))
            .enderecoComp(isStringNull(tarefa.getAgricultor().getPessoaFisica().getPessoa().getComplemento()))
            .enderecoNumero(isStringNull(tarefa.getAgricultor().getPessoaFisica().getPessoa().getNumero()))
            .latitude(isStringNull(tarefa.getCoordenada().getLatitude()))
            .longitude(isStringNull(tarefa.getCoordenada().getLongitude()))
            .paisCodigo(isStringNull(trim(tarefa.getAgricultor().getPessoaFisica().getNacionalidade().getCodigoIbge())))
            .municipioCodigo(isStringNull(tarefa.getAgricultor().getPessoaFisica().getPessoa().getMunicipio().getCodigoIbge()))
            .ufCodigo(isStringNull(trim(tarefa.getAgricultor().getPessoaFisica().getPessoa().getMunicipio().getUf().getCodigoIbge())))
            .siglaUf(isStringNull(tarefa.getAgricultor().getPessoaFisica().getPessoa().getMunicipio().getUf().getSigla()))
            .distritoCodigo(isStringNull(tarefa.getAgricultor().getPessoaFisica().getPessoa().getDistrito().getCodigoIbge()))
            .localidadeCodigo(isStringNull(tarefa.getAgricultor().getPessoaFisica().getPessoa().getLocalidade().getCodigoSda()))
            .nomePai(isStringNull(tarefa.getAgricultor().getPessoaFisica().getNomePai()))
            .nis(isStringNull(tarefa.getAgricultor().getPessoaFisica().getNis()))
            .tipo(PESSOA_FISICA)
            .executadoQuantidade(1.0)
            .build();

        IndicadorPlanejamentoExecucao ipe = IndicadorPlanejamentoExecucao.builder()
            .responsavel(isStringNull(tarefa.getAgricultor().getPessoaFisica().getCpf()))
            .tipo(PESSOA_FISICA)
            .quantidade(1.0)
            //.valor()
            .data(tarefa.getDataRealizacao().toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
            .situacao("3") //não requerido
            //.observacao("")//não requerido
            .indicadorItem(INDICADOR_ITEM_CADASTRO_FAMILIA)
            //.valorUnitario()
            .dataFinal(tarefa.getDataRealizacao().toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
            .build();

        indicadorProjetoBeneficiarios.add(ipb);
        indicadorPlanejamentoExecucoes.add(ipe);
        ip.setProjetoBeneficiarios(indicadorProjetoBeneficiarios);
        ip.setPlanejamentoExecucoes(indicadorPlanejamentoExecucoes);

        return ip;
    }

}
