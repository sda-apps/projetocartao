package br.gov.ce.sda.service;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.lesstif.jira.issue.Issue;
import com.lesstif.jira.issue.IssueFields;
import com.lesstif.jira.services.IssueService;

import br.gov.ce.sda.web.rest.dto.FeedbackDTO;

@Service
public class JiraService {
	
	private static final String FEEDBACK_PROJETO_CARTAO = "FPC";
	private static final String RATING_FIELD_ID = "customfield_10500";
	private static final String EMAIL_FIELD_ID = "customfield_10800";
	private static final String INFO_FIELD_ID = "customfield_10802";
	
	@Inject
	private IssueService jiraIssueService;

	public void createIssue(FeedbackDTO feedback) throws Exception {
        Issue issue = new Issue();

        IssueFields fields = new IssueFields();
        Map<String, Object> customField = new HashMap<>();
        customField.put(RATING_FIELD_ID, feedback.getRating().toString());
        customField.put(EMAIL_FIELD_ID, feedback.getEmail());
        customField.put(INFO_FIELD_ID, feedback.getInformacoes());
        
		fields.setProjectKey(FEEDBACK_PROJETO_CARTAO)
              .setSummary(feedback.getUserName())
              .setIssueTypeName(feedback.getType())
              .setCustomfield(customField)
              .setDescription(feedback.getMessage());
        
        issue.setFields(fields);

        jiraIssueService.createIssue(issue);
    }
    
}
