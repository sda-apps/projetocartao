package br.gov.ce.sda.service;

import br.gov.ce.sda.domain.ater.Contrato;
import br.gov.ce.sda.repository.ContratoRepository;
import br.gov.ce.sda.repository.CustomContratoRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;
import java.util.Set;

@Service
public class ContratoService {

    @Inject
    private ContratoRepository contratoRepository;

    @Inject
    private CustomContratoRepository customContratoRepository;


    @Transactional
    public Contrato save(Contrato contrato) {
        Contrato savedContrato = contratoRepository.save(contrato);
        return savedContrato;
    }

    @Transactional(readOnly = true)
    public List<Contrato> findAll() {
        return contratoRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Contrato findById(Long id) {
        return customContratoRepository.findById(id);
    }

    @Transactional(readOnly = true)
    public Set<Contrato> findByEmpresaId(List<Long> id) {
        return customContratoRepository.findByEmpresaId(id);
    }

    @Transactional
    public void update(Contrato currentContrato) {
        contratoRepository.save(currentContrato);
    }

    @Transactional
    public void delete(Long id) {
        contratoRepository.deleteById(id);
    }
}
