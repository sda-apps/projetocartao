package br.gov.ce.sda.service;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.gov.ce.sda.domain.Agricultor;
import br.gov.ce.sda.domain.mapa.AreaGeorreferenciada;
import br.gov.ce.sda.repository.AgricultorRepository;
import br.gov.ce.sda.repository.PoligonoRepository;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class AgricultorService {

    @Inject
    private AgricultorRepository agricultorRepository;

    @Inject
    private PoligonoRepository poligonoRepository;

    @Transactional(readOnly = true)
    public Agricultor getAgricultorComAreasGeograficas(Long id) {
        Agricultor agricultor = agricultorRepository.findById(id).get();

        if (agricultor != null) {
            for (AreaGeorreferenciada ag : agricultor.getAreasGeorreferenciadas()) {
                ag.setPoligono(poligonoRepository.findByAreaGeorreferenciada_Id(ag.getId()));
            }
        }

        return agricultor;
    }

}
