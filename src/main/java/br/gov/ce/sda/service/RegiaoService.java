package br.gov.ce.sda.service;

import br.gov.ce.sda.domain.Municipio;
import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.domain.ater.Regiao;
import br.gov.ce.sda.repository.UserRepository;
import br.gov.ce.sda.repository.ater.CustomAbrangenciaRegiaoRepository;
import br.gov.ce.sda.repository.ater.RegiaoRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;

@Service
public class RegiaoService {

    private final CustomAbrangenciaRegiaoRepository customAbrangenciaRegiaoRepository;
    private final UserRepository userRepository;
    private final RegiaoRepository regiaoRepository;

    public RegiaoService(
        CustomAbrangenciaRegiaoRepository customAbrangenciaRegiaoRepository,
        UserRepository userRepository,
        RegiaoRepository regiaoRepository
    ) {
        this.customAbrangenciaRegiaoRepository = customAbrangenciaRegiaoRepository;
        this.userRepository = userRepository;
        this.regiaoRepository = regiaoRepository;
    }

    @Transactional(readOnly = true)
    public Optional<Regiao> findBy(String nome) {
        return regiaoRepository.findAllByNomeIgnoreCaseContainingOrderByNome(nome)
            .stream()
            .findFirst();
    }

    @Transactional(readOnly = true)
    public Regiao findRegiaoBy(Usuario usuario) {
        requireNonNull(usuario, "O campo usuário é obrigatório.");

        usuario = userRepository.findById(usuario.getId()).get();
        Regiao regiao =  null;
        if (nonNull(usuario.getUsuarioAvancado()) &&
            nonNull(usuario.getUsuarioAvancado().getPessoaFisica()) &&
                nonNull(usuario.getUsuarioAvancado().getPessoaFisica().getPessoa())) {
            Municipio municipio = usuario.getUsuarioAvancado().getPessoaFisica().getPessoa().getMunicipio();
            regiao = customAbrangenciaRegiaoRepository.findRegiaoByMunicipio(municipio.getId());
        }

        return regiao;
    }
}
