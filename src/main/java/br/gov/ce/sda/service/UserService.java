package br.gov.ce.sda.service;

import br.gov.ce.sda.MessageKey;
import br.gov.ce.sda.Messages;
import br.gov.ce.sda.config.security.AuthoritiesConstants;
import br.gov.ce.sda.config.security.SecurityUtils;
import br.gov.ce.sda.domain.PessoaFisica;
import br.gov.ce.sda.domain.UsuarioAvancado;
import br.gov.ce.sda.domain.acesso.Autoridade;
import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.repository.*;
import br.gov.ce.sda.service.ater.PlanoTrabalhoAnualService;
import br.gov.ce.sda.service.util.RandomUtil;
import br.gov.ce.sda.web.rest.dto.FiltroUsuarioDTO;
import br.gov.ce.sda.web.rest.dto.ManagedUserDTO;
import br.gov.ce.sda.web.rest.dto.ResponseMessage;
import br.gov.ce.sda.web.rest.dto.UsuarioDTO;
import br.gov.ce.sda.web.rest.util.HttpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.*;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Inject
    private UserRepository userRepository;

    @Inject
    private CustomUserRepository customUserRepository;

    @Inject
    private AuthorityRepository authorityRepository;

    @Inject
    private CustomAgricultorRepository customAgricultorRepository;

    @Inject
    private CustomUsuarioAvancadoRepository customUsuarioAvancadoRepository;

    @Inject
    private MailService mailService;

    @Inject
    private UsuarioAvancadoRepository usuarioAvancadoRepository;

    @Inject
    private CustomPessoaFisicaRepository customPessoaFisicaRepository;

    @Inject
    private PlanoTrabalhoAnualService planoTrabalhoAnualService;

    public Optional<Usuario> activateRegistration(String chave) {
        log.debug("Activating user for activation key {}", chave);
        userRepository.findOneByChaveAtivacao(chave)
            .map(usuario -> {
                // activate given user for the registration key.
                usuario.setStatus(Usuario.Status.ATIVADO);
                usuario.setChaveAtivacao(null);
                userRepository.save(usuario);
                log.debug("Activated user: {}", usuario);
                return usuario;
            });
        return Optional.empty();
    }

    public Optional<Usuario> completePasswordReset(String novaSenha, String chave) {
        log.debug("Reset user password for reset key {}", chave);

        return userRepository.findOneByChaveReativacao(chave)
            .filter(user -> {
                LocalDateTime oneDayAgo = LocalDateTime.now().minusHours(24);
                return user.getResetDate().isAfter(oneDayAgo);
            })
            .map(user -> {
                user.setSenha(passwordEncoder.encode(novaSenha));
                user.setChaveReativacao(null);
                //user.setResetDate(null);
                userRepository.save(user);
                String login = user.getLogin();
                String baseUrl = HttpUtil.PORTAL_AGRICULTURA_FAMILIAR_URL;
                String emailUsuario = user.getUsuarioAvancado().getEmailInstitucional();
                mailService.sendFinishPasswordResetEmail(login, novaSenha, emailUsuario, baseUrl);
                return user;
            });
    }

    public Optional<Usuario> completeSignUp(String chave, String login, String senha, String baseUrl) {
        return userRepository.findOneByChaveAtivacao(chave).map(usuario -> {
            if (usuario.getAutoridades().isEmpty()) {
                Autoridade authority = authorityRepository.findById("ROLE_TECNICO").get();
                Set<Autoridade> authorities = new HashSet<>();
                authorities.add(authority);
                usuario.setAutoridades(authorities);
            }

            usuario.setLogin(login);
            usuario.setSenha(passwordEncoder.encode(senha));
            usuario.setChaveAtivacao(null);
            usuario.setStatus(Usuario.Status.ATIVADO);
            userRepository.save(usuario);

            String emailUsuario = customUserRepository.bucarEmailUsuario(usuario.getId());
            mailService.sendFinishSignUpEmail(login, senha, emailUsuario, baseUrl);

            return usuario;
        });
    }

    public Optional<Usuario> requestPasswordReset(String emailInstitucional, String baseUrl) {
        Usuario usuario = customUserRepository.buscarUsuarioPorEmailInstitucional(emailInstitucional);
        if (Objects.nonNull(usuario) && usuario.isEnabled()) {
            usuario.setChaveReativacao(RandomUtil.generateResetKey());
            usuario.setResetDate(LocalDateTime.now());
            userRepository.save(usuario);

            String nome = usuario.getUsuarioAvancado().getPessoaFisica().getPessoa().getNome();
            String chaveReativacao = usuario.getChaveReativacao();

            mailService.sendPasswordResetMail(nome, emailInstitucional, chaveReativacao, baseUrl);
        }

        return Optional.ofNullable(usuario);
    }

    public Usuario createUserInformation(String login, String senha) {

        Usuario novoUsuario = new Usuario();
        Autoridade authority = authorityRepository.findById("ROLE_USER").get();
        Set<Autoridade> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode(senha);
        novoUsuario.setLogin(login);
        // new user gets initially a generated password
        novoUsuario.setSenha(encryptedPassword);
        // new user is not active
        novoUsuario.setStatus(Usuario.Status.INATIVO);
        // new user gets registration key
        novoUsuario.setChaveAtivacao(RandomUtil.generateActivationKey());
        authorities.add(authority);
        novoUsuario.setAutoridades(authorities);
        userRepository.save(novoUsuario);
        log.debug("Created Information for User: {}", novoUsuario);
        return novoUsuario;
    }

    public Usuario createUser(ManagedUserDTO managedUserDTO) {
        Usuario usuario = new Usuario();
        usuario.setLogin(managedUserDTO.getLogin());
        //usuario.setNome(managedUserDTO.getNome());
        //usuario.setSobrenome(managedUserDTO.getSobrenome());
        //usuario.setEmail(managedUserDTO.getEmail());
        if (!managedUserDTO.getAuthorities().isEmpty()) {
            Set<Autoridade> authorities = new HashSet<>();
            managedUserDTO.getAuthorities().stream().forEach(
                authority -> authorities.add(authorityRepository.findById(authority).get())
            );
            usuario.setAutoridades(authorities);
        }
        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        usuario.setSenha(encryptedPassword);
        usuario.setChaveReativacao(RandomUtil.generateResetKey());
        //user.setResetDate(ZonedDateTime.now());
        usuario.setStatus(Usuario.Status.ATIVADO);
        userRepository.save(usuario);
        log.debug("Created Information for User: {}", usuario);
        return usuario;
    }

    @Transactional
    public Usuario cadastrarUsuairo(Usuario usuario) {
        String cpf = usuario.getUsuarioAvancado().getPessoaFisica().getCpf();
        Usuario usuarioExistente = customUserRepository.buscarUsuarioPorCpf(cpf);
        UsuarioAvancado usuarioAvancado = customUsuarioAvancadoRepository.buscarPorCpf(cpf);
        PessoaFisica pessoaFisica = customPessoaFisicaRepository.buscarPessoaPorCpf(cpf);

        if (isNull(usuarioExistente) && nonNull(usuarioAvancado)) {
            usuarioAvancado.setEmpresa(usuario.getUsuarioAvancado().getEmpresa());
            usuarioAvancado.setEmailInstitucional(usuario.getUsuarioAvancado().getEmailInstitucional());

            usuarioAvancado = usuarioAvancadoRepository.save(usuarioAvancado);

            usuario.setUsuarioAvancado(usuarioAvancado);
        }

        if (isNull(usuarioExistente) && isNull(usuarioAvancado) && nonNull(pessoaFisica)) {
            usuarioAvancado = new UsuarioAvancado();

            usuarioAvancado.setPessoaFisica(pessoaFisica);
            usuarioAvancado.setEmpresa(usuario.getUsuarioAvancado().getEmpresa());
            usuarioAvancado.setEmailInstitucional(usuario.getUsuarioAvancado().getEmailInstitucional());

            usuarioAvancado = usuarioAvancadoRepository.save(usuarioAvancado);

            usuario.setUsuarioAvancado(usuarioAvancado);
        }

        usuario.setLogin(RandomUtil.generateLogin());
        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        usuario.setSenha(encryptedPassword);
        usuario.setChaveAtivacao(RandomUtil.generateActivationKey());
        usuario.setResetDate(LocalDateTime.now());
        usuario.setStatus(Usuario.Status.INATIVO);
        usuario = userRepository.save(usuario);

        enviarSignUpEmail(usuario);

        return usuario;
    }

    @Transactional
    public void reenviarSignUpEmail(Usuario usuario) {
        Usuario currentUser = userRepository.findById(usuario.getId()).get();
        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        currentUser.setLogin(RandomUtil.generateLogin());
        currentUser.setSenha(encryptedPassword);
        currentUser.setChaveAtivacao(RandomUtil.generateActivationKey());
        currentUser.setResetDate(LocalDateTime.now());
        currentUser.setStatus(Usuario.Status.INATIVO);
        userRepository.save(currentUser);
        enviarSignUpEmail(currentUser);
    }

    public void enviarSignUpEmail(String baseUrl) {
        String nome = null;
        String email = null;
        String chaveAtivacao = null;

        List<UsuarioAvancado> usuarioAvancadoSemLogin = customUsuarioAvancadoRepository.buscarTodosUsuarioAvancadoSemLogin();

        for (UsuarioAvancado usuarioAvancado : usuarioAvancadoSemLogin) {
            email = usuarioAvancado.getEmailInstitucional();
            if (!StringUtils.isEmpty(email) &&
                (email.matches("[a-zA-Z0-9\\.]*@(sda.ce.gov.br)") ||
                    email.matches("[a-zA-Z0-9\\.]*@(idace.ce.gov.br)") ||
                    email.matches("[a-zA-Z0-9\\.]*@(ematerce.ce.gov.br)") ||
                    email.equals("oscar.dalva@gmail.com"))) {

                Usuario usuario = criarUsuarioInativo(usuarioAvancado);
                nome = usuario.getUsuarioAvancado().getPessoaFisica().getPessoa().getNome();
                chaveAtivacao = usuario.getChaveAtivacao();
                mailService.sendSignUpEmail(nome, email, chaveAtivacao, baseUrl);
            }
        }
    }

    public void enviarSignUpEmail(List<Long> usuariosId, String baseUrl) {
        String nome = null;
        String email = null;
        String chaveAtivacao = null;

        List<Usuario> usuarios = new ArrayList<Usuario>();

        for (Long id : usuariosId) {
            Usuario usuario = userRepository.findById(id).get();

            if (!usuario.isEnabled() && usuario.getChaveAtivacao() != null) {
                usuario.setChaveAtivacao(RandomUtil.generateActivationKey());
                usuario.setResetDate(LocalDateTime.now());
                usuario.setStatus(Usuario.Status.INATIVO);
                usuarios.add(usuario);
            }
        }

        for (Usuario usuario : usuarios) {
            nome = usuario.getUsuarioAvancado().getPessoaFisica().getPessoa().getNome();
            email = usuario.getUsuarioAvancado().getEmailInstitucional();
            chaveAtivacao = usuario.getChaveAtivacao();
            mailService.sendSignUpEmail(nome, email, chaveAtivacao, baseUrl);
        }
    }

    public void enviarSignUpEmail(Usuario usuario) {
        String nome = usuario.getUsuarioAvancado().getPessoaFisica().getPessoa().getNome();
        String email = usuario.getUsuarioAvancado().getEmailInstitucional();
        String chaveAtivacao = usuario.getChaveAtivacao();
        String baseUrl = HttpUtil.PORTAL_AGRICULTURA_FAMILIAR_URL;
        mailService.sendSignUpEmail(nome, email, chaveAtivacao, baseUrl);
    }

    public Usuario criarUsuarioInativo(UsuarioAvancado usuarioAvancado) {
        Usuario usuario = new Usuario();
        usuario.setLogin(RandomUtil.generateLogin());
        usuario.setUsuarioAvancado(usuarioAvancado);
        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        usuario.setSenha(encryptedPassword);
        usuario.setChaveAtivacao(RandomUtil.generateActivationKey());
        usuario.setResetDate(LocalDateTime.now());
        usuario.setStatus(Usuario.Status.INATIVO);
        userRepository.save(usuario);

        return usuario;
    }

    public void deleteUserInformation(String login) {
        userRepository.findOneByLogin(login).ifPresent(u -> {
            userRepository.delete(u);
            log.debug("Deleted User: {}", u);
        });
    }

    public void changePassword(String password) {
        userRepository.findOneByLogin(SecurityUtils.getCurrentUser().getUsername()).ifPresent(u -> {
            String encryptedPassword = passwordEncoder.encode(password);
            u.setSenha(encryptedPassword);
            userRepository.save(u);
            log.debug("Changed password for User: {}", u);
        });
    }

    @Transactional(readOnly = true)
    public Optional<Usuario> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneByLogin(login).map(u -> {
            u.getAutoridades().size();
            return u;
        });
    }

    @Transactional(readOnly = true)
    public Usuario getUserWithAuthorities(Long id) {
        Usuario usuario = userRepository.findById(id).get();
        usuario.getAutoridades().size(); // eagerly load the association
        return usuario;
    }


    @Transactional(readOnly = true)
    public Usuario getUserWithAuthorities() {
        Usuario usuario = SecurityUtils.getCurrentUser();

        if (Objects.nonNull(usuario.getId())) {
            usuario = customUserRepository.buscarUsuarioCompletoPorId(usuario.getId());
        }

        if (usuario.getAgricultor() != null) {
            usuario.setAgricultor(customAgricultorRepository.buscarPorId(usuario.getAgricultor().getId()));
        }

        if (usuario.getUsuarioAvancado() != null) {
            usuario.setUsuarioAvancado(customUsuarioAvancadoRepository.buscarPorId(usuario.getUsuarioAvancado().getId()));
        }

        usuario.getAutoridades().size(); // eagerly load the association

        Optional<String> autoridade = usuario.getAutoridades().stream().map(Autoridade::getNome).findFirst();

        verificarAutoridadeUsuarioAter(usuario, autoridade);

        return usuario;
    }


    @SuppressWarnings("Duplicates")
    public void verificarAutoridadeUsuarioAter(Usuario usuario, Optional<String> autoridade) {
        if (nonNull(autoridade) && autoridade.get().equals("ROLE_USUARIO_ATER")) {
            String autoridadeUsuarioAter = findAutoridadeUsuarioAter(usuario.getUsuarioAvancado().getId());

            if (nonNull(autoridadeUsuarioAter)) {
                Autoridade authority = authorityRepository.findById(autoridadeUsuarioAter).get();
                Set<Autoridade> authorities = new HashSet<>();
                authorities.add(authority);

                usuario.setAutoridades(authorities);
            }
        }
    }

    @SuppressWarnings("Duplicates")
    public String findAutoridadeUsuarioAter(Long usuarioAvancadoId) {
        if (nonNull(planoTrabalhoAnualService.findPlanoTrabalhoAnualPorCoordenadorId(usuarioAvancadoId))) {
            return "ROLE_USUARIO_COORDENADOR_ATER";
        } else if (nonNull(planoTrabalhoAnualService.findPlanoTrabalhoAnualPorTecnicoId(usuarioAvancadoId))) {
            return "ROLE_USUARIO_ATER_TECNICO";
        } else if (nonNull(planoTrabalhoAnualService.findPlanoTrabalhoAnualPorAssessorId(usuarioAvancadoId))) {
            return "ROLE_USUARIO_ATER_ASSESSOR";
        } else {
            return null;
        }
    }

    public Optional<Usuario> ativarUsuario(Long id) {
        userRepository.findOneById(id)
            .map(usuario -> {
                usuario.setStatus(Usuario.Status.ATIVADO);
                userRepository.save(usuario);
                return usuario;
            });
        return Optional.empty();
    }

    public Optional<Usuario> inativarUsuario(Long id) {
        userRepository.findOneById(id)
            .map(usuario -> {
                usuario.setStatus(Usuario.Status.INATIVO);
                userRepository.save(usuario);
                return usuario;
            });
        return Optional.empty();
    }

    public ResponseEntity solicitarAcesso(String cpf) {

        // Verificar se existe uma pessoa com esse CPF
        PessoaFisica pessoaFisica = customPessoaFisicaRepository.buscarPessoaPorCpf(cpf);

        // CPF não encontrado
        if (Objects.isNull(pessoaFisica)) {
            return ResponseEntity.notFound().build();
        }

        // Verificar se existe um usuário avançado
        UsuarioAvancado usuarioAvancado = customUsuarioAvancadoRepository.buscarPorCpf(cpf);

        // Usuário avançado não cadastrado
        if (Objects.isNull(usuarioAvancado)) {
            return ResponseEntity.notFound().build();
        }

        // Verificar se existe um usuário
        Usuario usuario = customUserRepository.buscarUsuarioPorCpf(cpf);


        String nome = pessoaFisica.getPessoa().getNome();
        String emailInstitucional = usuarioAvancado.getEmailInstitucional();
        String baseUrl = HttpUtil.PORTAL_AGRICULTURA_FAMILIAR_URL;

        ResponseEntity response = ResponseEntity.ok().build();

        if (Objects.isNull(usuario)) {

            if (Objects.nonNull(usuarioAvancado.getEmailInstitucional())) {
                Usuario usuarioInativo = criarUsuarioInativo(usuarioAvancado);
                usuarioInativo.setChaveAtivacao(RandomUtil.generateActivationKey());
                mailService.sendSignUpEmail(nome, emailInstitucional, usuarioInativo.getChaveAtivacao(), baseUrl);
                ResponseMessage responseMessage = new ResponseMessage(
                    Messages.getString(MessageKey.EMAIL_ENVIADO_SUCESSO_PARAMETRO, emailInstitucional),
                    Messages.getString(MessageKey.EMAIL_PARA_CONTATO));
                response = ResponseEntity.ok(responseMessage);
            } else {
                response = ResponseEntity.status(HttpStatus.SEE_OTHER).body(usuarioAvancado);
            }

        } else {
            // Esqueceu a senha?
            if (usuario.isEnabled()) {
                ResponseMessage responseMessage = new ResponseMessage(Messages.getString(MessageKey.AVISO_AGRICULTOR_CADASTRADO));
                response = new ResponseEntity<>(responseMessage, HttpStatus.FOUND);
                // Reenviar o convite
            } else {
                usuario.setChaveAtivacao(RandomUtil.generateActivationKey());
                mailService.sendSignUpEmail(nome, emailInstitucional, usuario.getChaveAtivacao(), baseUrl);
                ResponseMessage responseMessage = new ResponseMessage(
                    Messages.getString(MessageKey.EMAIL_ENVIADO_SUCESSO_PARAMETRO, emailInstitucional) + " " +
                        Messages.getString(MessageKey.EMAIL_PARA_CONTATO));
                response = ResponseEntity.ok(responseMessage);
            }
        }

        return response;
    }

    public void approveAccessRequest(List<Usuario> usuarios) {
        for (Usuario usuario : usuarios) {
            Usuario user = userRepository.findById(usuario.getId()).get();

            if (user.getStatus() != null && user.getStatus().equals(Usuario.Status.EM_ANALISE)) {
                user.setStatus(Usuario.Status.INATIVO);
                user.setChaveAtivacao(RandomUtil.generateActivationKey());
                userRepository.save(user);
                String nome = user.getUsuarioAvancado().getPessoaFisica().getPessoa().getNome();
                String emailInstitucional = user.getUsuarioAvancado().getEmailInstitucional();
                String chaveAtivacao = user.getChaveAtivacao();
                String baseUrl = HttpUtil.PORTAL_AGRICULTURA_FAMILIAR_URL;
                mailService.sendSignUpEmail(nome, emailInstitucional, chaveAtivacao, baseUrl);
            }
        }
    }

    public List<UsuarioAvancado> buscarPorNome(String nome) {
        Usuario usuario = SecurityUtils.getCurrentUser();

        List<UsuarioAvancado> usuariosAvancados = new ArrayList<UsuarioAvancado>();

        if (!empresaIsNull(usuario) || isSuperUser(usuario)) {
            usuariosAvancados = customUsuarioAvancadoRepository.buscarPorNome(nome);
        }

        return usuariosAvancados;
    }

    public Boolean empresaIsNull(Usuario usuario) {
        if (Objects.isNull(usuario.getUsuarioAvancado())) {
            return true;
        }

        if (Objects.isNull(usuario.getUsuarioAvancado().getEmpresa())) {
            return true;
        }

        return false;
    }

    public boolean isSuperUser(Usuario usuario) {
        usuario = getUserWithAuthorities(usuario.getId());

        for (Autoridade a : usuario.getAuthorities()) {
            if (a.getAuthority().equals(AuthoritiesConstants.ADMIN)) {
                return true;
            }
        }

        return false;
    }

    public List<Usuario> buscarUsuariosEmpresaPorNome(String nome) {
        Usuario usuario = SecurityUtils.getCurrentUser();

        List<Usuario> usuarios = new ArrayList<Usuario>();

        if (!empresaIsNull(usuario) || isSuperUser(usuario)) {
            usuarios = customUserRepository.buscarPorNome(nome, usuario, isSuperUser(usuario));
        }

        return usuarios;
    }

    public List<UsuarioDTO> buscarUsuariosEmpresa(FiltroUsuarioDTO filters) {
        Usuario usuario = SecurityUtils.getCurrentUser();

        return customUserRepository.buscarUsuariosEmpresaPorFiltros(filters, usuario, isSuperUser(usuario));
    }

    public Usuario buscarUsuarioCompletoPorId(Long id) {
        return customUserRepository.buscarUsuarioCompletoPorId(id);
    }

    public Usuario buscarUsuarioPorId(Long id) {
        return customUserRepository.buscarUsuarioPorId(id);
    }

    @Transactional
    public Usuario updateUsuario(Long id, Usuario usuario) {
        Usuario usuarioAtual = userRepository.findById(id).get();

        if (isNull(usuarioAtual)) {
            return null;
        }

        BeanUtils.copyProperties(usuario.getUsuarioAvancado(), usuarioAtual.getUsuarioAvancado());

        if (nonNull(usuarioAtual.getUsuarioAvancado()) && nonNull(usuario.getUsuarioAvancado())) {
            usuarioAtual.getUsuarioAvancado().setEmailInstitucional(usuario.getUsuarioAvancado().getEmailInstitucional());
            if (usuario.isEnabled()) {
                usuarioAtual.setStatus(Usuario.Status.ATIVADO);
            } else {
                usuarioAtual.setStatus(Usuario.Status.INATIVO);
            }

            if (!usuario.getAutoridades().isEmpty()) {
                usuarioAtual.setAutoridades(usuario.getAutoridades());
            }
            usuarioAtual.setPrimeiroAcesso(usuario.isPrimeiroAcesso());
        }

        if(nonNull(usuarioAtual.getUsuarioAvancado())
            && nonNull(usuarioAtual.getUsuarioAvancado().getPessoaFisica())
            && nonNull(usuarioAtual.getUsuarioAvancado().getPessoaFisica().getPessoa())
            && nonNull(usuarioAtual.getUsuarioAvancado().getPessoaFisica().getPessoa().getTelefones())) {

            usuarioAtual.getUsuarioAvancado().getPessoaFisica().getPessoa().getTelefones().forEach(
                telefone -> telefone.setPessoa(usuarioAtual.getUsuarioAvancado().getPessoaFisica().getPessoa())
            );
        }

        userRepository.save(usuarioAtual);
        return usuarioAtual;
    }


}
