package br.gov.ce.sda.service;


import java.util.List;

import javax.inject.Inject;

import br.gov.ce.sda.web.rest.dto.EmpresaDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.gov.ce.sda.domain.Empresa;
import br.gov.ce.sda.repository.CustomEmpresaRepository;
import br.gov.ce.sda.repository.EmpresaRepository;
import br.gov.ce.sda.web.rest.dto.FiltroEmpresaDTO;

@Service
public class EmpresaService {

    @Inject
    private EmpresaRepository empresaRepository;

    @Inject
    private CustomEmpresaRepository customEmpresaRepository;

    @Transactional
    public Empresa save(Empresa empresa) {
        Empresa savedEmpresa = empresaRepository.save(empresa);
        return savedEmpresa;
    }

    @Transactional(readOnly = true)
    public List<Empresa> findAll() {
        return empresaRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<Empresa> findAllPauloFreire() {
        return customEmpresaRepository.findAllByPauloFreireOrderByNomeAsc();
    }


    @Transactional(readOnly = true)
	public List<Empresa> findAllByFilter(FiltroEmpresaDTO filtro){
		return customEmpresaRepository.findAllByFilter(filtro);
	}

    @Transactional(readOnly = true)
    public Empresa findById(Long id) {
        return customEmpresaRepository.findById(id);
    }

    @Transactional
    public void update(Empresa currentEmpresa) {
        empresaRepository.save(currentEmpresa);
    }

    @Transactional
    public void delete(Long id) {
        empresaRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public List<EmpresaDTO> findAllByRegiao(List<Long> regiao){
        return customEmpresaRepository.findAllByRegiao(regiao);
    }

}
