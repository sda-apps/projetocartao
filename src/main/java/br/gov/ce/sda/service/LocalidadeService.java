package br.gov.ce.sda.service;


import br.gov.ce.sda.domain.Empresa;
import br.gov.ce.sda.domain.Localidade;
import br.gov.ce.sda.repository.CustomEmpresaRepository;
import br.gov.ce.sda.repository.CustomLocalidadeRepository;
import br.gov.ce.sda.repository.EmpresaRepository;
import br.gov.ce.sda.repository.LocalidadeRepository;
import br.gov.ce.sda.web.rest.dto.FiltroEmpresaDTO;
import br.gov.ce.sda.web.rest.dto.FiltroLocalidadeDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

@Service
public class LocalidadeService {

    @Inject
    private CustomLocalidadeRepository customLocalidadeRepository;

    @Inject
    private LocalidadeRepository localidadeRepository;

    @Transactional
    public Localidade save(Localidade localidade) {
        Localidade savedlocalidade = localidadeRepository.save(localidade);
        return savedlocalidade;
    }

    @Transactional(readOnly = true)
	public List<Localidade> findAllByFilter(FiltroLocalidadeDTO filtro){
		return customLocalidadeRepository.findAllByFilter(filtro);
	}

    @Transactional(readOnly = true)
    public Localidade findById(Long id) {
        return customLocalidadeRepository.findById(id);
    }

    @Transactional(readOnly = true)
    public List<Localidade> findByComunidade(Long id) {
        return localidadeRepository.findAllByComunidade_Id(id);
    }

    @Transactional(readOnly = true)
    public List<Localidade> findByComunidades(List<Long> id) {
        return localidadeRepository.findAllByComunidade_IdIn(id);
    }

    @Transactional
    public void update(Localidade currentLocalidade) {
        localidadeRepository.save(currentLocalidade);
    }

    @Transactional
    public void delete(Long id) {
        localidadeRepository.deleteById(id);
    }
}
