package br.gov.ce.sda.service;

import br.gov.ce.sda.MessageKey;
import br.gov.ce.sda.Messages;
import br.gov.ce.sda.domain.ater.AtividadePTA;
import br.gov.ce.sda.web.rest.dto.ResponseMessage;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
public class ResponseMessageService {

    private final AtividadeService atividadeService;

    public ResponseMessageService(AtividadeService atividadeService) {
        this.atividadeService = atividadeService;
    }

    public ResponseMessage createErrorMessage(DataIntegrityViolationException ex) {
        ResponseMessage errorMessage = new ResponseMessage();

        if (ex.getCause() instanceof ConstraintViolationException) {
            ConstraintViolationException constraintViolation = (ConstraintViolationException) ex.getCause();
            String constraintName = constraintViolation.getConstraintName();

            switch (constraintName) {
                case "usuario_key1":
                    errorMessage = new ResponseMessage(Messages.getString(MessageKey.USUARIO_EXISTENTE));
                    break;
                case "usuario_avancado_key2":
                    errorMessage = new ResponseMessage(Messages.getString(MessageKey.EMAIL_EXISTENTE));
                    break;
                case "plano_trabalho_anual_key1":
                    errorMessage = new ResponseMessage(Messages.getString(MessageKey.PTA_CONTRATO_EXISTENTE));
                    break;
                case "contrato_key1":
                    errorMessage = new ResponseMessage(Messages.getString(MessageKey.CONTRATO_EXISTENTE));
                    break;
                case "comunidade_key1":
                    errorMessage = new ResponseMessage(Messages.getString(MessageKey.COMUNIDADE_EXISTENTE));
                    break;
                case "atividade_key1":
                    errorMessage = new ResponseMessage(Messages.getString(MessageKey.ATIVIDADE_EXISTENTE));
                    break;
                case "acao_key1":
                    errorMessage = new ResponseMessage(Messages.getString(MessageKey.ACAO_EXISTENTE));
                    break;
                case "meta_atividades_trimestral_key1":
                    String message = constraintViolation.getSQLException().getMessage();
                    String[] constraintMessage = message.split("\\(");

                    if(constraintMessage.length > 0) {
                        String[] ids = constraintMessage[2].split(",");
                        String idAtividade = ids[1].trim();

                        AtividadePTA atividade = atividadeService.findById(Long.decode(idAtividade));

                        errorMessage = new ResponseMessage(
                            Messages.getString(MessageKey.META_TRIMESTRAL_EXISTENTE, atividade.getNome()));

                        break;
                    }

                    errorMessage = new ResponseMessage(
                        Messages.getString(MessageKey.META_TRIMESTRAL_EXISTENTE));
                    break;
            }
        }

        return errorMessage;
    }
}
