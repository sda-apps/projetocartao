package br.gov.ce.sda.service;


import br.gov.ce.sda.config.security.SecurityUtils;
import br.gov.ce.sda.domain.Agricultor;
import br.gov.ce.sda.domain.Empresa;
import br.gov.ce.sda.domain.Municipio;
import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.domain.ater.Comunidade;
import br.gov.ce.sda.domain.ater.Regiao;
import br.gov.ce.sda.repository.CustomAgricultorRepository;
import br.gov.ce.sda.repository.CustomAreaAtuacaoRepository;
import br.gov.ce.sda.repository.CustomEmpresaRepository;
import br.gov.ce.sda.repository.MunicipioRepository;
import br.gov.ce.sda.repository.ater.ComunidadeRepository;
import br.gov.ce.sda.repository.ater.RegiaoRepository;
import br.gov.ce.sda.web.rest.EmpresaResource;
import br.gov.ce.sda.web.rest.dto.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.*;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.type.HorizontalTextAlignEnum;
import net.sf.jasperreports.engine.type.ModeEnum;
import net.sf.jasperreports.engine.type.SplitTypeEnum;
import net.sf.jasperreports.engine.type.VerticalTextAlignEnum;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Service
public class ServicosService {

    private static final String LOGO_GOVERNO = "LOGO_GOVERNO";

    private static final String LOGO_SISTEMA = "LOGO_SISTEMA";

    public static final String LOGO_SISTEMA_PNG = "marcaCartao.png";

    public static final String LOGO_PAULO_FREIRE_PNG = "logo_paulo_freire.png";

    public static final String LOGO_GOVERNO_PNG = "marcaGoverno.png";

    public static final String FORMATO_RELATORIO = ".jrxml";

    public static final String PATH_IMAGES = "relatorios/imagens/";

    public static final String PATH_RELATORIOS = "relatorios/";

    public static final String PATH_RELATORIO_PERFIL_FAMILIA = "perfil_familia/";

    public static final String PATH_RELATORIO_SUBSISTEMA_PRODUTIVO = "subsistema_produtivo/";

    public static final String PATH_RELATORIO_INDICADORES_SOCIAIS = "indicadores_sociais/";

    public static final String PATH_RELATORIO_CHEFE_FAMILIA_MOBILE = "caracteristica_chefe_familia/portal_mobile/";

    public static final String PATH_RELATORIO_PERFIL_FAMILIA_PER_CAPITA = "perfil_familia/per_capita/";

    private static final String PARAMETRO_AGRICULTOR = "PARAMETRO_AGRICULTOR";

    private static final String PATH_RELATORIO_CHEFE_FAMILIA = "caracteristica_chefe_familia/";

    @Inject
    private DataSource dataSource;

    @Inject
    private CustomAgricultorRepository agricultorRepository;

    @Inject
    private RegiaoRepository regiaoRepository;

    @Inject
    private MunicipioRepository municipioRepository;

    @Inject
    private ComunidadeRepository comunidadeRepository;

    @Inject
    private CustomAreaAtuacaoRepository customAreaAtuacaoRepository;

    @Inject
    private CustomEmpresaRepository customEmpresaRepository;

    public JasperPrint gerarExtratoHoraPlantar(Long agricultor) throws Exception {
        String path = PATH_RELATORIOS + "ExtratoHoraPlantar" + FORMATO_RELATORIO;

        Map<String, Object> params = new HashMap<>();

        params.put(PARAMETRO_AGRICULTOR, agricultor);

        return this.gerarRelatorioDinamico(path, params, agricultor, "Hora de Plantar");
    }

    public JasperPrint gerarExtratoPNAE(Long agricultor) throws Exception {
        String path = PATH_RELATORIOS + "ExtratoPNAE" + FORMATO_RELATORIO;

        Map<String, Object> params = new HashMap<>();

        params.put(PARAMETRO_AGRICULTOR, agricultor);

        return this.gerarRelatorioDinamico(path, params, agricultor, "PNAE");
    }

    public JasperPrint gerarExtratoPAALeite(Long agricultor) throws Exception {
        String path = PATH_RELATORIOS + "ExtratoPAALeite" + FORMATO_RELATORIO;

        Map<String, Object> params = new HashMap<>();

        params.put(PARAMETRO_AGRICULTOR, agricultor);

        return this.gerarRelatorioDinamico(path, params, agricultor, "PAA Leite");
    }

    public JasperPrint gerarExtratoPAAAlimentos(Long agricultor) throws Exception {
        String path = PATH_RELATORIOS + "ExtratoPAAAlimentos" + FORMATO_RELATORIO;

        Map<String, Object> params = new HashMap<>();

        params.put(PARAMETRO_AGRICULTOR, agricultor);

        return this.gerarRelatorioDinamico(path, params, agricultor, "PAA Alimentos");
    }

    public JasperPrint gerarRelatorioCadastroAgricultores(FiltroRelatorioCadastroAgricultorDTO filtro) throws Exception {
        String path;

        if (filtro.getAtc() != null) {
            path = PATH_RELATORIOS + "cadastro_agricultor/RelatorioCadastroAgricultores.jasper";
        } else {
            path = PATH_RELATORIOS + "cadastro_agricultor/RelatorioCadastroAgricultoresSemATC.jasper";
        }

        Map<String, Object> params = fillParametersForReportATER(filtro);

        return this.gerarRelatorio(path, params);
    }

    public byte[] gerarRelaorioXlsCadastroAgricultores(FiltroRelatorioCadastroAgricultorDTO filtro) throws Exception {
        String path = PATH_RELATORIOS + "xls/cadastro-agricultor/RelatorioCadastroAgricultoresXls.jasper";

        Map<String, Object> params = fillParametersForReportATER(filtro);

        return this.gerarXls(path, params);
    }

    public JasperPrint gerarRelatorioAnaliticoFamilias(FiltroRelatorioCadastroAgricultorDTO filtro) throws Exception {

        String path;

        if (filtro.isParentes()) {
            if (filtro.getAtc() != null) {
                path = PATH_RELATORIOS + "analitico_familia/RelatorioAnaliticoFamiliasComParentes.jasper";
            } else {
                path = PATH_RELATORIOS + "analitico_familia/RelatorioAnaliticoFamiliasComParentesSemATC.jasper";
            }
        } else {
            if (filtro.getAtc() != null) {
                path = PATH_RELATORIOS + "analitico_familia/RelatorioAnaliticoFamilias.jasper";
            } else {
                path = PATH_RELATORIOS + "analitico_familia/RelatorioAnaliticoFamiliasSemATC.jasper";
            }
        }

        Map<String, Object> params = fillParametersForReportATER(filtro);

        return this.gerarRelatorio(path, params);
    }

    public byte[] gerarRelaorioXlsAnaliticoFamilias(FiltroRelatorioCadastroAgricultorDTO filtro) throws Exception {
        String path;

        if (filtro.isParentes()) {
            if (filtro.getAtc() != null) {
                path = PATH_RELATORIOS + "xls/analitico-familia/RelatorioAnaliticoFamiliasComParentesXls.jasper";
            } else {
                path = PATH_RELATORIOS + "xls/analitico-familia/RelatorioAnaliticoFamiliasComParentesSemATCXls.jasper";
            }
        } else {
            if (filtro.getAtc() != null) {
                path = PATH_RELATORIOS + "xls/analitico-familia/RelatorioAnaliticoFamiliasXls.jasper";
            } else {
                path = PATH_RELATORIOS + "xls/analitico-familia/RelatorioAnaliticoFamiliasSemATCXls.jasper";
            }
        }

        Map<String, Object> params = fillParametersForReportATER(filtro);

        return this.gerarXls(path, params);
    }

    private Map<String, Object> fillParametersForReportATER(FiltroRelatorioCadastroAgricultorDTO filtro) {
        Map<String, Object> params = new HashMap<>();

        if (isNull(filtro.getDataInicio())) {
            filtro.setDataInicio("2017-10-01");
        }

        if (filtro.getRegiao().equals("0")) {
            params.put("PARAMETRO_REGIAO_TODOS", "0");
        }

        if (filtro.getAtc() != null && filtro.getAtc().equals("0")) {
            params.put("PARAMETRO_ATC_TODOS", "0");
        }

        if (filtro.getMunicipio() != null && filtro.getMunicipio().equals("0")) {
            params.put("PARAMETRO_MUNICIPIO_TODOS", "0");
        }

        if (filtro.getComunidade() != null && filtro.getComunidade().equals("0")) {
            params.put("PARAMETRO_COMUNIDADE_TODOS", "0");
        }

        if (filtro.getLocalidade() != null && filtro.getLocalidade().equals("0")) {
            params.put("PARAMETRO_LOCALIDADE_TODOS", "0");
        }

        params.put("PARAMETRO_DATA_INICIAL", filtro.getDataInicio());
        params.put("PARAMETRO_DATA_FIM", filtro.getDataFim());
        params.put("PARAMETRO_REGIAO", filtro.getRegiao());
        params.put("PARAMETRO_ATC", filtro.getAtc());
        params.put("PARAMETRO_MUNICIPIO", filtro.getMunicipio());
        params.put("PARAMETRO_COMUNIDADE", filtro.getComunidade());
        params.put("PARAMETRO_LOCALIDADE", filtro.getLocalidade());
        params.put("PARAMETRO_TIPO_COMUNIDADE", filtro.getTipoComunidade());
        params.put("PARAMETRO_FILTROS_APLICADOS", montarStringFiltrosAplicados(filtro));
        params.put(LOGO_SISTEMA, this.getClass().getClassLoader().getResource(PATH_IMAGES + LOGO_PAULO_FREIRE_PNG).getPath());
        params.put(LOGO_GOVERNO, this.getClass().getClassLoader().getResource(PATH_IMAGES + LOGO_GOVERNO_PNG).getPath());
        return params;
    }

    public JasperPrint gerarRelatorioPerfilDasFamilias(FiltroRelatorioCadastroPerfilFamilia filtro) throws Exception {

        Map<String, Object> params = new HashMap<>();

        String path = "";

        switch (filtro.getTipoRelatorio()) {
            case 0:
                path = buildPathCaracteristicasChefeFamilia(filtro, params);
                break;
            case 1:
                path = buildPathRelatorioPopulacaoAtendida(filtro, params);
                break;
            case 2:
                path = buildPathRelatorioPefilDeFamiliaIndicadesSociais(filtro, params);
                break;
            case 5:
                path = buildPathRelatorioPefilDeFamiliaSubsistemaProdutivo(filtro, params);
                break;
            case 6:
                path = buildPathRelatorioPefilDeFamiliaPerCapita(filtro, params);
                break;
        }

        fillParametersForReportPerfilDasFamilias(filtro, params);

        return this.gerarRelatorio(path, params);
    }

    private String buildPathRelatorioPopulacaoAtendida(FiltroRelatorioCadastroPerfilFamilia filtro, Map<String, Object> params) {
        String path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "populacao_atendida/RelatorioPerfilDeFamilia_PopulacaoAtendida.jasper";
        String pathSubreport = PATH_RELATORIOS + "/Blank_A4_Landscape.jasper";

        if (nonNull(filtro.getRegiao())) {
            pathSubreport = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "populacao_atendida/RelatorioPerfilDeFamiliaRegiao_PopulacaoAtendida.jasper";
        }

        if (nonNull(filtro.getMunicipio())) {
            pathSubreport = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "populacao_atendida/RelatorioPerfilDeFamiliaMunicipio_PopulacaoAtendida.jasper";
        }

        if (nonNull(filtro.getComunidade())) {
            pathSubreport = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "populacao_atendida/RelatorioPerfilDeFamiliaComunidade_PopulacaoAtendida.jasper";
        }

        params.put("SUBREPORT_DIR", pathSubreport);

        return path;
    }

    private String buildPathRelatorioPefilDeFamiliaPerCapita(FiltroRelatorioCadastroPerfilFamilia filtro, Map<String, Object> params) {
        String path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "per_capita/RelatorioPerfilDeFamilia_Per_Capita.jasper";
        String pathSubreport = PATH_RELATORIOS + "/Blank_A4_Landscape.jasper";

        if (nonNull(filtro.getRegiao())) {
            pathSubreport = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "per_capita/RelatorioPerfilDeFamiliaRegiao_Per_Capita_SubReport.jasper";
        }

        if (nonNull(filtro.getMunicipio())) {
            pathSubreport = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "per_capita/RelatorioPerfilDeFamiliaMunicipio_Per_Capita_SubReport.jasper";
        }

        if (nonNull(filtro.getComunidade())) {
            pathSubreport = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "per_capita/RelatorioPerfilDeFamiliaComunidade_Per_Capita_SubReport.jasper";
        }

        params.put("SUBREPORT_DIR", pathSubreport);

        return path;
    }

    private String buildPathRelatorioPefilDeFamiliaIndicadesSociais(FiltroRelatorioCadastroPerfilFamilia filtro, Map<String, Object> params) {

        String path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "indicadores_sociais/RelatorioPerfilDeFamilia_Indicadores_Sociais.jasper";
        String pathSubreport = PATH_RELATORIOS + "/Blank_A4_Landscape.jasper";

        if (nonNull(filtro.getRegiao())) {
            pathSubreport = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "indicadores_sociais/RelatorioPerfilDeFamiliaRegiao_Indicadores_Sociais.jasper";
        }

        if (nonNull(filtro.getMunicipio())) {
            pathSubreport = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "indicadores_sociais/RelatorioPerfilDeFamiliaMunicipio_Indicadores_Sociais.jasper";
        }

        if (nonNull(filtro.getComunidade())) {
            pathSubreport = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "indicadores_sociais/RelatorioPerfilDeFamiliaComunidade_Indicadores_Sociais.jasper";

        }

        params.put("SUBREPORT_DIR", pathSubreport);

        return path;
    }

    private void fillParametersForReportPerfilDasFamilias(FiltroRelatorioCadastroPerfilFamilia filtro, Map<String, Object> params) {
        params.put("PARAMETRO_DATA_INICIAL", filtro.getDataInicio());
        params.put("PARAMETRO_DATA_FIM", filtro.getDataFim());
        params.put("PARAMETRO_LINHA_BASE", filtro.getLinhaBase());
        params.put("PARAMETRO_TIPO_RELATORIO", filtro.getTipoRelatorio());
        params.put("PARAMETRO_REGIAO", filtro.getRegiao());
        params.put("PARAMETRO_MUNICIPIO", filtro.getMunicipio());
        params.put("PARAMETRO_COMUNIDADE", filtro.getComunidade());
        params.put("PARAMETRO_FILTROS_APLICADOS", montarStringFiltrosAplicados(filtro));
        params.put("PARAMETRO_TIPO_COMUNIDADE", filtro.getTipoComunidade());
        params.put(LOGO_SISTEMA, this.getClass().getClassLoader().getResource(PATH_IMAGES + LOGO_PAULO_FREIRE_PNG).getPath());
        params.put(LOGO_GOVERNO, this.getClass().getClassLoader().getResource(PATH_IMAGES + LOGO_GOVERNO_PNG).getPath());
    }

    private String buildPathRelatorioPefilDeFamiliaSubsistemaProdutivo(FiltroRelatorioCadastroPerfilFamilia filtro, Map<String, Object> params) {
        String path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "subsistema_produtivo/RelatorioPerfilDeFamilia_Subsistema_Produtivo.jasper";
        String pathSubreport = PATH_RELATORIOS + "/Blank_A4_Landscape.jasper";

        if (nonNull(filtro.getRegiao())) {
            pathSubreport = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "subsistema_produtivo/RelatorioPerfilDeFamiliaRegiao_Subsistema_SubReport.jasper";
        }

        if (nonNull(filtro.getMunicipio())) {
            pathSubreport = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "subsistema_produtivo/RelatorioPerfilDeFamiliaMunicipio_Subsistema_SubReport.jasper";
        }

        if (nonNull(filtro.getComunidade())) {
            pathSubreport = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "subsistema_produtivo/RelatorioPerfilDeFamiliaComunidade_Subsistema_SubReport.jasper";
        }

        params.put("SUBREPORT_DIR", pathSubreport);

        return path;
    }

    private String buildPathCaracteristicasChefeFamilia(FiltroRelatorioCadastroPerfilFamilia filtro, Map<String, Object> params) {
        String path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_CHEFE_FAMILIA + "RelatorioPerfilDeFamilia.jasper";

        if (filtro.getVisualizacaoCadastros().equals("naoavaliados")) {
            path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_CHEFE_FAMILIA_MOBILE + "RelatorioPerfilDeFamilia_NaoAvaliados.jasper";
        }

        String pathSubreport = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_CHEFE_FAMILIA + "RelatorioPerfilDeFamiliaRegiao_SubReport.jasper";

        if (filtro.getVisualizacaoCadastros().equals("naoavaliados")) {
            pathSubreport = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_CHEFE_FAMILIA_MOBILE + "RelatorioPerfilDeFamiliaRegiao_NaoAvaliados_SubReport.jasper";
        }

        if (nonNull(filtro.getMunicipio())) {
            if (filtro.getVisualizacaoCadastros().equals("naoavaliados")) {
                pathSubreport = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_CHEFE_FAMILIA_MOBILE + "RelatorioPerfilDeFamiliaMunicipio_NaoAvaliados_SubReport.jasper";
            } else {
                pathSubreport = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_CHEFE_FAMILIA + "RelatorioPerfilDeFamiliaMunicipio_SubReport.jasper";
            }
        }

        if (nonNull(filtro.getComunidade())) {
            if (filtro.getVisualizacaoCadastros().equals("naoavaliados")) {
                pathSubreport = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_CHEFE_FAMILIA_MOBILE + "RelatorioPerfilDeFamiliaComunidade_NaoAvaliados_SubReport.jasper";
            } else {
                pathSubreport = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_CHEFE_FAMILIA + "RelatorioPerfilDeFamiliaComunidade_SubReport.jasper";
            }
        }
        params.put("SUBREPORT_DIR", pathSubreport);

        return path;
    }

    public JasperPrint gerarRelatorioPerfilDasFamiliasGrafico(FiltroRelatorioCadastroPerfilFamilia filtro) throws Exception {
        String path = "";

        switch (filtro.getTipoRelatorio()) {
            case 0:
                path = buildPathPerfilDasFamiliasGrafico(filtro);
                break;
            case 1:
                path = buildPathRelatorioPopulacaoAtendidaGrafico(filtro);
                break;
            case 2:
                path = buildPathPerfilDasFamiliasIndicadoresSociaisGrafico(filtro);
                break;
            case 5:
                path = buildPathPerfilDasFamiliasSubsistemaGrafico(filtro);
                break;
            case 6:
                path = buildPathRelatorioRendaPerCapitaGrafico(filtro);
        }

        Map<String, Object> params = new HashMap<>();

        fillParametersForReportPerfilDasFamilias(filtro, params);

        return this.gerarRelatorio(path, params);
    }

    private String buildPathRelatorioPopulacaoAtendidaGrafico(FiltroRelatorioCadastroPerfilFamilia filtro) {
        String path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "populacao_atendida/RelatorioGraficoPizzaPerfilFamilia_PopulacaoAtendida.jasper";

        if (nonNull(filtro.getRegiao())) {
            path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "populacao_atendida/RelatorioGraficoBarraPerfilFamiliaRegiao_PopulacaoAtendida.jasper";
        }

        if (nonNull(filtro.getMunicipio())) {
            path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "populacao_atendida/RelatorioGraficoBarraPerfilFamiliaMunicipio_PopulacaoAtendida.jasper";
        }

        if (nonNull(filtro.getComunidade())) {
            path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "populacao_atendida/RelatorioGraficoBarraPerfilFamiliaComunidade_PopulacaoAtendida.jasper";
        }

        return path;
    }

    private String buildPathPerfilDasFamiliasIndicadoresSociaisGrafico(FiltroRelatorioCadastroPerfilFamilia filtro) {
        String path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_INDICADORES_SOCIAIS + "RelatorioGraficoPizzaPerfilDeFamilia_IndicadoresSociais.jasper";

        if (nonNull(filtro.getRegiao())) {
            path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_INDICADORES_SOCIAIS + "RelatorioGraficoBarraPerfilDeFamiliaRegiao_IndicadoresSociais.jasper";
        }

        if (nonNull(filtro.getMunicipio())) {
            path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_INDICADORES_SOCIAIS + "RelatorioGraficoPerfilDeFamiliaMunicipio_IndicadoresSociais.jasper";
        }

        if (nonNull(filtro.getComunidade())) {
            path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_INDICADORES_SOCIAIS + "RelatorioGraficoPerfilDeFamiliaComunidade_IndicadoresSociais.jasper";
        }

        return path;
    }

    private String buildPathPerfilDasFamiliasGrafico(FiltroRelatorioCadastroPerfilFamilia filtro) {
        String path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_CHEFE_FAMILIA + "RelatorioGraficoPizzaPerfilFamilia.jasper";

        if (filtro.getVisualizacaoCadastros().equals("naoavaliados")) {
            path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_CHEFE_FAMILIA_MOBILE + "RelatorioGraficoPizzaPerfilFamilia_NaoAvaliados.jasper";
        }

        if (nonNull(filtro.getRegiao())) {
            if (filtro.getVisualizacaoCadastros().equals("naoavaliados")) {
                path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_CHEFE_FAMILIA_MOBILE + "RelatorioGraficoBarraPerfilFamiliaRegiao_NaoAvaliados.jasper";
            } else {
                path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_CHEFE_FAMILIA + "RelatorioGraficoBarraPerfilFamiliaRegiao.jasper";
            }
        }

        if (nonNull(filtro.getMunicipio())) {
            if (filtro.getVisualizacaoCadastros().equals("naoavaliados")) {
                path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_CHEFE_FAMILIA_MOBILE + "RelatorioGraficoBarraPerfilFamiliaMunicipio_NaoAvaliados.jasper";
            } else {
                path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_CHEFE_FAMILIA + "RelatorioGraficoBarraPerfilFamiliaMunicipio.jasper";
            }
        }

        if (nonNull(filtro.getComunidade())) {
            if (filtro.getVisualizacaoCadastros().equals("naoavaliados")) {
                path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_CHEFE_FAMILIA_MOBILE + "RelatorioGraficoBarraPerfilFamiliaComunidade_NaoAvaliados.jasper";
            } else {
                path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_CHEFE_FAMILIA + "RelatorioGraficoBarraPerfilFamiliaComunidade.jasper";
            }
        }
        return path;
    }

    private String buildPathPerfilDasFamiliasSubsistemaGrafico(FiltroRelatorioCadastroPerfilFamilia filtro) {
        String path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_SUBSISTEMA_PRODUTIVO + "RelatorioGraficoPizzaPerfilFamilia_Subsistema.jasper";

        if (nonNull(filtro.getRegiao())) {
            path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_SUBSISTEMA_PRODUTIVO + "RelatorioGraficoBarraPerfilFamilia_Subsistema_Regiao.jasper";
        }

        if (nonNull(filtro.getMunicipio())) {
            path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_SUBSISTEMA_PRODUTIVO + "RelatorioGraficoBarraPerfilFamilia_Subsistema_Municipio.jasper";
        }

        if (nonNull(filtro.getComunidade())) {
            path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + PATH_RELATORIO_SUBSISTEMA_PRODUTIVO + "RelatorioGraficoBarraPerfilFamilia_Subsistema_Comunidade.jasper";
        }

        return path;
    }

    public String buildPathRelatorioRendaPerCapitaGrafico(FiltroRelatorioCadastroPerfilFamilia filtro) throws Exception {

        String path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA_PER_CAPITA + "RelatorioGraficoPizzaRendaFamiliarPerCapita.jasper";

        if (nonNull(filtro.getRegiao())) {
            path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA_PER_CAPITA + "RelatorioGraficoBarraRendaFamiliarPerCapitaRegiao.jasper";
        }

        if (nonNull(filtro.getMunicipio())) {
            path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA_PER_CAPITA + "RelatorioGraficoBarraRendaFamiliarPerCapitaMunicipio.jasper";
        }

        if (nonNull(filtro.getComunidade())) {
            path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA_PER_CAPITA + "RelatorioGraficoBarraRendaFamiliarPerCapitaComunidade.jasper";
        }

        return path;
    }

    public byte[] gerarRelatorioXlsPerfilDasFamilias(FiltroRelatorioCadastroPerfilFamilia filtro) throws Exception {
        Map<String, Object> params = new HashMap<>();

        String path = buildPathCaracteristicasChefeFamilia(filtro, params);

        fillParametersForReportPerfilDasFamilias(filtro, params);

        return this.gerarXls(path, params);
    }

    public byte[] gerarRelatorioXlsPerfilDasFamiliasGrafico(FiltroRelatorioCadastroPerfilFamilia filtro) throws Exception {
        String path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "RelatorioGraficoPizzaPerfilFamilia.jasper";

        if (nonNull(filtro.getRegiao())) {
            path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "RelatorioGraficoBarraPerfilFamiliaRegiao.jasper";
        }

        if (nonNull(filtro.getMunicipio())) {
            path = PATH_RELATORIOS + PATH_RELATORIO_PERFIL_FAMILIA + "RelatorioGraficoBarraPerfilFamiliaMunicipio.jasper";
        }

        Map<String, Object> params = new HashMap<>();

        fillParametersForReportPerfilDasFamilias(filtro, params);

        return this.gerarXls(path, params);
    }

    public JasperPrint gerarRelatorioPlanoTrabalhoAnual(Long ptaId) throws Exception {
        String path = PATH_RELATORIOS + "plano_anual/RelatorioPlanoAnual.jasper";

        Map<String, Object> params = new HashMap<>();

        params.put("PARAMETRO_PLANO_ANUAL_ID", ptaId);
        params.put("SUBREPORT_FAMILIAS_DIR", PATH_RELATORIOS + "plano_anual/RelatorioPlanoAnualFamilias_SubReport.jasper");
        params.put("SUBREPORT_ATIVIDADES_DIR", PATH_RELATORIOS + "plano_anual/RelatorioPlanoAnualAtividades_SubReport.jasper");
        params.put(LOGO_SISTEMA, this.getClass().getClassLoader().getResource(PATH_IMAGES + LOGO_PAULO_FREIRE_PNG).getPath());
        params.put(LOGO_GOVERNO, this.getClass().getClassLoader().getResource(PATH_IMAGES + LOGO_GOVERNO_PNG).getPath());

        return this.gerarRelatorio(path, params);
    }

    public JasperPrint gerarRelatorioPlanoTrabalhoTrimestral(Long planoTrimestralId) throws Exception {
        String path = PATH_RELATORIOS + "plano_trimestral/RelatorioPlanoTrimestral.jasper";

        Map<String, Object> params = new HashMap<>();

        params.put("PARAMETRO_PLANO_TRIMESTRAL_ID", planoTrimestralId);
        params.put("SUBREPORT_ATIVIDADES_DIR", PATH_RELATORIOS + "plano_trimestral/RelatorioPlanoTrimestralAtividades_SubReport.jasper");
        params.put(LOGO_SISTEMA, this.getClass().getClassLoader().getResource(PATH_IMAGES + LOGO_PAULO_FREIRE_PNG).getPath());
        params.put(LOGO_GOVERNO, this.getClass().getClassLoader().getResource(PATH_IMAGES + LOGO_GOVERNO_PNG).getPath());

        return this.gerarRelatorio(path, params);
    }

    public JasperPrint gerarRelatorioTrimestralAtividadeAnalitico(FiltroRelatorioTrimestralAtividades filtro) throws Exception {
        String path = PATH_RELATORIOS + "trimestral_atividades/RelatorioTrimestralAtividadesAnalitico.jasper";

        Map<String, Object> params = new HashMap<>();

        setFiltersRelatorioTrimestralAtividades(filtro, params);
        params.put(LOGO_SISTEMA, this.getClass().getClassLoader().getResource(PATH_IMAGES + LOGO_PAULO_FREIRE_PNG).getPath());
        params.put(LOGO_GOVERNO, this.getClass().getClassLoader().getResource(PATH_IMAGES + LOGO_GOVERNO_PNG).getPath());
        params.put("SUBREPORT_TEMAS_DIR", PATH_RELATORIOS + "trimestral_atividades/TemasPlanoTrimestralAnalitico.jasper");

        return this.gerarRelatorio(path, params);
    }


    public JasperPrint gerarRelatorioTrimestralAtividadesSintetico(FiltroRelatorioTrimestralAtividades filtro) throws Exception {
        String path = PATH_RELATORIOS + "trimestral_atividades/RelatorioTrimestralAtividadesSintetico.jasper";

        Map<String, Object> params = new HashMap<>();

        Usuario usuario = SecurityUtils.getCurrentUser();

        if (usuario.getAuthoritiesList().contains("ROLE_USUARIO_ATER")) {
            String comunidadesIds = getAreasAtuacaoTecnico(usuario);

            if (nonNull(comunidadesIds)) {
                params.put("PARAMETRO_COMUNIDADES", comunidadesIds);
                filtro.setComunidade(comunidadesIds);
            }
        }

        setFiltersRelatorioTrimestralAtividades(filtro, params);
        params.put(LOGO_SISTEMA, this.getClass().getClassLoader().getResource(PATH_IMAGES + LOGO_PAULO_FREIRE_PNG).getPath());
        params.put(LOGO_GOVERNO, this.getClass().getClassLoader().getResource(PATH_IMAGES + LOGO_GOVERNO_PNG).getPath());

        return this.gerarRelatorio(path, params);
    }

    private void setFiltersRelatorioTrimestralAtividades(FiltroRelatorioTrimestralAtividades filtro, Map<String, Object> params) {
        Empresa atc = customEmpresaRepository.findById(Long.parseLong(filtro.getAtc()));

        params.put("PARAMETRO_PLANO_TRIMESTRAL", filtro.getPtId());
        params.put("PARAMETRO_PLANO_TRIMESTAL_TRIMESTRE", filtro.getPtTrimestre());
        params.put("PARAMETRO_PLANO_ANUAL_CODIGO", "Plano de Trabalho Anual ("+filtro.getPtaCodigo()+")");
        params.put("PARAMETRO_ATC", "ATC ("+atc.getPessoaJuridica().getNomeFantasia()+")");
        params.put("PARAMETRO_ATC_ID", atc.getId());
        params.put("PARAMETRO_FILTROS_APLICADOS", montarStringFiltrosAplicados(filtro));
    }

    private String getAreasAtuacaoTecnico(Usuario usuario) {
        List<Comunidade> comunidadesByUsuarioAvancadoId = customAreaAtuacaoRepository.findComunidadesByUsuarioAvancadoId(usuario.getUsuarioAvancado().getId());

        return comunidadesByUsuarioAvancadoId.stream()
            .map(comunidade -> comunidade.getId().toString())
            .collect(Collectors.joining(","));
    }

    private String montarStringFiltrosAplicados(FiltroRelatorioCadastro filtro) {
        List<String> filtros = new ArrayList<>();
        if (nonNull(filtro.getRegiao())) {
            if (filtro.getRegiao().equals("0")) {
                filtros.add("Região (Todos)");
            } else {
                List<Long> collect = Arrays.stream(filtro.getRegiao().split(","))
                    .map(s -> Long.valueOf(s))
                    .collect(Collectors.toList());

                List<Regiao> regioes = regiaoRepository.findByInativoIsFalseAndIdIn(collect);

                filtros.add("Região (" + regioes.toString() + ")");
            }
        }

        if (nonNull(filtro.getMunicipio())) {
            if (filtro.getMunicipio().equals("0")) {
                filtros.add("Município (Todos)");
            } else {
                List<Long> collect = Arrays.stream(filtro.getMunicipio().split(","))
                    .map(s -> Long.valueOf(s))
                    .collect(Collectors.toList());

                List<Municipio> municipios = municipioRepository.findByIdIn(collect);

                filtros.add("Município (" + municipios.toString() + ")");
            }
        }

        if (nonNull(filtro.getComunidade())) {
            if (filtro.getComunidade().equals("0")) {
                filtros.add("Comunidade (Todos)");
            } else {
                List<Long> collect = Arrays.stream(filtro.getComunidade().split(","))
                    .map(s -> Long.valueOf(s))
                    .collect(Collectors.toList());

                List<Comunidade> comunidades = comunidadeRepository.findByIdIn(collect);

                filtros.add("Comunidade (" + comunidades.toString() + ")");
            }
        }

        if (nonNull(filtro.getTipoComunidade())) {
            if (filtro.getTipoComunidade().equals("0")) {
                filtros.add("Tipo Comunidade (Todos)");
            } else {
                List<String> collect = Arrays.stream(filtro.getTipoComunidade().split(","))
                    .map(s -> String.valueOf(s))
                    .collect(Collectors.toList());

                for (int i = 0; i < collect.size(); i++) {
                    collect.set(i, Comunidade.Tipo.getDescricaoById(collect.get(i)));
                }

                filtros.add("Tipo Comunidade (" + collect.toString() + ")");
            }
        }

        return filtros.toString().replaceAll("\\[|]", "");
    }

    private JasperPrint gerarRelatorioDinamico(String path, Map<String, Object> params, Long agricultorId, String programa) throws Exception {
        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream(path);
        JasperDesign jasperDesign = JRXmlLoader.load(resourceAsStream);

        params.put(LOGO_SISTEMA, this.getClass().getClassLoader().getResource(PATH_IMAGES + LOGO_SISTEMA_PNG).getPath());
        params.put(LOGO_GOVERNO, this.getClass().getClassLoader().getResource(PATH_IMAGES + LOGO_GOVERNO_PNG).getPath());

        // Criar Title Band
        JRDesignBand titleBand = new JRDesignBand();
        titleBand.setHeight(170);
        titleBand.setSplitType(SplitTypeEnum.STRETCH);

        inserirLogosETitulo(jasperDesign, titleBand, programa);

        inserirDadosDoAgricultor(jasperDesign, titleBand);

        if (existeConjunge(agricultorId)) {
            inserirDadosDoConjuge(jasperDesign, titleBand);
        }

        jasperDesign.setTitle(titleBand);

        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);

        Connection connection = dataSource.getConnection();

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, connection);

        if (!connection.isClosed()) {
            connection.close();
        }

        return jasperPrint;
    }

    private JasperPrint gerarRelatorio(String path, Map<String, Object> params) throws JRException, SQLException {
        InputStream jasperStream = this.getClass().getClassLoader().getResourceAsStream(path);

        Connection connection = dataSource.getConnection();

        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, connection);

        if (!connection.isClosed()) {
            connection.close();
        }

        return jasperPrint;
    }

    private byte[] gerarXls(String path, Map<String, Object> params) throws Exception {
        InputStream jasperStream = this.getClass().getClassLoader().getResourceAsStream(path);

        Connection connection = dataSource.getConnection();

        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, connection);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        JRXlsxExporter exporter = new JRXlsxExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));

        SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
        configuration.setDetectCellType(true);
        configuration.setCollapseRowSpan(false);
        configuration.setOnePagePerSheet(false);
        configuration.setRemoveEmptySpaceBetweenRows(true);
        exporter.setConfiguration(configuration);

        exporter.exportReport();

        byte[] bytes = outputStream.toByteArray();
        outputStream.close();

        if (!connection.isClosed()) {
            connection.close();
        }

        return bytes;
    }


    private boolean existeConjunge(Long agricultorId) {
        Agricultor conjuge = agricultorRepository.buscarConjugePorAgricultorId(agricultorId);
        return Objects.nonNull(conjuge);
    }

    private void inserirLogosETitulo(JasperDesign jasperDesign, JRDesignBand titleBand, String programa) {
        // Imagens
        JRDesignImage logoSistema = new JRDesignImage(jasperDesign);
        logoSistema.setHeight(39);
        logoSistema.setWidth(168);
        logoSistema.setX(0);
        logoSistema.setY(0);
        JRDesignExpression expressionLogoSistema = new JRDesignExpression();
        expressionLogoSistema.setText("$P{LOGO_SISTEMA}");
        logoSistema.setExpression(expressionLogoSistema);

        JRDesignImage logoGoverno = new JRDesignImage(jasperDesign);
        logoGoverno.setHeight(37);
        logoGoverno.setWidth(131);
        logoGoverno.setX(424);
        logoGoverno.setY(0);
        JRDesignExpression expressionLogoGoverno = new JRDesignExpression();
        expressionLogoGoverno.setText("$P{LOGO_GOVERNO}");
        logoGoverno.setExpression(expressionLogoGoverno);

        // Título Programa
        JRDesignStaticText tituloPrograma = new JRDesignStaticText(jasperDesign);
        tituloPrograma.setX(2);
        tituloPrograma.setY(0);
        tituloPrograma.setWidth(553);
        tituloPrograma.setHeight(39);
        tituloPrograma.setForecolor(Color.decode("#333333"));
        tituloPrograma.setFontSize(Float.valueOf(14.00F));
        tituloPrograma.setBold(Boolean.TRUE);
        tituloPrograma.setHorizontalTextAlign(HorizontalTextAlignEnum.CENTER);
        tituloPrograma.setVerticalTextAlign(VerticalTextAlignEnum.TOP);
        tituloPrograma.setText("Extrato Projeto\n" + programa);

        // Inserir logos
        titleBand.addElement(logoSistema);
        titleBand.addElement(logoGoverno);
        titleBand.addElement(tituloPrograma);
    }

    private void inserirDadosDoAgricultor(JasperDesign jasperDesign, JRDesignBand titleBand) {
        // Dados do Agricultor
        JRDesignStaticText dadosDoAgricultor = new JRDesignStaticText(jasperDesign);
        dadosDoAgricultor.setMode(ModeEnum.TRANSPARENT);
        dadosDoAgricultor.setX(-10);
        dadosDoAgricultor.setY(50);
        dadosDoAgricultor.setWidth(575);
        dadosDoAgricultor.setHeight(20);
        dadosDoAgricultor.setForecolor(Color.decode("#FFFFFF"));
        dadosDoAgricultor.setBackcolor(Color.decode("#0F0F0F"));
        dadosDoAgricultor.setHorizontalTextAlign(HorizontalTextAlignEnum.CENTER);
        dadosDoAgricultor.setVerticalTextAlign(VerticalTextAlignEnum.MIDDLE);
        dadosDoAgricultor.setFontName("SansSerif");
        dadosDoAgricultor.setFontSize(Float.valueOf(14.00F));
        dadosDoAgricultor.setBold(Boolean.TRUE);
        dadosDoAgricultor.setText("Dados do Agricultor");


        // Field Nome do Agricultor
        JRDesignStaticText fieldNomeAgricultor = new JRDesignStaticText(jasperDesign);
        fieldNomeAgricultor.setX(1);
        fieldNomeAgricultor.setY(79);
        fieldNomeAgricultor.setWidth(214);
        fieldNomeAgricultor.setHeight(21);
        fieldNomeAgricultor.setFontSize(Float.valueOf(10.00F));
        fieldNomeAgricultor.setBold(Boolean.TRUE);
        fieldNomeAgricultor.setText("Nome");

        // Nome do Agricultor
        JRDesignTextField nomeAgricultor = new JRDesignTextField(jasperDesign);
        nomeAgricultor.setBlankWhenNull(Boolean.TRUE);
        nomeAgricultor.setStretchWithOverflow(Boolean.TRUE);
        nomeAgricultor.setX(1);
        nomeAgricultor.setY(96);
        nomeAgricultor.setWidth(214);
        nomeAgricultor.setHeight(20);
        nomeAgricultor.setFontSize(Float.valueOf(12.00F));
        JRDesignExpression expressionNomeAgricultor = new JRDesignExpression();
        expressionNomeAgricultor.setText("$F{nome}");
        nomeAgricultor.setExpression(expressionNomeAgricultor);


        // Field RG do Agricultor
        JRDesignStaticText fieldRGAgricultor = new JRDesignStaticText(jasperDesign);
        fieldRGAgricultor.setX(228);
        fieldRGAgricultor.setY(79);
        fieldRGAgricultor.setWidth(155);
        fieldRGAgricultor.setHeight(21);
        fieldRGAgricultor.setFontSize(Float.valueOf(10.00F));
        fieldRGAgricultor.setBold(Boolean.TRUE);
        fieldRGAgricultor.setText("RG");

        // RG do Agricultor
        JRDesignTextField rgAgricultor = new JRDesignTextField(jasperDesign);
        rgAgricultor.setBlankWhenNull(Boolean.TRUE);
        rgAgricultor.setX(228);
        rgAgricultor.setY(96);
        rgAgricultor.setWidth(155);
        rgAgricultor.setHeight(20);
        rgAgricultor.setFontSize(Float.valueOf(12.00F));
        JRDesignExpression expressionRGAgricultor = new JRDesignExpression();
        expressionRGAgricultor.setText("$F{rg}");
        rgAgricultor.setExpression(expressionRGAgricultor);


        // Field CPF do Agricultor
        JRDesignStaticText fieldCpfAgricultor = new JRDesignStaticText(jasperDesign);
        fieldCpfAgricultor.setX(400);
        fieldCpfAgricultor.setY(79);
        fieldCpfAgricultor.setWidth(155);
        fieldCpfAgricultor.setHeight(21);
        fieldCpfAgricultor.setFontSize(Float.valueOf(10.00F));
        fieldCpfAgricultor.setBold(Boolean.TRUE);
        fieldCpfAgricultor.setText("CPF");

        // CPF do Agricultor
        JRDesignTextField cpfAgricultor = new JRDesignTextField(jasperDesign);
        cpfAgricultor.setBlankWhenNull(Boolean.TRUE);
        cpfAgricultor.setX(400);
        cpfAgricultor.setY(96);
        cpfAgricultor.setWidth(155);
        cpfAgricultor.setHeight(20);
        cpfAgricultor.setFontSize(Float.valueOf(12.00F));
        JRDesignExpression expressionCpfAgricultor = new JRDesignExpression();
        expressionCpfAgricultor.setText("$F{cpf}");
        cpfAgricultor.setExpression(expressionCpfAgricultor);


        // Field DAP do Agricultor
        JRDesignStaticText fieldDAPAgricultor = new JRDesignStaticText(jasperDesign);
        fieldDAPAgricultor.setX(1);
        fieldDAPAgricultor.setY(126);
        fieldDAPAgricultor.setWidth(214);
        fieldDAPAgricultor.setHeight(21);
        fieldDAPAgricultor.setFontSize(Float.valueOf(10.00F));
        fieldDAPAgricultor.setBold(Boolean.TRUE);
        fieldDAPAgricultor.setText("DAP");

        // DAP do Agricultor
        JRDesignTextField dapAgricultor = new JRDesignTextField(jasperDesign);
        dapAgricultor.setBlankWhenNull(Boolean.TRUE);
        dapAgricultor.setX(2);
        dapAgricultor.setY(142);
        dapAgricultor.setWidth(214);
        dapAgricultor.setHeight(20);
        dapAgricultor.setFontSize(Float.valueOf(12.00F));
        JRDesignExpression expressionDAPAgricultor = new JRDesignExpression();
        expressionDAPAgricultor.setText("$F{dap}");
        dapAgricultor.setExpression(expressionDAPAgricultor);


        // Field Ano do Cadastro do Agricultor
        JRDesignStaticText fieldAnoCadastroAgricultor = new JRDesignStaticText(jasperDesign);
        fieldAnoCadastroAgricultor.setX(228);
        fieldAnoCadastroAgricultor.setY(126);
        fieldAnoCadastroAgricultor.setWidth(155);
        fieldAnoCadastroAgricultor.setHeight(21);
        fieldAnoCadastroAgricultor.setFontSize(Float.valueOf(10.00F));
        fieldAnoCadastroAgricultor.setBold(Boolean.TRUE);
        fieldAnoCadastroAgricultor.setText("Ano do Cadastro");

        // Ano do Cadastro do Agricultor
        JRDesignTextField anoCadastroAgricultor = new JRDesignTextField(jasperDesign);
        anoCadastroAgricultor.setBlankWhenNull(Boolean.TRUE);
        anoCadastroAgricultor.setX(228);
        anoCadastroAgricultor.setY(142);
        anoCadastroAgricultor.setWidth(155);
        anoCadastroAgricultor.setHeight(20);
        anoCadastroAgricultor.setFontSize(Float.valueOf(12.00F));
        JRDesignExpression expressionAnoCadastroAgricultor = new JRDesignExpression();
        expressionAnoCadastroAgricultor.setText("$F{ano_cadastro}");
        anoCadastroAgricultor.setExpression(expressionAnoCadastroAgricultor);


        // Field Município do Agricultor
        JRDesignStaticText fieldMunicipioAgricultor = new JRDesignStaticText(jasperDesign);
        fieldMunicipioAgricultor.setX(400);
        fieldMunicipioAgricultor.setY(126);
        fieldMunicipioAgricultor.setWidth(155);
        fieldMunicipioAgricultor.setHeight(21);
        fieldMunicipioAgricultor.setFontSize(Float.valueOf(10.00F));
        fieldMunicipioAgricultor.setBold(Boolean.TRUE);
        fieldMunicipioAgricultor.setText("Município");

        // Município do Agricultor
        JRDesignTextField municipioAgricultor = new JRDesignTextField(jasperDesign);
        municipioAgricultor.setBlankWhenNull(Boolean.TRUE);
        municipioAgricultor.setStretchWithOverflow(Boolean.TRUE);
        municipioAgricultor.setX(400);
        municipioAgricultor.setY(142);
        municipioAgricultor.setWidth(155);
        municipioAgricultor.setHeight(20);
        municipioAgricultor.setFontSize(Float.valueOf(12.00F));
        JRDesignExpression expressionMunicipioAgricultor = new JRDesignExpression();
        expressionMunicipioAgricultor.setText("$F{municipio}");
        municipioAgricultor.setExpression(expressionMunicipioAgricultor);


        // Retângulo dados do agricultor
        JRDesignRectangle rectangleDadosDoAgricultor = new JRDesignRectangle(jasperDesign);
        rectangleDadosDoAgricultor.setX(-10);
        rectangleDadosDoAgricultor.setY(50);
        rectangleDadosDoAgricultor.setWidth(575);
        rectangleDadosDoAgricultor.setHeight(20);
        rectangleDadosDoAgricultor.setForecolor(Color.WHITE);
        rectangleDadosDoAgricultor.setBackcolor(Color.decode("#9CC96B"));


        // Inserir Dados do Agricultor
        titleBand.addElement(rectangleDadosDoAgricultor);
        titleBand.addElement(dadosDoAgricultor);
        titleBand.addElement(fieldNomeAgricultor);
        titleBand.addElement(nomeAgricultor);
        titleBand.addElement(fieldCpfAgricultor);
        titleBand.addElement(cpfAgricultor);
        titleBand.addElement(fieldRGAgricultor);
        titleBand.addElement(rgAgricultor);
        titleBand.addElement(fieldDAPAgricultor);
        titleBand.addElement(dapAgricultor);
        titleBand.addElement(fieldAnoCadastroAgricultor);
        titleBand.addElement(anoCadastroAgricultor);
        titleBand.addElement(fieldMunicipioAgricultor);
        titleBand.addElement(municipioAgricultor);

    }

    private void inserirDadosDoConjuge(JasperDesign jasperDesign, JRDesignBand titleBand) {
        titleBand.setHeight(240);

        // Dados do Cônjuge
        JRDesignStaticText dadosDoConjuge = new JRDesignStaticText(jasperDesign);
        dadosDoConjuge.setMode(ModeEnum.TRANSPARENT);
        dadosDoConjuge.setX(-10);
        dadosDoConjuge.setY(170);
        dadosDoConjuge.setWidth(575);
        dadosDoConjuge.setHeight(20);
        dadosDoConjuge.setForecolor(Color.decode("#FFFFFF"));
        dadosDoConjuge.setBackcolor(Color.decode("#0F0F0F"));
        dadosDoConjuge.setFontSize(Float.valueOf(14.00F));
        dadosDoConjuge.setBold(Boolean.TRUE);
        dadosDoConjuge.setHorizontalTextAlign(HorizontalTextAlignEnum.CENTER);
        dadosDoConjuge.setVerticalTextAlign(VerticalTextAlignEnum.MIDDLE);
        dadosDoConjuge.setFontName("SansSerif");
        dadosDoConjuge.setText("Dados do Cônjuge");

        // Field Nome do Cônjuge
        JRDesignStaticText fieldNomeConjuge = new JRDesignStaticText(jasperDesign);
        fieldNomeConjuge.setX(2);
        fieldNomeConjuge.setY(199);
        fieldNomeConjuge.setWidth(214);
        fieldNomeConjuge.setHeight(21);
        fieldNomeConjuge.setFontSize(Float.valueOf(10.00F));
        fieldNomeConjuge.setBold(Boolean.TRUE);
        fieldNomeConjuge.setText("Nome");

        //Nome do Cônjuge
        JRDesignTextField nomeConjuge = new JRDesignTextField(jasperDesign);
        nomeConjuge.setBlankWhenNull(Boolean.TRUE);
        nomeConjuge.setStretchWithOverflow(Boolean.TRUE);
        nomeConjuge.setX(2);
        nomeConjuge.setY(218);
        nomeConjuge.setWidth(214);
        nomeConjuge.setHeight(20);
        nomeConjuge.setFontSize(Float.valueOf(12.00F));
        JRDesignExpression expressionNomeConjuge = new JRDesignExpression();
        expressionNomeConjuge.setText("$F{conjuge}");
        nomeConjuge.setExpression(expressionNomeConjuge);

        // Field CPF do Cônjuge
        JRDesignStaticText fieldCpfConjuge = new JRDesignStaticText(jasperDesign);
        fieldCpfConjuge.setX(400);
        fieldCpfConjuge.setY(199);
        fieldCpfConjuge.setWidth(155);
        fieldCpfConjuge.setHeight(21);
        fieldCpfConjuge.setFontSize(Float.valueOf(10.00F));
        fieldCpfConjuge.setBold(Boolean.TRUE);
        fieldCpfConjuge.setText("CPF");

        // CPF do Cônjuge
        JRDesignTextField cpfConjuge = new JRDesignTextField(jasperDesign);
        cpfConjuge.setBlankWhenNull(Boolean.TRUE);
        cpfConjuge.setX(400);
        cpfConjuge.setY(218);
        cpfConjuge.setWidth(155);
        cpfConjuge.setHeight(20);
        cpfConjuge.setFontSize(Float.valueOf(12.00F));
        JRDesignExpression expressionCpfConjuge = new JRDesignExpression();
        expressionCpfConjuge.setText("$F{cpf_conjuge}");
        cpfConjuge.setExpression(expressionCpfConjuge);

        // Field RG do Cônjuge
        JRDesignStaticText fieldRGConjuge = new JRDesignStaticText(jasperDesign);
        fieldRGConjuge.setX(228);
        fieldRGConjuge.setY(199);
        fieldRGConjuge.setWidth(155);
        fieldRGConjuge.setHeight(20);
        fieldRGConjuge.setFontSize(Float.valueOf(10.00F));
        fieldRGConjuge.setBold(Boolean.TRUE);
        fieldRGConjuge.setText("RG");

        // RG do Cônjuge
        JRDesignTextField rgConjuge = new JRDesignTextField(jasperDesign);
        rgConjuge.setBlankWhenNull(Boolean.TRUE);
        rgConjuge.setX(228);
        rgConjuge.setY(218);
        rgConjuge.setWidth(155);
        rgConjuge.setHeight(20);
        rgConjuge.setFontSize(Float.valueOf(12.00F));
        JRDesignExpression expressionRGConjuge = new JRDesignExpression();
        expressionCpfConjuge.setText("$F{rg_conjuge}");
        rgConjuge.setExpression(expressionRGConjuge);


        JRDesignRectangle rectangleDadosDoConjuge = new JRDesignRectangle(jasperDesign);
        rectangleDadosDoConjuge.setX(-10);
        rectangleDadosDoConjuge.setY(170);
        rectangleDadosDoConjuge.setWidth(575);
        rectangleDadosDoConjuge.setHeight(20);
        rectangleDadosDoConjuge.setForecolor(Color.WHITE);
        rectangleDadosDoConjuge.setBackcolor(Color.decode("#9CC96B"));

        // Inserir Dados do Cônjuge
        titleBand.addElement(rectangleDadosDoConjuge);
        titleBand.addElement(dadosDoConjuge);
        titleBand.addElement(fieldNomeConjuge);
        titleBand.addElement(nomeConjuge);
        titleBand.addElement(fieldCpfConjuge);
        titleBand.addElement(cpfConjuge);
        titleBand.addElement(fieldRGConjuge);
        titleBand.addElement(rgConjuge);
    }

    public JasperPrint gerarRelatorioAnaliticoParceiras(FiltroRelatorioAnaliticoParceiraDTO filtro) throws SQLException, JRException {
        String path = PATH_RELATORIOS + "RelatorioEntidadeParceira.jasper";
        Map<String, Object> params = fillParametersForRelatorioAnaliticoParceiras(filtro);
        return this.gerarRelatorio(path, params);
    }

    public byte[] gerarRelatorioXlsAnaliticoParceiras(FiltroRelatorioAnaliticoParceiraDTO filtro) throws Exception {
        String path = PATH_RELATORIOS + "xls/entidade-parceira/RelatorioEntidadeParceiraXls.jasper";
        Map<String, Object> params = fillParametersForRelatorioAnaliticoParceiras(filtro);
        return this.gerarXls(path, params);
    }

    private Map<String, Object> fillParametersForRelatorioAnaliticoParceiras(FiltroRelatorioAnaliticoParceiraDTO filtro) {
        Map<String, Object> params = new HashMap<>();

        params.put("PARAMETRO_PARCEIRA", filtro.getParceira());
        params.put("PARAMETRO_AREA", filtro.isArea());
        params.put("PARAMETRO_EQUIPE", filtro.isEquipe());
        params.put("PARAMETRO_METAS", filtro.isMetas());
        params.put("PARAMETRO_PLANOS", filtro.isPlanos());

        params.put("SUBREPORT_DIR_TECNICOS", PATH_RELATORIOS + "RelatorioEntidadeParceira_TecnicosSubReport.jasper");
        params.put("SUBREPORT_DIR_ASSESSORES", PATH_RELATORIOS + "RelatorioEntidadeParceira_AssessoresSubReport.jasper");
        params.put("SUBREPORT_DIR_METAS", PATH_RELATORIOS + "RelatorioEntidadeParceira_MetasSubReport.jasper");
        params.put("SUBREPORT_DIR_AREA_ATUACAO", PATH_RELATORIOS + "RelatorioEntidadeParceira_AreaAtuacaoSubReport.jasper");
        params.put("SUBREPORT_DIR_PLANOS_TRIMESTRAIS", PATH_RELATORIOS + "RelatorioEntidadeParceira_PlanosTrimestraisSubReporte.jasper");
        params.put("SUBREPORT_DIR_EQUIPE", PATH_RELATORIOS + "RelatorioEntidadeParceira_EquipeSubReport.jasper");

        params.put(LOGO_SISTEMA, this.getClass().getClassLoader().getResource(PATH_IMAGES + LOGO_PAULO_FREIRE_PNG).getPath());
        params.put(LOGO_GOVERNO, this.getClass().getClassLoader().getResource(PATH_IMAGES + LOGO_GOVERNO_PNG).getPath());

        return params;
    }

}
