package br.gov.ce.sda.service;

import br.gov.ce.sda.web.rest.dto.PowerBIRequestReportDTO;
import br.gov.ce.sda.web.rest.dto.PowerBIResponseReportDTO;
import br.gov.ce.sda.web.rest.dto.PowerBITokenDTO;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;

@Service
@RequiredArgsConstructor
public class PowerBiService {

    public PowerBITokenDTO getToken() throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("https://login.microsoftonline.com/common/oauth2/token");

        httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");

        NameValuePair[] data = {
            new BasicNameValuePair("grant_type", "password"),
            new BasicNameValuePair("client_id", "1beaa76d-b26c-480c-a4a8-f38de2ce0943"),
            new BasicNameValuePair("resource", "https://analysis.windows.net/powerbi/api"),
            new BasicNameValuePair("scope", "openid"),
            new BasicNameValuePair("username", "ideiaepratica@ideiaepratica.com.br"),
            new BasicNameValuePair("password", "OAdA1975"),
        };

        httpPost.setEntity(new UrlEncodedFormEntity(Arrays.asList(data), HTTP.UTF_8));

        CloseableHttpResponse response = client.execute(httpPost);

        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), PowerBITokenDTO.class);
    }

    public PowerBIResponseReportDTO getReport(PowerBIRequestReportDTO powerBIRequestReportDTO) throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("https://api.powerbi.com/v1.0/myorg/" +
            "groups/" + powerBIRequestReportDTO.getGroups() +
            "/reports/" + powerBIRequestReportDTO.getReports() + "/GenerateToken");

        httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");
        httpPost.setHeader("Authorization", "Bearer " + powerBIRequestReportDTO.getToken());

        NameValuePair[] data = {
            new BasicNameValuePair("accessLevel", "View")
        };

        httpPost.setEntity(new UrlEncodedFormEntity(Arrays.asList(data), HTTP.UTF_8));

        CloseableHttpResponse response = client.execute(httpPost);

        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), PowerBIResponseReportDTO.class);
    }
}
