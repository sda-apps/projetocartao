package br.gov.ce.sda.service;


import br.gov.ce.sda.domain.ater.AtividadePTA;
import br.gov.ce.sda.repository.ater.AtividadeRepository;
import br.gov.ce.sda.repository.ater.CustomAtividadeRepository;
import br.gov.ce.sda.web.rest.dto.FiltroAtividadeDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

@Service
public class AtividadeService {

    @Inject
    private AtividadeRepository atividadeRepository;

    @Inject
    private CustomAtividadeRepository customAtividadeRepository;

    @Transactional
    public AtividadePTA save(AtividadePTA atividade) {
        AtividadePTA savedAtividade = atividadeRepository.save(atividade);
        return savedAtividade;
    }

    @Transactional(readOnly = true)
    public List<AtividadePTA> findAll() {
        return atividadeRepository.findAll();
    }

    @Transactional
    public void update(AtividadePTA currentAtividade) {
        atividadeRepository.save(currentAtividade);
    }

    @Transactional(readOnly = true)
    public AtividadePTA findById(Long id) {
        return atividadeRepository.findOneById(id);
    }

    @Transactional
    public void delete(Long id) {
        atividadeRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public List<AtividadePTA> findAllByFilter(FiltroAtividadeDTO filtro){
        return customAtividadeRepository.findAllByFilter(filtro);
    }

}
