package br.gov.ce.sda.service;


import br.gov.ce.sda.domain.ater.Acao;
import br.gov.ce.sda.domain.ater.AtividadePTA;
import br.gov.ce.sda.repository.ater.AcaoRepository;
import br.gov.ce.sda.repository.ater.AtividadeRepository;
import br.gov.ce.sda.repository.ater.CustomAcaoRepository;
import br.gov.ce.sda.repository.ater.CustomAtividadeRepository;
import br.gov.ce.sda.web.rest.dto.FiltroAcaoDTO;
import br.gov.ce.sda.web.rest.dto.FiltroAtividadeDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

@Service
public class AcaoService {

    @Inject
    private AcaoRepository acaoRepository;

    @Inject
    private CustomAcaoRepository customAcaoRepository;

    @Transactional
    public Acao save(Acao acao) {
        Acao savedAcao = acaoRepository.save(acao);
        return savedAcao;
    }

    @Transactional(readOnly = true)
    public List<Acao> findAll() {
        return acaoRepository.findAll();
    }

    @Transactional
    public void update(Acao currentAcao) {
        acaoRepository.save(currentAcao);
    }

    @Transactional
    public void delete(Long id) {
        acaoRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public List<Acao> findAllByFilter(FiltroAcaoDTO filtro){
        return customAcaoRepository.findAllByFilter(filtro);
    }

    @Transactional(readOnly = true)
    public Acao findById(Long id) {
        return acaoRepository.findOneById(id);
    }

}
