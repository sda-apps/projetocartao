package br.gov.ce.sda.service.ater;


import br.gov.ce.sda.domain.ater.EixoTematico;
import br.gov.ce.sda.repository.ater.CustomEixoTematicoRepository;
import br.gov.ce.sda.repository.ater.EixoTematicoRepository;
import br.gov.ce.sda.web.rest.dto.FiltroEixoTematicoDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

@Service
public class EixoTematicoService {

    @Inject
    private EixoTematicoRepository eixoTematicoRepository;

    @Inject
    private CustomEixoTematicoRepository customEixoTematicoRepository;

    @Transactional(readOnly = true)
    public List<EixoTematico> findAll() {
        return eixoTematicoRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<EixoTematico> findAllByFilter(FiltroEixoTematicoDTO filtro) {
        return customEixoTematicoRepository.findAllByFilter(filtro);
    }

    @Transactional(readOnly = true)
    public EixoTematico findById(Long id) {
        return eixoTematicoRepository.findOneById(id);
    }

    @Transactional
    public EixoTematico save(EixoTematico eixoTematico) {
        EixoTematico savedEixoTematico = eixoTematicoRepository.save(eixoTematico);
        return savedEixoTematico;
    }

    @Transactional
    public void update(EixoTematico currentEixoTematico) {
        eixoTematicoRepository.save(currentEixoTematico);
    }

    @Transactional
    public void delete(Long id) {
        eixoTematicoRepository.deleteById(id);
    }

}
