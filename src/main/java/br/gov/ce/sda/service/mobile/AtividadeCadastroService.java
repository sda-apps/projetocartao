package br.gov.ce.sda.service.mobile;

import br.gov.ce.sda.domain.*;
import br.gov.ce.sda.domain.ater.Regiao;
import br.gov.ce.sda.domain.mapa.Coordenada;
import br.gov.ce.sda.domain.mapa.Elemento;
import br.gov.ce.sda.domain.mobile.*;
import br.gov.ce.sda.repository.*;
import br.gov.ce.sda.repository.ater.CustomAbrangenciaRegiaoRepository;
import br.gov.ce.sda.repository.mobile.AtividadeCadastroRepository;
import br.gov.ce.sda.repository.mobile.CustomAtividadeCadastroRepository;
import br.gov.ce.sda.repository.mobile.CustomParenteMobileRepository;
import br.gov.ce.sda.service.ida.DataService;
import br.gov.ce.sda.web.rest.dto.mobile.FiltroCadastroDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static br.gov.ce.sda.service.util.DateUtil.getIdade;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Service
public class AtividadeCadastroService {

    private static final Character TIPO_ELEMENTO_PROPRIEDADE = 'P';

    private final CustomAtividadeCadastroRepository customAtividadeCadastroRepository;
    private final CustomAgricultorRepository customAgricultorRepository;
    private final CustomFamiliaRepository customFamiliaRepository;
    private final CustomParenteRepository customParenteRepository;
    private final ParenteRepository parenteRepository;
    private final TipoElementoRepository tipoElementoRepository;
    private final AgricultorRepository agricultorRepository;
    private final PessoaFisicaRepository pessoaFisicaRepository;
    private final CustomParenteMobileRepository customParenteMobileRepository;
    private final ElementoRepository elementoRepository;
    private final CoordenadaRepository coordenadaRepository;
    private final CustomLocalidadeRepository customLocalidadeRepository;
    private final CustomAbrangenciaRegiaoRepository customAbrangenciaRegiaoRepository;
    private final AtividadeCadastroRepository atividadeCadastroRepository;
    private final DataService dataService;

    public AtividadeCadastroService(
        CustomAtividadeCadastroRepository customAtividadeCadastroRepository,
        CustomAgricultorRepository customAgricultorRepository,
        CustomFamiliaRepository customFamiliaRepository,
        CustomParenteRepository customParenteRepository,
        ParenteRepository parenteRepository,
        TipoElementoRepository tipoElementoRepository,
        AgricultorRepository agricultorRepository,
        PessoaFisicaRepository pessoaFisicaRepository,
        CustomParenteMobileRepository customParenteMobileRepository,
        ElementoRepository elementoRepository,
        CoordenadaRepository coordenadaRepository,
        CustomLocalidadeRepository customLocalidadeRepository,
        CustomAbrangenciaRegiaoRepository customAbrangenciaRegiaoRepository,
        AtividadeCadastroRepository atividadeCadastroRepository,
        DataService dataService
    ) {
        this.customAtividadeCadastroRepository = customAtividadeCadastroRepository;
        this.customAgricultorRepository = customAgricultorRepository;
        this.customFamiliaRepository = customFamiliaRepository;
        this.customParenteRepository = customParenteRepository;
        this.parenteRepository = parenteRepository;
        this.tipoElementoRepository = tipoElementoRepository;
        this.agricultorRepository = agricultorRepository;
        this.pessoaFisicaRepository = pessoaFisicaRepository;
        this.customParenteMobileRepository = customParenteMobileRepository;
        this.elementoRepository = elementoRepository;
        this.coordenadaRepository = coordenadaRepository;
        this.customLocalidadeRepository = customLocalidadeRepository;
        this.customAbrangenciaRegiaoRepository = customAbrangenciaRegiaoRepository;
        this.atividadeCadastroRepository = atividadeCadastroRepository;
        this.dataService = dataService;
    }

    public List<AtividadeCadastro> findByFilters(FiltroCadastroDTO filters) {
		return customAtividadeCadastroRepository.findByFilters(filters);
	}

    public ResponseEntity<AtividadeCadastro> aprovarAutomaticamentePor(Regiao regiao) {
        List<Long> municipiosByRegiao = customAbrangenciaRegiaoRepository.getMunicipiosByRegiao(regiao.getId());

        findByFilters(
            FiltroCadastroDTO.builder().municipios(municipiosByRegiao).status("T").start(0).limit(1).build())
            .forEach(atividadeCadastro -> {
                atividadeCadastro.setStatus(AtividadeCadastro.Status.ACEITO.getStatus());
                aprovarCadastro(atividadeCadastro.getId(), atividadeCadastro);
            });

        /*List<AtividadeCadastro> list = atividadeCadastroService.findByFilters(
            FiltroCadastroDTO.builder().status("T").start(start).limit(limit).build());

        if(list.size() > 0) {
            for (AtividadeCadastro atividadeCadastro : list) {
                atividadeCadastro = customAtividadeCadastroRepository.buscarPorId(atividadeCadastro.getId());
                atividadeCadastro.setStatus(AtividadeCadastro.Status.ACEITO.getStatus());
                atividadeCadastroService.aprovarCadastro(atividadeCadastro.getId(), atividadeCadastro);
            }

            System.out.println("**********------------Nova rodada----------------************");
            aprovarAutomaticamentePorRegiao(nomeRegiao, start, limit);

        }*/

        return ResponseEntity.ok().build();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
	public AtividadeCadastro aprovarCadastro(Long id, AtividadeCadastro atividadeCadastro) {
        AtividadeCadastro atividadeCadastroAtual = customAtividadeCadastroRepository.buscarPorId(id);

        if(nonNull(atividadeCadastroAtual)) {
            atividadeCadastroAtual.setStatus(atividadeCadastro.getStatus());
            atividadeCadastroAtual.setRecusado(atividadeCadastro.getRecusado());
            atividadeCadastroAtual.setObservacao(atividadeCadastro.getObservacao());

            if (atividadeCadastro.getStatus().equals(AtividadeCadastro.Status.ACEITO.getStatus())) {
                atualizarAgricultor(atividadeCadastroAtual);

                atividadeCadastroAtual.setRecusado(Boolean.FALSE);
            }
        }

        atividadeCadastroRepository.save(atividadeCadastroAtual);

        dataService.enviarInformacoesATERParaIDA(atividadeCadastroAtual);

        return atividadeCadastroAtual;
    }

    @Transactional(propagation = Propagation.NESTED)
	public void atualizarAgricultor(AtividadeCadastro atividadeCadastro) {
        /** Atualização do Agricultor
         *  ◦ Verifica se o agricultor está no banco do portal
         *      - Se sim, atualiza dados do agricultor
         *      - Se não, inclui novo agricultor
         * */

        AgricultorMobile agricultorMobile = atividadeCadastro.getInformacaoInicial().getAgricultor();
        Agricultor agricultorPortal = customAgricultorRepository.buscarAgricultorCompletoPorCpf(agricultorMobile.getPessoaFisica().getCpf());

        if(nonNull(agricultorPortal)) {
            PessoaFisica pessoaFisicaPortal = agricultorPortal.getPessoaFisica();

            setPessoaPorAtividadeCadastro(agricultorMobile, pessoaFisicaPortal);

            setAgricultorPorAtividadeCadastro(agricultorMobile, agricultorPortal);

			setFamiliaPorAtividadeCadastro(atividadeCadastro, agricultorPortal);

			setTelefonesPorAtividadeCadastro(agricultorMobile, pessoaFisicaPortal.getPessoa());

			agricultorRepository.save(agricultorPortal);

		} else {
			Agricultor novoAgricultor = new Agricultor();

			PessoaFisica novaPessoaFisica = pessoaFisicaRepository.findByCpf(agricultorMobile.getPessoaFisica().getCpf());

			if(isNull(novaPessoaFisica)) {
				novaPessoaFisica = new PessoaFisica();
				novaPessoaFisica.setPessoa(new Pessoa());
			}

			novoAgricultor.setPessoaFisica(novaPessoaFisica);

            setPessoaPorAtividadeCadastro(agricultorMobile, novaPessoaFisica);

            setAgricultorPorAtividadeCadastro(agricultorMobile, novoAgricultor);

			setFamiliaPorAtividadeCadastro(atividadeCadastro, novoAgricultor);

			setTelefonesPorAtividadeCadastro(agricultorMobile, novaPessoaFisica.getPessoa());

			agricultorRepository.save(novoAgricultor);

		}

	}

    @Transactional(propagation = Propagation.NESTED)
	private void setTelefonesPorAtividadeCadastro(AgricultorMobile agricultorMobile, Pessoa pessoaPortal) {
	    if(pessoaPortal.getTelefones() != null && !pessoaPortal.getTelefones().isEmpty()) {
            pessoaPortal.getTelefones().clear();
        }

		agricultorMobile.getPessoaFisica().getPessoa().getTelefones().forEach(telefone -> {
			Telefone novoTelefone = new Telefone();
			novoTelefone.setContato(telefone.getContato());
			novoTelefone.setNumero(telefone.getNumero());
			novoTelefone.setPessoa(pessoaPortal);
			novoTelefone.setTipo(telefone.getTipo());

			if(isNull(pessoaPortal.getTelefones())) {
			    pessoaPortal.setTelefones(new HashSet<>());
            }

            pessoaPortal.getTelefones().add(novoTelefone);
		});
	}

    @Transactional(propagation = Propagation.NESTED)
	private void setFamiliaPorAtividadeCadastro(AtividadeCadastro atividadeCadastro, Agricultor agricultor) {
		if(nonNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getFamilia()) &&
            nonNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getFamilia().getId())) {
            /** Atualização da Família
             *  ◦ Verifica se há família no banco do portal
             *      - Se sim, atualiza os membros da família do agricultor
             *          - Verifica se há conjuge e verifica se ainda é o mesmo
             *          - Se não há conjuge e havia antes no banco, eles se separaram
             *      - Se não, inclui uma nova família
             * */

			FamiliaMobile familiaMobile = atividadeCadastro.getInformacaoInicial().getAgricultor().getFamilia();

            Familia familiaPortal = customFamiliaRepository.findByCpfChefe(agricultor.getPessoaFisica().getCpf());

			if(nonNull(familiaPortal) && nonNull(familiaPortal.getId())) {
				Parente conjugeBanco = customParenteRepository.findConjugeByFamiliaId(familiaPortal.getId());

				// atualizar conjuge
				if(nonNull(conjugeBanco) && conjugeBanco.getDataFimRelacao() != null) {
                    Boolean haConjugeMobile = false;

                    for(ParenteMobile parenteMobile : familiaMobile.getParentes()) {
                        if(parenteMobile.getParentesco().equals(Parente.Parentesco.CONJUJE)) { // procura se há conjuge e verifica se ainda é o mesmo do banco
                            haConjugeMobile = true;

                            if(!parenteMobile.getAgricultor().getPessoaFisica().getCpf().equals(conjugeBanco.getAgricultor().getPessoaFisica().getCpf())) {
                                parenteMobile.setDataFimRelacao(new Date());
                                conjugeBanco.setDataFimRelacao(new Date());

                                //parenteRepository.save(conjugeBanco);
                            }
                        }
                    }

                    if(haConjugeMobile == false) {  // separaram-se
                        conjugeBanco.setDataFimRelacao(new Date());

                        //parenteRepository.save(conjugeBanco);
                    }
				}

                // atualiza membros
                for(ParenteMobile parenteMobile : familiaMobile.getParentes()) {
				    Boolean haParente = false;
                    parenteMobile = customParenteRepository.findByIdMobile(parenteMobile.getId());

                    forParentesPortal : for(Parente parentePortal : familiaPortal.getParentes()) {

                        if(parenteMobile.getAgricultor().getPessoaFisica().getCpf().equals(parentePortal.getAgricultor().getPessoaFisica().getCpf())) {
                            //atualizar dados do parente
                            parentePortal.setParentesco(parenteMobile.getParentesco());
                            setAgricultorPorAtividadeCadastro(parenteMobile.getAgricultor(), parentePortal.getAgricultor());
                            setPessoaPorAtividadeCadastro(parenteMobile.getAgricultor(), parentePortal.getAgricultor().getPessoaFisica());

                            //parenteRepository.save(parentePortal);

                            haParente = true;

                            break forParentesPortal;
                        }
                    }

                    // adiciona novo parente
                    if(!haParente) {
                        salvarNovoParente(familiaPortal, parenteMobile, atividadeCadastro);
                    }
                }
			} else {
				constituirNovaFamilia(agricultor, familiaMobile, atividadeCadastro);
			}
		}
	}

    @Transactional(propagation = Propagation.NESTED)
	private void setPessoaPorAtividadeCadastro(AgricultorMobile agricultorMobile, PessoaFisica pessoaFisicaPortal) {
		Pessoa pessoaPortal = pessoaFisicaPortal.getPessoa();
		PessoaMobile pessoaMobile = agricultorMobile.getPessoaFisica().getPessoa();

        pessoaPortal.setNome(nonNull(pessoaMobile.getNome()) ? pessoaMobile.getNome().trim().toUpperCase() : null);
        pessoaPortal.setApelido(nonNull(pessoaMobile.getApelido()) ? pessoaMobile.getApelido().trim().toUpperCase() : null);
        pessoaPortal.setFoto(pessoaMobile.getFoto());
        pessoaPortal.setAvatar(pessoaMobile.getAvatar());
        pessoaPortal.setEmail(nonNull(pessoaMobile.getEmail()) ? pessoaMobile.getEmail().trim() : null);
        setEnderecoPessoa(pessoaPortal, pessoaMobile);

        if(nonNull(pessoaMobile.getLocalidade())) {
            Localidade localidade = customLocalidadeRepository.findById(pessoaMobile.getLocalidade().getId());
            pessoaPortal.setComunidade(localidade.getComunidade());
        }

		pessoaFisicaPortal.setCpf(agricultorMobile.getPessoaFisica().getCpf());
		pessoaFisicaPortal.setDataExpedicaoRg(agricultorMobile.getPessoaFisica().getDataExpedicaoRg());
		pessoaFisicaPortal.setDataNascimento(agricultorMobile.getPessoaFisica().getDataNascimento());
		pessoaFisicaPortal.setEscolaridade(agricultorMobile.getPessoaFisica().getEscolaridade());
		pessoaFisicaPortal.setEstadoCivil(agricultorMobile.getPessoaFisica().getEstadoCivil());
		pessoaFisicaPortal.setLocalExpedicaoRg(agricultorMobile.getPessoaFisica().getLocalExpedicaoRg());
		pessoaFisicaPortal.setNacionalidade(agricultorMobile.getPessoaFisica().getNacionalidade());
		pessoaFisicaPortal.setNaturalidade(agricultorMobile.getPessoaFisica().getNaturalidade());
		pessoaFisicaPortal.setNomeMae(nonNull(agricultorMobile.getPessoaFisica().getNomeMae()) ? agricultorMobile.getPessoaFisica().getNomeMae().trim().toUpperCase() : null);
		pessoaFisicaPortal.setNomePai(nonNull(agricultorMobile.getPessoaFisica().getNomePai()) ? agricultorMobile.getPessoaFisica().getNomePai().trim().toUpperCase() : null);
		pessoaFisicaPortal.setOrgaoExpedidorRg(agricultorMobile.getPessoaFisica().getOrgaoExpedidorRg());
		pessoaFisicaPortal.setRg(agricultorMobile.getPessoaFisica().getRg());
		pessoaFisicaPortal.setSexo(agricultorMobile.getPessoaFisica().getSexo());
        pessoaFisicaPortal.setNis(nonNull(agricultorMobile.getPessoaFisica().getNis()) ?  agricultorMobile.getPessoaFisica().getNis().trim().toUpperCase() : null);
        pessoaFisicaPortal.setNit(nonNull(agricultorMobile.getPessoaFisica().getNit()) ?  agricultorMobile.getPessoaFisica().getNit().trim().toUpperCase() : null);

	}

    @Transactional(propagation = Propagation.NESTED)
    private void setAgricultorPorAtividadeCadastro(AgricultorMobile agricultorMobile, Agricultor agricultorPortal) {
		if(nonNull(agricultorMobile.getCoordenada())) {
            setElementoCoordenada(agricultorMobile, agricultorPortal);
        }

		/*if(nonNull(agricultorMobile.getDaps()) && !agricultorMobile.getDaps().isEmpty()) {//TODO Oscar falou que o processo da DAP irá mudar
            Dap dap = null;

		    if(nonNull(agricultorPortal.getId())) {
                dap = dapRepository.findFirstByAgricultor_IdAndNumero(agricultorPortal.getId(), agricultorMobile.getDaps().get(0).getNumero());
            }

			if(isNull(dap)) {
				Dap novaDap = new Dap();
				novaDap.setNumero(agricultorMobile.getDaps().get(0).getNumero());
				novaDap.setAgricultor(agricultorPortal);
				novaDap.setModelo("S/M");

				agricultorPortal.getDaps().add(novaDap);

				dapRepository.save(novaDap);
			}
		}*/

        agricultorPortal.setAtualizacaoProjeto('P');
        agricultorPortal.setAtualizacaoPauloFreire(true);

		//agricultorRepository.save(agricultorPortal);

	}

    private void setElementoCoordenada(AgricultorMobile agricultorMobile, Agricultor agricultorPortal) {
        if (nonNull(agricultorPortal.getId())) {
            Elemento elemento = elementoRepository.findFirstByAgricultor_IdAndTipoElemento_Tipo(agricultorPortal.getId(), TIPO_ELEMENTO_PROPRIEDADE);

            if (isNull(elemento)) {// agricultor sem coordenada do tipo 'P' - propriedade
                salvarNovaCoordenada(agricultorMobile, agricultorPortal);
            }

        } else {
            salvarNovaCoordenada(agricultorMobile, agricultorPortal);
        }
    }

    @Transactional(propagation = Propagation.NESTED)
    private void salvarNovaCoordenada(AgricultorMobile agricultorMobile, Agricultor agricultorPortal) {
        Coordenada coordenada = coordenadaRepository.findFirstByLatitudeAndLongitude(agricultorMobile.getCoordenada().getLatitude(), agricultorMobile.getCoordenada().getLongitude());

        if(nonNull(coordenada)) {
            salvarNovoElemento(agricultorPortal, coordenada);
        } else {
            Coordenada novaCoordenada = new Coordenada();
            novaCoordenada.setLatitude(agricultorMobile.getCoordenada().getLatitude());
            novaCoordenada.setLongitude(agricultorMobile.getCoordenada().getLongitude());

            salvarNovoElemento(agricultorPortal, novaCoordenada);
        }
    }

    @Transactional(propagation = Propagation.NESTED)
    private void salvarNovoElemento(Agricultor agricultorPortal, Coordenada coordenada) {
        Elemento elemento = new Elemento();

        elemento.setAgricultor(agricultorPortal);
        elemento.setCoordenada(coordenada);
        elemento.setTipoElemento(tipoElementoRepository.findByTipo(TIPO_ELEMENTO_PROPRIEDADE));

        Set<Elemento> elementos = new HashSet<>();
        elementos.add(elemento);

        agricultorPortal.setPossuiElemento(Boolean.TRUE);
        agricultorPortal.setElementos(elementos);

	}

    @Transactional(propagation = Propagation.NESTED)
	private void constituirNovaFamilia(Agricultor chefe, FamiliaMobile familiaMobile, AtividadeCadastro atividadeCadastro) {
		Familia novaFamilia = new Familia();
		novaFamilia.setChefe(chefe);
        chefe.setFamilia(novaFamilia);

		for(ParenteMobile parenteMobile : familiaMobile.getParentes()) {
            salvarNovoParente(novaFamilia, parenteMobile, atividadeCadastro);
		}

	}

    @Transactional(propagation = Propagation.NESTED)
    private void salvarNovoParente(Familia novaFamilia, ParenteMobile parenteMobile, AtividadeCadastro atividadeCadastro) {
        parenteMobile = customParenteMobileRepository.findPessoaByParenteId(parenteMobile.getId());

        /**
        * Salva o parente no portal apenas se existir CPF e for maior de 16 anos
        *
         */
        if(nonNull(parenteMobile) &&
            nonNull(parenteMobile.getAgricultor().getPessoaFisica().getCpf()) &&
            getIdade(parenteMobile.getAgricultor().getPessoaFisica().getDataNascimento()) >= 16)
        {
            Parente parentePortal = customParenteRepository.findByCpfPessoaAndParentesco(parenteMobile.getAgricultor().getPessoaFisica().getCpf(), parenteMobile.getParentesco());

            Agricultor agricultor = customAgricultorRepository.buscarAgricultorCompletoPorCpf(parenteMobile.getAgricultor().getPessoaFisica().getCpf());

            if(nonNull(parentePortal)) {//Se o PARENTE existir, adiciona apenas o parente a nova família

                parentePortal.getAgricultor().getPessoaFisica().getPessoa().setNome(parenteMobile.getAgricultor().getPessoaFisica().getPessoa().getNome());
                parentePortal.setParentesco(parenteMobile.getParentesco());
                parentePortal.getAgricultor().getPessoaFisica().setSexo(parenteMobile.getAgricultor().getPessoaFisica().getSexo());
                setEnderecoPessoa(agricultor.getPessoaFisica().getPessoa(), atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa());

                parentePortal.setDataInicioRelacao(new Date());

                if(novaFamilia.getParentes() == null) {
                    novaFamilia.setParentes(new ArrayList<>());
                }

                novaFamilia.getParentes().add(parentePortal);

                agricultor.setAtualizacaoPauloFreire(true);
                agricultor.setAtualizacaoProjeto('P');
                setElementoCoordenada(atividadeCadastro.getInformacaoInicial().getAgricultor(), agricultor);

            } else if(nonNull(agricultor) && isNull(parentePortal)) {
                Parente novoParente = new Parente();

                if(novaFamilia.getParentes() == null) {
                    novaFamilia.setParentes(new ArrayList<>());
                }

                novoParente.setAgricultor(agricultor);
                novoParente.setDataInicioRelacao(new Date());
                novoParente.setFamilia(novaFamilia);
                novoParente.setParentesco(parenteMobile.getParentesco());
                setEnderecoPessoa(agricultor.getPessoaFisica().getPessoa(), atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa());
                novaFamilia.getParentes().add(novoParente);

                agricultor.setAtualizacaoPauloFreire(true);
                agricultor.setAtualizacaoProjeto('P');
                setElementoCoordenada(atividadeCadastro.getInformacaoInicial().getAgricultor(), agricultor);

                //parenteRepository.save(novoParente);
            } else {//Se não, verifica se a existencia da PESSOA, cria um novo parente e adiciona a nova família
                PessoaFisica pessoaFisicaPorCpf = pessoaFisicaRepository.findByCpf(parenteMobile.getAgricultor().getPessoaFisica().getCpf());

                Parente novoParente = new Parente();
                Agricultor novoAgricultor = new Agricultor();
                novoParente.setAgricultor(novoAgricultor);

                novoAgricultor.setAtualizacaoPauloFreire(true);
                novoAgricultor.setAtualizacaoProjeto('P');
                setElementoCoordenada(atividadeCadastro.getInformacaoInicial().getAgricultor(), novoAgricultor);

                if(nonNull(pessoaFisicaPorCpf)) {
                    novoParente.getAgricultor().setPessoaFisica(pessoaFisicaPorCpf);
                } else {
                    Pessoa novaPessoa = new Pessoa();
                    novaPessoa.setNome(parenteMobile.getAgricultor().getPessoaFisica().getPessoa().getNome());

                    PessoaFisica novaPessoaFisica = new PessoaFisica();
                    novaPessoaFisica.setCpf(parenteMobile.getAgricultor().getPessoaFisica().getCpf());
                    novaPessoaFisica.setSexo(parenteMobile.getAgricultor().getPessoaFisica().getSexo());
                    novaPessoaFisica.setDataNascimento(parenteMobile.getAgricultor().getPessoaFisica().getDataNascimento());
                    setEnderecoPessoa(novaPessoa, atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa());

                    novaPessoaFisica.setPessoa(novaPessoa);

                    novoParente.getAgricultor().setPessoaFisica(novaPessoaFisica);

                }

                if(novaFamilia.getParentes() == null) {
                    novaFamilia.setParentes(new ArrayList<>());
                }

                novoParente.setDataInicioRelacao(new Date());
                novoParente.setFamilia(novaFamilia);
                novoParente.setParentesco(parenteMobile.getParentesco());
                novaFamilia.getParentes().add(novoParente);

                //parenteRepository.save(novoParente);
            }
        }
    }

    @Transactional(propagation = Propagation.NESTED)
    private void setEnderecoPessoa(Pessoa pessoaPortal, PessoaMobile pessoaMobile) {
        pessoaPortal.setBairro(nonNull(pessoaMobile.getBairro()) ? pessoaMobile.getBairro().trim().toUpperCase() : null);
        pessoaPortal.setCep(pessoaMobile.getCep());
        pessoaPortal.setComplemento(nonNull(pessoaMobile.getComplemento()) ? pessoaMobile.getComplemento().trim().toUpperCase() : null);
        pessoaPortal.setPontoDeReferencia(nonNull(pessoaMobile.getPontoDeReferencia()) ? pessoaMobile.getPontoDeReferencia().trim().toUpperCase() : null);
        pessoaPortal.setLogradouro(nonNull(pessoaMobile.getLogradouro()) ? pessoaMobile.getLogradouro().trim().toUpperCase() : null);
        pessoaPortal.setMunicipio(pessoaMobile.getMunicipio());
        pessoaPortal.setNumero(pessoaMobile.getNumero());
        if(nonNull(pessoaMobile.getMunicipio()) && nonNull(pessoaMobile.getMunicipio().getUf())) {
            pessoaPortal.setUf(pessoaMobile.getMunicipio().getUf());
        }
        if(nonNull(pessoaMobile.getMunicipio()) && nonNull(pessoaMobile.getMunicipio().getTerritorio())) {
            pessoaPortal.setTerritorio(pessoaMobile.getMunicipio().getTerritorio());
        }
        pessoaPortal.setDistrito(pessoaMobile.getDistrito());
        pessoaPortal.setLocalidade(pessoaMobile.getLocalidade());
        if(nonNull(pessoaMobile.getLocalidade()) && nonNull(pessoaMobile.getLocalidade().getComunidade())) {
            pessoaPortal.setComunidade(pessoaMobile.getLocalidade().getComunidade());
        }
    }

}
