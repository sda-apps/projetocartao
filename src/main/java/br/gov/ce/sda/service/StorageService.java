package br.gov.ce.sda.service;

import br.gov.ce.sda.MessageKey;
import br.gov.ce.sda.Messages;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;

@Service
public class StorageService {

    private final Path rootLocation = Paths.get("/opt/planos");

    private final static String SEPARATOR = FileSystems.getDefault().getSeparator();

    public void saveFile(String bucketName, String key, InputStream inputStream) throws IOException {
        Path bucket = rootLocation.resolve(bucketName);
        if (!Files.exists(bucket)) {
            Files.createDirectory(bucket);
        }
        Files.copy(inputStream, this.rootLocation.resolve(bucketName + SEPARATOR + key));
    }

    public Resource getFile(String bucketName, String key) throws IOException {
        Path path = rootLocation.resolve(bucketName + SEPARATOR + key);
        Resource resource = new UrlResource(path.toUri());

        if (!resource.exists() || !resource.isReadable()) {
            throw new IOException(Messages.getString(MessageKey.FILE_NOT_FOUND_ERROR));
        }

        return resource;
    }

    public boolean exists(String bucketName, String key) {
        Path path = rootLocation.resolve(bucketName + SEPARATOR + key);
        return path.toFile().exists();
    }

    public void deleteFile(String bucketName, String key) throws IOException {
        Path path = rootLocation.resolve(bucketName + SEPARATOR + key);
        Files.deleteIfExists(path);
    }

    public void deleteAllFilesInBucket(String bucketName) throws IOException {
        Path path = rootLocation.resolve(bucketName);
        if (Files.exists(path)) {
            Files.walk(path)
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);
        }
    }

}
