package br.gov.ce.sda.service.ida;

import br.gov.ce.sda.domain.sda.UserDetailSDA;
import br.gov.ce.sda.service.ida.converters.DataServiceStrategy;
import br.gov.ce.sda.web.rest.dto.ida.IndicadorProjeto;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Log4j2
@Service
public class DataService {

    private static final String PREFIX_URL = "http://10.85.0.53:8080/dataservices";
    //private static final String PREFIX_URL_LOCAL = "http://172.28.3.138:9090/dataservices";
    private static final String RESOURCE_URL_AUTH = PREFIX_URL + "/login";
    private static final String RESOURCE_URL = PREFIX_URL + "/api/indicador-projeto";

    public String getToken() {
        MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
        mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML, MediaType.ALL));

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);

        HttpEntity<UserAuthSDA> entity = new HttpEntity<>(new UserAuthSDA("ws.ppf", "3te#DEyA"));
        ResponseEntity<UserDetailSDA> response = restTemplate.postForEntity(RESOURCE_URL_AUTH, entity, UserDetailSDA.class);
        List<String> authorization = response.getHeaders().get("Authorization");

        assert authorization != null;
        return authorization.stream().findFirst().orElse("");
    }

    public String enviarInformacoesATERParaIDA(Object object) {
        try {
            String token = getToken();
            IndicadorProjeto indicadorProjeto = DataServiceStrategy.getInstance(object);

            MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
            mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON_UTF8, MediaType.TEXT_HTML, MediaType.ALL));
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", token);
            headers.add("Content-Type", "application/json");

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);
            HttpEntity<IndicadorProjeto> request = new HttpEntity<>(indicadorProjeto, headers);

            ResponseEntity<String> response = restTemplate.postForEntity(RESOURCE_URL, request, String.class);

            System.out.print(response.getBody());

            return response.getBody();
        } catch (HttpClientErrorException ex) {
            log.error(ex.getResponseHeaders().get("error"));
            log.error(ex.getResponseBodyAsString());
            return ex.getResponseBodyAsString();
        } catch (Exception e) {
            log.error(e.getCause());
            return e.getCause().getMessage();
        }
    }

    @Data @AllArgsConstructor @NoArgsConstructor private class UserAuthSDA {
        @JsonProperty(value = "username")
        private String username;

        @JsonProperty(value = "password")
        private String password;
    }

}
