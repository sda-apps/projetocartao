package br.gov.ce.sda.service.ater;

import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.domain.ater.HistoricoAvaliacaoPlanos;
import br.gov.ce.sda.domain.ater.PlanoTrabalhoAnual;
import br.gov.ce.sda.domain.ater.PlanoTrimestral;
import br.gov.ce.sda.repository.ater.CustomHistoricoAvaliacaoPlanosRepository;
import br.gov.ce.sda.repository.ater.HistoricoAvaliacaoPlanosRepository;
import br.gov.ce.sda.repository.ater.PlanoTrabalhoAnualRepository;
import br.gov.ce.sda.repository.ater.PlanoTrimestralRepository;
import br.gov.ce.sda.config.security.SecurityUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

import static java.util.Objects.nonNull;

@Service
public class HistoricoAvaliacaoPlanosService {

    @Inject
    private HistoricoAvaliacaoPlanosRepository historicoAvaliacaoPlanosRepository;

    @Inject
    private CustomHistoricoAvaliacaoPlanosRepository customHistoricoAvaliacaoPlanosRepository;

    @Inject
    private PlanoTrimestralRepository planoTrimestralRepository;

    @Inject
    private PlanoTrabalhoAnualRepository planoTrabalhoAnualRepository;

    @Transactional
    public HistoricoAvaliacaoPlanos save(HistoricoAvaliacaoPlanos historicoAvaliacaoPlanos) {
        Usuario usuario = SecurityUtils.getCurrentUser();
        historicoAvaliacaoPlanos.setUsuario(usuario);

        HistoricoAvaliacaoPlanos historico = null;

        if (verifyingPlanoTrimestral(historicoAvaliacaoPlanos.getPlanoTrimestral())) {
            PlanoTrimestral planoTrimestral = planoTrimestralRepository.findById(historicoAvaliacaoPlanos.getPlanoTrimestral().getId()).get();
            if (nonNull(planoTrimestral)) {
                if(nonNull(historicoAvaliacaoPlanos.getPlanoTrimestral().getFileStatus())) {
                    updateRelatorioPlanoTrimestral(planoTrimestral, historicoAvaliacaoPlanos.getPlanoTrimestral());
                    historico = historicoAvaliacaoPlanosRepository.save(historicoAvaliacaoPlanos);
                } else {
                    updatePlanoTrimestral(planoTrimestral, historicoAvaliacaoPlanos.getPlanoTrimestral());
                    historico = historicoAvaliacaoPlanosRepository.save(historicoAvaliacaoPlanos);
                }
            }  else {
                return null;
            }
        } else if (verifyingPlanoDeTrabalhoAnual(historicoAvaliacaoPlanos.getPlanoTrabalhoAnual())) {
            PlanoTrabalhoAnual planoTrabalhoAnual = planoTrabalhoAnualRepository.findById(historicoAvaliacaoPlanos.getPlanoTrabalhoAnual().getId()).get();
            if (nonNull(planoTrabalhoAnual)) {
                historico = historicoAvaliacaoPlanosRepository.save(historicoAvaliacaoPlanos);
                updatePlanoDeTrabalhoAnual(planoTrabalhoAnual, historicoAvaliacaoPlanos.getPlanoTrabalhoAnual());
            } else {
                return null;
            }
        } else {
            return null;
        }

        return customHistoricoAvaliacaoPlanosRepository.findById(historico.getId());
    }

    private void updateRelatorioPlanoTrimestral(PlanoTrimestral planoTrimestral, PlanoTrimestral newPlanoTrimestral) {
        planoTrimestral.setFileStatus(newPlanoTrimestral.getFileStatus());
        planoTrimestralRepository.save(planoTrimestral);
    }

    private boolean verifyingPlanoTrimestral(PlanoTrimestral planoTrimestral) {
        return nonNull(planoTrimestral) && nonNull(planoTrimestral.getId()) && nonNull(planoTrimestral.getStatus());
    }

    private boolean verifyingPlanoDeTrabalhoAnual(PlanoTrabalhoAnual planoTrabalhoAnual) {
        return nonNull(planoTrabalhoAnual) && nonNull(planoTrabalhoAnual.getId()) && nonNull(planoTrabalhoAnual.getStatus());
    }

    private void updatePlanoTrimestral(PlanoTrimestral planoTrimestral, PlanoTrimestral newPlanoTrimestral) {
        planoTrimestral.setStatus(newPlanoTrimestral.getStatus());
        planoTrimestral.setRecusado(newPlanoTrimestral.getRecusado());
        planoTrimestralRepository.save(planoTrimestral);
    }

    private void updatePlanoDeTrabalhoAnual(PlanoTrabalhoAnual planoTrabalhoAnual, PlanoTrabalhoAnual newPlanoTrabalhoAnual) {
        planoTrabalhoAnual.setStatus(newPlanoTrabalhoAnual.getStatus());
        planoTrabalhoAnual.setRecusado(newPlanoTrabalhoAnual.isRecusado());
        planoTrabalhoAnualRepository.save(planoTrabalhoAnual);
    }

    public List<HistoricoAvaliacaoPlanos> findByPlanoId(final long id, final String type) {
        return customHistoricoAvaliacaoPlanosRepository.findByPlanoId(id, type);
    }

}
