package br.gov.ce.sda.service.ater;

import br.gov.ce.sda.config.security.AuthoritiesConstants;
import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.domain.ater.*;
import br.gov.ce.sda.repository.ater.CustomPlanoTrabalhoAnualRepository;
import br.gov.ce.sda.repository.ater.PlanoTrabalhoAnualRepository;
import br.gov.ce.sda.config.security.SecurityUtils;
import br.gov.ce.sda.service.RegiaoService;
import br.gov.ce.sda.service.StorageService;
import br.gov.ce.sda.web.rest.dto.PlanoTrabalhoAnualDTO;
import br.gov.ce.sda.web.rest.dto.RelatorioSinteticoATCsDTO;
import br.gov.ce.sda.web.rest.dto.TotalMetasATCsDTO;
import br.gov.ce.sda.web.rest.dto.ater.FiltroPlanoTrabalhoAnualDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.io.IOException;
import java.util.List;
import java.util.Set;

@Service
public class PlanoTrabalhoAnualService {

	@Inject
	private PlanoTrabalhoAnualRepository ptaRepository;

	@Inject
	private CustomPlanoTrabalhoAnualRepository customPTARepository;

    @Inject
    private StorageService storageService;

    @Inject
    private RegiaoService regiaoService;

	@Transactional
	public PlanoTrabalhoAnual save(PlanoTrabalhoAnual pta){
		return ptaRepository.save(pta);
	}


	public Set<PlanoTrabalhoAnual> findAll(){
        return customPTARepository.findAll();
    }

	@Transactional(readOnly = true)
	public List<PlanoTrabalhoAnualDTO> findAllByFiltro(FiltroPlanoTrabalhoAnualDTO filtro){
        Usuario usuario = SecurityUtils.getCurrentUser();
        if (usuario.hasAuthorities(AuthoritiesConstants.USUARIO_ERP)) {
            filtro.setRegiaoUsuario(regiaoService.findRegiaoBy(usuario));
        }

		return customPTARepository.findAllByFiltro(filtro);
	}

    public List<ViewPlanosAnuais> findAllFromView(FiltroPlanoTrabalhoAnualDTO filter) {
        Usuario usuario = SecurityUtils.getCurrentUser();
        List<ViewPlanosAnuais> planos = null;

        if (usuario.hasAuthorities(AuthoritiesConstants.USUARIO_ERP)) {
            filter.setRegiaoUsuario(regiaoService.findRegiaoBy(usuario));
            planos = customPTARepository.findAllForErpUser(filter);
        } else if (usuario.hasAuthorities(AuthoritiesConstants.USUARIO_UGP) || usuario.hasAuthorities(AuthoritiesConstants.ADMIN)) {
            planos = customPTARepository.findAllForUgpUser(filter);
        }

        return planos;
    }

    @Transactional
    public PlanoTrabalhoAnual findById(Long id) {
        return customPTARepository.findById(id);
    }

	@Transactional
	public PlanoTrabalhoAnual update(PlanoTrabalhoAnual planoAnualAtual, PlanoTrabalhoAnual planoAnual) throws IOException {
        for(MetaFamiliasPTA metaFamilias: planoAnual.getMetasFamiliasPta()){
            metaFamilias.setPlanoTrabalhoAnual(planoAnual);
        }

        for(MetaAtividadesPTA metaAtividades: planoAnual.getMetasAtividadesPta()){
            metaAtividades.setPlanoTrabalhoAnual(planoAnual);
        }

        storageService.deleteAllFilesInBucket(planoAnual.getId().toString());

        BeanUtils.copyProperties(planoAnual, planoAnualAtual);

        return ptaRepository.save(planoAnual);
    }

	@Transactional
	public void delete(Long id){
		ptaRepository.deleteById(id);
	}

    @Transactional(readOnly = true)
    public List<RelatorioSinteticoATCsDTO> findRelatorioAtcs() {
        return customPTARepository.findRelatorioAtcs();
    }

    @Transactional(readOnly = true)
    public List<TotalMetasATCsDTO> findAllMetasAtcs() {
        return customPTARepository.findAllMetasAtcs();
    }

    public List<Long> findRegiaoPorTecnicoDoPlanoDeTrabalho(Long tecnicoId) {
        return customPTARepository.findRegiaoPorTecnicoDoPlanoDeTrabalho(tecnicoId);
    }

    public PlanoTrabalhoAnual findPlanoTrabalhoAnualPorCoordenadorId(Long coordenadorId) {
        return customPTARepository.findPlanoTrabalhoAnualPorCoordenadorId(coordenadorId);
    }

    public PlanoTrabalhoAnual findPlanoTrabalhoAnualPorTecnicoId(Long tecnicoId) {
        return customPTARepository.findPlanoTrabalhoAnualPorTecnicoId(tecnicoId);
    }

    public PlanoTrabalhoAnual findPlanoTrabalhoAnualPorAssessorId(Long assessorId) {
        return customPTARepository.findPlanoTrabalhoAnualPorAssessorId(assessorId);
    }

    public Set<PlanoTrabalhoAnual> findAllByEmpresa(Long empresaId) {
        return customPTARepository.findAllByEmpresa(empresaId);
    }

    public List<Comunidade> findAllComunidadeByMunicipioAndPta(Long municipio, Long ptaId) {
        return customPTARepository.findAllComunidadeByMunicipioAndPta(municipio,ptaId);
    }
}
