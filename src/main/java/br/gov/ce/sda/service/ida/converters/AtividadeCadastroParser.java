package br.gov.ce.sda.service.ida.converters;

import br.gov.ce.sda.domain.Localidade;
import br.gov.ce.sda.domain.mobile.AtividadeCadastro;
import br.gov.ce.sda.web.rest.dto.ida.IndicadorPlanejamentoExecucao;
import br.gov.ce.sda.web.rest.dto.ida.IndicadorProjeto;
import br.gov.ce.sda.web.rest.dto.ida.IndicadorProjetoBeneficiario;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashSet;
import java.util.Set;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.StringUtils.trim;

public class AtividadeCadastroParser implements IndicadorProjetoParcer {

    private static final Long INDICADOR_CADASTRO_FAMILIA = 526L;
    private static final Integer INDICADOR_ITEM_CADASTRO_FAMILIA = 247;
    private static final String EXECUCAO = "2";
    private final AtividadeCadastro atividadeCadastro;

    AtividadeCadastroParser(AtividadeCadastro atividadeCadastro) {
        this.atividadeCadastro = atividadeCadastro;
    }

    @Override
    public IndicadorProjeto parse() {
        Set<IndicadorProjetoBeneficiario> indicadorProjetoBeneficiarios = new HashSet<>();
        Set<IndicadorPlanejamentoExecucao> indicadorPlanejamentoExecucoes = new HashSet<>();

        IndicadorProjeto ip = IndicadorProjeto.builder()
            .ano(atividadeCadastro.getData().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getYear())
            .dataImportacao(LocalDateTime.now())
            .latitude(atividadeCadastro.getInformacaoInicial().getAgricultor().getCoordenada().getLatitude())
            .longitude(atividadeCadastro.getInformacaoInicial().getAgricultor().getCoordenada().getLongitude())
            .indicadorProjetoSituacaoCodigo(SITUACAO_CONCLUIDO)
            .situacao(SITUACAO_CONCLUIDO.toString())
            .indicador(INDICADOR_CADASTRO_FAMILIA)
            .solicitado("07954563000168")//TODO ? // CNPJ - responsável pela informação - saber PF ou SDA (07954563000168)
            .solicitante("07954563000168")//TODO ? // CNPJ - responsável pela informação - saber PF ou SDA (07954563000168)
            .tipoIndicador(INDICADOR_BENEFICIARIOS)
            .projetoCodigo(PROJETO_PAULO_FREIRE)
            .unidadeMedidaCodigo(UNIDADE_MEDIDA_UNIDADE)
            .fonteInformacaoCodigo(FONTE_INFORMACAO_PAULO_FREIRE)
            .publico(NAO)
            .paisCodigo(isStringNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getNacionalidade().getSigla()))
            .ufCodigo(isStringNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa().getMunicipio().getUf().getCodigoIbge()))
            .municipioCodigo(isStringNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa().getMunicipio().getCodigoIbge().substring(0,6)))
            .distritoCodigo(isStringNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa().getDistrito().getCodigoIbge()))
            .localidadeCodigo(isLocalidadeNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa().getLocalidade(),
                atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa().getDistrito().getCodigoIbge()))
            .build();

        IndicadorProjetoBeneficiario ipb = IndicadorProjetoBeneficiario.builder()
            .nome(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa().getNome())
            .cpfCnpj(isStringNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getCpf()))
            .beneficiario(isStringNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getCpf()))
            .sexo(isStringNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getSexo()))
            .dataNascimento(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getDataNascimento().toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
            .email(isStringNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa().getEmail()))
            .cep(isNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa().getCep()) ? "00000000" : atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa().getCep())
            .endereco(isStringNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa().getLogradouro()))
            .enderecoComp(isStringNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa().getComplemento()))
            .enderecoNumero(isEnderecoNumeroNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa().getNumero()))
            .latitude(isStringNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getCoordenada().getLatitude()))
            .longitude(isStringNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getCoordenada().getLongitude()))
            .paisCodigo(isStringNull(trim(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getNacionalidade().getSigla())))
            .municipioCodigo(isStringNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa().getMunicipio().getCodigoIbge().substring(0,6)))
            .ufCodigo(isStringNull(trim(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa().getMunicipio().getUf().getCodigoIbge())))
            .siglaUf(isStringNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa().getMunicipio().getUf().getSigla()))
            .distritoCodigo(isStringNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa().getDistrito().getCodigoIbge()))
            .localidadeCodigo(isLocalidadeNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa().getLocalidade(),
                atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa().getDistrito().getCodigoIbge()))
            .nomePai(isStringNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getNomePai()))
            .nis(isStringNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getNis()))
            .tipo(PESSOA_FISICA)
            .executadoQuantidade(1.0)
            .nomeMun(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa().getMunicipio().getNome())
            .nomeDistrito(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa().getDistrito().getNome())
            .nomeUf(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getPessoa().getMunicipio().getUf().getDescricao())
            .nomePai(trim(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getNacionalidade().getDescricao()))
            .build();

        IndicadorPlanejamentoExecucao ipe = IndicadorPlanejamentoExecucao.builder()
            .responsavel(isStringNull(atividadeCadastro.getInformacaoInicial().getAgricultor().getPessoaFisica().getCpf()))
            //.tipo(PESSOA_FISICA)
            .tipo(EXECUCAO)
            .quantidade(1.0)
            .data(atividadeCadastro.getData().toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
            .situacao("3") //não requerido
            .indicadorItem(INDICADOR_ITEM_CADASTRO_FAMILIA)
            .dataFinal(atividadeCadastro.getData().toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
            .build();

        indicadorProjetoBeneficiarios.add(ipb);
        indicadorPlanejamentoExecucoes.add(ipe);
        ip.setProjetoBeneficiarios(indicadorProjetoBeneficiarios);
        ip.setPlanejamentoExecucoes(indicadorPlanejamentoExecucoes);

        return ip;
    }

    private String isEnderecoNumeroNull(String numero) {
        if(nonNull(numero)) {
            return numero;
        }

        return "000";
    }

    private String isLocalidadeNull(Localidade localidade, String codigoIbgeDistrito) {
        if(nonNull(localidade)) {
            return localidade.getCodigoSda();
        } else {
            return codigoIbgeDistrito;
        }
    }
}
