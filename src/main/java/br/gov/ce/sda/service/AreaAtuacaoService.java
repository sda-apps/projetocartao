package br.gov.ce.sda.service;

import br.gov.ce.sda.domain.Municipio;
import br.gov.ce.sda.domain.UsuarioAvancado;
import br.gov.ce.sda.domain.mobile.AreaAtuacao;
import br.gov.ce.sda.repository.AreaAtuacaoRepository;
import br.gov.ce.sda.repository.UsuarioAvancadoRepository;
import br.gov.ce.sda.web.rest.dto.TecnicoAreaAtuacaoDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class AreaAtuacaoService {

    @Inject
    private AreaAtuacaoRepository areaAtuacaoRepository;

    @Inject
    private UsuarioAvancadoRepository usuarioAvancadoRepository;

    public void associarNovasAreasAtuacao(TecnicoAreaAtuacaoDTO dto) {
        deleteAreasAtuacao(dto.getUsuarioAvancadoId());
        saveAreasAtuacao(dto);
    }

    @Transactional
    public void deleteAreasAtuacao(Long usuarioAvancadoId) {
        List<AreaAtuacao> oldAreasAtuacao = areaAtuacaoRepository.findAllByUsuarioAvancadoId(usuarioAvancadoId);
        areaAtuacaoRepository.deleteAll(oldAreasAtuacao);
    }

    @Transactional
    public void saveAreasAtuacao(TecnicoAreaAtuacaoDTO dto) {
        UsuarioAvancado usuarioAvancado = usuarioAvancadoRepository.findById(dto.getUsuarioAvancadoId()).get();

        if (Objects.nonNull(dto.getMunicipios())) {
            List<AreaAtuacao> newAreasAtuacao = new ArrayList<AreaAtuacao>();
            for (Municipio municipio : dto.getMunicipios()) {
                newAreasAtuacao.add(AreaAtuacao.builder()
                    .usuarioAvancado(usuarioAvancado)
                    .municipio(municipio).build());
            }
            areaAtuacaoRepository.saveAll(newAreasAtuacao);
        }
    }

}
