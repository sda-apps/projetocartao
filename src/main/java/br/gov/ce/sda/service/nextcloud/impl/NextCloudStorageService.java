package br.gov.ce.sda.service.nextcloud.impl;


import br.gov.ce.sda.config.nextcloud.NextCloudProperties;
import br.gov.ce.sda.exceptions.NextCloudFileNotFoundException;
import br.gov.ce.sda.exceptions.NextCloudIOException;
import br.gov.ce.sda.exceptions.StorageException;
import br.gov.ce.sda.service.nextcloud.StorageService;
import com.github.sardine.DavResource;
import com.github.sardine.Sardine;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Log4j2
@Service
public class NextCloudStorageService implements StorageService {

    private static final String WEB_DAV_BASE_PATH = "/remote.php/webdav/";
    private static final String ROOT_PATH = "projetocartao/";

    @Autowired
    private NextCloudProperties nextCloudProperties;

    @Autowired
    private Sardine sardine;

    @Override
    @SneakyThrows
    @Retryable(
        value = { IOException.class, NextCloudIOException.class },
        maxAttempts = 5,
        backoff = @Backoff(delay = 5000))
    public void saveFile(String bucketName, MultipartFile multipartFile) {
        try {
            String remotePath = buildRemotePath(
                bucketName,
                URLEncoder.encode(Objects.requireNonNull(multipartFile.getOriginalFilename()), "UTF-8")
            );
            String remoteBucket = buildRemotePath(bucketName);

            if (!sardine.exists(remoteBucket)) {
                sardine.createDirectory(remoteBucket);
            }
            sardine.put(remotePath, multipartFile.getInputStream(), multipartFile.getContentType(), false, multipartFile.getSize());
        } catch (IOException e) {
            throw new NextCloudIOException();
        }
    }

    @Override
    @Retryable(
        value = { IOException.class, NextCloudIOException.class },
        maxAttempts = 6,
        backoff = @Backoff(delay = 5000))
    public Optional<Resource> getFile(String bucketName, String fileName) throws StorageException {
        Resource resource;
        try {
            log.info("m=getFile, status=beginning, bucket={}, fileName={}", bucketName, fileName);
            String path = buildRemotePath(bucketName, URLEncoder.encode(fileName, "UTF-8"));

            if (!fileExists(path)) {
                throw new NextCloudFileNotFoundException();
            }

            resource = new InputStreamResource(sardine.get(path));
        } catch (IOException e) {
            log.error("m=getFile, status=error, bucket={}, fileName={}", bucketName, fileName, e);
            throw new NextCloudIOException();
        }

        return Optional.ofNullable(resource);
    }

    @Override
    @Retryable(
        value = { IOException.class, NextCloudIOException.class },
        maxAttempts = 5,
        backoff = @Backoff(delay = 5000))
    public Optional<List<Resource>> getListFiles(String bucketName) throws StorageException {
        log.info("m=getListFiles, status=beginning, bucket={}", bucketName);
        String path = buildRemotePath(bucketName);
        List<Resource> resources = new ArrayList<>();

        try {
            List<DavResource> davResources = sardine.list(path);

            for (DavResource res : davResources) {
                String remotePath = buildRemotePath() + res.getHref();

                resources.add(new InputStreamResource(sardine.get(remotePath)));
            }
            log.info("m=getListFiles, status=finish, bucket={}, files={}", bucketName, resources.toString());
        } catch (IOException e) {
            throw new NextCloudIOException();
        }

        return Optional.ofNullable(resources);
    }

    @Override
    @Retryable(
        value = { IOException.class, NextCloudIOException.class },
        maxAttempts = 5,
        backoff = @Backoff(delay = 7000))
    public Optional<List<String>> getListURLFiles(String bucketName) throws StorageException {
        log.info("m=getListURLFiles, status=beginning, bucket={}", bucketName);
        String path = buildRemotePath(bucketName);
        List<String> urls = new ArrayList<>();

        try {
            List<DavResource> davResources = sardine.list(path);

            for (DavResource res : davResources) {
                String remotePath = buildRemotePath() + res.getHref();

                urls.add(remotePath);
            }
            log.info("m=getListFiles, status=finish, bucket={}, files={}", bucketName, urls.toString());
        } catch (IOException e) {
            throw new NextCloudIOException();
        }

        return Optional.ofNullable(urls);
    }


    private boolean fileExists(String remotePath) {
        boolean exists = false;
        try {
            exists = sardine.exists(remotePath);
        } catch (IOException e) {
            log.error("m=fileExists, path={}", remotePath, e);
        }

        return exists;
    }

    private String buildRemotePath() {
        String path = (nextCloudProperties.isHttps() ? "https://" : "http://") +
            nextCloudProperties.getServerName();

        return path;
    }

    private String buildRemotePath(String bucketName, String fileName) {
        return (nextCloudProperties.isHttps() ? "https://" : "http://") +
            nextCloudProperties.getServerName() +
            WEB_DAV_BASE_PATH +
            ROOT_PATH + bucketName + "/" + fileName;
    }

    private String buildRemotePath(String bucketName) {
        return (nextCloudProperties.isHttps() ? "https://" : "http://") +
            nextCloudProperties.getServerName() +
            WEB_DAV_BASE_PATH +
            ROOT_PATH + bucketName;
    }

}
