package br.gov.ce.sda.service.ida.converters;

import br.gov.ce.sda.web.rest.dto.ida.IndicadorProjeto;

import static java.util.Objects.isNull;

public interface IndicadorProjetoParcer {

    Integer SITUACAO_CONCLUIDO = 3;
    String PESSOA_FISICA = "F";
    String NAO = "N";
    Integer INDICADOR_BENEFICIARIOS = 67;
    Long PROJETO_PAULO_FREIRE = 154L;
    Integer UNIDADE_MEDIDA_UNIDADE = 1;
    Integer FONTE_INFORMACAO_PAULO_FREIRE = 9;
    String CNPJ_RESPONSAVEL_INFORMACAO = "07954563000168";
    Long INDICADOR_CADASTRO_FAMILIA = 526L;
    Integer INDICADOR_ITEM_CADASTRO_FAMILIA = 247;

    IndicadorProjeto parse();

    default String isStringNull(String string) {
        return isNull(string) ? "" : string;
    }

}
