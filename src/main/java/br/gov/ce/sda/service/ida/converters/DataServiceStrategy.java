package br.gov.ce.sda.service.ida.converters;

import br.gov.ce.sda.domain.ater.Tarefa;
import br.gov.ce.sda.domain.mobile.AtividadeCadastro;
import br.gov.ce.sda.web.rest.dto.ida.IndicadorProjeto;

public class DataServiceStrategy {

    public static IndicadorProjeto getInstance(Object o) {
        IndicadorProjeto indicadorProjeto = null;

        if(o instanceof Tarefa) {
            return new TarefaParser((Tarefa) o).parse();
        }

        if(o instanceof AtividadeCadastro) {
            return new AtividadeCadastroParser((AtividadeCadastro) o).parse();
        }

        return null;
    }

}
