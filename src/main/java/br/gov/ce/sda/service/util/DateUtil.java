package br.gov.ce.sda.service.util;

import org.apache.commons.lang.StringUtils;

import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    public static Integer getIdade(Date data) {
        Calendar dataNascimento = Calendar.getInstance();
        dataNascimento.setTime(data);
        Calendar dataAtual = Calendar.getInstance();
        Integer diferencaMes = dataAtual.get(Calendar.MONTH) - dataNascimento.get(Calendar.MONTH);
        Integer diferencaDia = dataAtual.get(Calendar.DAY_OF_MONTH) - dataNascimento.get(Calendar.DAY_OF_MONTH);
        Integer idade = (dataAtual.get(Calendar.YEAR) - dataNascimento.get(Calendar.YEAR));
        if(diferencaMes < 0	|| (diferencaMes == 0 && diferencaDia < 0)) {
            idade--;
        }
        return idade;
    }

}
