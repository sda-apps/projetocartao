package br.gov.ce.sda.aop;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import br.gov.ce.sda.config.security.SecurityUtils;

@Aspect
@Component
public class LogHandlerAspect {

	@Pointcut("execution(* br.gov.ce.sda.web.rest.*.*(..))")
    public void requestMethods() { }

	@Around("requestMethods()")
    public Object logRequests(ProceedingJoinPoint joinPoint) throws Throwable {
		String path = this.getClass().getClassLoader().getResource("").getPath();
		String fullPath = URLDecoder.decode(path, "UTF-8");
		String pathArr[] = fullPath.split("/target/classes/");

		BufferedWriter buffWrite = new BufferedWriter(new FileWriter(pathArr[0]
				.replace(".jar!/", "").replace("file:", "") + ".txt", true));

    	Object output = this.comporArquivo(buffWrite, joinPoint);

    	buffWrite.close();

        return output;
    }

	private Object comporArquivo(BufferedWriter buffWrite, ProceedingJoinPoint joinPoint) throws Throwable {
		Long inicio = System.currentTimeMillis();
		String espacamento = "\t";
    	Date agora = new Date();
        SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        buffWrite.append("\r\n");

        buffWrite.append(formatoData.format(agora) + espacamento);

        buffWrite.append(SecurityUtils.getCurrentUserLogin() + espacamento);

        //buffWrite.append("Rest" + espacamento);

        Object output = joinPoint.proceed();

        Long tempoGasto = System.currentTimeMillis() - inicio;

        buffWrite.append(tempoGasto + " ms" + espacamento);

        buffWrite.append(joinPoint.getSignature().getDeclaringTypeName().toString() + espacamento);

        buffWrite.append(joinPoint.getSignature().getName() + espacamento);

		return output;
	}

}
