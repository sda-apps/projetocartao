package br.gov.ce.sda.config.nextcloud;

import com.github.sardine.Sardine;
import com.github.sardine.SardineFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SardineConfiguration {

    @Autowired
    private NextCloudProperties nextCloudProperties;

    @Bean
    public Sardine sardine() {
        Sardine sardine = SardineFactory.begin(nextCloudProperties.getUsername(), nextCloudProperties.getPassword());
        sardine.enablePreemptiveAuthentication(nextCloudProperties.getServerName());
        return sardine;
    }

}
