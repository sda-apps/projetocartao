package br.gov.ce.sda.config.security;

import br.gov.ce.sda.domain.Agricultor;
import br.gov.ce.sda.domain.acesso.Autoridade;
import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.repository.AuthorityRepository;
import br.gov.ce.sda.repository.CustomAgricultorRepository;
import br.gov.ce.sda.repository.UserRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.ldap.authentication.ad.ActiveDirectoryLdapAuthenticationProvider;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Service
@Transactional
public class CustomAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    private static final String LDAP_DOMAIN = "sda.ce.gov.br";

    private static final String LDAP_URL = "ldap://10.85.0.2";

    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Inject
    private UserRepository userRepository;

    @Inject
    private AuthorityRepository authorityRepository;

    @Inject
    private CustomAgricultorRepository agricultorRepository;

    @Inject
    private AuthenticationEventPublisher eventPublisher;

    private ActiveDirectoryLdapAuthenticationProvider ldapProvider = new ActiveDirectoryLdapAuthenticationProvider(LDAP_DOMAIN, LDAP_URL);

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails,
                                                  UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
        throws AuthenticationException {

        preconditions(username, authentication);


        Usuario usuario = null;

        try {
            usuario = tryLdapAuthentication(authentication);
        } catch (Exception e) {
        }


        if (isNull(usuario)) {
            usuario = tryLocalAuthentication(authentication);
        }

        eventPublisher.publishAuthenticationSuccess(authentication);
        return usuario;
    }

    private Usuario tryLdapAuthentication(Authentication authentication) {
        Usuario usuario = null;
        String username = authentication.getPrincipal().toString();
        ldapProvider.setConvertSubErrorCodesToExceptions(true);
        ldapProvider.setUseAuthenticationRequestCredentials(true);
        Authentication authenticate = ldapProvider.authenticate(authentication);

        if (authenticate.isAuthenticated()) {
            Autoridade autoridade = authorityRepository.findById("ROLE_TECNICO").get();
            Set<Autoridade> autoridades = new HashSet<>();
            autoridades.add(autoridade);
            usuario = new Usuario();
            usuario.setAutoridades(autoridades);
            usuario.setLogin(username);
            usuario.setStatus(Usuario.Status.ATIVADO);
        }

        return usuario;
    }

    private Usuario tryLocalAuthentication(Authentication authentication) {
        String username = authentication.getPrincipal().toString();
        Optional<Usuario> oneByLogin = userRepository.findOneByLogin(username);
        Usuario usuario = null;
        if (oneByLogin.isPresent()) {
            usuario = oneByLogin.get();
        }

        if (isNull(usuario)) {
            usuario = criarUsuarioAgricultor(username);
        } else {
            posconditions(usuario, authentication);
        }

        return usuario;
    }

    private Usuario criarUsuarioAgricultor(String cpf) {
        Agricultor agricultor = agricultorRepository.buscarAgricultorPorCpf(cpf);
        Usuario usuarioAgricultor;

        if (nonNull(agricultor)) {
            usuarioAgricultor = new Usuario();
            Autoridade autoridade = authorityRepository.findById("ROLE_AGRI_REST").get();
            Set<Autoridade> autoridades = new HashSet<>();
            autoridades.add(autoridade);
            usuarioAgricultor.setAutoridades(autoridades);
            usuarioAgricultor.setAgricultor(agricultor);
            usuarioAgricultor.setLogin(agricultor.getPessoaFisica().getCpf());
            LocalDate localDate = agricultor.getPessoaFisica().getDataNascimento().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyyyy");
            String dataNascimento = localDate.format(formatter);
            usuarioAgricultor.setSenha(passwordEncoder.encode(dataNascimento));
            usuarioAgricultor.setStatus(Usuario.Status.ATIVADO);
        } else {
            throw new UsernameNotFoundException("Agricultor não encontrado.");
        }

        return usuarioAgricultor;
    }

    private void posconditions(Usuario usuario, Authentication authentication) {
        String password = authentication.getCredentials().toString();
        if (!passwordEncoder.matches(password, usuario.getPassword())) {
            throw new BadCredentialsException("Senha inválida.");
        }

        if (!usuario.isEnabled()) {
            throw new AccountExpiredException("Usuário desabilitado.");
        }
    }

    private void preconditions(String username, Authentication authentication) {
        String password = authentication.getCredentials().toString();
        authentication.getPrincipal().toString();
        if (StringUtils.isEmpty(username)) {
            setHideUserNotFoundExceptions(false);
            throw new UsernameNotFoundException("Informe o username.");
        }

        if (StringUtils.isEmpty(password)) {
            throw new BadCredentialsException("Informe o password.");
        }
    }

}
