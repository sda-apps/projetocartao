package br.gov.ce.sda.config.nextcloud;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Data
@Configuration
@PropertySource("classpath:nextcloud.properties")
@ConfigurationProperties(prefix = "nextcloud")
public class NextCloudProperties {
    private String username;
    private String password;
    private String serverName;
    private boolean https;
}
