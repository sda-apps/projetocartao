package br.gov.ce.sda.config;

import java.util.Properties;

import javax.inject.Inject;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import lombok.extern.java.Log;

@Log
@Configuration
public class MailConfiguration {

	private static final String MAIL_DEBUG = "mail.debug";
	private static final String MAIL_SMTP_SSL_ENABLE = "mail.smtp.ssl.enable";
	private static final String MAIL_SMTP_STARTTLS_ENABLE = "mail.smtp.starttls.enable";
	private static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
	private static final String MAIL_TRANSPORT_PROTOCOL = "mail.transport.protocol";
	private static final String MAIL_AUTH = "mail.auth";
	private static final String MAIL_TLS = "mail.tls";
	private static final String MAIL_SSL = "mail.ssl";
	private static final String MAIL_PROTOCOL = "mail.protocol";
	private static final String MAIL_PASSWORD = "mail.password";
	private static final String MAIL_USERNAME = "mail.username";
	private static final String MAIL_PORT = "mail.port";
	private static final String MAIL_HOST = "mail.host";

	@Inject
	private Environment env;

	@Bean
	public JavaMailSender mailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

		mailSender.setHost(env.getProperty(MAIL_HOST));
		mailSender.setPort(env.getProperty(MAIL_PORT, Integer.class));
		mailSender.setUsername(env.getProperty(MAIL_USERNAME));
		mailSender.setPassword(env.getProperty(MAIL_PASSWORD));
		bypassSSL();
		mailSender.setJavaMailProperties(mailProperties());

		return mailSender;
	}

	private Properties mailProperties() {
		Properties sendProperties = new Properties();

		sendProperties.put(MAIL_TRANSPORT_PROTOCOL, env.getProperty(MAIL_PROTOCOL));
		sendProperties.put(MAIL_SMTP_AUTH, env.getProperty(MAIL_AUTH));
		sendProperties.put(MAIL_SMTP_STARTTLS_ENABLE, env.getProperty(MAIL_TLS));
		sendProperties.put(MAIL_SMTP_SSL_ENABLE, env.getProperty(MAIL_SSL));
		sendProperties.put(MAIL_DEBUG, env.getProperty(MAIL_DEBUG));

		return sendProperties;
	}

	public void bypassSSL() {
		final TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			@Override
			public void checkClientTrusted(final java.security.cert.X509Certificate[] certs, final String authType) {
			}

			@Override
			public void checkServerTrusted(final java.security.cert.X509Certificate[] certs, final String authType) {
			}

			@Override
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}
		} };
		try {
			final SSLContext sc = SSLContext.getInstance("TLS");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			SSLContext.setDefault(sc);
		} catch (final Exception e) {
			log.severe(e.getMessage());
		}
	}

}
