package br.gov.ce.sda.config.security;

/**
 * Constants for authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    public static final String USUARIO_ATER = "ROLE_USUARIO_ATER";

    public static final String USUARIO_ERP = "ROLE_USUARIO_ERP";

    public static final String USUARIO_UGP = "ROLE_USUARIO_UGP";

    public static final String RESPONSAVEL_ATER = "ROLE_RESPONSAVEL_ATER";

    public static final String COORDENADOR_ATER = "ROLE_USUARIO_COORDENADOR_ATER";

    public static final String ASSESSOR_ATER = "ROLE_USUARIO_ATER_ASSESSOR";

    public static final String OBSERVATORIO = "ROLE_OBSERVATORIO";


    private AuthoritiesConstants() { }

}
