package br.gov.ce.sda.config;

import br.gov.ce.sda.config.util.JSR310DateTimeSerializer;
import br.gov.ce.sda.config.util.JSR310LocalDateDeserializer;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.time.*;

@Configuration
public class JacksonConfiguration {

    @Bean
    Jackson2ObjectMapperBuilder jackson2ObjectMapperBuilder() {
        JavaTimeModule javaTimeModule = new JavaTimeModule();
        javaTimeModule.addSerializer(OffsetDateTime.class, JSR310DateTimeSerializer.INSTANCE);
        javaTimeModule.addSerializer(ZonedDateTime.class, JSR310DateTimeSerializer.INSTANCE);
        javaTimeModule.addSerializer(LocalDateTime.class, JSR310DateTimeSerializer.INSTANCE);
        javaTimeModule.addSerializer(Instant.class, JSR310DateTimeSerializer.INSTANCE);
        javaTimeModule.addDeserializer(LocalDate.class, JSR310LocalDateDeserializer.INSTANCE);

        Hibernate5Module hibernateModule = new Hibernate5Module();
        hibernateModule.disable(Hibernate5Module.Feature.USE_TRANSIENT_ANNOTATION);

        return new Jackson2ObjectMapperBuilder()
            .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            .findModulesViaServiceLoader(true)
            .modules(javaTimeModule, hibernateModule);
    }

}
