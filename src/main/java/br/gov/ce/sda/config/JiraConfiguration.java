package br.gov.ce.sda.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.lesstif.jira.services.IssueService;

@Configuration
public class JiraConfiguration {
	
	@Bean
	public IssueService jiraIssueService() throws Exception {
		IssueService issueService = new IssueService();
		return issueService;
	}

}
