package br.gov.ce.sda.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.PessoaFisica;

public interface PessoaFisicaRepository extends JpaRepository<PessoaFisica, Long> {

	PessoaFisica findByCpf(String cpf);

}
