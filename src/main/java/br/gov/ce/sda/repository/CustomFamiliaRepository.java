package br.gov.ce.sda.repository;

import javax.inject.Inject;

import org.springframework.stereotype.Repository;

import com.querydsl.jpa.impl.JPAQueryFactory;

import br.gov.ce.sda.domain.Familia;
import br.gov.ce.sda.domain.QAgricultor;
import br.gov.ce.sda.domain.QFamilia;
import br.gov.ce.sda.domain.QParente;
import br.gov.ce.sda.domain.QPessoa;
import br.gov.ce.sda.domain.QPessoaFisica;

@Repository
public class CustomFamiliaRepository {

	@Inject
	private JPAQueryFactory queryFactory;

	public Familia findByCpfChefe(String cpf) {
		QFamilia qFamilia = QFamilia.familia;
        QAgricultor agricultor = QAgricultor.agricultor;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;

		Familia familia = queryFactory.selectFrom(qFamilia)
            .join(qFamilia.parentes, QParente.parente).fetchJoin()
            .join(qFamilia.chefe, agricultor)
            .join(agricultor.pessoaFisica, pessoaFisica)
				.where(pessoaFisica.cpf.eq(cpf)).fetchFirst();

		return familia;
	}

	public Familia findByIdChefe(Long id) {
		QFamilia qFamilia = QFamilia.familia;
		QParente qParente = QParente.parente;
		QAgricultor qAgricultor = QAgricultor.agricultor;
		QPessoaFisica qPessoaFisica = QPessoaFisica.pessoaFisica;
		QPessoa qPessoa = QPessoa.pessoa;
		Familia familia = queryFactory.selectFrom(qFamilia)
				.join(qFamilia.parentes, qParente).fetchJoin()
				.join(qParente.agricultor, qAgricultor).fetchJoin()
				.join(qAgricultor.pessoaFisica, qPessoaFisica).fetchJoin()
				.join(qPessoaFisica.pessoa, qPessoa).fetchJoin()
				.where(qFamilia.chefe.id.eq(id)).fetchFirst();

		return familia;
	}

}
