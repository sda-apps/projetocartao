package br.gov.ce.sda.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.Territorio;

/**
 * Spring Data JPA repository for the User entity.
 */
public interface TerritorioRepository extends JpaRepository<Territorio, Long> {

   List<Territorio> findAllByNomeIgnoreCaseContaining(String nome);

}
