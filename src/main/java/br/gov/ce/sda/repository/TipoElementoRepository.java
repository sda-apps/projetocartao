package br.gov.ce.sda.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.mapa.TipoElemento;

public interface TipoElementoRepository extends JpaRepository<TipoElemento, Long> {
	
	public TipoElemento findByTipo(Character tipo);

}
