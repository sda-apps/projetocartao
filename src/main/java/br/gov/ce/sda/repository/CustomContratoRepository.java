package br.gov.ce.sda.repository;

import br.gov.ce.sda.domain.QEmpresa;
import br.gov.ce.sda.domain.QMunicipio;
import br.gov.ce.sda.domain.QTerritorio;
import br.gov.ce.sda.domain.ater.Contrato;
import br.gov.ce.sda.domain.ater.QContrato;
import br.gov.ce.sda.domain.ater.QRegiao;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Repository
public class CustomContratoRepository {


    @Inject
    private JPAQueryFactory queryFactory;


    public Contrato findById(Long id) {
        QContrato contrato = QContrato.contrato;
        QEmpresa empresa = QEmpresa.empresa;
        QTerritorio territorio = QTerritorio.territorio;
        QMunicipio municipio = QMunicipio.municipio;
        QRegiao regiao = QRegiao.regiao;

        Contrato fetchOne = queryFactory.selectFrom(contrato)
            .leftJoin(contrato.empresa, empresa)
            .leftJoin(contrato.territorio, territorio)
            .leftJoin(contrato.regiao, regiao).fetchJoin()
            .leftJoin(contrato.abrangencia, municipio).fetchJoin()
            .where(contrato.id.eq(id)).fetchOne();

        return fetchOne;
    }


    public Set<Contrato> findByEmpresaId(List<Long> id) {
        QContrato contrato = QContrato.contrato;
        QEmpresa empresa = QEmpresa.empresa;
        QMunicipio abrangencia = QMunicipio.municipio;

        List<Contrato> fetch = queryFactory.selectFrom(contrato)
            .join(contrato.empresa, empresa).fetchJoin()
            .leftJoin(contrato.regiao, QRegiao.regiao).fetchJoin()
            .leftJoin(contrato.associacaoComunitaria).fetchJoin()
            .leftJoin(contrato.abrangencia, abrangencia).fetchJoin()
            .leftJoin(contrato.regiao, QRegiao.regiao).fetchJoin()
            .leftJoin(contrato.territorio, QTerritorio.territorio).fetchJoin()
            .leftJoin(abrangencia.territorio, QTerritorio.territorio).fetchJoin()
            .where(empresa.id.in(id)).fetch();

        Set<Contrato> contratos = new LinkedHashSet<>();
        contratos.addAll(fetch);

        return contratos;
    }
}
