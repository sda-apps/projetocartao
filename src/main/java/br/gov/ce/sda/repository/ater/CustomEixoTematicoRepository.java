package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.QPessoaFisica;
import br.gov.ce.sda.domain.QUsuarioAvancado;
import br.gov.ce.sda.domain.acesso.QUsuario;
import br.gov.ce.sda.domain.ater.EixoTematico;
import br.gov.ce.sda.domain.ater.QEixoTematico;
import br.gov.ce.sda.web.rest.dto.FiltroEixoTematicoDTO;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;

@Repository
public class CustomEixoTematicoRepository {

    @Inject
    private JPAQueryFactory queryFactory;

    public List<EixoTematico> findAllByFilter(FiltroEixoTematicoDTO filtro) {
        QEixoTematico eixoTematico = QEixoTematico.eixoTematico;
        QUsuario usuario = QUsuario.usuario;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
        JPAQuery<EixoTematico> query = queryFactory.selectFrom(eixoTematico)
            .leftJoin(eixoTematico.usuario, usuario).fetchJoin()
            .leftJoin(usuario.usuarioAvancado, usuarioAvancado).fetchJoin()
            .leftJoin(usuarioAvancado.pessoaFisica, pessoaFisica).fetchJoin()
            .leftJoin(pessoaFisica.pessoa).fetchJoin();

        query.where(filtro.getPredicates(eixoTematico));
        query.orderBy(eixoTematico.nome.asc());

        if (Objects.isNull(filtro.getStart())) {
            filtro.setStart(0);
        }

        filtro.setLimit(10);

        query.offset(filtro.getStart()).limit(filtro.getLimit());

        List<EixoTematico> eixoTematicoList = query.fetch();

        return eixoTematicoList;
    }

}
