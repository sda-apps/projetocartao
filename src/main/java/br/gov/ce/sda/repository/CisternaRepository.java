package br.gov.ce.sda.repository;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Repository;

import com.querydsl.jpa.impl.JPAQueryFactory;

import br.gov.ce.sda.domain.QAgricultor;
import br.gov.ce.sda.domain.QEmpresa;
import br.gov.ce.sda.domain.mapa.QCoordenada;
import br.gov.ce.sda.domain.mapa.QElemento;
import br.gov.ce.sda.domain.mapa.QTipoElemento;
import br.gov.ce.sda.domain.programaseprojeto.Cisterna;
import br.gov.ce.sda.domain.programaseprojeto.QCisterna;
import br.gov.ce.sda.domain.programaseprojeto.QProjeto;


@Repository
public class CisternaRepository {
	
	@Inject
	private JPAQueryFactory queryFactory;
	
	public List<Cisterna> buscarCisternasPorAgricultor(Long agricultorId) {
		QCisterna cisterna = QCisterna.cisterna;
		QAgricultor agricultor = QAgricultor.agricultor;
		QEmpresa empresa = QEmpresa.empresa;
		QProjeto projeto = QProjeto.projeto;
		QElemento elemento = QElemento.elemento;
		QCoordenada coordenada = QCoordenada.coordenada;
		QTipoElemento tipoElemento = QTipoElemento.tipoElemento;
		
		List<Cisterna> cisternas = queryFactory.selectFrom(cisterna)
			.join(cisterna.agricultor, agricultor).fetchJoin()
			.leftJoin(cisterna.empresa, empresa).fetchJoin()
			.leftJoin(cisterna.projeto, projeto).fetchJoin()
			.leftJoin(cisterna.elemento, elemento).fetchJoin()
			.leftJoin(elemento.coordenada, coordenada).fetchJoin()
			.where(cisterna.agricultor.id.eq(agricultorId)).fetch();
		
		return cisternas;
	}
}
