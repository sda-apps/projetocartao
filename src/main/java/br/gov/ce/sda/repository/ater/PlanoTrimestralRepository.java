package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.ater.PlanoTrimestral;
import br.gov.ce.sda.domain.ater.TecnicoPTA;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PlanoTrimestralRepository extends JpaRepository<PlanoTrimestral, Long> {

    List<PlanoTrimestral> findByPlanoTrabalhoAnual_Id(Long id);

    List<PlanoTrimestral> findByPlanoTrabalhoAnual_IdIn(List<Long> id);

    List<PlanoTrimestral> findByTrimestreAndPlanoTrabalhoAnual_Id(Integer trimestre, Long id);

}
