package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.ater.AssociacaoComunitaria;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AssociacaoComunitariaRepository extends JpaRepository<AssociacaoComunitaria, Long> {

}
