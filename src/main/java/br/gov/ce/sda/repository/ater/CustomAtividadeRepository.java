package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.QPessoaFisica;
import br.gov.ce.sda.domain.QUsuarioAvancado;
import br.gov.ce.sda.domain.acesso.QUsuario;
import br.gov.ce.sda.domain.ater.AtividadePTA;
import br.gov.ce.sda.domain.ater.QAcao;
import br.gov.ce.sda.domain.ater.QAtividadePTA;
import br.gov.ce.sda.web.rest.dto.FiltroAtividadeDTO;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;

@Repository
public class CustomAtividadeRepository {

    @Inject
    private JPAQueryFactory queryFactory;

    public List<AtividadePTA> findAllByFilter(FiltroAtividadeDTO filtro){
        QAtividadePTA atividade = QAtividadePTA.atividadePTA;
        QAcao acao = QAcao.acao;
        QUsuario usuario = QUsuario.usuario;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
        JPAQuery<AtividadePTA> query = queryFactory.selectFrom(atividade)
            .leftJoin(atividade.acao, acao).fetchJoin()
            .leftJoin(atividade.usuario,usuario).fetchJoin()
            .leftJoin(usuario.usuarioAvancado,usuarioAvancado).fetchJoin()
            .leftJoin(usuarioAvancado.pessoaFisica,pessoaFisica).fetchJoin()
            .leftJoin(pessoaFisica.pessoa).fetchJoin();

        query.where(filtro.getPredicates(atividade, acao));
        query.where(atividade.inativo.isFalse().or(atividade.inativo.isNull()));
        query.orderBy(atividade.nome.asc());

        if (Objects.isNull(filtro.getStart())) {
            filtro.setStart(0);
        }

        filtro.setLimit(10);

        query.offset(filtro.getStart()).limit(filtro.getLimit());

        List<AtividadePTA> atividades = query.fetch();

        // Remover atividades duplicadas
        LinkedHashSet<AtividadePTA> atividadesHashSet = Sets.newLinkedHashSet(atividades);
        atividades = Lists.newArrayList(atividadesHashSet);

        return atividades;
    }

    public AtividadePTA findById(Long id) {
        QAtividadePTA atividade = QAtividadePTA.atividadePTA;
        QAcao acao = QAcao.acao;

        AtividadePTA fetchOne = queryFactory.selectFrom(atividade)
            .leftJoin(atividade.acao, acao).fetchJoin()
            .where(atividade.id.eq(id)).fetchOne();

        return fetchOne;
    }
}
