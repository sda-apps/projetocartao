package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.QMunicipio;
import br.gov.ce.sda.domain.ater.AbrangenciaRegiao;
import br.gov.ce.sda.domain.ater.QAbrangenciaRegiao;
import br.gov.ce.sda.domain.ater.QRegiao;
import br.gov.ce.sda.domain.ater.Regiao;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.List;

@Repository
public class CustomAbrangenciaRegiaoRepository {

    @Inject
    private JPAQueryFactory queryFactory;

    public List<AbrangenciaRegiao> findByRegiao (String nomeMunicipio,Long regiaoId){
        QMunicipio municipio = QMunicipio.municipio;
        QRegiao regiao = QRegiao.regiao;

        JPAQuery<AbrangenciaRegiao> query = buildQuery(municipio, regiao);

        query.where(regiao.id.eq(regiaoId).and(municipio.nome.containsIgnoreCase(nomeMunicipio)));

        return query.fetch();
    }

    public List<AbrangenciaRegiao> findByRegiao (String nomeMunicipio, List<Long> regiaoId){
        QMunicipio municipio = QMunicipio.municipio;
        QRegiao regiao = QRegiao.regiao;

        JPAQuery<AbrangenciaRegiao> query = buildQuery(municipio, regiao);

        query.where(regiao.id.in(regiaoId).and(municipio.nome.containsIgnoreCase(nomeMunicipio))).fetch();

        return query.fetch();
    }

    public List<AbrangenciaRegiao> findByRegiao (List<Long> regiaoId){
        QMunicipio municipio = QMunicipio.municipio;
        QRegiao regiao = QRegiao.regiao;

        JPAQuery<AbrangenciaRegiao> query = buildQuery(municipio, regiao);

        query.where(regiao.id.in(regiaoId)).fetch();

        return query.fetch();
    }

    private JPAQuery<AbrangenciaRegiao> buildQuery(QMunicipio municipio, QRegiao regiao) {
        QAbrangenciaRegiao abrangenciaRegiao = QAbrangenciaRegiao.abrangenciaRegiao;

        JPAQuery<AbrangenciaRegiao> query = queryFactory.selectFrom(abrangenciaRegiao)
            .join(abrangenciaRegiao.municipio, municipio).fetchJoin()
            .join(abrangenciaRegiao.regiao, regiao).fetchJoin();

        return query;
    }

    public List<Long> getMunicipiosByRegioes (List<Long> regioesId) {
        QAbrangenciaRegiao abrangenciaRegiao = QAbrangenciaRegiao.abrangenciaRegiao;
        QMunicipio municipio = QMunicipio.municipio;

        List<Long> municipios = queryFactory.select(abrangenciaRegiao.municipio.id).from(abrangenciaRegiao)
            .join(abrangenciaRegiao.municipio, municipio)
            .where(abrangenciaRegiao.regiao.id.in(regioesId))
            .fetch();

        return municipios;
    }

    public List<Long> getMunicipiosByRegiao (Long regiaoId) {
        QAbrangenciaRegiao abrangenciaRegiao = QAbrangenciaRegiao.abrangenciaRegiao;
        QMunicipio municipio = QMunicipio.municipio;

        List<Long> municipios = queryFactory.select(abrangenciaRegiao.municipio.id)
            .from(abrangenciaRegiao)
            .join(abrangenciaRegiao.municipio, municipio)
            .where(abrangenciaRegiao.regiao.id.eq(regiaoId))
            .fetch();

        return municipios;
    }

    public Regiao findRegiaoByMunicipio(Long municipioId) {
        QMunicipio municipio = QMunicipio.municipio;
        QRegiao regiao = QRegiao.regiao;

        JPAQuery<AbrangenciaRegiao> query = buildQuery(municipio, regiao);

        query.where(municipio.id.eq(municipioId).and(regiao.inativo.isFalse()));

        AbrangenciaRegiao ar = query.fetchFirst();

        if(ar != null) {
            return ar.getRegiao();
        } else {
            return null;
        }

    }
}
