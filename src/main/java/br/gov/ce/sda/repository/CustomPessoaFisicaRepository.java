package br.gov.ce.sda.repository;

import javax.inject.Inject;

import org.springframework.stereotype.Repository;

import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

import br.gov.ce.sda.domain.PessoaFisica;
import br.gov.ce.sda.domain.QPessoa;
import br.gov.ce.sda.domain.QPessoaFisica;

@Repository
public class CustomPessoaFisicaRepository {

	@Inject
	private JPAQueryFactory queryFactory;
	
	public PessoaFisica buscarPessoaPorCpf(String cpf) {
		QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
		QPessoa pessoa = QPessoa.pessoa;
		
		JPAQuery<PessoaFisica> query = queryFactory.selectFrom(pessoaFisica)
				.join(pessoaFisica.pessoa, pessoa).fetchJoin()
				.where(pessoaFisica.cpf.eq(cpf));
		
		return query.fetchOne();
	}
}