package br.gov.ce.sda.repository.mobile;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.mobile.AtividadeCadastro;

public interface AtividadeCadastroRepository extends JpaRepository<AtividadeCadastro, Long> {
	
	
}
