package br.gov.ce.sda.repository;

import br.gov.ce.sda.domain.*;
import br.gov.ce.sda.domain.ater.QContrato;
import br.gov.ce.sda.domain.ater.QRegiao;
import br.gov.ce.sda.web.rest.dto.EmpresaDTO;
import br.gov.ce.sda.web.rest.dto.FiltroEmpresaDTO;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class CustomEmpresaRepository {

    @Inject
    private JPAQueryFactory queryFactory;

    public List<Empresa> findAllByNomeIgnoreCaseContainingOrderByNomeAsc(String empresa) {
        QEmpresa qEmpresa = QEmpresa.empresa;
        QPessoaJuridica pessoaJuridica = QPessoaJuridica.pessoaJuridica;

        List<Empresa> empresas = queryFactory.selectFrom(qEmpresa)
            .join(qEmpresa.pessoaJuridica, pessoaJuridica).fetchJoin()
            .join(pessoaJuridica.pessoa, QPessoa.pessoa).fetchJoin()
            .where(qEmpresa.pessoaJuridica.nomeFantasia.containsIgnoreCase(empresa)).fetch();

        return empresas;
    }

    public List<Empresa> findAllByPauloFreireOrderByNomeAsc() {
        QEmpresa qEmpresa = QEmpresa.empresa;
        QPessoaJuridica pessoaJuridica = QPessoaJuridica.pessoaJuridica;

        List<Empresa> empresas = queryFactory.selectFrom(qEmpresa)
            .join(qEmpresa.pessoaJuridica, pessoaJuridica).fetchJoin()
            .join(pessoaJuridica.pessoa, QPessoa.pessoa).fetchJoin()
            .where(qEmpresa.isProjetoPauloFreire.isTrue()).orderBy(qEmpresa.pessoaJuridica.nomeFantasia.asc()).fetch();

        return empresas;
    }

    public List<Empresa> findAllByNomeOrByNomeFantasiaOrderByNomeAsc(String empresa) {
        QEmpresa qEmpresa = QEmpresa.empresa;
        QPessoaJuridica pessoaJuridica = QPessoaJuridica.pessoaJuridica;

        List<Empresa> empresas = queryFactory.selectFrom(qEmpresa)
            .join(qEmpresa.pessoaJuridica, pessoaJuridica).fetchJoin()
            .join(pessoaJuridica.pessoa, QPessoa.pessoa).fetchJoin()
            .where(qEmpresa.isProjetoPauloFreire.isTrue().and(qEmpresa.pessoaJuridica.pessoa.nome.containsIgnoreCase(empresa).or(qEmpresa.pessoaJuridica.nomeFantasia.containsIgnoreCase(empresa)))).fetch();

        return empresas;
    }

    public Empresa findById(Long id) {
        QEmpresa empresa = QEmpresa.empresa;
        QPessoaJuridica pessoaJuridica = QPessoaJuridica.pessoaJuridica;
        QPessoa pessoa = QPessoa.pessoa;
        QMunicipio municipio = QMunicipio.municipio;
        QUf uf = QUf.uf;
        QTelefone telefone = QTelefone.telefone;

        Empresa fetchOne = queryFactory.selectFrom(empresa)
            .join(empresa.pessoaJuridica, pessoaJuridica).fetchJoin()
            .join(pessoaJuridica.pessoa, pessoa).fetchJoin()
            .leftJoin(pessoa.municipio, municipio).fetchJoin()
            .leftJoin(pessoa.uf, uf).fetchJoin()
            .leftJoin(pessoa.telefones, telefone).fetchJoin()
            .leftJoin(empresa.nacionalidadeRepresentante, QPais.pais).fetchJoin()
            .leftJoin(empresa.tituloRepresentante, QCargo.cargo).fetchJoin()
            .leftJoin(empresa.estadoCivilRepresentante, QEstadoCivil.estadoCivil).fetchJoin()
            .leftJoin(empresa.profissaoRepresentante, QFuncao.funcao).fetchJoin()
            .where(empresa.id.eq(id)).fetchOne();

        return fetchOne;
    }

    public List<Empresa> findAllByFilter(FiltroEmpresaDTO filtro) {
        QEmpresa empresa = QEmpresa.empresa;
        QPessoaJuridica pessoaJuridica = QPessoaJuridica.pessoaJuridica;
        QContrato contrato = QContrato.contrato;
        QPessoa pessoa = QPessoa.pessoa;

        JPAQuery<Empresa> query = queryFactory.selectFrom(empresa)
            .join(empresa.pessoaJuridica, pessoaJuridica).fetchJoin()
            .join(pessoaJuridica.pessoa, pessoa).fetchJoin()
            .leftJoin(empresa.contrato, contrato).fetchJoin()
            .leftJoin(contrato.regiao).fetchJoin();

        query.where(empresa.isProjetoPauloFreire.isTrue());
        query.where(filtro.getPredicates(pessoaJuridica, contrato));

        filtro.setSubquery(query, contrato);

        query.orderBy(pessoa.nome.asc());

        if (Objects.isNull(filtro.getStart())) {
            filtro.setStart(0);
        }

        filtro.setLimit(10);

        query.offset(filtro.getStart()).limit(filtro.getLimit());

        List<Empresa> empresas = query.fetch();

        // Remover empresas duplicadas
        LinkedHashSet<Empresa> empresaHashSet = Sets.newLinkedHashSet(empresas);
        empresas = Lists.newArrayList(empresaHashSet);

        return empresas;
    }

    public List<EmpresaDTO> findAllByRegiao(List<Long> regiaoId) {
        QEmpresa empresa = QEmpresa.empresa;
        QContrato contrato = QContrato.contrato;
        QPessoaJuridica pessoaJuridica = QPessoaJuridica.pessoaJuridica;
        QPessoa pessoa = QPessoa.pessoa;
        QRegiao regiao = QRegiao.regiao;

        JPAQuery<Empresa> query = queryFactory.selectFrom(empresa)
            .leftJoin(empresa.contrato, contrato).fetchJoin()
            .leftJoin(empresa.pessoaJuridica, pessoaJuridica).fetchJoin()
            .leftJoin(pessoaJuridica.pessoa, pessoa).fetchJoin()
            .leftJoin(contrato.regiao, regiao).fetchJoin();

        query.where(regiao.id.in(regiaoId));

        query.where(empresa.isProjetoPauloFreire.eq(true));
        query.orderBy(pessoaJuridica.nomeFantasia.asc());

        // Remover empresas duplicadas
        LinkedHashSet<Empresa> empresaHashSet = Sets.newLinkedHashSet(query.fetch());
        List<Empresa> empresas = Lists.newArrayList(empresaHashSet);

        return empresas.stream().map(empresa1 -> new EmpresaDTO(empresa1)).collect(Collectors.toList());
    }

}
