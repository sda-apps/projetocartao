package br.gov.ce.sda.repository;

import br.gov.ce.sda.domain.Agricultor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AgricultorRepository extends JpaRepository<Agricultor, Long> {

}
