package br.gov.ce.sda.repository;

import br.gov.ce.sda.config.audit.AuditEventConverter;
import br.gov.ce.sda.domain.acesso.PersistentAuditEvent;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.boot.actuate.audit.AuditEventRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

/**
 * Wraps an implementation of Spring Boot's AuditEventRepository.
 */
@Repository
public class CustomAuditEventRepository {

    @Inject
    private PersistenceAuditEventRepository persistenceAuditEventRepository;

    @Bean
    public AuditEventRepository auditEventRepository() {
        return new AuditEventRepository() {

            private static final String AUTHORIZATION_FAILURE = "AUTHORIZATION_FAILURE";

            private static final String ANONYMOUS_USER = "anonymousUser";

            @Inject
            private AuditEventConverter auditEventConverter;

            @Override
            @Transactional(propagation = Propagation.REQUIRES_NEW)
            public void add(AuditEvent event) {
                if (!AUTHORIZATION_FAILURE.equals(event.getType()) &&
                    !ANONYMOUS_USER.equals(event.getPrincipal().toString())) {

                    PersistentAuditEvent persistentAuditEvent = new PersistentAuditEvent();
                    persistentAuditEvent.setPrincipal(event.getPrincipal());
                    persistentAuditEvent.setAuditEventType(event.getType());
                    persistentAuditEvent.setAuditEventDate(LocalDateTime.ofInstant(event.getTimestamp(), ZoneId.systemDefault()));
                    persistentAuditEvent.setData(auditEventConverter.convertDataToStrings(event.getData()));
                    persistenceAuditEventRepository.save(persistentAuditEvent);
                }
            }

            @Override
            public List<AuditEvent> find(String principal, Instant after, String type) {
                Iterable<PersistentAuditEvent> persistentAuditEvents;
                if (principal == null) {
                    persistentAuditEvents = persistenceAuditEventRepository.findAll();
                } else {
                    persistentAuditEvents = persistenceAuditEventRepository.findByPrincipal(principal);
                }

                return auditEventConverter.convertToAuditEvent(persistentAuditEvents);
            }

        };
    }
}
