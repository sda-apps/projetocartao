package br.gov.ce.sda.repository.mobile;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Repository;

import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

import br.gov.ce.sda.domain.mobile.ProdutoPaaPoliticaPublica;
import br.gov.ce.sda.domain.mobile.QProdutoPaaPoliticaPublica;
import br.gov.ce.sda.domain.programaseprojeto.QProdutoEspecificacao;

@Repository
public class CustomProdutoPaaPoliticaPublicaRepository {
	
	@Inject
	private JPAQueryFactory queryFactory;
	
	public List<ProdutoPaaPoliticaPublica> findByPoliticasPublicasId(Long id) {
		QProdutoPaaPoliticaPublica produtoPaaPoliticaPublica = QProdutoPaaPoliticaPublica.produtoPaaPoliticaPublica;
		
		JPAQuery<ProdutoPaaPoliticaPublica> query = queryFactory.selectFrom(produtoPaaPoliticaPublica)
				.join(produtoPaaPoliticaPublica.produtoEspecificacao, QProdutoEspecificacao.produtoEspecificacao).fetchJoin()
    			.where(produtoPaaPoliticaPublica.politicasPublicas.id.eq(id));
		
		return query.fetch();
	}

	
}