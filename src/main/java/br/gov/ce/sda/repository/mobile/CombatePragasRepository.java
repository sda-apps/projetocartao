package br.gov.ce.sda.repository.mobile;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.mobile.CombatePragas;

public interface CombatePragasRepository extends JpaRepository<CombatePragas, Long> {
	
	public List<CombatePragas> findAllByAtividadeProdutiva_Id(Long id);
	
}
