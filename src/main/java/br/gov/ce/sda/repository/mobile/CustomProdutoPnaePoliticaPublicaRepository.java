package br.gov.ce.sda.repository.mobile;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Repository;

import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

import br.gov.ce.sda.domain.mobile.ProdutoPnaePoliticaPublica;
import br.gov.ce.sda.domain.mobile.QProdutoPnaePoliticaPublica;
import br.gov.ce.sda.domain.programaseprojeto.QProdutoEspecificacao;

@Repository
public class CustomProdutoPnaePoliticaPublicaRepository {
	
	@Inject
	private JPAQueryFactory queryFactory;
	
	public List<ProdutoPnaePoliticaPublica> findByPoliticasPublicasId(Long id) {
		QProdutoPnaePoliticaPublica produtoPnaePoliticaPublica = QProdutoPnaePoliticaPublica.produtoPnaePoliticaPublica;
		
		JPAQuery<ProdutoPnaePoliticaPublica> query = queryFactory.selectFrom(produtoPnaePoliticaPublica)
				.join(produtoPnaePoliticaPublica.produtoEspecificacao, QProdutoEspecificacao.produtoEspecificacao).fetchJoin()
    			.where(produtoPnaePoliticaPublica.politicasPublicas.id.eq(id));
		
		return query.fetch();
	}

	
}