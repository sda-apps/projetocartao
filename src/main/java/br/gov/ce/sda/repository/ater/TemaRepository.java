package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.ater.Tema;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TemaRepository extends JpaRepository<Tema, Long> {

    Tema findOneById(Long id);

    List<Tema> findAllByNomeIgnoreCaseContaining(String nome);
}
