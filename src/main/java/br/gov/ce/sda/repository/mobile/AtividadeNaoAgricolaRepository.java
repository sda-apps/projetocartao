package br.gov.ce.sda.repository.mobile;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.mobile.AtividadeNaoAgricola;

public interface AtividadeNaoAgricolaRepository extends JpaRepository<AtividadeNaoAgricola, Long> {
	
	public List<AtividadeNaoAgricola> findAllByAtividadeProdutiva_Id(Long id);
	
}
