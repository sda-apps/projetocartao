package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.QPessoa;
import br.gov.ce.sda.domain.QPessoaFisica;
import br.gov.ce.sda.domain.QUsuarioAvancado;
import br.gov.ce.sda.domain.acesso.QUsuario;
import br.gov.ce.sda.domain.ater.HistoricoAvaliacaoTarefa;
import br.gov.ce.sda.domain.ater.QHistoricoAvaliacaoTarefa;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.List;

@Repository
public class CustomHistoricoAvaliacaoTarefaRepository {

    @Inject
    private JPAQueryFactory queryFactory;

    public List<HistoricoAvaliacaoTarefa> findByTarefaId(Long id) {

        QHistoricoAvaliacaoTarefa historicoAvaliacaoTarefa = QHistoricoAvaliacaoTarefa.historicoAvaliacaoTarefa;
        QUsuario qUsuario = QUsuario.usuario;
        QUsuarioAvancado qUsuarioAvancado = QUsuarioAvancado.usuarioAvancado;
        QPessoaFisica qPessoaFisica = QPessoaFisica.pessoaFisica;

        JPAQuery<HistoricoAvaliacaoTarefa> query = queryFactory.selectFrom(historicoAvaliacaoTarefa)
            .leftJoin(historicoAvaliacaoTarefa.usuario, qUsuario).fetchJoin()
            .leftJoin(qUsuario.usuarioAvancado, qUsuarioAvancado).fetchJoin()
            .leftJoin(qUsuarioAvancado.pessoaFisica, qPessoaFisica).fetchJoin()
            .leftJoin(qPessoaFisica.pessoa, QPessoa.pessoa).fetchJoin()
            .where(historicoAvaliacaoTarefa.tarefa.id.eq(id));

        return query.fetch();
    }
}
