package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.config.security.AuthoritiesConstants;
import br.gov.ce.sda.config.security.SecurityUtils;
import br.gov.ce.sda.domain.*;
import br.gov.ce.sda.domain.acesso.Autoridade;
import br.gov.ce.sda.domain.acesso.QUsuario;
import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.domain.ater.*;
import br.gov.ce.sda.repository.AuthorityRepository;
import br.gov.ce.sda.repository.CustomUsuarioAvancadoRepository;
import br.gov.ce.sda.web.rest.dto.PlanoTrabalhoAnualDTO;
import br.gov.ce.sda.web.rest.dto.RelatorioSinteticoATCsDTO;
import br.gov.ce.sda.web.rest.dto.TotalMetasATCsDTO;
import br.gov.ce.sda.web.rest.dto.ater.FiltroPlanoTrabalhoAnualDTO;
import com.google.common.collect.Sets;
import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

import static br.gov.ce.sda.domain.ater.PlanoTrabalhoAnual.Situacao.ATIVO;
import static br.gov.ce.sda.domain.ater.TecnicoPTA.Perfil.TECNICO_CAMPO;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Repository
public class CustomPlanoTrabalhoAnualRepository {

    private static final int DEFAULT_LIMIT_QUERY = 10;

    @Inject
    private JPAQueryFactory queryFactory;

    @Inject
    private AuthorityRepository authorityRepository;

    @Inject
    private CustomUsuarioAvancadoRepository customUsuarioAvancadoRepository;

    public PlanoTrabalhoAnual findById(Long planosAnualId) {
        QPlanoTrabalhoAnual planoAnual = QPlanoTrabalhoAnual.planoTrabalhoAnual;
        QContrato contrato = QContrato.contrato;
        QUsuarioAvancado coordenador = QUsuarioAvancado.usuarioAvancado;
        QMetaFamiliasPTA metaFamiliasPTA = QMetaFamiliasPTA.metaFamiliasPTA;
        QMetaAtividadesPTA metaAtividadesPTA = QMetaAtividadesPTA.metaAtividadesPTA;
        QEmpresa empresa = QEmpresa.empresa;
        QPessoaJuridica pessoaJuridica = QPessoaJuridica.pessoaJuridica;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
        QHistoricoAvaliacaoPlanos historicoAvaliacaoPlanos = QHistoricoAvaliacaoPlanos.historicoAvaliacaoPlanos;
        QUsuario usuario = QUsuario.usuario;
        QUsuarioAvancado usuarioAvancadoHistorico = QUsuarioAvancado.usuarioAvancado;
        QPessoaFisica pessoaFisicaHistorico = QPessoaFisica.pessoaFisica;

        PlanoTrabalhoAnual plano = queryFactory.selectFrom(planoAnual)
            .leftJoin(planoAnual.contrato, contrato).fetchJoin()
            .leftJoin(contrato.empresa, empresa).fetchJoin()
            .leftJoin(contrato.regiao, QRegiao.regiao).fetchJoin()
            .leftJoin(empresa.pessoaJuridica, pessoaJuridica).fetchJoin()
            .leftJoin(pessoaJuridica.pessoa, QPessoa.pessoa).fetchJoin()
            .leftJoin(planoAnual.coordenador, coordenador).fetchJoin()
            .leftJoin(coordenador.pessoaFisica, pessoaFisica).fetchJoin()
            .leftJoin(pessoaFisica.pessoa, QPessoa.pessoa).fetchJoin()
            .leftJoin(planoAnual.metasAtividadesPta, metaAtividadesPTA).fetchJoin()
            .leftJoin(metaAtividadesPTA.atividade, QAtividadePTA.atividadePTA).fetchJoin()
            .leftJoin(contrato.abrangencia, QMunicipio.municipio).fetchJoin()
            .leftJoin(planoAnual.metasFamiliasPta, metaFamiliasPTA).fetchJoin()
            .leftJoin(metaFamiliasPTA.municipio, QMunicipio.municipio).fetchJoin()
            .leftJoin(metaFamiliasPTA.comunidade, QComunidade.comunidade).fetchJoin()

            //.leftJoin(planoAnual.historico, historicoAvaliacaoPlanos).fetchJoin()
            //.leftJoin(historicoAvaliacaoPlanos.usuario, usuario).fetchJoin()
            //.leftJoin(usuario.usuarioAvancado, usuarioAvancadoHistorico).fetchJoin()
            //.leftJoin(usuarioAvancadoHistorico.pessoaFisica, pessoaFisicaHistorico).fetchJoin()
            //.leftJoin(pessoaFisicaHistorico.pessoa, QPessoa.pessoa).fetchJoin()

            .where(planoAnual.id.eq(planosAnualId)).fetchOne();

        return plano;
    }

    public Set<PlanoTrabalhoAnual> findAll() {
        QPlanoTrabalhoAnual planoAnual = QPlanoTrabalhoAnual.planoTrabalhoAnual;
        QContrato contrato = QContrato.contrato;
        QEmpresa empresa = QEmpresa.empresa;

        JPAQuery<PlanoTrabalhoAnual> query = queryFactory.selectFrom(planoAnual)
            .leftJoin(planoAnual.contrato, contrato).fetchJoin()
            .leftJoin(contrato.regiao, QRegiao.regiao).fetchJoin()
            .leftJoin(contrato.empresa, empresa).fetchJoin()
            .leftJoin(empresa.pessoaJuridica, QPessoaJuridica.pessoaJuridica).fetchJoin();

        return Sets.newLinkedHashSet(query.fetch());
    }

    public List<PlanoTrabalhoAnualDTO> findAllByFiltro(FiltroPlanoTrabalhoAnualDTO filtro) {
        QPlanoTrabalhoAnual planoAnual = QPlanoTrabalhoAnual.planoTrabalhoAnual;
        QContrato contrato = QContrato.contrato;
        QEmpresa parceira = QEmpresa.empresa;
        QPessoaJuridica pessoaJuridica = QPessoaJuridica.pessoaJuridica;
        QUsuarioAvancado coordenador = QUsuarioAvancado.usuarioAvancado;
        QPessoa pessoa = QPessoa.pessoa;

        JPAQuery<PlanoTrabalhoAnual> query = queryFactory.selectFrom(planoAnual)
            .leftJoin(planoAnual.contrato, contrato).fetchJoin()
            .leftJoin(contrato.empresa, parceira).fetchJoin()
            .leftJoin(contrato.regiao, QRegiao.regiao).fetchJoin()
            .leftJoin(parceira.pessoaJuridica, pessoaJuridica).fetchJoin()
            .leftJoin(pessoaJuridica.pessoa, pessoa).fetchJoin()
            .leftJoin(planoAnual.coordenador, coordenador).fetchJoin();

        query.where(filtro.getPredicates(planoAnual));

        setRegrasDeNegocio(planoAnual, query);

        filtro.setSubquery(query, contrato);

        query.orderBy(planoAnual.dataInicio.desc());
        query.orderBy(pessoa.nome.asc());

        if (isNull(filtro.getStart())) {
            filtro.setStart(0);
        }

        if (isNull(filtro.getLimit())) {
            filtro.setLimit(10);
        }

        query.offset(filtro.getStart()).limit(filtro.getLimit());

        return query.fetch().stream().map(plano -> new PlanoTrabalhoAnualDTO(plano)).collect(Collectors.toList());
    }

    private void setRegrasDeNegocio(QPlanoTrabalhoAnual planoAnual, JPAQuery<PlanoTrabalhoAnual> query) {
        Usuario usuario = SecurityUtils.getCurrentUser();
        UsuarioAvancado usuarioAvancado = customUsuarioAvancadoRepository.buscarPorId(usuario.getUsuarioAvancado().getId());

        if (usuario.getAuthoritiesList().contains(AuthoritiesConstants.RESPONSAVEL_ATER)) {
            query.where(planoAnual.contrato.empresa.id.eq(usuarioAvancado.getEmpresa().getId()));
        }

        Optional<String> autoridade = usuario.getAutoridades().stream().map(Autoridade::getNome).findFirst();
        verificarAutoridadeUsuarioAter(usuario, autoridade);

        if (usuario.getAuthoritiesList().contains(AuthoritiesConstants.COORDENADOR_ATER)) {
            query.where(planoAnual.contrato.empresa.id.eq(usuarioAvancado.getEmpresa().getId()));
            query.where(planoAnual.coordenador.id.eq(usuarioAvancado.getId()));
        }

        if (usuario.getAuthoritiesList().contains(AuthoritiesConstants.ASSESSOR_ATER)) {
            QTecnicoPTA qTecnicoPTA = QTecnicoPTA.tecnicoPTA;

            query.where(planoAnual.contrato.empresa.id.eq(usuarioAvancado.getEmpresa().getId()));
            query.join(planoAnual.tecnicos, qTecnicoPTA)
                .where(qTecnicoPTA.tecnico.id.eq(usuarioAvancado.getId()));
        }
    }

    @SuppressWarnings("Duplicates")
    public void verificarAutoridadeUsuarioAter(Usuario usuario, Optional<String> autoridade) {
        if (nonNull(autoridade) && autoridade.get().equals("ROLE_USUARIO_ATER")) {
            String autoridadeUsuarioAter = findAutoridadeUsuarioAter(usuario.getUsuarioAvancado().getId());

            if (nonNull(autoridadeUsuarioAter)) {
                Autoridade authority = authorityRepository.findById(autoridadeUsuarioAter).get();
                Set<Autoridade> authorities = new HashSet<>();
                authorities.add(authority);

                usuario.setAutoridades(authorities);
            }
        }
    }

    @SuppressWarnings("Duplicates")
    public String findAutoridadeUsuarioAter(Long usuarioAvancadoId) {
        if (nonNull(findPlanoTrabalhoAnualPorCoordenadorId(usuarioAvancadoId))) {
            return "ROLE_USUARIO_COORDENADOR_ATER";
        } else if (nonNull(findPlanoTrabalhoAnualPorTecnicoId(usuarioAvancadoId))) {
            return "ROLE_USUARIO_ATER_TECNICO";
        } else if (nonNull(findPlanoTrabalhoAnualPorAssessorId(usuarioAvancadoId))) {
            return "ROLE_USUARIO_ATER_ASSESSOR";
        } else {
            return null;
        }
    }

    public List<Long> findAllTecnicosPTAPorCoordenador(Long coordenador) {
        QPlanoTrabalhoAnual planoAnual = QPlanoTrabalhoAnual.planoTrabalhoAnual;
        QTecnicoPTA tecnicoPTA = QTecnicoPTA.tecnicoPTA;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;

        List<Long> tecnicos = queryFactory.select(tecnicoPTA.tecnico.id).from(tecnicoPTA)
            .join(tecnicoPTA.planoTrabalhoAnual, planoAnual)
            .join(planoAnual.coordenador, usuarioAvancado)
            .where(usuarioAvancado.id.eq(coordenador)
                .and(planoAnual.contrato.empresa.id.eq(usuarioAvancado.empresa.id))
                .and(planoAnual.situacao.eq(PlanoTrabalhoAnual.Situacao.ATIVO.getId())))
            .fetch();

        return tecnicos;
    }

    public List<RelatorioSinteticoATCsDTO> findRelatorioAtcs() {
        Usuario usuario = SecurityUtils.getCurrentUser();

        QPlanoTrabalhoAnual planoAnual = QPlanoTrabalhoAnual.planoTrabalhoAnual;
        QContrato contrato = QContrato.contrato;
        QRegiao regiao = QRegiao.regiao;
        QEmpresa empresa = QEmpresa.empresa;
        QPessoaJuridica pessoaJuridica = QPessoaJuridica.pessoaJuridica;
        QMetaFamiliasPTA metaFamiliasPTA = QMetaFamiliasPTA.metaFamiliasPTA;

        JPAQuery<Tuple> query = queryFactory.select(planoAnual.id, regiao.nome, pessoaJuridica.nomeFantasia, contrato.codigo, metaFamiliasPTA.municipio.nome.countDistinct(), metaFamiliasPTA.comunidade.nome.countDistinct(), metaFamiliasPTA.quantidadeFamilias.sum()).from(planoAnual)
            .leftJoin(planoAnual.contrato, contrato)
            .leftJoin(contrato.regiao, regiao)
            .leftJoin(contrato.empresa, empresa)
            .leftJoin(empresa.pessoaJuridica, pessoaJuridica)
            .leftJoin(planoAnual.metasFamiliasPta, metaFamiliasPTA)
            .groupBy(regiao.nome, pessoaJuridica.nomeFantasia, contrato.codigo, planoAnual.id)
            .orderBy(regiao.nome.asc());

        if (usuario.getAuthoritiesList().contains("ROLE_RESPONSAVEL_ATER") || usuario.getAuthoritiesList().contains("ROLE_USUARIO_ATER")) {
            //usuario = userService.buscarUsuarioPorId(usuario.getId());

            query.where(empresa.id.in(usuario.getUsuarioAvancado().getEmpresa().getId()));
        }

        List<Tuple> dados = query.fetch();

        List<RelatorioSinteticoATCsDTO> dtoList = new ArrayList<>();

        for (Tuple dado : dados) {
            RelatorioSinteticoATCsDTO dto = new RelatorioSinteticoATCsDTO();
            dto.setId(dado.get(planoAnual.id));
            dto.setRegiao(dado.get(regiao.nome));
            dto.setNomeFantasia(dado.get(pessoaJuridica.nomeFantasia));
            dto.setContrato(dado.get(contrato.codigo));
            dto.setQtdMunicipio(dado.get(metaFamiliasPTA.municipio.nome.countDistinct()));
            dto.setQtdComunidade(dado.get(metaFamiliasPTA.comunidade.nome.countDistinct()));
            dto.setQtdFamilias(dado.get(metaFamiliasPTA.quantidadeFamilias.sum()));
            dtoList.add(dto);
        }

        return dtoList;

    }

    public List<TotalMetasATCsDTO> findAllMetasAtcs() {
        QPlanoTrabalhoAnual planoAnual = QPlanoTrabalhoAnual.planoTrabalhoAnual;
        QMetaFamiliasPTA metaFamiliasPTA = QMetaFamiliasPTA.metaFamiliasPTA;
        QContrato contrato = QContrato.contrato;
        QEmpresa empresa = QEmpresa.empresa;
        QPessoaJuridica pessoaJuridica = QPessoaJuridica.pessoaJuridica;

        JPAQuery<Tuple> query = queryFactory.select(pessoaJuridica.nomeFantasia, metaFamiliasPTA.quantidadeFamilias.sum()).from(planoAnual)
            .join(planoAnual.contrato, contrato)
            .join(planoAnual.metasFamiliasPta, metaFamiliasPTA)
            .join(contrato.empresa, empresa)
            .join(empresa.pessoaJuridica, pessoaJuridica)
            .groupBy(pessoaJuridica.nomeFantasia);

        List<Tuple> dados = query.fetch();

        List<TotalMetasATCsDTO> dtoList = new ArrayList<>();

        for (Tuple dado : dados) {
            TotalMetasATCsDTO dto = new TotalMetasATCsDTO();
            dto.setNomeFantasia(dado.get(pessoaJuridica.nomeFantasia));
            dto.setQtdMetas(dado.get(metaFamiliasPTA.quantidadeFamilias.sum()));
            dtoList.add(dto);
        }

        return dtoList;

    }

    public List<Long> findRegiaoPorTecnicoDoPlanoDeTrabalho(Long tecnicoId) {
        QPlanoTrabalhoAnual planoAnual = QPlanoTrabalhoAnual.planoTrabalhoAnual;
        QTecnicoPTA tecnicoPTA = QTecnicoPTA.tecnicoPTA;
        QContrato contrato = QContrato.contrato;

        List<Long> query = queryFactory.select(contrato.regiao.id).from(tecnicoPTA)
            .join(tecnicoPTA.planoTrabalhoAnual, planoAnual)
            .join(planoAnual.contrato, contrato)
            .join(contrato.regiao)
            .where(tecnicoPTA.tecnico.id.eq(tecnicoId)).fetch();

        return query;
    }

    public List findAllForErpUser(FiltroPlanoTrabalhoAnualDTO filter) {
        QViewPlanosAnuaisERP qViewPlanosAnuaisERP = QViewPlanosAnuaisERP.viewPlanosAnuaisERP;
        setStartAndLimit(filter);
        JPAQuery<ViewPlanosAnuaisERP> query = queryFactory.selectFrom(qViewPlanosAnuaisERP);
        query.where(filter.getPredicatesPlanoAnualERP(qViewPlanosAnuaisERP));
        query.offset(filter.getStart()).limit(filter.getLimit());

        return query.fetch();
    }

    public List findAllForUgpUser(FiltroPlanoTrabalhoAnualDTO filter) {
        setStartAndLimit(filter);
        QViewPlanosAnuaisUGP viewPlanosAnuaisUGP = QViewPlanosAnuaisUGP.viewPlanosAnuaisUGP;
        JPAQuery<ViewPlanosAnuaisUGP> query = queryFactory.selectFrom(viewPlanosAnuaisUGP);
        query.where(filter.getPredicatesPlanoAnualUGP(viewPlanosAnuaisUGP));
        query.offset(filter.getStart()).limit(filter.getLimit());

        return query.fetch();
    }

    private void setStartAndLimit(FiltroPlanoTrabalhoAnualDTO filter) {
        if (isNull(filter.getStart())) {
            filter.setStart(0);
        }

        filter.setLimit(DEFAULT_LIMIT_QUERY);
    }

    public PlanoTrabalhoAnual findPlanoTrabalhoAnualPorCoordenadorId(Long coordenadorId) {
        QPlanoTrabalhoAnual planoTrabalhoAnual = QPlanoTrabalhoAnual.planoTrabalhoAnual;
        QUsuarioAvancado coordenador = QUsuarioAvancado.usuarioAvancado;

        return queryFactory.selectFrom(planoTrabalhoAnual)
            .leftJoin(planoTrabalhoAnual.coordenador, coordenador)
            .where(coordenador.id.eq(coordenadorId)
                .and(planoTrabalhoAnual.situacao.eq(ATIVO.getId()))
            ).fetchFirst();
    }

    public PlanoTrabalhoAnual findPlanoTrabalhoAnualPorTecnicoId(Long tecnicoId) {
        QTecnicoPTA qTecnicoPTA = QTecnicoPTA.tecnicoPTA;
        QPlanoTrabalhoAnual planoTrabalhoAnual = QPlanoTrabalhoAnual.planoTrabalhoAnual;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;

        TecnicoPTA tecnicoPTA = queryFactory.selectFrom(qTecnicoPTA)
            .join(qTecnicoPTA.tecnico, usuarioAvancado)
            .join(qTecnicoPTA.planoTrabalhoAnual, planoTrabalhoAnual)
            .where(usuarioAvancado.id.eq(tecnicoId)
                .and(qTecnicoPTA.perfil.eq(TECNICO_CAMPO.getPerfil()))
                .and(planoTrabalhoAnual.situacao.eq(ATIVO.getId()))
            ).fetchFirst();

        if (nonNull(tecnicoPTA)) {
            return tecnicoPTA.getPlanoTrabalhoAnual();
        } else {
            return null;
        }
    }

    public PlanoTrabalhoAnual findPlanoTrabalhoAnualPorAssessorId(Long assessorId) {
        QTecnicoPTA qTecnicoPTA = QTecnicoPTA.tecnicoPTA;
        QPlanoTrabalhoAnual planoTrabalhoAnual = QPlanoTrabalhoAnual.planoTrabalhoAnual;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;

        TecnicoPTA tecnicoPTA = queryFactory.selectFrom(qTecnicoPTA)
            .join(qTecnicoPTA.tecnico, usuarioAvancado)
            .join(qTecnicoPTA.planoTrabalhoAnual, planoTrabalhoAnual).fetchJoin()
            .where(usuarioAvancado.id.eq(assessorId)
                .and(planoTrabalhoAnual.situacao.eq(ATIVO.getId()))
                .and(qTecnicoPTA.perfil.eq(TecnicoPTA.Perfil.ASSESSOR_PRODUTIVO.getPerfil())
                    .or(qTecnicoPTA.perfil.eq(TecnicoPTA.Perfil.ASSESSOR_SOCIAL.getPerfil()))
                )).fetchFirst();

        if (nonNull(tecnicoPTA)) {
            return tecnicoPTA.getPlanoTrabalhoAnual();
        } else {
            return null;
        }
    }

    public List<Long> findAllTecnicosPTAPorAssessor(Long assessorId) {
        PlanoTrabalhoAnual ptaAssessor = findPlanoTrabalhoAnualPorAssessorId(assessorId);

        QPlanoTrabalhoAnual planoAnual = QPlanoTrabalhoAnual.planoTrabalhoAnual;
        QTecnicoPTA tecnicoPTA = QTecnicoPTA.tecnicoPTA;

        List<Long> tecnicos = queryFactory.select(tecnicoPTA.tecnico.id).from(tecnicoPTA)
            .join(tecnicoPTA.planoTrabalhoAnual, planoAnual)
            .where(planoAnual.id.eq(ptaAssessor.getId()))
            .fetch();

        return tecnicos;
    }

    public Set<PlanoTrabalhoAnual> findAllByEmpresa(Long empresaId) {
        QPlanoTrabalhoAnual planoAnual = QPlanoTrabalhoAnual.planoTrabalhoAnual;
        QContrato contrato = QContrato.contrato;
        QEmpresa empresa = QEmpresa.empresa;

        JPAQuery<PlanoTrabalhoAnual> query = queryFactory.selectFrom(planoAnual)
            .leftJoin(planoAnual.contrato, contrato).fetchJoin()
            .leftJoin(contrato.regiao, QRegiao.regiao).fetchJoin()
            .leftJoin(contrato.empresa, empresa).fetchJoin()
            .leftJoin(empresa.pessoaJuridica, QPessoaJuridica.pessoaJuridica).fetchJoin()
            .where(empresa.id.eq(empresaId));

        return Sets.newLinkedHashSet(query.fetch());
    }

    public List<Comunidade> findAllComunidadeByMunicipioAndPta(Long municipio, Long ptaId) {
        QMetaFamiliasPTA metaFamiliasPTA = QMetaFamiliasPTA.metaFamiliasPTA;
        QMunicipio qMunicipio = QMunicipio.municipio;
        QPlanoTrabalhoAnual planoTrabalhoAnual = QPlanoTrabalhoAnual.planoTrabalhoAnual;
        QComunidade comunidade = QComunidade.comunidade;

        JPAQuery<Comunidade> query = queryFactory.select(comunidade).from(metaFamiliasPTA)
            .leftJoin(metaFamiliasPTA.comunidade,comunidade)
            .leftJoin(metaFamiliasPTA.planoTrabalhoAnual,planoTrabalhoAnual)
            .leftJoin(metaFamiliasPTA.municipio,qMunicipio)
            .where(planoTrabalhoAnual.id.eq(ptaId)
                .and(qMunicipio.id.eq(municipio)));

        return query.fetch();
    }
}
