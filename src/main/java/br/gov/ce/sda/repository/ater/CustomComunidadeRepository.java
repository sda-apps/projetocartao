package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.config.security.SecurityUtils;
import br.gov.ce.sda.domain.Municipio;
import br.gov.ce.sda.domain.QDistrito;
import br.gov.ce.sda.domain.QLocalidade;
import br.gov.ce.sda.domain.QMunicipio;
import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.domain.ater.Comunidade;
import br.gov.ce.sda.domain.ater.QComunidade;
import br.gov.ce.sda.repository.CustomAreaAtuacaoRepository;
import br.gov.ce.sda.web.rest.dto.FiltroComunidadeDTO;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Repository
public class CustomComunidadeRepository {

    @Inject
    private JPAQueryFactory queryFactory;

    @Inject
    private CustomAbrangenciaRegiaoRepository customAbrangenciaRegiaoRepository;

    @Inject
    private CustomAreaAtuacaoRepository customAreaAtuacaoRepository;

    public List<Comunidade> findByMunicipio(List<Municipio> municipios) {
        QComunidade qComunidade = QComunidade.comunidade;

        return queryFactory.selectFrom(qComunidade)
            .join(qComunidade.municipio, QMunicipio.municipio).fetchJoin()
            .where(qComunidade.municipio.in(municipios))
            .orderBy(qComunidade.nome.asc()).fetch();
    }

    public List<Comunidade> findAllByNomeIgnoreCaseContaining(String nome) {
        QComunidade qComunidade = QComunidade.comunidade;

        return queryFactory.selectFrom(qComunidade)
            .join(qComunidade.municipio, QMunicipio.municipio).fetchJoin()
            .where(qComunidade.nome.containsIgnoreCase(nome))
            .orderBy(qComunidade.nome.asc()).fetch();
    }

    public List<Comunidade> findAllByNomeAndMunicipioo(String nome, List<Long> municipios) {
        QComunidade qComunidade = QComunidade.comunidade;

        return queryFactory.selectFrom(qComunidade)
            .join(qComunidade.municipio, QMunicipio.municipio).fetchJoin()
            .where(qComunidade.nome.containsIgnoreCase(nome).and(qComunidade.municipio.id.in(municipios)))
            .orderBy(qComunidade.nome.asc()).fetch();
    }

    public List<Comunidade> findAllByFilter(FiltroComunidadeDTO filtro) {
        QComunidade qComunidade = QComunidade.comunidade;
        QDistrito qDistrito = QDistrito.distrito;
        QMunicipio qMunicipio = QMunicipio.municipio;
        QLocalidade qLocalidade = QLocalidade.localidade;

        JPAQuery<Comunidade> query = queryFactory.selectFrom(qComunidade)
            .leftJoin(qComunidade.distrito, qDistrito).fetchJoin()
            .leftJoin(qComunidade.municipio, qMunicipio).fetchJoin()
            .leftJoin(qComunidade.localidades, qLocalidade).fetchJoin();

        query.where(filtro.getPredicates(qComunidade, qDistrito, qMunicipio));
        filtro.setSubquery(query, qComunidade);

        if (nonNull(filtro.getRegiao())) {
            query.where(qMunicipio.id.in(customAbrangenciaRegiaoRepository.getMunicipiosByRegioes(filtro.getRegiao())));
        }

        query.orderBy(qComunidade.nome.asc());

        if (Objects.isNull(filtro.getStart())) {
            filtro.setStart(0);
        }

        filtro.setLimit(10);

        query.offset(filtro.getStart()).limit(filtro.getLimit());

        List<Comunidade> comunidades = query.fetch();

        // Remover comunidades duplicadas
        LinkedHashSet<Comunidade> comunidadeHashSet = Sets.newLinkedHashSet(comunidades);
        comunidades = Lists.newArrayList(comunidadeHashSet);

        comunidades.forEach(c -> c.getDistrito().getMunicipio().setRegiao(customAbrangenciaRegiaoRepository.findRegiaoByMunicipio(c.getDistrito().getMunicipio().getId())));

        return comunidades;
    }

    public Comunidade findById(Long id) {
        QComunidade qComunidade = QComunidade.comunidade;
        QDistrito qDistrito = QDistrito.distrito;
        QMunicipio qMunicipio = QMunicipio.municipio;
        QLocalidade qLocalidade = QLocalidade.localidade;

        return queryFactory.selectFrom(qComunidade)
            .leftJoin(qComunidade.distrito, qDistrito).fetchJoin()
            .leftJoin(qComunidade.municipio, qMunicipio).fetchJoin()
            .leftJoin(qComunidade.localidades, qLocalidade).fetchJoin()
            .where(qComunidade.id.eq(id)).fetchOne();
    }

    public List<Comunidade> buscarPorDistrito(String comunidadeNome, List<Long> longDistrito) {
        QComunidade qComunidade = QComunidade.comunidade;
        QDistrito qDistrito = QDistrito.distrito;
        JPAQuery<Comunidade> query = queryFactory.selectFrom(qComunidade)
            .leftJoin(qComunidade.distrito, qDistrito).fetchJoin()
            .leftJoin(qComunidade.municipio).fetchJoin()
            .where(qComunidade.nome.contains(comunidadeNome));

        if (longDistrito != null) {
            query.where(qDistrito.id.in(longDistrito));
        }

        return query.fetch();
    }

    public List<Comunidade> findByMunicipioAndAreaAtuacao(List<Long> municipios) {
        Usuario usuario = SecurityUtils.getCurrentUser();

        List<Comunidade> comunidadesByUsuarioAvancadoId = customAreaAtuacaoRepository.findComunidadesByUsuarioAvancadoId(usuario.getUsuarioAvancado().getId());

        return comunidadesByUsuarioAvancadoId.stream().filter(comunidade -> municipios.contains(comunidade.getMunicipio().getId())).collect(Collectors.toList());
    }
}
