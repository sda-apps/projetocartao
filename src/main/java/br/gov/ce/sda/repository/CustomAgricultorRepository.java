package br.gov.ce.sda.repository;

import static java.util.Objects.nonNull;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import br.gov.ce.sda.domain.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

import br.gov.ce.sda.domain.mapa.AgricultorAreaGeorreferenciada;
import br.gov.ce.sda.domain.mapa.QAgricultorAreaGeorreferenciada;
import br.gov.ce.sda.domain.mapa.QAreaGeorreferenciada;
import br.gov.ce.sda.domain.mapa.QCoordenada;
import br.gov.ce.sda.domain.programaseprojeto.QProjeto;
import br.gov.ce.sda.web.rest.dto.FiltroAgricultorDTO;

@Repository
public class CustomAgricultorRepository {

	@Inject
	private JPAQueryFactory queryFactory;

    @Inject
    private TipoATERRepository tipoATERRepository;

	@Transactional(readOnly = true)
    public FiltroAgricultorDTO buscarAgricultoresPorFiltro(FiltroAgricultorDTO filtro) {
		JPAQuery<Agricultor> query = construirBuscaAgricultor(filtro);

		if (Objects.isNull(filtro.getStart())) {
			filtro.setStart(0);
		}

		if (Objects.isNull(filtro.getLimit())) {
			filtro.setLimit(10);
		}

	    query.offset(filtro.getStart()).limit(filtro.getLimit());

	    filtro.setAgricultores(query.fetch());

		return filtro;
    }

	public Long buscarTotalAgricultoresPorFiltro(FiltroAgricultorDTO filtro) {
		JPAQuery<Agricultor> query = construirBuscaAgricultor(filtro);

		return query.fetchCount();
	}

	private JPAQuery<Agricultor> construirBuscaAgricultor(FiltroAgricultorDTO filtro) {
		QAgricultor agricultor = QAgricultor.agricultor;
		QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
	    QPessoa pessoa = QPessoa.pessoa;
	    QMunicipio municipio = QMunicipio.municipio;
        QTerritorio territorio = QTerritorio.territorio;

	    JPAQuery<Agricultor> query = queryFactory.selectFrom(agricultor)
	    		.join(agricultor.pessoaFisica, pessoaFisica).fetchJoin()
		    	.join(pessoaFisica.pessoa, pessoa).fetchJoin()
		    	.leftJoin(pessoa.distrito, QDistrito.distrito).fetchJoin()
		    	.leftJoin(pessoa.municipio, municipio).fetchJoin()
		    	.leftJoin(municipio.territorio, territorio).fetchJoin();

	    query.where(filtro.getPredicates(pessoa,territorio));
	    query.where(filtro.getPredicates(agricultor));

	    filtro.setSubquery(query, agricultor, tipoATERRepository);

	    if(filtro.getOrdenar() != null && filtro.getOrdenar()) {
	    	if(filtro.getOrdem().equals("asc")) {
	    		query.orderBy(agricultor.pessoaFisica.pessoa.nome.asc());
	    	} else if (filtro.getOrdem().equals("desc")){
	    		query.orderBy(agricultor.pessoaFisica.pessoa.nome.desc());
	    	}
	    }

		return query;
	}

	public List<AgricultorAreaGeorreferenciada> buscarAgricultoresAreasGeorreferenciadasPorFiltro(FiltroAgricultorDTO filtro) {
		QAgricultorAreaGeorreferenciada area = QAgricultorAreaGeorreferenciada.agricultorAreaGeorreferenciada;
        QAgricultor agricultor = QAgricultor.agricultor;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
        QPessoa pessoa = QPessoa.pessoa;
        QMunicipio municipio = QMunicipio.municipio;
        QTerritorio territorio = QTerritorio.territorio;

		JPAQuery<AgricultorAreaGeorreferenciada> query = queryFactory.selectFrom(area)
            .leftJoin(area.areaGeorreferenciada, QAreaGeorreferenciada.areaGeorreferenciada).fetchJoin()
            .leftJoin(area.agricultor, agricultor)
            .leftJoin(agricultor.pessoaFisica, pessoaFisica)
            .leftJoin(pessoaFisica.pessoa, pessoa);
            //.leftJoin(pessoa.municipio, municipio).fetchJoin()
            //.leftJoin(municipio.territorio, territorio).fetchJoin();

		query.where(filtro.getPredicates(agricultor));
        query.where(filtro.getPredicates(pessoa,territorio));
		filtro.setSubquery(query, area.agricultor, tipoATERRepository);

		return query.fetch();
	}

    @Transactional(readOnly = true)
    public List <AgricultorProjeto> buscarProjetosPorAno(Long agricultorId, Integer ano){

    	QAgricultorProjeto agricultorProjeto = QAgricultorProjeto.agricultorProjeto;

    	List<AgricultorProjeto> query = queryFactory.selectFrom(agricultorProjeto)
    			.where(agricultorProjeto.agricultor.id.eq(agricultorId).and(agricultorProjeto.ano.eq(ano)).and(agricultorProjeto.projeto.inativo.isFalse()))
    			.join(agricultorProjeto.projeto, QProjeto.projeto).fetchJoin().fetch();

    	List<AgricultorProjeto> query2 = queryFactory.selectFrom(agricultorProjeto)
    			.join(agricultorProjeto.projeto, QProjeto.projeto).fetchJoin()
    			.where(agricultorProjeto.agricultor.id.eq(agricultorId)
    			.and(agricultorProjeto.projeto.codigoSda.in(22,28,26,25))).fetch();

    	query.addAll(query2);

    	return query;
    }

    @Transactional(readOnly = true)
    public List <AgricultorProjeto> buscarProjetos(Long agricultorId){

    	QAgricultorProjeto agricultorProjeto = QAgricultorProjeto.agricultorProjeto;

    	JPAQuery<AgricultorProjeto> query = queryFactory.selectFrom(agricultorProjeto)
    			.where(agricultorProjeto.agricultor.id.eq(agricultorId).and(agricultorProjeto.projeto.inativo.isFalse()))
    			.join(agricultorProjeto.projeto, QProjeto.projeto).fetchJoin().orderBy(agricultorProjeto.ano.desc());

    	return query.fetch();
    }

    public Agricultor buscarPorId(Long agricultorId) {
    	QAgricultor agricultor = QAgricultor.agricultor;
    	QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;

    	JPAQuery<Agricultor> query = queryFactory.selectFrom(agricultor);

	    buildQueryAgricultor(agricultor, pessoaFisica, query);

	    query.where(agricultor.id.eq(agricultorId));

		return query.fetchOne();
	}

	public Agricultor buscarConjugePorAgricultorId(Long agricultorId) {
		QAgricultor qAgricultor = QAgricultor.agricultor;
        QFamilia qFamilia = QFamilia.familia;
        QParente qParente = QParente.parente;
        QPessoaFisica qPessoaFisica = QPessoaFisica.pessoaFisica;
        QPessoa qPessoa = QPessoa.pessoa;


		JPAQuery<Parente> query = queryFactory.selectFrom(qParente)
				.leftJoin(qParente.agricultor, qAgricultor).fetchJoin()
                .leftJoin(qAgricultor.pessoaFisica, qPessoaFisica).fetchJoin()
                .leftJoin(qPessoaFisica.pessoa, qPessoa).fetchJoin()
                .join(qParente.familia,qFamilia).fetchJoin()
                .where(qFamilia.chefe.id.eq(agricultorId)
                    .and(qParente.parentesco.eq(Parente.Parentesco.CONJUJE.getStatus()))
                )
            ;

		Parente parente = query.fetchOne();
		if (parente != null){
            Agricultor agricultor = parente.getAgricultor();
            return agricultor;
		}else{
		    return null;
        }
	}

	public Agricultor buscarAgricultorPorCpf(String cpf) {
		QAgricultor agricultor = QAgricultor.agricultor;
		QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
	    QPessoa pessoa = QPessoa.pessoa;

		Agricultor fetch = queryFactory.selectFrom(agricultor)
				.join(agricultor.pessoaFisica, pessoaFisica).fetchJoin()
				.join(pessoaFisica.pessoa, pessoa).fetchJoin()
			.where(pessoaFisica.cpf.eq(cpf))
			.fetchOne();

		return fetch;
	}

	@Transactional(readOnly = true)
	public Agricultor buscarAgricultorCompletoPorCpf(String cpf) {
		QAgricultor agricultor = QAgricultor.agricultor;
		QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;

	    JPAQuery<Agricultor> query = queryFactory.selectFrom(agricultor);

	    buildQueryAgricultor(agricultor, pessoaFisica, query);

	    query.where(pessoaFisica.cpf.eq(cpf));

	    Agricultor result = query.fetchOne();

	    if(nonNull(result)) {
	    	result.getDaps().size();
		    result.getPessoaFisica().getPessoa().getTelefones().size();
	    }

		return result;
	}

	private void buildQueryAgricultor(QAgricultor agricultor, QPessoaFisica pessoaFisica, JPAQuery<Agricultor> query) {
		QPessoa pessoa = QPessoa.pessoa;
		QMunicipio municipio = QMunicipio.municipio;
	    QDistrito distrito = QDistrito.distrito;
	    QCoordenada coordenada = QCoordenada.coordenada;
	    QTelefone telefone = QTelefone.telefone;

	    query.join(agricultor.pessoaFisica, pessoaFisica).fetchJoin()
	    	.join(pessoaFisica.pessoa, pessoa).fetchJoin()
	    	.leftJoin(pessoa.distrito, distrito).fetchJoin()
	    		.leftJoin(distrito.coordenada, coordenada).fetchJoin()
	    	.leftJoin(pessoa.localidade, QLocalidade.localidade).fetchJoin()
	    	.leftJoin(pessoa.telefones, telefone).fetchJoin()
	    	.leftJoin(pessoa.municipio, municipio).fetchJoin()
	    		.leftJoin(municipio.uf, QUf.uf).fetchJoin()
	    		.leftJoin(municipio.coordenada, coordenada).fetchJoin()
	    		.leftJoin(municipio.territorio, QTerritorio.territorio).fetchJoin()
	    	.leftJoin(municipio.uf, QUf.uf).fetchJoin()
	    	.leftJoin(pessoaFisica.estadoCivil, QEstadoCivil.estadoCivil).fetchJoin()
	    	.leftJoin(pessoaFisica.escolaridade, QEscolaridade.escolaridade).fetchJoin()
	    	.leftJoin(pessoaFisica.localExpedicaoRg, QUf.uf).fetchJoin()
            .leftJoin(pessoaFisica.nacionalidade, QPais.pais).fetchJoin()
	    	.leftJoin(pessoaFisica.naturalidade, QMunicipio.municipio).fetchJoin()
	    	//.leftJoin(agricultor.etnia, QEtnia.etnia).fetchJoin()
	    	.leftJoin(agricultor.areasGeorreferenciadas, QAreaGeorreferenciada.areaGeorreferenciada).fetchJoin();
	}

}
