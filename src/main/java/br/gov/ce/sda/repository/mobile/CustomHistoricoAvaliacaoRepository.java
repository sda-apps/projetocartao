package br.gov.ce.sda.repository.mobile;

import br.gov.ce.sda.domain.QPessoa;
import br.gov.ce.sda.domain.QPessoaFisica;
import br.gov.ce.sda.domain.QUsuarioAvancado;
import br.gov.ce.sda.domain.acesso.QUsuario;
import br.gov.ce.sda.domain.mobile.HistoricoAvaliacao;
import br.gov.ce.sda.domain.mobile.QHistoricoAvaliacao;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;

@Repository
public class CustomHistoricoAvaliacaoRepository {

	@Inject
	private JPAQueryFactory queryFactory;

	public HistoricoAvaliacao findById(Long id) {
		QHistoricoAvaliacao historicoAvaliacao = QHistoricoAvaliacao.historicoAvaliacao;
        QUsuario usuario = QUsuario.usuario;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;

		JPAQuery<HistoricoAvaliacao> query = queryFactory.selectFrom(historicoAvaliacao)
            .join(historicoAvaliacao.usuario, usuario).fetchJoin()
            .join(usuario.usuarioAvancado, usuarioAvancado).fetchJoin()
            .join(usuarioAvancado.pessoaFisica, pessoaFisica).fetchJoin()
            .join(pessoaFisica.pessoa, QPessoa.pessoa).fetchJoin()
    			.where(historicoAvaliacao.id.eq(id));

        return query.fetchOne();
	}

}
