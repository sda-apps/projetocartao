package br.gov.ce.sda.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.Uf;

public interface UfRepository extends JpaRepository<Uf, Long> {
	
	public List <Uf> findAllByOrderByDescricaoAsc();
	
	List<Uf> findAllByDescricaoIgnoreCaseContaining(String descricao);

}
