package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.ater.MetaAtividadesTrimestral;
import br.gov.ce.sda.domain.ater.PlanoTrimestral;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MetaAtividadeTrimestralRepository extends JpaRepository<MetaAtividadesTrimestral, Long> {

}
