package br.gov.ce.sda.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.Telefone;


public interface TelefoneRepository extends JpaRepository<Telefone, Long> {
	
	
}
