package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.ater.AbrangenciaRegiao;
import br.gov.ce.sda.domain.ater.Comunidade;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AbrangenciaRegiaoRepository extends JpaRepository<AbrangenciaRegiao, Long> {

    AbrangenciaRegiao findOneByMunicipio_IdAndRegiao_InativoIsFalse(Long municipio);

}
