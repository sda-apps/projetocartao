package br.gov.ce.sda.repository.mobile;

import br.gov.ce.sda.domain.mobile.CombatePragas;
import br.gov.ce.sda.domain.mobile.HistoricoAvaliacao;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HistoricoAvaliacaoRepository extends JpaRepository<HistoricoAvaliacao, Long> {

	List<HistoricoAvaliacao> findAllByAtividadeCadastro_Id(Long id);

}
