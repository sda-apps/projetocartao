package br.gov.ce.sda.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.ater.TipoATER;

public interface TipoATERRepository extends JpaRepository<TipoATER, Long> {

   List<TipoATER> findAllByNomeIgnoreCaseContaining(String nome);

}
