package br.gov.ce.sda.repository.ater;


import br.gov.ce.sda.domain.ater.HistoricoAvaliacaoTarefa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HistoricoAvaliacaoTarefaRepository extends JpaRepository<HistoricoAvaliacaoTarefa, Long> {

}
