package br.gov.ce.sda.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.Pais;

public interface PaisRepository extends JpaRepository<Pais, Long> {
	
	List<Pais> findAllByNacionalidadeIgnoreCaseContaining(String nome);

}
