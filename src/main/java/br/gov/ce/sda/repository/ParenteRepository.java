package br.gov.ce.sda.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.Parente;

public interface ParenteRepository extends JpaRepository<Parente, Long> {
	
}
