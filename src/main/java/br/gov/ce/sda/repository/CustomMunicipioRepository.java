package br.gov.ce.sda.repository;

import br.gov.ce.sda.domain.Municipio;
import br.gov.ce.sda.domain.QMunicipio;
import br.gov.ce.sda.domain.QTerritorio;
import br.gov.ce.sda.domain.ater.Contrato;
import br.gov.ce.sda.domain.ater.QAbrangenciaContrato;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.List;
import java.util.Set;

@Repository
public class CustomMunicipioRepository {

    @Inject
    private JPAQueryFactory queryFactory;

    public List<Municipio> buscarPorMunicipio(String municipioNome, List<Long> longTerritorios) {
        QMunicipio municipio = QMunicipio.municipio;
        QTerritorio territorio = QTerritorio.territorio;
        JPAQuery<Municipio> query = queryFactory.selectFrom(municipio)
            .leftJoin(municipio.territorio, territorio).fetchJoin()
            .where(municipio.nome.containsIgnoreCase(municipioNome)
                .and(territorio.isNotNull()))
            .orderBy(municipio.nome.asc());

        if (longTerritorios != null) {
            query.where(territorio.id.in(longTerritorios));
        }

        return query.fetch();
    }

    public List<Municipio> buscarPorContrato(Set<Contrato> contratos) {
        QAbrangenciaContrato abrangenciaContrato = QAbrangenciaContrato.abrangenciaContrato;

        JPAQuery<Municipio> query = queryFactory.selectDistinct(abrangenciaContrato.municipio).from(abrangenciaContrato)
            .where(abrangenciaContrato.contrato.in(contratos))
            .orderBy(abrangenciaContrato.municipio.nome.asc());

        return query.fetch();
    }

}
