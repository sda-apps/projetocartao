package br.gov.ce.sda.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.acesso.Autoridade;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Autoridade, String> {
}
