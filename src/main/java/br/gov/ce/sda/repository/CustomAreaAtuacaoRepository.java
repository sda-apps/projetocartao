package br.gov.ce.sda.repository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import br.gov.ce.sda.domain.QLocalidade;
import br.gov.ce.sda.domain.ater.Comunidade;
import br.gov.ce.sda.domain.ater.QComunidade;
import org.springframework.stereotype.Repository;

import com.querydsl.jpa.impl.JPAQueryFactory;

import br.gov.ce.sda.domain.Municipio;
import br.gov.ce.sda.domain.QMunicipio;
import br.gov.ce.sda.domain.mobile.AreaAtuacao;
import br.gov.ce.sda.domain.mobile.QAreaAtuacao;

@Repository
public class CustomAreaAtuacaoRepository {

	@Inject
	private JPAQueryFactory queryFactory;

	public Set<Municipio> findByUsuarioAvancadoId(Long usuarioAvancadoId) {
		QAreaAtuacao areaAtuacao = QAreaAtuacao.areaAtuacao;
		QMunicipio municipio = QMunicipio.municipio;

		List<AreaAtuacao> areas = queryFactory.selectFrom(areaAtuacao)
				.join(areaAtuacao.municipio, municipio).fetchJoin()
				.where(areaAtuacao.usuarioAvancado.id.eq(usuarioAvancadoId)).fetch();

        Set<Municipio> municipios = new HashSet<>();
		areas.forEach(area -> municipios.add(area.getMunicipio()));

		return municipios;
	}

    public List<Comunidade> findComunidadesByUsuarioAvancadoId(Long usuarioAvancadoId) {
        QAreaAtuacao areaAtuacao = QAreaAtuacao.areaAtuacao;
        QMunicipio municipio = QMunicipio.municipio;
        QComunidade qComunidade = QComunidade.comunidade;

        List<AreaAtuacao> areas = queryFactory.selectFrom(areaAtuacao)
            .join(areaAtuacao.municipio, municipio).fetchJoin()
            .join(areaAtuacao.comunidade, qComunidade).fetchJoin()
            .where(areaAtuacao.usuarioAvancado.id.eq(usuarioAvancadoId)).fetch();

        List<Comunidade> comunidades = new ArrayList<>();
        areas.forEach(area -> comunidades.add(area.getComunidade()));

        return comunidades;
    }

}
