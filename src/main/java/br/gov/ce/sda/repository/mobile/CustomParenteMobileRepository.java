package br.gov.ce.sda.repository.mobile;

import javax.inject.Inject;

import br.gov.ce.sda.domain.QEscolaridade;
import br.gov.ce.sda.domain.QFamilia;
import br.gov.ce.sda.domain.mobile.*;
import org.springframework.stereotype.Repository;

import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Repository
public class CustomParenteMobileRepository {

	@Inject
	private JPAQueryFactory queryFactory;

	public ParenteMobile findPessoaByParenteId(Long id) {
		QParenteMobile parente = QParenteMobile.parenteMobile;
		QAgricultorMobile agricultor = QAgricultorMobile.agricultorMobile;
		QPessoaFisicaMobile pessoaFisica = QPessoaFisicaMobile.pessoaFisicaMobile;

		JPAQuery<ParenteMobile> query = queryFactory.selectFrom(parente)
				.join(parente.agricultor, agricultor).fetchJoin()
                .join(parente.familia, QFamiliaMobile.familiaMobile).fetchJoin()
		    	.leftJoin(agricultor.beneficiosSociais, QBeneficioSocial.beneficioSocial).fetchJoin()
				.join(agricultor.pessoaFisica, pessoaFisica).fetchJoin()
				.join(pessoaFisica.pessoa, QPessoaMobile.pessoaMobile).fetchJoin()
                .join(pessoaFisica.escolaridade, QEscolaridade.escolaridade).fetchJoin()

    			.where(parente.id.eq(id));

		return query.fetchOne();
	}

}
