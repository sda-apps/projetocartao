package br.gov.ce.sda.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.mapa.Elemento;

public interface ElementoRepository extends JpaRepository<Elemento, Long> {

	Elemento findFirstByAgricultor_IdAndTipoElemento_Tipo(Long id, Character tipo);

	Elemento findFirstByTipoElemento_TipoAndAgricultor_Id(Character tipo, Long id);

}
