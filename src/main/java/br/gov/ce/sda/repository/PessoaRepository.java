package br.gov.ce.sda.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

	//public Pessoa findByCpf(String cpf);
	
}
