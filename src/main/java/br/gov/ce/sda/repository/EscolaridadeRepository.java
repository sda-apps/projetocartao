package br.gov.ce.sda.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.Escolaridade;

public interface EscolaridadeRepository extends JpaRepository<Escolaridade, Long> {
	
	public List <Escolaridade> findAllByOrderByDescricaoAsc();

}
