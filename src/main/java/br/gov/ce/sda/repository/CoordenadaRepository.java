package br.gov.ce.sda.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.mapa.Coordenada;

public interface CoordenadaRepository extends JpaRepository<Coordenada, Long>{
	
	public Coordenada findFirstByLatitudeAndLongitude(String latitude, String longitude);
	
}
