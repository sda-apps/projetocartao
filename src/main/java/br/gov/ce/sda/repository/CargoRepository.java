package br.gov.ce.sda.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.Cargo;


public interface CargoRepository extends JpaRepository<Cargo, Long> {
	
	public List <Cargo> findAllByDescricaoIgnoreCaseContainingOrderByDescricaoAsc(String descricao);
	
	public List <Cargo> findAllByOrderByDescricaoAsc();
	
}
