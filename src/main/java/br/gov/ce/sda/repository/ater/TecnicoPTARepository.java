package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.ater.TecnicoPTA;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TecnicoPTARepository extends JpaRepository<TecnicoPTA, Long> {

    TecnicoPTA findByTecnico_IdAndPlanoTrabalhoAnual_Id(Long idTecnico, Long idPTA);

}
