package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.ater.Tarefa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TarefaRepository extends JpaRepository<Tarefa, Long> {

    Tarefa findOneById(Long id);

}
