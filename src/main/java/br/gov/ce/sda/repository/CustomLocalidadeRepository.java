package br.gov.ce.sda.repository;

import br.gov.ce.sda.domain.*;
import br.gov.ce.sda.domain.ater.QComunidade;
import br.gov.ce.sda.domain.ater.QContrato;
import br.gov.ce.sda.domain.ater.QRegiao;
import br.gov.ce.sda.repository.ater.CustomAbrangenciaRegiaoRepository;
import br.gov.ce.sda.web.rest.dto.FiltroEmpresaDTO;
import br.gov.ce.sda.web.rest.dto.FiltroLocalidadeDTO;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;

import static java.util.Objects.nonNull;

@Repository
public class CustomLocalidadeRepository {

	@Inject
	private JPAQueryFactory queryFactory;

    @Inject
    private CustomAbrangenciaRegiaoRepository customAbrangenciaRegiaoRepository;

    public List<Localidade> findAllByFilter(FiltroLocalidadeDTO filtro){
    	QLocalidade localidade = QLocalidade.localidade;
        QDistrito distrito = QDistrito.distrito;
        QMunicipio municipio = QMunicipio.municipio;
        QTerritorio territorio = QTerritorio.territorio;

        JPAQuery<Localidade> query = queryFactory.selectFrom(localidade)
            .join(localidade.distrito, distrito).fetchJoin()
            .join(distrito.municipio, municipio).fetchJoin()
            .join(municipio.territorio, territorio).fetchJoin();

        query.where(filtro.getPredicates(localidade, distrito, municipio,  territorio));

        if(nonNull(filtro.getRegiao())) {
            query.where(municipio.id.in(customAbrangenciaRegiaoRepository.getMunicipiosByRegioes(filtro.getRegiao())));
        }

        query.orderBy(localidade.nome.asc());

        if (Objects.isNull(filtro.getStart())) {
            filtro.setStart(0);
        }

        filtro.setLimit(10);

        query.offset(filtro.getStart()).limit(filtro.getLimit());

        List<Localidade> localidades = query.fetch();

        // Remover localidades duplicadas
        LinkedHashSet<Localidade> localidadeHashSet = Sets.newLinkedHashSet(localidades);
        localidades = Lists.newArrayList(localidadeHashSet);

        localidades.forEach(l -> {
            l.getDistrito().getMunicipio().setRegiao(customAbrangenciaRegiaoRepository.findRegiaoByMunicipio(l.getDistrito().getMunicipio().getId()));
        });

    	return localidades;
    }

    public Localidade findById(Long id) {
        QLocalidade localidade = QLocalidade.localidade;
        QDistrito distrito = QDistrito.distrito;
        QMunicipio municipio = QMunicipio.municipio;
        QTerritorio territorio = QTerritorio.territorio;

        Localidade fetchOne = queryFactory.selectFrom(localidade)
            .leftJoin(localidade.distrito, distrito).fetchJoin()
            .leftJoin(distrito.municipio, municipio).fetchJoin()
            .leftJoin(municipio.territorio, territorio).fetchJoin()
            .leftJoin(localidade.comunidade, QComunidade.comunidade).fetchJoin()
            .where(localidade.id.eq(id)).fetchOne();

        return fetchOne;
    }

    public List<Localidade> findAllByNomeIgnoreCaseContaining(String nome) {
        QLocalidade localidade = QLocalidade.localidade;
        QDistrito distrito = QDistrito.distrito;

        List<Localidade> localidades = queryFactory.selectFrom(localidade)
            .join(localidade.distrito, distrito).fetchJoin()
            .join(distrito.municipio).fetchJoin()
            .where(localidade.nome.containsIgnoreCase(nome))
            .orderBy(localidade.nome.asc()).fetch();

        return localidades;
    }
}
