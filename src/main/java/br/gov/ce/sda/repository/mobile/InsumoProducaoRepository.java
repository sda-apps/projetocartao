package br.gov.ce.sda.repository.mobile;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.mobile.InsumoProducao;

public interface InsumoProducaoRepository extends JpaRepository<InsumoProducao, Long> {
	
	public List<InsumoProducao> findAllByAtividadeProdutiva_Id(Long id);
	
}
