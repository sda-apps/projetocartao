package br.gov.ce.sda.repository;

import java.time.LocalDateTime;
import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.acesso.PersistentAuditEvent;

import java.util.List;

/**
 * Spring Data JPA repository for the PersistentAuditEvent entity.
 */
public interface PersistenceAuditEventRepository extends JpaRepository<PersistentAuditEvent, Long> {

    List<PersistentAuditEvent> findByPrincipal(String principal);

    List<PersistentAuditEvent> findByPrincipalAndAuditEventDateAfter(String principal, LocalDateTime after);

    List<PersistentAuditEvent> findAllByAuditEventDateBetweenOrderByAuditEventDateDescPrincipalDesc(LocalDateTime fromDate, LocalDateTime toDate);
}
