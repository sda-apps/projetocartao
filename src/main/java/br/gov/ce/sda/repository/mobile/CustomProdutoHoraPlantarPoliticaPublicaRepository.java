package br.gov.ce.sda.repository.mobile;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Repository;

import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

import br.gov.ce.sda.domain.mobile.ProdutoHoraPlantarPoliticaPublica;
import br.gov.ce.sda.domain.mobile.QProdutoHoraPlantarPoliticaPublica;
import br.gov.ce.sda.domain.programaseprojeto.QProdutoEspecificacao;

@Repository
public class CustomProdutoHoraPlantarPoliticaPublicaRepository {
	
	@Inject
	private JPAQueryFactory queryFactory;
	
	public List<ProdutoHoraPlantarPoliticaPublica> findByPoliticasPublicasId(Long id) {
		QProdutoHoraPlantarPoliticaPublica produtoHoraPlantarPoliticaPublica = QProdutoHoraPlantarPoliticaPublica.produtoHoraPlantarPoliticaPublica;
		
		JPAQuery<ProdutoHoraPlantarPoliticaPublica> query = queryFactory.selectFrom(produtoHoraPlantarPoliticaPublica)
				.join(produtoHoraPlantarPoliticaPublica.produtoEspecificacao, QProdutoEspecificacao.produtoEspecificacao).fetchJoin()
    			.where(produtoHoraPlantarPoliticaPublica.politicasPublicas.id.eq(id));
		
		return query.fetch();
	}

	
}