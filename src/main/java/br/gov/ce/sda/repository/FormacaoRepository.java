package br.gov.ce.sda.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.Cargo;
import br.gov.ce.sda.domain.Distrito;
import br.gov.ce.sda.domain.Empresa;
import br.gov.ce.sda.domain.Etnia;
import br.gov.ce.sda.domain.Formacao;

public interface FormacaoRepository extends JpaRepository<Formacao, Long> {
	
	List<Formacao> findAllByDescricaoIgnoreCaseContaining(String descricao);

}
