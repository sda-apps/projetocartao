package br.gov.ce.sda.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.Localidade;

public interface LocalidadeRepository extends JpaRepository<Localidade, Long> {

   List<Localidade> findAllByNomeIgnoreCaseContaining(String nome);

   List<Localidade> findAllByDistrito_Id(Long distritoId);

   List<Localidade> findAllByDistrito_Municipio_Id(Long municipio);

   List<Localidade> findAllByNomeIgnoreCaseContainingAndDistrito_Municipio_Id(String nomeLocalidade, Long municipio);

    List<Localidade> findAllByNomeIgnoreCaseContainingAndDistrito_Id(String nomeLocalidade, Long distritoId);

    List<Localidade> findAllByComunidade_Id(Long comunidadeId);

    List<Localidade> findAllByComunidade_IdIn(List<Long> comunidadeId);

}
