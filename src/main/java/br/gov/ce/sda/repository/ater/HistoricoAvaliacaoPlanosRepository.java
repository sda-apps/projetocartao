package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.ater.HistoricoAvaliacaoPlanos;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HistoricoAvaliacaoPlanosRepository extends JpaRepository<HistoricoAvaliacaoPlanos, Long> {

    List<HistoricoAvaliacaoPlanos> findAllByPlanoTrabalhoAnual_Id(Long id);

}
