package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.config.security.SecurityUtils;
import br.gov.ce.sda.domain.*;
import br.gov.ce.sda.domain.acesso.QUsuario;
import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.domain.ater.*;
import br.gov.ce.sda.domain.mapa.QCoordenada;
import br.gov.ce.sda.exceptions.StorageException;
import br.gov.ce.sda.repository.CustomAreaAtuacaoRepository;
import br.gov.ce.sda.repository.CustomUserRepository;
import br.gov.ce.sda.web.rest.dto.ater.FiltroTarefaDTO;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static br.gov.ce.sda.domain.mobile.QInformacaoInicial.informacaoInicial;
import static java.util.Objects.isNull;

@Repository
public class CustomTarefaRepository {

    private static final int DEFAULT_LIMIT_QUERY = 5;

    @Inject
    private JPAQueryFactory queryFactory;

    @Inject
    private CustomHistoricoAvaliacaoTarefaRepository historicoAvaliacaoTarefaRepository;

    @Inject
    private CustomUserRepository customUserRepository;

    @Inject
    private CustomPlanoTrabalhoAnualRepository customPlanoTrabalhoAnualRepository;

    @Inject
    private CustomAbrangenciaRegiaoRepository customAbrangenciaRegiaoRepository;

    @Inject
    private CustomAreaAtuacaoRepository customAreaAtuacaoRepository;

    public List<Tarefa> findByFilters(FiltroTarefaDTO filters) {
        JPAQuery<Tarefa> query = buildFilters(filters);

        return query.fetch();
    }

    public long findTotalByFilters(FiltroTarefaDTO filtro) {
        JPAQuery<Tarefa> query = buildFiltersTotal(filtro);

        return query.fetchCount();
    }

    private JPAQuery<Tarefa> buildFiltersTotal(FiltroTarefaDTO filters) {
        QTarefa tarefa = QTarefa.tarefa;
        QAgricultor agricultor = QAgricultor.agricultor;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
        QPessoa pessoa = QPessoa.pessoa;
        QUsuario usuario = QUsuario.usuario;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;

        JPAQuery<Tarefa> query = queryFactory.selectFrom(tarefa)
            .leftJoin(tarefa.usuario, usuario).fetchJoin()
            .leftJoin(usuario.usuarioAvancado, usuarioAvancado).fetchJoin()
            .leftJoin(usuarioAvancado.empresa, QEmpresa.empresa).fetchJoin()
            .leftJoin(tarefa.agricultor, agricultor).fetchJoin()
            .leftJoin(agricultor.pessoaFisica, pessoaFisica).fetchJoin()
            .leftJoin(pessoaFisica.pessoa, pessoa).fetchJoin()
            .leftJoin(pessoa.comunidade).fetchJoin();

        query.where(filters.getPredicates(tarefa));

        setRegrasDeNegocio(usuarioAvancado, pessoa, query);

        return query;
    }

    private JPAQuery<Tarefa> buildFilters(FiltroTarefaDTO filters) {
        QTarefa tarefa = QTarefa.tarefa;

        JPAQuery<Tarefa> query = buildQuery(tarefa);
        query.orderBy(tarefa.dataAteste.desc());
        query.where(filters.getPredicates(tarefa));

        if (isNull(filters.getStart())) {
            filters.setStart(0);
        }

        filters.setLimit(DEFAULT_LIMIT_QUERY);

        query.offset(filters.getStart()).limit(filters.getLimit());

        return query;
    }

    private JPAQuery<Tarefa> buildQuery(QTarefa tarefa) {
        QUsuario usuario = QUsuario.usuario;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
        QPessoaFisica pessoaFisicaAgricultor = QPessoaFisica.pessoaFisica;
        QAgricultor agricultor = QAgricultor.agricultor;
        QPlanoTrimestral planoTrimestral = QPlanoTrimestral.planoTrimestral;
        QEmpresa empresa = QEmpresa.empresa;
        QMunicipio qMunicipio = QMunicipio.municipio;
        QPessoa qPessoaAgricultor = QPessoa.pessoa;

        JPAQuery<Tarefa> query = queryFactory.selectFrom(tarefa)
            .leftJoin(tarefa.planoTrimestral, planoTrimestral).fetchJoin()
            .leftJoin(planoTrimestral.planoTrabalhoAnual, QPlanoTrabalhoAnual.planoTrabalhoAnual).fetchJoin()
            .leftJoin(tarefa.atividade, QAtividadePTA.atividadePTA).fetchJoin()
            .leftJoin(tarefa.municipio, qMunicipio).fetchJoin()
            .leftJoin(qMunicipio.uf, QUf.uf).fetchJoin()
            .leftJoin(tarefa.comunidade, QComunidade.comunidade).fetchJoin()
            .leftJoin(tarefa.agricultor, agricultor).fetchJoin()
            .leftJoin(agricultor.pessoaFisica, pessoaFisicaAgricultor).fetchJoin()
            .leftJoin(pessoaFisicaAgricultor.pessoa, qPessoaAgricultor).fetchJoin()
            .leftJoin(qPessoaAgricultor.comunidade).fetchJoin()
            .leftJoin(tarefa.usuario, usuario).fetchJoin()
            .leftJoin(usuario.usuarioAvancado, usuarioAvancado).fetchJoin()
            .leftJoin(usuarioAvancado.pessoaFisica, pessoaFisica).fetchJoin()
            .leftJoin(pessoaFisica.pessoa).fetchJoin()
            .leftJoin(usuarioAvancado.empresa, empresa).fetchJoin()
            .leftJoin(empresa.pessoaJuridica, QPessoaJuridica.pessoaJuridica).fetchJoin();

        setRegrasDeNegocio(usuarioAvancado, qPessoaAgricultor, query);

        return query;
    }

    public Tarefa findById(Long id) {
        QTarefa qTarefa = QTarefa.tarefa;
        QComunidadeParticipante qComunidadeParticipante = QComunidadeParticipante.comunidadeParticipante;

        JPAQuery<Tarefa> query = buildQuery(qTarefa)
            .leftJoin(qTarefa.coordenada, QCoordenada.coordenada).fetchJoin()
            .leftJoin(qTarefa.comunidadeParticipante, qComunidadeParticipante).fetchJoin()
            .leftJoin(qComunidadeParticipante.comunidade, QComunidade.comunidade).fetchJoin()
            //.leftJoin(qPessoaAgricultor.localidade).fetchJoin
            //.leftJoin(comunidade.localidades).fetchJoin()
            //.leftJoin(comunidade.municipio).fetchJoin()
            .where(qTarefa.id.eq(id));

        Tarefa tarefa = query.fetchOne();

        Set<HistoricoAvaliacaoTarefa> historico = new LinkedHashSet<>();
        historico.addAll(historicoAvaliacaoTarefaRepository.findByTarefaId(id));
        tarefa.setHistorico(historico);

        return tarefa;
    }

    public Tarefa findByIdComplemento(Long id) throws StorageException {
        QTarefa tarefa = QTarefa.tarefa;
        QListaPresencaTarefa listaPresencaTarefa = QListaPresencaTarefa.listaPresencaTarefa;
        QAgricultor qAgricultor = QAgricultor.agricultor;
        QPessoaFisica qPessoaFisica = QPessoaFisica.pessoaFisica;
        QTemaEspecificacao qTemaEspecificacao = QTemaEspecificacao.temaEspecificacao;
        QTema qTema = QTema.tema;
        QTarefaTema qTarefaTema = QTarefaTema.tarefaTema;
        QPessoa qPessoa = QPessoa.pessoa;

        JPAQuery<Tarefa> query = queryFactory.selectFrom(tarefa);
        query.leftJoin(tarefa.listaPresencaTarefa, listaPresencaTarefa).fetchJoin()
            .leftJoin(listaPresencaTarefa.agricultor, qAgricultor).fetchJoin()
            .leftJoin(qAgricultor.pessoaFisica, qPessoaFisica).fetchJoin()
            .leftJoin(qPessoaFisica.pessoa, qPessoa).fetchJoin()
            .leftJoin(qPessoa.comunidade).fetchJoin()
            .leftJoin(qPessoa.municipio).fetchJoin()
            .leftJoin(tarefa.tarefaTemas, qTarefaTema).fetchJoin()
            .leftJoin(qTarefaTema.tema, QTema.tema).fetchJoin()
            .leftJoin(qTarefaTema.temaEspecificacoes, qTemaEspecificacao).fetchJoin()
            .leftJoin(qTema.eixoTematico).fetchJoin()
            .where(tarefa.id.eq(id));

        return query.fetchOne();
    }

    @SuppressWarnings("Duplicates")
    private void setRegrasDeNegocio(QUsuarioAvancado qUsuarioAvancado, QPessoa qPessoaAgricultor, JPAQuery<Tarefa> query) {
        Usuario usuario = SecurityUtils.getCurrentUser();

        if (usuario.getAuthoritiesList().contains("ROLE_USUARIO_ATER")) {
            usuario = customUserRepository.buscarUsuarioPorId(usuario.getId());

            if (isUsuarioCoordenador(usuario)) {
                List<Long> tecnicosId = customPlanoTrabalhoAnualRepository.findAllTecnicosPTAPorCoordenador(usuario.getUsuarioAvancado().getId());

                query.where(qUsuarioAvancado.id.in(tecnicosId));
            } else if (isUsuarioAssessor(usuario)) {
                List<Long> tecnicosId = customPlanoTrabalhoAnualRepository.findAllTecnicosPTAPorAssessor(usuario.getUsuarioAvancado().getId());

                query.where(qUsuarioAvancado.id.in(tecnicosId));
            } else {
                List<Comunidade> comunidadesByUsuarioAvancadoId = customAreaAtuacaoRepository.findComunidadesByUsuarioAvancadoId(usuario.getUsuarioAvancado().getId());

                Set<Long> comunidadesIds = comunidadesByUsuarioAvancadoId.stream().map(Comunidade::getId).collect(Collectors.toSet());

                query.where(qPessoaAgricultor.comunidade.id.in(comunidadesIds));
            }

            query.where(qUsuarioAvancado.empresa.id.in(usuario.getUsuarioAvancado().getEmpresa().getId()));
        }

        if (usuario.getAuthoritiesList().contains("ROLE_RESPONSAVEL_ATER")) {
            usuario = customUserRepository.buscarUsuarioPorId(usuario.getId());

            query.where(qUsuarioAvancado.empresa.id.in(usuario.getUsuarioAvancado().getEmpresa().getId()));
        }

        if (usuario.getAuthoritiesList().contains("ROLE_USUARIO_ERP")) {
            usuario = customUserRepository.buscarUsuarioCompletoPorId(usuario.getId());

            Regiao regiao = customAbrangenciaRegiaoRepository.findRegiaoByMunicipio(usuario.getUsuarioAvancado().getPessoaFisica().getPessoa().getMunicipio().getId());

            List<Long> municipiosByRegiao = customAbrangenciaRegiaoRepository.getMunicipiosByRegiao(regiao.getId());

            query.where(qPessoaAgricultor.municipio.id.in(municipiosByRegiao));
            query.where(qUsuarioAvancado.empresa.id.in(usuario.getUsuarioAvancado().getEmpresa().getId()));
        }
    }

    private boolean isUsuarioAssessor(Usuario usuario) {
        PlanoTrabalhoAnual pta = customPlanoTrabalhoAnualRepository.findPlanoTrabalhoAnualPorAssessorId(usuario.getUsuarioAvancado().getId());

        return Objects.nonNull(pta);
    }

    private boolean isUsuarioCoordenador(Usuario usuario) {
        PlanoTrabalhoAnual pta = customPlanoTrabalhoAnualRepository.findPlanoTrabalhoAnualPorCoordenadorId(usuario.getUsuarioAvancado().getId());

        return Objects.nonNull(pta);
    }
}


