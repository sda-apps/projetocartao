package br.gov.ce.sda.repository;

import br.gov.ce.sda.domain.UsuarioAvancado;
import br.gov.ce.sda.domain.acesso.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Spring Data JPA repository for the User entity.
 */
public interface UserRepository extends JpaRepository<Usuario, Long> {

	Optional<Usuario> findOneByChaveAtivacao(String chaveAtivacao);

    Optional<Usuario> findOneByChaveReativacao(String chaveReativacao);

    Optional<Usuario> findOneByLogin(String login);

    Optional<Usuario> findOneById(Long userId);

    Optional<Usuario> findOneByUsuarioAvancado(UsuarioAvancado usuarioAvancado);

    @Override
    void delete(Usuario t);

}
