package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.ater.Comunidade;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ComunidadeRepository extends JpaRepository<Comunidade, Long> {

    List<Comunidade> findAllByNomeIgnoreCaseContainingOrderByNome(String nome);

    List<Comunidade> findAllByMunicipio_IdInOrderByNome(List<Long> municipioId);

    List<Comunidade> findAllByNomeIgnoreCaseContainingAndMunicipio_IdIn(String nome,List<Long> municipio);

    Comunidade findOneById(Long id);

    List<Comunidade> findByIdIn(List<Long> collect);

}
