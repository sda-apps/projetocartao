package br.gov.ce.sda.repository;

import javax.inject.Inject;

import br.gov.ce.sda.domain.*;
import org.springframework.stereotype.Repository;

import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Repository
public class CustomPessoaRepository {

    @Inject
    private JPAQueryFactory queryFactory;

    public PessoaFisica buscarPessoaPorCpf(String cpf) {
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
        QPessoa pessoa = QPessoa.pessoa;

        JPAQuery<PessoaFisica> query = queryFactory.selectFrom(pessoaFisica)
            .join(pessoaFisica.pessoa, pessoa).fetchJoin()
            .where(pessoaFisica.cpf.eq(cpf));

        return query.fetchOne();
    }

    public Pessoa buscarPessoaPorId(Long id) {
        QPessoa pessoa = QPessoa.pessoa;

        JPAQuery<Pessoa> query = queryFactory.selectFrom(pessoa)
            .leftJoin(pessoa.uf).fetchJoin()
            .leftJoin(pessoa.municipio).fetchJoin()
            .leftJoin(pessoa.distrito).fetchJoin()
            .leftJoin(pessoa.comunidade).fetchJoin()
            .leftJoin(pessoa.telefones).fetchJoin()
            .where(pessoa.id.eq(id));

        return query.fetchOne();
    }

}
