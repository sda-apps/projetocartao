package br.gov.ce.sda.repository;

import java.util.List;

import javax.inject.Inject;

import br.gov.ce.sda.domain.*;
import org.springframework.stereotype.Repository;

import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

import br.gov.ce.sda.domain.mapa.Elemento;
import br.gov.ce.sda.domain.mapa.QCoordenada;
import br.gov.ce.sda.domain.mapa.QElemento;
import br.gov.ce.sda.web.rest.dto.FiltroAgricultorDTO;

@Repository
public class CustomElementoRepository {

	@Inject
	private JPAQueryFactory queryFactory;

	@Inject
	private CustomAgricultorRepository customAgricultorRepository;

    @Inject
    private TipoATERRepository tipoATERRepository;

	public List<Elemento> buscarElementosCoordenadasPorFiltro(FiltroAgricultorDTO filtro) {
		QElemento elemento = QElemento.elemento;
        QAgricultor agricultor = QAgricultor.agricultor;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
        QPessoa pessoa = QPessoa.pessoa;
        QMunicipio municipio = QMunicipio.municipio;
        QTerritorio territorio = QTerritorio.territorio;

		JPAQuery<Elemento> query = queryFactory.selectFrom(elemento)
				.join(elemento.agricultor, agricultor)
                .join(agricultor.pessoaFisica, pessoaFisica)
                .join(pessoaFisica.pessoa, pessoa)
				.join(elemento.coordenada, QCoordenada.coordenada).fetchJoin();
                //.leftJoin(pessoa.municipio, municipio).fetchJoin()
                //.leftJoin(municipio.territorio, territorio).fetchJoin();

        query.where(filtro.getPredicates(agricultor));
        query.where(filtro.getPredicates(pessoa,territorio));

		filtro.setSubquery(query, elemento.agricultor, tipoATERRepository);

		return query.fetch();
	}

	public Elemento buscarElementoPorIdComAgricultor(Long elementoId) {
		QElemento elemento = QElemento.elemento;

		JPAQuery<Elemento> query = queryFactory.selectFrom(elemento)
				.where(elemento.id.eq(elementoId));

		query.join(QElemento.elemento.agricultor, QAgricultor.agricultor).fetchJoin();

		Elemento e = query.fetchOne();

		e.setAgricultor(customAgricultorRepository.buscarPorId(e.getAgricultor().getId()));

		return e;
	}

	public Elemento buscarCisternaPorAgricultor(Long agricultorId) {
		QElemento elemento = QElemento.elemento;

		JPAQuery<Elemento> query = queryFactory.selectFrom(elemento)
				.where(elemento.agricultor.id.eq(agricultorId)
						.and(elemento.tipoElemento.tipo.eq('C')
								.or(elemento.tipoElemento.tipo.eq('I'))
								.or(elemento.tipoElemento.tipo.eq('P'))))
				.join(elemento.coordenada, QCoordenada.coordenada).fetchJoin();

		return query.fetchFirst();
	}

}
