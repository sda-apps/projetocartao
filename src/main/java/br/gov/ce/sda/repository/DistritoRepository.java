package br.gov.ce.sda.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.gov.ce.sda.domain.Distrito;
import br.gov.ce.sda.domain.Municipio;

/**
 * Spring Data JPA repository for the User entity.
 */

public interface DistritoRepository extends JpaRepository<Distrito, Long> {
	
	public final static String FIND_BY_DISTRITO_QUERY = "SELECT * FROM distrito d " + 
														"JOIN municipio m on d.municipio = m.municipio_id " +
														"WHERE m.municipio_id in (?2) "+
														"AND d.nome like %?1%";

   List<Distrito> findAllByNomeIgnoreCaseContaining(String nome);

   @Query(value = FIND_BY_DISTRITO_QUERY, nativeQuery = true)
   public List<Distrito> findByDistrito(String distrito, List<Long> longMunicipio);
}
