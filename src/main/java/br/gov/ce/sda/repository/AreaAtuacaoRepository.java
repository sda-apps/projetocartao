package br.gov.ce.sda.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.mobile.AreaAtuacao;

public interface AreaAtuacaoRepository extends JpaRepository<AreaAtuacao, Long> {
	
	List<AreaAtuacao> findAllByUsuarioAvancadoId(Long id);

}
