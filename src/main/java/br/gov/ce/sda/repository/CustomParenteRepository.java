package br.gov.ce.sda.repository;

import javax.inject.Inject;

import br.gov.ce.sda.domain.*;
import br.gov.ce.sda.domain.mobile.*;
import org.springframework.stereotype.Repository;

import com.querydsl.jpa.impl.JPAQueryFactory;

import java.util.List;

@Repository
public class CustomParenteRepository {

	@Inject
	private JPAQueryFactory queryFactory;

	public Parente findByCpfPessoa(String cpf) {
		QParente qParente = QParente.parente;
        QAgricultor agricultor = QAgricultor.agricultor;
		QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
		QPessoa pessoa = QPessoa.pessoa;

		Parente parente = queryFactory.selectFrom(qParente)
                .join(qParente.agricultor, agricultor).fetchJoin()
				.join(agricultor.pessoaFisica, pessoaFisica).fetchJoin()
				.join(pessoaFisica.pessoa, pessoa).fetchJoin()
				.where(pessoaFisica.cpf.eq(cpf)).fetchOne();

		return parente;
	}

    public Parente findByCpfPessoaAndParentesco(String cpf, String parentesco) {
        QParente qParente = QParente.parente;
        QAgricultor agricultor = QAgricultor.agricultor;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
        QPessoa pessoa = QPessoa.pessoa;

        Parente parente = queryFactory.selectFrom(qParente)
            .join(qParente.agricultor, agricultor).fetchJoin()
            .join(agricultor.pessoaFisica, pessoaFisica).fetchJoin()
            .join(pessoaFisica.pessoa, pessoa).fetchJoin()
            .where(pessoaFisica.cpf.eq(cpf)
                .and(qParente.parentesco.eq(parentesco)))
            .orderBy(qParente.dataInicioRelacao.desc()).fetchFirst();

        return parente;
    }

	public Parente findConjugeByFamiliaId(Long familiaId) {
		QParente qParente = QParente.parente;
        QAgricultor agricultor = QAgricultor.agricultor;
		QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
		QPessoa pessoa = QPessoa.pessoa;

		Parente parente = queryFactory.selectFrom(qParente)
            .join(qParente.agricultor, agricultor).fetchJoin()
            .join(agricultor.pessoaFisica, pessoaFisica).fetchJoin()
            .join(pessoaFisica.pessoa, pessoa).fetchJoin()
            .where(qParente.parentesco.eq(Parente.Parentesco.CONJUJE.getStatus())
                .and(qParente.dataFimRelacao.isNull())
                .and(qParente.familia.id.eq(familiaId))).fetchFirst();

		return parente;
	}

    public ParenteMobile findByIdMobile(Long id) {
        QParenteMobile qParente = QParenteMobile.parenteMobile;
        QAgricultorMobile agricultor = QAgricultorMobile.agricultorMobile;
        QPessoaFisicaMobile pessoaFisica = QPessoaFisicaMobile.pessoaFisicaMobile;
        QPessoaMobile pessoa = QPessoaMobile.pessoaMobile;

        ParenteMobile parente = queryFactory.selectFrom(qParente)
            .join(qParente.agricultor, agricultor).fetchJoin()
            .join(agricultor.pessoaFisica, pessoaFisica).fetchJoin()
            .join(pessoaFisica.pessoa, pessoa).fetchJoin()
            .where(qParente.id.eq(id)).fetchOne();

        return parente;
    }
}
