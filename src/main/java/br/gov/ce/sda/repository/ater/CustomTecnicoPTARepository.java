package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.QMunicipio;
import br.gov.ce.sda.domain.QPessoa;
import br.gov.ce.sda.domain.QPessoaFisica;
import br.gov.ce.sda.domain.QUsuarioAvancado;
import br.gov.ce.sda.domain.ater.*;
import br.gov.ce.sda.domain.mobile.QAreaAtuacao;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Repository
public class CustomTecnicoPTARepository {

    @Inject
    private JPAQueryFactory queryFactory;

    public Set<TecnicoPTA> findByPlanoAnual(Long planoAnualId) {
        QTecnicoPTA tecnicoPTA = QTecnicoPTA.tecnicoPTA;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
        QAreaAtuacao areaAtuacao = QAreaAtuacao.areaAtuacao;
        QPessoa qPessoa = QPessoa.pessoa;
        QPlanoTrabalhoAnual qPlanoTrabalhoAnual = QPlanoTrabalhoAnual.planoTrabalhoAnual;

        JPAQuery<TecnicoPTA> tecnicoPTAJPAQuery = queryFactory.selectFrom(tecnicoPTA)
            .join(tecnicoPTA.tecnico, usuarioAvancado).fetchJoin()
            .join(usuarioAvancado.pessoaFisica, pessoaFisica).fetchJoin()
            .join(pessoaFisica.pessoa, qPessoa).fetchJoin()
            .leftJoin(usuarioAvancado.areasAtuacao, areaAtuacao).fetchJoin()
            .leftJoin(areaAtuacao.municipio, QMunicipio.municipio).fetchJoin()
            .leftJoin(areaAtuacao.comunidade, QComunidade.comunidade).fetchJoin()
            .leftJoin(areaAtuacao.planoTrabalhoAnual, qPlanoTrabalhoAnual).fetchJoin()
            .where(tecnicoPTA.planoTrabalhoAnual.id.eq(planoAnualId)
                .and(areaAtuacao.planoTrabalhoAnual.id.eq(planoAnualId)));

        tecnicoPTAJPAQuery.orderBy(qPessoa.nome.asc());

        List<TecnicoPTA> list = findAssessorByPTA(planoAnualId);

        list.addAll(tecnicoPTAJPAQuery.fetch());

        Set<TecnicoPTA> tecnicos = new LinkedHashSet<>();
        tecnicos.addAll(list);

        return tecnicos;
    }

    public TecnicoPTA findByTecnicoIdAndPlanoTrabalhoAnualId(Long tecnicoId, Long planoTrabalhoAnualId) {
        QTecnicoPTA tecnicoPTA = QTecnicoPTA.tecnicoPTA;

        JPAQuery<TecnicoPTA> tecnicoPTAJPAQuery = queryFactory.selectFrom(tecnicoPTA)
            .where(tecnicoPTA.planoTrabalhoAnual.id.eq(planoTrabalhoAnualId)
                .and(tecnicoPTA.tecnico.id.eq(tecnicoId))
                .and(tecnicoPTA.planoTrabalhoAnual.situacao.eq(PlanoTrabalhoAnual.Situacao.ATIVO.getId())));

        return tecnicoPTAJPAQuery.fetchFirst();
    }

    public List<TecnicoPTA> findAssessorByPTA(Long planoAnual) {
        QTecnicoPTA tecnicoPTA = QTecnicoPTA.tecnicoPTA;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
        QPessoa qPessoa = QPessoa.pessoa;

        List<TecnicoPTA> tecnicos = queryFactory.selectFrom(tecnicoPTA)
            .join(tecnicoPTA.tecnico, usuarioAvancado).fetchJoin()
            .join(usuarioAvancado.pessoaFisica, pessoaFisica).fetchJoin()
            .join(pessoaFisica.pessoa, qPessoa).fetchJoin()
            .where(tecnicoPTA.planoTrabalhoAnual.id.eq(planoAnual)
                .and(tecnicoPTA.perfil.notEqualsIgnoreCase(TecnicoPTA.Perfil.TECNICO_CAMPO.getPerfil())))
            .orderBy(qPessoa.nome.asc()).fetch();

        return tecnicos;
    }

    public List<TecnicoPTA> findTecnicoByPTA(Long planoAnual) {
        QTecnicoPTA tecnicoPTA = QTecnicoPTA.tecnicoPTA;

        List<TecnicoPTA> tecnicos = queryFactory.selectFrom(tecnicoPTA)
            .where(tecnicoPTA.planoTrabalhoAnual.id.eq(planoAnual).and(tecnicoPTA.perfil.eq(TecnicoPTA.Perfil.TECNICO_CAMPO.getPerfil()))).fetch();

        return tecnicos;
    }


}
