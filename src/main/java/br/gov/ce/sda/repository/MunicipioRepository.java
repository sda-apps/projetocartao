package br.gov.ce.sda.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.gov.ce.sda.domain.Municipio;

/**
 * Spring Data JPA repository for the User entity.
 */
public interface MunicipioRepository extends JpaRepository<Municipio, Long> {

	public final static String FIND_BY_TERRITORIO_QUERY = "SELECT * FROM municipio m " +
        												"JOIN territorio t on m.territorio = t.territorio_id " +
        												"WHERE t.territorio_id in (?2) "+
        												"AND m.nome like %?1%";

   List<Municipio> findAllByNomeIgnoreCaseContainingAndTerritorioIsNotNull(String nome);

   List<Municipio> findAllByNomeIgnoreCaseContainingAndUf_IdOrderByNomeAsc(String nome, Long uf);

   List<Municipio> findAllByUf_IdOrderByNomeAsc(Long uf);

   @Query(value = FIND_BY_TERRITORIO_QUERY, nativeQuery = true)
   public List<Municipio> findByMunicipio(String municipio, List<Long> longTerritorios);


    List<Municipio> findByIdIn(List<Long> collect);
}
