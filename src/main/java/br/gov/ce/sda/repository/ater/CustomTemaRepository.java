package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.QPessoaFisica;
import br.gov.ce.sda.domain.QUsuarioAvancado;
import br.gov.ce.sda.domain.acesso.QUsuario;
import br.gov.ce.sda.domain.ater.QTema;
import br.gov.ce.sda.domain.ater.Tema;
import br.gov.ce.sda.web.rest.dto.FiltroTemaDTO;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.List;
import java.util.Objects;

@Repository
public class CustomTemaRepository {

    @Inject
    private JPAQueryFactory queryFactory;

    public List<Tema> findAllByFilter(FiltroTemaDTO filtro) {
        QTema tema = QTema.tema;
        QUsuario usuario = QUsuario.usuario;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
        JPAQuery<Tema> query = queryFactory.selectFrom(tema)
            .leftJoin(tema.usuario, usuario).fetchJoin()
            .leftJoin(usuario.usuarioAvancado, usuarioAvancado).fetchJoin()
            .leftJoin(usuarioAvancado.pessoaFisica, pessoaFisica).fetchJoin()
            .leftJoin(pessoaFisica.pessoa).fetchJoin()
            .leftJoin(tema.eixoTematico).fetchJoin()
            .leftJoin(tema.especificacoes).fetchJoin();

        query.where(filtro.getPredicates(tema));
        query.orderBy(tema.nome.asc());

        if (Objects.isNull(filtro.getStart())) {
            filtro.setStart(0);
        }

        filtro.setLimit(10);

        query.offset(filtro.getStart()).limit(filtro.getLimit());

        List<Tema> temas = query.fetch();

        return temas;
    }

}
