package br.gov.ce.sda.repository;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Repository;

import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

import br.gov.ce.sda.domain.QEmpresa;
import br.gov.ce.sda.domain.QPessoa;
import br.gov.ce.sda.domain.QPessoaFisica;
import br.gov.ce.sda.domain.QUnidadeMedida;
import br.gov.ce.sda.domain.QUsuarioAvancado;
import br.gov.ce.sda.domain.ater.AtendimentoATER;
import br.gov.ce.sda.domain.ater.AtendimentoATERIndicador;
import br.gov.ce.sda.domain.ater.QAtendimentoATER;
import br.gov.ce.sda.domain.ater.QAtendimentoATERIndicador;
import br.gov.ce.sda.domain.ater.QAtendimentoAssociacao;
import br.gov.ce.sda.domain.ater.QAtividade;
import br.gov.ce.sda.domain.ater.QAtividadeEspecificacao;
import br.gov.ce.sda.domain.ater.QIndicador;
import br.gov.ce.sda.domain.ater.QLinhaAcao;
import br.gov.ce.sda.domain.programaseprojeto.QPrograma;
import br.gov.ce.sda.web.rest.dto.AtendimentoAssociacaoDTO;
import br.gov.ce.sda.web.rest.dto.AtendimentoDTO;

@Repository
public class CustomATERRepository {

	@Inject
	private JPAQueryFactory queryFactory;

	public List<AtendimentoAssociacaoDTO> buscarTecnicosPorAgricultor(Long agricultorId) {
		QAtendimentoAssociacao atendimentoAssociacao = QAtendimentoAssociacao.atendimentoAssociacao;

    	JPAQuery<Tuple> query = queryFactory.select(atendimentoAssociacao.ano.max(),
    				atendimentoAssociacao.ano.min(),
    				atendimentoAssociacao.tecnico.id,
    				atendimentoAssociacao.empresa.pessoaJuridica.pessoa.nome,
    				atendimentoAssociacao.tecnico.pessoaFisica.pessoa.nome,
    				atendimentoAssociacao.tecnico.pessoaFisica.pessoa.foto,
    				atendimentoAssociacao.tecnico.id,
    				atendimentoAssociacao.tipoAter.nome)
    			.from(atendimentoAssociacao)
    			.where(atendimentoAssociacao.agricultor.id.eq(agricultorId))
    			.groupBy(atendimentoAssociacao.tecnico.id,
    					atendimentoAssociacao.agricultor.id,
    					atendimentoAssociacao.empresa.pessoaJuridica.pessoa.nome,
    					atendimentoAssociacao.tecnico.pessoaFisica.pessoa.nome,
    					atendimentoAssociacao.tecnico.pessoaFisica.pessoa.foto,
    					atendimentoAssociacao.tecnico.id,
    					atendimentoAssociacao.tipoAter.nome)
    			.orderBy(atendimentoAssociacao.ano.max().desc(), atendimentoAssociacao.ano.min().desc());

    	List<Tuple> result = query.fetch();

    	List<AtendimentoAssociacaoDTO> tecnicos = new ArrayList<AtendimentoAssociacaoDTO>();

    	for (Tuple row : result) {
    		AtendimentoAssociacaoDTO tecnico = new AtendimentoAssociacaoDTO(
    				row.get(atendimentoAssociacao.tecnico.id),
    				row.get(atendimentoAssociacao.tecnico.pessoaFisica.pessoa.nome),
    				row.get(atendimentoAssociacao.ano.min()),
    				row.get(atendimentoAssociacao.ano.max()),
    				row.get(atendimentoAssociacao.empresa.pessoaJuridica.pessoa.nome),
    				row.get(atendimentoAssociacao.tecnico.pessoaFisica.pessoa.foto),
    				row.get(atendimentoAssociacao.tipoAter.nome)
    		);

    		tecnicos.add(tecnico);

    	}

    	return tecnicos;
	}

	public List<AtendimentoATER> buscarAtendimentoPorAgricultor(AtendimentoDTO filtro) {
		QAtendimentoATER atendimentoATER = QAtendimentoATER.atendimentoATER;
		QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
		QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;

		List<AtendimentoATER> atendimentos = queryFactory.selectFrom(atendimentoATER)
				.join(atendimentoATER.atividadeEspecifica, QAtividadeEspecificacao.atividadeEspecificacao).fetchJoin()
				.join(atendimentoATER.atividadeGeral, QAtividade.atividade).fetchJoin()
				.join(atendimentoATER.programa, QPrograma.programa).fetchJoin()
				.join(atendimentoATER.linhaAcao, QLinhaAcao.linhaAcao).fetchJoin()
				.join(atendimentoATER.tecnico, usuarioAvancado).fetchJoin()
				.leftJoin(usuarioAvancado.empresa, QEmpresa.empresa).fetchJoin()
				.join(usuarioAvancado.pessoaFisica, pessoaFisica).fetchJoin()
				.join(pessoaFisica.pessoa, QPessoa.pessoa).fetchJoin()
				.where(filtro.getPredicates(atendimentoATER))
				.orderBy(atendimentoATER.dataAtendimento.desc()).fetch();

		return atendimentos;
	}

	public List<AtendimentoATERIndicador> buscarIndicadorPorAtendimento(Long atendimentoId) {
		QAtendimentoATERIndicador atendimentoATERIndicador = QAtendimentoATERIndicador.atendimentoATERIndicador;

		List<AtendimentoATERIndicador> indicadores = queryFactory.selectFrom(atendimentoATERIndicador)
				.join(atendimentoATERIndicador.indicador, QIndicador.indicador).fetchJoin()
				.join(atendimentoATERIndicador.unidade, QUnidadeMedida.unidadeMedida).fetchJoin()
				.where(atendimentoATERIndicador.atendimentoAter.id.eq(atendimentoId)).fetch();

		return indicadores;
	}


}
