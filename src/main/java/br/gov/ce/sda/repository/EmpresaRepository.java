package br.gov.ce.sda.repository;

import br.gov.ce.sda.domain.Empresa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmpresaRepository extends JpaRepository<Empresa, Long> {

    List<Empresa> findAllByIsProjetoPauloFreireIsTrue();
}
