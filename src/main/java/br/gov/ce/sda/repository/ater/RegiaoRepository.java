package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.ater.Regiao;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;

public interface RegiaoRepository extends JpaRepository<Regiao, Long> {

    List<Regiao> findAllByNomeIgnoreCaseContainingOrderByNome(String nome);

    List<Regiao> findAllByInativoIsFalseOrderByNome();

    List<Regiao> findByInativoIsFalseAndIdIn(List<Long> collect);
}
