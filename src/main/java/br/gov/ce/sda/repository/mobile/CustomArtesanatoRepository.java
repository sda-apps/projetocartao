package br.gov.ce.sda.repository.mobile;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.springframework.stereotype.Repository;

import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

import br.gov.ce.sda.domain.mobile.Artesanato;
import br.gov.ce.sda.domain.mobile.QArtesanato;
import br.gov.ce.sda.domain.mobile.QFormaComercializacao;

@Repository
public class CustomArtesanatoRepository {

	@Inject
	private JPAQueryFactory queryFactory;

	public List<Artesanato> findByAtividadeProdutivaId(Long id) {
		QArtesanato artesanato = QArtesanato.artesanato;

		JPAQuery<Artesanato> query = queryFactory.selectFrom(artesanato)
				.leftJoin(artesanato.formasComercializacoes).fetchJoin()
                .leftJoin(artesanato.produtoEspecificacao).fetchJoin()
    			.where(artesanato.atividadeProdutiva.id.eq(id));

        List<Artesanato> artesanatos = query.fetch();
        Set<Artesanato> artesanatoSet = new HashSet<>();
        artesanatoSet.addAll(artesanatos);
        artesanatos.clear();
        artesanatos.addAll(artesanatoSet);

        return artesanatos;
	}

}
