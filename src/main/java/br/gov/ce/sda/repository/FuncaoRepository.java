package br.gov.ce.sda.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.Funcao;

public interface FuncaoRepository extends JpaRepository<Funcao, Long> {

    List<Funcao> findAllByDescricaoIgnoreCaseContainingOrderByDescricaoAsc(String funcao);

}
