package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.ater.EixoTematico;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EixoTematicoRepository extends JpaRepository<EixoTematico, Long> {

    EixoTematico findOneById(Long id);

    List<EixoTematico> findAllByNomeIgnoreCaseContaining(String nome);
}
