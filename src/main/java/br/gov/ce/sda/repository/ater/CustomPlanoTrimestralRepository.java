package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.QMunicipio;
import br.gov.ce.sda.domain.QUf;
import br.gov.ce.sda.domain.ater.*;
import br.gov.ce.sda.web.rest.dto.ater.filtros.FiltroPlanoTrimestralDTO;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Repository
public class CustomPlanoTrimestralRepository {

    private static final int DEFAULT_LIMIT_QUERY = 10;

    private final JPAQueryFactory queryFactory;

    public CustomPlanoTrimestralRepository(JPAQueryFactory queryFactory) {
        this.queryFactory = queryFactory;
    }

    public List<MetaAtividadesTrimestral> findById(Long planoTrimestralId) {
        QMetaAtividadesTrimestral metaAtividadesTrimestral = QMetaAtividadesTrimestral.metaAtividadesTrimestral;
        QMetaAtividadesPTA metaAtividadesPTA = QMetaAtividadesPTA.metaAtividadesPTA;
        QMunicipio municipio = QMunicipio.municipio;
        QPlanoTrimestral qPlanoTrimestral = QPlanoTrimestral.planoTrimestral;

        return queryFactory.selectFrom(metaAtividadesTrimestral)
            .join(metaAtividadesTrimestral.municipio, municipio).fetchJoin()
            .join(municipio.uf, QUf.uf).fetchJoin()
            .leftJoin(metaAtividadesTrimestral.comunidade, QComunidade.comunidade).fetchJoin()
            .join(metaAtividadesTrimestral.metaAtividadesPTA, metaAtividadesPTA).fetchJoin()
            .join(metaAtividadesPTA.atividade, QAtividadePTA.atividadePTA).fetchJoin()
            .join(metaAtividadesTrimestral.planoTrimestral, qPlanoTrimestral).fetchJoin()
            .join(qPlanoTrimestral.planoTrabalhoAnual).fetchJoin()
            .where(qPlanoTrimestral.id.eq(planoTrimestralId)).fetch();
    }

    private void setStartAndLimit(FiltroPlanoTrimestralDTO filter) {
        if (isNull(filter.getStart())) {
            filter.setStart(0);
        }

        if(nonNull(filter.getLimit())) {
            filter.setLimit(filter.getLimit());
        } else {
            filter.setLimit(DEFAULT_LIMIT_QUERY);
        }
    }

    public List<ViewPlanosTrimestraisERP> findAllForErpUser(FiltroPlanoTrimestralDTO filter) {
        QViewPlanosTrimestraisERP qViewPlanosTrimestraisERP = QViewPlanosTrimestraisERP.viewPlanosTrimestraisERP;
        setStartAndLimit(filter);
        JPAQuery<ViewPlanosTrimestraisERP> query = queryFactory.selectFrom(qViewPlanosTrimestraisERP);
        query.where(filter.getPredicatesPlanoTrimestralERP(qViewPlanosTrimestraisERP));
        query.offset(filter.getStart()).limit(filter.getLimit());

        return query.fetch();
    }

    public List<ViewPlanosTrimestraisUGP> findAllForUgpUser(FiltroPlanoTrimestralDTO filter) {
        QViewPlanosTrimestraisUGP qViewPlanosTrimestraisUGP = QViewPlanosTrimestraisUGP.viewPlanosTrimestraisUGP;
        setStartAndLimit(filter);
        JPAQuery<ViewPlanosTrimestraisUGP> query = queryFactory.selectFrom(qViewPlanosTrimestraisUGP);
        query.where(filter.getPredicatesPlanoTrimestralUGP(qViewPlanosTrimestraisUGP));
        query.offset(filter.getStart()).limit(filter.getLimit());

        return query.fetch();
    }

}
