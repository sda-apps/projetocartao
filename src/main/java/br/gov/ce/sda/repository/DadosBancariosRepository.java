package br.gov.ce.sda.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.DadosBancarios;

public interface DadosBancariosRepository extends JpaRepository<DadosBancarios, Long> {

    Optional<List<DadosBancarios>> findByPessoa_id(Long id);

}
