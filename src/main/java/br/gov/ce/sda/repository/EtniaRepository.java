package br.gov.ce.sda.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.Etnia;

public interface EtniaRepository extends JpaRepository<Etnia, Long> {
	
	public List <Etnia> findAllByOrderByNomeAsc();

}
