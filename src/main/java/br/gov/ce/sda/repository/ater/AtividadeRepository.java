package br.gov.ce.sda.repository.ater;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.ater.AtividadePTA;

import java.util.List;

public interface AtividadeRepository extends JpaRepository<AtividadePTA, Long> {
    AtividadePTA findOneById(Long id);

    List<AtividadePTA> findAllByInativoIsFalseOrInativoIsNull();

    List<AtividadePTA> findAllByNomeIgnoreCaseContainingAndInativoIsFalse(String nome);

}
