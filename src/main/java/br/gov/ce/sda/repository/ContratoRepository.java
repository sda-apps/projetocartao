package br.gov.ce.sda.repository;

import br.gov.ce.sda.domain.ater.Contrato;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ContratoRepository extends JpaRepository<Contrato, Long>{

	List<Contrato> findAllByCodigoIgnoreCaseContainingOrObjetoIgnoreCaseContainingOrTituloIgnoreCaseContaining(String codigo, String objeto, String titulo);

}
