package br.gov.ce.sda.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.programaseprojeto.Projeto;

public interface ProjetoRepository extends JpaRepository<Projeto, Long> {

   List<Projeto> findAllByNomeIgnoreCaseContaining(String nome);

}
