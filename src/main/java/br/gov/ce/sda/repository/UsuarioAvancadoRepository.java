package br.gov.ce.sda.repository;

import br.gov.ce.sda.domain.UsuarioAvancado;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioAvancadoRepository extends JpaRepository<UsuarioAvancado, Long> {

}
