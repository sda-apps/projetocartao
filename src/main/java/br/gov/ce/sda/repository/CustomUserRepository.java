package br.gov.ce.sda.repository;

import br.gov.ce.sda.config.security.SecurityUtils;
import br.gov.ce.sda.domain.*;
import br.gov.ce.sda.domain.acesso.QAutoridade;
import br.gov.ce.sda.domain.acesso.QUsuario;
import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.domain.ater.PlanoTrabalhoAnual;
import br.gov.ce.sda.domain.ater.Regiao;
import br.gov.ce.sda.repository.ater.CustomAbrangenciaRegiaoRepository;
import br.gov.ce.sda.repository.ater.CustomPlanoTrabalhoAnualRepository;
import br.gov.ce.sda.web.rest.dto.FiltroUsuarioDTO;
import br.gov.ce.sda.web.rest.dto.UsuarioDTO;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class CustomUserRepository {

    private static final int DEFAULT_LIMIT_QUERY = 10;

    @Inject
    private JPAQueryFactory queryFactory;

    @Inject
    private CustomAbrangenciaRegiaoRepository customAbrangenciaRegiaoRepository;

    @Inject
    private CustomPlanoTrabalhoAnualRepository customPlanoTrabalhoAnualRepository;

    public Usuario buscarUsuarioPorId(Long id) {
        QUsuario usuairo = QUsuario.usuario;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;

        Usuario usuario = queryFactory.selectFrom(usuairo)
            .join(usuairo.usuarioAvancado, usuarioAvancado).fetchJoin()
            .join(usuarioAvancado.empresa, QEmpresa.empresa).fetchJoin()
            .where(usuairo.id.eq(id))
            .fetchOne();

        return usuario;
    }

    public Usuario buscarUsuarioCompletoPorId(Long id) {
        QUsuario usuario = QUsuario.usuario;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
        QPessoa pessoa = QPessoa.pessoa;
        QEmpresa empresa = QEmpresa.empresa;
        QPessoaJuridica pessoaJuridica = QPessoaJuridica.pessoaJuridica;

        Usuario result = queryFactory.selectFrom(usuario)
            .join(usuario.usuarioAvancado, usuarioAvancado).fetchJoin()
            .leftJoin(usuarioAvancado.formacao).fetchJoin()
            .leftJoin(usuarioAvancado.cargo).fetchJoin()
            .leftJoin(usuarioAvancado.empresa, empresa).fetchJoin()
            .leftJoin(usuarioAvancado.cargo).fetchJoin()
            .leftJoin(usuarioAvancado.formacao).fetchJoin()
            .leftJoin(empresa.pessoaJuridica, pessoaJuridica).fetchJoin()
            .leftJoin(pessoaJuridica.pessoa).fetchJoin()

            .join(usuarioAvancado.pessoaFisica, pessoaFisica).fetchJoin()
            .leftJoin(pessoaFisica.nacionalidade).fetchJoin()
            .leftJoin(pessoaFisica.naturalidade).fetchJoin()
            .leftJoin(pessoaFisica.escolaridade).fetchJoin()
            .leftJoin(pessoaFisica.estadoCivil).fetchJoin()
            .leftJoin(pessoaFisica.localExpedicaoRg).fetchJoin()
            .leftJoin(pessoaFisica.pessoa, pessoa).fetchJoin()
            .leftJoin(pessoa.municipio).fetchJoin()
            .leftJoin(pessoa.uf).fetchJoin()
            .leftJoin(pessoa.telefones).fetchJoin()

            .where(usuario.id.eq(id)).fetchOne();

        return result;
    }

    public Usuario buscarUsuarioPorEmailInstitucional(String emailInstitucional) {
        QUsuario usuairo = QUsuario.usuario;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;

        Usuario usuario = queryFactory.selectFrom(usuairo)
            .join(usuairo.usuarioAvancado, usuarioAvancado).fetchJoin()
            .where(usuarioAvancado.emailInstitucional.eq(emailInstitucional))
            .fetchFirst();

        return usuario;
    }

    public Usuario buscarUsuarioPorCpf(String cpf) {
        QUsuario usuairo = QUsuario.usuario;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
        QPessoa pessoa = QPessoa.pessoa;

        Usuario usuario = queryFactory.selectFrom(usuairo)
            .join(usuairo.usuarioAvancado, usuarioAvancado).fetchJoin()
            .join(usuarioAvancado.pessoaFisica, pessoaFisica).fetchJoin()
            .join(pessoaFisica.pessoa, pessoa).fetchJoin()
            .where(pessoaFisica.cpf.eq(cpf))
            .fetchFirst();

        return usuario;
    }


    public String bucarEmailUsuario(Long id) {
        QUsuario usuario = QUsuario.usuario;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
        QPessoa pessoa = QPessoa.pessoa;

        String email = queryFactory.select(usuarioAvancado.emailInstitucional).from(usuario)
            .join(usuario.usuarioAvancado, usuarioAvancado)
            .join(usuarioAvancado.pessoaFisica, pessoaFisica)
            .join(pessoaFisica.pessoa, pessoa)
            .where(usuario.id.eq(id))
            .fetchOne();

        return email;
    }

    @Transactional(readOnly = true)
    public List<Usuario> buscarTodosUsuarios(FiltroUsuarioDTO filtro) {
        QUsuario usuario = QUsuario.usuario;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
        QAutoridade autoridade = QAutoridade.autoridade;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
        QPessoa pessoa = QPessoa.pessoa;

        JPAQuery<Usuario> query = queryFactory.selectFrom(usuario)
            .join(usuario.usuarioAvancado, usuarioAvancado).fetchJoin()
            .leftJoin(usuarioAvancado.pessoaFisica, pessoaFisica).fetchJoin()
            .leftJoin(pessoaFisica.pessoa, pessoa).fetchJoin()
            .leftJoin(usuario.autoridades, autoridade).fetchJoin()
            .where(filtro.getPredicates(pessoa));

        if (Objects.isNull(filtro.getStart())) {
            filtro.setStart(0);
        }

        if (Objects.isNull(filtro.getLimit())) {
            filtro.setLimit(10);
        }

        query.offset(filtro.getStart()).limit(filtro.getLimit());

        if (filtro.getOrdenar() != null && filtro.getOrdenar()) {
            PathBuilder builder = new PathBuilder<>(usuario.getType(), usuario.getMetadata());

            if (filtro.getOrdemAsc()) {
                query.orderBy(builder.getString(filtro.getOrdenarPor()).asc());
            } else {
                query.orderBy(builder.getString(filtro.getOrdenarPor()).desc());
            }
        }

        List<Usuario> usuarios = query.fetch();

        return usuarios;
    }

    public List<Usuario> buscarPorNome(String nome, Usuario usuario, Boolean isSuperUser) {
        QUsuario qUsuario = QUsuario.usuario;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
        QPessoa pessoa = QPessoa.pessoa;

        JPAQuery<Usuario> query = queryFactory.selectFrom(qUsuario)
            .join(qUsuario.usuarioAvancado, usuarioAvancado).fetchJoin()
            .join(usuarioAvancado.pessoaFisica, pessoaFisica).fetchJoin()
            .join(pessoaFisica.pessoa, pessoa).fetchJoin()
            .where(pessoa.nome.containsIgnoreCase(nome)
                .and(qUsuario.tipo.eq(Usuario.USUARIO_TIPO_ATER)));

        if (!isSuperUser) {
            query.join(usuarioAvancado.empresa, QEmpresa.empresa).fetchJoin();

            query.where(usuarioAvancado.empresa.id.eq(usuario.getUsuarioAvancado().getEmpresa().getId()));
        }

        return query.fetch();
    }

    public List<UsuarioDTO> buscarUsuariosEmpresaPorFiltros(FiltroUsuarioDTO filters, Usuario usuario, Boolean isSuperUser) {
        QUsuario qUsuario = QUsuario.usuario;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
        QPessoa pessoa = QPessoa.pessoa;
        QEmpresa empresa = QEmpresa.empresa;
        QPessoaJuridica pessoaJuridica = QPessoaJuridica.pessoaJuridica;
        JPAQuery<Usuario> query = queryFactory.selectFrom(qUsuario)
            .join(qUsuario.usuarioAvancado, usuarioAvancado).fetchJoin()
            .leftJoin(usuarioAvancado.cargo, QCargo.cargo).fetchJoin()
            .leftJoin(usuarioAvancado.empresa, empresa).fetchJoin()
            .leftJoin(empresa.pessoaJuridica, pessoaJuridica).fetchJoin()
            .leftJoin(pessoaJuridica.pessoa, QPessoa.pessoa).fetchJoin()
            .join(usuarioAvancado.pessoaFisica, pessoaFisica).fetchJoin()
            .join(pessoaFisica.pessoa, pessoa).fetchJoin()
            .leftJoin(usuarioAvancado.empresa).fetchJoin()
            .where(filters.getPredicates(qUsuario)
                .and(qUsuario.tipo.eq(Usuario.USUARIO_TIPO_ATER))
                .and(filters.getPredicates(usuarioAvancado)
                    .and(filters.getPredicates(pessoa))));

        setRegrasDeNegocio(empresa, pessoa, query, usuarioAvancado);

        if (Objects.isNull(filters.getStart())) {
            filters.setStart(0);
        }

        if (Objects.isNull(filters.getLimit())) {
            filters.setLimit(DEFAULT_LIMIT_QUERY);
        }

        query.orderBy(pessoa.nome.asc());

        query.offset(filters.getStart()).limit(filters.getLimit());

        return query.fetch().stream().map(user -> new UsuarioDTO(user)).collect(Collectors.toList());
    }

    @SuppressWarnings("Duplicates")
    private void setRegrasDeNegocio(QEmpresa empresa, QPessoa pessoa, JPAQuery<Usuario> query, QUsuarioAvancado usuarioAvancado) {
        Usuario usuario = SecurityUtils.getCurrentUser();

        usuario = buscarUsuarioPorId(usuario.getId());

        if (usuario.getAuthoritiesList().contains("ROLE_USUARIO_ATER")) {
            if (isUsuarioCoordenador(usuario)) {
                List<Long> tecnicosId = customPlanoTrabalhoAnualRepository.findAllTecnicosPTAPorCoordenador(usuario.getUsuarioAvancado().getId());

                query.where(usuarioAvancado.id.in(tecnicosId));
            } else if (isUsuarioAssessor(usuario)) {
                List<Long> tecnicosId = customPlanoTrabalhoAnualRepository.findAllTecnicosPTAPorAssessor(usuario.getUsuarioAvancado().getId());

                query.where(usuarioAvancado.id.in(tecnicosId));
            }
        }

        if (usuario.getAuthoritiesList().contains("ROLE_RESPONSAVEL_ATER")) {
            query.where(empresa.id.eq(usuario.getUsuarioAvancado().getEmpresa().getId()));
        }

        if (usuario.getAuthoritiesList().contains("ROLE_USUARIO_ERP")) {
            Regiao regiao = customAbrangenciaRegiaoRepository.findRegiaoByMunicipio(usuario.getUsuarioAvancado().getPessoaFisica().getPessoa().getMunicipio().getId());

            List<Long> municipiosByRegiao = customAbrangenciaRegiaoRepository.getMunicipiosByRegiao(regiao.getId());

            query.where(pessoa.municipio.id.in(municipiosByRegiao));
        }
    }

    private boolean isUsuarioCoordenador(Usuario usuario) {
        PlanoTrabalhoAnual pta = customPlanoTrabalhoAnualRepository.findPlanoTrabalhoAnualPorCoordenadorId(usuario.getUsuarioAvancado().getId());

        return Objects.nonNull(pta);
    }

    private boolean isUsuarioAssessor(Usuario usuario) {
        PlanoTrabalhoAnual pta = customPlanoTrabalhoAnualRepository.findPlanoTrabalhoAnualPorAssessorId(usuario.getUsuarioAvancado().getId());

        return Objects.nonNull(pta);
    }
}
