package br.gov.ce.sda.repository;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Repository;

import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

import br.gov.ce.sda.domain.Distrito;
import br.gov.ce.sda.domain.Municipio;
import br.gov.ce.sda.domain.QDistrito;
import br.gov.ce.sda.domain.QMunicipio;
import br.gov.ce.sda.domain.QTerritorio;

@Repository
public class CustomDistritoRepository {

	@Inject
	private JPAQueryFactory queryFactory;

	public List<Distrito> buscarPorMunicipio(String distritoNome, List<Long> longMunicipio) {
		QDistrito distrito = QDistrito.distrito;
		QMunicipio municipio = QMunicipio.municipio;
		JPAQuery<Distrito> query = queryFactory.selectFrom(distrito)
				.leftJoin(distrito.municipio, municipio).fetchJoin()
				.where(distrito.nome.contains(distritoNome));

		if(longMunicipio != null){
			query.where(municipio.id.in(longMunicipio));
		}



		return query.fetch();
	}

}
