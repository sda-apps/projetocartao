package br.gov.ce.sda.repository.mobile;

import br.gov.ce.sda.config.security.SecurityUtils;
import br.gov.ce.sda.domain.*;
import br.gov.ce.sda.domain.acesso.QUsuario;
import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.domain.ater.*;
import br.gov.ce.sda.domain.mapa.QCoordenada;
import br.gov.ce.sda.domain.mobile.*;
import br.gov.ce.sda.domain.mobile.QTema;
import br.gov.ce.sda.domain.programaseprojeto.QProdutoEspecificacao;
import br.gov.ce.sda.repository.CustomAreaAtuacaoRepository;
import br.gov.ce.sda.repository.CustomUserRepository;
import br.gov.ce.sda.repository.ater.CustomAbrangenciaRegiaoRepository;
import br.gov.ce.sda.repository.ater.CustomPlanoTrabalhoAnualRepository;
import br.gov.ce.sda.web.rest.dto.AtividadeCadastroEmpresaDTO;
import br.gov.ce.sda.web.rest.dto.mobile.FiltroCadastroDTO;
import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Repository
public class CustomAtividadeCadastroRepository {

    private static final int DEFAULT_LIMIT_QUERY = 10;

    @Inject
    private JPAQueryFactory queryFactory;

    @Inject
    private CustomParenteMobileRepository customParenteMobileRepository;

    @Inject
    private CustomUserRepository customUserRepository;

    @Inject
    private CustomPlanoTrabalhoAnualRepository customPlanoTrabalhoAnualRepository;

    @Inject
    private CustomAbrangenciaRegiaoRepository customAbrangenciaRegiaoRepository;

    @Inject
    private CustomAreaAtuacaoRepository customAreaAtuacaoRepository;

    public List<AtividadeCadastro> findByFilters(FiltroCadastroDTO filters) {
        JPAQuery<AtividadeCadastro> query = buildFilters(filters);

        query.offset(filters.getStart()).limit(filters.getLimit());

        return query.fetch();
    }

    private JPAQuery<AtividadeCadastro> buildFilters(FiltroCadastroDTO filters) {
        QAtividadeCadastro atividadeCadastro = QAtividadeCadastro.atividadeCadastro;
        QInformacaoInicial informacaoInicial = QInformacaoInicial.informacaoInicial;
        QAgricultorMobile agricultor = QAgricultorMobile.agricultorMobile;
        QCoordenadaMobile coordenadaMobile = QCoordenadaMobile.coordenadaMobile;
        QPessoaMobile pessoaAgricultorMobile = QPessoaMobile.pessoaMobile;
        QPessoaFisica pessoaFisicaUA = QPessoaFisica.pessoaFisica;
        QPessoaFisicaMobile pessoaFisicaMobile = QPessoaFisicaMobile.pessoaFisicaMobile;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
        QPessoa pessoaUA = QPessoa.pessoa;

        JPAQuery<AtividadeCadastro> query = queryFactory.selectFrom(atividadeCadastro)
            .join(atividadeCadastro.tecnico, usuarioAvancado).fetchJoin()
            .join(usuarioAvancado.pessoaFisica, pessoaFisicaUA).fetchJoin()
            .join(pessoaFisicaUA.pessoa, pessoaUA).fetchJoin()
            .join(atividadeCadastro.informacaoInicial, informacaoInicial).fetchJoin()
            .join(informacaoInicial.agricultor, agricultor).fetchJoin()
            .leftJoin(agricultor.coordenada, coordenadaMobile).fetchJoin()
            .join(agricultor.pessoaFisica, pessoaFisicaMobile).fetchJoin()
            .join(pessoaFisicaMobile.pessoa, pessoaAgricultorMobile).fetchJoin()
            .leftJoin(pessoaAgricultorMobile.municipio, QMunicipio.municipio).fetchJoin()
            .leftJoin(pessoaAgricultorMobile.distrito, QDistrito.distrito).fetchJoin()
            .leftJoin(pessoaAgricultorMobile.localidade, QLocalidade.localidade).fetchJoin()
            .leftJoin(pessoaAgricultorMobile.comunidade, QComunidade.comunidade).fetchJoin();

        query.where(filters.getPredicatesCadastro(atividadeCadastro));
        query.where(filters.getPredicatesPessoa(pessoaAgricultorMobile));
        setRegrasDeNegocio(atividadeCadastro, informacaoInicial, pessoaAgricultorMobile, query);

        query.orderBy(atividadeCadastro.data.desc());

        if (isNull(filters.getStart())) {
            filters.setStart(0);
        }

        filters.setLimit(DEFAULT_LIMIT_QUERY);

        return query;
    }

    @SuppressWarnings("Duplicates")
    private void setRegrasDeNegocio(QAtividadeCadastro atividadeCadastro, QInformacaoInicial informacaoInicial, QPessoaMobile pessoaAgricultorMobile, JPAQuery<AtividadeCadastro> query) {
        Usuario usuario = SecurityUtils.getCurrentUser();

        if (usuario.getAuthoritiesList().contains("ROLE_USUARIO_ATER")) {
            usuario = customUserRepository.buscarUsuarioPorId(usuario.getId());

            if (isUsuarioCoordenador(usuario)) {
                List<Long> tecnicosId = customPlanoTrabalhoAnualRepository.findAllTecnicosPTAPorCoordenador(usuario.getUsuarioAvancado().getId());

                query.where(atividadeCadastro.tecnico.id.in(tecnicosId));
            } else if(isUsuarioAssessor(usuario)) {
                List<Long> tecnicosId = customPlanoTrabalhoAnualRepository.findAllTecnicosPTAPorAssessor(usuario.getUsuarioAvancado().getId());

                query.where(atividadeCadastro.tecnico.id.in(tecnicosId));
            } else {
                List<Comunidade> comunidadesByUsuarioAvancadoId = customAreaAtuacaoRepository.findComunidadesByUsuarioAvancadoId(usuario.getUsuarioAvancado().getId());

                Set<Long> comunidadesIds = comunidadesByUsuarioAvancadoId.stream().map(Comunidade::getId).collect(Collectors.toSet());

                query.where(pessoaAgricultorMobile.comunidade.id.in(comunidadesIds));
            }
        }

        if (usuario.getAuthoritiesList().contains("ROLE_RESPONSAVEL_ATER")) {
            usuario = customUserRepository.buscarUsuarioPorId(usuario.getId());

            query.where(informacaoInicial.parceiraAter.id.in(usuario.getUsuarioAvancado().getEmpresa().getId()));
        }

        if (usuario.getAuthoritiesList().contains("ROLE_USUARIO_ERP")) {
            usuario = customUserRepository.buscarUsuarioCompletoPorId(usuario.getId());

            Regiao regiao = customAbrangenciaRegiaoRepository.findRegiaoByMunicipio(usuario.getUsuarioAvancado().getPessoaFisica().getPessoa().getMunicipio().getId());

            List<Long> municipiosByRegiao = customAbrangenciaRegiaoRepository.getMunicipiosByRegiao(regiao.getId());

            query.where(pessoaAgricultorMobile.municipio.id.in(municipiosByRegiao));
        }

    }

    private boolean isUsuarioAssessor(Usuario usuario) {
        PlanoTrabalhoAnual pta = customPlanoTrabalhoAnualRepository.findPlanoTrabalhoAnualPorAssessorId(usuario.getUsuarioAvancado().getId());

        return Objects.nonNull(pta);
    }

    private boolean isUsuarioCoordenador(Usuario usuario) {
        PlanoTrabalhoAnual pta = customPlanoTrabalhoAnualRepository.findPlanoTrabalhoAnualPorCoordenadorId(usuario.getUsuarioAvancado().getId());

        return Objects.nonNull(pta);
    }

    @Transactional(readOnly = true)
    public AtividadeCadastro buscarPorId(Long atividadeCadastroId) {
        QAtividadeCadastro qAtividadeCadastro = QAtividadeCadastro.atividadeCadastro;

        QInformacaoInicial informacaoInicial = QInformacaoInicial.informacaoInicial;
        QAgricultorMobile agricultorMobile = QAgricultorMobile.agricultorMobile;
        QPessoaFisicaMobile pessoaFisicaMobile = QPessoaFisicaMobile.pessoaFisicaMobile;
        QPessoaFisica pessoaFisicaUsuarioAvancado = QPessoaFisica.pessoaFisica;
        QPessoaJuridica pessoaJuridica = QPessoaJuridica.pessoaJuridica;
        QPessoaMobile pessoa = QPessoaMobile.pessoaMobile;
        QPessoa pessoaUsuarioAvancado = QPessoa.pessoa;
        QMunicipio municipio = QMunicipio.municipio;
        QDistrito distrito = QDistrito.distrito;
        QCoordenada coordenada = QCoordenada.coordenada;
        QCoordenadaMobile coordenadaMobile = QCoordenadaMobile.coordenadaMobile;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
        QEmpresa empresa = QEmpresa.empresa;
        QFamiliaMobile familiaMobile = QFamiliaMobile.familiaMobile;

        JPAQuery<AtividadeCadastro> query = queryFactory.selectFrom(qAtividadeCadastro)
            .join(qAtividadeCadastro.informacaoInicial, informacaoInicial).fetchJoin()
            .join(informacaoInicial.agricultor, agricultorMobile).fetchJoin()
            .join(agricultorMobile.pessoaFisica, pessoaFisicaMobile).fetchJoin()
            .join(pessoaFisicaMobile.pessoa, pessoa).fetchJoin()
            .join(qAtividadeCadastro.tecnico, usuarioAvancado).fetchJoin()
            .join(usuarioAvancado.pessoaFisica, pessoaFisicaUsuarioAvancado).fetchJoin()
            .join(pessoaFisicaUsuarioAvancado.pessoa, pessoaUsuarioAvancado).fetchJoin()
            .leftJoin(informacaoInicial.parceiraAter, empresa).fetchJoin()
            .leftJoin(empresa.pessoaJuridica, pessoaJuridica).fetchJoin()
            .leftJoin(pessoaJuridica.pessoa).fetchJoin()
            .leftJoin(pessoaFisicaMobile.estadoCivil).fetchJoin()
            .leftJoin(pessoaFisicaMobile.escolaridade).fetchJoin()
            .leftJoin(pessoaFisicaMobile.localExpedicaoRg).fetchJoin()
            .leftJoin(pessoaFisicaMobile.nacionalidade).fetchJoin()
            .leftJoin(pessoaFisicaMobile.naturalidade).fetchJoin()
            .leftJoin(pessoa.municipio, municipio).fetchJoin()
            .leftJoin(pessoa.comunidade).fetchJoin()
            .leftJoin(pessoa.distrito, distrito).fetchJoin()
            .leftJoin(distrito.coordenada, coordenada).fetchJoin()
            .leftJoin(municipio.uf, QUf.uf).fetchJoin()
            .leftJoin(municipio.coordenada, coordenada).fetchJoin()
            .leftJoin(municipio.territorio, QTerritorio.territorio).fetchJoin()
            .leftJoin(municipio.uf, QUf.uf).fetchJoin()
            .leftJoin(agricultorMobile.beneficiosSociais).fetchJoin()
            .leftJoin(agricultorMobile.coordenada, coordenadaMobile).fetchJoin()
            .leftJoin(agricultorMobile.familia, familiaMobile).fetchJoin()
            .where(qAtividadeCadastro.id.eq(atividadeCadastroId));

        return query.fetchOne();
    }

    @Transactional(readOnly = true)
    public void carregarInformacoesIniciais(AtividadeCadastro atividadeCadastro) {
        InformacaoInicial infoInicial = atividadeCadastro.getInformacaoInicial();

        AgricultorMobile agricultor = infoInicial.getAgricultor();
        if (infoInicial != null && agricultor != null) {

            if (agricultor.getPessoaFisica().getPessoa().getTelefones() != null) {
                agricultor.getPessoaFisica().getPessoa().getTelefones().size();
            }

            if (agricultor.getFamilia() != null) {
                agricultor.getFamilia().getParentes().size();

                agricultor.getFamilia().getParentes().stream().forEach(
                    parente -> customParenteMobileRepository.findPessoaByParenteId(parente.getId())
                );
            }

            agricultor.getDaps().size();
        }
    }

    @Transactional(readOnly = true)
    public AtividadeCadastro buscarPorIdResumo(Long id) {
        QAtividadeCadastro qAtividadeCadastro = QAtividadeCadastro.atividadeCadastro;
        QInformacaoInicial informacaoInicial = QInformacaoInicial.informacaoInicial;
        QProducao producao = QProducao.producao;
        QAcessoPoliticasPublicas acessoPoliticasPublicas = QAcessoPoliticasPublicas.acessoPoliticasPublicas;
        QCapacitacao capacitacao = QCapacitacao.capacitacao;

        QAgricultorMobile agricultorMobile = QAgricultorMobile.agricultorMobile;
        QPessoaFisicaMobile pessoaFisicaMobile = QPessoaFisicaMobile.pessoaFisicaMobile;
        QPessoaMobile pessoa = QPessoaMobile.pessoaMobile;

        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
        QPessoaFisica pessoaFisicaUsuarioAvancado = QPessoaFisica.pessoaFisica;
        QPessoa pessoaUsuarioAvancado = QPessoa.pessoa;

        QMunicipio municipio = QMunicipio.municipio;
        QDistrito distrito = QDistrito.distrito;
        QCoordenada coordenada = QCoordenada.coordenada;
        QCoordenadaMobile coordenadaMobile = QCoordenadaMobile.coordenadaMobile;
        QMunicipio naturalidade = QMunicipio.municipio;

        QHistoricoAvaliacao historicoAvaliacao = QHistoricoAvaliacao.historicoAvaliacao;
        QUsuario usuario = QUsuario.usuario;
        QUsuarioAvancado usuarioAvancadoHistorico = QUsuarioAvancado.usuarioAvancado;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;

        JPAQuery<AtividadeCadastro> query = queryFactory.selectFrom(qAtividadeCadastro)
            .join(qAtividadeCadastro.informacaoInicial, informacaoInicial).fetchJoin()
            .leftJoin(qAtividadeCadastro.acessoPoliticasPublicas, acessoPoliticasPublicas).fetchJoin()
            .leftJoin(qAtividadeCadastro.producao, producao).fetchJoin()
            .leftJoin(qAtividadeCadastro.capacitacao, capacitacao).fetchJoin()
            .join(informacaoInicial.agricultor, agricultorMobile).fetchJoin()
            .leftJoin(agricultorMobile.associacaoComunitaria, QAssociacaoComunitaria.associacaoComunitaria).fetchJoin()
            .join(agricultorMobile.pessoaFisica, pessoaFisicaMobile).fetchJoin()
            .leftJoin(pessoaFisicaMobile.nacionalidade, QPais.pais).fetchJoin()
            .leftJoin(pessoaFisicaMobile.estadoCivil, QEstadoCivil.estadoCivil).fetchJoin()
            .join(pessoaFisicaMobile.pessoa, pessoa).fetchJoin()
            .leftJoin(pessoaFisicaMobile.escolaridade).fetchJoin()
            .leftJoin(pessoaFisicaMobile.localExpedicaoRg).fetchJoin()
            .leftJoin(pessoaFisicaMobile.naturalidade, naturalidade).fetchJoin()
            .leftJoin(naturalidade.uf).fetchJoin()
            .join(qAtividadeCadastro.tecnico, usuarioAvancado).fetchJoin()
            .join(usuarioAvancado.pessoaFisica, pessoaFisicaUsuarioAvancado).fetchJoin()
            .join(pessoaFisicaUsuarioAvancado.pessoa, pessoaUsuarioAvancado).fetchJoin()
            .leftJoin(pessoa.municipio, municipio).fetchJoin()
            .leftJoin(pessoa.comunidade).fetchJoin()
            .leftJoin(pessoa.distrito, distrito).fetchJoin()
            .leftJoin(pessoa.localidade, QLocalidade.localidade).fetchJoin()
            .leftJoin(distrito.coordenada, coordenada).fetchJoin()
            .leftJoin(municipio.uf, QUf.uf).fetchJoin()
            .leftJoin(municipio.coordenada, coordenada).fetchJoin()
            .leftJoin(municipio.territorio, QTerritorio.territorio).fetchJoin()
            .leftJoin(municipio.uf, QUf.uf).fetchJoin()
            .leftJoin(agricultorMobile.coordenada, coordenadaMobile).fetchJoin()
            .leftJoin(agricultorMobile.beneficiosSociais, QBeneficioSocial.beneficioSocial).fetchJoin()
            .leftJoin(qAtividadeCadastro.historico, historicoAvaliacao).fetchJoin()
            .leftJoin(historicoAvaliacao.usuario, usuario).fetchJoin()
            .leftJoin(usuario.usuarioAvancado, usuarioAvancadoHistorico).fetchJoin()
            .leftJoin(usuarioAvancadoHistorico.pessoaFisica, pessoaFisica).fetchJoin()
            .leftJoin(pessoaFisica.pessoa, QPessoa.pessoa).fetchJoin()
            .where(qAtividadeCadastro.id.eq(id));

        AtividadeCadastro atividadeCadastro = query.fetchOne();

        if (atividadeCadastro != null) {
            carregarInformacoesIniciais(atividadeCadastro);
        }

        return atividadeCadastro;
    }

    public AcessoPoliticasPublicas buscarAcessoPoliticasPublicasPorId(Long id) {
        QAcessoPoliticasPublicas qAcessoPoliticasPublicas = QAcessoPoliticasPublicas.acessoPoliticasPublicas;
        QPoliticasPublicas politicasPublicas = QPoliticasPublicas.politicasPublicas;
        QHabitacaoSeneamentoEnergia habitacaoSeneamentoEnergia = QHabitacaoSeneamentoEnergia.habitacaoSeneamentoEnergia;
        QProdutoHoraPlantarPoliticaPublica produtoHoraPlantar = QProdutoHoraPlantarPoliticaPublica.produtoHoraPlantarPoliticaPublica;
        QProdutoPaaPoliticaPublica produtoPaa = QProdutoPaaPoliticaPublica.produtoPaaPoliticaPublica;
        QProdutoPnaePoliticaPublica produtoPnae = QProdutoPnaePoliticaPublica.produtoPnaePoliticaPublica;

        JPAQuery<AcessoPoliticasPublicas> query = queryFactory.selectFrom(qAcessoPoliticasPublicas)
            .leftJoin(qAcessoPoliticasPublicas.politicasPublicas, politicasPublicas).fetchJoin()
            .leftJoin(politicasPublicas.produtosPnae, produtoPnae).fetchJoin()
            .leftJoin(produtoPnae.produtoEspecificacao).fetchJoin()
            .leftJoin(politicasPublicas.produtosPaa, produtoPaa).fetchJoin()
            .leftJoin(produtoPaa.produtoEspecificacao).fetchJoin()
            .leftJoin(politicasPublicas.produtosHoraPlantar, produtoHoraPlantar).fetchJoin()
            .leftJoin(produtoHoraPlantar.produtoEspecificacao).fetchJoin()
            .leftJoin(qAcessoPoliticasPublicas.habitacaoSeneamentoEnergia, habitacaoSeneamentoEnergia).fetchJoin()
            .where(qAcessoPoliticasPublicas.id.eq(id));

        return query.fetchOne();
    }

    public Producao buscarProducaoPorId(Long id) {
        QProducao producao = QProducao.producao;
        QAtividadeProdutiva qAtividadeProdutiva = QAtividadeProdutiva.atividadeProdutiva;
        QSubsistemasProducao subsistemasProducao = QSubsistemasProducao.subsistemasProducao;
        QProdutoSubsistemas produtoSubsistemas = QProdutoSubsistemas.produtoSubsistemas;
        QProdutoEspecificacao tipoProduto = QProdutoEspecificacao.produtoEspecificacao;
        QProdutoBeneficio produtoBeneficio = QProdutoBeneficio.produtoBeneficio;
        QArtesanato artesanato = QArtesanato.artesanato;

        JPAQuery<Producao> query = queryFactory.selectFrom(producao)
            .leftJoin(producao.atividadeProdutiva, qAtividadeProdutiva).fetchJoin()
            .leftJoin(qAtividadeProdutiva.subsistemasProducao, subsistemasProducao).fetchJoin()
            .leftJoin(qAtividadeProdutiva.insumosProducao, QInsumoProducao.insumoProducao).fetchJoin()
            .leftJoin(qAtividadeProdutiva.combatePragas, QCombatePragas.combatePragas).fetchJoin()
            .leftJoin(qAtividadeProdutiva.reservaAlimentarRebalho, QReservaAlimentarRebalho.reservaAlimentarRebalho).fetchJoin()
            .leftJoin(qAtividadeProdutiva.atividadesNaoAgricolas, QAtividadeNaoAgricola.atividadeNaoAgricola).fetchJoin()
            .leftJoin(qAtividadeProdutiva.artesanatos, artesanato).fetchJoin()
            .leftJoin(qAtividadeProdutiva.atividadesAmbientais, QAtividadeAmbiental.atividadeAmbiental).fetchJoin()

            .leftJoin(producao.produtosBeneficio, produtoBeneficio).fetchJoin()
            .leftJoin(produtoBeneficio.produto, QProdutoEspecificacao.produtoEspecificacao).fetchJoin()
            .leftJoin(produtoBeneficio.certificacao, QCertificacao.certificacao).fetchJoin()
            .leftJoin(produtoBeneficio.locaisComercializacao, QLocalComercializacao.localComercializacao).fetchJoin()
            .leftJoin(subsistemasProducao.produtosSubsistemas, produtoSubsistemas).fetchJoin()
            .leftJoin(subsistemasProducao.tiposIrrigacao).fetchJoin()
            .leftJoin(produtoSubsistemas.tipoProducao, tipoProduto).fetchJoin()
            .leftJoin(tipoProduto.produto).fetchJoin()
            .leftJoin(produtoSubsistemas.unidadeProducao, QUnidadeMedida.unidadeMedida).fetchJoin()
            .leftJoin(produtoSubsistemas.certificacao, QCertificacao.certificacao).fetchJoin()

            .where(producao.id.eq(id));

        return query.fetchOne();
    }

    public Capacitacao buscarCapacitacaoPorId(Long id) {
        QCapacitacao capacitacao = QCapacitacao.capacitacao;
        QProgramaCapacitacao programaCapacitacao = QProgramaCapacitacao.programaCapacitacao;
        QOrganizacaoCapacitacao organizacaoCapacitacao = QOrganizacaoCapacitacao.organizacaoCapacitacao;

        JPAQuery<Capacitacao> query = queryFactory.selectFrom(capacitacao)
            .leftJoin(capacitacao.programa, programaCapacitacao).fetchJoin()
            .leftJoin(programaCapacitacao.temas, QTema.tema).fetchJoin()
            .leftJoin(programaCapacitacao.orgaos, QOrgaoCapacitacao.orgaoCapacitacao).fetchJoin()
            .leftJoin(capacitacao.organizacao, organizacaoCapacitacao).fetchJoin()
            .leftJoin(organizacaoCapacitacao.organizacaoLocal, QOrganizacaoLocal.organizacaoLocal).fetchJoin()
            .where(capacitacao.id.eq(id));

        return query.fetchOne();
    }

    public List<AtividadeCadastroEmpresaDTO> countAtividadeCadastroPorEmpresa() {
        Usuario usuario = SecurityUtils.getCurrentUser();

        QAtividadeCadastro atividadeCadastro = QAtividadeCadastro.atividadeCadastro;
        QUsuarioAvancado qTecnico = QUsuarioAvancado.usuarioAvancado;
        QEmpresa emrpesa = QEmpresa.empresa;
        QPessoaJuridica pessoaJuridica = QPessoaJuridica.pessoaJuridica;

        JPAQuery<Tuple> query = queryFactory.select(pessoaJuridica.nomeFantasia, atividadeCadastro.id.count())
            .from(atividadeCadastro)
            .leftJoin(atividadeCadastro.tecnico, qTecnico)
            .leftJoin(qTecnico.empresa, emrpesa)
            .leftJoin(emrpesa.pessoaJuridica, pessoaJuridica)
            .groupBy(pessoaJuridica.nomeFantasia);

        if (usuario.getAuthoritiesList().contains("ROLE_RESPONSAVEL_ATER") || usuario.getAuthoritiesList().contains("ROLE_USUARIO_ATER")) {
            usuario = customUserRepository.buscarUsuarioPorId(usuario.getId());

            query.where(atividadeCadastro.informacaoInicial.parceiraAter.id.in(usuario.getUsuarioAvancado().getEmpresa().getId()));
        }
        query.orderBy(pessoaJuridica.nomeFantasia.asc());
        List<Tuple> result = query.fetch();

        List<AtividadeCadastroEmpresaDTO> entidades = new ArrayList<>();

        for (Tuple row : result) {
            AtividadeCadastroEmpresaDTO entidade = new AtividadeCadastroEmpresaDTO(
                row.get(pessoaJuridica.nomeFantasia),
                row.get(atividadeCadastro.id.count())
            );

            entidades.add(entidade);
        }

        return entidades;
    }

}

