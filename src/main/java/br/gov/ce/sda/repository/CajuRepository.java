package br.gov.ce.sda.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.programaseprojeto.Caju;

public interface CajuRepository extends JpaRepository <Caju, Long>{
	public List <Caju> findByAgricultor_IdOrderByAno(Long id);
}