package br.gov.ce.sda.repository.mobile;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.mobile.AtividadeAmbiental;

public interface AtividadeAmbientalRepository extends JpaRepository<AtividadeAmbiental, Long> {
	
	public List<AtividadeAmbiental> findAllByAtividadeProdutiva_Id(Long id);
	
}
