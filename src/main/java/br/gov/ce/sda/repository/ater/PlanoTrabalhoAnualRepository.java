package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.ater.PlanoTrabalhoAnual;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PlanoTrabalhoAnualRepository extends JpaRepository<PlanoTrabalhoAnual, Long> {

    List<PlanoTrabalhoAnual> findAllByCodigoIgnoreCaseContaining(String codigo);

}
