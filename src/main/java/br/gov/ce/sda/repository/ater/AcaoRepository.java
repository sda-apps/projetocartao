package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.ater.Acao;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AcaoRepository extends JpaRepository<Acao, Long> {

	public List <Acao> findAllByOrderByNomeAsc();

    Acao findOneById(Long id);

}
