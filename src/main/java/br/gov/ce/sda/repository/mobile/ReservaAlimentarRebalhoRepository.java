package br.gov.ce.sda.repository.mobile;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.mobile.ReservaAlimentarRebalho;

public interface ReservaAlimentarRebalhoRepository extends JpaRepository<ReservaAlimentarRebalho, Long> {
	
	public List<ReservaAlimentarRebalho> findAllByAtividadeProdutiva_Id(Long id);
	
}
