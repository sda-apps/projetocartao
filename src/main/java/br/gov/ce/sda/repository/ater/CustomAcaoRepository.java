package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.QPessoa;
import br.gov.ce.sda.domain.QPessoaFisica;
import br.gov.ce.sda.domain.QUsuarioAvancado;
import br.gov.ce.sda.domain.acesso.QUsuario;
import br.gov.ce.sda.domain.ater.Acao;
import br.gov.ce.sda.domain.ater.QAcao;
import br.gov.ce.sda.web.rest.dto.FiltroAcaoDTO;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;

@Repository
public class CustomAcaoRepository {

    @Inject
    private JPAQueryFactory queryFactory;

    public List<Acao> findAllByFilter(FiltroAcaoDTO filtro){
        QAcao acao = QAcao.acao;
        QUsuario usuario = QUsuario.usuario;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
        JPAQuery<Acao> query = queryFactory.selectFrom(acao)
            .leftJoin(acao.usuario,usuario).fetchJoin()
            .leftJoin(usuario.usuarioAvancado,usuarioAvancado).fetchJoin()
            .leftJoin(usuarioAvancado.pessoaFisica,pessoaFisica).fetchJoin()
            .leftJoin(pessoaFisica.pessoa).fetchJoin();

        query.where(filtro.getPredicates(acao));
        query.orderBy(acao.nome.asc());

        if (Objects.isNull(filtro.getStart())) {
            filtro.setStart(0);
        }

        filtro.setLimit(10);

        query.offset(filtro.getStart()).limit(filtro.getLimit());

        List<Acao> acoes = query.fetch();

        // Remover ações duplicadas
        LinkedHashSet<Acao> acoesHashSet = Sets.newLinkedHashSet(acoes);
        acoes = Lists.newArrayList(acoesHashSet);

        return acoes;
    }

}
