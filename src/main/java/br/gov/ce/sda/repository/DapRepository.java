package br.gov.ce.sda.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.Dap;

public interface DapRepository extends JpaRepository<Dap, Long> {

	List <Dap> findByAgricultor_IdOrderByDataEmissaoDesc(Long id);

	Dap findFirstByNumero(String numero);

	Dap findFirstByAgricultor_IdAndNumero(Long id, String numero);

}
