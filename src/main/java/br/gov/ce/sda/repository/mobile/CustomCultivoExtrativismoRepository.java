package br.gov.ce.sda.repository.mobile;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Repository;

import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

import br.gov.ce.sda.domain.mobile.CultivoExtrativismo;
import br.gov.ce.sda.domain.mobile.QCultivoExtrativismo;
import br.gov.ce.sda.domain.mobile.QLocalComercializacao;
import br.gov.ce.sda.domain.programaseprojeto.QProdutoEspecificacao;

@Repository
public class CustomCultivoExtrativismoRepository {
	
	@Inject
	private JPAQueryFactory queryFactory;
	
	public List<CultivoExtrativismo> findByAtividadeProdutivaId(Long id) {
		QCultivoExtrativismo cultivoExtrativismo = QCultivoExtrativismo.cultivoExtrativismo;
		
		JPAQuery<CultivoExtrativismo> query = queryFactory.selectFrom(cultivoExtrativismo)
				.leftJoin(cultivoExtrativismo.cultura, QProdutoEspecificacao.produtoEspecificacao).fetchJoin()
				.leftJoin(cultivoExtrativismo.locaisComercializacao, QLocalComercializacao.localComercializacao).fetchJoin()
    			.where(cultivoExtrativismo.atividadeProdutiva.id.eq(id));
		
		return query.fetch();
	}

	
}