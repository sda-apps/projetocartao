package br.gov.ce.sda.repository;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Repository;

import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

import br.gov.ce.sda.domain.QCargo;
import br.gov.ce.sda.domain.QEmpresa;
import br.gov.ce.sda.domain.QEscolaridade;
import br.gov.ce.sda.domain.QEscritorio;
import br.gov.ce.sda.domain.QEstadoCivil;
import br.gov.ce.sda.domain.QFormacao;
import br.gov.ce.sda.domain.QMunicipio;
import br.gov.ce.sda.domain.QPais;
import br.gov.ce.sda.domain.QPessoa;
import br.gov.ce.sda.domain.QPessoaFisica;
import br.gov.ce.sda.domain.QSetor;
import br.gov.ce.sda.domain.QUf;
import br.gov.ce.sda.domain.QUsuarioAvancado;
import br.gov.ce.sda.domain.UsuarioAvancado;
import br.gov.ce.sda.domain.acesso.QAutoridade;
import br.gov.ce.sda.domain.acesso.QUsuario;
import br.gov.ce.sda.domain.acesso.Usuario;

@Repository
public class CustomUsuarioAvancadoRepository {

	@Inject
	private JPAQueryFactory queryFactory;

	public List<String> buscarTodosEmails() {
		QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
		QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
		QPessoa pessoa = QPessoa.pessoa;

		List<String> emails = queryFactory.select(usuarioAvancado.pessoaFisica.pessoa.email)
				.from(usuarioAvancado)
				.join(usuarioAvancado.pessoaFisica, pessoaFisica)
				.join(pessoaFisica.pessoa, pessoa)
				.fetch();

		return emails;
	}

	public List<UsuarioAvancado> buscarTodosUsuarioAvancadoSemLogin() {
		QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
		QUsuario usuario = QUsuario.usuario;

		List<UsuarioAvancado> usuarios = queryFactory.selectFrom(usuarioAvancado)
			.where(usuarioAvancado.id.notIn(queryFactory.select(usuario.usuarioAvancado.id)
					.from(usuario)
					.where(usuario.usuarioAvancado.isNotNull()))
				.and(usuarioAvancado.emailInstitucional.isNotNull()))
			.fetch();

		return usuarios;
	}

	public UsuarioAvancado buscarPorId(Long id) {
		QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;

	    return buscarUsuarioAvancado(usuarioAvancado.id.eq(id));
	}

	public UsuarioAvancado buscarPorCpf(String cpf) {
		QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;

	    return buscarUsuarioAvancado(usuarioAvancado.pessoaFisica.cpf.eq(cpf));
	}

	public List<UsuarioAvancado> buscarPorNome(String nome) {
		QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
		QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
		QPessoa pessoa = QPessoa.pessoa;

		JPAQuery<UsuarioAvancado> query = queryFactory.selectFrom(usuarioAvancado)
				.join(usuarioAvancado.pessoaFisica, pessoaFisica).fetchJoin()
				.join(pessoaFisica.pessoa, pessoa).fetchJoin()
	    		.where(pessoa.nome.containsIgnoreCase(nome));

	    return query.fetch();
	}

	private UsuarioAvancado buscarUsuarioAvancado(Predicate predicate) {
		QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
	    QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
	    QPessoa pessoa = QPessoa.pessoa;
	    QMunicipio municipio = QMunicipio.municipio;

	    JPAQuery<UsuarioAvancado> query = queryFactory.selectFrom(usuarioAvancado)
	    		.where(predicate);

	    query.leftJoin(usuarioAvancado.empresa, QEmpresa.empresa).fetchJoin()
	    	.leftJoin(usuarioAvancado.cargo, QCargo.cargo).fetchJoin()
            .leftJoin(usuarioAvancado.formacao, QFormacao.formacao).fetchJoin()
            .leftJoin(usuarioAvancado.escritorio, QEscritorio.escritorio).fetchJoin()
            .leftJoin(usuarioAvancado.setor, QSetor.setor).fetchJoin()
	    	.join(usuarioAvancado.pessoaFisica, pessoaFisica).fetchJoin()
	    	.join(pessoaFisica.pessoa, pessoa).fetchJoin()
	    	.leftJoin(pessoa.municipio, municipio).fetchJoin()
	    		.leftJoin(municipio.uf, QUf.uf).fetchJoin()
	    	.leftJoin(pessoaFisica.estadoCivil, QEstadoCivil.estadoCivil).fetchJoin()
	    	.leftJoin(pessoaFisica.escolaridade, QEscolaridade.escolaridade).fetchJoin()
	    	.leftJoin(pessoaFisica.localExpedicaoRg, QUf.uf).fetchJoin()
            .leftJoin(pessoaFisica.naturalidade, QMunicipio.municipio).fetchJoin()
            .leftJoin(pessoaFisica.nacionalidade, QPais.pais).fetchJoin();

	    return query.fetchOne();
	}

	public Usuario buscarUsuarioComAutoridadesPorLogin(String login) {
		QUsuario usuario = QUsuario.usuario;
		QAutoridade autoridades = QAutoridade.autoridade;

		Usuario usuairo= queryFactory.selectFrom(usuario)
				.join(usuario.autoridades, autoridades).fetchJoin()
				.where(usuario.login.eq(login)).fetchOne();

		return usuairo;
	}

	public List<UsuarioAvancado> buscarPorNomeEEmpresa(String nome, Long empresa) {
		QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
		QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;
		QPessoa pessoa = QPessoa.pessoa;

		JPAQuery<UsuarioAvancado> query = queryFactory.selectFrom(usuarioAvancado)
				.join(usuarioAvancado.pessoaFisica, pessoaFisica).fetchJoin()
				.join(pessoaFisica.pessoa, pessoa).fetchJoin()
	    		.where(pessoa.nome.containsIgnoreCase(nome)
	    				.and(usuarioAvancado.empresa.id.eq(empresa)));

	    return query.fetch();
	}

}
