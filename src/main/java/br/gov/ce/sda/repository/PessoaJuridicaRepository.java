package br.gov.ce.sda.repository;

import br.gov.ce.sda.domain.PessoaFisica;
import br.gov.ce.sda.domain.PessoaJuridica;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PessoaJuridicaRepository extends JpaRepository<PessoaJuridica, Long> {

	public PessoaJuridica findByCnpj(String cnpj);

}
