package br.gov.ce.sda.repository;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Repository;

import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

import br.gov.ce.sda.domain.AgricultorProjeto;
import br.gov.ce.sda.domain.QAgricultorProjeto;
import br.gov.ce.sda.domain.QUnidadeMedida;
import br.gov.ce.sda.domain.programaseprojeto.Nota;
import br.gov.ce.sda.domain.programaseprojeto.QItemNota;
import br.gov.ce.sda.domain.programaseprojeto.QNota;
import br.gov.ce.sda.domain.programaseprojeto.QProduto;
import br.gov.ce.sda.domain.programaseprojeto.QProdutoEspecificacao;

@Repository
public class CustomProjetoRepository {

	@Inject
	private JPAQueryFactory queryFactory;

	public List<Nota> buscarNotasProjetos(Long agricultorId, Long projetoId) {
		QNota nota = QNota.nota;
		QItemNota itemNota = QItemNota.itemNota;
		QProdutoEspecificacao produtoEspecificacao = QProdutoEspecificacao.produtoEspecificacao;

    	JPAQuery<Nota> query = queryFactory.selectFrom(nota).distinct()
    			.leftJoin(nota.itensNota, itemNota).fetchJoin()
    			.leftJoin(itemNota.produtoEspecificacao, produtoEspecificacao).fetchJoin()
    			.leftJoin(produtoEspecificacao.produto, QProduto.produto).fetchJoin()
    			.leftJoin(produtoEspecificacao.unidade, QUnidadeMedida.unidadeMedida).fetchJoin()
    			.where(nota.agricultor.id.eq(agricultorId)
    					.and(nota.projeto.codigoSda.eq(projetoId)));

		return query.fetch();
	}

	public List <AgricultorProjeto> buscarProjetosPorAgricultorEProjeto(Long agricultorId, Long projetoCodigoSDA) {

		QAgricultorProjeto agricultorProjeto = QAgricultorProjeto.agricultorProjeto;

    	JPAQuery<AgricultorProjeto> query = queryFactory.selectFrom(agricultorProjeto)
    			.where(agricultorProjeto.agricultor.id.eq(agricultorId)
    			.and(agricultorProjeto.projeto.codigoSda.eq(projetoCodigoSDA)
    			.and(agricultorProjeto.valorTotal.isNotNull())));

		return query.fetch();
	}

}
