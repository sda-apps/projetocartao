package br.gov.ce.sda.repository.ater;

import br.gov.ce.sda.domain.QPessoa;
import br.gov.ce.sda.domain.QPessoaFisica;
import br.gov.ce.sda.domain.QUsuarioAvancado;
import br.gov.ce.sda.domain.acesso.QUsuario;
import br.gov.ce.sda.domain.ater.HistoricoAvaliacaoPlanos;
import br.gov.ce.sda.domain.ater.QHistoricoAvaliacaoPlanos;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.List;

import static java.util.Objects.isNull;


@Repository
public class CustomHistoricoAvaliacaoPlanosRepository {

    @Inject
    private JPAQueryFactory queryFactory;

    public HistoricoAvaliacaoPlanos findById(Long id) {
        QHistoricoAvaliacaoPlanos historicoAvaliacaoPlanos = QHistoricoAvaliacaoPlanos.historicoAvaliacaoPlanos;
        QUsuario usuario = QUsuario.usuario;
        QUsuarioAvancado usuarioAvancado = QUsuarioAvancado.usuarioAvancado;
        QPessoaFisica pessoaFisica = QPessoaFisica.pessoaFisica;

        JPAQuery<HistoricoAvaliacaoPlanos> query = queryFactory.selectFrom(historicoAvaliacaoPlanos)
            .join(historicoAvaliacaoPlanos.usuario, usuario).fetchJoin()
            .join(usuario.usuarioAvancado, usuarioAvancado).fetchJoin()
            .join(usuarioAvancado.pessoaFisica, pessoaFisica).fetchJoin()
            .join(pessoaFisica.pessoa, QPessoa.pessoa).fetchJoin()
            .where(historicoAvaliacaoPlanos.id.eq(id));

        return query.fetchOne();
    }

    public List<HistoricoAvaliacaoPlanos> findByPlanoId(Long id, String type) {
        QHistoricoAvaliacaoPlanos historicoAvaliacaoPlanos = QHistoricoAvaliacaoPlanos.historicoAvaliacaoPlanos;
        QUsuario qUsuario = QUsuario.usuario;
        QUsuarioAvancado qUsuarioAvancado = QUsuarioAvancado.usuarioAvancado;
        QPessoaFisica qPessoaFisica = QPessoaFisica.pessoaFisica;

        JPAQuery<HistoricoAvaliacaoPlanos> query = queryFactory.selectFrom(historicoAvaliacaoPlanos)
            .join(historicoAvaliacaoPlanos.usuario, qUsuario).fetchJoin()
            .leftJoin(historicoAvaliacaoPlanos.planoTrabalhoAnual).fetchJoin()
            .leftJoin(historicoAvaliacaoPlanos.planoTrimestral).fetchJoin()
            .join(qUsuario.usuarioAvancado, qUsuarioAvancado).fetchJoin()
            .join(qUsuarioAvancado.pessoaFisica, qPessoaFisica).fetchJoin()
            .join(qPessoaFisica.pessoa, QPessoa.pessoa).fetchJoin()
            .where(historicoAvaliacaoPlanos.planoTrabalhoAnual.id.eq(id).
                or(historicoAvaliacaoPlanos.planoTrimestral.id.eq(id)));

        if(isNull(type)) {
            query.where(historicoAvaliacaoPlanos.tipo.isNull());
        } else {
            query.where(historicoAvaliacaoPlanos.tipo.eq(type));
        }

        return query.fetch();
    }

}
