package br.gov.ce.sda.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.sda.domain.mapa.Poligono;

public interface PoligonoRepository extends JpaRepository<Poligono, Long> {
	
	public List<Poligono> findByAreaGeorreferenciada_Id(Long id);
	
}
