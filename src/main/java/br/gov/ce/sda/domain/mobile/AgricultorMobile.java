package br.gov.ce.sda.domain.mobile;

import br.gov.ce.sda.domain.ater.AssociacaoComunitaria;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.querydsl.core.annotations.QueryInit;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity(name = "br.gov.ce.sda.domain.mobile.agricultor")
@Table(schema = "portalmobile", name = "agricultor")
@Getter @Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = AgricultorMobile.class)
public class AgricultorMobile {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "agricultor_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "pessoa_fisica")
	@QueryInit("pessoa.municipio.territorio")
	private PessoaFisicaMobile pessoaFisica;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "familia")
    private FamiliaMobile familia;

	@Column(name = "grupo_produtivo")
	private String grupoProdutivo;

	@Column(name = "grupo_etnico")
	private String grupoEtnico;

	@Column(name = "atividade_economica_principal")
	private String atividadeEconomicaPrincipal;

	@Column(name = "atividade_economica_secundaria")
    private String atividadeEconomicaSecundaria;

	@Column(name = "renda_mensal")
    private Double rendaMensal;

    @Column(name = "renda_mensal_beneficio_social")
    private Double rendaMensalBeneficioSocial;

    @Column(name = "renda_mensal_aposentadoria")
    private Double rendaMensalAposentadoria;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "coordenada")
	private CoordenadaMobile coordenada;

	@OneToMany(mappedBy = "agricultor", fetch = FetchType.LAZY)
    private Set<BeneficioSocial> beneficiosSociais;

    @Column(name = "tipo_aposentadoria")
    private String tipoAposentadoria;

    /*@Column(name = "atualizacao_paulo_freire")
	private Boolean eAtualizacaoPauloFreire;*/

    @Column(name = "projeto")
    private Character projeto;

    @OneToMany(mappedBy = "agricultor", fetch = FetchType.LAZY)
    private List<DapMobile> daps;

    @Column(name = "_criacao")
	private Date criacao;

	@Column(name = "_atualizacao")
	private Date alteracao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "associacao_comunitaria")
    private AssociacaoComunitaria associacaoComunitaria;
}
