package br.gov.ce.sda.domain;

import java.util.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "parente")
@Getter @Setter
public class Parente {

	public enum Parentesco {
		FILHO("F"), PAI("P"), MAE("M"), AVO("O"), AVOH("H"), CONJUJE("C"), ENTEADO("E"), AGREGADO("A");

		private String parentescoId;

		Parentesco(String parentescoId) {
			this.parentescoId = parentescoId;
		}

		public String getStatus() {
			return this.parentescoId;
		}
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "parente_id")
    private Long id;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name = "familia")
    private Familia familia;

	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name = "agricultor")
    private Agricultor agricultor;

    @Column(name = "parentesco")
    private String parentesco;

    @Column(name = "data_inicio_relacao")
    private Date dataInicioRelacao;

	@Column(name = "data_fim_relacao")
    private Date dataFimRelacao;

}
