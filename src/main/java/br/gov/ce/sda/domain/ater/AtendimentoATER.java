package br.gov.ce.sda.domain.ater;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.gov.ce.sda.domain.Agricultor;
import br.gov.ce.sda.domain.UsuarioAvancado;
import br.gov.ce.sda.domain.programaseprojeto.Programa;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name = "atendimento_ater")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AtendimentoATER {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "atendimento_ater_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "agricultor")
	private Agricultor agricultor;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tecnico")
	private UsuarioAvancado tecnico;

	@Column(name = "data_atendimento")
	private Date dataAtendimento;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "linha_acao")
	private LinhaAcao linhaAcao;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "programa")
	private Programa programa;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "atividade_geral")
	private Atividade atividadeGeral;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "atividade_especifica")
	private AtividadeEspecificacao atividadeEspecifica;


}
