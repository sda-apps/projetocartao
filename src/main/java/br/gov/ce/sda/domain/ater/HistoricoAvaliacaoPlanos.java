package br.gov.ce.sda.domain.ater;

import br.gov.ce.sda.domain.acesso.Usuario;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(schema = "gestaoater", name = "historico_avaliacao_planos")
@Getter
@Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = HistoricoAvaliacaoPlanos.class)
public class HistoricoAvaliacaoPlanos {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
    @SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
    @Column(name = "historico_avaliacao_planos_id")
    private Long id;

    @Column(name = "motivo")
    private String motivo;

    @Column(name = "avaliacao")
    private String avaliacao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "plano_trabalho_anual")
    private PlanoTrabalhoAnual planoTrabalhoAnual;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario")
    private Usuario usuario;

    @Column(name = "_criacao")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", locale = "pt-BR", timezone = "Brazil/East")
    private Date criacao;

    @Column(name = "_atualizacao")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", locale = "pt-BR", timezone = "Brazil/East")
    private Date atualizacao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "plano_trimestral")
    private PlanoTrimestral planoTrimestral;

    @Column(name = "objeto")
    private Long objeto;

    @Column(name = "tipo")
    private String tipo;
}
