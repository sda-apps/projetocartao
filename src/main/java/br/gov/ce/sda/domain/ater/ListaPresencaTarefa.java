package br.gov.ce.sda.domain.ater;

import br.gov.ce.sda.domain.Agricultor;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(schema = "gestaoater", name = "lista_presenca_tarefa")
@Getter
@Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ListaPresencaTarefa.class)
public class ListaPresencaTarefa {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
    @SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
    @Column(name = "lista_presenca_tarefa_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tarefa")
    private Tarefa tarefa;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agricultor")
    private Agricultor agricultor;

    @JsonIgnore
    @Column(name = "_criacao", updatable = false, insertable = false)
    private LocalDateTime criacao;

    @JsonIgnore
    @Column(name = "_atualizacao", updatable = false, insertable = false)
    private LocalDateTime alteracao;
}
