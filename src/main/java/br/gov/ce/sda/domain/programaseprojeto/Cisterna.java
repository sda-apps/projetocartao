package br.gov.ce.sda.domain.programaseprojeto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.gov.ce.sda.domain.Agricultor;
import br.gov.ce.sda.domain.Empresa;
import br.gov.ce.sda.domain.mapa.Elemento;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name = "cisterna")
public class Cisterna {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "cisterna_id")
    private Long id;

	@Column(name = "tipo_cisterna")
	private String tipoCisterna;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "projeto")
	private Projeto projeto;

	@Column(name = "numero")
	private Long numero;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa")
	private Empresa empresa;

	@Column(name = "ano_implantacao")
	private Integer anoImplantacao;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "agricultor")
	private Agricultor agricultor;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "elemento")
	private Elemento elemento;

}
