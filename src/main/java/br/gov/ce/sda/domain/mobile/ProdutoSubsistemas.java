package br.gov.ce.sda.domain.mobile;

import br.gov.ce.sda.domain.UnidadeMedida;
import br.gov.ce.sda.domain.programaseprojeto.ProdutoEspecificacao;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

@Entity
@Getter
@Setter
@Table(schema = "portalmobile", name = "produtos_subsistema")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ProdutoSubsistemas.class)
public class ProdutoSubsistemas {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
    @SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
    @Column(name = "produtos_subsistema_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "subsistema")
    private SubsistemasProducao subsistemasProducao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tipo_producao")
    private ProdutoEspecificacao tipoProducao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "unidade_producao")
    private UnidadeMedida unidadeProducao;

    @Column(name = "valor_unidade")
    private Double valorUnidade;

    @Column(name = "custo_medio")
    private Double custoMedio;

    @Column(name = "quantidade_venda_mercado_institucional")
    private Double quantidadeVendaMercadoInstitucional;

    @Column(name = "quantidade_venda_mercado_formal")
    private Double quantidadeVendaMercadoFormal;

    @Column(name = "quantidade_beneficiamento")
    private Double quantidadeBeneficiamento;

    @Column(name = "innatura_beneficiado")
    private String innaturaBeneficiado;

    @OneToMany(fetch = LAZY, mappedBy = "produtoSubsistema", cascade = ALL)
    private Set<Certificacao> certificacao;

    @Column(name = "quantidade_autoconsumo")
    private Long quantidadeAutoConsumo;

    @Column(name = "quantidade_venda")
    private Long quantidadeVenda;

    @Column(name = "quantidade_estoque")
    private Long quantidadeEstoque;

    @Column(name = "local_feira_municipal")
    private Boolean localFeiraMunicipal;

    @Column(name = "local_feira_familiar")
    private Boolean localFeiraFamiliar;

    @Column(name = "local_feira_agroecologica")
    private Boolean localFeiraAgroecologica;

    @Column(name = "local_mercado_institucional")
    private Boolean localMercadoInstitucional;

    @Column(name = "local_ceasa")
    private Boolean localCEASA;

    @Column(name = "local_comunidade")
    private Boolean localComunidade;

    @Column(name = "local_mercado_local")
    private Boolean localMercadoLocal;

    @Column(name = "local_cooperativa")
    private Boolean localCooperativa;

    @Column(name = "local_atravessador")
    private Boolean localAtravessador;

}
