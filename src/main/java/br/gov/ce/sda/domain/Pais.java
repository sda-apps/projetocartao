package br.gov.ce.sda.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import static org.apache.commons.lang.StringUtils.capitalize;

@Entity
@Getter @Setter
@Table(name = "pais")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Pais {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "pais_id")
    private Long id;

	@Column(name = "descricao")
	private String descricao;

    @Column(name = "nacionalidade")
	private String nacionalidade;

	@Column(name = "codigo_ibge")
	private String codigoIbge;

    @Column(name = "pais_codigo")
    private String sigla;

    public String getNacionalidade() {
        return capitalize(nacionalidade);
    }

}
