package br.gov.ce.sda.domain.mobile;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "br.gov.ce.sda.domain.mobile.parente")
@Table(schema = "portalmobile", name = "parente")
@Getter @Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ParenteMobile.class)
public class ParenteMobile {

	public enum Parentesco {
		FILHO("F"), PAI("P"), MAE("M"), AVO("O"), AVOH("H"), CONJUJE("C"), ENTEADO("E"), AGREGADO("A");

		private String parentescoId;

		Parentesco(String parentescoId) {
			this.parentescoId = parentescoId;
		}

		public String getStatus() {
			return this.parentescoId;
		}
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "parente_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "familia")
    private FamiliaMobile familia;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "agricultor")
    private AgricultorMobile agricultor;

    @Column(name = "parentesco")
    private String parentesco;

    @Column(name = "data_inicio_relacao")
    private Date dataInicioRelacao;

	@Column(name = "data_fim_relacao")
    private Date dataFimRelacao;

}
