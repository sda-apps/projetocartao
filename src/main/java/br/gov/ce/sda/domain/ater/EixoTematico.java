package br.gov.ce.sda.domain.ater;

import br.gov.ce.sda.domain.acesso.Usuario;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(schema = "gestaoater", name = "eixo_tematico")
@Getter @Setter
public class EixoTematico {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
    @SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
    @Column(name = "eixo_tematico_id")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "_criacao")
    private Date criacao;

    @Column(name = "_atualizacao")
    private Date atualizacao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "_usuario")
    private Usuario usuario;

}
