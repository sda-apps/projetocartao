package br.gov.ce.sda.domain.mobile;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(schema = "portalmobile", name = "atividade_produtiva")
@Getter @Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = AtividadeProdutiva.class)
public class AtividadeProdutiva {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "atividade_produtiva_id")
    private Long id;

	@OneToMany(fetch = FetchType.LAZY, mappedBy="atividadeProdutiva", cascade=CascadeType.ALL)
    private Set<SubsistemasProducao> subsistemasProducao;

	@OneToMany(fetch = FetchType.LAZY, mappedBy="atividadeProdutiva", cascade=CascadeType.ALL)
    private Set<InsumoProducao> insumosProducao;

	@OneToMany(fetch = FetchType.LAZY, mappedBy="atividadeProdutiva", cascade=CascadeType.ALL)
    private Set<CombatePragas> combatePragas;

	@OneToMany(fetch = FetchType.LAZY, mappedBy="atividadeProdutiva", cascade=CascadeType.ALL)
    private Set<ReservaAlimentarRebalho> reservaAlimentarRebalho;

	@Column(name = "sistema_criacao_animais")
    private String sistemaCriacaoAnimais;

	@OneToMany(fetch = FetchType.LAZY, mappedBy="atividadeProdutiva", cascade=CascadeType.ALL)
    private Set<AtividadeNaoAgricola> atividadesNaoAgricolas;

	@OneToMany(fetch = FetchType.LAZY, mappedBy="atividadeProdutiva", cascade=CascadeType.ALL)
    private Set<Artesanato> artesanatos;

	@OneToMany(fetch = FetchType.LAZY, mappedBy="atividadeProdutiva", cascade=CascadeType.ALL)
    private Set<AtividadeAmbiental> atividadesAmbientais;

	@Column(name = "uso_fogo_area_plantio")
    private Boolean usoFogoAreaPlantio;

	@Column(name = "tipo_tracao")
    private String tipoTracao;

    @Column(name = "recebe_ater")
    private Boolean recebeAter;

    @Column(name = "instituicao_ater")
    private String instituicaoAter;

    @Column(name = "frequencia_ater")
    private String frequenciaAter;

}
