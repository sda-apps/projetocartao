package br.gov.ce.sda.domain;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import br.gov.ce.sda.domain.mobile.AreaAtuacao;
import com.querydsl.core.annotations.QueryInit;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "usuario_avancado")
@Getter @Setter
public class UsuarioAvancado {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "usuario_avancado_id")
    private Long id;

	@ManyToOne(fetch = FetchType.EAGER, cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name = "pessoa_fisica")
	private PessoaFisica pessoaFisica;

	@Column(name = "telefone_trabalho")
	private String telefoneTrabalho;

	@Column(name = "email_institucional")
	private String emailInstitucional;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa")
    @QueryInit({"empresa.pessoaJuridica", "pessoaJuridica.pessoa"})
	private Empresa empresa;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "escritorio")
	private Escritorio escritorio;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cargo")
	private Cargo cargo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "formacao")
	private Formacao formacao;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "setor")
	private Setor setor;

	@OneToMany(mappedBy = "usuarioAvancado", fetch = FetchType.LAZY, cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE})
    private Set<AreaAtuacao> areasAtuacao;

}
