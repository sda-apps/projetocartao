package br.gov.ce.sda.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tecnologia")
@Getter @Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Tecnologia.class)
public class Tecnologia {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
    @SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
    @Column(name = "tecnologia_id")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "grupo")
    private String grupo;

    @Column(name = "aplicacao")
    private String aplicacao;

    @JsonIgnore
    @Column(name = "_criacao", updatable = false, insertable = false)
    private LocalDateTime criacao;

    @JsonIgnore
    @Column(name = "_atualizacao", updatable = false, insertable = false)
    private LocalDateTime alteracao;

}
