package br.gov.ce.sda.domain.mobile;

import br.gov.ce.sda.domain.Tecnologia;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

import static javax.persistence.FetchType.LAZY;

@Entity
@Table(schema = "portalmobile", name = "tecnologia_subsistema")
@Getter @Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = TecnologiaSubsistema.class)
public class TecnologiaSubsistema {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
    @SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
    @Column(name = "tecnologia_subsistema_id")
    private Long id;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "tecnologia")
    private Tecnologia tecnologia;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "subsistema")
    private SubsistemasProducao subsistema;

    @JsonIgnore
    @Column(name = "_criacao", updatable = false, insertable = false)
    private LocalDateTime criacao;

    @JsonIgnore
    @Column(name = "_atualizacao", updatable = false, insertable = false)
    private LocalDateTime alteracao;

}
