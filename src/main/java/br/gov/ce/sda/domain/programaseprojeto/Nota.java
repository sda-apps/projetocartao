package br.gov.ce.sda.domain.programaseprojeto;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.gov.ce.sda.domain.Agricultor;
import br.gov.ce.sda.domain.AgricultorProjeto;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "nota_projeto")
@Getter @Setter
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Nota {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "nota_projeto_id")
    private Long id;

	@Column(name = "numero")
	private Long numero;

	@Column(name = "data_recebimento")
	private Date dataRecebimento;

	@Column(name = "data_vencimento")
	private Date dataVencimento;

	@Column(name = "data_pagamento")
	private Date dataPagamento;

	@Column(name = "data_emissao")
	private Date dataEmissao;

	@Column(name = "situacao")
	private Character situacao;

	@Column(name = "valor_total")
	private Double valorTotal;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "agricultor")
	private Agricultor agricultor;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "projeto")
	private Projeto projeto;

	@Column(name = "ano")
	private Integer ano;

	@OneToMany(mappedBy = "notaProjeto", fetch = FetchType.LAZY)
    private List<ItemNota> itensNota;

}
