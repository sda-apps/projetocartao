package br.gov.ce.sda.domain.mapa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name = "tipo_elemento")
public class TipoElemento {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "tipo_elemento_id")
    private Long id;

	@Column(name = "tipo")
	private Character tipo;

	@Column(name = "descricao")
	private String descricao;

}
