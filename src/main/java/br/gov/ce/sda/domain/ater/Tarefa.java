package br.gov.ce.sda.domain.ater;

import br.gov.ce.sda.domain.Agricultor;
import br.gov.ce.sda.domain.Municipio;
import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.domain.mapa.Coordenada;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.querydsl.core.annotations.QueryInit;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

@Entity
@Table(schema = "gestaoater", name = "tarefa")
@Getter
@Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Tarefa.class)
public class Tarefa {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
    @SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
    @Column(name = "tarefa_id")
    private Long id;

    @ManyToOne(fetch = LAZY)
    @QueryInit("planoTrabalhoAnual")
    @JoinColumn(name = "plano_trimestral")
    private PlanoTrimestral planoTrimestral;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "atividade")
    private AtividadePTA atividade;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "municipio")
    private Municipio municipio;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "comunidade")
    private Comunidade comunidade;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agricultor")
    @QueryInit("pessoaFisica.pessoa")
    private Agricultor agricultor;

    @OneToMany(fetch = LAZY, mappedBy = "tarefa", cascade = ALL)
    private Set<ListaPresencaTarefa> listaPresencaTarefa;

    @ManyToOne(fetch = LAZY, cascade = ALL)
    @JoinColumn(name = "coordenada")
    private Coordenada coordenada;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "encaminhamento")
    private String encaminhamento;

    @Column(name = "data_ateste")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", locale = "pt-BR", timezone = "Brazil/East")
    private Date dataAteste;

    @Column(name = "data_status")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", locale = "pt-BR", timezone = "Brazil/East")
    private Date dataStatus;

    @Column(name = "data_realizacao")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", locale = "pt-BR", timezone = "Brazil/East")
    private Date dataRealizacao;

    @Column(name = "hora_realizacao")
    private LocalTime horaRealizacao;

    @NotNull(message = "{NotNull.task.duracao}")
    @Column(name = "duracao")
    private Long duracao;

    @Column(name = "status")
    private String status;

    @Column(name = "recusado")
    private Boolean recusado;

    @Column(name = "nao_assina")
    private Boolean naoAssina;

    @OneToMany(fetch = LAZY, mappedBy = "tarefa", cascade = ALL, orphanRemoval = true)
    private Set<TarefaTema> tarefaTemas;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario")
    @QueryInit({"usuarioAvancado", "usuarioAvancado.empresa"})
    private Usuario usuario;

    @JsonIgnore
    @Column(name = "_criacao", updatable = false, insertable = false)
    private LocalDateTime criacao;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", locale = "pt-BR", timezone = "Brazil/East")
    @Column(name = "_atualizacao", updatable = false, insertable = false)
    private Date alteracao;

    @OneToMany(fetch = FetchType.LAZY, mappedBy="tarefa", cascade = CascadeType.ALL)
    private Set<HistoricoAvaliacaoTarefa> historico = new HashSet<>();

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "abrangencia")
    private String abrangencia;

    @Column(name = "anotacao_pessoal")
    private String anotacaoPessoal;

    @Column(name = "anexo")
    private String anexo;

    @OneToMany(fetch = LAZY, cascade = CascadeType.ALL, mappedBy = "tarefa", orphanRemoval = true)
    private Set<ComunidadeParticipante> comunidadeParticipante;

    public enum Status {

        NAO_AVALIADO("N"), EM_AVALIACAO("E"), ACEITO("A"), RECUSADO("R");

        private String status;

        Status(String status) {
            this.status = status;
        }

        public String getStatus() {
            return this.status;
        }
    }

    public enum Tipo {

        PLANEJADA("PL"), NAO_PLANEJADA("NP"), PARTICIPANTE("PA");

        private String status;

        Tipo(String status) {
            this.status = status;
        }

        public String getStatus() {
            return this.status;
        }
    }

    public enum Abrangencia {

        COMUNITARIA("1"), MUNICIPAL("2"), INTERMUNICIPAL("3"), REGIONAL("4"), NACIONAL("5"), INTERNACIONAL("6");

        private String status;

        Abrangencia(String status) {
            this.status = status;
        }

        public String getStatus() {
            return this.status;
        }
    }
}
