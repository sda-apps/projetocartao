package br.gov.ce.sda.domain.mobile;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.querydsl.core.annotations.QueryInit;

import br.gov.ce.sda.domain.Escolaridade;
import br.gov.ce.sda.domain.EstadoCivil;
import br.gov.ce.sda.domain.Municipio;
import br.gov.ce.sda.domain.Pais;
import br.gov.ce.sda.domain.mobile.PessoaMobile;
import br.gov.ce.sda.domain.Uf;
import lombok.Getter;
import lombok.Setter;

@Entity(name = "br.gov.ce.sda.domain.mobile.pessoa_fisica")
@Getter @Setter
@Table(schema = "portalmobile", name = "pessoa_fisica")

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = PessoaFisicaMobile.class)
public class PessoaFisicaMobile {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "pessoa_fisica_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name = "pessoa")
	@QueryInit("municipio.territorio")
	private PessoaMobile pessoa;

	@Column(name = "cpf")
	private String cpf;

	@Column(name = "sexo")
	private String sexo;

	@Column(name = "data_nascimento")
	private Date dataNascimento;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "estado_civil")
	private EstadoCivil estadoCivil;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "escolaridade")
	private Escolaridade escolaridade;

	@Column(name = "rg")
	private String rg;

	@Column(name = "orgao_expedidor_rg")
	private String orgaoExpedidorRg;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "local_expedicao_rg")
	private Uf localExpedicaoRg;

	@Temporal(TemporalType.DATE)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "pt-BR", timezone = "Brazil/East")
	@Column(name = "data_expedicao_rg")
	private Date dataExpedicaoRg;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "naturalidade")
	private Municipio naturalidade;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "nacionalidade")
	private Pais nacionalidade;

	@Column(name = "nome_mae")
	private String nomeMae;

	@Column(name = "nome_pai")
	private String nomePai;

	@Column(name = "nis")
	private String nis;

	@Column(name = "nit")
	private String nit;

	@Column(name = "raca")
	private String raca;

}
