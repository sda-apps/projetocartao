package br.gov.ce.sda.domain.programaseprojeto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name = "item_nota")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ItemNota {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "item_nota_id")
    private Long id;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "nota_projeto")
	private Nota notaProjeto;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "produto_especificacao")
	private ProdutoEspecificacao produtoEspecificacao;

	@Column(name = "quantidade")
	private Double quantidade;

	@Column(name = "valor")
	private Double valor;

	@Column(name = "percentual_desconto")
	private Double percentualDesconto;

	@Column(name = "tanque")
	private String tanque;

	@Column(name = "laticinio")
	private String laticinio;

}
