package br.gov.ce.sda.domain.ater;

import javax.persistence.*;

import br.gov.ce.sda.domain.acesso.Usuario;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Entity
@Table(schema = "gestaoater", name = "atividade")
@Getter @Setter
public class AtividadePTA {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "atividade_id")
    private Long id;

	@Column(name = "nome")
	private String nome;

	@Column(name = "unidade")
    private String unidade;

    @Column(name = "descricao")
    private String descricao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "acao")
    private Acao acao;

    @Column(name = "abrangencia")
    private String abrangencia;

    @Column(name = "_criacao")
    private Date criacao;

    @Column(name = "_atualizacao")
    private Date atualizacao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario")
    private Usuario usuario;

    @Column(name = "inativo")
    private Boolean inativo;


}
