package br.gov.ce.sda.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "dados_bancarios")
@Getter @Setter
public class DadosBancarios {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "dados_bancarios_id")
    private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "banco")
	private Banco banco;

	@Column(name = "agencia")
	private String agencia;

	@Column(name = "conta")
	private String conta;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pessoa")
	private Pessoa pessoa;

	@Column(name = "ativo")
	private boolean ativo;
}
