package br.gov.ce.sda.domain.ater;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(schema = "gestaoater", name = "plano_trimestral")
@Getter
@Setter
public class PlanoTrimestral implements Cloneable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
    @SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
    @Column(name = "plano_trimestral_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "plano_trabalho_anual")
    private PlanoTrabalhoAnual planoTrabalhoAnual;

    @Column(name = "trimestre")
    private Integer trimestre;

    @Column(name = "primeiro_mes")
    private LocalDate primeiroMes;

    @Column(name = "terceiro_mes")
    private LocalDate terceiroMes;

    @Column(name = "status")
    private String status;

    @Column(name = "recusado")
    private Boolean recusado;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "file_path")
    private String filePath;

    @Column(name = "file_status")
    private String fileStatus;

    @Override
    public PlanoTrimestral clone() throws CloneNotSupportedException {
        return (PlanoTrimestral) super.clone();
    }
}
