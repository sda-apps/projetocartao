package br.gov.ce.sda.domain.mobile;

import br.gov.ce.sda.domain.Municipio;
import br.gov.ce.sda.domain.UsuarioAvancado;
import br.gov.ce.sda.domain.ater.Comunidade;
import br.gov.ce.sda.domain.ater.PlanoTrabalhoAnual;
import br.gov.ce.sda.domain.ater.TecnicoPTA;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "area_atuacao")
@Builder @AllArgsConstructor @NoArgsConstructor
@Getter @Setter
public class AreaAtuacao {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "area_atuacao_id")
    private Long id;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_avancado")
	private UsuarioAvancado usuarioAvancado;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "municipio")
	private Municipio municipio;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "comunidade")
    private Comunidade comunidade;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "plano_trabalho_anual")
    private PlanoTrabalhoAnual planoTrabalhoAnual;

}
