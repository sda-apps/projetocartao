package br.gov.ce.sda.domain.mobile;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import br.gov.ce.sda.domain.UsuarioAvancado;
import br.gov.ce.sda.domain.ater.Contrato;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.querydsl.core.annotations.QueryInit;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(schema = "portalmobile", name = "atividade_cadastro")
@Getter @Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = AtividadeCadastro.class)
public class AtividadeCadastro  {

	public enum Status {

		NAO_AVALIADO("N"), EM_AVALIACAO("E"), ACEITO("A"), RECUSADO("R");

		private String status;

		Status(String status) {
			this.status = status;
		}

		public String getStatus() {
			return this.status;
		}
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "atividade_cadastro_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tecnico")
    @QueryInit("empresa.pessoaJuridica")
    private UsuarioAvancado tecnico;

	@Column(name = "data")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", locale = "pt-BR", timezone = "Brazil/East")
    private Date data;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contrato")
    private Contrato contrato;

	@ManyToOne(fetch = FetchType.LAZY, cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name = "informacao_inicial")
    @QueryInit({"agricultor.pessoaFisica", "parceiraAter"})
    private InformacaoInicial informacaoInicial;

	@ManyToOne(fetch = FetchType.LAZY, cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name = "acesso_politicas_publicas")
    private AcessoPoliticasPublicas acessoPoliticasPublicas;

	@ManyToOne(fetch = FetchType.LAZY, cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name = "producao")
    private Producao producao;

	@ManyToOne(fetch = FetchType.LAZY, cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name = "capacitacao")
    private Capacitacao capacitacao;

	@Column(name = "status")
	private String status;

	@Column(name = "observacao")
	private String observacao;

    @Column(name = "recusado")
    private Boolean recusado;

    @OneToMany(fetch = FetchType.LAZY, mappedBy="atividadeCadastro", cascade = CascadeType.ALL)
    private Set<HistoricoAvaliacao> historico = new HashSet<>();

}
