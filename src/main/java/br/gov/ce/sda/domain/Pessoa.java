package br.gov.ce.sda.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.querydsl.core.annotations.QueryInit;

import br.gov.ce.sda.domain.ater.Comunidade;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name = "pessoa")
public class Pessoa {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "pessoa_id")
    private Long id;

	@Column(name = "nome")
	private String nome;

	@Column(name = "apelido")
	private String apelido;

	@Column(name = "nome_fonetico")
	private String nomeFonetico;

	@Column(name = "cep")
	private String cep;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uf")
	private Uf uf;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "territorio")
	private Territorio territorio;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "municipio")
	@QueryInit("municipio.territorio")
	private Municipio municipio;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "distrito")
	private Distrito distrito;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "localidade")
	private Localidade localidade;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "comunidade")
    private Comunidade comunidade;

	@Column(name = "bairro")
	private String bairro;

	@Column(name = "logradouro")
	private String logradouro;

	@Column(name = "numero")
	private String numero;

	@Column(name = "complemento")
	private String complemento;

	@Column(name = "ponto_referencia")
	private String pontoDeReferencia;

	@Column(name = "email")
	private String email;

	@Column(name = "foto")
	private String foto;

	@Column(name = "avatar")
	private String avatar;

	@OneToMany(mappedBy = "pessoa", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Telefone> telefones = new HashSet<Telefone>();

	public void setTelefones(Set<Telefone> telefones) {
	    if (telefones != null) {
            this.telefones.clear();
            this.telefones.addAll(telefones);
        }
    }

}
