package br.gov.ce.sda.domain.mobile;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(schema = "portalmobile", name = "politicas_publicas")
@Getter @Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = PoliticasPublicas.class)
public class PoliticasPublicas {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "politicas_publicas_id")
    private Long id;

	@Column(name = "acesso_pronaf")
    private Boolean acessoPronaf;

	@Column(name = "modalidade_pronaf")
    private String modalidadePronaf;

	@Column(name = "acesso_fedaf")
    private Boolean acessoFedaf;

	@Column(name = "acesso_agroamigo")
    private Boolean acessoAgroamigo;

	@Column(name = "acesso_credamigo")
    private Boolean acessoCredamigo;

	@Column(name = "acesso_garantia_safra")
    private Boolean acessoGarantiaSafra;

	@Column(name = "acesso_segura_defeso")
    private Boolean acessoSeguraDefeso;

	@Column(name = "acesso_seaf")
    private Boolean acessoSeaf;

	@Column(name = "acesso_bolsa_estiagem")
	private Boolean acessoBolsaEstiagem;

	@Column(name = "acesso_paa_tipo_produtor")
	private Boolean acessoPaaTipoProdutor;

	@Column(name = "acesso_paa_tipo_receptor")
    private Boolean acessoPaaTipoReceptor;

    @Column(name = "modalidade_paa")
    private String modalidadePaa;

	@OneToMany(fetch = FetchType.LAZY, mappedBy="politicasPublicas", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ProdutoPaaPoliticaPublica> produtosPaa = new HashSet<>();

	@Column(name = "acesso_paa_leite_categoria_bovino")
	private Boolean acessoPaaLeiteCategoriaBovino;

	@Column(name = "acesso_paa_leite_categoria_caprino")
	private Boolean acessoPaaLeiteCategoriaCaprino;

	@Column(name = "acesso_pnae")
	private Boolean acessoPnae;

	@OneToMany(fetch = FetchType.LAZY, mappedBy="politicasPublicas", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ProdutoPnaePoliticaPublica> produtosPnae;

	@Column(name = "acesso_hora_plantar")
	private Boolean acessoHoraPlantar;

	@OneToMany(fetch = FetchType.LAZY, mappedBy="politicasPublicas", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ProdutoHoraPlantarPoliticaPublica> produtosHoraPlantar;

	@Column(name = "beneficio_mandala")
    private Boolean beneficioMandala;

	@Column(name = "beneficio_quintal_produtivo")
    private Boolean beneficioQuintalProdutivo;

	@Column(name = "acesso_milho_racao_conab")
	private Boolean acessoMilhoRacaoConab;

	@Column(name = "quantidade_milho_racao_conab")
	private Double quantidadeMilhoRacaoConab;

	@Column(name = "beneficio_pais")
    private Boolean beneficioPais;

	@Column(name = "acesso_habitacao_rural")
	private Boolean acessoHabitacaoRural;

	@Column(name = "categoria_habitacao_rural")
    private String categoriaHabitacaoRural;

	@Column(name = "acesso_banheiro_popular")
    private Boolean acessoBanheiroPopular;

	@Column(name = "categoria_banheiro_popular")
    private String categoriaBanheiroPopular;

	@Column(name = "beneficio_cisterna_placa")
    private Boolean beneficioCisternaPlaca;

	@Column(name = "beneficio_cisterna_polietileno")
    private Boolean beneficioCisternaPolietileno;

	@Column(name = "beneficio_cisterna_ferrocimento")
	private Boolean beneficioCisternaFerrocimento;

	@Column(name = "beneficio_cisterna_outra")
    private Boolean beneficioCisternaOutra;

	@Column(name = "beneficio_cisterna_enxurrada")
    private Boolean beneficioCisternaEnxurrada;

	@Column(name = "beneficio_cisterna_calcadao")
    private Boolean beneficioCisternaCalcadao;

	@Column(name = "beneficio_barragem_subterranea")
    private Boolean beneficioBarragemSubterranea;

	@Column(name = "beneficio_tanque_pedra")
    private Boolean beneficioTanquePedra;

	@Column(name = "beneficio_bomba_popular")
    private Boolean beneficioBombaPopular;

	@Column(name = "beneficio_abastecimento_agua")
    private Boolean beneficioAbastecimentoAgua;

	@Column(name = "acesso_arca_letras")
    private Boolean acessoArcaLetras;

	@Column(name = "acesso_casa_digital")
    private Boolean acessoCasaDigital;

	@Column(name = "acesso_pbsm")
	private Boolean acessoPbsm;

	@Column(name = "acesso_auxilio_maternidade")
	private Boolean acessoAuxilioMaternidade;

	@Column(name = "acesso_auxilio_doenca")
	private Boolean acessoAuxilioDoenca;

	@Column(name = "sao_jose_produtivo")
	private Boolean saoJoseProdutivo;

	@Column(name = "sao_jose_infraestrutura")
    private Boolean saoJoseInfraestrutura;

	@Column(name = "sao_jose_outro")
    private Boolean saoJoseOutro;

	@Column(name = "acesso_educacao_comunidade")
    private Boolean acessoEducacaoComunidade;

	@Column(name = "acesso_educacao_outra_comunidade")
    private Boolean acessoEducacaoOutraComunidade;

	@Column(name = "acesso_educacao_municipio")
    private Boolean acessoEducacaoMunicipio;

	@Column(name = "acesso_educacao_outro_municipio")
    private Boolean acessoEducacaoOutroMunicipio;

	@Column(name = "acesso_saude_comunidade")
    private Boolean acessoSaudeComunidade;

	@Column(name = "acesso_saude_distrito")
    private Boolean acessoSaudeDistrito;

	@Column(name = "acesso_saude_municipio")
    private Boolean acessoSaudeMunicipio;

	@Column(name = "acesso_saude_outro_municipio")
    private Boolean acessoSaudeOutroMunicipio;

}
