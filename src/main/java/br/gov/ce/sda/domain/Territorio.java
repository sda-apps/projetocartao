package br.gov.ce.sda.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name = "territorio")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Territorio {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "territorio_id")
    private Long id;

	@Column(name = "nome")
	private String nome;

}
