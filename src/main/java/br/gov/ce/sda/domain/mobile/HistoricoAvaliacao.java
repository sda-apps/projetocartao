package br.gov.ce.sda.domain.mobile;

import br.gov.ce.sda.domain.acesso.Usuario;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(schema = "portalmobile", name = "historico_avaliacao")
@Getter @Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = HistoricoAvaliacao.class)
public class HistoricoAvaliacao {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "historico_avaliacao_id")
    private Long id;

    @Column(name = "motivo")
    private String motivo;

    @Column(name = "avaliacao")
    private String avaliacao;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "atividade_cadastro")
    private AtividadeCadastro atividadeCadastro;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario")
    private Usuario usuario;

	@Column(name = "_criacao")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", locale = "pt-BR", timezone = "Brazil/East")
    private Date criacao;

    @Column(name = "_atualizacao")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", locale = "pt-BR", timezone = "Brazil/East")
    private Date atualizacao;

}
