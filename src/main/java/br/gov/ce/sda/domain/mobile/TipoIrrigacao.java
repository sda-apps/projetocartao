package br.gov.ce.sda.domain.mobile;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter @Setter
@Table(schema = "portalmobile", name = "tipo_irrigacao")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = TipoIrrigacao.class)
public class TipoIrrigacao {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
    @SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
    @Column(name = "tipo_irrigacao_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "subsistema")
    private SubsistemasProducao subsistemasProducao;

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "descricao")
    private String descricao;

}
