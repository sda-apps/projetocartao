package br.gov.ce.sda.domain.sda;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserSDA {

    @JsonProperty(value = "usu_codigo")
    private String codigo;

    @JsonProperty(value = "usu_nome")
    private String nome;

    @JsonProperty(value = "usu_senha")
    private String senha;

    @JsonProperty(value = "usu_email")
    private String email;

    @JsonProperty(value = "usu_apelido")
    private String apelido;

}
