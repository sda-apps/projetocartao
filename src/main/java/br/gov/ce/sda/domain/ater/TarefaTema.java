package br.gov.ce.sda.domain.ater;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.FetchType.LAZY;

@Entity
@Table(schema = "gestaoater", name = "tarefa_tema")
@Getter
@Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = TarefaTema.class)
public class TarefaTema {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
    @SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
    @Column(name = "tarefa_tema_id")
    private Long id;

    @ManyToOne(fetch = EAGER)
    @JoinColumn(name = "tarefa")
    private Tarefa tarefa;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "tema")
    private Tema tema;

    @ManyToMany(cascade = ALL)
    @JoinTable(schema = "gestaoater", name = "tarefa_tema_especificacao",
        joinColumns = @JoinColumn(name = "tarefa_tema", referencedColumnName = "tarefa_tema_id"),
        inverseJoinColumns = @JoinColumn(name = "tema_especificacao", referencedColumnName = "tema_especificacao_id"))
    private Set<TemaEspecificacao> temaEspecificacoes;

}
