package br.gov.ce.sda.domain.mobile;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import br.gov.ce.sda.domain.programaseprojeto.ProdutoEspecificacao;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(schema = "portalmobile", name = "produto_beneficio")
@Getter @Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ProdutoBeneficio.class)
public class ProdutoBeneficio {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "produto_beneficio_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "producao")
    private Producao producao;

	@ManyToOne(fetch = FetchType.LAZY, cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name = "produto")
	private ProdutoEspecificacao produto;

	@Column(name = "estrutura")
    private String estrutura;

	@Column(name = "valor_mensal")
    private Double valorMensal;

	@OneToMany(fetch = FetchType.LAZY, mappedBy="produtoBeneficio", cascade = CascadeType.ALL)
    private Set<LocalComercializacao> locaisComercializacao;

	@OneToMany(fetch = FetchType.LAZY, mappedBy="produtoBeneficio", cascade = CascadeType.ALL)
    private Set<Certificacao> certificacao;

}
