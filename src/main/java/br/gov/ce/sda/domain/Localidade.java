package br.gov.ce.sda.domain;

import br.gov.ce.sda.domain.ater.Comunidade;
import br.gov.ce.sda.domain.mapa.Coordenada;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

@Entity
@Table(name = "localidade")
@Getter @Setter
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Localidade {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "localidade_id")
    private Long id;

	@Column(name = "nome")
	private String nome;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "distrito")
	private Distrito distrito;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coordenada")
	private Coordenada coordenada;

	@Column(name = "codigo_sda")
    private String codigoSda;

	@JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "comunidade")
    private Comunidade comunidade;

}
