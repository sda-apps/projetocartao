package br.gov.ce.sda.domain.ater;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(schema = "gestaoater", name = "tarefa_comunidade_participante")
@Builder @AllArgsConstructor @NoArgsConstructor
@Getter
@Setter
public class ComunidadeParticipante {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
    @SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
    @Column(name = "tarefa_comunidade_participante_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tarefa")
    private Tarefa tarefa;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "comunidade")
    private Comunidade comunidade;

}
