package br.gov.ce.sda.domain.ater;

import br.gov.ce.sda.domain.acesso.Usuario;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(schema = "gestaoater", name = "acao")
@Getter @Setter
public class Acao {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "acao_id")
    private Long id;

	@Column(name = "nome")
	private String nome;

	@Column(name = "descricao")
    private String descricao;

    @Column(name = "_criacao")
    private Date criacao;

    @Column(name = "_atualizacao")
    private Date atualizacao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario")
    private Usuario usuario;

}
