package br.gov.ce.sda.domain.mobile;

import br.gov.ce.sda.domain.programaseprojeto.ProdutoEspecificacao;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(schema = "portalmobile", name = "produto_paa_politica_publica")
@Getter @Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ProdutoPaaPoliticaPublica.class)
public class ProdutoPaaPoliticaPublica {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "produto_paa_politica_publica_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "politicas_publicas")
    private PoliticasPublicas politicasPublicas;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "produto_especificacao")
    private ProdutoEspecificacao produtoEspecificacao;

}
