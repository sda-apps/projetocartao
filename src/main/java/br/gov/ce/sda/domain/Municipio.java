package br.gov.ce.sda.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.gov.ce.sda.domain.ater.Comunidade;
import br.gov.ce.sda.domain.ater.Regiao;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.gov.ce.sda.domain.mapa.Coordenada;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "municipio")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Getter @Setter
public class Municipio {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "municipio_id")
    private Long id;

	@Column(name = "nome")
	private String nome;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uf")
	private Uf uf;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "territorio")
	private Territorio territorio;

	@Column(name = "codigo_ibge")
	private String codigoIbge;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coordenada")
	private Coordenada coordenada;

	@Transient
	private String municipioTerritorio;

    @Transient
    private Regiao regiao;

    @Transient
    private List<Comunidade> comunidades;

    @Override
    public String toString() {
        return this.nome;
    }

}
