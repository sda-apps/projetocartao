package br.gov.ce.sda.domain.ater;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

import static javax.persistence.FetchType.LAZY;

@Entity
@Table(schema = "gestaoater", name = "tarefa_tema_especificacao")
@Getter
@Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = TarefaTemaEspecificacao.class)
public class TarefaTemaEspecificacao {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
    @SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
    @Column(name = "tarefa_tema_especificacao_id")
    private Long id;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "tarefa_tema")
    private TarefaTema tarefaTema;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tema_especificacao")
    private TemaEspecificacao temaEspecificacao;

}
