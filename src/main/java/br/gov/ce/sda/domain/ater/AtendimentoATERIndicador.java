package br.gov.ce.sda.domain.ater;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.gov.ce.sda.domain.UnidadeMedida;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name = "atendimento_ater_indicador")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AtendimentoATERIndicador {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "atendimento_ater_indicador_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "atendimento_ater")
	private AtendimentoATER atendimentoAter;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "indicador")
	private Indicador indicador;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "unidade")
	private UnidadeMedida unidade;

	@Column(name = "quantidade")
	private Double quantidade;

}
