package br.gov.ce.sda.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.querydsl.core.annotations.QueryInit;

import br.gov.ce.sda.domain.mapa.AreaGeorreferenciada;
import br.gov.ce.sda.domain.mapa.Elemento;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "agricultor")
@Getter @Setter
public class Agricultor {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "agricultor_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "pessoa_fisica")
	@QueryInit("pessoa.municipio.territorio")
	private PessoaFisica pessoaFisica;

	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name = "familia")
	private Familia familia;

	@Column(name = "grupo_produtivo")
	private String grupoProdutivo;

	@Column(name = "possui_elemento")
    private Boolean possuiElemento;

    @Column(name = "possui_poligono")
    private Boolean possuiPoligono;

    @Column(name = "atualizacao_projeto")
    private Character atualizacaoProjeto;

    @Column(name = "atualizacao_paulo_freire")
    private Boolean atualizacaoPauloFreire;

    @OneToMany(mappedBy = "agricultor", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private Set<Dap> daps;

    @OneToMany(mappedBy = "agricultor", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private Set<Elemento> elementos;

    @OneToMany(mappedBy = "agricultor", fetch = FetchType.LAZY)
	private List <AgricultorProjeto> projetos;

    @ManyToMany
    @JoinTable(name = "agricultor_area_georreferenciada",
        joinColumns = {@JoinColumn(name = "agricultor", referencedColumnName = "agricultor_id")},
        inverseJoinColumns = {@JoinColumn(name = "area_georreferenciada", referencedColumnName = "area_georreferenciada_id")})
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<AreaGeorreferenciada> areasGeorreferenciadas = new ArrayList<AreaGeorreferenciada>();

}
