package br.gov.ce.sda.domain.ater;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public class ViewPlanosAnuais {

    @Id
    @Column
    private Long id;

    @Column
    private String status;

    @Column
    private Boolean recusado;

    @Column(name = "data_inicio")
    private Date dataInicio;

    @Column(name = "contrato_id")
    private Long contratoId;

    @Column(name = "contrato")
    private String contrato;

    @Column(name = "empresa_id")
    private Long empresaId;

    @Column(name = "nome_fantasia")
    private String nomeFantasia;

    @Column(name = "regiao_id")
    private Long regiaoId;

    @Column(name = "regiao")
    private String regiao;

}
