package br.gov.ce.sda.domain;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.util.List;

@Entity
@Table(name = "uf")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Getter @Setter
public class Uf {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "uf_id")
    private Long id;

	@Column(name = "sigla")
	private String sigla;

	@Column(name = "descricao")
	private String descricao;

    @Column(name = "codigo_ibge")
    private String codigoIbge;

	@Transient
	private List<Municipio> municipios;

}
