package br.gov.ce.sda.domain.mobile;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import br.gov.ce.sda.domain.Empresa;
import com.querydsl.core.annotations.QueryInit;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(schema = "portalmobile", name = "informacao_inicial")
@Getter @Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = InformacaoInicial.class)
public class InformacaoInicial {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "informacao_inicial_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parceira_ater")
    private Empresa parceiraAter;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "agricultor")
    @QueryInit("agricultor.pessoaFisica")
    private AgricultorMobile agricultor;

	@Column(name = "reside_imovel_rural")
	private Boolean resideImovelRural;

	@Column(name = "possui_outro_imovel")
    private Boolean possuiOutroImovel;

	@Column(name = "tempo_preenchimento")
    private Long tempoPreenchimento;

}
