package br.gov.ce.sda.domain.ater;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public class ViewPlanosTrimestrais {

    @Id
    @Column
    private Long id;

    @Column
    private String status;

    @Column(name = "contrato_id")
    private Long contratoId;

    @Column(name = "codigo_do_contrato")
    private String codigoDoContrato;

    @Column(name = "codigo_do_plano_de_trabalho")
    private String codigoDoPlanoDeTrabalho;

    @Column
    private int trimestre;

    @Column(name = "primeiro_mes")
    private Date primeiroMes;

    @Column(name = "terceiro_mes")
    private Date terceiroMes;

    @Column(name = "codigo_empresa")
    private Long empresaId;

    @Column(name = "nome_fantasia_da_empresa")
    private String nomeFantasiaDaEmpresa;

    @Column(name = "codigo_regiao")
    private Long regiaoId;

    @Column(name = "nome_regiao")
    private String nomeRegiao;

    @Column
    private Boolean recusado;

    @Column(name = "nome_coordenador")
    private String nomeCoordenador;

    @Column(name = "pta_status")
    private String ptaStatus;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "file_path")
    private String filePath;

    @Column(name = "file_status")
    private String fileStatus;
}
