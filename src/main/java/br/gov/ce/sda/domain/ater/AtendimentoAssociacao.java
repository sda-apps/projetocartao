package br.gov.ce.sda.domain.ater;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.querydsl.core.annotations.QueryInit;

import br.gov.ce.sda.domain.Agricultor;
import br.gov.ce.sda.domain.Empresa;
import br.gov.ce.sda.domain.UsuarioAvancado;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name = "atendimento_associacao")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AtendimentoAssociacao {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "atendimento_associacao_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "agricultor")
	private Agricultor agricultor;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tecnico")
	@QueryInit("pessoaFisica.pessoa")
	private UsuarioAvancado tecnico;

	@Column(name = "ano")
	private String ano;

	@Transient
	private String anoInicio;

	@Transient
	private String anoFim;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa")
	@QueryInit("pessoaJuridica.pessoa")
	private Empresa empresa;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_ater")
	private TipoATER tipoAter;

}
