package br.gov.ce.sda.domain.mapa;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.querydsl.core.annotations.QueryInit;

import br.gov.ce.sda.domain.Agricultor;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name = "elemento")
public class Elemento {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "elemento_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "agricultor")
	@QueryInit(value={"pessoaFisica.pessoa", "pessoaFisica.pessoa.municipio.territorio", "distrito"})
	private Agricultor agricultor;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "coordenada")
	private Coordenada coordenada;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_elemento")
	private TipoElemento tipoElemento;

}
