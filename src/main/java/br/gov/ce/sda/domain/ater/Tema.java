package br.gov.ce.sda.domain.ater;

import br.gov.ce.sda.domain.acesso.Usuario;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(schema = "gestaoater", name = "tema")
@Getter
@Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Tema.class)
public class Tema {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
    @SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
    @Column(name = "tema_id")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "descricao")
    private String descricao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "eixo_tematico")
    private EixoTematico eixoTematico;

    @Column(name = "_criacao")
    private Date criacao;

    @Column(name = "_atualizacao")
    private Date atualizacao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "_usuario")
    private Usuario usuario;

    @OneToMany(mappedBy = "tema", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<TemaEspecificacao> especificacoes = new HashSet<>();
}
