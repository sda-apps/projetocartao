package br.gov.ce.sda.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.querydsl.core.annotations.QueryInit;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name = "pessoa_fisica")
public class PessoaFisica {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "pessoa_fisica_id")
    private Long id;

	@ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name = "pessoa")
	@QueryInit("municipio.territorio")
	private Pessoa pessoa;

	@Column(name = "cpf")
	private String cpf;

	@Column(name = "sexo")
	private String sexo;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", locale = "pt-BR", timezone = "Brazil/East")
	@Column(name = "data_nascimento")
	private Date dataNascimento;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "estado_civil")
	private EstadoCivil estadoCivil;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "escolaridade")
	private Escolaridade escolaridade;

	@Column(name = "rg")
	private String rg;

	@Column(name = "orgao_expedidor_rg")
	private String orgaoExpedidorRg;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "local_expedicao_rg")
	private Uf localExpedicaoRg;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", locale = "pt-BR", timezone = "Brazil/East")
	@Column(name = "data_expedicao_rg")
	private Date dataExpedicaoRg;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "naturalidade")
	private Municipio naturalidade;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "nacionalidade")
	private Pais nacionalidade;

	@Column(name = "nome_mae")
	private String nomeMae;

	@Column(name = "nome_pai")
	private String nomePai;

	@Column(name = "nis")
	private String nis;

	@Column(name = "nit")
	private String nit;

	@Column(name = "raca")
	private String raca;

}
