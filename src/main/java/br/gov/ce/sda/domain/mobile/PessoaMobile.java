package br.gov.ce.sda.domain.mobile;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.gov.ce.sda.domain.ater.Comunidade;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.querydsl.core.annotations.QueryInit;

import br.gov.ce.sda.domain.Distrito;
import br.gov.ce.sda.domain.Localidade;
import br.gov.ce.sda.domain.Municipio;
import br.gov.ce.sda.domain.Territorio;
import br.gov.ce.sda.domain.Uf;
import lombok.Getter;
import lombok.Setter;

@Entity(name = "br.gov.ce.sda.domain.mobile.pessoa")
@Getter @Setter
@Table(schema = "portalmobile", name = "pessoa")

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = PessoaMobile.class)
public class PessoaMobile {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "pessoa_id")
    private Long id;

	@Column(name = "nome")
	private String nome;

	@Column(name = "apelido")
	private String apelido;

	@Column(name = "nome_fonetico")
	private String nomeFonetico;

	@Column(name = "cep")
	private String cep;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uf")
	private Uf uf;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "territorio")
	private Territorio territorio;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "municipio")
	@QueryInit("municipio.territorio")
	private Municipio municipio;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "distrito")
	private Distrito distrito;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "localidade")
	private Localidade localidade;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "comunidade")
    private Comunidade comunidade;

	@Column(name = "bairro")
	private String bairro;

	@Column(name = "logradouro")
	private String logradouro;

	@Column(name = "numero")
	private String numero;

	@Column(name = "complemento")
	private String complemento;

	@Column(name = "ponto_referencia")
	private String pontoDeReferencia;

	@Column(name = "email")
	private String email;

	@Column(name = "foto")
	private String foto;

	@Column(name = "avatar")
	private String avatar;

	@OneToMany(mappedBy = "pessoa", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<TelefoneMobile> telefones;

}
