package br.gov.ce.sda.domain.mobile;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(schema = "portalmobile", name = "acesso_politicas_publicas")
@Getter @Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = AcessoPoliticasPublicas.class)
public class AcessoPoliticasPublicas {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "acesso_politicas_publicas_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY, cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name = "politicas_publicas")
    private PoliticasPublicas politicasPublicas;

	@ManyToOne(fetch = FetchType.LAZY, cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name = "habitacao_seneamento_energia")
    private HabitacaoSeneamentoEnergia habitacaoSeneamentoEnergia;

	@Column(name = "tempo_preenchimento")
    private Long tempoPreenchimento;

}
