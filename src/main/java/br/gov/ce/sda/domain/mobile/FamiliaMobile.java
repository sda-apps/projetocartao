package br.gov.ce.sda.domain.mobile;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.gov.ce.sda.domain.Agricultor;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "br.gov.ce.sda.domain.mobile.familia")
@Table(schema = "portalmobile", name = "familia")
@Getter @Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = FamiliaMobile.class)
public class FamiliaMobile {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "familia_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "chefe")
    private Agricultor chefe;

	@OneToMany(fetch = FetchType.LAZY, mappedBy="familia", cascade = CascadeType.ALL)
    private List<ParenteMobile> parentes;

}
