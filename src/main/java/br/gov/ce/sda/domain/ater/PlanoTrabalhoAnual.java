package br.gov.ce.sda.domain.ater;

import br.gov.ce.sda.domain.UsuarioAvancado;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(schema = "gestaoater", name = "plano_trabalho_anual")
@Getter
@Setter
public class PlanoTrabalhoAnual {

    public enum Situacao {
        ATIVO("A"), INATIVO("I");

        @Getter
        String id;

        Situacao(String id) {
            this.id = id;
        }

    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
    @SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
    @Column(name = "plano_trabalho_anual_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contrato")
    private Contrato contrato;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "coordenador")
    private UsuarioAvancado coordenador;

    @Column(name = "ano_competencia")
    private Integer anoCompetencia;

    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "pt-BR", timezone = "Brazil/East")
    @Column(name = "data_inicio")
    private Date dataInicio;

    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "pt-BR", timezone = "Brazil/East")
    @Column(name = "data_encerramento")
    private Date dataEncerramento;

    @Column(name = "status")
    private String status;

    @Column(name = "codigo")
    private String codigo;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "planoTrabalhoAnual", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<MetaFamiliasPTA> metasFamiliasPta;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "planoTrabalhoAnual", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<MetaAtividadesPTA> metasAtividadesPta;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "planoTrabalhoAnual")
    private Set<PlanoTrimestral> planosTrimestrais;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "planoTrabalhoAnual")
    private Set<TecnicoPTA> tecnicos;

    @Column(name = "recusado")
    private boolean recusado;

    @Column(name = "situacao")
    private String situacao;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "planoTrabalhoAnual", cascade = CascadeType.ALL)
    private Set<HistoricoAvaliacaoPlanos> historico = new HashSet<>();

    @Column(name = "plano")
    private String plano;

    @Column(name = "reaberto")
    private boolean reaberto;

}
