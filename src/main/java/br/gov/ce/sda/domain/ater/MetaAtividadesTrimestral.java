package br.gov.ce.sda.domain.ater;

import br.gov.ce.sda.domain.Municipio;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(schema = "gestaoater", name = "meta_atividades_trimestral")
@Getter @Setter
public class MetaAtividadesTrimestral implements Cloneable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "meta_atividades_trimestral_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "meta_atividades_pta")
    private MetaAtividadesPTA metaAtividadesPTA;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "atividade")
    private AtividadePTA atividade;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plano_trimestral")
    private PlanoTrimestral planoTrimestral;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "municipio")
    private Municipio municipio;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "comunidade")
    private Comunidade comunidade;

	@Column(name = "quantidade_meta")
    private Long quantidadeMeta;

    @Column(name = "abrangencia")
    private String abrangencia;

    @Override
    public MetaAtividadesTrimestral clone() throws CloneNotSupportedException {
        return (MetaAtividadesTrimestral) super.clone();
    }
}
