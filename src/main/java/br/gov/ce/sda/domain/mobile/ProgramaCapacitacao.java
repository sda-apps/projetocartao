package br.gov.ce.sda.domain.mobile;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(schema = "portalmobile", name = "programa_capacitacao")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ProgramaCapacitacao.class)
public class ProgramaCapacitacao {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "programa_capacitacao_id")
    private Long id;

	@OneToMany(fetch = FetchType.LAZY, mappedBy="programaCapacitacao", cascade = CascadeType.ALL)
    private Set<Tema> temas;

    @OneToMany(mappedBy = "programaCapacitacao", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<OrgaoCapacitacao> orgaos;
}
