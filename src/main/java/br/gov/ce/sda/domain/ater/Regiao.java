package br.gov.ce.sda.domain.ater;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

@Entity
@Getter @Setter
@Table(schema = "gestaoater", name = "regiao")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Regiao {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
    @SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
    @Column(name = "regiao_id")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "inativo")
    private boolean inativo;

    @Override
    public String toString() {
        return this.nome;
    }
}
