package br.gov.ce.sda.domain;

import br.gov.ce.sda.domain.ater.Contrato;
import com.querydsl.core.annotations.QueryInit;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "empresa")
public class Empresa {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
    @SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
    @Column(name = "empresa_id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinColumn(name = "pessoa_juridica")
    @QueryInit({"pessoaJuridica.pessoa", "pessoa.municipio"})
    private PessoaJuridica pessoaJuridica;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "orgao")
    private Orgao orgao;

    @Column(name = "observacao")
    private String observacao;

    @Column(name = "tipo_sociedade")
    private String tipoSociedade;

    @Column(name = "nome_representante")
    private String nomeRepresentante;

    @Column(name = "cpf_representante")
    private String cpfRepresentante;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "nacionalidade_representante")
    private Pais nacionalidadeRepresentante;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "titulo_representante")
    private Cargo tituloRepresentante;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "estado_civil_representante")
    private EstadoCivil estadoCivilRepresentante;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "profissao_representante")
    private Funcao profissaoRepresentante;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "empresa", cascade = CascadeType.ALL)
    private Set<Contrato> contrato;

    @Column(name = "projeto_paulo_freire")
    private Boolean isProjetoPauloFreire;

}
