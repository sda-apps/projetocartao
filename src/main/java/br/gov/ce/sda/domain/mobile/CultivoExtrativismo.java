package br.gov.ce.sda.domain.mobile;

import java.util.List;

import javax.persistence.*;

import br.gov.ce.sda.domain.UnidadeMedida;
import br.gov.ce.sda.domain.programaseprojeto.ProdutoEspecificacao;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(schema = "portalmobile", name = "cultivo_extrativismo")
@Getter @Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = CultivoExtrativismo.class)
public class CultivoExtrativismo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "cultivo_extrativismo_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "atividade_produtiva")
    private AtividadeProdutiva atividadeProdutiva;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cultura")
    private ProdutoEspecificacao cultura;

	@Column(name = "area")
    private Double area;

	@Column(name = "producao")
    private Double producao;

    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "unidade")
    private UnidadeMedida unidade;

    @Column(name = "quantidade_consumo")
    private Double quantidadeConsumo;

    @OneToMany(fetch = FetchType.LAZY, mappedBy="atividadeCultivoExtrativismo", cascade = CascadeType.ALL)
    private List<LocalComercializacao> locaisComercializacao;

}
