package br.gov.ce.sda.domain.mapa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "area_georreferenciada")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AreaGeorreferenciada {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "area_georreferenciada_id")
    private Long id;

	@Column(name = "tipo")
	private Character tipo;

	@OneToMany(mappedBy = "areaGeorreferenciada", fetch = FetchType.EAGER)
    private List<Poligono> poligono  = new ArrayList<Poligono>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Poligono> getPoligono() {
		return poligono;
	}

	public void setPoligono(List<Poligono> poligono) {
		this.poligono = poligono;
	}
}
