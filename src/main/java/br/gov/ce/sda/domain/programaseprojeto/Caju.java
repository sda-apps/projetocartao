package br.gov.ce.sda.domain.programaseprojeto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.gov.ce.sda.domain.Agricultor;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name = "movimentacao_caju")
public class Caju {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "movimentacao_caju_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="agricultor")
	private Agricultor agricultor;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "projeto")
	@JsonIgnore
	private Projeto projeto;

	@Column(name="ano")
	private Integer ano;

	@Column(name="qtd_corte_ate_70")
	private Double quantidadeCorteAte70;

	@Column(name="vlr_corte_ate_70")
	private Double valorCorteAte70;

	@Column(name="qtd_corte_acima_70")
	private Double quantidadeCorteAcima70;

	@Column(name="vlr_corte_acima_70")
	private Double valorCorteAcima70;

	@Column(name="qtd_pegada_ate_70")
	private Double quantidadePegadaAte70;

	@Column(name="vlr_pegada_ate_70")
	private Double valorPegadaAte70;

	@Column(name="qtd_pegada_acima_70")
	private Double quantidadePegadaAcima70;

	@Column(name="vlr_pegada_acima_70")
	private Double valorPegadaAcima70;

}
