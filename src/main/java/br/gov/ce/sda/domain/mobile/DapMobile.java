package br.gov.ce.sda.domain.mobile;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "br.gov.ce.sda.domain.mobile.dap")
@Getter @Setter
@Table(schema = "portalmobile", name = "dap")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = DapMobile.class)
public class DapMobile {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "dap_id")
    private Long id;

	@Column(name = "modelo")
	private String modelo;

	@Column(name = "variacao")
	private String variacao;

	@Column(name="data_emissao")
	private Date dataEmissao;

	@Column(name="data_expiracao")
	private Date dataExpiracao;

	@Column(name="numero")
	private String numero;

	@Column(name="tempo_validade")
	private String tempoValidade;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "agricultor")
	private AgricultorMobile agricultor;

}
