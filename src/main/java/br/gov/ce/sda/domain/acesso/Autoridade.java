package br.gov.ce.sda.domain.acesso;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * An authority (a security role) used by Spring Security.
 */
@Entity
@Table(name = "autoridade")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Getter @Setter @ToString
public class Autoridade implements GrantedAuthority {

	private static final long serialVersionUID = 1L;

	@NotNull
    @Size(min = 0, max = 50)
    @Id
    @Column(name="nome", length = 50)
    private String nome;

	@JsonIgnore
    @Override
	public String getAuthority() {
		return nome;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Autoridade authority = (Autoridade) o;

        if (nome != null ? !nome.equals(authority.nome) : authority.nome != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return nome != null ? nome.hashCode() : 0;
    }

}
