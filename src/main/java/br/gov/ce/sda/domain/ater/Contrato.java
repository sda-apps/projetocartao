package br.gov.ce.sda.domain.ater;

import java.util.Date;
import java.util.Set;

import javax.persistence.*;

import br.gov.ce.sda.domain.Empresa;
import br.gov.ce.sda.domain.Municipio;
import br.gov.ce.sda.domain.Territorio;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(schema = "gestaoater", name = "contrato")
@Getter @Setter
public class Contrato {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "contrato_id")
    private Long id;

	@Column(name = "codigo")
    private String codigo;

	@Column(name = "titulo")
    private String titulo;

	@Column(name = "objeto")
    private String objeto;

    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "pt-BR", timezone = "Brazil/East")
	@Column(name = "data_assinatura")
    private Date dataAssinatura;

    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "pt-BR", timezone = "Brazil/East")
	@Column(name = "data_encerramento")
    private Date dataEncerramento;

	@Column(name = "valor")
    private Double valor;

	@Column(name = "observacao")
    private String observacao;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa")
    private Empresa empresa;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "regiao")
    private Regiao regiao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "territorio")
    private Territorio territorio;

	@ManyToMany
	@JoinTable(schema="gestaoater", name = "abrangencia_contrato",
	joinColumns = @JoinColumn(name = "contrato", referencedColumnName = "contrato_id"),
	inverseJoinColumns = @JoinColumn(name = "municipio", referencedColumnName = "municipio_id"))
	private Set<Municipio> abrangencia;

    @Column(name = "projeto")
	private String projeto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "associacaoComunitaria")
    private AssociacaoComunitaria associacaoComunitaria;

}
