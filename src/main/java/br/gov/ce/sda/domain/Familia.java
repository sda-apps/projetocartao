package br.gov.ce.sda.domain;

import java.util.List;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "familia")
@Getter @Setter
public class Familia {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "familia_id")
    private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "chefe")
    private Agricultor chefe;

	@OneToMany(fetch = FetchType.LAZY, mappedBy="familia", cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, orphanRemoval = true)
    private List<Parente> parentes;

}
