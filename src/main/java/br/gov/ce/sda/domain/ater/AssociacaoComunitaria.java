package br.gov.ce.sda.domain.ater;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter @Setter
@Table(schema = "gestaoater", name = "associacao_comunitaria")
public class AssociacaoComunitaria {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
    @SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
    @Column(name = "associacao_comunitaria_id")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "tipo")
    private String tipo;

}
