package br.gov.ce.sda.domain.mobile;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(schema = "portalmobile", name = "orgao_capacitacao")
@Getter
@Setter
public class OrgaoCapacitacao {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
    @SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
    @Column(name = "orgao_capacitacao_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "programa_capacitacao")
    private ProgramaCapacitacao programaCapacitacao;

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "descricao")
    private String descricao;

}
