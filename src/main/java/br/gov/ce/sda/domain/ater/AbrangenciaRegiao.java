package br.gov.ce.sda.domain.ater;

import br.gov.ce.sda.domain.Municipio;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

@Entity
@Getter @Setter
@Table(schema = "gestaoater", name = "abrangencia_regiao")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AbrangenciaRegiao {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
    @SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
    @Column(name = "abrangencia_regiao_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "regiao")
    private Regiao regiao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "municipio")
    private Municipio municipio;

}
