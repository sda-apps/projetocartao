package br.gov.ce.sda.domain.mobile;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Entity
@Table(schema = "portalmobile", name = "organizacao_capacitacao")
@Getter @Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = OrganizacaoCapacitacao.class)
public class OrganizacaoCapacitacao {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "organizacao_capacitacao_id")
    private Long id;

	@Column(name = "existe_festa_tradicao")
    private Boolean existeFestaTradicao;

	@Column(name = "qual_festa_tradicao")
    private String qualFestaTradicao;

	@Column(name = "quantidade_membros_sindicalizados")
    private Integer quantidadeMembrosSindicalizados;

    @OneToMany(mappedBy = "organizacaoCapacitacao", fetch = FetchType.LAZY, cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE})
    private Set<OrganizacaoLocal> organizacaoLocal;
}
