package br.gov.ce.sda.domain.ater;

import br.gov.ce.sda.domain.Distrito;
import br.gov.ce.sda.domain.Localidade;
import br.gov.ce.sda.domain.Municipio;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(schema = "gestaoater",name = "comunidade")
@Builder @AllArgsConstructor @NoArgsConstructor
@Getter @Setter
public class Comunidade {

    public enum Tipo {

        AGRICULTURA_FAMILIAR("A","Agricultura Familiar"), ASSENTAMENTO("S", "Assentamento de Reforma Agrária"), PESCA("P", "Pesca Artesanal"), INDIGENA("I","Indígena"), QUILOMBOLA("Q","Quilombola"), TODOS("0", "Todos");

        private String id;
        private String descricao;

        Tipo(String id, String descricao) {
            this.id = id;
            this.descricao = descricao;
        }

        public String getId() {
            return id;
        }

        public String getDescricao() {
            return descricao;
        }

        public static String getDescricaoById(String id) {
            for(Tipo t : values()) {
                if(t.id.equals(id)) return t.descricao;
            }
            return Tipo.TODOS.descricao;
        }

    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
    @SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
    @Column(name = "comunidade_id")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "municipio")
    private Municipio municipio;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "distrito")
    private Distrito distrito;

    @OneToMany(mappedBy = "comunidade", fetch = FetchType.LAZY)
    private Set<Localidade> localidades;

    private String tipo;

    @Transient
    private String comunidadeDistrito;

    @Override
    public String toString() {
        return this.nome;
    }
}
