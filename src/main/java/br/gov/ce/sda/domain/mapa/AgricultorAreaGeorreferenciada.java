package br.gov.ce.sda.domain.mapa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.querydsl.core.annotations.QueryInit;

import br.gov.ce.sda.domain.Agricultor;

@Entity
@Table(name = "agricultor_area_georreferenciada")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AgricultorAreaGeorreferenciada {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "agricultor_area_georreferenciada_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "agricultor")
	@QueryInit({"pessoaFisica.pessoa", "pessoaFisica.pessoa.municipio.territorio", "distrito"})
	private Agricultor agricultor;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "area_georreferenciada")
	private AreaGeorreferenciada areaGeorreferenciada;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Agricultor getAgricultor() {
		return agricultor;
	}

	public void setAgricultor(Agricultor agricultor) {
		this.agricultor = agricultor;
	}

	public AreaGeorreferenciada getAreaGeorreferenciada() {
		return areaGeorreferenciada;
	}

	public void setAreaGeorreferenciada(AreaGeorreferenciada areaGeorreferenciada) {
		this.areaGeorreferenciada = areaGeorreferenciada;
	}

}
