package br.gov.ce.sda.domain.mobile;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter @Setter
@Table(schema = "portalmobile", name = "subsistema")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = SubsistemasProducao.class)
public class SubsistemasProducao {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "subsistema_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "atividade_produtiva")
	private AtividadeProdutiva atividadeProdutiva;

	@Column(name = "subsistema_descricao")
    private String subsistema;

	@Column(name = "area")
    private Double area;

	@Column(name = "rebanho")
    private Integer rebanho;

	@Column(name = "situacao_fundiaria")
    private String situacaoFundiaria;

	@Column(name = "area_irrigada")
    private Double areaIrrigada;

	@Column(name = "agua_nascente")
    private Boolean aguaNascente;

	@Column(name = "agua_poco")
    private Boolean aguaPoco;

	@Column(name = "agua_cacimba")
    private Boolean aguaCacimba;

	@Column(name = "agua_acude")
    private Boolean aguaAcude;

	@Column(name = "agua_rio")
    private Boolean aguaRio;

	@Column(name = "agua_cisterna")
    private Boolean aguaCisterna;

	@Column(name = "agua_canal")
    private Boolean aguaCanal;

	@Column(name = "agua_reuso")
    private Boolean aguaReuso;

	@Column(name = "agua_outro")
    private Boolean aguaOutro;

    @OneToMany(fetch = FetchType.LAZY, mappedBy="subsistemasProducao", cascade = CascadeType.ALL)
	private Set<TipoIrrigacao> tiposIrrigacao;

	@OneToMany(fetch = FetchType.LAZY, mappedBy="subsistemasProducao", cascade = CascadeType.ALL)
    private Set<ProdutoSubsistemas> produtosSubsistemas;

    @OneToMany(fetch = FetchType.LAZY, mappedBy="subsistema", cascade = CascadeType.ALL)
    private Set<TecnologiaSubsistema> tecnologiaSubsistema;

}
