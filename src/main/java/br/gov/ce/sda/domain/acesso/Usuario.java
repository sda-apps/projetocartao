package br.gov.ce.sda.domain.acesso;

import br.gov.ce.sda.domain.Agricultor;
import br.gov.ce.sda.domain.UsuarioAvancado;
import com.fasterxml.jackson.annotation.*;
import com.querydsl.core.annotations.QueryInit;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;

@Entity
@Table(name = "usuario")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Getter @Setter
@JsonIgnoreProperties(ignoreUnknown=true)
public class Usuario extends AbstractAuditingEntity implements Serializable, UserDetails {

	private static final long serialVersionUID = 1L;
    public static final String USUARIO_TIPO_ATER = "A";

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "usuario_id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "agricultor")
	private Agricultor agricultor;

    @ManyToOne(fetch = FetchType.EAGER, cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name = "usuario_avancado")
    @QueryInit("empresa")
	private UsuarioAvancado usuarioAvancado;

    @NotNull
    @Size(min = 1, max = 50)
    @Column(length = 50, unique = true, nullable = false)
    private String login;

    @Column(name="primeiro_acesso")
    private boolean primeiroAcesso;

    @JsonIgnore
    @NotNull
    @Size(min = 6, max = 60)
    @Column(name = "senha",length = 60)
    private String senha;

    @Size(max = 20)
    @Column(name = "chave_ativacao", length = 20)
    @JsonIgnore
    private String chaveAtivacao;

    @Size(max = 20)
    @Column(name = "chave_reativacao", length = 20)
    private String chaveReativacao;

    @JsonIgnore
    @Column(name = "reset_date", nullable = true)
    private LocalDateTime resetDate = null;

    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(
        name = "autoridade_usuario",
        joinColumns = {@JoinColumn(name = "usuario", referencedColumnName = "usuario_id")},
        inverseJoinColumns = {@JoinColumn(name = "autoridade", referencedColumnName = "nome")})
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Autoridade> autoridades = new HashSet<>();

    @Column(name = "status")
    private String status;

    @Column(name = "tipo")
    private String tipo;

    public boolean hasAuthorities(String ... authorities) {
        return getAuthoritiesList().containsAll(newArrayList(authorities));
    }

    public void setStatus(Status status) {
        this.status = status.getId();
    }

    public Status getStatus() {
        return Status.getById(this.status);
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    public enum Status {

        ATIVADO("A","Ativado"), INATIVO("I", "Inativo"), BLOQUEADO("B", "Bloqueado"), EM_ANALISE("E","Em Análise"), VAZIO("", "");

        private String id;
        private String descricao;

        Status(String id, String descricao) {
            this.id = id;
            this.descricao = descricao;
        }

        public String getId() {
            return id;
        }

        public String getDescricao() {
            return descricao;
        }

        @JsonCreator
        public static Status forValue(String id) {
            return getById(id);
        }

        @JsonValue
        public String toValue() {
            return this.id;
        }

        public static Status getById(String id) {
            for(Status e : values()) {
                if(e.id.equals(id)) return e;
            }
            return Status.VAZIO;
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Usuario user = (Usuario) o;

        if (!login.equals(user.login)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return login.hashCode();
    }

    @Override
    public String toString() {
        return "User{" +
            "login='" + login + '\'' +
            ", chaveAtivacao='" + chaveAtivacao + '\'' +
            "}";
    }

    @JsonIgnore
	@Override
	public Collection<Autoridade> getAuthorities() {
		return this.autoridades;
	}

    @JsonIgnore
    public List<String> getAuthoritiesList() {
        return this.autoridades.stream().map(autoridade -> new String(autoridade.getNome())).collect(Collectors.toList());
    }

    @JsonIgnore
	@Override
	public String getPassword() {
		return this.senha;
	}

	@JsonIgnore
	@Override
	public String getUsername() {
		return this.login;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
        if (Objects.nonNull(this.status)) {
            return this.getStatus().equals(Status.ATIVADO);
        } else {
            return false;
        }
	}

}
