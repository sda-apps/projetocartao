package br.gov.ce.sda.domain.mobile;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import br.gov.ce.sda.domain.Agricultor;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(schema = "portalmobile", name = "beneficio_social")
@Getter @Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = BeneficioSocial.class)
public class BeneficioSocial {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "beneficio_social_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "agricultor")
    private AgricultorMobile agricultor;

	@Column(name = "tipo")
    private String tipo;

	@Column(name = "descricao")
    private String descricao;

}
