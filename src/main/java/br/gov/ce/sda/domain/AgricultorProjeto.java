package br.gov.ce.sda.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.gov.ce.sda.domain.programaseprojeto.Projeto;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name = "agricultor_projeto")
public class AgricultorProjeto {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "agricultor_projeto_id")
    private Long id;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "agricultor")
	private Agricultor agricultor;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "projeto")
	private Projeto projeto;

	@Column(name = "status")
	private Boolean status;

	@Column(name = "ano")
	private Integer ano;

	@Column(name = "valor_total")
	private Double valorTotal;

}
