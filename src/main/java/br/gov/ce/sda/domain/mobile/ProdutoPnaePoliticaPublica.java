package br.gov.ce.sda.domain.mobile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.gov.ce.sda.domain.programaseprojeto.ProdutoEspecificacao;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(schema = "portalmobile", name = "produto_pnae_politica_publica")
@Getter @Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ProdutoPnaePoliticaPublica.class)
public class ProdutoPnaePoliticaPublica {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "produto_pnae_politica_publica_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "politicas_publicas")
    private PoliticasPublicas politicasPublicas;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "produto_especificacao")
    private ProdutoEspecificacao produtoEspecificacao;

}
