package br.gov.ce.sda.domain.mobile;

import br.gov.ce.sda.domain.programaseprojeto.ProdutoEspecificacao;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(schema = "portalmobile", name = "artesanato")
@Getter
@Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Artesanato.class)
public class Artesanato {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
    @SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
    @Column(name = "artesanato_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "atividade_produtiva")
    private AtividadeProdutiva atividadeProdutiva;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "produto_especificacao")
    private ProdutoEspecificacao produtoEspecificacao;

    @Column(name = "valor_unitario")
    private Double valorUnitario;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "artesanato", cascade = CascadeType.ALL)
    private Set<FormaComercializacao> formasComercializacoes;

    @Column(name = "materia_prima")
    private String materiaPrima;

}
