package br.gov.ce.sda.domain.ater;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(schema = "gestaoater", name = "meta_atividades_pta")
@Getter @Setter
public class MetaAtividadesPTA {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "meta_atividades_pta_id")
    private Long id;

    @JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plano_trabalho_anual")
    private PlanoTrabalhoAnual planoTrabalhoAnual;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "atividade")
    private AtividadePTA atividade;

	@Column(name = "quantidade_meta")
    private Long quantidadeMeta;

	@OneToMany(fetch = FetchType.LAZY, mappedBy="metaAtividadesPTA", cascade = CascadeType.ALL)
    private Set<MetaAtividadesTrimestral> metasAtividadesTrimestral;

    @Column(name = "nao_planejada")
    private Boolean naoPlanejada;

}
