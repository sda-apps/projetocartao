package br.gov.ce.sda.domain.ater;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import br.gov.ce.sda.domain.Localidade;
import br.gov.ce.sda.domain.Municipio;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(schema = "gestaoater", name = "meta_familias_pta")
@Getter @Setter
public class MetaFamiliasPTA {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "meta_familias_pta_id")
    private Long id;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plano_trabalho_anual")
    private PlanoTrabalhoAnual planoTrabalhoAnual;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "municipio")
    private Municipio municipio;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "comunidade")
    private Comunidade comunidade;

	@Column(name = "quantidade_familias")
    private Long quantidadeFamilias;

}
