package br.gov.ce.sda.domain.mobile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(schema = "portalmobile", name = "habitacao_seneamento_energia")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = HabitacaoSeneamentoEnergia.class)
public class HabitacaoSeneamentoEnergia {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "habitacao_seneamento_energia_id")
    private Long id;

	@Column(name = "tipo_habitacao")
    private String tipoHabitacao;

	@Column(name = "situacao_moradia")
    private String situacaoMoradia;

	@Column(name = "existe_banheiro")
    private Boolean existeBanheiro;

	@Column(name = "tipo_esgotamento_sanitario")
    private String tipoEsgotamentoSanitario;

	@Column(name = "existe_energia_eletrica")
    private Boolean existeEnergiaEletrica;

	@Column(name = "energia_monofasica")
	private Boolean energiaMonofasica;

	@Column(name = "energia_trifasica")
    private Boolean energiaTrifasica;

	@Column(name = "abastecimento_rede_municipal")
	private Boolean abastecimentoRedeMunicipal;

	@Column(name = "abastecimento_sistema_simplificado")
	private Boolean abastecimentoSistemaSimplificado;

	@Column(name = "abastecimento_cisterna")
    private Boolean abastecimentoCisterna;

	@Column(name = "abastecimento_poco_artesiano")
    private Boolean abastecimentoPocoArtesiano;

	@Column(name = "abastecimento_cacimba")
    private Boolean abastecimentoCacimba;

	@Column(name = "abastecimento_acude")
    private Boolean abastecimentoAcude;

	@Column(name = "abastecimento_barreiro")
    private Boolean abastecimentoBarreiro;

	@Column(name = "abastecimento_lagoa")
    private Boolean abastecimentoLagoa;

	@Column(name = "abastecimento_rio")
    private Boolean abastecimentoRio;

	@Column(name = "abastecimento_riacho")
    private Boolean abastecimentoRiacho;

	@Column(name = "abastecimento_tanque_pedra")
    private Boolean abastecimentoTanquePedra;

	@Column(name = "abastecimento_cacimbao")
    private Boolean abastecimentoCacimbao;

	@Column(name = "abastecimento_nascente")
    private Boolean abastecimentoNascente;

	@Column(name = "usa_carro_pipa")
	private Boolean usaCarroPipa;

	@Column(name = "lixo_coleta")
	private Boolean lixoColeta;

	@Column(name = "lixo_queima")
	private Boolean lixoQueima;

	@Column(name = "lixo_enterra")
	private Boolean lixoEnterra;

	@Column(name = "lixo_recicla")
	private Boolean lixoRecicla;

	@Column(name = "lixo_ceu_aberto")
	private Boolean lixoCeuAberto;

	@Column(name = "lixo_compotagem")
	private Boolean lixoCompotagem;

	@Column(name = "cozinha_carvao")
	private Boolean cozinhaCarvao;

	@Column(name = "cozinha_lenha")
	private Boolean cozinhaLenha;

	@Column(name = "cozinha_gas")
	private Boolean cozinhaGas;

	@Column(name = "cozinha_querosene")
	private Boolean cozinhaQuerosene;

	@Column(name = "cozinha_biogas")
	private Boolean cozinhaBiogas;

	@Column(name = "usa_energia_alternativa")
	private Boolean usaEnergiaAlternativa;

    @Column(name = "qual_energia_alternativa")
    private String qualEnergiaAlternativa;

}
