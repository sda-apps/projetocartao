package br.gov.ce.sda.domain.sda;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDetailSDA implements UserDetails {

    @JsonProperty(value = "user")
    private UserSDA user;

    @JsonProperty(access = WRITE_ONLY)
    private String username;

    @JsonProperty(access = WRITE_ONLY)
    private String password;

    @JsonProperty(value = "token", access = WRITE_ONLY)
    private String token;

    @JsonProperty(value = "sistemas", access = WRITE_ONLY)
    private List<SistemaSDA> sistemas;

    @Override public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.emptyList();
    }

    @Override public String getPassword() {
        return password;
    }

    @Override public String getUsername() {
        return username;
    }

    @Override public boolean isAccountNonExpired() {
        return true;
    }

    @Override public boolean isAccountNonLocked() {
        return true;
    }

    @Override public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override public boolean isEnabled() {
        return true;
    }
}
