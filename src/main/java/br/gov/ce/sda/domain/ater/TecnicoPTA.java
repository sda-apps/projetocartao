package br.gov.ce.sda.domain.ater;

import br.gov.ce.sda.domain.UsuarioAvancado;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(schema = "gestaoater", name = "pta_tecnicos")
@Getter @Setter
public class TecnicoPTA {

    public enum Perfil {

        ASSESSOR_PRODUTIVO("P"), ASSESSOR_SOCIAL("S"), TECNICO_CAMPO("C");

        @Getter
        private String perfil;

        Perfil(String perfil) {
            this.perfil = perfil;
        }
    }

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "soid")
	@SequenceGenerator(name = "soid", sequenceName = "soid", allocationSize = 1)
	@Column(name = "pta_tecnicos_id")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plano_trabalho_anual")
    private PlanoTrabalhoAnual planoTrabalhoAnual;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tecnico")
    private UsuarioAvancado tecnico;

    @Column(name = "perfil")
    private String perfil;

}
