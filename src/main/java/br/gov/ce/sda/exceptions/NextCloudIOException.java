package br.gov.ce.sda.exceptions;

import br.gov.ce.sda.Messages;

import static br.gov.ce.sda.MessageKey.NEXT_CLOUD_IO_EXCEPTION;

public class NextCloudIOException extends StorageException {
    public NextCloudIOException() {
        super(Messages.getString(NEXT_CLOUD_IO_EXCEPTION));
    }
}
