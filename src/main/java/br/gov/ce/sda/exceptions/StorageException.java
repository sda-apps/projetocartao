package br.gov.ce.sda.exceptions;

public class StorageException extends Exception {

    public StorageException(String message) {
        super(message);
    }

}
