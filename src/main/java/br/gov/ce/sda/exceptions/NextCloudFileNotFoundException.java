package br.gov.ce.sda.exceptions;


import br.gov.ce.sda.Messages;

import static br.gov.ce.sda.MessageKey.NEXT_CLOUD_FILE_NOT_FOUND;

public class NextCloudFileNotFoundException extends StorageException {
    public NextCloudFileNotFoundException() {
        super(Messages.getString(NEXT_CLOUD_FILE_NOT_FOUND));
    }
}
