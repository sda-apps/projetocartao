package br.gov.ce.sda.web.rest.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class FeedbackDTO {
	
	private String message;
	
	private Integer rating;
	
	private String type;
	
	private String userName;
	
	private String email;
	
	private String informacoes;

}
