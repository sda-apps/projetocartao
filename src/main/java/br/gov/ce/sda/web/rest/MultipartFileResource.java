package br.gov.ce.sda.web.rest;

import br.gov.ce.sda.exceptions.NextCloudIOException;
import br.gov.ce.sda.exceptions.StorageException;
import br.gov.ce.sda.service.nextcloud.impl.NextCloudStorageService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import java.io.IOException;

import static java.util.Objects.nonNull;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping(value = "/api/files")
public class MultipartFileResource {

    @Inject
    private NextCloudStorageService storageServiceNextCloud;

    @PostMapping
    public ResponseEntity uploadFileNextCloud(@RequestParam("bucketName") String bucketName,
                                              @RequestParam("file") MultipartFile multipartFile) {
        storageServiceNextCloud.saveFile(bucketName, multipartFile);
        return ok().build();
    }

    @GetMapping
    public ResponseEntity downloadFileNextCloud(@RequestParam("bucketName") String bucketName,
                                                @RequestParam(value = "fileName", required = false) String fileName) throws StorageException {

        if (nonNull(fileName)) {
            return storageServiceNextCloud.getFile(bucketName, fileName)
                .map(ResponseEntity::ok)
                .orElseThrow(NextCloudIOException::new);
        } else {
            return storageServiceNextCloud.getListURLFiles(bucketName)
                .map(ResponseEntity::ok)
                .orElseThrow(NextCloudIOException::new);
        }

    }
}
