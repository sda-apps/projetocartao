package br.gov.ce.sda.web.rest.dto.ater.filtros;

import br.gov.ce.sda.domain.ater.*;
import com.querydsl.core.BooleanBuilder;
import lombok.*;

import java.util.List;

import static java.util.Objects.nonNull;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FiltroPlanoTrimestralDTO {

    private String status;

    private List<Long> regiao;

    private List<Long> entidade;

    private List<Long> contrato;

    private Integer start;

    private Integer limit;

    private Integer paginacao;

    private Regiao regiaoUsuario;

    public BooleanBuilder getPredicatesPlanoTrimestralERP(QViewPlanosTrimestraisERP qViewPlanosTrimestraisERP) {
        BooleanBuilder predicates = new BooleanBuilder();

        if (nonNull(getStatus())) {
            predicates.and(qViewPlanosTrimestraisERP.status.eq(getStatus()));
        }

        if(nonNull(getRegiao())) {
            predicates.and(qViewPlanosTrimestraisERP.regiaoId.in(getRegiao()));
        }

        if(nonNull(getEntidade())) {
            predicates.and(qViewPlanosTrimestraisERP.empresaId.in(getEntidade()));
        }

        if(nonNull(getContrato())) {
            predicates.and(qViewPlanosTrimestraisERP.contratoId.in(getContrato()));
        }

        if(nonNull(getRegiaoUsuario())) {
            predicates.and(qViewPlanosTrimestraisERP.regiaoId.eq(regiaoUsuario.getId()));
        }

        return predicates;
    }

    public BooleanBuilder getPredicatesPlanoTrimestralUGP(QViewPlanosTrimestraisUGP qViewPlanosTrimestraisUGP) {
        BooleanBuilder predicates = new BooleanBuilder();

        if (nonNull(getStatus())) {
            predicates.and(qViewPlanosTrimestraisUGP.status.eq(getStatus()));
        }

        if(nonNull(getRegiao())) {
            predicates.and(qViewPlanosTrimestraisUGP.regiaoId.in(getRegiao()));
        }

        if(nonNull(getEntidade())) {
            predicates.and(qViewPlanosTrimestraisUGP.empresaId.in(getEntidade()));
        }

        if(nonNull(getContrato())) {
            predicates.and(qViewPlanosTrimestraisUGP.contratoId.in(getContrato()));
        }

        return predicates;
    }

}
