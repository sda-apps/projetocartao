package br.gov.ce.sda.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ce.sda.domain.mapa.Elemento;
import br.gov.ce.sda.repository.CustomElementoRepository;
import br.gov.ce.sda.web.rest.dto.FiltroAgricultorDTO;

@RestController
@RequestMapping("/api")
public class ElementoResource {

	@Inject
	private CustomElementoRepository customElementoRepository;
	
	@RequestMapping(value = "/elementos/coordenadas/filtro", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Elemento>> buscarElementosCoordenadasPorFiltro(FiltroAgricultorDTO filtro) throws URISyntaxException {
		
		List<Elemento> elementos = customElementoRepository.buscarElementosCoordenadasPorFiltro(filtro);
		
		if(elementos.size()==0){
			return new ResponseEntity<>(elementos, HttpStatus.NOT_FOUND);
		}
		
        return new ResponseEntity<>(elementos, HttpStatus.OK);
        
	}
	
	@RequestMapping(value = "/elemento/agricultor/{elementoId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Elemento> buscarElementoPorIdComAgricultor(@PathVariable Long elementoId) throws URISyntaxException {
		
		Elemento elemento = customElementoRepository.buscarElementoPorIdComAgricultor(elementoId);
		
        return new ResponseEntity<>(elemento, HttpStatus.OK);
        
	}

}