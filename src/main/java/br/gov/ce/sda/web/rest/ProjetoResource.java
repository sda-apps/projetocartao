package br.gov.ce.sda.web.rest;

import java.util.List;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ce.sda.domain.AgricultorProjeto;
import br.gov.ce.sda.domain.programaseprojeto.Nota;
import br.gov.ce.sda.repository.CustomProjetoRepository;

@RestController
@RequestMapping("/api")
public class ProjetoResource {

	@Inject
	private CustomProjetoRepository customProjetoRepository;
	
	@RequestMapping(value = "/agricultor/notas-projetos/{agricultorId}/{projetoCodigoSDA}",
			method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Nota>> getNotasProjetos(@PathVariable Long agricultorId, @PathVariable Long projetoCodigoSDA) {

		List <Nota> nota = customProjetoRepository.buscarNotasProjetos(agricultorId, projetoCodigoSDA);

		return new ResponseEntity<>(nota, nota.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
	}
	
	@RequestMapping(value = "/projeto/agricultor-projeto/{agricultorId}/{projetoCodigoSDA}",
			method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List <AgricultorProjeto>> getAgricultorProjetoPorAgricultorEProjeto(@PathVariable Long agricultorId,  @PathVariable Long projetoCodigoSDA) {

		List <AgricultorProjeto> agricultorProjeto = customProjetoRepository.buscarProjetosPorAgricultorEProjeto(agricultorId, projetoCodigoSDA);
		
		return new ResponseEntity<>(agricultorProjeto, agricultorProjeto.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
	}

}
