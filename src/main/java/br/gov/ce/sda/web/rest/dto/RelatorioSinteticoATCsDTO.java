package br.gov.ce.sda.web.rest.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RelatorioSinteticoATCsDTO {

    private Long id;

    private String regiao;

    private String contrato;

    private String nomeFantasia;

    private Long qtdMunicipio;

    private Long qtdComunidade;

    private Long qtdFamilias;

    private int qtdTecnicos;

    private int qtdAssessor;



    public RelatorioSinteticoATCsDTO() {}

    public RelatorioSinteticoATCsDTO(Long id, String regiao, String contrato, String nomeFantasia, Long qtdMunicipio, Long qtdComunidade, Long qtdFamilias) {
        this.id = id;
        this.regiao = regiao;
        this.contrato = contrato;
        this.nomeFantasia = nomeFantasia;
        this.qtdMunicipio = qtdMunicipio;
        this.qtdComunidade = qtdComunidade;
        this.qtdFamilias = qtdFamilias;
    }

}
