package br.gov.ce.sda.web.rest.dto;

import br.gov.ce.sda.domain.ater.Tarefa;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

import static java.util.Objects.nonNull;

@Getter
@Setter
@NoArgsConstructor
public class TarefaDTO {

    private List<Tarefa> tarefas;

    private Long total;

    private Long id;

    private String descricao;

    private String codigo;

    private String trimestre;

    private String tecnico;

    private String empresa;

    private String agricultor;

    private String foto;

    private String municipio;

    private String comunidade;

    private Date dataRealizacao;

    private String uf;

    private String status;

    private int mes;

    private Boolean recusado;

    private String tipo;

    private String comunidadeAgricultor;

    //private String comunidadeParticipante;

    private String abrangencia;

    public TarefaDTO(Tarefa tarefa) {
        this.id = tarefa.getId();
        this.descricao = tarefa.getAtividade().getDescricao();
        if(nonNull(tarefa.getPlanoTrimestral())) {
            this.codigo = tarefa.getPlanoTrimestral().getPlanoTrabalhoAnual().getCodigo();
            this.trimestre = tarefa.getPlanoTrimestral().getTrimestre().toString();
            this.mes = tarefa.getPlanoTrimestral().getPrimeiroMes().getMonthValue();
        }

        this.tecnico = tarefa.getUsuario().getUsuarioAvancado().getPessoaFisica().getPessoa().getNome();
        if(nonNull(tarefa.getUsuario().getUsuarioAvancado().getEmpresa())) {
            this.empresa = tarefa.getUsuario().getUsuarioAvancado().getEmpresa().getPessoaJuridica().getNomeFantasia();
        }

        this.municipio = tarefa.getMunicipio().getNome();
        if (nonNull(tarefa.getComunidade())) {
            this.comunidade = tarefa.getComunidade().getNome();
        }
        this.uf = tarefa.getMunicipio().getUf().getSigla();
        this.status = tarefa.getStatus();
        this.recusado = tarefa.getRecusado();


        this.mes = tarefa.getPlanoTrimestral().getPrimeiroMes().getMonthValue();
        this.tipo = tarefa.getTipo();
        this.abrangencia = tarefa.getAbrangencia();
        if(nonNull(tarefa.getAgricultor())) {
            this.agricultor = tarefa.getAgricultor().getPessoaFisica().getPessoa().getNome();
            this.foto = tarefa.getAgricultor().getPessoaFisica().getPessoa().getFoto();
            //this.comunidadeParticipante = tarefa.getComunidadeParticipante().getNome();
            this.comunidadeAgricultor = tarefa.getAgricultor().getPessoaFisica().getPessoa().getComunidade().getNome();
        } else {
            this.agricultor = "N/I";
            this.comunidadeAgricultor = "N/I";
        }

        this.dataRealizacao = tarefa.getDataRealizacao();
    }

}
