package br.gov.ce.sda.web.rest.mobile;

import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.domain.mobile.HistoricoAvaliacao;
import br.gov.ce.sda.repository.mobile.CustomHistoricoAvaliacaoRepository;
import br.gov.ce.sda.repository.mobile.HistoricoAvaliacaoRepository;
import br.gov.ce.sda.config.security.SecurityUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/api/mobile/historico-avaliacao")
public class HistoricoAvaliacaoResource {

    @Inject
    private HistoricoAvaliacaoRepository historicoAvaliacaoRepository;

    @Inject
    private CustomHistoricoAvaliacaoRepository customHistoricoAvaliacaoRepository;

    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> save(@RequestBody HistoricoAvaliacao historicoAvaliacao) {
        Usuario usuario = SecurityUtils.getCurrentUser();

        historicoAvaliacao.setUsuario(usuario);

        HistoricoAvaliacao historico = historicoAvaliacaoRepository.save(historicoAvaliacao);

        historico = customHistoricoAvaliacaoRepository.findById(historico.getId());

        return new ResponseEntity<>(historico, HttpStatus.OK);
    }

}
