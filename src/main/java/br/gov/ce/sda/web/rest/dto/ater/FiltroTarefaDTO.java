package br.gov.ce.sda.web.rest.dto.ater;

import br.gov.ce.sda.domain.ater.QTarefa;
import com.querydsl.core.BooleanBuilder;
import lombok.*;

import java.util.List;

import static java.util.Objects.nonNull;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FiltroTarefaDTO {

    private List<Long> planoTrabalhoAnual;

    private List<Long> planoTrimestral;

    private List<Long> atividade;

    private List<Long> municipio;

    private List<Long> parceira;

    private List<Long> comunidade;

    private List<Long> tecnico;

    private String agricultor;

    private String status;

    private Integer start;

    private Integer limit;

    private Integer paginacao;

    private Long total;

    private Boolean ordenar;

    private String ordem;


    public BooleanBuilder getPredicates(QTarefa tarefa) {
        BooleanBuilder predicates = new BooleanBuilder();

        if (nonNull(getStatus())) {
            if (getStatus().equals("R")) {
                predicates.and(tarefa.recusado.isTrue());
            } else {
                predicates.and(tarefa.status.eq(getStatus()));
            }
        }

        if (nonNull(getPlanoTrabalhoAnual())) {
            predicates.and(tarefa.planoTrimestral.planoTrabalhoAnual.id.in(getPlanoTrabalhoAnual()));
        }

        if (nonNull(getPlanoTrimestral())) {
            predicates.and(tarefa.planoTrimestral.id.in(getPlanoTrimestral()));
        }

        if (nonNull(getAtividade())) {
            predicates.and(tarefa.atividade.id.in(getAtividade()));
        }

        if(nonNull(getParceira())) {
            predicates.and(tarefa.usuario.usuarioAvancado.empresa.id.in(getParceira()));
        }

        if (nonNull(getMunicipio())) {
            predicates.and(tarefa.municipio.id.in(getMunicipio()));
        }

        if (nonNull(getComunidade())) {
            predicates.and(tarefa.comunidade.id.in(getComunidade()));
        }

        if (nonNull(getTecnico())) {
            predicates.and(tarefa.usuario.usuarioAvancado.id.in(getTecnico()));
        }

        if (nonNull(getAgricultor())) {
            predicates.and(tarefa.agricultor.pessoaFisica.pessoa.nome.containsIgnoreCase(getAgricultor()));
        }

        return predicates;
    }

}
