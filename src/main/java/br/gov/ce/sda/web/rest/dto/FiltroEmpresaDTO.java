package br.gov.ce.sda.web.rest.dto;

import static java.util.Objects.nonNull;

import java.util.Calendar;
import java.util.List;

import br.gov.ce.sda.domain.QPessoa;
import br.gov.ce.sda.domain.QPessoaJuridica;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQuery;

import br.gov.ce.sda.domain.QEmpresa;
import br.gov.ce.sda.domain.ater.QAbrangenciaContrato;
import br.gov.ce.sda.domain.ater.QContrato;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class FiltroEmpresaDTO {

	private List<Long> regiao;

	private List<Long> municipio;

	private String empresa;

    private Integer start;

    private Integer limit;

    private Integer paginacao;

    private Long total;

    private Boolean ordenar;

    private String ordem;

    public BooleanBuilder getPredicates(QPessoaJuridica pessoaJuridica, QContrato contrato) {
        BooleanBuilder predicates = new BooleanBuilder();
        if(nonNull(getEmpresa())) {
            predicates.and(pessoaJuridica.pessoa.nome.containsIgnoreCase(getEmpresa())
                .or(pessoaJuridica.nomeFantasia.containsIgnoreCase(getEmpresa())));
        }
        if(nonNull(getRegiao())) {
            int year = Calendar.getInstance().get(Calendar.YEAR);

            predicates.and(contrato.regiao.id.in(getRegiao()));
            predicates.and(contrato.dataAssinatura.year().loe(year));
        }

        return predicates;
    }

    public void setSubquery(JPAQuery<?> query, QContrato contrato) {
    	if(nonNull(getMunicipio())) {
    		QAbrangenciaContrato abrangenciaContrato = QAbrangenciaContrato.abrangenciaContrato;

    		for (Long municipioId : getMunicipio()) {
    			query.where(contrato.id.in(JPAExpressions.select(abrangenciaContrato.contrato.id)
    				.from(abrangenciaContrato)
					.where(abrangenciaContrato.municipio.id.eq(municipioId))
				));
			}
    	}

    }

}
