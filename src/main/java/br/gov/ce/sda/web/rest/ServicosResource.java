package br.gov.ce.sda.web.rest;

import br.gov.ce.sda.service.ServicosService;
import br.gov.ce.sda.web.rest.dto.FiltroRelatorioAnaliticoParceiraDTO;
import br.gov.ce.sda.web.rest.dto.FiltroRelatorioCadastroAgricultorDTO;
import br.gov.ce.sda.web.rest.dto.FiltroRelatorioCadastroPerfilFamilia;
import br.gov.ce.sda.web.rest.dto.FiltroRelatorioTrimestralAtividades;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/servicos")
public class ServicosResource {

    @Inject
    private ServicosService servicosService;

    @GetMapping(value = "/extrato/horadeplantar/{agricultorId}", produces = APPLICATION_JSON_VALUE)
    public void emitirExtratoHoraPlantar(@PathVariable Long agricultorId, HttpServletResponse response) throws Exception {
        JasperPrint jasperPrint = servicosService.gerarExtratoHoraPlantar(agricultorId);

        adicionarRelatorioAoResponse(jasperPrint, response);
    }

    @GetMapping(value = "/extrato/pnae/{agricultorId}", produces = APPLICATION_JSON_VALUE)
    public void emitirExtratoPNAE(@PathVariable Long agricultorId, HttpServletResponse response) throws Exception {
        JasperPrint jasperPrint = servicosService.gerarExtratoPNAE(agricultorId);

        adicionarRelatorioAoResponse(jasperPrint, response);
    }

    @GetMapping(value = "/extrato/paa-leite/{agricultorId}", produces = APPLICATION_JSON_VALUE)
    public void emitirExtratoPAALeite(@PathVariable Long agricultorId, HttpServletResponse response) throws Exception {
        JasperPrint jasperPrint = servicosService.gerarExtratoPAALeite(agricultorId);

        adicionarRelatorioAoResponse(jasperPrint, response);
    }

    @GetMapping(value = "/extrato/paa-alimentos/{agricultorId}", produces = APPLICATION_JSON_VALUE)
    public void emitirExtratoPAAAlimentos(@PathVariable Long agricultorId, HttpServletResponse response) throws Exception {
        JasperPrint jasperPrint = servicosService.gerarExtratoPAAAlimentos(agricultorId);

        adicionarRelatorioAoResponse(jasperPrint, response);
    }

    @GetMapping(value = "/relatorio/cadastro-agricultores", produces = APPLICATION_JSON_VALUE)
    public void emitirRelatorioCadastroAgricultores(FiltroRelatorioCadastroAgricultorDTO filtro, HttpServletResponse response) throws Exception {
        JasperPrint jasperPrint = servicosService.gerarRelatorioCadastroAgricultores(filtro);

        adicionarRelatorioAoResponse(jasperPrint, response);
    }

    @GetMapping(value = "/relatorio/cadastro-agricultores/xls", produces = APPLICATION_JSON_VALUE)
    public void emitirXlsCadastroAgricultores(FiltroRelatorioCadastroAgricultorDTO filtro, HttpServletResponse response) throws Exception {
        byte[] bytes = servicosService.gerarRelaorioXlsCadastroAgricultores(filtro);

        adicionarXlsAoResponse(bytes, response);
    }

    @GetMapping(value = "/relatorio/analitico-familias", produces = APPLICATION_JSON_VALUE)
    public void emitirRelatorioAnaliticoFamilias(FiltroRelatorioCadastroAgricultorDTO filtro, HttpServletResponse response) throws Exception {
        JasperPrint jasperPrint = servicosService.gerarRelatorioAnaliticoFamilias(filtro);

        adicionarRelatorioAoResponse(jasperPrint, response);
    }

    @GetMapping(value = "/relatorio/analitico-familias/xls", produces = APPLICATION_JSON_VALUE)
    public void emitirXlsAnaliticoFamilias(FiltroRelatorioCadastroAgricultorDTO filtro, HttpServletResponse response) throws Exception {
        byte[] bytes = servicosService.gerarRelaorioXlsAnaliticoFamilias(filtro);

        adicionarXlsAoResponse(bytes, response);
    }

    @GetMapping(value = "/relatorio/analitico-parceiras", produces = APPLICATION_JSON_VALUE)
    public void emitirRelatorioAnaliticoParceiras(FiltroRelatorioAnaliticoParceiraDTO filtro, HttpServletResponse response) throws Exception {
        JasperPrint jasperPrint = servicosService.gerarRelatorioAnaliticoParceiras(filtro);
        adicionarRelatorioAoResponse(jasperPrint, response);
    }

    @GetMapping(value = "/relatorio/analitico-parceiras/xls", produces = APPLICATION_JSON_VALUE)
    public void emitirXlsAnaliticoParceiras(FiltroRelatorioAnaliticoParceiraDTO filtro, HttpServletResponse response) throws Exception {
        byte[] bytes = servicosService.gerarRelatorioXlsAnaliticoParceiras(filtro);

        adicionarXlsAoResponse(bytes, response);
    }

    @GetMapping(value = "/relatorio/perfil-das-familias", produces = APPLICATION_JSON_VALUE)
    public void emitirRelatorioPerfilDasFamilias(FiltroRelatorioCadastroPerfilFamilia filtro, HttpServletResponse response) throws Exception {
        JasperPrint jasperPrint = servicosService.gerarRelatorioPerfilDasFamilias(filtro);

        adicionarRelatorioAoResponse(jasperPrint, response);
    }

    @GetMapping(value = "/relatorio/perfil-das-familias/grafico", produces = APPLICATION_JSON_VALUE)
    public void emitirRelatorioPerfilDasFamiliasGrafico(FiltroRelatorioCadastroPerfilFamilia filtro, HttpServletResponse response) throws Exception {
        JasperPrint jasperPrint = servicosService.gerarRelatorioPerfilDasFamiliasGrafico(filtro);

        adicionarRelatorioAoResponse(jasperPrint, response);
    }

    @GetMapping(value = "/relatorio/perfil-das-familias/xls", produces = APPLICATION_JSON_VALUE)
    public void emitirXlsPerfilDasFamilias(FiltroRelatorioCadastroPerfilFamilia filtro, HttpServletResponse response) throws Exception {
        byte[] bytes = servicosService.gerarRelatorioXlsPerfilDasFamilias(filtro);

        adicionarXlsAoResponse(bytes, response);
    }

    @GetMapping(value = "/relatorio/perfil-das-familias/grafico/xls", produces = APPLICATION_JSON_VALUE)
    public void emitirXlsPerfilDasFamiliasGrafico(FiltroRelatorioCadastroPerfilFamilia filtro, HttpServletResponse response) throws Exception {
        byte[] bytes = servicosService.gerarRelatorioXlsPerfilDasFamiliasGrafico(filtro);

        adicionarXlsAoResponse(bytes, response);
    }

    @GetMapping(value = "/relatorio/pta/{ptaId}", produces = APPLICATION_JSON_VALUE)
    public void emitirRelatorioPlanoTrabalhoAnual(@PathVariable Long ptaId, HttpServletResponse response) throws Exception {
        JasperPrint jasperPrint = servicosService.gerarRelatorioPlanoTrabalhoAnual(ptaId);

        adicionarRelatorioAoResponse(jasperPrint, response);
    }

    @GetMapping(value = "/relatorio/plano-trimestral/{planoTrimestralId}", produces = APPLICATION_JSON_VALUE)
    public void emitirRelatorioPlanoTrabalhoTrimestral(@PathVariable Long planoTrimestralId, HttpServletResponse response) throws Exception {
        JasperPrint jasperPrint = servicosService.gerarRelatorioPlanoTrabalhoTrimestral(planoTrimestralId);

        adicionarRelatorioAoResponse(jasperPrint, response);
    }


    @GetMapping(value = "/relatorio/trimestral-atividades/analitico", produces = APPLICATION_JSON_VALUE)
    public void emitirRelatorioTrimestralAtividadesAnalitico(FiltroRelatorioTrimestralAtividades filtro, HttpServletResponse response) throws Exception {
        JasperPrint jasperPrint = servicosService.gerarRelatorioTrimestralAtividadeAnalitico(filtro);
        adicionarRelatorioAoResponse(jasperPrint, response);
    }

    @GetMapping(value = "/relatorio/trimestral-atividades/sintetico", produces = APPLICATION_JSON_VALUE)
    public void emitirRelatorioTrimestralAtividadesSintetico(FiltroRelatorioTrimestralAtividades filtro, HttpServletResponse response) throws Exception {
        JasperPrint jasperPrint = servicosService.gerarRelatorioTrimestralAtividadesSintetico(filtro);
        adicionarRelatorioAoResponse(jasperPrint, response);
    }

    private void adicionarXlsAoResponse(byte[] bytes, HttpServletResponse response) throws IOException {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "inline; filename=report.xlsx");
        OutputStream outputStream = response.getOutputStream();
        outputStream.write(bytes);
        outputStream.flush();
        outputStream.close();
    }

    private void adicionarRelatorioAoResponse(JasperPrint jasperPrint, HttpServletResponse response) throws Exception {
        response.setContentType("application/x-pdf");
        response.setHeader("Content-disposition", "inline; filename=report.pdf");
        OutputStream outStream = response.getOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
        outStream.flush();
        outStream.close();
    }

}
