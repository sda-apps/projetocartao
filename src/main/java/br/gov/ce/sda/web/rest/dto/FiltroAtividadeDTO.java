package br.gov.ce.sda.web.rest.dto;

import br.gov.ce.sda.domain.ater.QAcao;
import br.gov.ce.sda.domain.ater.QAtividadePTA;
import com.querydsl.core.BooleanBuilder;
import lombok.*;

import java.util.List;

import static java.util.Objects.nonNull;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class FiltroAtividadeDTO {

	private List<Long> acao;

	private String atividade;

    private Integer start;

    private Integer limit;

    private Integer paginacao;

    private Long total;

    private Boolean ordenar;

    private String ordem;

    public BooleanBuilder getPredicates(QAtividadePTA atividade, QAcao acao) {
    	BooleanBuilder predicates = new BooleanBuilder();
    	if(nonNull(getAtividade())) {
    		predicates.and(atividade.nome.containsIgnoreCase(getAtividade()));
    	}

        if(nonNull(getAcao())){
            predicates.and(acao.id.in(getAcao()));
        }

    	return predicates;
    }


}
