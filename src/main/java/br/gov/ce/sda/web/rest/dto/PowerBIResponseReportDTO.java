package br.gov.ce.sda.web.rest.dto;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PowerBIResponseReportDTO {

    @SerializedName("@odata.context")
    private String context;

    @SerializedName("token")
    private String token;

    @SerializedName("tokenId")
    private String tokenId;

    @SerializedName("expiration")
    private String expiration;

}
