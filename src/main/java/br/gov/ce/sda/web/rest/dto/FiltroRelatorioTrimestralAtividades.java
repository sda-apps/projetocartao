package br.gov.ce.sda.web.rest.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class FiltroRelatorioTrimestralAtividades extends FiltroRelatorioCadastro {

    private String planoTrimestral;

    private String atc;

    private String ptaCodigo;

    private String ptTrimestre;

    private Long ptId;

}
