package br.gov.ce.sda.web.rest.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AtendimentoAssociacaoDTO {
	
	private Long idTecnico;

    private String tecnico;

    private String anoInicio;

    private String anoFim;
    
    private String empresa;
    
    private String foto;
    
    private String tipoATER;

    public AtendimentoAssociacaoDTO() {}

	public AtendimentoAssociacaoDTO(Long idTecnico,String tecnico, String anoInicio, String anoFim, String empresa, String foto, String tipoATER) {
		this.idTecnico = idTecnico;
		this.tecnico = tecnico;
		this.anoInicio = anoInicio;
		this.anoFim = anoFim;
		this.empresa = empresa;
		this.foto = foto;
		this.tipoATER = tipoATER;
	}
    
}
