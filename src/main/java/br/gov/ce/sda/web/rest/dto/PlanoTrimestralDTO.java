package br.gov.ce.sda.web.rest.dto;

import br.gov.ce.sda.domain.ater.MetaAtividadesTrimestral;
import br.gov.ce.sda.domain.ater.PlanoTrimestral;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter
public class PlanoTrimestralDTO {

	private Long id;

	private PlanoTrimestral planoTrimestral;

	private List<MetaAtividadesTrimestral> metasTrimestrais;

}
