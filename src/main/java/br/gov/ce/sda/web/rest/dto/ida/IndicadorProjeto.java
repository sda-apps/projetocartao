package br.gov.ce.sda.web.rest.dto.ida;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = IndicadorProjeto.class)
public class IndicadorProjeto {

    private Long id;
    private Set<IndicadorPlanejamentoExecucao> planejamentoExecucoes;
    private Set<IndicadorProjetoBeneficiario> projetoBeneficiarios;
    private String solicitante;
    private String solicitado;
    private Integer sistemaCodigo;
    private Long projetoCodigo;
    private String paisCodigo;
    private String ufCodigo;
    private String municipioCodigo;
    private String distritoCodigo;
    private String localidadeCodigo;
    private Integer tipoIndicador;
    private Long indicador;
    private Integer unidadeMedidaCodigo;
    private Integer ano;
    private Double demandadoQuantidade;
    private Double demandadoValor;
    private Double programadoQuantidade;
    private Double programadoValor;
    private Double executadoQuantidade;
    private Double executadoValor;
    private String localidadeComplemento;
    private String indicadorOutro;
    private String latitude;
    private String longitude;
    private String situacao;
    private String observacao;
    private Integer fonteInformacaoCodigo;
    private Integer prioridade;
    private String importaSimNao;
    private Integer importaCodigo;
    private Integer beneficiarioQuantidade;
    private String empresa;
    private Integer indicadorProjetoSituacaoCodigo;
    private Integer beneficiarioIndicador;
    private String publico;
    private Double demandadoValorUnitario;
    private Double programadoValorUnitario;
    private Double executadoValorUnitario;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", locale = "pt-BR", timezone = "Brazil/East")
    private LocalDateTime dataImportacao;

}
