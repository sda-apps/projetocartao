package br.gov.ce.sda.web.rest.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class FiltroRelatorioCadastroAgricultorDTO extends FiltroRelatorioCadastro {

    private String atc;

    private String localidade;

    private boolean parentes;

}
