package br.gov.ce.sda.web.rest.dto;

import br.gov.ce.sda.domain.ater.QEixoTematico;
import br.gov.ce.sda.domain.ater.QTema;
import com.querydsl.core.BooleanBuilder;
import lombok.*;

import java.util.List;

import static java.util.Objects.nonNull;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class FiltroTemaDTO {

	private String nome;

    private List<Long> eixoTematico;

    private Integer start;

    private Integer limit;

    private Integer paginacao;

    private Long total;

    private Boolean ordenar;

    private String ordem;

    public BooleanBuilder getPredicates(QTema tema) {
    	BooleanBuilder predicates = new BooleanBuilder();
    	if(nonNull(getNome())) {
    		predicates.and(tema.nome.containsIgnoreCase(getNome()));
    	}

        if(nonNull(getEixoTematico())) {
            predicates.and(tema.eixoTematico.id.in(getEixoTematico()));
        }
    	return predicates;
    }


}
