package br.gov.ce.sda.web.rest;

import br.gov.ce.sda.service.ResponseMessageService;
import br.gov.ce.sda.web.rest.dto.ResponseMessage;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ControllerExceptionHandler {

    private final ResponseMessageService responseMessageService;

    public ControllerExceptionHandler(ResponseMessageService responseMessageService) {
        this.responseMessageService = responseMessageService;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseMessage handleDataIntegrityViolation(DataIntegrityViolationException ex) {
        ResponseMessage errorMessage = responseMessageService.createErrorMessage(ex);
        return errorMessage;
    }
}
