package br.gov.ce.sda.web.rest.dto.mobile;

import static java.util.Objects.nonNull;

import java.util.List;

import br.gov.ce.sda.domain.mobile.QPessoaMobile;
import com.querydsl.core.BooleanBuilder;

import br.gov.ce.sda.domain.mobile.QAgricultorMobile;
import br.gov.ce.sda.domain.mobile.QAtividadeCadastro;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class FiltroCadastroDTO {

    private String agricultor;
	private String status;
	private List<Long> municipios;
	private List<Long> comunidades;
	private Long tecnico;
	private Integer start;
    private Integer limit;
    private Integer paginacao;
    private Long total;
    private Boolean recusado;

    public BooleanBuilder getPredicatesCadastro(QAtividadeCadastro atividadeCadastro) {
    	BooleanBuilder predicates = new BooleanBuilder();

    	if(nonNull(getStatus())) {

    	    predicates.and(atividadeCadastro.status.eq(getStatus()));

            if(recusado != null && recusado) {
                predicates.and(atividadeCadastro.recusado.isTrue());
            }
    	}

    	if(nonNull(getTecnico())) {
    		predicates.and(atividadeCadastro.tecnico.id.eq(getTecnico()));
    	}

    	return predicates;
    }

	public BooleanBuilder getPredicatesPessoa(QPessoaMobile pessoaMobile) {
    	BooleanBuilder predicates = new BooleanBuilder();

		if(nonNull(getMunicipios())) {
			predicates.and(pessoaMobile.municipio.id.in(getMunicipios()));
		}

		if(nonNull(getComunidades())) {
			predicates.and(pessoaMobile.comunidade.id.in(getComunidades()));
		}

        if(nonNull(getAgricultor())) {
            predicates.and(pessoaMobile.nome.containsIgnoreCase(getAgricultor()));
        }

    	return predicates;
    }

}
