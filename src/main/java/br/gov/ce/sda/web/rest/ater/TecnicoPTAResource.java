package br.gov.ce.sda.web.rest.ater;

import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.domain.ater.TecnicoPTA;
import br.gov.ce.sda.repository.AreaAtuacaoRepository;
import br.gov.ce.sda.repository.UserRepository;
import br.gov.ce.sda.repository.ater.CustomTecnicoPTARepository;
import br.gov.ce.sda.repository.ater.TecnicoPTARepository;
import br.gov.ce.sda.service.ater.TecnicoPTAService;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URISyntaxException;
import java.util.Set;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping(value = "/api/tecnicopta")
public class TecnicoPTAResource {

    @Inject
    private TecnicoPTAService tecnicoPTAService;

    @Inject
    private TecnicoPTARepository tecnicoPTARepository;

    @Inject
    private CustomTecnicoPTARepository cstomTecnicoPTARepository;

    @Inject
    private UserRepository userRepository;


    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<TecnicoPTA> save(@RequestBody TecnicoPTA tecnicoPTA) {
        if (nonNull(cstomTecnicoPTARepository.findByTecnicoIdAndPlanoTrabalhoAnualId(tecnicoPTA.getTecnico().getId(), tecnicoPTA.getPlanoTrabalhoAnual().getId()))) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        /*Usuario usuario = userRepository.findOneByUsuarioAvancado(tecnicoPTA.getTecnico()).get();

        if (usuario.getAuthoritiesList().contains("ROLE_RESPONSAVEL_ATER")) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }*/

        tecnicoPTA = tecnicoPTAService.save(tecnicoPTA);

        return ResponseEntity.ok().body(tecnicoPTA);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TecnicoPTA> update(@PathVariable Long id, @RequestBody TecnicoPTA tecnicoPTA) throws URISyntaxException {
        TecnicoPTA tecnicoAtual = tecnicoPTARepository.findById(id).get();

        if (isNull(tecnicoAtual)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        tecnicoPTAService.atualizarAreasTrabalhoTecnico(tecnicoPTA);

        BeanUtils.copyProperties(tecnicoPTA, tecnicoAtual);

        tecnicoPTAService.update(tecnicoAtual);

        return new ResponseEntity<>(tecnicoAtual, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = DELETE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(@PathVariable Long id) {
        tecnicoPTAService.delete(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/pta/{id}", method = GET, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity<Set<TecnicoPTA>> findByPlanoAnual(@PathVariable Long id) {
        Set<TecnicoPTA> tecnicos = cstomTecnicoPTARepository.findByPlanoAnual(id);

        return new ResponseEntity<>(tecnicos, tecnicos.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

}
