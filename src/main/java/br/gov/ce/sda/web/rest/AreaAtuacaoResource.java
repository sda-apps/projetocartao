package br.gov.ce.sda.web.rest;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ce.sda.domain.Municipio;
import br.gov.ce.sda.repository.CustomAreaAtuacaoRepository;
import br.gov.ce.sda.service.AreaAtuacaoService;
import br.gov.ce.sda.web.rest.dto.TecnicoAreaAtuacaoDTO;

@RestController
@RequestMapping(value = "/api/areasatuacao")
public class AreaAtuacaoResource {

	@Inject
	private CustomAreaAtuacaoRepository customAreaAtuacaoRepository;

	@Inject
	private AreaAtuacaoService areaAtuacaoService;

	@RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<Set<Municipio>> findAll(@RequestParam(value="usuarioAvancadoId") Long usuarioAvancadoId) {
		Set<Municipio> areas = customAreaAtuacaoRepository.findByUsuarioAvancadoId(usuarioAvancadoId);
		return new ResponseEntity<>(areas, areas.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
	}

	@RequestMapping(method = POST, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
	public ResponseEntity<?> associarAreasAtuacao(@RequestBody TecnicoAreaAtuacaoDTO dto) {
		areaAtuacaoService.associarNovasAreasAtuacao(dto);
		return ResponseEntity.ok().build();
	}

}
