package br.gov.ce.sda.web.rest.dto;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ResponseMessage {
	
	private List<String> messages;
	
	private HttpStatus status;
	
    public ResponseMessage() { }

    public ResponseMessage(List<String> messages) {
        this.messages = messages;
    }
    
    public ResponseMessage(HttpStatus status, List<String> messages) {
        this.messages = messages;
        this.status = status;
    }

    public ResponseMessage(HttpStatus status, String message) {
        this(status, Collections.singletonList(message));
    }
    
    public ResponseMessage(String message) {
        this(Collections.singletonList(message));
    }

    public ResponseMessage(HttpStatus status, String ... messages) {
        this(status, Arrays.asList(messages));
    }
    
    public ResponseMessage(String ... messages) {
        this(Arrays.asList(messages));
    }
	
}
