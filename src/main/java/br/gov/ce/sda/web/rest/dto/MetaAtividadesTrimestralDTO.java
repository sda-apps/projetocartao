package br.gov.ce.sda.web.rest.dto;

import br.gov.ce.sda.domain.Municipio;
import br.gov.ce.sda.domain.ater.*;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MetaAtividadesTrimestralDTO {

    private Long id;
    private MetaAtividadesPTA metaAtividadesPTA;
    private AtividadePTA atividade;
    private PlanoTrimestral planoTrimestral;
    private Municipio municipio;
    private Comunidade comunidade;
    private Long quantidadeMeta;
    private String abrangencia;

    public static MetaAtividadesTrimestralDTO mapperTo(MetaAtividadesTrimestral meta) {
        return MetaAtividadesTrimestralDTO
            .builder()
            .id(meta.getId())
            .metaAtividadesPTA(meta.getMetaAtividadesPTA())
            .atividade(meta.getAtividade())
            .planoTrimestral(meta.getPlanoTrimestral())
            .municipio(meta.getMunicipio())
            .comunidade(meta.getComunidade())
            .quantidadeMeta(meta.getQuantidadeMeta())
            .abrangencia(meta.getAbrangencia()).build();
    }

}
