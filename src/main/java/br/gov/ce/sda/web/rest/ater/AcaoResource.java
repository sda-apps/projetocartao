package br.gov.ce.sda.web.rest.ater;

import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.domain.ater.Acao;
import br.gov.ce.sda.repository.ater.AcaoRepository;
import br.gov.ce.sda.config.security.SecurityUtils;
import br.gov.ce.sda.service.AcaoService;
import br.gov.ce.sda.web.rest.dto.FiltroAcaoDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

import static java.util.Objects.isNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping(value = "/api/ater/acao")
public class AcaoResource {

	@Inject
	private AcaoRepository acaoRepository;

    @Inject
    private AcaoService acaoService;

	@RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Acao>> findAll(){
		List<Acao> acoes = acaoRepository.findAll();

		return new ResponseEntity<>(acoes, acoes.isEmpty()? HttpStatus.NOT_FOUND: HttpStatus.OK);
	}

    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> save(@RequestBody Acao acao) {
        Usuario usuario = SecurityUtils.getCurrentUser();
        acao.setUsuario(usuario);
        Acao savedAcao = acaoService.save(acao);

        return new ResponseEntity<>(savedAcao, HttpStatus.OK);
    }

    @RequestMapping(value="/filtro", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Acao>> findAllFilter(FiltroAcaoDTO filtro){
        List<Acao> acoes = acaoService.findAllByFilter(filtro);

        return new ResponseEntity<>(acoes, acoes.isEmpty() ? HttpStatus.NOT_FOUND: HttpStatus.OK);
    }

    @RequestMapping(value="/{id}", method = DELETE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(@PathVariable Long id) {
        acaoService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value="/{id}", method = PUT, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Acao acao) {
        Acao currentAcao = acaoService.findById(id);
        if (isNull(currentAcao)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Usuario usuario = SecurityUtils.getCurrentUser();
        acao.setUsuario(usuario);

        BeanUtils.copyProperties(acao, currentAcao);

        acaoService.update(currentAcao);
        return ResponseEntity.ok().body(currentAcao);
    }

}
