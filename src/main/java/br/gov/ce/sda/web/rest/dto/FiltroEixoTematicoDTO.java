package br.gov.ce.sda.web.rest.dto;

import br.gov.ce.sda.domain.ater.QEixoTematico;
import com.querydsl.core.BooleanBuilder;
import lombok.*;

import static java.util.Objects.nonNull;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class FiltroEixoTematicoDTO {

	private String nome;

    private Integer start;

    private Integer limit;

    private Integer paginacao;

    private Long total;

    private Boolean ordenar;

    private String ordem;

    public BooleanBuilder getPredicates(QEixoTematico eixoTematico) {
    	BooleanBuilder predicates = new BooleanBuilder();
    	if(nonNull(getNome())) {
    		predicates.and(eixoTematico.nome.containsIgnoreCase(getNome()));
    	}
    	return predicates;
    }


}
