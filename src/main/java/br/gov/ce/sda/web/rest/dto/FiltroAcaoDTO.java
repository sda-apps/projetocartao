package br.gov.ce.sda.web.rest.dto;

import br.gov.ce.sda.domain.ater.QAcao;
import com.querydsl.core.BooleanBuilder;
import lombok.*;

import static java.util.Objects.nonNull;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class FiltroAcaoDTO {

	private String acao;

    private Integer start;

    private Integer limit;

    private Integer paginacao;

    private Long total;

    private Boolean ordenar;

    private String ordem;

    public BooleanBuilder getPredicates(QAcao acao) {
    	BooleanBuilder predicates = new BooleanBuilder();
    	if(nonNull(getAcao())) {
    		predicates.and(acao.nome.containsIgnoreCase(getAcao()));
    	}
    	return predicates;
    }


}
