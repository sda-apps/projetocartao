package br.gov.ce.sda.web.rest.dto.ida;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = IndicadorProjetoBeneficiario.class)
public class IndicadorProjetoBeneficiario {

    private Long id;
    private Long indicadorProjetoCodigo;
    private IndicadorProjeto indicadorProjeto;
    private String beneficiario;
    private String paisCodigo;
    private String ufCodigo;
    private String municipioCodigo;
    private String distritoCodigo;
    private String localidadeCodigo;
    private String fone;
    private String email;
    private String cep;
    private String endereco;
    private String enderecoNumero;
    private String enderecoComp;
    private String latitude;
    private String longitude;
    private Double executadoQuantidade;
    private Double executadoValor;
    private String nome;
    private String cpfCnpj;
    private String siglaUf;
    private String nomeUf;
    private String nomeMun;
    private String nomePai;
    private String nomeDistrito;
    private String nomeLocalidade;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "pt-BR", timezone = "Brazil/East")
    private LocalDate dataNascimento;
    private String sexo;
    private String tipo;
    private String nis;
    private String dap;

}
