package br.gov.ce.sda.web.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import br.gov.ce.sda.domain.ater.*;
import br.gov.ce.sda.repository.*;
import br.gov.ce.sda.repository.ater.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ce.sda.domain.Cargo;
import br.gov.ce.sda.domain.Distrito;
import br.gov.ce.sda.domain.Empresa;
import br.gov.ce.sda.domain.Escolaridade;
import br.gov.ce.sda.domain.EstadoCivil;
import br.gov.ce.sda.domain.Formacao;
import br.gov.ce.sda.domain.Funcao;
import br.gov.ce.sda.domain.Localidade;
import br.gov.ce.sda.domain.Municipio;
import br.gov.ce.sda.domain.Pais;
import br.gov.ce.sda.domain.Territorio;
import br.gov.ce.sda.domain.Uf;
import br.gov.ce.sda.domain.programaseprojeto.Projeto;

@RestController
@RequestMapping("/api")
public class GenericResource {
	//private final Logger log = LoggerFactory.getLogger(GenericResource.class);

	@Inject
	private TerritorioRepository territorioRepository;

    @Inject
    private RegiaoRepository regiaoRepository;

	@Inject
	private ProjetoRepository projetoRepository;

	@Inject
	private TipoATERRepository tipoATERRepository;

	@Inject
	private CustomMunicipioRepository customMunicipioRepository;

	@Inject
	private MunicipioRepository municipioRepository;

	@Inject
	private CustomDistritoRepository customDistritoRepository;

	@Inject
	private LocalidadeRepository localidadeRepository;

	@Inject
	private CustomEmpresaRepository customEmpresaRepository;

	@Inject
	private CargoRepository cargoRepository;

	@Inject
	private PaisRepository paisRepository;

	@Inject
	private FormacaoRepository formacaoRepository;

	@Inject
	private EstadoCivilRepository estadoCivilRepository;

	@Inject
	private UfRepository ufRepository;

	@Inject
	private EscolaridadeRepository escolaridadeRepository;

	@Inject
	private FuncaoRepository funcaoRepository;

    @Inject
    private CustomAbrangenciaRegiaoRepository customAbrangenciaRegiaoRepository;

    @Inject
    private AcaoRepository acaoRepository;

    @Inject
    private CustomComunidadeRepository customComunidadeRepository;

    @Inject
    private CustomLocalidadeRepository customLocalidadeRepository;

	@RequestMapping(value = "/municipio/{municipio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Municipio>> getMunicipios(@PathVariable String municipio) {
        List<Municipio> municipios = customMunicipioRepository.buscarPorMunicipio(municipio,null);

        for(Municipio m : municipios){
        	m.setMunicipioTerritorio((m.getNome()+", "+m.getTerritorio().getNome()));
        }

        if (municipios.isEmpty()) {
        	return new ResponseEntity<>(municipios, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(municipios, HttpStatus.OK);
	}

	@RequestMapping(value = "/municipio/{territorio}/{municipio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Municipio>> getMunicipios(@PathVariable String municipio, @PathVariable String territorio) {
        String[] stringTerritorios = territorio.split("&");
        List<Long> longTerritorios = new ArrayList<Long>();

        for (int i = 0; i < stringTerritorios.length; i++) {
            longTerritorios.add(Long.parseLong(stringTerritorios[i]));
        }

        List<Municipio> municipios = new ArrayList<Municipio>();

        municipios = customMunicipioRepository.buscarPorMunicipio(municipio.toUpperCase(), longTerritorios);

        for(Municipio m : municipios){
        	m.setMunicipioTerritorio(m.getNome()+", "+m.getTerritorio().getNome());
        }

        if (municipios.isEmpty()) {
        	return new ResponseEntity<>(municipios, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(municipios, HttpStatus.OK);
	}

	@RequestMapping(value = "/municipioUf/{uf}/{municipio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Municipio>> getMunicipios(@PathVariable String municipio, @PathVariable Long uf) {

		List<Municipio> municipios = municipioRepository.findAllByNomeIgnoreCaseContainingAndUf_IdOrderByNomeAsc(municipio, uf);

        if (municipios.isEmpty()) {
        	return new ResponseEntity<>(municipios, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(municipios, HttpStatus.OK);
	}

    @RequestMapping(value = "/municipio/regiao/{municipio}/{regioes}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Municipio>> getMunicipiosPorRegiao(@PathVariable String municipio, @PathVariable List<Long> regioes) {

        List<AbrangenciaRegiao> abrangencias = customAbrangenciaRegiaoRepository.findByRegiao(municipio, regioes);

        List<Municipio> municipios = abrangencias.stream()
            .map(abrangencia -> abrangencia.getMunicipio())
            .collect(Collectors.toList());

        if (municipios.isEmpty()) {
            return new ResponseEntity<>(municipios, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(municipios, HttpStatus.OK);
    }

    @RequestMapping(value = "/municipio/regiao/{regioes}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Municipio>> getMunicipiosPorRegioes(@PathVariable List<Long> regioes) {

        List<AbrangenciaRegiao> abrangencias = customAbrangenciaRegiaoRepository.findByRegiao(regioes);

        List<Municipio> municipios = abrangencias.stream()
            .map(abrangencia -> abrangencia.getMunicipio())
            .collect(Collectors.toList());

        if (municipios.isEmpty()) {
            return new ResponseEntity<>(municipios, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(municipios, HttpStatus.OK);
    }

	@RequestMapping(value = "/territorio/{territorio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Territorio>> getTerritorios(@PathVariable String territorio) {

		List<Territorio> territorios = territorioRepository.findAllByNomeIgnoreCaseContaining(territorio);

		if (territorios.isEmpty()) {
        	return new ResponseEntity<>(territorios, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(territorios, HttpStatus.OK);
	}

    @RequestMapping(value = "/regiao/{regiao}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Regiao>> getRegioes(@PathVariable String regiao) {

        List<Regiao> regioes = regiaoRepository.findAllByNomeIgnoreCaseContainingOrderByNome(regiao);

        if (regioes.isEmpty()) {
            return new ResponseEntity<>(regioes, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(regioes, HttpStatus.OK);
    }

    @RequestMapping(value = "/regiao", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Regiao>> getRegioes() {

        List<Regiao> regioes = regiaoRepository.findAllByInativoIsFalseOrderByNome();

        if (regioes.isEmpty()) {
            return new ResponseEntity<>(regioes, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(regioes, HttpStatus.OK);
    }

    @RequestMapping(value = "/territorio", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Territorio>> getTerritorios() {

        List<Territorio> territorios = territorioRepository.findAll();

        if (territorios.isEmpty()) {
            return new ResponseEntity<>(territorios, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(territorios, HttpStatus.OK);
    }


	@RequestMapping(value = "/distrito/{distrito}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Distrito>> getDistritos(@PathVariable String distrito) {

		List<Distrito> distritos = customDistritoRepository.buscarPorMunicipio(distrito.toUpperCase(),null);

        for(Distrito d : distritos){
        	d.setDistritoMunicipio(d.getNome()+", "+d.getMunicipio().getNome());
        }

		if(distritos.isEmpty()){
			return new ResponseEntity<>(distritos, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(distritos, HttpStatus.OK);
	}

	@RequestMapping(value = "/localidade/{localidade}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Localidade>> getLocalidades(@PathVariable String localidade) {

		List<Localidade> localidades = customLocalidadeRepository.findAllByNomeIgnoreCaseContaining(localidade);

		return new ResponseEntity<>(localidades, localidades.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
	}

	@RequestMapping(value = "/localidade/distrito/{distrito}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Localidade>> getLocalidadesPorDistrito(@PathVariable Long distritoId) {

		List<Localidade> localidades = localidadeRepository.findAllByDistrito_Id(distritoId);

		return new ResponseEntity<>(localidades, localidades.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
	}

    @RequestMapping(value = "/localidade/distrito/{distrito}/{nomeLocalidade}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Localidade>> getLocalidadesPorDistrito(@PathVariable Long distrito,@PathVariable String nomeLocalidade) {

        List<Localidade> localidades = localidadeRepository.findAllByNomeIgnoreCaseContainingAndDistrito_Id(nomeLocalidade,distrito);

        return new ResponseEntity<>(localidades, localidades.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

	@RequestMapping(value = "/localidade/municipio/{nomeLocalidade}/{municipio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Localidade>> getLocalidadesPorMunicipio(@PathVariable String nomeLocalidade, @PathVariable Long municipio) {

		List<Localidade> localidades = localidadeRepository.findAllByNomeIgnoreCaseContainingAndDistrito_Municipio_Id(nomeLocalidade, municipio);

		return new ResponseEntity<>(localidades, localidades.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
	}

	@RequestMapping(value = "/distrito/{municipio}/{distrito}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Distrito>> getDistritos(@PathVariable String distrito, @PathVariable String municipio) {

        String[] stringMunicipio = municipio.split("&");
        List<Long> longMunicipio = new ArrayList<Long>();

        for (int i = 0; i < stringMunicipio.length; i++) {
        	longMunicipio.add(Long.parseLong(stringMunicipio[i]));
        }

		List<Distrito> distritos = customDistritoRepository.buscarPorMunicipio(distrito.toUpperCase(),longMunicipio);

        for(Distrito d : distritos){
        	d.setDistritoMunicipio(d.getNome()+", "+d.getMunicipio().getNome());
        }

		if(distritos.isEmpty()){
			return new ResponseEntity<>(distritos, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(distritos, HttpStatus.OK);
	}

    @RequestMapping(value = "/comunidade/{distrito}/{comunidade}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Comunidade>> getComunidadesPorDistrito(@PathVariable String comunidade, @PathVariable String distrito) {

        String[] stringDistrito = distrito.split("&");
        List<Long> longDistrito = new ArrayList<Long>();

        for (int i = 0; i < stringDistrito.length; i++) {
            longDistrito.add(Long.parseLong(stringDistrito[i]));
        }

        List<Comunidade> comunidades = customComunidadeRepository.buscarPorDistrito(comunidade.toUpperCase(),longDistrito);

        for(Comunidade c : comunidades){
            c.setComunidadeDistrito(c.getNome()+", "+c.getDistrito().getNome());
        }

        if(comunidades.isEmpty()){
            return new ResponseEntity<>(comunidades, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(comunidades, HttpStatus.OK);
    }

    @RequestMapping(value = "/comunidade/{comunidade}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Comunidade>> getComunidades(@PathVariable String comunidade) {

        List<Comunidade> comunidades = customComunidadeRepository.buscarPorDistrito(comunidade.toUpperCase(), null);

        for(Comunidade c : comunidades){
            c.setComunidadeDistrito(c.getNome()+", "+c.getDistrito().getNome());
        }

        if(comunidades.isEmpty()){
            return new ResponseEntity<>(comunidades, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(comunidades, HttpStatus.OK);
    }

    @RequestMapping(value = "/comunidade/municipio/{comunidade}/{municipio}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Comunidade>> getComunidadePorMunicipio(@PathVariable List<Long> municipio,@PathVariable String comunidade) {

        List<Comunidade> comunidades = customComunidadeRepository.findAllByNomeAndMunicipioo(comunidade,municipio);

        return new ResponseEntity<>(comunidades, comunidades.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

	@RequestMapping(value = "/projeto/{projeto}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Projeto>> getProjetos(@PathVariable String projeto) {

		List<Projeto> projetos = projetoRepository.findAllByNomeIgnoreCaseContaining(projeto);

		if(projetos.isEmpty()){
			return new ResponseEntity<>(projetos, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(projetos, HttpStatus.OK);
	}

	@RequestMapping(value = "/tipoater/{tipoater}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TipoATER>> getATER(@PathVariable String tipoater) {

		List<TipoATER> ater = tipoATERRepository.findAllByNomeIgnoreCaseContaining(tipoater);

		if(tipoater.isEmpty()){
			return new ResponseEntity<>(ater, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(ater, HttpStatus.OK);
	}

	@RequestMapping(value = "/empresaNomeFantasia/{empresa}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Empresa>> getEmpresa(@PathVariable String empresa) {

		List<Empresa> empresas = customEmpresaRepository.findAllByNomeIgnoreCaseContainingOrderByNomeAsc(empresa);

		return new ResponseEntity<>(empresas, empresas.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
	}

    @RequestMapping(value = "/empresa/{empresa}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Empresa>> getEmpresaNome(@PathVariable String empresa) {

        List<Empresa> empresas = customEmpresaRepository.findAllByNomeOrByNomeFantasiaOrderByNomeAsc(empresa);

        return new ResponseEntity<>(empresas, empresas.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

	@RequestMapping(value = "/cargo/{cargo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Cargo>> getCargo(@PathVariable String cargo) {

		List<Cargo> cargos = cargoRepository.findAllByDescricaoIgnoreCaseContainingOrderByDescricaoAsc(cargo);

		return new ResponseEntity<>(cargos, cargos.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);

	}

	@RequestMapping(value = "/pais/{pais}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Pais>> getPais(@PathVariable String pais) {

		List<Pais> paises = paisRepository.findAllByNacionalidadeIgnoreCaseContaining(pais);

		if(paises.isEmpty()){
			return new ResponseEntity<>(paises, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(paises, HttpStatus.OK);

	}


	@RequestMapping(value = "/formacao/{formacao}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Formacao>> getFormacao(@PathVariable String formacao) {

		List<Formacao> formacaoList = formacaoRepository.findAllByDescricaoIgnoreCaseContaining(formacao);

		if(formacaoList.isEmpty()){
			return new ResponseEntity<>(formacaoList, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(formacaoList, HttpStatus.OK);

	}

	@RequestMapping(value = "/estadocivil", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EstadoCivil>> getEstadoCivil() {

		List<EstadoCivil> estadoCivilList = estadoCivilRepository.findAllByOrderByDescricaoAsc();

		if(estadoCivilList.isEmpty()){
			return new ResponseEntity<>(estadoCivilList, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(estadoCivilList, HttpStatus.OK);

	}

	@RequestMapping(value = "/uf/{uf}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Uf>> getUf(@PathVariable String uf) {

		List<Uf> ufList = ufRepository.findAllByDescricaoIgnoreCaseContaining(uf);

		if(ufList.isEmpty()){
			return new ResponseEntity<>(ufList, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(ufList, HttpStatus.OK);

	}

	@RequestMapping(value = "/uf", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Uf>> getUf() {

		List<Uf> ufList = ufRepository.findAllByOrderByDescricaoAsc();

		if(ufList.isEmpty()){
			return new ResponseEntity<>(ufList, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(ufList, HttpStatus.OK);

	}

	@RequestMapping(value = "/escolaridade", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Escolaridade>> getEscolaridade() {

		List<Escolaridade> escolaridadeList = escolaridadeRepository.findAllByOrderByDescricaoAsc();

		if(escolaridadeList.isEmpty()){
			return new ResponseEntity<>(escolaridadeList, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(escolaridadeList, HttpStatus.OK);

	}

	@RequestMapping(value = "/funcao/{funcao}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Funcao>> getFuncao(@PathVariable String funcao) {

		List<Funcao> funcaoList = funcaoRepository.findAllByDescricaoIgnoreCaseContainingOrderByDescricaoAsc(funcao);

		if(funcaoList.isEmpty()){
			return new ResponseEntity<>(funcaoList, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(funcaoList, HttpStatus.OK);

	}

	@RequestMapping(value = "/cargo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Cargo>> getCargo() {

		List<Cargo> cargoList = cargoRepository.findAllByOrderByDescricaoAsc();

		if(cargoList.isEmpty()){
			return new ResponseEntity<>(cargoList, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(cargoList, HttpStatus.OK);

	}

    @RequestMapping(value = "/acao", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Acao>> getAcao() {

        List<Acao> acaoList = acaoRepository.findAllByOrderByNomeAsc();

        if(acaoList.isEmpty()){
            return new ResponseEntity<>(acaoList, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(acaoList, HttpStatus.OK);

    }


}
