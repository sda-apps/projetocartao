package br.gov.ce.sda.web.rest.ater;

import br.gov.ce.sda.domain.ater.AssociacaoComunitaria;
import br.gov.ce.sda.repository.ater.AssociacaoComunitariaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping(value = "/api/ater/associacoes")
public class AssociacaoComunitariaResource {

    @Inject
    private AssociacaoComunitariaRepository associacaoComunitariaRepository;

    @GetMapping
    public ResponseEntity findAll() {
        List<AssociacaoComunitaria> associacoes = associacaoComunitariaRepository.findAll();
        return ok(associacoes);
    }

}
