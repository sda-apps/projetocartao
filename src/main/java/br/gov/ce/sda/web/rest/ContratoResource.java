package br.gov.ce.sda.web.rest;

import br.gov.ce.sda.domain.Municipio;
import br.gov.ce.sda.domain.ater.Contrato;
import br.gov.ce.sda.repository.ContratoRepository;
import br.gov.ce.sda.repository.CustomMunicipioRepository;
import br.gov.ce.sda.service.ContratoService;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static java.util.Objects.isNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/api/contratos")
public class ContratoResource {

    @Inject
    private ContratoService contratoService;

    @Inject
    private ContratoRepository contratoRepository;

    @Inject
    private CustomMunicipioRepository customMunicipioRepository;

    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> save(@RequestBody Contrato contrato) {
        Contrato savedContrato = contratoService.save(contrato);

        return new ResponseEntity<>(savedContrato, HttpStatus.OK);
    }

    @RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findAll() {
        List<Contrato> contratos = contratoService.findAll();

        return new ResponseEntity<>(contratos, contratos.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value = "/codigo/{filtro}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Contrato>> findAllByFiltro(@PathVariable String filtro) {
        List<Contrato> contratos = contratoRepository.findAllByCodigoIgnoreCaseContainingOrObjetoIgnoreCaseContainingOrTituloIgnoreCaseContaining(filtro, filtro, filtro);

        return new ResponseEntity<>(contratos, contratos.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = GET, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity<?> findById(@PathVariable Long id) {
        Contrato contrato = contratoService.findById(id);

        if (isNull(contrato)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(contrato, HttpStatus.OK);

    }


    @RequestMapping(value = "/empresa/{id}", method = GET, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity<?> findByEmpresaId(@PathVariable Long id) {
        List<Long> listId = new ArrayList<>();
        listId.add(id);
        Set<Contrato> contratos = contratoService.findByEmpresaId(listId);

        return new ResponseEntity<>(contratos, contratos.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, value = "/{id}")
    public ResponseEntity<Contrato> update(@PathVariable Long id, @RequestBody Contrato contrato) throws URISyntaxException {
        Contrato currentContrato = contratoService.findById(id);

        if (isNull(currentContrato)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        BeanUtils.copyProperties(contrato, currentContrato);
        contratoService.update(currentContrato);
        return new ResponseEntity<>(currentContrato, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = DELETE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(@PathVariable Long id) {
        contratoService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/municipio-empresa/{id}", method = GET, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity<?> findMunicipioByEmpresaId(@PathVariable List<Long> id) {
        Set<Contrato> contratos = contratoService.findByEmpresaId(id);

        if (isNull(contratos)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            List<Municipio> municipios = customMunicipioRepository.buscarPorContrato(contratos);
            return new ResponseEntity<>(municipios, HttpStatus.OK);
        }
    }

}
