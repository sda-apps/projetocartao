package br.gov.ce.sda.web.rest.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter
public class RelatorioSinteticoContratoDTO {

    private String contrato;

    private Long idPTA;

    private Long qtdMunicipio;

    private Long qtdComunidade;

    private Long qtdFamilias;

    private int qtdTecnicos;

    private int qtdAssessor;

    public RelatorioSinteticoContratoDTO() {}

    public RelatorioSinteticoContratoDTO(Long idPTA, String contrato,  Long qtdMunicipio, Long qtdComunidade, Long qtdFamilias) {
        this.contrato = contrato;
        this.idPTA = idPTA;
        this.qtdMunicipio = qtdMunicipio;
        this.qtdComunidade = qtdComunidade;
        this.qtdFamilias = qtdFamilias;
    }
}
