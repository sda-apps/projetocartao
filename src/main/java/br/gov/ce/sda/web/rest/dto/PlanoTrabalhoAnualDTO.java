package br.gov.ce.sda.web.rest.dto;

import br.gov.ce.sda.domain.ater.PlanoTrabalhoAnual;
import lombok.*;

import java.util.Date;

import static java.util.Objects.nonNull;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class PlanoTrabalhoAnualDTO {

    public PlanoTrabalhoAnualDTO(PlanoTrabalhoAnual plano) {

        if(nonNull(plano.getId())) {
            this.id = plano.getId();
        }

        if(nonNull(plano.getId())) {
            this.status = plano.getStatus();
        }

        if(nonNull(plano.getId())) {
            this.recusado = plano.isRecusado();
        }

        if(nonNull(plano.getDataInicio())) {
            this.dataInicio = plano.getDataInicio();
        }

        if(nonNull(plano.getContrato())) {
            this.contrato = plano.getContrato().getCodigo();

            if(nonNull(plano.getContrato().getEmpresa())) {
                this.nomeEmpresa = plano.getContrato().getEmpresa().getPessoaJuridica().getPessoa().getNome();
                this.nomeFantasia = plano.getContrato().getEmpresa().getPessoaJuridica().getNomeFantasia();
            }

            if(nonNull(plano.getContrato().getRegiao())) {
                this.regiao = plano.getContrato().getRegiao().getNome();
            }
        }

        if(nonNull(plano.getId())) {
            this.reaberto = plano.isReaberto();
        }
    }

    private Long id;
    private String contrato;
    private Date dataInicio;
    private String nomeEmpresa;
    private String regiao;
    private String status;
    private String nomeFantasia;
    private boolean recusado;
    private boolean reaberto;

}
