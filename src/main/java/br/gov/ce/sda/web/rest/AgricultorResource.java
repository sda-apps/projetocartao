package br.gov.ce.sda.web.rest;

import static java.util.Objects.isNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ce.sda.domain.Agricultor;
import br.gov.ce.sda.domain.AgricultorProjeto;
import br.gov.ce.sda.domain.Dap;
import br.gov.ce.sda.domain.Etnia;
import br.gov.ce.sda.domain.Familia;
import br.gov.ce.sda.domain.mapa.AgricultorAreaGeorreferenciada;
import br.gov.ce.sda.domain.mapa.Elemento;
import br.gov.ce.sda.domain.programaseprojeto.Caju;
import br.gov.ce.sda.repository.AgricultorRepository;
import br.gov.ce.sda.repository.CajuRepository;
import br.gov.ce.sda.repository.CustomAgricultorRepository;
import br.gov.ce.sda.repository.CustomElementoRepository;
import br.gov.ce.sda.repository.CustomFamiliaRepository;
import br.gov.ce.sda.repository.DapRepository;
import br.gov.ce.sda.repository.EtniaRepository;
import br.gov.ce.sda.service.AgricultorService;
import br.gov.ce.sda.web.rest.dto.FiltroAgricultorDTO;
import br.gov.ce.sda.web.rest.util.HeaderUtil;

@RestController
@RequestMapping("/api")
public class AgricultorResource {

	@Inject
	private CustomElementoRepository customElementoRepository;

	@Inject
	private DapRepository dapRepository;

	@Inject
	private CustomAgricultorRepository customAgricultorRepository;

	@Inject
    private AgricultorService agricultorService;

	@Inject
    private EtniaRepository etniaRepository;

	@Inject
    private AgricultorRepository agricultorRepository;

	@Inject
	private CajuRepository cajuRepository;

	@Inject
	private CustomFamiliaRepository customFamiliaRepository;

	@RequestMapping(value = "/agricultor/{id}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Agricultor> getDadosAgricultor(@PathVariable Long id) {

        Agricultor agricultor = customAgricultorRepository.buscarPorId(id);

		return Optional.ofNullable(agricultor)
            .map(agricultorResponce -> new ResponseEntity<Agricultor>(agricultorResponce, HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@RequestMapping(value = "/agricultor/{id}", method = PUT, consumes={APPLICATION_JSON_VALUE}, produces={APPLICATION_JSON_VALUE})
    public ResponseEntity<Agricultor> update(@PathVariable Long id, @RequestBody Agricultor agricultor) {
		Agricultor agricultorAtual = agricultorRepository.findById(id).get();

		//TODO conjuge para familia
		//agricultor.setConjuge(customAgricultorRepository.buscarConjugePorAgricultorId(agricultor.getId()));

        if (isNull(agricultorAtual)) {
            return new ResponseEntity<Agricultor>(HttpStatus.NOT_FOUND);
        }

        BeanUtils.copyProperties(agricultor, agricultorAtual);
        agricultorRepository.save(agricultorAtual);
        return ResponseEntity.ok().headers(HeaderUtil.sucess("Os dados de " + agricultorAtual.getPessoaFisica().getPessoa().getNome() + " foram atualizados com sucesso.")).body(agricultorAtual);
    }

	@RequestMapping(value = "/agricultor/familia/{agricultorId}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Familia> buscarFamiliaAgricultor(@PathVariable Long agricultorId) {

        Familia familia = customFamiliaRepository.findByIdChefe(agricultorId);

		return Optional.ofNullable(familia)
            .map(agricultorResponce -> new ResponseEntity<Familia>(agricultorResponce, HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@RequestMapping(value = "/agricultor/projetos/{agricultorId}/{agricultorProjetoAno}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List <AgricultorProjeto>> getProjetosAgricultorPorAno(@PathVariable Long agricultorId, @PathVariable Integer agricultorProjetoAno) {

		List <AgricultorProjeto> agricultorProjeto = customAgricultorRepository.buscarProjetosPorAno(agricultorId, agricultorProjetoAno);

		return new ResponseEntity<>(agricultorProjeto, agricultorProjeto.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
	}

	@RequestMapping(value = "/agricultor/projetos/{agricultorId}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List <AgricultorProjeto>> getProjetosAgricultor(@PathVariable Long agricultorId) {

        List <AgricultorProjeto> AgricultorProjeto = customAgricultorRepository.buscarProjetos(agricultorId);

        return new ResponseEntity<>(AgricultorProjeto, AgricultorProjeto.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
	}

	@RequestMapping(value = "/agricultor/daps/{agricultorId}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List <Dap>> getDapsAgricultor(@PathVariable Long agricultorId) {

		List <Dap> daps = dapRepository.findByAgricultor_IdOrderByDataEmissaoDesc(agricultorId);

		return new ResponseEntity<>(daps, daps.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
	}

	@RequestMapping(value = "/agricultores/filtro", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FiltroAgricultorDTO> buscarAgricultoresPorFiltro(FiltroAgricultorDTO filtro) throws URISyntaxException {

		FiltroAgricultorDTO dto = customAgricultorRepository.buscarAgricultoresPorFiltro(filtro);

        return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@RequestMapping(value = "/agricultores/filtro/total", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FiltroAgricultorDTO> buscarTotalAgricultoresPorFiltro(FiltroAgricultorDTO filtro) throws URISyntaxException {

		Long total = customAgricultorRepository.buscarTotalAgricultoresPorFiltro(filtro);

		filtro.setTotal(total);

		return new ResponseEntity<>(filtro, HttpStatus.OK);
	}

	@RequestMapping(value = "/agricultores/lotes/poligonos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AgricultorAreaGeorreferenciada>> buscarAgricultoresAreasGeorreferenciadasPorFiltro(FiltroAgricultorDTO filtro) throws URISyntaxException {

		List<AgricultorAreaGeorreferenciada> areas = customAgricultorRepository.buscarAgricultoresAreasGeorreferenciadasPorFiltro(filtro);

        return new ResponseEntity<>(areas, areas.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
	}

	@RequestMapping(value = "/agricultor/cisterna/{agricultorId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Elemento> buscarCisterna(@PathVariable Long agricultorId) throws URISyntaxException {

		Elemento elemento = customElementoRepository.buscarCisternaPorAgricultor(agricultorId);

		return Optional.ofNullable(elemento)
	            .map(agricultorResponce -> new ResponseEntity<Elemento>(agricultorResponce, HttpStatus.OK))
	            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));

	}

	@RequestMapping(value = "/agricultor/lote/poligono/{agricultorId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Agricultor> buscarLotesComPoligono(@PathVariable Long agricultorId) throws URISyntaxException {
		Agricultor agricultor = agricultorService.getAgricultorComAreasGeograficas(agricultorId);

		return new ResponseEntity<>(agricultor, agricultor.getAreasGeorreferenciadas().isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);

	}

	@RequestMapping(value = "/agricultor/etnia", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Etnia>> buscarEtnia() throws URISyntaxException {
		List<Etnia> etnias = etniaRepository.findAllByOrderByNomeAsc();

		return new ResponseEntity<>(etnias, etnias.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);

	}

	@RequestMapping(value = "/agricultor/caju/{agricultorId}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List <Caju>> getCajuAgricultor(@PathVariable Long agricultorId) {

		List <Caju> cajus = cajuRepository.findByAgricultor_IdOrderByAno(agricultorId);

		return new ResponseEntity<>(cajus, cajus.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
	}
}
