package br.gov.ce.sda.web.rest.mobile;

import br.gov.ce.sda.domain.mobile.AcessoPoliticasPublicas;
import br.gov.ce.sda.domain.mobile.AtividadeCadastro;
import br.gov.ce.sda.domain.mobile.Capacitacao;
import br.gov.ce.sda.domain.mobile.Producao;
import br.gov.ce.sda.repository.ater.CustomAbrangenciaRegiaoRepository;
import br.gov.ce.sda.repository.mobile.CustomAtividadeCadastroRepository;
import br.gov.ce.sda.service.RegiaoService;
import br.gov.ce.sda.service.mobile.AtividadeCadastroService;
import br.gov.ce.sda.web.rest.dto.AtividadeCadastroEmpresaDTO;
import br.gov.ce.sda.web.rest.dto.mobile.FiltroCadastroDTO;
import br.gov.ce.sda.web.rest.util.HeaderUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static java.util.Objects.isNull;
import static org.springframework.http.ResponseEntity.notFound;

@RestController
@RequestMapping("/api/mobile/cadastro")
@RequiredArgsConstructor
public class AtividadeCadastroResource {

    private final AtividadeCadastroService atividadeCadastroService;
    private final CustomAtividadeCadastroRepository customAtividadeCadastroRepository;
    private final RegiaoService regiaoService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AtividadeCadastro>> list(FiltroCadastroDTO filters) {

        List<AtividadeCadastro> cadastros = atividadeCadastroService.findByFilters(filters);

        return new ResponseEntity<>(cadastros, cadastros.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/{id}")
    public ResponseEntity<AtividadeCadastro> findById(@PathVariable Long id) {

        AtividadeCadastro atividadeCadastro = customAtividadeCadastroRepository.buscarPorId(id);

        return Optional.ofNullable(atividadeCadastro)
            .map(atividadeCadastroResponce -> new ResponseEntity<>(atividadeCadastroResponce, HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));

    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/resumo/{id}")
    public ResponseEntity<AtividadeCadastro> findByIdSummary(@PathVariable Long id) {

        AtividadeCadastro atividadeCadastro = customAtividadeCadastroRepository.buscarPorIdResumo(id);

        return Optional.ofNullable(atividadeCadastro)
            .map(atividadeCadastroResponce -> new ResponseEntity<>(atividadeCadastroResponce, HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));

    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/acesso-politicas-publicas/{id}")
    public ResponseEntity<AcessoPoliticasPublicas> findAcessoPoliticasPublicasByAtividadeCadastroId(@PathVariable Long id) {

        AcessoPoliticasPublicas acessoPoliticasPublicas = customAtividadeCadastroRepository.buscarAcessoPoliticasPublicasPorId(id);

        return Optional.ofNullable(acessoPoliticasPublicas)
            .map(acessoPoliticasPublicasResponce -> new ResponseEntity<>(acessoPoliticasPublicasResponce, HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));

    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/producao/{id}")
    public ResponseEntity<Producao> findProducaoByAtividadeCadastroId(@PathVariable Long id) {

        Producao producao = customAtividadeCadastroRepository.buscarProducaoPorId(id);

        return Optional.ofNullable(producao)
            .map(producaoResponce -> new ResponseEntity<>(producaoResponce, HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));

    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/capacitacao/{id}")
    public ResponseEntity<Capacitacao> findCapacitacaoByAtividadeCadastroId(@PathVariable Long id) {
        Capacitacao capacitacao = customAtividadeCadastroRepository.buscarCapacitacaoPorId(id);

        return Optional.ofNullable(capacitacao)
            .map(capacitacaoResponce -> new ResponseEntity<>(capacitacaoResponce, HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));

    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/aprovar/regiao")
    public ResponseEntity<AtividadeCadastro> aprovarAutomaticamentePorRegiao(@RequestParam(value = "nomeRegiao") String nomeRegiao,
                                                                             @RequestParam(value = "start") int start,
                                                                             @RequestParam(value = "limit") int limit) {
        return regiaoService.findBy(nomeRegiao)
            .map(atividadeCadastroService::aprovarAutomaticamentePor)
            .orElseGet(() -> notFound().build());
    }

    @RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, value = "/avaliar/{id}")
    public ResponseEntity<AtividadeCadastro> update(@PathVariable Long id, @RequestBody AtividadeCadastro atividadeCadastro) {
        AtividadeCadastro cadastro = atividadeCadastroService.aprovarCadastro(id, atividadeCadastro);

        if(isNull(cadastro)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok().headers(HeaderUtil.sucess("Avaliação realizada com sucesso")).body(atividadeCadastro);
    }


    @RequestMapping(value = "/count-atividades-cadastro", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AtividadeCadastroEmpresaDTO>> getTerritorios() {

        List<AtividadeCadastroEmpresaDTO> empresas = customAtividadeCadastroRepository.countAtividadeCadastroPorEmpresa();

        return new ResponseEntity<>(empresas, HttpStatus.OK);
    }

}
