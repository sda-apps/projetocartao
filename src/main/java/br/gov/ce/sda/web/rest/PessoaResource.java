package br.gov.ce.sda.web.rest;

import br.gov.ce.sda.domain.Pessoa;
import br.gov.ce.sda.repository.DadosBancariosRepository;
import br.gov.ce.sda.repository.PessoaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.Optional;

import static br.gov.ce.sda.MessageKey.PESSOA_ATUALIZADA_SUCESSO;
import static br.gov.ce.sda.Messages.getString;
import static br.gov.ce.sda.web.rest.util.HeaderUtil.sucess;
import static org.springframework.beans.BeanUtils.copyProperties;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api/pessoa")
public class PessoaResource {

    @Inject
    private PessoaRepository pessoaRepository;

    @Inject
    private DadosBancariosRepository dadosBancariosRepository;

    @Transactional
    @PutMapping(value = "/{id}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity update(@PathVariable Long id, @RequestBody Pessoa pessoa) {
        return pessoaRepository.findById(id)
            .map(pessoaAtual -> {
                copyProperties(pessoa, pessoaAtual);
                pessoaRepository.save(pessoaAtual);
                return ok()
                    .headers(sucess(getString(PESSOA_ATUALIZADA_SUCESSO, pessoaAtual.getNome())))
                    .body(pessoaAtual);
            }).orElse(notFound().build());
    }

    @GetMapping(value = "/dados-bancarios/{pessoaId}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity getDadosBancarios(@PathVariable Long pessoaId) {
        return dadosBancariosRepository.findByPessoa_id(pessoaId)
            .map(ResponseEntity::ok)
            .orElse(notFound().build());
    }

}
