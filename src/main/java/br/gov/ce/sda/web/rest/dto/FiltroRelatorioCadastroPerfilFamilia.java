package br.gov.ce.sda.web.rest.dto;

import lombok.*;

@Getter @Setter
public class FiltroRelatorioCadastroPerfilFamilia extends FiltroRelatorioCadastro {

    private String linhaBase;

    private Integer tipoRelatorio;

    private String visualizacaoCadastros;

}
