package br.gov.ce.sda.web.rest;


import br.gov.ce.sda.domain.Municipio;
import br.gov.ce.sda.repository.MunicipioRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/municipios")
public class MunicipioResource {

    private final MunicipioRepository municipioRepository;

    public MunicipioResource(MunicipioRepository municipioRepository) {
        this.municipioRepository = municipioRepository;
    }

    @RequestMapping(value = "/uf/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Municipio>> getMunicipiosPorUf(@PathVariable Long id) {

        List<Municipio> municipios = municipioRepository.findAllByUf_IdOrderByNomeAsc(id);

        return new ResponseEntity<>(municipios, municipios.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

}
