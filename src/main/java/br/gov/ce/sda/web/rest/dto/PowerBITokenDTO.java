package br.gov.ce.sda.web.rest.dto;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class PowerBITokenDTO {

    @SerializedName("token_type")
    private String tokenType;

    @SerializedName("expires_in")
    private String expiresIn;

    @SerializedName("ext_expires_in")
    private String extExpiresIn;

    @SerializedName("expires_on")
    private String expiresOn;

    @SerializedName("not_before")
    private String notBefore;

    @SerializedName("resource")
    private String resource;

    @SerializedName("access_token")
    private String accessToken;

    @SerializedName("refresh_token")
    private String refreshToken;

    @SerializedName("id_token")
    private String idToken;

}
