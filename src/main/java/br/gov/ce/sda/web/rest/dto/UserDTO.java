package br.gov.ce.sda.web.rest.dto;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import br.gov.ce.sda.domain.Agricultor;
import br.gov.ce.sda.domain.UsuarioAvancado;
import br.gov.ce.sda.domain.acesso.Autoridade;
import br.gov.ce.sda.domain.acesso.Usuario;

public class UserDTO {

    public static final int PASSWORD_MIN_LENGTH = 6;
    public static final int PASSWORD_MAX_LENGTH = 50;
    
    private Long id;

    @Pattern(regexp = "/^[a-z0-9\\.]+$/i")
    @NotNull
    @Size(min = 1, max = 50)
    private String login;

    @NotNull
    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
    private String senha;

	private Agricultor agricultor;

	private UsuarioAvancado usuarioAvancado;

    private boolean ativado = false;

    private boolean primeiroAcesso = false;
    
    private String status;

    private Set<String> authorities;

    public UserDTO() { }

    public UserDTO(Usuario user) {
        if (Objects.nonNull(user)) {
        	this.id = user.getId();
            this.login = user.getLogin();
            this.agricultor = user.getAgricultor();
            this.usuarioAvancado = user.getUsuarioAvancado();
            this.ativado = user.isEnabled();
            this.primeiroAcesso = user.isPrimeiroAcesso();
            this.status = user.getStatus().getId();
            this.authorities = user.getAutoridades().stream().map(Autoridade::getNome).collect(Collectors.toSet());
        }
    }

    public UserDTO(Long id, String login, String senha, Agricultor agricultor, UsuarioAvancado usuarioAvancado, boolean ativado, boolean primeiroAcesso, String status, Set<String> authorities) {
    	this.id = id;
    	this.login = login;
        this.senha = senha;
        this.agricultor = agricultor;
        this.usuarioAvancado = usuarioAvancado;
        this.status = status;
        this.ativado = ativado;
        this.authorities = authorities;
        this.primeiroAcesso = primeiroAcesso;
    }

    public UserDTO(String login, Set<String> authorities){
    	this.login = login;
    	this.authorities = authorities;
    }

    public String getSenha() {
        return senha;
    }
    
    public Long getId() {
		return id;
	}

    public String getLogin() {
        return login;
    }

    public Agricultor getAgricultor() {
		return agricultor;
	}

    public UsuarioAvancado getUsuarioAvancado() {
		return usuarioAvancado;
	}

    public boolean isAtivado() {
        return ativado;
    }

    public boolean isPrimeiroAcesso() {
    	return primeiroAcesso;
    }

    public Set<String> getAuthorities() {
        return authorities;
    }
    
    public String getStatus() {
		return status;
	}
    
    public void setStatus(String status) {
		this.status = status;
	}

    @Override
    public String toString() {
        return "UserDTO{" +
            "login='" + login + '\'' +
            ", senha='" + senha + '\'' +
            ", ativado=" + ativado +
            ", authorities=" + authorities +
            "}";
    }
}
