package br.gov.ce.sda.web.rest;


import br.gov.ce.sda.domain.Empresa;
import br.gov.ce.sda.domain.PessoaJuridica;
import br.gov.ce.sda.domain.Telefone;
import br.gov.ce.sda.domain.ater.Contrato;
import br.gov.ce.sda.repository.PessoaJuridicaRepository;
import br.gov.ce.sda.repository.TelefoneRepository;
import br.gov.ce.sda.service.EmpresaService;
import br.gov.ce.sda.web.rest.dto.EmpresaDTO;
import br.gov.ce.sda.web.rest.dto.FiltroEmpresaDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/api/empresas")
public class EmpresaResource {

    @Inject
    private EmpresaService empresaService;

    @Inject
    private TelefoneRepository telefoneService;

    @Inject
    private PessoaJuridicaRepository pessoaJuridicaRepository;

    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> save(@RequestBody Empresa empresa) {
        if(empresa.getPessoaJuridica().getPessoa().getTelefones() != null) {
            for(Telefone telefone: empresa.getPessoaJuridica().getPessoa().getTelefones()){
                telefone.setPessoa(empresa.getPessoaJuridica().getPessoa());
            }
        }

        PessoaJuridica verificaPJ = pessoaJuridicaRepository.findByCnpj(empresa.getPessoaJuridica().getCnpj());

        if(verificaPJ != null){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        Empresa savedEmpresa = empresaService.save(empresa);

        return new ResponseEntity<>(savedEmpresa, HttpStatus.OK);
    }

    @RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findAll() {
        List<Empresa> empresas = empresaService.findAll();
        return new ResponseEntity<>(empresas, empresas.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value="/{id}", method=GET, produces={APPLICATION_JSON_VALUE})
    public ResponseEntity<?> findById(@PathVariable Long id) {
        Empresa empresa = empresaService.findById(id);
        if (isNull(empresa)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(empresa, HttpStatus.OK);
    }

    @RequestMapping(value="/{id}", method = PUT, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Empresa empresa) {
        Empresa currentEmpresa = empresaService.findById(id);
        if (isNull(currentEmpresa)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if(empresa.getContrato() != null) {
            for (Contrato contrato : empresa.getContrato()) {
                contrato.setEmpresa(empresa);
            }
        }
        telefoneService.deleteAll(currentEmpresa.getPessoaJuridica().getPessoa().getTelefones());

        if(empresa.getPessoaJuridica().getPessoa().getTelefones() != null) {
            for(Telefone telefone: empresa.getPessoaJuridica().getPessoa().getTelefones()){
                telefone.setPessoa(empresa.getPessoaJuridica().getPessoa());
            }
        }

        BeanUtils.copyProperties(empresa, currentEmpresa);

        empresaService.update(currentEmpresa);
        return ResponseEntity.ok().body(currentEmpresa);
    }

    @RequestMapping(value="/{id}", method = DELETE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(@PathVariable Long id) {
        empresaService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value="/filtro", method = GET, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<List<EmpresaDTO>> findAllFilter(FiltroEmpresaDTO filtro){
		List<Empresa> empresas = empresaService.findAllByFilter(filtro);

        List<EmpresaDTO> empresasDTO = empresas.stream().map(empresa -> new EmpresaDTO(empresa)).collect(Collectors.toList());

        return new ResponseEntity<>(empresasDTO, empresasDTO.isEmpty() ? HttpStatus.NOT_FOUND: HttpStatus.OK);
	}

    @RequestMapping(value="/regiao/{id}", method=GET, produces={APPLICATION_JSON_VALUE})
    public ResponseEntity<List<EmpresaDTO>> findByRegiao(@PathVariable List<Long> id) {
        List<EmpresaDTO> empresas = empresaService.findAllByRegiao(id);
        if (isNull(empresas)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(empresas, HttpStatus.OK);
    }

    @RequestMapping(value = "/paulo-freire", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Empresa>> getEmpresaPauloFreire() {

        List<Empresa> empresas = empresaService.findAllPauloFreire();

        return new ResponseEntity<>(empresas, empresas.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }
}
