package br.gov.ce.sda.web.rest.dto;

import br.gov.ce.sda.domain.*;
import br.gov.ce.sda.domain.ater.QAbrangenciaContrato;
import br.gov.ce.sda.domain.ater.QContrato;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQuery;
import lombok.*;

import java.util.Calendar;
import java.util.List;

import static java.util.Objects.nonNull;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class FiltroLocalidadeDTO {

	private List<Long> regiao;

	private List<Long> municipio;

    private List<Long> distrito;

    private List<Long> territorio;

	private String localidade;

    private Integer start;

    private Integer limit;

    private Integer paginacao;

    private Long total;

    private Boolean ordenar;

    private String ordem;

    public BooleanBuilder getPredicates(QLocalidade localidade, QDistrito distrito, QMunicipio municipio, QTerritorio territorio) {
    	BooleanBuilder predicates = new BooleanBuilder();
    	if(nonNull(getLocalidade())) {
    		predicates.and(localidade.nome.containsIgnoreCase(getLocalidade()));
    	}

        if(nonNull(getDistrito())){
            predicates.and(distrito.id.in(getDistrito()));
        }

        if(nonNull(getMunicipio())){
            predicates.and(municipio.id.in(getMunicipio()));
        }

        if(nonNull(getTerritorio())){
            predicates.and(territorio.id.in(getTerritorio()));
        }

    	return predicates;
    }


}
