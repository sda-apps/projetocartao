package br.gov.ce.sda.web.rest.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter
public class RelatorioSinteticoAtcDTO {

    private String nomeFantasia;

    private List<RelatorioSinteticoContratoDTO> contratos;



    public RelatorioSinteticoAtcDTO() {}

    public RelatorioSinteticoAtcDTO(String nomeFantasia, List<RelatorioSinteticoContratoDTO> contratos) {
        this.nomeFantasia = nomeFantasia;
        this.contratos = contratos;

    }

}
