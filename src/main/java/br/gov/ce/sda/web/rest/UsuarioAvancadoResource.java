package br.gov.ce.sda.web.rest;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ce.sda.domain.UsuarioAvancado;
import br.gov.ce.sda.repository.CustomUsuarioAvancadoRepository;
import br.gov.ce.sda.service.UserService;
import br.gov.ce.sda.web.rest.util.HttpUtil;

@RestController
@RequestMapping(value = "/api/usuarioavancado")
public class UsuarioAvancadoResource {

	@Inject
	private CustomUsuarioAvancadoRepository customUsuarioAvancadoRepository;

	@Inject
	private UserService userService;

	@RequestMapping(value = "/buscarcpf/{cpf}", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UsuarioAvancado> buscarDadosUsuarioAvancado(@PathVariable String cpf) {
        UsuarioAvancado usuarioAvancado = customUsuarioAvancadoRepository.buscarPorCpf(cpf);
		return Optional.ofNullable(usuarioAvancado)
	            .map(usuarioAvancadoResponce -> new ResponseEntity<>(usuarioAvancadoResponce, HttpStatus.OK))
	            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@RequestMapping(value = "/sendbroadcastemail", method = GET)
	public ResponseEntity<?> sendBroadcastEmail() {
		String baseUrl = HttpUtil.PORTAL_AGRICULTURA_FAMILIAR_URL;
		userService.enviarSignUpEmail(baseUrl);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/sendbroadcastemail", method = POST)
	public ResponseEntity<?> sendBroadcastEmail(@RequestBody List<Long> usuariosId) {
		String baseUrl = HttpUtil.PORTAL_AGRICULTURA_FAMILIAR_URL;
		userService.enviarSignUpEmail(usuariosId, baseUrl);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}",  method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UsuarioAvancado> buscarDadosUsuarioAvancadoPorId(@PathVariable Long id) {
        UsuarioAvancado usuarioAvancado = customUsuarioAvancadoRepository.buscarPorId(id);
		return Optional.ofNullable(usuarioAvancado)
	            .map(usuarioAvancadoResponce -> new ResponseEntity<>(usuarioAvancadoResponce, HttpStatus.OK))
	            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@RequestMapping(value = "/nome/{nome}", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UsuarioAvancado>> buscarUsuarioAvancadoPorNome(@PathVariable String nome) {
		List<UsuarioAvancado> tecnicos = userService.buscarPorNome(nome);

		return new ResponseEntity<>(tecnicos, tecnicos.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
	}

	@RequestMapping(value = "/empresa/{nome}/{empresa}", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UsuarioAvancado>> buscarUsuarioAvancadoPorEmpresa(@PathVariable String nome, @PathVariable Long empresa) {
		List<UsuarioAvancado> tecnicos = customUsuarioAvancadoRepository.buscarPorNomeEEmpresa(nome, empresa);

		return new ResponseEntity<>(tecnicos, tecnicos.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
	}

}
