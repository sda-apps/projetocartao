package br.gov.ce.sda.web.rest.ater;

import br.gov.ce.sda.domain.Localidade;
import br.gov.ce.sda.domain.Municipio;
import br.gov.ce.sda.domain.ater.Comunidade;
import br.gov.ce.sda.repository.LocalidadeRepository;
import br.gov.ce.sda.repository.ater.CustomComunidadeRepository;
import br.gov.ce.sda.service.ComunidadeService;
import br.gov.ce.sda.service.LocalidadeService;
import br.gov.ce.sda.web.rest.dto.FiltroComunidadeDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

import static java.util.Objects.isNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping(value = "/api/ater/comunidade")
public class ComunidadeResource {

	@Inject
	private CustomComunidadeRepository customComunidadeRepository;

    @Inject
    private ComunidadeService comunidadeService;

    @Inject
    private LocalidadeService localidadeService;

    @Inject
    private LocalidadeRepository localidadeRepository;

    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> save(@RequestBody Comunidade comunidade) {

        Comunidade savedComunidade = comunidadeService.save(comunidade);

        for (Localidade localidade : savedComunidade.getLocalidades()) {
            localidade = localidadeService.findById(localidade.getId());
            localidade.setComunidade(savedComunidade);
            localidadeService.update(localidade);
        }

        return new ResponseEntity<>(savedComunidade, HttpStatus.OK);
    }

    @RequestMapping(value="/{id}", method = PUT, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Comunidade comunidade) {
        Comunidade currentComunidade = comunidadeService.findById(id);

        List<Localidade> localidades = localidadeRepository.findAllByComunidade_Id(currentComunidade.getId());

        for(Localidade localidade : localidades){
            localidade.setComunidade(null);
            localidadeService.update(localidade);
        }

        for (Localidade localidade : comunidade.getLocalidades()) {
            localidade = localidadeService.findById(localidade.getId());
            localidade.setComunidade(comunidade);
            localidadeService.update(localidade);
        }

        if (isNull(currentComunidade)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        BeanUtils.copyProperties(comunidade, currentComunidade);

        comunidadeService.update(currentComunidade);
        return ResponseEntity.ok().body(currentComunidade);
    }

    @RequestMapping(value="/{id}", method = DELETE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(@PathVariable Long id) {
        List<Localidade> localidades = localidadeRepository.findAllByComunidade_Id(id);

        for(Localidade localidade : localidades){
            localidade.setComunidade(null);
            localidadeService.update(localidade);
        }

        comunidadeService.delete(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(params = {"municipios"},method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Comunidade>> buscarComunidadesPorMunicipio(@RequestParam("municipios") List<Municipio> municipios) {

        List <Comunidade> comunidades = customComunidadeRepository.findByMunicipio(municipios);

        return new ResponseEntity<>(comunidades, comunidades.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value = "/{comunidade}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Comunidade>> getComunidades(@PathVariable String comunidade) {

        List<Comunidade> comunidades = customComunidadeRepository.findAllByNomeIgnoreCaseContaining(comunidade);

        if (comunidades.isEmpty()) {
            return new ResponseEntity<>(comunidades, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(comunidades, HttpStatus.OK);
    }

    @RequestMapping(value="/filtro", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Comunidade>> findAllFilter(FiltroComunidadeDTO filtro){
        List<Comunidade> comunidades = comunidadeService.findAllByFilter(filtro);

        return new ResponseEntity<>(comunidades, comunidades.isEmpty() ? HttpStatus.NOT_FOUND: HttpStatus.OK);
    }


    @RequestMapping(value = "/municipio/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Comunidade>> getComunidadesPorMunicipio(@PathVariable List<Long> id) {

        List<Comunidade> comunidades = comunidadeService.findByMunicipio(id);

        return new ResponseEntity<>(comunidades, comunidades.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value = "/municipio/areaatuacao/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Comunidade>> getComunidadesPorMunicipioAndAreaAtuacao(@PathVariable List<Long> id) {

        List<Comunidade> comunidades = comunidadeService.findByMunicipioAndAreaAtuacao(id);

        return new ResponseEntity<>(comunidades, comunidades.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

}
