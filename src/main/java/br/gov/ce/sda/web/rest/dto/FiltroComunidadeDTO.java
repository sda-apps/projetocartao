package br.gov.ce.sda.web.rest.dto;

import br.gov.ce.sda.domain.QDistrito;
import br.gov.ce.sda.domain.QLocalidade;
import br.gov.ce.sda.domain.QMunicipio;
import br.gov.ce.sda.domain.ater.QAbrangenciaContrato;
import br.gov.ce.sda.domain.ater.QComunidade;
import br.gov.ce.sda.domain.ater.QContrato;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQuery;
import lombok.*;

import java.util.List;

import static java.util.Objects.nonNull;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FiltroComunidadeDTO {

    private List<Long> regiao;

    private List<Long> municipio;

    private List<Long> distrito;

    private List<Long> localidade;

    private String comunidade;

    private Integer start;

    private Integer limit;

    private Integer paginacao;

    private Long total;

    private Boolean ordenar;

    private String ordem;

    public BooleanBuilder getPredicates(QComunidade qComunidade, QDistrito qDistrito, QMunicipio qMunicipio) {
        BooleanBuilder predicates = new BooleanBuilder();
        if (nonNull(getComunidade())) {
            predicates.and(qComunidade.nome.containsIgnoreCase(getComunidade()));
        }

        if (nonNull(getDistrito())) {
            predicates.and(qDistrito.id.in(getDistrito()));
        }

        if (nonNull(getMunicipio())) {
            predicates.and(qMunicipio.id.in(getMunicipio()));
        }

        return predicates;
    }

    public void setSubquery(JPAQuery<?> query, QComunidade qComunidade) {
        if (nonNull(getLocalidade())) {
            QLocalidade qLocalidade = QLocalidade.localidade;
            query.where(qComunidade.id.in(
                JPAExpressions.select(qLocalidade.comunidade.id)
                    .from(qLocalidade)
                    .where(qLocalidade.id.in(getLocalidade()))
                )
            );
        }
    }
}
