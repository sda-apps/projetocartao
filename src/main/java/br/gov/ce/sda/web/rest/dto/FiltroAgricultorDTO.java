package br.gov.ce.sda.web.rest.dto;

import br.gov.ce.sda.domain.*;
import br.gov.ce.sda.domain.ater.QAtendimentoAssociacao;
import br.gov.ce.sda.domain.ater.TipoATER;
import br.gov.ce.sda.repository.TipoATERRepository;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQuery;
import lombok.*;

import java.util.List;

import static java.util.Objects.nonNull;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class FiltroAgricultorDTO {

    private String cpf;

    private String nome;

    private List<Long> territorio;

    private List<Long> municipio;

    private List<Long> distrito;

    private List<Long> comunidade;

    private List<Long> projeto;

    private List<Agricultor> agricultores;

    private Integer start;

    private Integer limit;

    private Integer paginacao;

    private Long total;

    private Boolean ordenar;

    private String ordem;

    private List<Long> tipoATER;

    public BooleanBuilder getPredicates(QAgricultor agricultor) {
    	BooleanBuilder predicates = new BooleanBuilder();

    	if(nonNull(getCpf())) {
    		predicates.and(agricultor.pessoaFisica.cpf.containsIgnoreCase(getCpf()));
    	}

    	return predicates;
    }

    public BooleanBuilder getPredicates(QPessoa pessoa, QTerritorio territorio) {
        BooleanBuilder predicates = new BooleanBuilder();

        if (nonNull(getNome())) {
            predicates.and(pessoa.nome.containsIgnoreCase(getNome()));
        }

        if(nonNull(getTerritorio())) {
            predicates.and(territorio.id.in(getTerritorio()));
        }

        if(nonNull(getMunicipio())) {
            predicates.and(pessoa.municipio.id.in(getMunicipio()));
        }

        if(nonNull(getDistrito())) {
            predicates.and(pessoa.distrito.id.in(getDistrito()));
        }

        if(nonNull(getComunidade())) {
            predicates.and(pessoa.comunidade.id.in(getComunidade()));
        }

        return predicates;
    }

    public void setSubquery(JPAQuery<?> query, QAgricultor agricultor, TipoATERRepository tipoATERRepository) {
    	if(nonNull(getProjeto())) {
    		QAgricultorProjeto agricultorProjeto = QAgricultorProjeto.agricultorProjeto;

    		for (Long projetoId : getProjeto()) {
    			query.where(agricultor.id.in(JPAExpressions.select(agricultorProjeto.agricultor.id)
    				.from(agricultorProjeto)
					.where(agricultorProjeto.projeto.id.eq(projetoId))
				));
			}
    	}

    	if(nonNull(getTipoATER())){
    	    List<TipoATER> tipoATERList = tipoATERRepository.findAllById(getTipoATER());
    	    for (TipoATER tipoATER: tipoATERList){
    	        if(tipoATER.getCodigoEmaterce().compareTo(0L) == 0){
    	            query.where(agricultor.atualizacaoProjeto.eq(tipoATER.getTipo()));

    	            for (int i = 0; i < getTipoATER().size(); i++){
    	                if(getTipoATER().get(i).equals(tipoATER.getId())){
    	                    getTipoATER().remove(i);
                        }
                    }
                }
            }
            if(getTipoATER().size() > 0) {
                QAtendimentoAssociacao atendimentoAssociacao = QAtendimentoAssociacao.atendimentoAssociacao;
                query.where(agricultor.id.in(JPAExpressions.select(atendimentoAssociacao.agricultor.id)
                    .from(atendimentoAssociacao)
                    .where(atendimentoAssociacao.tipoAter.id.in(getTipoATER()))
                ));
            }
    	}
    }

}
