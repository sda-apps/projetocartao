package br.gov.ce.sda.web.rest;

import java.util.List;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ce.sda.domain.ater.AtendimentoATER;
import br.gov.ce.sda.domain.ater.AtendimentoATERIndicador;
import br.gov.ce.sda.repository.CustomATERRepository;
import br.gov.ce.sda.web.rest.dto.AtendimentoAssociacaoDTO;
import br.gov.ce.sda.web.rest.dto.AtendimentoDTO;

@RestController
@RequestMapping("/api")
public class ATERResource {

	@Inject
	private CustomATERRepository customATERRepository;

	@RequestMapping(value = "/ater/tecnico/{agricultorId}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AtendimentoAssociacaoDTO>> buscarTecnicoAgricultor(@PathVariable Long agricultorId) {

		List <AtendimentoAssociacaoDTO> atendimentoAssociacao = customATERRepository.buscarTecnicosPorAgricultor(agricultorId);

		return new ResponseEntity<>(atendimentoAssociacao, atendimentoAssociacao.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
	}

	@RequestMapping(value = "/ater/atendimento",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AtendimentoATER>> buscarAtendimentoAgricultor(AtendimentoDTO filtro) {

		List <AtendimentoATER> atendimentoATER = customATERRepository.buscarAtendimentoPorAgricultor(filtro);

		return new ResponseEntity<>(atendimentoATER, atendimentoATER.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
	}

	@RequestMapping(value = "/ater/atendimento/indicadores/{atendimentoId}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AtendimentoATERIndicador>> buscarIndicadoresAtendimento(@PathVariable Long atendimentoId) {

		List <AtendimentoATERIndicador> atendimentoATERIndicador = customATERRepository.buscarIndicadorPorAtendimento(atendimentoId);

		return new ResponseEntity<>(atendimentoATERIndicador, atendimentoATERIndicador.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
	}









}
