package br.gov.ce.sda.web.rest.dto;

import static java.util.Objects.nonNull;

import com.querydsl.core.BooleanBuilder;

import br.gov.ce.sda.domain.QPessoa;
import br.gov.ce.sda.domain.QUsuarioAvancado;
import br.gov.ce.sda.domain.acesso.QUsuario;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FiltroUsuarioDTO {

	private Long usuario;

    private String nome;

    private String cpf;

    private Long empresa;

    private Long cargo;

    private String status;

    private String ordenarPor;

    private Integer start;

    private Integer limit;

    private Integer paginacao;

    private Boolean ordenar;

    private Boolean ordemAsc;

    public BooleanBuilder getPredicates(QPessoa pessoa) {
    	BooleanBuilder predicates = new BooleanBuilder();

	    if (nonNull(getNome())) {
	    	predicates.and(pessoa.nome.containsIgnoreCase(getNome()));
		}

    	return predicates;
    }

    public BooleanBuilder getPredicates(QUsuario usuario) {
    	BooleanBuilder predicates = new BooleanBuilder();

	    if (nonNull(getUsuario())) {
	    	predicates.and(usuario.id.eq(getUsuario()));
		}

        if (nonNull(getStatus())) {
            predicates.and(usuario.status.eq(getStatus()));
        }

    	return predicates;
    }

    public BooleanBuilder getPredicates(QUsuarioAvancado usuarioAvancado) {
    	BooleanBuilder predicates = new BooleanBuilder();

	    if (nonNull(getEmpresa())) {
	    	predicates.and(usuarioAvancado.empresa.id.eq(getEmpresa()));
		}

	    if (nonNull(getCargo())) {
	    	predicates.and(usuarioAvancado.cargo.id.eq(getCargo()));
		}
	    
	    if (nonNull(getCpf())) {
	    	predicates.and(usuarioAvancado.pessoaFisica.cpf.eq(getCpf()));
		}

    	return predicates;
    }

}
