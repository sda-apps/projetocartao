package br.gov.ce.sda.web.rest;

import javax.inject.Inject;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ce.sda.service.JiraService;
import br.gov.ce.sda.service.MailService;
import br.gov.ce.sda.web.rest.dto.FeedbackDTO;

@RestController
@RequestMapping("/api")
public class FeedbackResource {
	
	@Inject
	private JiraService jiraService;
	
    @Inject
    private MailService mailService;
	
	@RequestMapping(value = "/feedback", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> enviarFeedback(@RequestBody FeedbackDTO feedback) throws Exception {
		jiraService.createIssue(feedback);
		String nome = feedback.getUserName();
		String email = feedback.getEmail();
		mailService.sendFeedbackEmail(nome, email);
		return ResponseEntity.ok().build();
	}
	
}
