package br.gov.ce.sda.web.rest.ater;

import br.gov.ce.sda.domain.ater.MetaAtividadesTrimestral;
import br.gov.ce.sda.domain.ater.PlanoTrimestral;
import br.gov.ce.sda.repository.ater.PlanoTrimestralRepository;
import br.gov.ce.sda.service.ater.PlanoTrimestralService;
import br.gov.ce.sda.web.rest.dto.MetaAtividadesTrimestralDTO;
import br.gov.ce.sda.web.rest.dto.PlanoTrimestralDTO;
import br.gov.ce.sda.web.rest.dto.ater.filtros.FiltroPlanoTrimestralDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping(value = "/api/planotrimestral")
public class PlanoTrimestralResource {

    private final PlanoTrimestralService planoTrimestralService;
    private final PlanoTrimestralRepository planoTrimestralRepository;

    public PlanoTrimestralResource(
        PlanoTrimestralService planoTrimestralService,
        PlanoTrimestralRepository planoTrimestralRepository
    ) {
        this.planoTrimestralService = planoTrimestralService;
        this.planoTrimestralRepository = planoTrimestralRepository;
    }

    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PlanoTrimestral> save(@RequestBody List<MetaAtividadesTrimestral> metasTrimestrais) {
        return planoTrimestralService.save(metasTrimestrais);
    }

    @RequestMapping(value = "/pta/{id}", method = GET, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity<List<PlanoTrimestral>> findByPlanoAnual(@PathVariable Long id) {
        List<PlanoTrimestral> planos = planoTrimestralRepository.findByPlanoTrabalhoAnual_Id(id);

        return new ResponseEntity<>(planos, planos.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value = "/pta-lista/{id}", method = GET, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity<List<PlanoTrimestral>> findAllByPlanoAnual(@PathVariable List<Long> id) {
        List<PlanoTrimestral> planos = planoTrimestralRepository.findByPlanoTrabalhoAnual_IdIn(id);

        return new ResponseEntity<>(planos, planos.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = GET, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity<List<MetaAtividadesTrimestralDTO>> findById(@PathVariable Long id) {
        List<MetaAtividadesTrimestralDTO> metas = planoTrimestralService.findById(id);

        return new ResponseEntity<>(metas, metas.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = DELETE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(@PathVariable Long id) {
        planoTrimestralService.delete(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, value = "summary/{id}")
    public ResponseEntity update(@PathVariable Long id, @RequestBody PlanoTrimestral planoTrimestral) throws URISyntaxException {
        Optional<PlanoTrimestral> planoTrimestralAtual = planoTrimestralRepository.findById(id);

        if (!planoTrimestralAtual.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return ok(planoTrimestralService.updateSummary(planoTrimestral, planoTrimestralAtual.get()));
    }

    @RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, value = "/{id}")
    public ResponseEntity update(@PathVariable Long id, @RequestBody PlanoTrimestralDTO dto) throws URISyntaxException {
        Optional<PlanoTrimestral> planoTrimestralAtual = planoTrimestralRepository.findById(id);

        if (!planoTrimestralAtual.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        BeanUtils.copyProperties(dto.getPlanoTrimestral(), planoTrimestralAtual.get());

        PlanoTrimestral planoTrimestral = planoTrimestralService.update(planoTrimestralAtual.get(), dto.getMetasTrimestrais());

        return ok(planoTrimestral);
    }

    @RequestMapping(value = "/avaliacao/filtro", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity findAll(FiltroPlanoTrimestralDTO filter) {
        List planosTrimestrais = planoTrimestralService.findAll(filter);

        return new ResponseEntity<>(planosTrimestrais, planosTrimestrais.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value = "/copy/{id}", method = GET, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity copy(@PathVariable Long id) throws CloneNotSupportedException {
        Optional<PlanoTrimestral> planoTrimestral = planoTrimestralRepository.findById(id);

        if(planoTrimestral.isPresent()) {
            planoTrimestralService.copy(planoTrimestral.get());

            return ok(planoTrimestral);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
