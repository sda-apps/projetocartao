package br.gov.ce.sda.web.rest.dto.ater;

import static java.util.Objects.nonNull;

import java.util.List;

import br.gov.ce.sda.domain.ater.*;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQuery;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class FiltroPlanoTrabalhoAnualDTO {

    private List<Integer> ano;

    private List<Long> contrato;

    private List<Long> parceira;

    private List<Long> regiao;

    private List<Long> municipio;

    private List<String> status;

    private Integer start;

    private Integer limit;

    private Integer paginacao;

    private Long total;

    private Boolean ordenar;

    private String ordem;

    private Regiao regiaoUsuario;


    public BooleanBuilder getPredicates(QPlanoTrabalhoAnual planoAnual) {
    	BooleanBuilder predicates = new BooleanBuilder();

	    if (nonNull(getAno())) {
	    	predicates.and(planoAnual.dataInicio.year().in(getAno()));
		}

        if (nonNull(getStatus())) {
            predicates.and(planoAnual.status.in(getStatus()));
        }

    	if(nonNull(getContrato())) {
    		predicates.and(planoAnual.contrato.id.in(getContrato()));
    	}

    	if(nonNull(getParceira())) {
    		predicates.and(planoAnual.contrato.empresa.id.in(getParceira()));
    	}

 		if(nonNull(getRegiao())) {
 			predicates.and(planoAnual.contrato.regiao.id.in(getRegiao()));
		}

        if(nonNull(getRegiaoUsuario())) {
            predicates.and(planoAnual.contrato.regiao.id.eq(regiaoUsuario.getId()));
        }

    	return predicates;
    }

    public void setSubquery(JPAQuery<?> query, QContrato contrato) {
    	if(nonNull(getMunicipio())) {
    		QAbrangenciaContrato abrangenciaContrato = QAbrangenciaContrato.abrangenciaContrato;

    		for (Long municipioId : getMunicipio()) {
    			query.where(contrato.id.in(JPAExpressions.select(abrangenciaContrato.contrato.id)
    				.from(abrangenciaContrato)
					.where(abrangenciaContrato.municipio.id.eq(municipioId))
				));
			}
    	}

    }

    public BooleanBuilder getPredicates(QAbrangenciaContrato abrangenciaContrato) {
    	BooleanBuilder predicates = new BooleanBuilder();

    	if(nonNull(getMunicipio())) {
			predicates.and(abrangenciaContrato.municipio.id.in(getMunicipio()));
		}

    	return predicates;
    }

    public BooleanBuilder getPredicatesPlanoAnualERP(QViewPlanosAnuaisERP qViewPlanosAnuaisERP) {
        BooleanBuilder predicates = new BooleanBuilder();

        if (nonNull(getStatus())) {
            predicates.and(qViewPlanosAnuaisERP.status.in(getStatus()));
        }

        if(nonNull(getRegiao())) {
            predicates.and(qViewPlanosAnuaisERP.regiaoId.in(getRegiao()));
        }

        if(nonNull(getParceira())) {
            predicates.and(qViewPlanosAnuaisERP.empresaId.in(getParceira()));
        }

        if(nonNull(getContrato())) {
            predicates.and(qViewPlanosAnuaisERP.contratoId.in(getContrato()));
        }

        if(nonNull(getRegiaoUsuario())) {
            predicates.and(qViewPlanosAnuaisERP.regiaoId.eq(regiaoUsuario.getId()));
        }

        return predicates;
    }

    public BooleanBuilder getPredicatesPlanoAnualUGP(QViewPlanosAnuaisUGP qViewPlanosAnuaisUGP) {
        BooleanBuilder predicates = new BooleanBuilder();

        if(nonNull(getRegiao())) {
            predicates.and(qViewPlanosAnuaisUGP.regiaoId.in(getRegiao()));
        }

        if (nonNull(getStatus())) {
            predicates.and(qViewPlanosAnuaisUGP.status.in(getStatus()));
        }

        if(nonNull(getParceira())) {
            predicates.and(qViewPlanosAnuaisUGP.empresaId.in(getParceira()));
        }

        if(nonNull(getContrato())) {
            predicates.and(qViewPlanosAnuaisUGP.contratoId.in(getContrato()));
        }

        return predicates;
    }

}
