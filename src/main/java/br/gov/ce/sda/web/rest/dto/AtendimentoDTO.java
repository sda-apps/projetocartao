package br.gov.ce.sda.web.rest.dto;

import static java.util.Objects.nonNull;

import com.querydsl.core.BooleanBuilder;

import br.gov.ce.sda.domain.ater.QAtendimentoATER;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter @Setter
public class AtendimentoDTO {
	
	private Long id;
	private Integer ano;
	private Integer mes;
    private String tecnico;
    private String atividade;

    public AtendimentoDTO() {}

	public AtendimentoDTO(Long id, Integer ano, Integer mes, String tecnico, String atividade) {
		this.id = id;
		this.tecnico = tecnico;
		this.ano = ano;
		this.mes = mes;
		this.atividade = atividade;
	}
	
    public BooleanBuilder getPredicates(QAtendimentoATER atendimentoATER) {
    	BooleanBuilder predicates = new BooleanBuilder();
	    
	    if (nonNull(getId())) {
	    	predicates.and(atendimentoATER.agricultor.id.in(getId()));
		}
	    
    	if(nonNull(getTecnico())) {
    		predicates.and(atendimentoATER.tecnico.pessoaFisica.pessoa.nome.containsIgnoreCase(getTecnico()));
    	}
    	
 		if(nonNull(getAno())) {
 			predicates.and(atendimentoATER.dataAtendimento.year().eq(getAno()));
		}
		
		if(nonNull(getMes())) {
			predicates.and(atendimentoATER.dataAtendimento.month().eq(getMes()));
		}
	
		if(nonNull(getAtividade())) {
			predicates.and(atendimentoATER.atividadeEspecifica.nome.eq(getAtividade()));
		}
		
    	return predicates;
    }
    
}
