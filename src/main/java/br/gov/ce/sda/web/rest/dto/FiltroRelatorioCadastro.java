package br.gov.ce.sda.web.rest.dto;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class FiltroRelatorioCadastro {

	private String dataInicio;

    private String dataFim;

    private String regiao;

    private String municipio;

    private String comunidade;

    private String tipoComunidade;
}
