package br.gov.ce.sda.web.rest.dto;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class FiltroRelatorioAnaliticoParceiraDTO {

    private String parceira;
    private boolean equipe;
    private boolean area;
    private boolean metas;
    private boolean planos;
    private String tipo;

}
