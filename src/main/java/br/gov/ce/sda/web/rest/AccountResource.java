package br.gov.ce.sda.web.rest;

import br.gov.ce.sda.Messages;
import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.repository.UserRepository;
import br.gov.ce.sda.service.UserService;
import br.gov.ce.sda.web.rest.dto.KeyAndPasswordDTO;
import br.gov.ce.sda.web.rest.dto.KeyAndUserDTO;
import br.gov.ce.sda.web.rest.dto.ResponseMessage;
import br.gov.ce.sda.web.rest.dto.UserDTO;
import br.gov.ce.sda.web.rest.util.HttpUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

import static br.gov.ce.sda.MessageKey.*;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
public class AccountResource {

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserService userService;

    /**
     * GET  /activate -> activate the registered user.
     */
    @RequestMapping(value = "/activate", method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> activateAccount(@RequestParam(value = "key") String key) {
        return Optional.ofNullable(userService.activateRegistration(key))
            .map(user -> new ResponseEntity<String>(HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    @RequestMapping(value = "/account/inactive", method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> inactiveAccount(@RequestBody List<Usuario> usuarios) {
        usuarios.forEach(usuario -> userService.inativarUsuario(usuario.getId()));
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/account/activate", method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> activateAccount(@RequestBody List<Usuario> usuarios) {
        usuarios.forEach(usuario -> userService.ativarUsuario(usuario.getId()));
        return ResponseEntity.ok().build();
    }

    /**
     * GET  /authenticate -> check if the user is authenticated, and return its login.
     */
    @RequestMapping(value = "/authenticate",
        method = GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("REST request to check if the current user is authenticated");
        return request.getRemoteUser();
    }

    /**
     * GET  /account -> get the current user.
     */
    @RequestMapping(value = "/account",
        method = GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDTO> getAccount() {
        return Optional.ofNullable(userService.getUserWithAuthorities())
            .map(user -> new ResponseEntity<>(new UserDTO(user), HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * POST  /change_password -> changes the current user's password
     */
    @RequestMapping(value = "/account/change_password",
        method = POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> changePassword(@RequestBody String password) {
        if (!checkPasswordLength(password)) {
            return new ResponseEntity<>("Incorrect password", HttpStatus.BAD_REQUEST);
        }
        userService.changePassword(password);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/account/reset_password/init", method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> requestPasswordReset(@RequestBody String email, HttpServletRequest request) {
        String baseUrl = HttpUtil.PORTAL_AGRICULTURA_FAMILIAR_URL;
        return userService.requestPasswordReset(email, baseUrl)
            .map(user -> ResponseEntity.ok(new ResponseMessage(Messages.getString(EMAIL_ENVIADO_SUCESSO))))
            .orElse(new ResponseEntity(new ResponseMessage(Messages.getString(EMAIL_NAO_REGISTRADO)), HttpStatus.NOT_FOUND));
    }

    @RequestMapping(value = "/account/reset_password/finish", method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> finishPasswordReset(@RequestBody KeyAndPasswordDTO keyAndPassword) {
        if (!checkPasswordLength(keyAndPassword.getNewPassword())) {
            return new ResponseEntity<>("Incorrect password", HttpStatus.BAD_REQUEST);
        }
        return userService.completePasswordReset(keyAndPassword.getNewPassword(), keyAndPassword.getKey())
            .map(user -> new ResponseEntity<String>(HttpStatus.OK)).orElse(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    @RequestMapping(value = "/account/signup/finish", method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> finishSignUp(@RequestBody KeyAndUserDTO keyAndUser) {
        if (!checkPasswordLength(keyAndUser.getPassword())) {
            return new ResponseEntity<>(new ResponseMessage(Messages.getString(TAMANHO_INCORRETO_SENHA)), HttpStatus.BAD_REQUEST);
        }
        String baseUrl = HttpUtil.PORTAL_AGRICULTURA_FAMILIAR_URL;
        Optional<Usuario> usuario = userService.completeSignUp(keyAndUser.getKey(), keyAndUser.getLogin(), keyAndUser.getPassword(), baseUrl);
        return ResponseEntity.ok(usuario);
    }

    @RequestMapping(value = "/account/approve/accessrequest", method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> approveAccessRequest(@RequestBody List<Usuario> usuarios) {
        userService.approveAccessRequest(usuarios);
        return ResponseEntity.ok().build();
    }

    private boolean checkPasswordLength(String password) {
        return (!StringUtils.isEmpty(password) &&
            password.length() >= UserDTO.PASSWORD_MIN_LENGTH &&
            password.length() <= UserDTO.PASSWORD_MAX_LENGTH);
    }

}
