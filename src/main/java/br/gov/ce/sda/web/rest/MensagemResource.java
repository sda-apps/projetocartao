package br.gov.ce.sda.web.rest;

import java.util.List;

import javax.inject.Inject;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ce.sda.service.MensagemService;
import br.gov.ce.sda.web.rest.dto.MensagemDTO;

@RestController
@RequestMapping(value = "/api/mensagens", produces = MediaType.APPLICATION_JSON_VALUE)
public class MensagemResource {
	
	@Inject
	private MensagemService mensagemService;
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> sendMessage(@RequestBody(required=true) MensagemDTO mensagem) throws Exception {
		mensagemService.sendMessage(mensagem);
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAllMessages(@RequestParam(required=true) String de, @RequestParam(required=true) String para) throws Exception {
		List<MensagemDTO> mensagens = mensagemService.getAllMessages(de, para);
		return ResponseEntity.ok(mensagens);
	}
	
}
