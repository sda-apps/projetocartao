package br.gov.ce.sda.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PowerBIRequestReportDTO {

    private String token;
    private String groups;
    private String reports;

}
