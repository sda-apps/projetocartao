package br.gov.ce.sda.web.rest.ater;

import br.gov.ce.sda.config.security.SecurityUtils;
import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.domain.ater.Tema;
import br.gov.ce.sda.domain.ater.TemaEspecificacao;
import br.gov.ce.sda.service.ater.TemaService;
import br.gov.ce.sda.web.rest.dto.FiltroTemaDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

import static java.util.Objects.isNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping(value = "/api/ater/tema")
public class TemaResource {

    @Inject
    private TemaService temaService;

    @RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Tema>> findAll() {
        List<Tema> temas = temaService.findAll();

        return new ResponseEntity<>(temas, temas.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value = "/filtro", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Tema>> findAllFilter(FiltroTemaDTO filtro) {
        List<Tema> temas = temaService.findAllByFilter(filtro);

        return new ResponseEntity<>(temas, temas.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> save(@RequestBody Tema tema) {
        Usuario usuario = SecurityUtils.getCurrentUser();
        tema.setUsuario(usuario);

        for(TemaEspecificacao especificacao: tema.getEspecificacoes()){
            especificacao.setTema(tema);
        }

        Tema savedTema = temaService.save(tema);

        return new ResponseEntity<>(savedTema, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = DELETE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(@PathVariable Long id) {
        temaService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/{id}", method = PUT, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Tema tema) {
        Tema currentTema = temaService.findById(id);
        if (isNull(currentTema)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Usuario usuario = SecurityUtils.getCurrentUser();
        tema.setUsuario(usuario);

        for(TemaEspecificacao especificacao: tema.getEspecificacoes()){
            especificacao.setTema(tema);
        }

        BeanUtils.copyProperties(tema, currentTema);

        temaService.update(currentTema);
        return ResponseEntity.ok().body(currentTema);
    }


}
