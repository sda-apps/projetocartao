package br.gov.ce.sda.web.rest.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TotalMetasATCsDTO {


    private String nomeFantasia;

    private Long qtdMetas;


    public TotalMetasATCsDTO() {}

    public TotalMetasATCsDTO(String nomeFantasia, Long qtdMetas) {
        this.nomeFantasia = nomeFantasia;
        this.qtdMetas = qtdMetas;
    }

}
