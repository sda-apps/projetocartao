package br.gov.ce.sda.web.rest;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.net.URISyntaxException;
import java.util.List;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ce.sda.domain.programaseprojeto.Cisterna;
import br.gov.ce.sda.repository.CisternaRepository;

@RestController
@RequestMapping("/api/cisterna")
public class CisternaResource {
	
	@Inject
	private CisternaRepository cisternaRepository;
	
	@RequestMapping(value = "/agricultor/{agricultorId}", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Cisterna>> buscarCisternasPorAgricultor(@PathVariable Long agricultorId) throws URISyntaxException {
		List<Cisterna> cisternas = cisternaRepository.buscarCisternasPorAgricultor(agricultorId);
		return new ResponseEntity<>(cisternas, cisternas.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
	}
	
}
