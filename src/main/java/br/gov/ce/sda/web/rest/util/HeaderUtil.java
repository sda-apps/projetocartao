package br.gov.ce.sda.web.rest.util;

import java.util.Objects;

import org.springframework.http.HttpHeaders;

/**
 * Utility class for http header creation.
 *
 */
public class HeaderUtil {

    private static final String MESSAGE_OBRIGATORIA = "O parâmetro message é obrigatório.";

	private static HttpHeaders createAlert(String message, MessageStatus status) {
		Objects.requireNonNull(message, MESSAGE_OBRIGATORIA);
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-projetocartaoApp-message", message);
        headers.add("X-projetocartaoApp-status", status.toString());
        return headers;
    }
    
    public static HttpHeaders sucess(String message) {
        return createAlert(message, MessageStatus.SUCCESS);
    }
    
    public static HttpHeaders info(String message) {
        return createAlert(message, MessageStatus.INFO);
    }
    
    public static HttpHeaders error(String message) {
        return createAlert(message, MessageStatus.ERROR);
    }
    
    public static HttpHeaders warning(String message) {
        return createAlert(message, MessageStatus.WARNING);
    }
    
    private enum MessageStatus {
    	
    	SUCCESS("S"), 
    	ERROR("E"),
    	INFO("I"),
    	WARNING("W");
    	
    	private String status;
    	
    	private MessageStatus(String stauts) {
    		this.status = stauts;
    	}
    	
    	@Override
    	public String toString() {
    		return this.status;
    	}
    	
    }
}


