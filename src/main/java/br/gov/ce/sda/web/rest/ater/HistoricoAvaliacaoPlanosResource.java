package br.gov.ce.sda.web.rest.ater;


import br.gov.ce.sda.domain.ater.HistoricoAvaliacaoPlanos;
import br.gov.ce.sda.service.ater.HistoricoAvaliacaoPlanosService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/api/ater/historico-avaliacao-planos")
public class HistoricoAvaliacaoPlanosResource {

    @Inject
    private HistoricoAvaliacaoPlanosService historicoPlanosService;

    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity save(@RequestBody HistoricoAvaliacaoPlanos historicoAvaliacaoPlanos) {
        HistoricoAvaliacaoPlanos historico = historicoPlanosService.save(historicoAvaliacaoPlanos);

        if (isNull(historico) && (nonNull(historicoAvaliacaoPlanos.getPlanoTrabalhoAnual()) || nonNull(historicoAvaliacaoPlanos.getPlanoTrimestral()))) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return ok(historico);
    }

    @RequestMapping(value = "/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity findByPlanoId(@PathVariable long id) {
        final String type = null;
        List<HistoricoAvaliacaoPlanos> historicoDoPlano = historicoPlanosService.findByPlanoId(id, type);

        return new ResponseEntity<>(historicoDoPlano, historicoDoPlano.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value = "/file/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity findByPlanoIdAndFileIsPresent(@PathVariable long id) {
        final String FILE_TYPE = "F";

        List<HistoricoAvaliacaoPlanos> historicoDoPlano = historicoPlanosService.findByPlanoId(id, FILE_TYPE)
            .stream()
            .filter(history -> nonNull(history.getPlanoTrimestral().getFileStatus()))
            .collect(Collectors.toList());

        return new ResponseEntity<>(historicoDoPlano, historicoDoPlano.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }
}
