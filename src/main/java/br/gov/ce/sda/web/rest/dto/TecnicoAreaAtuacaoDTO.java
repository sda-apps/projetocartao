package br.gov.ce.sda.web.rest.dto;

import java.util.List;

import br.gov.ce.sda.domain.Municipio;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder @AllArgsConstructor @NoArgsConstructor @Getter @Setter
public class TecnicoAreaAtuacaoDTO {
	
	private Long usuarioAvancadoId;
	
	private List<Municipio> municipios;

}
 