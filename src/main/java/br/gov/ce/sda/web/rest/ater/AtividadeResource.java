package br.gov.ce.sda.web.rest.ater;

import static java.util.Objects.isNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.config.security.SecurityUtils;
import br.gov.ce.sda.service.AtividadeService;
import br.gov.ce.sda.web.rest.dto.FiltroAtividadeDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import br.gov.ce.sda.domain.ater.AtividadePTA;
import br.gov.ce.sda.repository.ater.AtividadeRepository;

@RestController
@RequestMapping(value = "/api/ater/atividade")
public class AtividadeResource {

	@Inject
    private AtividadeRepository atividadeRepository;

    @Inject
    private AtividadeService atividadeService;

    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> save(@RequestBody AtividadePTA atividade) {
        Usuario usuario = SecurityUtils.getCurrentUser();
        atividade.setUsuario(usuario);
        AtividadePTA savedAtividade = atividadeService.save(atividade);

        return new ResponseEntity<>(savedAtividade, HttpStatus.OK);
    }

	@RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<List<AtividadePTA>> findAll(Boolean isEdicao){
        List<AtividadePTA> atividades;
        if(isEdicao) {
            atividades = atividadeRepository.findAll();
        } else {
            atividades = atividadeRepository.findAllByInativoIsFalseOrInativoIsNull();
        }

		return new ResponseEntity<>(atividades, atividades.isEmpty()? HttpStatus.NOT_FOUND: HttpStatus.OK);
	}

    @RequestMapping(value="/filtro", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AtividadePTA>> findAllFilter(FiltroAtividadeDTO filtro){
        List<AtividadePTA> atividades = atividadeService.findAllByFilter(filtro);

        return new ResponseEntity<>(atividades, atividades.isEmpty() ? HttpStatus.NOT_FOUND: HttpStatus.OK);
    }

    @RequestMapping(value="/{id}", method = DELETE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(@PathVariable Long id) {
        atividadeService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value="/{id}", method = PUT, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody AtividadePTA atividade) {
        AtividadePTA currentAtividade = atividadeService.findById(id);
        if (isNull(currentAtividade)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Usuario usuario = SecurityUtils.getCurrentUser();
        atividade.setUsuario(usuario);

        BeanUtils.copyProperties(atividade, currentAtividade);

        atividadeService.update(currentAtividade);
        return ResponseEntity.ok().body(currentAtividade);
    }

    @RequestMapping(value = "/{atividade}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AtividadePTA>> getAtividades(@PathVariable String atividade) {

        List<AtividadePTA> atividades = atividadeRepository.findAllByNomeIgnoreCaseContainingAndInativoIsFalse(atividade);

        if(atividades.isEmpty()){
            return new ResponseEntity<>(atividades, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(atividades, HttpStatus.OK);
    }

}
