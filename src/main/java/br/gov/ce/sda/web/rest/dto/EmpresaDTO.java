package br.gov.ce.sda.web.rest.dto;

import br.gov.ce.sda.domain.Empresa;
import br.gov.ce.sda.domain.ater.Regiao;
import lombok.*;

import java.util.List;
import java.util.stream.Collectors;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EmpresaDTO {

    private Long id;

    private String nome;

    private String nomeFantasia;

    private List<Regiao> contratoRegioes;

    public EmpresaDTO(Empresa empresa) {
        this.id = empresa.getId();
        this.nome = empresa.getPessoaJuridica().getPessoa().getNome();
        this.nomeFantasia = empresa.getPessoaJuridica().getNomeFantasia();
        this.contratoRegioes = empresa.getContrato().stream().map(contrato1 -> contrato1.getRegiao()).collect(Collectors.toList());

    }
}
