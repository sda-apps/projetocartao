package br.gov.ce.sda.web.rest.ater;

import br.gov.ce.sda.domain.ater.Tarefa;
import br.gov.ce.sda.exceptions.StorageException;
import br.gov.ce.sda.repository.ater.TarefaRepository;
import br.gov.ce.sda.service.ater.TarefaService;
import br.gov.ce.sda.service.ida.DataService;
import br.gov.ce.sda.web.rest.dto.TarefaDTO;
import br.gov.ce.sda.web.rest.dto.ater.FiltroTarefaDTO;
import br.gov.ce.sda.web.rest.util.HeaderUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RestController
@RequestMapping(value = "/api/ater/tarefa")
public class TarefaResource {

    private final TarefaRepository tarefaRepository;
    private final TarefaService tarefaService;

    @Autowired public TarefaResource(
        TarefaRepository tarefaRepository,
        TarefaService tarefaService
    ) {
        this.tarefaRepository = tarefaRepository;
        this.tarefaService = tarefaService;
    }

    @RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Tarefa>> findAll() {
        List<Tarefa> tarefas = tarefaRepository.findAll();

        return new ResponseEntity<>(tarefas, tarefas.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findById(@PathVariable Long id) {
        Tarefa tarefa = tarefaService.findById(id);

        if (isNull(tarefa)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok().body(tarefa);
    }

    @GetMapping(value = "/complemento/{id}", produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity<?> findByIdComplemento(@PathVariable("id") Long id) throws StorageException {
        Tarefa tarefa = tarefaService.findByIdComplemento(id);

        if (isNull(tarefa)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok().body(tarefa);
    }

    @RequestMapping(value = "/filtro", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity findAll(FiltroTarefaDTO filtro) {

        List<Tarefa> tarefas = tarefaService.findByFilters(filtro);

        List<TarefaDTO> tarefasDTO = tarefas.stream().map(TarefaDTO::new).collect(Collectors.toList());

        return new ResponseEntity<>(tarefasDTO, tarefasDTO.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value = "/filtro/total", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FiltroTarefaDTO> buscarTotalAgricultoresPorFiltro(FiltroTarefaDTO filtro) {

        Long total = tarefaService.findTotalByFilters(filtro);

        return new ResponseEntity<>(FiltroTarefaDTO.builder().total(total).build(), HttpStatus.OK);
    }


    @RequestMapping(method = PUT, produces = MediaType.APPLICATION_JSON_VALUE, value = "/{id}")
    public ResponseEntity<Tarefa> evaluateTask(@PathVariable Long id, @RequestBody Tarefa tarefa) throws StorageException {
        Tarefa tarefaAtual = tarefaService.findById(id);

        if (!isNull(tarefaAtual)) {
            tarefaService.evaluateTask(tarefa, tarefaAtual);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok().headers(HeaderUtil.sucess("Avaliação realizada com sucesso")).body(tarefa);

    }

    @RequestMapping(value = "/anexo/{id}", method = PUT, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Tarefa> updateUser(@PathVariable Long id, @RequestBody Tarefa tarefa) {
        Tarefa tarefaAtual = tarefaService.findById(id);

        if (!isNull(tarefaAtual)) {
            tarefaService.setAnexo(tarefa, tarefaAtual);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok().body(tarefa);
    }

}
