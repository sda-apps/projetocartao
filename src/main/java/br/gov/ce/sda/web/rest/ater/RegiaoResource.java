package br.gov.ce.sda.web.rest.ater;

import br.gov.ce.sda.domain.Municipio;
import br.gov.ce.sda.domain.ater.AbrangenciaRegiao;
import br.gov.ce.sda.domain.ater.Regiao;
import br.gov.ce.sda.repository.MunicipioRepository;
import br.gov.ce.sda.repository.ater.CustomAbrangenciaRegiaoRepository;
import br.gov.ce.sda.repository.ater.RegiaoRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping(value = "/api/ater/regiao")
public class RegiaoResource {

	@Inject
	private RegiaoRepository regiaoRepository;

    @Inject
    private CustomAbrangenciaRegiaoRepository customAbrangenciaRegiaoRepository;

	@RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Regiao>> findAll(){
		List<Regiao> regioes = regiaoRepository.findAll();

		return new ResponseEntity<>(regioes, regioes.isEmpty()? HttpStatus.NOT_FOUND: HttpStatus.OK);
	}

    @RequestMapping(value = "/municipio/{nomeMunicipio}/{regiao}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Municipio>> getMunicipioPorRegiao(@PathVariable String nomeMunicipio, @PathVariable Long regiao) {

        List<AbrangenciaRegiao> abrangenciaRegioes = customAbrangenciaRegiaoRepository.findByRegiao(nomeMunicipio, regiao);

        List<Municipio> municipios = new ArrayList<>();

        for(AbrangenciaRegiao abrangenciaRegiao: abrangenciaRegioes){
            municipios.add(abrangenciaRegiao.getMunicipio());
        }

        return new ResponseEntity<>(municipios, municipios.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

}
