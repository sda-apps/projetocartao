package br.gov.ce.sda.web.rest;


import br.gov.ce.sda.domain.Localidade;
import br.gov.ce.sda.service.LocalidadeService;
import br.gov.ce.sda.web.rest.dto.FiltroLocalidadeDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

import static java.util.Objects.isNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/api/localidades")
public class LocalidadeResource {

    @Inject
    private LocalidadeService localidadeService;

    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> save(@RequestBody Localidade localidade) {

        Localidade savedLocalidade = localidadeService.save(localidade);

        return new ResponseEntity<>(savedLocalidade, HttpStatus.OK);
    }

    @RequestMapping(value="/filtro", method = GET, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Localidade>> findAllFilter(FiltroLocalidadeDTO filtro){
		List<Localidade> localidades = localidadeService.findAllByFilter(filtro);

		return new ResponseEntity<>(localidades, localidades.isEmpty() ? HttpStatus.NOT_FOUND: HttpStatus.OK);
	}

    @RequestMapping(value="/{id}", method = DELETE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(@PathVariable Long id) {
        localidadeService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value="/{id}", method = PUT, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Localidade localidade) {
        Localidade currentLocalidade = localidadeService.findById(id);
        if (isNull(currentLocalidade)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        BeanUtils.copyProperties(localidade, currentLocalidade);

        localidadeService.update(currentLocalidade);
        return ResponseEntity.ok().body(currentLocalidade);
    }

    @RequestMapping(value = "/comunidade/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Localidade>> getLocalidadesPorComunidade(@PathVariable List<Long> id) {

        List<Localidade> localidades = localidadeService.findByComunidades(id);

        return new ResponseEntity<>(localidades, localidades.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

}
