package br.gov.ce.sda.web.rest.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter @ToString
public class MensagemDTO {
	
	private String de;
	
	private String para;
	
	private String mensagem;
	
	private Date dataEnvio;
	
	private String horaEnvio;
	
	private Boolean enviado;
	
}
