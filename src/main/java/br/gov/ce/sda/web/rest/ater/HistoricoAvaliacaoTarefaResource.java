package br.gov.ce.sda.web.rest.ater;

import br.gov.ce.sda.config.security.SecurityUtils;
import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.domain.ater.HistoricoAvaliacaoTarefa;
import br.gov.ce.sda.service.ater.HistoricoAvaliacaoTarefaService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/api/ater/historico-avaliacao-tarefa")
public class HistoricoAvaliacaoTarefaResource {

    @Inject
    private HistoricoAvaliacaoTarefaService historicoAvaliacaoTarefaService;

    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity save(@RequestBody HistoricoAvaliacaoTarefa historicoAvaliacaoTarefa) {

        Usuario usuario = SecurityUtils.getCurrentUser();

        historicoAvaliacaoTarefa.setUsuario(usuario);

        HistoricoAvaliacaoTarefa historico = historicoAvaliacaoTarefaService.save(historicoAvaliacaoTarefa);

        if (isNull(historico) && nonNull(historicoAvaliacaoTarefa.getTarefa())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return ok(historico);
    }


    @RequestMapping(value = "/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity findByTarefaId(@PathVariable long id) {

        List<HistoricoAvaliacaoTarefa> historico = historicoAvaliacaoTarefaService.findByTarefaId(id);

        return new ResponseEntity<>(historico, historico.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }
}
