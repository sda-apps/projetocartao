package br.gov.ce.sda.web.rest;

import br.gov.ce.sda.config.security.AuthoritiesConstants;
import br.gov.ce.sda.domain.PessoaFisica;
import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.repository.*;
import br.gov.ce.sda.service.UserService;
import br.gov.ce.sda.web.rest.dto.FiltroUsuarioDTO;
import br.gov.ce.sda.web.rest.dto.UsuarioDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static br.gov.ce.sda.web.rest.util.HeaderUtil.error;
import static br.gov.ce.sda.web.rest.util.HeaderUtil.sucess;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Optional.ofNullable;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * REST controller for managing users.
 * <p>
 * <p>This class accesses the User entity, and needs to fetch its collection of authorities.</p>
 * <p>
 * For a normal use-case, it would be better to have an eager relationship between User and Authority,
 * and send everything to the client side: there would be no DTO, a lot less code, and an outer-join
 * which would be good for performance.
 * </p>
 * <p>
 * We use a DTO for 3 reasons:
 * <ul>
 * <li>We want to keep a lazy association between the user and the authorities, because people will
 * quite often do relationships with the user, and we don't want them to get the authorities all
 * the time for nothing (for performance reasons). This is the #1 goal: we should not impact our users'
 * application because of this use-case.</li>
 * <li> Not having an outer join causes n+1 requests to the database. This is not a real issue as
 * we have by default a second-level cache. This means on the first HTTP call we do the n+1 requests,
 * but then all authorities come from the cache, so in fact it's much better than doing an outer join
 * (which will get lots of data from the database, for each HTTP call).</li>
 * <li> As this manages users, for security reasons, we'd rather have a DTO layer.</li>
 * </p>
 * <p>Another option would be to have a specific JPA entity graph to handle this case.</p>
 */
@RestController
@RequestMapping(value = "/api")
public class UserResource {

    private final Logger log = LoggerFactory.getLogger(UserResource.class);

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserService userService;

    @Inject
    private CustomUserRepository customUserRepository;

    @Inject
    private CustomUsuarioAvancadoRepository customUsuarioAvancadoRepository;

    @Inject
    private UsuarioAvancadoRepository usuarioAvancadoRepository;

    @Inject
    private PessoaFisicaRepository pessoaFisicaRepository;


    @RequestMapping(value = "/users", method = POST, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Usuario> createUser(@RequestBody Usuario usuario) {
        PessoaFisica pessoaFisica = pessoaFisicaRepository.findByCpf(usuario.getUsuarioAvancado().getPessoaFisica().getCpf());

        if (nonNull(pessoaFisica)) {
            Usuario usuarioExistente = customUserRepository.buscarUsuarioPorCpf(pessoaFisica.getCpf());

            if (nonNull(usuarioExistente)) {
                return new ResponseEntity<>(CONFLICT);
            }
        }

        usuario = userService.cadastrarUsuairo(usuario);

        return ok().headers(sucess("Usuario " + usuario.getUsuarioAvancado().getPessoaFisica().getPessoa().getNome() + " foi adicionado com sucesso.")).body(usuario);
    }

    @RequestMapping(value = "/users/public", method = POST, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Usuario> createUserPublic(@RequestBody Usuario usuario) {
        usuario = userService.cadastrarUsuairo(usuario);
        return ok().headers(sucess("Usuario " + usuario.getUsuarioAvancado().getPessoaFisica().getPessoa().getNome() + " foi adicionado com sucesso.")).body(usuario);
    }

    /**
     * PUT  /users -> Updates an existing User.
     */

    @RequestMapping(value = "/users/{id}", method = PUT, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Usuario> updateUser(@PathVariable Long id, @RequestBody Usuario usuario) throws URISyntaxException {
        Usuario usuarioAtual = userService.updateUsuario(id, usuario);

        if (isNull(usuarioAtual)) {
            return new ResponseEntity<>(NOT_FOUND);
        }

        String nome = usuarioAtual.getUsuarioAvancado().getPessoaFisica().getPessoa().getNome();

        if (!usuarioAtual.isEnabled()) {
            userService.reenviarSignUpEmail(usuario);
            return ok().headers(sucess("Foi reenviado um convite para " + nome)).body(usuarioAtual);
        }

        return ok().headers(sucess("Os dados de " + nome + " foram atualizados com sucesso.")).body(usuarioAtual);
    }

    @RequestMapping(value = "/users",
        method = GET,
        produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Usuario>> getAllUsers(FiltroUsuarioDTO filtro) {
        List<Usuario> usuarios = customUserRepository.buscarTodosUsuarios(filtro);
        return new ResponseEntity<>(usuarios, usuarios.isEmpty() ? NOT_FOUND : OK);
    }


    /**
     * GET  /users/:login -> get the "login" user.
     */
    /*@RequestMapping(value = "/users/{login:[_'.@a-z0-9-]+}", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ManagedUserDTO> getUser(@PathVariable String login) {
        log.debug("REST request to get User : {}", login);
        return userService.getUserWithAuthoritiesByLogin(login)
                .map(ManagedUserDTO::new)
                .map(managedUserDTO -> new ResponseEntity<>(managedUserDTO, OK))
                .orElse(new ResponseEntity<>(NOT_FOUND));
    }*/

    /**
     * DELETE  USER :login -> delete the "login" User.
     */
    @RequestMapping(value = "/users/{login}",
        method = DELETE,
        produces = APPLICATION_JSON_VALUE)
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<Void> deleteUser(@PathVariable String login) {
        Optional<Usuario> opt = userRepository.findOneByLogin(login);
        if (opt.isPresent()) {
            log.debug("REST request to delete User: {}", login);
            userService.deleteUserInformation(login);
            return ok().headers(sucess("Um usuário foi excluído com o identificador " + login)).build();
        }
        return notFound().headers(error("Nao existe usuario para ser excluído com o identificador " + login)).build();
    }

    @RequestMapping(value = "/users/requestaccess", method = GET, produces = APPLICATION_JSON_VALUE, params = "cpf")
    public ResponseEntity<?> solicitarAcesso(@RequestParam(name = "cpf") String cpf) {
        ResponseEntity<?> response = userService.solicitarAcesso(cpf);
        return response;
    }

    @RequestMapping(value = "/users/company/name/{nome}",
        method = GET,
        produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Usuario>> buscarUsuariosEmpresaPorNome(@PathVariable String nome) {
        List<Usuario> response = userService.buscarUsuariosEmpresaPorNome(nome);

        return new ResponseEntity<>(response, response.isEmpty() ? NOT_FOUND : OK);
    }

    @RequestMapping(value = "/users/company", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UsuarioDTO>> buscarUsuariosEmpresa(FiltroUsuarioDTO filters) {
        List<UsuarioDTO> usuarios = userService.buscarUsuariosEmpresa(filters);
        return new ResponseEntity<>(usuarios, usuarios.isEmpty() ? NOT_FOUND : OK);
    }


    @RequestMapping(value = "/users/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity getUserById(@PathVariable Long id) {
        return ofNullable(userService.buscarUsuarioCompletoPorId(id))
            .map(ResponseEntity::ok)
            .orElse(notFound().build());
    }

}
