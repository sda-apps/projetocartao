package br.gov.ce.sda.web.rest.ater;

import br.gov.ce.sda.config.security.SecurityUtils;
import br.gov.ce.sda.domain.acesso.Usuario;
import br.gov.ce.sda.domain.ater.EixoTematico;
import br.gov.ce.sda.service.ater.EixoTematicoService;
import br.gov.ce.sda.web.rest.dto.FiltroEixoTematicoDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

import static java.util.Objects.isNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping(value = "/api/ater/eixo-tematico")
public class EixoTematicoResource {

    @Inject
    private EixoTematicoService eixoTematicoService;

    @RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EixoTematico>> findAll() {
        List<EixoTematico> eixoTematicoList = eixoTematicoService.findAll();

        return new ResponseEntity<>(eixoTematicoList, eixoTematicoList.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value = "/filtro", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EixoTematico>> findAllFilter(FiltroEixoTematicoDTO filtro) {
        List<EixoTematico> eixoTematicoList = eixoTematicoService.findAllByFilter(filtro);

        return new ResponseEntity<>(eixoTematicoList, eixoTematicoList.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> save(@RequestBody EixoTematico eixoTematico) {
        Usuario usuario = SecurityUtils.getCurrentUser();
        eixoTematico.setUsuario(usuario);
        EixoTematico savedEixoTematico = eixoTematicoService.save(eixoTematico);

        return new ResponseEntity<>(savedEixoTematico, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = DELETE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(@PathVariable Long id) {
        eixoTematicoService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/{id}", method = PUT, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody EixoTematico eixoTematico) {
        EixoTematico currentEixoTematico = eixoTematicoService.findById(id);
        if (isNull(currentEixoTematico)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Usuario usuario = SecurityUtils.getCurrentUser();
        eixoTematico.setUsuario(usuario);

        BeanUtils.copyProperties(eixoTematico, currentEixoTematico);

        eixoTematicoService.update(currentEixoTematico);
        return ResponseEntity.ok().body(currentEixoTematico);
    }


}
