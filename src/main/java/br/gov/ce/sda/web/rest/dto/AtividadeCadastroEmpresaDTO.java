package br.gov.ce.sda.web.rest.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AtividadeCadastroEmpresaDTO {

    private String nomeFantasia;

    private Long quantidade;

    public AtividadeCadastroEmpresaDTO() {}

    public AtividadeCadastroEmpresaDTO(String nomeFantasia, Long quantidade) {
        this.nomeFantasia = nomeFantasia;
        this.quantidade = quantidade;
    }

}
