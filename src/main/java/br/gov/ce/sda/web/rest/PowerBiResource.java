package br.gov.ce.sda.web.rest;

import br.gov.ce.sda.service.PowerBiService;
import br.gov.ce.sda.web.rest.dto.PowerBIRequestReportDTO;
import br.gov.ce.sda.web.rest.dto.PowerBIResponseReportDTO;
import br.gov.ce.sda.web.rest.dto.PowerBITokenDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/powerbi")
public class PowerBiResource {

    private final PowerBiService powerBiService;

    @RequestMapping(
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PowerBITokenDTO> getToken() throws IOException {
        return ok(powerBiService.getToken());
    }

    @RequestMapping(
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PowerBIResponseReportDTO> getReport(@RequestBody PowerBIRequestReportDTO powerBIRequestReportDTO) throws IOException {
        return ok(powerBiService.getReport(powerBIRequestReportDTO));
    }

}
