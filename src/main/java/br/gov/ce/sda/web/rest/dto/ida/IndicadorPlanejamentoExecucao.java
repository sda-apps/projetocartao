package br.gov.ce.sda.web.rest.dto.ida;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = IndicadorPlanejamentoExecucao.class)
public class IndicadorPlanejamentoExecucao {

    private Long id;
    private Long indicadorProjetoCodigo;
    private IndicadorProjeto indicadorProjeto;
    private String responsavel;
    private String tipo;
    private Double quantidade;
    private Double valor;
    private LocalDate data;
    private String situacao;
    private String observacao;
    private Integer indicadorItem;
    private Double valorUnitario;
    private LocalDate dataFinal;

}
