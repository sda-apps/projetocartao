package br.gov.ce.sda.web.rest.ater;

import br.gov.ce.sda.domain.ater.*;
import br.gov.ce.sda.repository.ater.CustomTecnicoPTARepository;
import br.gov.ce.sda.repository.ater.PlanoTrabalhoAnualRepository;
import br.gov.ce.sda.repository.ater.PlanoTrimestralRepository;
import br.gov.ce.sda.service.ater.PlanoTrabalhoAnualService;
import br.gov.ce.sda.web.rest.dto.PlanoTrabalhoAnualDTO;
import br.gov.ce.sda.web.rest.dto.RelatorioSinteticoATCsDTO;
import br.gov.ce.sda.web.rest.dto.TotalMetasATCsDTO;
import br.gov.ce.sda.web.rest.dto.ater.FiltroPlanoTrabalhoAnualDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.Objects.isNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping(value = "/api/pta")
public class PlanoTrabalhoAnualResource {

    @Inject
    private PlanoTrabalhoAnualService ptaService;

    @Inject
    private CustomTecnicoPTARepository customTecnicoPTARepository;

    @Inject
    private PlanoTrimestralRepository planoTrimestralRepository;

    @Inject
    private PlanoTrabalhoAnualRepository planoTrabalhoAnualRepository;

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity save(@RequestBody PlanoTrabalhoAnual pta) {

        for (MetaAtividadesPTA metaAtividades : pta.getMetasAtividadesPta()) {
            metaAtividades.setPlanoTrabalhoAnual(pta);
        }

        for (MetaFamiliasPTA metaFamilias : pta.getMetasFamiliasPta()) {
            metaFamilias.setPlanoTrabalhoAnual(pta);
        }

        pta = ptaService.save(pta);

        return ok(pta);
    }

    @RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity findAll() {
        Set<PlanoTrabalhoAnual> planosAnuais = ptaService.findAll();
        return new ResponseEntity<>(planosAnuais, planosAnuais.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value = "/empresa/{empresaId}", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity findAllByEmpresa(@PathVariable Long empresaId) {
        Set<PlanoTrabalhoAnual> planosAnuais = ptaService.findAllByEmpresa(empresaId);
        return new ResponseEntity<>(planosAnuais, planosAnuais.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value = "/filtro", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity findAll(FiltroPlanoTrabalhoAnualDTO filtro) {

        List<PlanoTrabalhoAnualDTO> planosAnuais = ptaService.findAllByFiltro(filtro);

        return new ResponseEntity<>(planosAnuais, planosAnuais.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value = "/avaliacao/filtro", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity findAllFromView(FiltroPlanoTrabalhoAnualDTO filter) {

        List<ViewPlanosAnuais> planosTrimestrais = ptaService.findAllFromView(filter);

        return new ResponseEntity<>(planosTrimestrais, planosTrimestrais.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = GET, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity<PlanoTrabalhoAnual> findById(@PathVariable Long id) {
        PlanoTrabalhoAnual planoAnual = ptaService.findById(id);

        if (isNull(planoAnual)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(planoAnual, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, value = "/{id}")
    public ResponseEntity<PlanoTrabalhoAnual> update(@PathVariable Long id, @RequestBody PlanoTrabalhoAnual planoAnual) throws URISyntaxException, IOException {
        PlanoTrabalhoAnual planoAnualAtual = ptaService.findById(id);

        if (isNull(planoAnualAtual)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        ptaService.update(planoAnualAtual, planoAnual);

        planoAnualAtual = findById(planoAnualAtual.getId()).getBody();

        Set<TecnicoPTA> tecnicos = customTecnicoPTARepository.findByPlanoAnual(planoAnualAtual.getId());
        List<PlanoTrimestral> planos = planoTrimestralRepository.findByPlanoTrabalhoAnual_Id(id);
        Set<PlanoTrimestral> planosSet = new HashSet<PlanoTrimestral>(planos);
        planoAnualAtual.setTecnicos(tecnicos);
        planoAnualAtual.setPlanosTrimestrais(planosSet);
        return new ResponseEntity<>(planoAnualAtual, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = DELETE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity delete(@PathVariable Long id) {
        ptaService.delete(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/relatorio-atcs", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity findRelatorioSintetico() {
        List<RelatorioSinteticoATCsDTO> listRelatorio = ptaService.findRelatorioAtcs();

        for (int i = 0; i < listRelatorio.size(); i++) {
            List<TecnicoPTA> tecnicos = customTecnicoPTARepository.findTecnicoByPTA(listRelatorio.get(i).getId());
            List<TecnicoPTA> assessores = customTecnicoPTARepository.findAssessorByPTA(listRelatorio.get(i).getId());
            listRelatorio.get(i).setQtdTecnicos(tecnicos.size());
            listRelatorio.get(i).setQtdAssessor(assessores.size());
        }

        return new ResponseEntity<>(listRelatorio, listRelatorio.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value = "/count-metas-atcs", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity findAllMetas() {
        List<TotalMetasATCsDTO> listTotalMetas = ptaService.findAllMetasAtcs();

        return new ResponseEntity<>(listTotalMetas, listTotalMetas.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value = "/tecnico-pta/regiao/{tecnicoId}", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity findRegiaoPorTecnicoDoPlanoDeTrabalho(@PathVariable Long tecnicoId) {
        List<Long> regioesId = ptaService.findRegiaoPorTecnicoDoPlanoDeTrabalho(tecnicoId);

        return new ResponseEntity<>(regioesId, regioesId.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value = "/codigo/{codigo}", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity findByCodigo(@PathVariable String codigo) {

        List<PlanoTrabalhoAnual> planosAnuais = planoTrabalhoAnualRepository.findAllByCodigoIgnoreCaseContaining(codigo);

        return new ResponseEntity<>(planosAnuais, planosAnuais.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @RequestMapping(value = "/comunidade/{municipio}/{ptaId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Comunidade>> getComunidadeByMunicipioAndPta(@PathVariable Long municipio,@PathVariable Long ptaId) {

        List<Comunidade> comunidades = ptaService.findAllComunidadeByMunicipioAndPta(municipio,ptaId);

        return new ResponseEntity<>(comunidades, comunidades.isEmpty()? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

}
