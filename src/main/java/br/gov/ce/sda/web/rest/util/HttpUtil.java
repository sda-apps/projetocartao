package br.gov.ce.sda.web.rest.util;

import javax.servlet.http.HttpServletRequest;

public class HttpUtil {

    public static final String PORTAL_AGRICULTURA_FAMILIAR_URL = "http://servicos.sda.ce.gov.br/portal";

    public static String getBaseUrl(HttpServletRequest request) {
        String baseUrl = request.getScheme() +
            "://" +
            request.getServerName() +
            ":" +
            request.getServerPort() +
            request.getContextPath();

        return baseUrl;
    }

}
