package br.gov.ce.sda.web.rest.dto;

import br.gov.ce.sda.domain.QPessoa;
import br.gov.ce.sda.domain.QUsuarioAvancado;
import br.gov.ce.sda.domain.acesso.QUsuario;
import br.gov.ce.sda.domain.acesso.Usuario;
import com.querydsl.core.BooleanBuilder;
import lombok.*;

import static java.util.Objects.nonNull;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UsuarioDTO {
    public UsuarioDTO(Usuario usuario) {

        if(nonNull(usuario.getId())) {
            this.usuario = usuario.getId();
        }

        if(nonNull(usuario.getUsuarioAvancado().getPessoaFisica().getPessoa().getNome())) {
            this.nome = usuario.getUsuarioAvancado().getPessoaFisica().getPessoa().getNome();
        }

        if(nonNull(usuario.getUsuarioAvancado().getPessoaFisica().getCpf())) {
            this.cpf = usuario.getUsuarioAvancado().getPessoaFisica().getCpf();
        }

        if(nonNull(usuario.getUsuarioAvancado().getEmpresa())) {
            this.empresa = usuario.getUsuarioAvancado().getEmpresa().getPessoaJuridica().getNomeFantasia();
        }

        if(nonNull(usuario.getUsuarioAvancado().getCargo())) {
            this.cargo = usuario.getUsuarioAvancado().getCargo().getDescricao();
        }

        if(nonNull(usuario.getStatus())) {
            this.status = usuario.getStatus().getId();
        }

        if(nonNull(usuario.getUsuarioAvancado().getPessoaFisica().getPessoa().getFoto())) {
            this.foto = usuario.getUsuarioAvancado().getPessoaFisica().getPessoa().getFoto();
        }
    }

	private Long usuario;

    private String nome;

    private String cpf;

    private String empresa;

    private String cargo;

    private String status;

    private String foto;








}
