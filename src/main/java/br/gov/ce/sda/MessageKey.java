package br.gov.ce.sda;

public enum MessageKey {

	// Mensagens de erro
	USUARIO_EXISTENTE("usuario.existente"),
	CHAVE_ATIVACAO_EXPIRADA("chave.ativacao.expirada"),
	TAMANHO_INCORRETO_SENHA("tamanho.incorreto.senha"),
	EMAIL_NAO_REGISTRADO("email.nao.registrado"),
	EMAIL_EXISTENTE("email.existente"),
    PTA_CONTRATO_EXISTENTE("pta.contrato.existente"),
    CONTRATO_EXISTENTE("contrato.existente"),
    COMUNIDADE_EXISTENTE("comunidade.existente"),
    ATIVIDADE_EXISTENTE("atividade.existente"),
    ACAO_EXISTENTE("acao.existente"),
    META_TRIMESTRAL_EXISTENTE("meta.trimestral.existente"),
    FILE_NOT_FOUND_ERROR("file.notfound.error"),



	// Mensagens de sucesso
	EMAIL_ENVIADO_SUCESSO("email.enviado.sucesso"),
	PESSOA_ATUALIZADA_SUCESSO("pessoa.atualizada.sucesso"),

	EMAIL_ENVIADO_SUCESSO_PARAMETRO("email.enviado.sucesso.parametro"),
	EMAIL_PARA_CONTATO("email.para.contato"),

	// Mensagens de aviso
	AVISO_AGRICULTOR_CADASTRADO("aviso.agricultor.cadastrado"),

    // NextCloud error messages
    NEXT_CLOUD_FILE_NOT_FOUND("next.cloud.file.not.found"),
    NEXT_CLOUD_IO_EXCEPTION("next.cloud.io.exception");

	private String key;

	MessageKey(String key) {
		this.key = key;
	}

	@Override
	public String toString() {
		return this.key;
	}

}
